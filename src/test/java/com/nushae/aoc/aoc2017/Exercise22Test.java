package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	void testExample1() {
		Exercise22 ex = new Exercise22("..#\n" +
				"#..\n" +
				"...");
		assertEquals(41, ex.doPart1(70));
		assertEquals(5587, ex.doPart1(10000));
	}
}