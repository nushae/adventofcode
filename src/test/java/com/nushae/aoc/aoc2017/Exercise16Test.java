package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	void testExample1() {
		Exercise16 ex = new Exercise16("abcde", "s1,x3/4,pe/b");
		assertEquals("baedc", ex.doPart1());
		assertEquals("abcde", ex.doPart2()); // this is why there was no example ;)
	}
}