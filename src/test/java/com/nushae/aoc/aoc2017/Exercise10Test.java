package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	public void testPart1() {
		Exercise10 ex = new Exercise10(5, "3,4,1,5");
		assertEquals(12, ex.doPart1());
	}

	@ParameterizedTest
	@MethodSource("provideArgumentsForPart2")
	public void testPart2(String input, String expectedHash) {
		Exercise10 ex = new Exercise10(256, input);
		assertEquals(expectedHash, ex.doPart2());
	}
	private static Stream<Arguments> provideArgumentsForPart2() {
		return Stream.of(
				Arguments.of("", "a2582a3a0e66e6e86e3812dcb672a272"),
				Arguments.of("AoC 2017", "33efeb34ea91902bb2f59c9920caa6cd"),
				Arguments.of("1,2,3", "3efbe78a8d82f29979031a4aa0b16a9d"),
				Arguments.of("1,2,4", "63960835bcdc130f0b66d7ff4f6a5a8e")
		);
	}
}