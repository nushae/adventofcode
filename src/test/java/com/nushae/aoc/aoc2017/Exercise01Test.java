package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise01Test {

	@Test
	public void testPart1a() {
		Exercise01 ex = new Exercise01("1234");
		assertEquals(0, ex.doPart1());
	}

	@Test
	public void testPart1b() {
		Exercise01 ex = new Exercise01("91212129");
		assertEquals(9, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise01 ex = new Exercise01("1212");
		assertEquals(6, ex.doPart2());
	}
}