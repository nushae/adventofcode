package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testPart1() {
		Exercise15 ex = new Exercise15();
		assertEquals(588, ex.doPart1(65, 8921));
	}
}