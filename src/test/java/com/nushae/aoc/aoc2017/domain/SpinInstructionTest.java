package com.nushae.aoc.aoc2017.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SpinInstructionTest {

	@Test
	void testSpinZero() {
		SpinInstruction s = new SpinInstruction("s0");
		assertEquals("abcde", s.apply("abcde"));
	}

	@Test
	void testSpinPartial() {
		SpinInstruction s = new SpinInstruction("s3");
		assertEquals("cdeab", s.apply("abcde"));
	}

	@Test
	void testSpinAll() {
		SpinInstruction s = new SpinInstruction("s5");
		assertEquals("abcde", s.apply("abcde"));
	}

	@Test
	void testSpinTooSmall() {
		SpinInstruction s = new SpinInstruction("s7");
		assertEquals("abcde", s.apply("abcde"));
	}
}