package com.nushae.aoc.aoc2017.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PartnerInstructionTest {

	@Test
	void testPartnerSame() {
		PartnerInstruction e = new PartnerInstruction("pc/c");
		assertEquals("abcde", e.apply("abcde"));
	}

	@Test
	void testPartnerWithFirst() {
		PartnerInstruction e = new PartnerInstruction("pa/d");
		assertEquals("dbcae", e.apply("abcde"));
	}

	@Test
	void testPartnerWithLast() {
		PartnerInstruction e = new PartnerInstruction("pc/e");
		assertEquals("abedc", e.apply("abcde"));
	}

	@Test
	void testPartnerSuccessors() {
		PartnerInstruction e = new PartnerInstruction("pb/c");
		assertEquals("acbde", e.apply("abcde"));
	}

	@Test
	void testPartnerNoSuchLetter() {
		PartnerInstruction e = new PartnerInstruction("pa/g");
		assertEquals("abcde", e.apply("abcde"));
	}

	@Test
	void testPartnerOrderDoesntMatter() {
		PartnerInstruction e1 = new PartnerInstruction("pb/d");
		PartnerInstruction e2 = new PartnerInstruction("pd/b");
		assertEquals(e1.apply("abcde"), e2.apply("abcde"));
	}
}