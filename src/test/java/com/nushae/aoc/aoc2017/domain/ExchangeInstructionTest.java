package com.nushae.aoc.aoc2017.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExchangeInstructionTest {

	@Test
	void testExchangeSame() {
		ExchangeInstruction e = new ExchangeInstruction("x3/3");
		assertEquals("abcde", e.apply("abcde"));
	}

	@Test
	void testExchangeWithFirst() {
		ExchangeInstruction e = new ExchangeInstruction("x0/3");
		assertEquals("dbcae", e.apply("abcde"));
	}

	@Test
	void testExchangeWithLast() {
		ExchangeInstruction e = new ExchangeInstruction("x2/4");
		assertEquals("abedc", e.apply("abcde"));
	}

	@Test
	void testExchangeSuccessors() {
		ExchangeInstruction e = new ExchangeInstruction("x2/3");
		assertEquals("abdce", e.apply("abcde"));
	}

	@Test
	void testExchangeOutOfRange() {
		ExchangeInstruction e = new ExchangeInstruction("x2/7");
		assertEquals("abcde", e.apply("abcde"));
	}

	@Test
	void testExchangeOrderDoesntMatter() {
		ExchangeInstruction e1 = new ExchangeInstruction("x1/3");
		ExchangeInstruction e2 = new ExchangeInstruction("x3/1");
		assertEquals(e1.apply("abcde"), e2.apply("abcde"));
	}
}