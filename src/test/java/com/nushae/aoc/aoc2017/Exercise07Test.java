package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.DiscProg;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise07Test {

	@Test
	public void testAll() {
		Exercise07 ex = new Exercise07("pbga (66)\n" +
				"xhth (57)\n" +
				"ebii (61)\n" +
				"havc (66)\n" +
				"ktlj (57)\n" +
				"fwft (72) -> ktlj, cntj, xhth\n" +
				"qoyq (66)\n" +
				"padx (45) -> pbga, havc, qoyq\n" +
				"tknk (41) -> ugml, padx, fwft\n" +
				"jptl (61)\n" +
				"ugml (68) -> gyxo, ebii, jptl\n" +
				"gyxo (61)\n" +
				"cntj (57)");
		DiscProg root = ex.makeProgTree();
		assertEquals("tknk", root.name);
		assertEquals(60, ex.findNewWeight(root));
	}
}