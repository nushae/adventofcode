package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise04Test {

	@Test
	public void testPart1() {
		Exercise04 ex = new Exercise04("");
		assertEquals(-1, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise04 ex = new Exercise04("");
		assertEquals(-1, ex.doPart2());
	}
}