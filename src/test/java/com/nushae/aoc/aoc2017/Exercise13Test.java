package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import javax.swing.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise13Test {

	@Test
	public void testPart1() {
		Exercise13 ex = new Exercise13("0: 3\n" +
				"1: 2\n" +
				"4: 4\n" +
				"6: 4");

		if (ex.SPEED > 0) {
			SwingUtilities.invokeLater(() -> Exercise13.createAndShowGUI(ex));
		}

		assertEquals(24, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise13 ex = new Exercise13("0: 3\n" +
				"1: 2\n" +
				"4: 4\n" +
				"6: 4");

		if (ex.SPEED > 0) {
			SwingUtilities.invokeLater(() -> Exercise13.createAndShowGUI(ex));
		}

		assertEquals(10, ex.doPart2());
	}
}