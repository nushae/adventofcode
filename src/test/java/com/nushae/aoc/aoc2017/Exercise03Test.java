package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@ParameterizedTest
	@MethodSource("provideArgumentsForPart1")
	public void testPart1(int input, int expectedValue) {
		Exercise03 ex = new Exercise03();
		assertEquals(expectedValue, ex.doPart1(input));
	}
	private static Stream<Arguments> provideArgumentsForPart1() {
		return Stream.of(
				Arguments.of(1, 0),
				Arguments.of(9, 2),
				Arguments.of(12, 3),
				Arguments.of(23, 2),
				Arguments.of(25, 4),
				Arguments.of(1024, 31)
		);
	}

	@Test
	public void testPart2() {
		Exercise03 ex = new Exercise03();
		assertEquals(5, ex.doPart2(4));
	}
}