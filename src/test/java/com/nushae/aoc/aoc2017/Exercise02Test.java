package com.nushae.aoc.aoc2017;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise02Test {

	@Test
	public void testPart1() {
		Exercise02 ex = new Exercise02("5 1 9 5\n" +
				"7 5 3\n" +
				"2 4 6 8");
		assertEquals(18, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise02 ex = new Exercise02("5 9 2 8\n" +
				"9 4 7 3\n" +
				"3 8 6 5");
		assertEquals(9, ex.doPart2());
	}
}