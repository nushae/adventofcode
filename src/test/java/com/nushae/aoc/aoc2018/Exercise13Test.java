package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise13Test {

	@Test
	public void testExample1() {
		Exercise13 ex = new Exercise13("/->-\\        \n" +
				"|   |  /----\\\n" +
				"| /-+--+-\\  |\n" +
				"| | |  | v  |\n" +
				"\\-+-/  \\-+--/\n" +
				"  \\------/   ");
		assertEquals(new Point(7, 3), ex.doPart1());
	}
}