package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@Test
	void testExample1() {
		Exercise18 ex = new Exercise18(".#.#...|#.\n" +
				".....#|##|\n" +
				".|..|...#.\n" +
				"..|#.....#\n" +
				"#.#|||#|#|\n" +
				"...#.||...\n" +
				".|....|...\n" +
				"||...#|.#|\n" +
				"|.||||..|.\n" +
				"...#.|..|.");
		assertEquals(1147, ex.doPart1());
	}
}