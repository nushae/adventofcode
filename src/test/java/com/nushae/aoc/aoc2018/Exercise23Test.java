package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise23Test {

	@Test
	void testExample1() {
		Exercise23 ex = new Exercise23("pos=<0,0,0>, r=4\n" +
				"pos=<1,0,0>, r=1\n" +
				"pos=<4,0,0>, r=3\n" +
				"pos=<0,2,0>, r=1\n" +
				"pos=<0,5,0>, r=3\n" +
				"pos=<0,0,3>, r=1\n" +
				"pos=<1,1,1>, r=1\n" +
				"pos=<1,1,2>, r=1\n" +
				"pos=<1,3,1>, r=1");
		assertEquals(7, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise23 ex = new Exercise23("pos=<10,12,12>, r=2\n" +
				"pos=<12,14,12>, r=2\n" +
				"pos=<16,12,12>, r=4\n" +
				"pos=<14,14,14>, r=6\n" +
				"pos=<50,50,50>, r=200\n" +
				"pos=<10,10,10>, r=5");
		assertEquals(36, ex.doPart2());
	}
}