package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise19Test {

	@Test
	void testExample1() {
		Exercise19 ex = new Exercise19("#ip 0\n" +
				"seti 5 0 1\n" +
				"seti 6 0 2\n" +
				"addi 0 1 0\n" +
				"addr 1 2 3\n" +
				"setr 1 0 0\n" +
				"seti 8 0 4\n" +
				"seti 9 0 5");
		assertEquals(6, ex.doPart1());
	}
}