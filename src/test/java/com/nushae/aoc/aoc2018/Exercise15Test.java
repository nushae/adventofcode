package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testExample1_1() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#.G...#\n" +
				"#...EG#\n" +
				"#.#.#G#\n" +
				"#..G#E#\n" +
				"#.....#\n" +
				"#######");
		assertEquals(27730, ex.doPart1()); // 47 * 590
	}

	@Test
	void testExample1_2() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#.G...#\n" +
				"#...EG#\n" +
				"#.#.#G#\n" +
				"#..G#E#\n" +
				"#.....#\n" +
				"#######");
		assertEquals(4988, ex.doPart2()); // 29 * 172
	}

	@Test
	void testExample2_1() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#G..#E#\n" +
				"#E#E.E#\n" +
				"#G.##.#\n" +
				"#...#E#\n" +
				"#...E.#\n" +
				"#######");
		assertEquals(36334, ex.doPart1()); // 37 * 982
	}

	@Test
	void testExample2_2() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#G..#E#\n" +
				"#E#E.E#\n" +
				"#G.##.#\n" +
				"#...#E#\n" +
				"#...E.#\n" +
				"#######");
		assertEquals(18717, ex.doPart2()); // 17 * 1101
	}

	@Test
	void testExample3_1() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#E..EG#\n" +
				"#.#G.E#\n" +
				"#E.##E#\n" +
				"#G..#.#\n" +
				"#..E#.#\n" +
				"#######");
		assertEquals(39514, ex.doPart1()); // 46 * 859
	}

	@Test
	void testExample3_2() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#E..EG#\n" +
				"#.#G.E#\n" +
				"#E.##E#\n" +
				"#G..#.#\n" +
				"#..E#.#\n" +
				"#######");
		assertEquals(20007, ex.doPart2()); // 19 * 1053
	}

	@Test
	void testExample4_1() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#E.G#.#\n" +
				"#.#G..#\n" +
				"#G.#.G#\n" +
				"#G..#.#\n" +
				"#...E.#\n" +
				"#######");
		assertEquals(27755, ex.doPart1()); // 35 * 793
	}

	@Test
	void testExample4_2() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#E.G#.#\n" +
				"#.#G..#\n" +
				"#G.#.G#\n" +
				"#G..#.#\n" +
				"#...E.#\n" +
				"#######");
		assertEquals(3478, ex.doPart2()); // 37 * 94
	}

	@Test
	void testExample5_1() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#.E...#\n" +
				"#.#..G#\n" +
				"#.###.#\n" +
				"#E#G#G#\n" +
				"#...#G#\n" +
				"#######");
		assertEquals(28944, ex.doPart1()); // 54 * 536
	}

	@Test
	void testExample5_2() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#.E...#\n" +
				"#.#..G#\n" +
				"#.###.#\n" +
				"#E#G#G#\n" +
				"#...#G#\n" +
				"#######");
		assertEquals(6474, ex.doPart2()); // 39 * 166
	}

	@Test
	void testExample6_1() {
		Exercise15 ex = new Exercise15("#########\n" +
				"#G......#\n" +
				"#.E.#...#\n" +
				"#..##..G#\n" +
				"#...##..#\n" +
				"#...#...#\n" +
				"#.G...G.#\n" +
				"#.....G.#\n" +
				"#########");
		assertEquals(18740, ex.doPart1()); // 20 * 937
	}

	@Test
	void testExample6_2() {
		Exercise15 ex = new Exercise15("#########\n" +
				"#G......#\n" +
				"#.E.#...#\n" +
				"#..##..G#\n" +
				"#...##..#\n" +
				"#...#...#\n" +
				"#.G...G.#\n" +
				"#.....G.#\n" +
				"#########");
		assertEquals(1140, ex.doPart2()); // 30 * 38
	}
}