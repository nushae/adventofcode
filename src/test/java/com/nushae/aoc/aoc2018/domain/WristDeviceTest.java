package com.nushae.aoc.aoc2018.domain;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WristDeviceTest {

	@Test
	void testHumanReadable() { // the example for day 19
		List<String> original = Arrays.asList(
				"#ip 0",
				"seti 5 0 1",
				"seti 6 0 2",
				"muli 5 7 3",
				"addi 0 1 0",
				"addr 1 2 3",
				"setr 1 0 0",
				"seti 8 0 4",
				"seti 9 0 5");
		WristDevice wd = new WristDevice();
		wd.loadProgramFromData(original);
		List<String> result = wd.programToHumanReadable();
		assertEquals(original.size(), result.size());
		showComparison(original, result);
	}

	@Test
	void testHumanReadable2() { // the code for day 19
		List<String> original = Arrays.asList(
				"#ip 2",
				"addi 2 16 2", "seti 1 1 1", "seti 1 4 3", "mulr 1 3 5", "eqrr 5 4 5",
				"addr 5 2 2", "addi 2 1 2", "addr 1 0 0", "addi 3 1 3", "gtrr 3 4 5",
				"addr 2 5 2", "seti 2 4 2", "addi 1 1 1", "gtrr 1 4 5", "addr 5 2 2",
				"seti 1 0 2", "mulr 2 2 2", "addi 4 2 4", "mulr 4 4 4", "mulr 2 4 4",
				"muli 4 11 4", "addi 5 1 5", "mulr 5 2 5", "addi 5 17 5", "addr 4 5 4",
				"addr 2 0 2", "seti 0 9 2", "setr 2 3 5", "mulr 5 2 5", "addr 2 5 5",
				"mulr 2 5 5", "muli 5 14 5", "mulr 5 2 5", "addr 4 5 4", "seti 0 9 0",
				"seti 0 6 2");
		WristDevice wd = new WristDevice();
		wd.loadProgramFromData(original);
		List<String> result = wd.programToHumanReadable();
		assertEquals(original.size(), result.size());
		showComparison(original, result);
	}

	@Test
	void testHumanReadable3() { // the code for day 21
		List<String> original = Arrays.asList(
				"#ip 5",
				"seti 123 0 2",
				"bani 2 456 2",
				"eqri 2 72 2",
				"addr 2 5 5",
				"seti 0 0 5",
				"seti 0 5 2",
				"bori 2 65536 4",
				"seti 6718165 9 2",
				"bani 4 255 3",
				"addr 2 3 2",
				"bani 2 16777215 2",
				"muli 2 65899 2",
				"bani 2 16777215 2",
				"gtir 256 4 3",
				"addr 3 5 5",
				"addi 5 1 5",
				"seti 27 8 5",
				"seti 0 4 3",
				"addi 3 1 1",
				"muli 1 256 1",
				"gtrr 1 4 1",
				"addr 1 5 5",
				"addi 5 1 5",
				"seti 25 8 5",
				"addi 3 1 3",
				"seti 17 3 5",
				"setr 3 6 4",
				"seti 7 9 5",
				"eqrr 2 0 3",
				"addr 3 5 5",
				"seti 5 1 5");
		WristDevice wd = new WristDevice();
		wd.loadProgramFromData(original);
		List<String> result = wd.programToHumanReadable();
		assertEquals(original.size(), result.size());
		showComparison(original, result);
	}

	private void showComparison(List<String> left, List<String> right) {
		for (int i = 0; i < Math.min(left.size(), right.size()); i++) {
			String leftLine = left.get(i);
			String rightLine = right.get(i);
			System.out.println(leftLine + StringUtils.repeat(' ', 20 - leftLine.length()) + rightLine);
		}
	}

}