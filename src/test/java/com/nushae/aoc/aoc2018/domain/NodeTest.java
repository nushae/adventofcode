package com.nushae.aoc.aoc2018.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NodeTest {

	@Test
	public void testMetadata() {
		Node n = new Node();
		n.addMeta(1);
		n.addMeta(2);
		n.addMeta(0);
		n.addMeta(4);
		n.addMeta(1);
		assertEquals(8, n.metaSum());
		Node m = new Node();
		m.addMeta(0);
		m.addMeta(7);
		n.addChild(m);
		assertEquals(7, m.metaSum());
		assertEquals(15, n.metaSum());
	}

	@Test
	public void testValue() {
		Node n = new Node();
		n.addMeta(1);
		n.addMeta(2);
		n.addMeta(0);
		n.addMeta(4);
		n.addMeta(1);
		assertEquals(8, n.value());
		Node m = new Node();
		m.addMeta(0);
		m.addMeta(7);
		n.addChild(m);
		assertEquals(7, m.value());
		assertEquals(14, n.value());
	}
}