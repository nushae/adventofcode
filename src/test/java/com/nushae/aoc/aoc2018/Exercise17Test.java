package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	void testSimpleLayer() {
		Exercise17 ex = new Exercise17("x=491, y=8..22\n" +
				"x=508, y=9..22\n" +
				"y=22, x=491..508");
		Point result = ex.waterCount();
		assertEquals(240, result.x);
		assertEquals(208, result.y);
	}

	@Test
	void testNestedLayerBarrier() {
		Exercise17 ex = new Exercise17("x=483, y=33..45\n" +
				"x=501, y=34..45\n" +
				"y=45, x=483..501\n" +
				"x=488, y=31..42\n" +
				"x=491, y=31..42\n" +
				"y=42, x=488..491");
		Point result = ex.waterCount();
		assertEquals(140, result.x);
		assertEquals(115, result.y);
	}

	@Test
	void testNestedLayerSpillover() {
		Exercise17 ex = new Exercise17("x=497, y=124..127\n" +
				"x=499, y=124..127\n" +
				"y=127, x=497..499\n" +
				"x=486, y=122..131\n" +
				"x=511, y=121..131\n" +
				"y=131, x=486..511");
		Point result = ex.waterCount();
		assertEquals(243, result.x);
		assertEquals(207, result.y);
	}
}