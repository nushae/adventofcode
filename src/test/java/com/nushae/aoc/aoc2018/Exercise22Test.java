package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.CaveRegion;
import com.nushae.aoc.domain.SearchNode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static com.nushae.aoc.aoc2018.Exercise22.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Exercise22Test {

	@Test
	void testExample1() {
		Exercise22 ex = new Exercise22();
		assertEquals(114, ex.doPart1(510, new Point(10, 10), new Point(10, 10)));
	}

	@ParameterizedTest
	@MethodSource("argumentsForAdjacentNodes")
	void testCaveRegionAdjacentNodes(int x, int y, int tx, int ty, int tool, Set<String> expected) {
		Exercise22 ex = new Exercise22();
		ex.doPart1(510, new Point(10, 10), new Point(15, 15));
		CaveRegion cr = new CaveRegion(x, y, tx, ty, tool, ex.types);
		Map<SearchNode, Long> result = cr.getAdjacentNodes();
		assertEquals(expected.size(), result.size());
		for (Map.Entry<SearchNode, Long> sn : result.entrySet()) {
			assertTrue(expected.contains(sn.getKey().getState() + " " + sn.getValue()), "Not in expected set: '" + sn.getKey().getState() + " " + sn.getValue() + "'");
		}
	}
	private static Stream<Arguments> argumentsForAdjacentNodes() {
		return Stream.of(
				// the entire path of example2
				Arguments.of(0, 0, 10, 10, TOOL_TORCH, new HashSet<>(Arrays.asList("0,1/1 1", "1,0/2 8"))),
				Arguments.of(0, 1, 10, 10, TOOL_TORCH, new HashSet<>(Arrays.asList("0,0/1 1", "0,2/1 1", "1,1/1 1"))),
				Arguments.of(1, 1, 10, 10, TOOL_TORCH, new HashSet<>(Arrays.asList("0,1/1 1", "1,0/0 8", "2,1/0 8", "1,2/0 8"))),
				Arguments.of(2, 1, 10, 10, TOOL_NEITHER, new HashSet<>(Arrays.asList("1,1/0 1", "2,0/2 8", "3,1/0 1", "2,2/0 1"))),
				Arguments.of(3, 1, 10, 10, TOOL_NEITHER, new HashSet<>(Arrays.asList("2,1/0 1", "3,0/0 1", "4,1/0 1", "3,2/0 1"))),
				Arguments.of(4, 1, 10, 10, TOOL_NEITHER, new HashSet<>(Arrays.asList("3,1/0 1", "4,0/0 1", "5,1/0 1", "4,2/2 8"))),
				Arguments.of(4, 2, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,2/1 8", "4,1/2 1", "4,3/2 1", "5,2/2 1"))),
				Arguments.of(4, 3, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,3/2 1", "4,2/2 1", "4,4/2 1", "5,3/2 1"))),
				Arguments.of(4, 4, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,4/2 1", "4,3/2 1", "4,5/2 1", "5,4/2 1"))),
				Arguments.of(4, 5, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,5/2 1", "4,4/2 1", "4,6/2 1", "5,5/2 1"))),
				Arguments.of(4, 6, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,6/2 1", "4,5/2 1", "4,7/2 1", "5,6/2 1"))),
				Arguments.of(4, 7, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,7/2 1", "4,6/2 1", "4,8/2 1", "5,7/0 8"))),
				Arguments.of(4, 8, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("3,8/2 1", "4,7/2 1", "4,9/2 1", "5,8/2 1"))),
				Arguments.of(5, 8, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("4,8/2 1", "5,7/0 8", "5,9/2 1", "6,8/2 1"))),
				Arguments.of(5, 9, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("4,9/2 1", "5,8/2 1", "5,10/2 1", "6,9/2 1"))),
				Arguments.of(5, 10, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("4,10/0 8", "5,9/2 1", "5,11/2 1", "6,10/0 8"))),
				Arguments.of(5, 11, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("4,11/2 1", "5,10/2 1", "5,12/2 1", "6,11/2 1"))),
				Arguments.of(6, 11, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("5,11/2 1", "6,10/1 8", "6,12/2 1", "7,11/1 8"))),
				Arguments.of(6, 12, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("5,12/2 1", "6,11/2 1", "6,13/2 1", "7,12/2 1"))),
				Arguments.of(7, 12, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("6,12/2 1", "7,11/1 8", "7,13/2 1", "8,12/2 1"))),
				Arguments.of(8, 12, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("7,12/2 1", "8,11/2 1", "8,13/1 8", "9,12/2 1"))),
				Arguments.of(9, 12, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("8,12/2 1", "9,11/2 1", "9,13/2 1", "10,12/2 1"))),
				Arguments.of(10, 12, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("9,12/2 1", "10,11/2 1", "10,13/2 1", "11,12/1 8"))),
				Arguments.of(10, 11, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("9,11/2 1", "10,10/1 8", "10,12/2 1", "11,11/2 1"))),

				// random points
				Arguments.of(7, 5, 10, 10, TOOL_NEITHER, new HashSet<>(Arrays.asList("7,6/0 1", "7,4/1 8", "6,5/0 1", "8,5/0 1"))),
				// surrounding the (10,10) target:
				Arguments.of(9, 10, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("8,10/2 1", "10,10/1 8", "9,9/0 8", "9,11/2 1"))),
				Arguments.of(9, 10, 10, 10, TOOL_TORCH, new HashSet<>(Arrays.asList("8,10/1 1", "10,10/1 15", "9,9/0 8", "9,11/1 1"))),
				Arguments.of(10, 9, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("9,9/0 8", "11,9/0 8", "10,8/0 8", "10,10/1 8"))),
				Arguments.of(10, 9, 10, 10, TOOL_NEITHER, new HashSet<>(Arrays.asList("9,9/0 1", "11,9/0 1", "10,8/0 1", "10,10/1 15"))),
				Arguments.of(11, 10, 10, 10, TOOL_NEITHER, new HashSet<>(Arrays.asList("11,9/0 1", "11,11/2 8", "10,10/1 15", "12,10/0 1"))),
				Arguments.of(11, 10, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("11,9/0 8", "11,11/2 1", "10,10/1 8", "12,10/2 1"))),
				Arguments.of(10, 11, 10, 10, TOOL_CLIMBING, new HashSet<>(Arrays.asList("9,11/2 1", "10,10/1 8", "10,12/2 1", "11,11/2 1"))),
				Arguments.of(10, 11, 10, 10, TOOL_TORCH, new HashSet<>(Arrays.asList("9,11/2 8", "10,10/1 1", "10,12/1 1", "11,11/1 1")))
		);
	}

	@Test
	void testExample2() {
		Exercise22 ex = new Exercise22();
		assertEquals(45, ex.doPart2(510, new Point(0, 0), TOOL_TORCH, new Point(10, 10), new Point(30, 30)));
	}

	@Test
	void testExample2_1() {
		Exercise22 ex = new Exercise22();
		assertEquals(26, ex.doPart2(510, new Point(4,1), TOOL_CLIMBING, new Point(10, 10), new Point(30, 30)));
		assertEquals(34, ex.doPart2(510, new Point(3,1), TOOL_NEITHER, new Point(10, 10), new Point(30, 30)));
		assertEquals(36, ex.doPart2(510, new Point(1,1), TOOL_NEITHER, new Point(10, 10), new Point(30, 30)));
	}

}