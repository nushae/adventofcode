package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@Test
	public void testExample1() {
		Exercise03 ex = new Exercise03("#1 @ 1,3: 4x4\n" +
				"#2 @ 3,1: 4x4\n" +
				"#3 @ 5,5: 2x2");
		assertEquals(4, ex.doPart1());
	}

	@Test
	public void testExample2() {
		Exercise03 ex = new Exercise03("#1 @ 1,3: 4x4\n" +
				"#2 @ 3,1: 4x4\n" +
				"#3 @ 4,4: 2x2");
		assertEquals(6, ex.doPart1());
	}
}