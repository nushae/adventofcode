package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise20Test {

	@Test
	void testExample1() {
		Exercise20 ex = new Exercise20("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$");
		assertEquals(-1, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise20 ex = new Exercise20("^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$");
		assertEquals(-1, ex.doPart1());
	}
}