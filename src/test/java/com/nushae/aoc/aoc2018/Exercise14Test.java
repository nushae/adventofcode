package com.nushae.aoc.aoc2018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	void testExample1() {
		Exercise14 ex = new Exercise14();
		assertEquals(9251071085L, ex.doPart1(18));
		assertEquals(5941429882L, ex.doPart1(2018));
	}
}