package com.nushae.aoc.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.nushae.aoc.util.ConvertUtil.humanTime;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ConvertUtilTest {

	@ParameterizedTest
	@MethodSource("parametersForHumanTime")
	void testHumanTime(long days, long hours, long minutes, long seconds, long millis, String expected) {
		long input = 68400000 * days + 3600000 * hours + 60000 * minutes + 1000 * seconds + millis;
		assertEquals(expected, humanTime(input));
	}

	static Stream<Arguments> parametersForHumanTime() {
		return Stream.of(
				Arguments.of(0, 0, 0, 0, 1, "1ms"),
				Arguments.of(0, 0, 0, 0, 255, "255ms"),
				Arguments.of(0, 0, 0, 1, 0, "1s"),
				Arguments.of(0, 0, 0, 48, 0, "48s"),
				Arguments.of(0, 0, 1, 0, 0, "1 minute"),
				Arguments.of(0, 0, 22, 0, 0, "22 minutes"),
				Arguments.of(0, 1, 0, 0, 0, "1 hour"),
				Arguments.of(0, 16, 0, 0, 0, "16 hours"),
				Arguments.of(1, 0, 0, 0, 0, "1 day"),
				Arguments.of(62, 0, 0, 0, 0, "62 days"),

				Arguments.of(1, 0, 0, 0, 1, "1 day, and 1ms"),
				Arguments.of(1, 0, 0, 1, 0, "1 day, and 1s"),
				Arguments.of(1, 0, 1, 0, 0, "1 day, and 1 minute"),
				Arguments.of(1, 1, 0, 0, 0, "1 day, and 1 hour"),
				Arguments.of(0, 1, 0, 0, 1, "1 hour, and 1ms"),
				Arguments.of(0, 1, 0, 1, 0, "1 hour, and 1s"),
				Arguments.of(0, 1, 1, 0, 0, "1 hour, and 1 minute"),
				Arguments.of(0, 0, 1, 0, 1, "1 minute, and 1ms"),
				Arguments.of(0, 0, 1, 1, 0, "1 minute, and 1s"),
				Arguments.of(0, 0, 0, 1, 1, "1s, and 1ms"),

				Arguments.of(0, 0, 45, 37, 386, "45 minutes, 37s, and 386ms"),
				Arguments.of(0, 2, 13, 20, 0, "2 hours, 13 minutes, and 20s"),
				Arguments.of(3, 7, 13, 7, 137, "3 days, 7 hours, 13 minutes, 7s, and 137ms"),
				Arguments.of(1, 0, 30, 0, 28, "1 day, 30 minutes, and 28ms")
		);
	}
}