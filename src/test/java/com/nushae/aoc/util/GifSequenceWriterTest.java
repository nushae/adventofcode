package com.nushae.aoc.util;

import org.junit.jupiter.api.Test;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

import static com.nushae.aoc.util.GraphicsUtil.DEFAULT_FONT;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;

class GifSequenceWriterTest {


	@Test
	void testFilestoFile() throws Exception {

		ImageOutputStream output = new FileImageOutputStream(new File("D:\\scratch\\output.gif"));
		GifSequenceWriter writer = new GifSequenceWriter(output, BufferedImage.TYPE_INT_RGB, 25, true);

		for(int i = 1; i < 11; i++) {
			BufferedImage img = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
			Graphics g = img.getGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, 100, 100);
			g.setColor(Color.black);
			drawCenteredString((Graphics2D) g, ""+i, new Rectangle(0, 0, 100, 100), DEFAULT_FONT);
			if (i == 5) {
				writer.writeToSequence(img, 2000);
			} else {
				writer.writeToSequence(img);
			}
		}

		writer.close();
		output.close();
	}

}