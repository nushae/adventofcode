package com.nushae.aoc.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import static com.nushae.aoc.util.StringUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StringUtilTest {

	@Test
	void testStringIntersection() {
		assertEquals("", intersection("", "bacefd"));
		assertEquals("", intersection("abcdef", ""));
		assertEquals("", intersection("cab", "xzy"));
		assertEquals("abc", intersection("afbac", "bczba"));

		String a = RandomStringUtils.random(13);
		String b = RandomStringUtils.random(17);
		String inter = intersection(a, b);
		assertEquals(inter, intersection(b, a), "Fail: intersect(\"" + a + "\", \"" + b + "\"");
	}

	@Test
	void testStringUnion() {
		assertEquals("abcdef", union("", "bacefd"));
		assertEquals("abcdef", union("bacefd", ""));
		assertEquals("abcxyz", union("cab", "xzy"));
		assertEquals("abcfz", union("afbac", "bczba"));
		assertEquals("abcfz", union("bczba", "afbac"));

		String a = RandomStringUtils.random(13);
		String b = RandomStringUtils.random(17);
		String uni = union(a, b);
		assertEquals(uni, union(b, a), "Fail: union(\"" + a + "\", \"" + b + "\"");
	}

	@Test
	void testSwapPos() {
		assertEquals("abcdefgh", swapPos("abcdefgh", -1, 5));
		assertEquals("abcdefgh", swapPos("abcdefgh", 2, 9));
		assertEquals("abcdefgh", swapPos("abcdefgh", 3, 3));
		assertEquals("abfdecgh", swapPos("abcdefgh", 5, 2));
		assertEquals("abfdecgh", swapPos("abcdefgh", 2, 5));
	}

	@Test
	void testTranspose() {
		String[] input = {"ABCDE", "FGHIJ", "KLMNO"};
		String[] output = transpose(input);
		assertEquals(5, output.length);
		assertEquals("AFK", output[0]);
		assertEquals("BGL", output[1]);
		assertEquals("CHM", output[2]);
		assertEquals("DIN", output[3]);
		assertEquals("EJO", output[4]);
	}

	@Test
	void testTransposeLastRowMissing() {
		String[] input = new String[4];
		input[0] = "ABCDE";
		input[1] = "FGHIJ";
		input[2] = "KLMNO";
		String[] output = transpose(input);
		assertEquals(5, output.length);
		assertEquals("AFK", output[0]);
		assertEquals("BGL", output[1]);
		assertEquals("CHM", output[2]);
		assertEquals("DIN", output[3]);
		assertEquals("EJO", output[4]);
	}

	@Test
	void testTransposeNotAllRowsEquallyLong() {
		String[] input = new String[4];
		input[0] = "ABCD";
		input[1] = "FGHIJ";
		input[2] = "KLMNO";
		input[3] = "E";
		String[] output = transpose(input);
		assertEquals(5, output.length);
		assertEquals("AFKE", output[0]);
		assertEquals("BGL ", output[1]);
		assertEquals("CHM ", output[2]);
		assertEquals("DIN ", output[3]);
		assertEquals(" JO ", output[4]);
	}
}