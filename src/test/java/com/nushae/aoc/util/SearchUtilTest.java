package com.nushae.aoc.util;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.nushae.aoc.util.SearchUtil.openEndedBinarySearch;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SearchUtilTest {

	@Test
	void testPriorityQueue() {
		Map<String, Integer> valueLookup = new HashMap<>();
		valueLookup.put("A", 1);
		valueLookup.put("B", 2);
		valueLookup.put("C", 3);
		Set<String> Q = new HashSet<>();
		Q.add("B");
		Q.add("A");
		assertEquals("A", Q.stream().min((a, b) -> (int) Math.signum(valueLookup.getOrDefault(a, Integer.MAX_VALUE) - valueLookup.getOrDefault(b, Integer.MAX_VALUE))).get());
		valueLookup.put("A", 10);
		assertEquals("B", Q.stream().min((a, b) -> (int) Math.signum(valueLookup.getOrDefault(a, Integer.MAX_VALUE) - valueLookup.getOrDefault(b, Integer.MAX_VALUE))).get());
	}

	@Test
	void testOpenEndedBinarySearch() {
		assertEquals(17, openEndedBinarySearch(0L, 289, x -> x*x));
		assertEquals(-1, openEndedBinarySearch(0L, 290, x -> x*x));
	}

}