package com.nushae.aoc.util;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.MathUtil.*;
import static java.math.BigInteger.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MathUtilTest {

	@Test
	void testManhattanDistance() {
		// ints
		assertEquals(5, manhattanDistance(2, 3));
		assertEquals(5, manhattanDistance(-2, 3));
		assertEquals(5, manhattanDistance(2, -3));
		assertEquals(5, manhattanDistance(-2, -3));
		assertEquals(3, manhattanDistance(1, 1, 2, 3));
		assertEquals(5, manhattanDistance(1, 1, -2, 3));
		assertEquals(5, manhattanDistance(1, 1, 2, -3));
		assertEquals(7, manhattanDistance(1, 1, -2, -3));
		// longs
		assertEquals(5, manhattanDistance(2L, 3L));
		assertEquals(5, manhattanDistance(-2L, 3L));
		assertEquals(5, manhattanDistance(2L, -3L));
		assertEquals(5, manhattanDistance(-2L, -3L));
		assertEquals(3, manhattanDistance(1L, 1L, 2L, 3L));
		assertEquals(5, manhattanDistance(1L, 1L, -2L, 3L));
		assertEquals(5, manhattanDistance(1L, 1L, 2L, -3L));
		assertEquals(7, manhattanDistance(1L, 1L, -2L, -3L));
		// Points
		assertEquals(5, manhattanDistance(new Point(2, 3)));
		assertEquals(5, manhattanDistance(new Point(-2, 3)));
		assertEquals(5, manhattanDistance(new Point(2, -3)));
		assertEquals(5, manhattanDistance(new Point(-2, -3)));
		assertEquals(3, manhattanDistance(new Point(1, 1), new Point(2, 3)));
		assertEquals(5, manhattanDistance(new Point(1, 1), new Point(-2, 3)));
		assertEquals(5, manhattanDistance(new Point(1, 1), new Point(2, -3)));
		assertEquals(7, manhattanDistance(new Point(1, 1), new Point(-2, -3)));
	}

	@Test
	void testManhattanRing() {
		Set<Point> result = manhattanRing(new Point(0,0), 0);
		assertEquals(1, result.size());
		assertTrue(result.contains(new Point(0,0)));

		result = manhattanRing(new Point(0,0), 3);
		assertEquals(12, result.size());
		assertTrue(result.contains(new Point(0, -3)));
		assertTrue(result.contains(new Point(1, -2)));
		assertTrue(result.contains(new Point(2, -1)));
		assertTrue(result.contains(new Point(3, 0)));
		assertTrue(result.contains(new Point(2, 1)));
		assertTrue(result.contains(new Point(1, 2)));
		assertTrue(result.contains(new Point(0, 3)));
		assertTrue(result.contains(new Point(-1, 2)));
		assertTrue(result.contains(new Point(-2, 1)));
		assertTrue(result.contains(new Point(-3, -0)));
		assertTrue(result.contains(new Point(-2, -1)));
		assertTrue(result.contains(new Point(-1, -2)));
	}

	@Test
	void testCrossProd() {
		assertEquals(11, crossProd(new Point(2, 3), new Point(1, 7)));
		assertEquals(-11, crossProd(new Point(1, 7), new Point(2, 3)));
		assertEquals(0, crossProd(new Point(4, 8), new Point(2, 4)));
		assertEquals(-97, crossProd(new Point(4, 9), new Point(9, -4)));
	}

	@Test
	void testDotProd() {
		assertEquals(23, dotProd(new Point(2, 3), new Point(1, 7)));
		assertEquals(23, dotProd(new Point(1, 7), new Point(2, 3)));
		assertEquals(40, dotProd(new Point(4, 8), new Point(2, 4)));
		assertEquals(0, dotProd(new Point(4, 9), new Point(9, -4)));
	}

	@Test
	void testMul() {
		assertEquals(new Point(3, 21), scale(new Point(1, 7), 3));
		assertEquals(new Point(10, 15), scale(new Point(2, 3), 5));
		assertEquals(new Point(2, 4), scale(new Point(2, 4), 1));
		assertEquals(new Point(-27, 12), scale(new Point(9, -4), -3));
	}

	@Test
	void testDiff() {
		assertEquals(new Point(6, -6), diff(new Point(7, 1), new Point(1, 7)));
		assertEquals(new Point(-6, 6), diff(new Point(1, 7), new Point(7, 1)));
		assertEquals(new Point(0, 0), diff(new Point(2, 4), new Point(2, 4)));
		assertEquals(new Point(-11, 5), diff(new Point(-2, 1), new Point(9, -4)));
	}

	@Test
	void testSum() {
		assertEquals(new Point(8, 8), sum(new Point(7, 1), new Point(1, 7)));
		assertEquals(new Point(8, 8), sum(new Point(1, 7), new Point(7, 1)));
		assertEquals(new Point(4, 8), sum(new Point(2, 4), new Point(2, 4)));
		assertEquals(new Point(7, -3), sum(new Point(-2, 1), new Point(9, -4)));
	}

	@Test
	void testRot90L() {
		assertEquals(new Point(7, -1), rot90L(1, new Point(1, 7)));
		assertEquals(new Point(-2, -3), rot90L(2, new Point(2, 3)));
		assertEquals(new Point(-4, 2), rot90L(3, new Point(2, 4)));
		assertEquals(new Point(9, -4), rot90L(4, new Point(9, -4)));
	}

	@Test
	void testRot90R() {
		assertEquals(new Point(-7, 1), rot90R(1, new Point(1, 7)));
		assertEquals(new Point(-2, -3), rot90R(2, new Point(2, 3)));
		assertEquals(new Point(4, -2), rot90R(3, new Point(2, 4)));
		assertEquals(new Point(9, -4), rot90R(4, new Point(9, -4)));
	}

	@Test
	void testRefYX() {
		assertEquals(new Point(7, 1), refYX(new Point(1, 7)));
		assertEquals(new Point(3, 2), refYX(new Point(2, 3)));
		assertEquals(new Point(4, 2), refYX(new Point(2, 4)));
		assertEquals(new Point(-4, 9), refYX(new Point(9, -4)));
	}

	@Test
	void testRefYnegX() {
		assertEquals(new Point(-7, -1), refYnegX(new Point(1, 7)));
		assertEquals(new Point(-3, -2), refYnegX(new Point(2, 3)));
		assertEquals(new Point(-4, -2), refYnegX(new Point(2, 4)));
		assertEquals(new Point(4, -9), refYnegX(new Point(9, -4)));
	}

	@Test
	void testIdentity() {
		assertEquals(1337, identity(1337)); // chosen with a perfectly random die
		assertEquals(371, identity(371)); // chosen with a perfectly random die
		assertEquals(42, identity(42)); // chosen with a perfectly random die
		assertEquals(-17, identity(-17)); // chosen with a perfectly random die
	}

	@Test
	void testSquare() {
		assertEquals(1,  square(1)); // chosen with a perfectly random die
		assertEquals(9, square(3)); // chosen with a perfectly random die
		assertEquals(144, square(-12)); // chosen with a perfectly random die
		assertEquals(0, square(0)); // chosen with a perfectly random die
	}

	@Test
	void testTriangle() {
		assertEquals(1, triangle(1)); // chosen with a perfectly random die
		assertEquals(3, triangle(2)); // chosen with a perfectly random die
		assertEquals(6, triangle(3)); // chosen with a perfectly random die
		assertEquals(10, triangle(4)); // chosen with a perfectly random die
	}

	@Test
	void testGcd() {
		// int
		assertEquals(0, gcd(0,0));
		assertEquals(3, gcd(0, 3));
		assertEquals(6, gcd(6, 0));
		assertEquals(gcd(3, 2), gcd(2,3));
		assertEquals(3, gcd(15, 6));
		assertEquals(3, gcd(-15, 6));
		assertEquals(1, gcd(7, 13));
		// long
		assertEquals(0, gcd(0L,0L));
		assertEquals(3, gcd(0L, 3L));
		assertEquals(6, gcd(6L, 0L));
		assertEquals(gcd(3L, 2L), gcd(2L,3L));
		assertEquals(3, gcd(15L, 6L));
		assertEquals(3, gcd(-15L, 6L));
		assertEquals(1, gcd(7L, 13L));
	}

	@Test
	void testLcm() {
		// int
		assertEquals(0, lcm(0, 0));
		assertEquals(0, lcm(0, 3));
		assertEquals(0, lcm(-7, 0));
		assertEquals(lcm(3, 2), lcm(2, 3));
		assertEquals(30, lcm(15, 6));
		assertEquals(30, lcm(-15, 6));
		assertEquals(91, lcm(7, 13));
		// long
		assertEquals(0, lcm(0L,0L));
		assertEquals(0, lcm(0L, 3L));
		assertEquals(0, lcm(-7L, 0L));
		assertEquals(lcm(3L, 2L), lcm(2L,3L));
		assertEquals(30, lcm(15L, 6L));
		assertEquals(30, lcm(-15L, 6L));
		assertEquals(91, lcm(7L, 13L));
		// BigInteger
		assertEquals(ZERO, lcm(ZERO, ZERO));
		assertEquals(ZERO, lcm(ZERO, BigInteger.valueOf(3)));
		assertEquals(ZERO, lcm(BigInteger.valueOf(-7), ZERO));
		assertEquals(lcm(BigInteger.valueOf(3), BigInteger.valueOf(2)), lcm(BigInteger.valueOf(2), BigInteger.valueOf(3)));
		assertEquals(BigInteger.valueOf(30), lcm(BigInteger.valueOf(15), BigInteger.valueOf(6)));
		assertEquals(BigInteger.valueOf(30), lcm(BigInteger.valueOf(-15), BigInteger.valueOf(6)));
		assertEquals(BigInteger.valueOf(91), lcm(BigInteger.valueOf(7), BigInteger.valueOf(13)));
	}

	@Test
	void testBezous() {
		long[] p = bezout_coeff(7, 9); // we're deliberately not using refYX() here for independence
		long[] q = bezout_coeff(9, 7);
		assertEquals(p[0], q[1]);
		assertEquals(p[1], q[0]);
		long[] result = bezout_coeff(3, 4); // 4 - 3 == 1
		assertEquals(-1, result[0]);
		assertEquals(1, result[1]);
		result = bezout_coeff(9, 7); // 4*7 - 3*9 == 1
		assertEquals(-3, result[0]);
		assertEquals(4, result[1]);
		result = bezout_coeff(78642,142892); // 71 * 142892 - 129 * 78642 == 514
		assertEquals(-129, result[0]);
		assertEquals(71, result[1]);
	}

	@Test
	void testCRT() {
		List<Long> n = Arrays.asList(3L, 4L, 5L);
		List<Long> a = Arrays.asList(0L, 3L, 4L);
		assertEquals(39, crt(n, a));
	}

	@Test
	void testSumOfDivisors() {
		assertEquals(12, sumOfDivisors(6)); // 1, 2, 3, 6
		assertEquals(28, sumOfDivisors(12)); // 1, 2, 3, 4, 6, 12
		assertEquals(24, sumOfDivisors(14)); // 1, 2, 7, 14
		assertEquals(63, sumOfDivisors(32)); // 1, 2, 4, 8, 16, 32
		assertEquals(98, sumOfDivisors(97)); // 1, 97
	}

	@Test
	void testToPositiveDegrees() {
		assertEquals(180, toPositiveDegrees(Math.PI));
		assertEquals(180, toPositiveDegrees(-Math.PI));
		assertEquals(10, toPositiveDegrees(Math.PI/18));
		assertEquals(350, toPositiveDegrees(-Math.PI/18));
	}

	@Test
	void testToGrayCode() {
		for (int i = 0; i < 1024; i++) {
			assertEquals(1, Integer.bitCount(toGrayCode(i)^ toGrayCode(i+1)));
		}
		assertEquals(0, toGrayCode(0));
		assertEquals(15, toGrayCode(10));
		assertEquals(86, toGrayCode(100));
		assertEquals(540, toGrayCode(1000));
	}

	@Test
	void testFromGrayCode() {
		for (int i = 0; i < 1024; i++) {
			assertEquals(i, fromGrayCode(toGrayCode(i)));
		}
		assertEquals(0, fromGrayCode(0));
		assertEquals(10, fromGrayCode(15));
		assertEquals(100, fromGrayCode(86));
		assertEquals(1000, fromGrayCode(540));
	}

}