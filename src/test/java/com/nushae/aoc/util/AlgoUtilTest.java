package com.nushae.aoc.util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.nushae.aoc.util.AlgoUtil.*;
import static org.junit.jupiter.api.Assertions.*;

class AlgoUtilTest {

	@Test
	void testLCSInteger() {
		List<Integer> left = Arrays.asList(1, 3, 1, 4, 2, 5);
		List<Integer> right = Arrays.asList(3, 2, 4, 1, 5);
		List<Integer> result = longestCommonSubsequence(left, right);
		assertNotNull(result);
		assertEquals(3, result.size());
		assertEquals(3, result.get(0));
		assertEquals(2, result.get(1));
		assertEquals(5, result.get(2));
	}

	@Test
	void testLCSString() {
		List<String> left = Arrays.asList("Noot", "Mies", "Wim", "Aap", "Aap", "Mies", "Wim");
		List<String> right = Arrays.asList("Aap", "Mies", "Wim", "Noot", "Aap", "Mies");
		List<String> result = longestCommonSubsequence(left, right);
		assertNotNull(result);
		assertEquals(4, result.size());
		assertEquals("Mies", result.get(0));
		assertEquals("Wim", result.get(1));
		assertEquals("Aap", result.get(2));
		assertEquals("Mies", result.get(3));
	}


	@Test
	void testSwap1() {
		List<String> orig = Arrays.asList("Aap", "Noot", "Mies", "Wim", "Zus", "Jet");
		List<String> result = swapElements(orig, 0, 5);
		assertEquals(orig.get(0), result.get(5));
		assertEquals(orig.get(1), result.get(1));
		assertEquals(orig.get(2), result.get(2));
		assertEquals(orig.get(3), result.get(3));
		assertEquals(orig.get(4), result.get(4));
		assertEquals(orig.get(5), result.get(0));
	}

	@Test
	void testSwap2() {
		List<String> orig = Arrays.asList("Aap", "Noot", "Mies", "Wim", "Zus", "Jet");
		List<String> result = swapElements(orig, 5, 1);
		assertEquals(orig.get(0), result.get(0));
		assertEquals(orig.get(1), result.get(5));
		assertEquals(orig.get(2), result.get(2));
		assertEquals(orig.get(3), result.get(3));
		assertEquals(orig.get(4), result.get(4));
		assertEquals(orig.get(5), result.get(1));
	}

	@Test
	void testMD5() {
		assertFalse(getMD5("abcdef" + 609042).startsWith("00000"));
		assertTrue(getMD5("abcdef" + 609043).startsWith("00000"));
		assertFalse(getMD5("pqrstuv" + 1048969).startsWith("00000"));
		assertTrue(getMD5("pqrstuv" + 1048970).startsWith("00000"));
	}

	@Test
	void testAllPermutationsArray() {
		assertEquals(120, allPermutations(1, 2, 3, 4, 5).mapToLong(e -> 1L).sum());
	}

	@Test
	void testAllPermutationsList() {
		assertEquals(120, allPermutations(List.of(1, 2, 3, 4, 5)).mapToLong(e -> 1L).sum());
	}

	@Test
	void testAllCombinations1() {
		assertEquals(10, allCombinations(3, new Integer[] {1, 2, 3, 4, 5}).mapToLong(e -> 1L).sum());
	}

	@Test
	void testAllCombinations2() {
		assertEquals(31, allCombinations(new Integer[] {1, 2, 3, 4, 5}).mapToLong(e -> 1L).sum());
	}
}