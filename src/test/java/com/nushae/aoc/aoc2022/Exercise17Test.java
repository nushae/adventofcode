package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	void testExample() {
		Exercise17 ex = new Exercise17(">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>");
		assertEquals(3068, ex.doPart1(2022));
		assertEquals(1514285714288L, ex.doPart2());
	}
}