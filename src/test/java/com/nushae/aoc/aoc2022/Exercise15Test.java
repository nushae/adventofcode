package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static com.nushae.aoc.util.IOUtil.aocPath;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testExample() {
		Exercise15 ex = new Exercise15("Sensor at x=2, y=18: closest beacon is at x=-2, y=15\n" +
				"Sensor at x=9, y=16: closest beacon is at x=10, y=16\n" +
				"Sensor at x=13, y=2: closest beacon is at x=15, y=3\n" +
				"Sensor at x=12, y=14: closest beacon is at x=10, y=16\n" +
				"Sensor at x=10, y=20: closest beacon is at x=10, y=16\n" +
				"Sensor at x=14, y=17: closest beacon is at x=10, y=16\n" +
				"Sensor at x=8, y=7: closest beacon is at x=2, y=10\n" +
				"Sensor at x=2, y=0: closest beacon is at x=2, y=10\n" +
				"Sensor at x=0, y=11: closest beacon is at x=2, y=10\n" +
				"Sensor at x=20, y=14: closest beacon is at x=25, y=17\n" +
				"Sensor at x=17, y=20: closest beacon is at x=21, y=22\n" +
				"Sensor at x=16, y=7: closest beacon is at x=15, y=3\n" +
				"Sensor at x=14, y=3: closest beacon is at x=15, y=3\n" +
				"Sensor at x=20, y=1: closest beacon is at x=15, y=3");
		ex.processWithPattern();
		assertEquals(26, ex.doPart1(10));
		assertEquals(56000011, ex.doPart2(20));
	}

	@Test
	void testFlipsInput() {
		Exercise15 ex = new Exercise15(aocPath(2022, 15, "input_flip.txt"));
		ex.processWithPattern();
		assertEquals(5832528, ex.doPart1(2000000));
		assertEquals(13360899249595L, ex.doPart2(4000000));
	}
}