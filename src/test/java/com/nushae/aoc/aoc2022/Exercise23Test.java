package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise23Test {

	@Test
	void testExample() {
		Exercise23 ex = new Exercise23("..............\n" +
				"..............\n" +
				".......#......\n" +
				".....###.#....\n" +
				"...#...#.#....\n" +
				"....#...##....\n" +
				"...#.###......\n" +
				"...##.#.##....\n" +
				"....#..#......\n" +
				"..............\n" +
				"..............\n" +
				"..............");
		assertEquals(110, ex.doPart1());
		assertEquals(20, ex.doPart2());
	}
}