package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

class Exercise20Test {

	@Test
	void t() {
		Exercise20 ex = new Exercise20("1\n" +
				"2\n" +
				"-3\n" +
				"3\n" +
				"-2\n" +
				"0\n" +
				"4");
		System.out.println(ex.doPart1(1, 1)); // 3
		System.out.println(ex.doPart1(10, 811589153)); // 1623178306
	}
}