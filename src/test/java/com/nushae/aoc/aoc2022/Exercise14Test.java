package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	void testExample() {
		Exercise14 ex = new Exercise14("498,4 -> 498,6 -> 496,6\n" +
				"503,4 -> 502,4 -> 502,9 -> 494,9");
		assertEquals(24, ex.simulateSand(true));
		assertEquals(93, ex.simulateSand(false));
	}
}