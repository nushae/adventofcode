package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import static com.nushae.aoc.aoc2022.Exercise22.INSTRUCTIONS;
import static com.nushae.aoc.util.IOUtil.aocPath;
import static com.nushae.aoc.util.MathUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	void testExample() {
		Exercise22 ex = new Exercise22("        ...#\n" +
				"        .#..\n" +
				"        #...\n" +
				"        ....\n" +
				"...#.......#\n" +
				"........#...\n" +
				"..#....#....\n" +
				"..........#.\n" +
				"        ...#....\n" +
				"        .....#..\n" +
				"        .#......\n" +
				"        ......#.\n" +
				"\n" +
				"10R5L5R10L4R5L5");
		assertEquals(6032, ex.runMap(true));
		// assertEquals(-1, ex.runMap(false)); yeah not doing that one
	}

	@Test
	void testWall() {
		Exercise22 ex = new Exercise22("...#\n" +
				"....\n" +
				"..#.\n" +
				"\n" +
				"10L5R");
		assertEquals(1012, ex.runMap(true));
	}

	@ParameterizedTest
	@MethodSource("provideInputForCubeFolds")
	void testCubeFolds(String filename, long expected) {
		Exercise22 ex = new Exercise22(aocPath(2022,22, filename));
		assertEquals(expected, ex.runMap(false));
	}

	private static Stream<Arguments> provideInputForCubeFolds() {
		return Stream.of(
				Arguments.of("4x4x4_a.txt", 1049),
				Arguments.of("4x4x4_b.txt", 12023),
				Arguments.of("4x4x4_c.txt", 12023),
				Arguments.of("4x4x4_d.txt", 12023),
				Arguments.of("4x4x4_e.txt", 12023),
				Arguments.of("4x4x4_f.txt", 9020),
				Arguments.of("4x4x4_g.txt", 12034),
				Arguments.of("4x4x4_h.txt", 12034),
				Arguments.of("4x4x4_i.txt", 12023),
				Arguments.of("4x4x4_j.txt", 12023),
				Arguments.of("4x4x4_k.txt", 12023)
		);
	}

	@ParameterizedTest
	@MethodSource("provideInstructionsForPathing")
	void testPathing(Point startPoint, Point startFacing, String instructions, Point endPoint, Point endFacing) {
		Exercise22 ex = new Exercise22(aocPath(2022,22, "input_pathing.txt"));
		ex.processInput(false); // cube connections
		assertEquals(6*50*50, ex.map.size());
		assertEquals(0, ex.instructions.size());
		assertEquals(new Point(50,0), ex.startLoc);
		assertEquals(EAST, ex.facing);
		ex.startLoc = startPoint;
		ex.facing = startFacing;
		Matcher m = INSTRUCTIONS.matcher(instructions);
		while (m.find()) {
			Point p;
			if (m.group(1).equals("L") || m.group(1).equals("R")) {
				p = new Point(0, m.group(1).charAt(0) == 'L' ? -1 : 1);
			} else {
				p = new Point(Integer.parseInt(m.group(1)), 0);
			}
			ex.instructions.add(p);
		}
		ex.current = ex.map.get(ex.startLoc);
		for (Point inst : ex.instructions) {
			ex.execute(inst);
		}
		assertEquals(endFacing, ex.facing);
		assertEquals(endPoint.x, ex.current.x);
		assertEquals(endPoint.y, ex.current.y);
	}

	private static Stream<Arguments> provideInstructionsForPathing() {
		return Stream.of(
				Arguments.of(new Point(0, 100), NORTH, "1L1L1L", new Point(0, 100), NORTH), // corner: 4 -> 3 -> 6 -> 4
				Arguments.of(new Point(50, 50), NORTH, "1L1L1L", new Point(50, 50), NORTH), // corner: 3 -> 6 -> 4 -> 3
				Arguments.of(new Point(50, 49), WEST, "1L1L1L", new Point(50, 49), WEST), // corner: 6 -> 4 -> 3 -> 6

				Arguments.of(new Point(0, 150), NORTH, "1L1L1L", new Point(0, 150), NORTH), // corner: 5 -> 4 -> 6 -> 5
				Arguments.of(new Point(50, 0), NORTH, "1L1L1L", new Point(50, 0), NORTH), // corner: 6 -> 5 -> 4 -> 6
				Arguments.of(new Point(0, 149), WEST, "1L1L1L", new Point(0, 149), WEST), // corner: 4 -> 6 -> 5 -> 4

				Arguments.of(new Point(50, 100), NORTH, "1L1L1L", new Point(50, 100), NORTH), // corner: 1 -> 3 -> 4 -> 1
				Arguments.of(new Point(50, 99), WEST, "1L1L1L", new Point(50, 99), WEST), // corner: 3 -> 4 -> 1 -> 3
				Arguments.of(new Point(49, 100), EAST, "1L1L1L", new Point(49, 100), EAST), // corner: 4 -> 1 -> 3 -> 4

				Arguments.of(new Point(100, 0), NORTH, "1L1L1L", new Point(100, 0), NORTH), // corner: 2 -> 5 -> 6 -> 2
				Arguments.of(new Point(0, 199), WEST, "1L1L1L", new Point(0, 199), WEST), // corner: 5 -> 6 -> 2 -> 5
				Arguments.of(new Point(99, 0), EAST, "1L1L1L", new Point(99, 0), EAST), // corner: 6 -> 2 -> 5 -> 6

				Arguments.of(new Point(50, 149), WEST, "1L1L1L", new Point(50, 149), WEST), // corner: 1 -> 4 -> 5 -> 1
				Arguments.of(new Point(49, 149), SOUTH, "1L1L1L", new Point(49, 149), SOUTH), // corner: 4 -> 5 -> 1 -> 4
				Arguments.of(new Point(49, 150), EAST, "1L1L1L", new Point(49, 150), EAST), // corner: 5 -> 1 -> 4 -> 5

				Arguments.of(new Point(100, 49), WEST, "1L1L1L", new Point(100, 49), WEST), // corner: 2 -> 6 -> 3 -> 2
				Arguments.of(new Point(99, 49), SOUTH, "1L1L1L", new Point(99, 49), SOUTH), // corner: 6 -> 3 -> 2 -> 2
				Arguments.of(new Point(99, 50), EAST, "1L1L1L", new Point(99, 50), EAST), // corner: 3 -> 2 -> 6 -> 3

				Arguments.of(new Point(149, 49), SOUTH, "1L1L1L", new Point(149, 49), SOUTH), // corner: 2 -> 3 -> 1 -> 2
				Arguments.of(new Point(99, 99), SOUTH, "1L1L1L", new Point(99, 99), SOUTH), // corner: 3 -> 1 -> 2 -> 3
				Arguments.of(new Point(99, 100), EAST, "1L1L1L", new Point(99, 100), EAST), // corner: 6 -> 3 -> 2 -> 3

				Arguments.of(new Point(149, 0), EAST, "1L1L1L", new Point(149, 0), EAST), // corner: 2 -> 1 -> 5 -> 2
				Arguments.of(new Point(99, 149), SOUTH, "1L1L1L", new Point(99, 149), SOUTH), // corner: 1 -> 5 -> 2 -> 1
				Arguments.of(new Point(49, 199), SOUTH, "1L1L1L", new Point(49, 199), SOUTH), // corner: 5 -> 2 -> 1 -> 5

				Arguments.of(new Point(60, 110), NORTH, "200", new Point(60, 110), NORTH), // straight: 1 -> 3 -> 6 -> 5 -> 1
				Arguments.of(new Point(60, 110), EAST, "200", new Point(60, 110), EAST), // straight: 1 -> 2 -> 6 -> 4 -> 1
				Arguments.of(new Point(60, 110), SOUTH, "200", new Point(60, 110), SOUTH), // straight: 1 -> 5 -> 6 -> 3 -> 1
				Arguments.of(new Point(60, 110), WEST, "200", new Point(60, 110), WEST), // straight: 1 -> 4 -> 6 -> 2 -> 1

				Arguments.of(new Point(110, 10), NORTH, "200", new Point(110, 10), NORTH), // straight: 2 -> 5 -> 4 -> 3 -> 2
				Arguments.of(new Point(110, 10), EAST, "200", new Point(110, 10), EAST), // straight: 2 -> 1 -> 4 -> 6 -> 2
				Arguments.of(new Point(110, 10), SOUTH, "200", new Point(110, 10), SOUTH), // straight: 2 -> 3 -> 4 -> 5 -> 2
				Arguments.of(new Point(110, 10), WEST, "200", new Point(110, 10), WEST), // straight: 2 -> 6 -> 4 -> 1 -> 2

				Arguments.of(new Point(60, 60), NORTH, "200", new Point(60, 60), NORTH), // straight: 3 -> 6 -> 5 -> 1 -> 3
				Arguments.of(new Point(60, 60), EAST, "200", new Point(60, 60), EAST), // straight: 3 -> 2 -> 5 -> 4 -> 3
				Arguments.of(new Point(60, 60), SOUTH, "200", new Point(60, 60), SOUTH), // straight: 3 -> 1 -> 5 -> 6 -> 3
				Arguments.of(new Point(60, 60), WEST, "200", new Point(60, 60), WEST), // straight: 3 -> 4 -> 5 -> 2 -> 3

				Arguments.of(new Point(10, 110), NORTH, "200", new Point(10, 110), NORTH), // straight: 4 -> 3 -> 2 -> 5 -> 4
				Arguments.of(new Point(10, 110), EAST, "200", new Point(10, 110), EAST), // straight: 4 -> 1 -> 2 -> 6 -> 4
				Arguments.of(new Point(10, 110), SOUTH, "200", new Point(10, 110), SOUTH), // straight: 4 -> 5 -> 2 -> 3 -> 4
				Arguments.of(new Point(10, 110), WEST, "200", new Point(10, 110), WEST), // straight: 4 -> 6 -> 2 -> 1 -> 4

				Arguments.of(new Point(10, 160), NORTH, "200", new Point(10, 160), NORTH), // straight: 5 -> 4 -> 3 -> 2 -> 5
				Arguments.of(new Point(10, 160), EAST, "200", new Point(10, 160), EAST), // straight: 5 -> 1 -> 3 -> 6 -> 5
				Arguments.of(new Point(10, 160), SOUTH, "200", new Point(10, 160), SOUTH), // straight: 5 -> 2 -> 3 -> 4 -> 5
				Arguments.of(new Point(10, 160), WEST, "200", new Point(10, 160), WEST), // straight: 5 -> 6 -> 3 -> 1 -> 5

				Arguments.of(new Point(60, 10), NORTH, "200", new Point(60, 10), NORTH), // straight: 6 -> 5 -> 1 -> 3 -> 6
				Arguments.of(new Point(60, 10), EAST, "200", new Point(60, 10), EAST), // straight: 6 -> 2 -> 1 -> 4 -> 6
				Arguments.of(new Point(60, 10), SOUTH, "200", new Point(60, 10), SOUTH), // straight: 6 -> 3 -> 1 -> 5 -> 6
				Arguments.of(new Point(60, 10), WEST, "200", new Point(60, 10), WEST) // straight: 6 -> 4 -> 1 -> 2 -> 6

		);
	}

	@Test
	void test3DCubeA() {
		Exercise22 ex = new Exercise22(aocPath(2022,22, "input_3d_cube_A.txt"));
		assertEquals(20029, ex.runMap(false));
	}

	@Test
	void test3DCubeH() {
		Exercise22 ex = new Exercise22(aocPath(2022,22, "input_3d_cube_H.txt"));
		assertEquals(11059, ex.runMap(false));
	}
}