package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise25Test {

	@Test
	void testExample() {
		Exercise25 ex = new Exercise25("1=-0-2\n" +
				"12111\n" +
				"2=0=\n" +
				"21\n" +
				"2=01\n" +
				"111\n" +
				"20012\n" +
				"112\n" +
				"1=-1=\n" +
				"1-12\n" +
				"12\n" +
				"1=\n" +
				"122");
		assertEquals("2=-1=0", ex.doPart1());
	}

	@Test
	void testFromSnafu() {
		Exercise25 ex = new Exercise25();
		assertEquals(1, ex.fromSnafu("1"));
		assertEquals(2, ex.fromSnafu("2"));
		assertEquals(3, ex.fromSnafu("1="));
		assertEquals(4, ex.fromSnafu("1-"));
		assertEquals(5, ex.fromSnafu("10"));
		assertEquals(6, ex.fromSnafu("11"));
		assertEquals(7, ex.fromSnafu("12"));
		assertEquals(8, ex.fromSnafu("2="));
		assertEquals(9, ex.fromSnafu("2-"));
		assertEquals(10, ex.fromSnafu("20"));
		assertEquals(15, ex.fromSnafu("1=0"));
		assertEquals(20, ex.fromSnafu("1-0"));
		assertEquals(2022, ex.fromSnafu("1=11-2"));
		assertEquals(12345, ex.fromSnafu("1-0---0"));
		assertEquals(314159265, ex.fromSnafu("1121-1110-1=0"));
	}

	@Test
	void testToSnafu() {
		Exercise25 ex = new Exercise25();
		assertEquals("1", ex.toSnafu(1));
		assertEquals("2", ex.toSnafu(2));
		assertEquals("1=", ex.toSnafu(3));
		assertEquals("1-", ex.toSnafu(4));
		assertEquals("10", ex.toSnafu(5));
		assertEquals("11", ex.toSnafu(6));
		assertEquals("12", ex.toSnafu(7));
		assertEquals("2=", ex.toSnafu(8));
		assertEquals("2-", ex.toSnafu(9));
		assertEquals("20", ex.toSnafu(10));
		assertEquals("1=0", ex.toSnafu(15));
		assertEquals("1-0", ex.toSnafu(20));
		assertEquals("1=11-2", ex.toSnafu(2022));
		assertEquals("1-0---0", ex.toSnafu(12345));
		assertEquals("1121-1110-1=0", ex.toSnafu(314159265));
	}
}