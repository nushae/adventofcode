package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise24Test {

	@Test
	void testExample() {
		Exercise24 ex = new Exercise24("#.######\n" +
				"#>>.<^<#\n" +
				"#.<..<<#\n" +
				"#>v.><>#\n" +
				"#<^v^^>#\n" +
				"######.#");
		assertEquals(18, ex.doPart1());
		assertEquals(54, ex.doPart2());
	}
}