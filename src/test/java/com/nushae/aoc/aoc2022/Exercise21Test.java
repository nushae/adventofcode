package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static com.nushae.aoc.util.IOUtil.aocPath;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise21Test {

	@Test
	void testExample() {
		Exercise21 ex = new Exercise21("root: pppw + sjmn\n" +
				"dbpl: 5\n" +
				"cczh: sllz + lgvd\n" +
				"zczc: 2\n" +
				"ptdq: humn - dvpt\n" +
				"dvpt: 3\n" +
				"lfqf: 4\n" +
				"humn: 5\n" +
				"ljgn: 2\n" +
				"sjmn: drzm * dbpl\n" +
				"sllz: 4\n" +
				"pppw: cczh / lfqf\n" +
				"lgvd: ljgn * ptdq\n" +
				"drzm: hmdt - zczc\n" +
				"hmdt: 32");
		assertEquals(152, ex.doPart1());
		assertEquals(301, ex.doPart2());
	}

	@Test
	void testDiederiksInputFile() {
		Exercise21 ex = new Exercise21(aocPath(2022, 21, "input_diederik.txt"));
		assertEquals(80326079210554L, ex.doPart1());
		assertEquals(3617613952378L, ex.doPart2());
	}
}