package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise05Test {

	@Test
	void testPart1() {
		Exercise05 ex = new Exercise05("    [D]    \n" +
				"[N] [C]    \n" +
				"[Z] [M] [P]\n" +
				" 1   2   3 \n" +
				"\n" +
				"move 1 from 2 to 1\n" +
				"move 3 from 1 to 3\n" +
				"move 2 from 2 to 1\n" +
				"move 1 from 1 to 2");
		assertEquals("CMZ", ex.calculate(true));
		assertEquals("MCD", ex.calculate(false));
	}
}