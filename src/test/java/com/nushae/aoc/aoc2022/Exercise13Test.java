package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise13Test {

	@Test
	void testCompareTo() {
		Exercise13.Packet p1 = new Exercise13.Packet("[[[2],[]],[1]]");
		Exercise13.Packet p2 = new Exercise13.Packet("[[2]]");
		assertEquals(1, p1.compareTo(p2));
	}

	@Test
	void testEquals() {
		Exercise13.Packet p1 = new Exercise13.Packet("[[[2]]]]");
		Exercise13.Packet p2 = new Exercise13.Packet("[[2]]");
		assertEquals(p1, p2);
	}

	@Test
	void testExample() {
		Exercise13 ex = new Exercise13("[1,1,3,1,1]\n" +
				"[1,1,5,1,1]\n" +
				"\n" +
				"[[1],[2,3,4]]\n" +
				"[[1],4]\n" +
				"\n" +
				"[9]\n" +
				"[[8,7,6]]\n" +
				"\n" +
				"[[4,4],4,4]\n" +
				"[[4,4],4,4,4]\n" +
				"\n" +
				"[7,7,7,7]\n" +
				"[7,7,7]\n" +
				"\n" +
				"[]\n" +
				"[3]\n" +
				"\n" +
				"[[[]]]\n" +
				"[[]]\n" +
				"\n" +
				"[1,[2,[3,[4,[5,6,7]]]],8,9]\n" +
				"[1,[2,[3,[4,[5,6,0]]]],8,9]");
		assertEquals(13, ex.doPart1());
		assertEquals(140, ex.doPart2());
	}
}