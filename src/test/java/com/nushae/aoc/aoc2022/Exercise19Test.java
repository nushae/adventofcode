package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise19Test {

	@Test
	void testExample() {
		Exercise19 ex = new Exercise19("Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\n" +
				"Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.");
		ex.processWithPattern();
		assertEquals(33, ex.doPart1());
		// throws coredump ?!
		// assertEquals(3472, ex.doPart2());
	}
}