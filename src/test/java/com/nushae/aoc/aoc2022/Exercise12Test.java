package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise12Test {

	@Test
	void testExample() {
		Exercise12 ex = new Exercise12("Sabqponm\n" +
				"abcryxxl\n" +
				"accszExk\n" +
				"acctuvwj\n" +
				"abdefghi");
		ex.processInput();
		assertEquals(31, ex.doPart1());
		assertEquals(29, ex.doPart2());
	}
}