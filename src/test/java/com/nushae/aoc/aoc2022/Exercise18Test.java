package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@Test
	void testExample() {
		Exercise18 ex = new Exercise18("2,2,2\n" +
				"1,2,2\n" +
				"3,2,2\n" +
				"2,1,2\n" +
				"2,3,2\n" +
				"2,2,1\n" +
				"2,2,3\n" +
				"2,2,4\n" +
				"2,2,6\n" +
				"1,2,5\n" +
				"3,2,5\n" +
				"2,1,5\n" +
				"2,3,5");
		assertEquals(64, ex.doPart1());
		assertEquals(58, ex.doPart2(64));
	}
}