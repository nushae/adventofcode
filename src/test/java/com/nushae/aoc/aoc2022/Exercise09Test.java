package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise09Test {
	@Test
	void testExample1() {
		Exercise09 ex = new Exercise09("R 4\n" +
				"U 4\n" +
				"L 3\n" +
				"D 1\n" +
				"R 4\n" +
				"D 1\n" +
				"L 5\n" +
				"R 2");
		assertEquals(13, ex.doPart1());
		assertEquals(1, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise09 ex = new Exercise09("R 5\n" +
				"U 8\n" +
				"L 8\n" +
				"D 3\n" +
				"R 17\n" +
				"D 10\n" +
				"L 25\n" +
				"U 20");
		assertEquals(36, ex.doPart2());
	}

}