package com.nushae.aoc.aoc2022;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise08Test {

	@Test
	void testExample1() {
		Exercise08 ex = new Exercise08("30373\n" +
				"25512\n" +
				"65332\n" +
				"33549\n" +
				"35390");
		ex.processInput();
		assertEquals(21, ex.doPart1());
		assertEquals(8, ex.doPart2());
	}
}