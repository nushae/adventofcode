package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise20Test {

	@Test
	public void testFactorization() {
		Exercise20 ex = new Exercise20(60);
		assertEquals(4, ex.doPart1());
	}
}