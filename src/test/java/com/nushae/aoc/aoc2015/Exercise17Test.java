package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	public void testPart1() {
		Exercise17 ex = new Exercise17(20, 15, 10, 5, 5);
		assertEquals(4, ex.doPart1(25));
	}

	@Test
	public void testPart2() {
		Exercise17 ex = new Exercise17(20, 15, 10, 5, 5);
		assertEquals(-1, ex.doPart2(25));
	}
}