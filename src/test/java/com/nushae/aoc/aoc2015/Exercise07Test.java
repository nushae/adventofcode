package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;

import static com.nushae.aoc.aoc2015.Exercise07.PATTERN_WIRE_AND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Exercise07Test {

	@Test
	public void patternTest() {
		assertTrue("lf AND lq -> ls".matches("([a-z]+|\\d+) AND ([a-z]+|\\d+) -> ([a-z]+)"));
		Matcher m = PATTERN_WIRE_AND.matcher("lf AND lq -> ls");
		assertTrue(m.find());
		assertEquals("lf", m.group(1));
		assertEquals("lq", m.group(2));
		assertEquals("ls", m.group(3));
	}

	@Test
	public void evalTest() {
		System.out.println((65535 << 2) % 65536);
	}
}