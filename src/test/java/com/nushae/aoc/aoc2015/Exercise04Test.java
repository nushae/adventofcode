package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise04Test {

	@Test
	void testExample1() {
		Exercise04 ex = new Exercise04("abcdef");
		assertEquals(609043, ex.doPart1());
		assertEquals(6742839, ex.doPart2());
	}
}