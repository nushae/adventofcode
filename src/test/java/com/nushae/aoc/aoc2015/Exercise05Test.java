package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Exercise05Test {

	@Test
	public void testHasDoubleLetter() {
		assertTrue(Exercise05.hasDoubleLetter().test("aabcdef"));
		assertFalse(Exercise05.hasDoubleLetter().test("abcadef"));
	}

	@Test
	public void testHasVowels() {
		assertTrue(Exercise05.hasVowels(3).test("abacdef"));
		assertFalse(Exercise05.hasVowels(3).test("abcdef"));
	}

	@Test
	public void testHasNoTabooWord() {
		assertTrue(Exercise05.hasNoTabooWord("ab", "cd", "pq", "xy").test("vbgnueibvrefd"));
		assertFalse(Exercise05.hasNoTabooWord("ab", "cd", "pq", "xy").test("vbgnabibvrefd"));
	}

	@Test
	public void testHasDoubleSkipOne() {
		assertTrue(Exercise05.hasDoubleSkipOne().test("wwwzzazzzz"));
		assertFalse(Exercise05.hasDoubleSkipOne().test("zzaadefaabc"));
	}

	@Test
	public void testHasDoublePair() {
		assertTrue(Exercise05.hasDoublePair().test("abcdefab"));
		assertFalse(Exercise05.hasDoublePair().test("abcdefacb"));
	}
}