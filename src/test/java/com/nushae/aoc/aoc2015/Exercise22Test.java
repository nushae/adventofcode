package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	public void testPart1() {
		Exercise22 ex = new Exercise22(20, 250, 13, 8);
		assertEquals(900, ex.doGame(false));
	}
}