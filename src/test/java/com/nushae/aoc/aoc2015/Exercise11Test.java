package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercise11Test {

	@Test
	public void testPart1() {
		assertEquals("abcdffaa", Exercise11.findNextPW("abcdefgh"));
		assertEquals("ghjaabcc", Exercise11.findNextPW("ghijklmn")); // LONG test
		assertEquals("abcdffbb", Exercise11.findNextPW("abcdffaa"));
		assertEquals("abcdffff", Exercise11.findNextPW("abcdffee")); // Should be abcdaabb imho
	}

	@Test
	public void testStraight3() {
		assertTrue(Exercise11.hasStraight3("hijklmmn"));
	}

	@Test
	public void testNoOil() {
		assertFalse(Exercise11.hasNoOil("hijklmmn"));
	}

	@Test
	public void testTwoDifferentPairs() {
		assertTrue(Exercise11.hasTwoDifferentPairs("abbceffg"));
		assertFalse(Exercise11.hasTwoDifferentPairs("abbcegjk"));
	}

	@Test
	public void testIncrement() {
		assertEquals("aab", Exercise11.lexicographicIncrement("aaa"));
		assertEquals("ga", Exercise11.lexicographicIncrement("fz"));
		assertEquals("aa", Exercise11.lexicographicIncrement("z"));
		assertEquals("aaaa", Exercise11.lexicographicIncrement("zzz"));
		assertEquals("aaaaaaaa", Exercise11.lexicographicIncrement("zzzzzzzz"));
	}
}