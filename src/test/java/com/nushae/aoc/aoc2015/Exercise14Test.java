package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	public void testPart1() {
		Exercise14 ex = new Exercise14("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\n" +
				"Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.");
		ex.processInput();
		assertEquals(1120, ex.doPart1(1000));
	}

	@Test
	public void testPart2() {
		Exercise14 ex = new Exercise14("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\n" +
				"Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.");
		ex.processInput();
		assertEquals(689, ex.doPart2(1000));
	}
}