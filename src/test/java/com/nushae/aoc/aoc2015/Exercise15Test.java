package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	public void testPart1() {
		Exercise15 ex = new Exercise15("Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\n" +
				"Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3");
		assertEquals(62842880, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise15 ex = new Exercise15("Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\n" +
				"Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3");
		assertEquals(57600000, ex.doPart2());
	}
}