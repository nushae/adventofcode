package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	public void testLookAndSay() {
		assertEquals("111213", Exercise10.lookAndSay("123"));
	}
}