package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@Test
	public void testPart1() {
		Exercise18 ex = new Exercise18(".#.#.#\n" +
				"...##.\n" +
				"#....#\n" +
				"..#...\n" +
				"#.#..#\n" +
				"####..");
		assertEquals(4, ex.doPart1(4, true));
	}

	@Test
	public void testPart2() {
		Exercise18 ex = new Exercise18(".#.#.#\n" +
				"...##.\n" +
				"#....#\n" +
				"..#...\n" +
				"#.#..#\n" +
				"####..");
		assertEquals(17, ex.doPart2(5, true));
	}
}