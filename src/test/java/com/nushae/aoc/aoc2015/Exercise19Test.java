package com.nushae.aoc.aoc2015;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise19Test {

	@Test
	public void testPart1() {
		Exercise19 ex = new Exercise19("H => HO\n" +
				"H => OH\n" +
				"O => HH\n" +
				"HOH");
		assertEquals(4, ex.doPart1());
		ex = new Exercise19("H => HO\n" +
				"H => OH\n" +
				"O => HH\n" +
				"HOHOHO");
		assertEquals(7, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise19 ex = new Exercise19("e => H\n" +
				"e => O\n" +
				"H => HO\n" +
				"H => OH\n" +
				"O => HH\n +" +
				"HOH");
		assertEquals(3, ex.doPart2());
		ex = new Exercise19("e => H\n" +
				"e => O\n" +
				"H => HO\n" +
				"H => OH\n" +
				"O => HH\n +" +
				"HOHOHO");
		assertEquals(6, ex.doPart2());
	}
}