package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise13Test {

	@Test
	public void testSmallFold() {
		Exercise13 ex = new Exercise13("0,0\n" +
				"3,2\n" +
				"\n" +
				"fold along y=1");
		assertEquals(2, ex.doPart1());
	}

	@Test
	public void testPart1ForExample() {
		Exercise13 ex = new Exercise13("6,10\n" +
				"0,14\n" +
				"9,10\n" +
				"0,3\n" +
				"10,4\n" +
				"4,11\n" +
				"6,0\n" +
				"6,12\n" +
				"4,1\n" +
				"0,13\n" +
				"10,12\n" +
				"3,4\n" +
				"3,0\n" +
				"8,4\n" +
				"1,10\n" +
				"2,14\n" +
				"8,10\n" +
				"9,0\n" +
				"\n" +
				"fold along y=7\n" +
				"fold along x=5");
		assertEquals(17, ex.doPart1());
	}

	@Test
	public void testPart2ForExample() {
		Exercise13 ex = new Exercise13("6,10\n" +
				"0,14\n" +
				"9,10\n" +
				"0,3\n" +
				"10,4\n" +
				"4,11\n" +
				"6,0\n" +
				"6,12\n" +
				"4,1\n" +
				"0,13\n" +
				"10,12\n" +
				"3,4\n" +
				"3,0\n" +
				"8,4\n" +
				"1,10\n" +
				"2,14\n" +
				"8,10\n" +
				"9,0\n" +
				"\n" +
				"fold along y=7\n" +
				"fold along x=5");
		assertEquals("█████\n" +
				"█   █\n" +
				"█   █\n" +
				"█   █\n" +
				"█████\n" +
				"     \n" +
				"     \n", ex.doPart2());
	}
}