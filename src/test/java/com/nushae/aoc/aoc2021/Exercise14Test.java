package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	public void testPart1(){
		Exercise14 ex = new Exercise14("NNCB\n" +
				"\n" +
				"CH -> B\n" +
				"HH -> N\n" +
				"CB -> H\n" +
				"NH -> C\n" +
				"HB -> C\n" +
				"HC -> B\n" +
				"HN -> C\n" +
				"NN -> C\n" +
				"BH -> H\n" +
				"NC -> B\n" +
				"NB -> B\n" +
				"BN -> B\n" +
				"BB -> N\n" +
				"BC -> B\n" +
				"CC -> N\n" +
				"CN -> C");
		assertEquals(BigInteger.valueOf(1588), ex.applyRules(10));
	}

	@Test
	public void testPart2(){
		Exercise14 ex = new Exercise14("NNCB\n" +
				"\n" +
				"CH -> B\n" +
				"HH -> N\n" +
				"CB -> H\n" +
				"NH -> C\n" +
				"HB -> C\n" +
				"HC -> B\n" +
				"HN -> C\n" +
				"NN -> C\n" +
				"BH -> H\n" +
				"NC -> B\n" +
				"NB -> B\n" +
				"BN -> B\n" +
				"BB -> N\n" +
				"BC -> B\n" +
				"CC -> N\n" +
				"CN -> C");
		assertEquals(BigInteger.valueOf(2188189693529L), ex.applyRules(40));
	}
}