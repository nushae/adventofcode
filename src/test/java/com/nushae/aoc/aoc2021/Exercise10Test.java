package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	public void testPart1() {
		Exercise10 ex = new Exercise10("[({(<(())[]>[[{[]{<()<>>\n" +
				"[(()[<>])]({[<{<<[]>>(\n" +
				"{([(<{}[<>[]}>{[]{[(<()>\n" +
				"(((({<>}<{<{<>}{[]{[]{}\n" +
				"[[<[([]))<([[{}[[()]]]\n" +
				"[{[{({}]{}}([{[{{{}}([]\n" +
				"{<[[]]>}<{[{[{[]{()[[[]\n" +
				"[<(<(<(<{}))><([]([]()\n" +
				"<{([([[(<>()){}]>(<<{{\n" +
				"<{([{{}}[<[[[<>{}]]]>[]]");
		assertEquals(26397, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise10 ex = new Exercise10("[({(<(())[]>[[{[]{<()<>>\n" +
				"[(()[<>])]({[<{<<[]>>(\n" +
				"{([(<{}[<>[]}>{[]{[(<()>\n" +
				"(((({<>}<{<{<>}{[]{[]{}\n" +
				"[[<[([]))<([[{}[[()]]]\n" +
				"[{[{({}]{}}([{[{{{}}([]\n" +
				"{<[[]]>}<{[{[{[]{()[[[]\n" +
				"[<(<(<(<{}))><([]([]()\n" +
				"<{([([[(<>()){}]>(<<{{\n" +
				"<{([{{}}[<[[[<>{}]]]>[]]");
		assertEquals(288957, ex.doPart2());
	}
}