package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise06Test {

	@Test
	public void testCalculation() {
		Exercise06 ex = new Exercise06("3,4,3,1,2");
		assertEquals(26, ex.doItSmart(18));
		assertEquals(5934, ex.doItSmart(80));
		assertEquals(26984457539L, ex.doItSmart(256));
	}
}