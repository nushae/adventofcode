package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise05Test {

	@Test
	public void testPart1() {
		Exercise05 ex = new Exercise05("0,9 -> 5,9\n" +
				"8,0 -> 0,8\n" +
				"9,4 -> 3,4\n" +
				"2,2 -> 2,1\n" +
				"7,0 -> 7,4\n" +
				"6,4 -> 2,0\n" +
				"0,9 -> 2,9\n" +
				"3,4 -> 1,4\n" +
				"0,0 -> 8,8\n" +
				"5,5 -> 8,2");
		assertEquals(5, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise05 ex = new Exercise05("0,9 -> 5,9\n" +
				"8,0 -> 0,8\n" +
				"9,4 -> 3,4\n" +
				"2,2 -> 2,1\n" +
				"7,0 -> 7,4\n" +
				"6,4 -> 2,0\n" +
				"0,9 -> 2,9\n" +
				"3,4 -> 1,4\n" +
				"0,0 -> 8,8\n" +
				"5,5 -> 8,2");
		assertEquals(12, ex.doPart2());
	}

	@Test
	public void testDiag() {
		Exercise05 ex = new Exercise05("0,0 -> 4,4\n" +
				"4,4 -> 0,0\n" +
				"4,0 -> 0,4\n" +
				"0,4 -> 4,0");
		assertEquals(9, ex.doPart2());
	}
}