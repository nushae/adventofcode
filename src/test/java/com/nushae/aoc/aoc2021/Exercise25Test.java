package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise25Test {

	@Test
	public void testExample1() {
		Exercise25 ex = new Exercise25("v...>>.vv>\n" +
				".vv>>.vv..\n" +
				">>.>v>...v\n" +
				">>v>>.>.v.\n" +
				"v>v.vv.v..\n" +
				">.>>..v...\n" +
				".vv..>.>v.\n" +
				"v.v..>>v.v\n" +
				"....v..v.>");
		ex.processInput();
		assertEquals(58, ex.doPart1());
	}
}