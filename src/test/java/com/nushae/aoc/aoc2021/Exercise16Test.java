package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	public void testBitLength() {
		Exercise16 ex = new Exercise16("38006F45291200");
		ex.processInput();
		assertEquals(9, ex.doPart1());
	}

	@Test
	public void testSubPacketCount() {
		Exercise16 ex = new Exercise16("EE00D40C823060");
		ex.processInput();
		assertEquals(14, ex.doPart1());
	}

	@Test
	public void testPart1a() {
		Exercise16 ex = new Exercise16("8A004A801A8002F478");
		ex.processInput();
		assertEquals(16, ex.doPart1());
	}

	@Test
	public void testPart1b() {
		Exercise16 ex = new Exercise16("620080001611562C8802118E34");
		ex.processInput();
		assertEquals(12, ex.doPart1());
	}

	@Test
	public void testPart1c() {
		Exercise16 ex = new Exercise16("C0015000016115A2E0802F182340");
		ex.processInput();
		assertEquals(23, ex.doPart1());
	}

	@Test
	public void testPart1d() {
		Exercise16 ex = new Exercise16("A0016C880162017C3686B18A3D4780");
		ex.processInput();
		assertEquals(31, ex.doPart1());
	}
}