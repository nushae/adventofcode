package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	public void testPath() {
		Exercise15 ex = new Exercise15("11\n" +
				"23");
		assertEquals(4, ex.doPart1());
	}

	@Test
	public void testPart1() {
		Exercise15 ex = new Exercise15("1163751742\n" +
				"1381373672\n" +
				"2136511328\n" +
				"3694931569\n" +
				"7463417111\n" +
				"1319128137\n" +
				"1359912421\n" +
				"3125421639\n" +
				"1293138521\n" +
				"2311944581");
		assertEquals(40, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise15 ex = new Exercise15("1163751742\n" +
				"1381373672\n" +
				"2136511328\n" +
				"3694931569\n" +
				"7463417111\n" +
				"1319128137\n" +
				"1359912421\n" +
				"3125421639\n" +
				"1293138521\n" +
				"2311944581");
		assertEquals(315, ex.doPart2());
	}
}