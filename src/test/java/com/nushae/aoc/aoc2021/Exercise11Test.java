package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise11Test {

	@Test
	public void testPart1() {
		Exercise11 ex = new Exercise11("5483143223\n" +
				"2745854711\n" +
				"5264556173\n" +
				"6141336146\n" +
				"6357385478\n" +
				"4167524645\n" +
				"2176841721\n" +
				"6882881134\n" +
				"4846848554\n" +
				"5283751526");
		assertEquals(1656, ex.doPart1(100));
	}

	@Test
	public void testPart2() {
		Exercise11 ex = new Exercise11("5483143223\n" +
				"2745854711\n" +
				"5264556173\n" +
				"6141336146\n" +
				"6357385478\n" +
				"4167524645\n" +
				"2176841721\n" +
				"6882881134\n" +
				"4846848554\n" +
				"5283751526");
		assertEquals(195, ex.doPart2());
	}
}