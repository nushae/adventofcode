package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise01Test {

	@Test
	public void testPart1() {
		Exercise01 ex = new Exercise01("199\n" +
				"200\n" +
				"208\n" +
				"210\n" +
				"200\n" +
				"207\n" +
				"240\n" +
				"269\n" +
				"260\n" +
				"263");
		ex.processInput();
		assertEquals(7, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise01 ex = new Exercise01("199\n" +
				"200\n" +
				"208\n" +
				"210\n" +
				"200\n" +
				"207\n" +
				"240\n" +
				"269\n" +
				"260\n" +
				"263");
		ex.processInput();
		assertEquals(5, ex.doPart2());
	}

}