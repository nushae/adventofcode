package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise02Test {

	@Test
	public void testPart1() {
		Exercise02 ex = new Exercise02("forward 5\n" +
				"down 5\n" +
				"forward 8\n" +
				"up 3\n" +
				"down 8\n" +
				"forward 2");
		assertEquals(150, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise02 ex = new Exercise02("forward 5\n" +
				"down 5\n" +
				"forward 8\n" +
				"up 3\n" +
				"down 8\n" +
				"forward 2");
		assertEquals(900, ex.doPart2());
	}
}