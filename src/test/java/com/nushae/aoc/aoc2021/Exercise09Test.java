package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise09Test {

	@Test
	public void testPart1() {
		Exercise09 ex = new Exercise09("2199943210\n" +
				"3987894921\n" +
				"9856789892\n" +
				"8767896789\n" +
				"9899965678");
		assertEquals(15, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise09 ex = new Exercise09("2199943210\n" +
				"3987894921\n" +
				"9856789892\n" +
				"8767896789\n" +
				"9899965678");
		assertEquals(1134, ex.doPart2());
	}
}