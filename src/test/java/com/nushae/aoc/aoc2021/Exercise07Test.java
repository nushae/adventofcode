package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise07Test {

	@Test
	public void testPart1() {
		Exercise07 ex = new Exercise07("16,1,2,0,4,2,7,1,2,14");
		assertEquals(37, ex.findBestScore(a -> a));
	}

	@Test
	public void testPart2() {
		Exercise07 ex = new Exercise07("16,1,2,0,4,2,7,1,2,14");
		assertEquals(168, ex.findBestScore(a -> a*(a+1)/2));
	}
}