package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise23Test {

	@Test
	public void testExample() {
		Exercise23 ex = new Exercise23();
		assertEquals(12521, ex.doPart1("..(BA).(CD).(BC).(DA)..", "..(AA).(BB).(CC).(DD).."));
	}
}