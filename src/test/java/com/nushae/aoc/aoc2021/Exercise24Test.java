package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise24Test {


	@Test
	public void testExample1() {
		Exercise24 ex = new Exercise24("inp x\n" +
				"mul x -1");
		ex.run(new int[] {5});
		assertEquals(-5, ex.registers.get("x"));
	}

	@Test
	public void testExample2() {
		Exercise24 ex = new Exercise24("inp z\n" +
				"inp x\n" +
				"mul z 3\n" +
				"eql z x");
		ex.run(new int[] {2, 6});
		assertEquals(1, ex.registers.get("z"));
		ex.run(new int[] {2, 5});
		assertEquals(0, ex.registers.get("z"));
	}

	@Test
	public void testExample3() {
		Exercise24 ex = new Exercise24("inp w\n" +
				"add z w\n" +
				"mod z 2\n" +
				"div w 2\n" +
				"add y w\n" +
				"mod y 2\n" +
				"div w 2\n" +
				"add x w\n" +
				"mod x 2\n" +
				"div w 2\n" +
				"mod w 2");
		List<Long> input = new ArrayList<>();
		ex.run(new int[] {15});
		assertEquals(1, ex.registers.get("w"));
		assertEquals(1, ex.registers.get("x"));
		assertEquals(1, ex.registers.get("y"));
		assertEquals(1, ex.registers.get("z"));
	}
}