package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@ParameterizedTest
	@MethodSource("argumentsForMagnitude")
	public void testMagnitude(String input, int magnitude) {
		Exercise18 ex = new Exercise18(input);
		assertEquals(magnitude, ex.doPart1());
	}
	private static Stream<Arguments> argumentsForMagnitude() {
		return Stream.of(
				Arguments.of("[1,2]", 7),
				Arguments.of("[[1,2],3]", 27),
				Arguments.of("[9,[8,7]]", 103),
				Arguments.of("[[1,9],[8,5]]", 131),
				Arguments.of("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]", 1443),
				Arguments.of("[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]", 1875),
				Arguments.of("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]", 2763),
				Arguments.of("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488)
		);
	}

	@Test
	public void testPart1() {
		Exercise18 ex = new Exercise18("[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n" +
				"[[[5,[2,8]],4],[5,[[9,9],0]]]\n" +
				"[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n" +
				"[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n" +
				"[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n" +
				"[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n" +
				"[[[[5,4],[7,7]],8],[[8,3],8]]\n" +
				"[[9,3],[[9,9],[6,[4,9]]]]\n" +
				"[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n" +
				"[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]");
		assertEquals(4140, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise18 ex = new Exercise18("[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n" +
				"[[[5,[2,8]],4],[5,[[9,9],0]]]\n" +
				"[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n" +
				"[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n" +
				"[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n" +
				"[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n" +
				"[[[[5,4],[7,7]],8],[[8,3],8]]\n" +
				"[[9,3],[[9,9],[6,[4,9]]]]\n" +
				"[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n" +
				"[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]");
		assertEquals(3993, ex.doPart2());
	}
}