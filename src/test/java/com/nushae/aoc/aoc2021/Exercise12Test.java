package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise12Test {

	@Test
	public void testPart1a() {
		Exercise12 ex = new Exercise12("start-A\n" +
				"start-b\n" +
				"A-c\n" +
				"A-b\n" +
				"b-d\n" +
				"A-end\n" +
				"b-end");
		assertEquals(10, ex.doPart1());
	}

	@Test
	public void testPart1b() {
		Exercise12 ex = new Exercise12("dc-end\n" +
				"HN-start\n" +
				"start-kj\n" +
				"dc-start\n" +
				"dc-HN\n" +
				"LN-dc\n" +
				"HN-end\n" +
				"kj-sa\n" +
				"kj-HN\n" +
				"kj-dc");
		assertEquals(19, ex.doPart1());
	}

	@Test
	public void testPart1c() {
		Exercise12 ex = new Exercise12("fs-end\n" +
				"he-DX\n" +
				"fs-he\n" +
				"start-DX\n" +
				"pj-DX\n" +
				"end-zg\n" +
				"zg-sl\n" +
				"zg-pj\n" +
				"pj-he\n" +
				"RW-he\n" +
				"fs-DX\n" +
				"pj-RW\n" +
				"zg-RW\n" +
				"start-pj\n" +
				"he-WI\n" +
				"zg-he\n" +
				"pj-fs\n" +
				"start-RW");
		assertEquals(226, ex.doPart1());
	}

	@Test
	public void testPart2a() {
		Exercise12 ex = new Exercise12("start-A\n" +
				"start-b\n" +
				"A-c\n" +
				"A-b\n" +
				"b-d\n" +
				"A-end\n" +
				"b-end");
		assertEquals(36, ex.doPart2());
	}

	@Test
	public void testPart2b() {
		Exercise12 ex = new Exercise12("dc-end\n" +
				"HN-start\n" +
				"start-kj\n" +
				"dc-start\n" +
				"dc-HN\n" +
				"LN-dc\n" +
				"HN-end\n" +
				"kj-sa\n" +
				"kj-HN\n" +
				"kj-dc");
		assertEquals(103, ex.doPart2());
	}

	@Test
	public void testPart2c() {
		Exercise12 ex = new Exercise12("fs-end\n" +
				"he-DX\n" +
				"fs-he\n" +
				"start-DX\n" +
				"pj-DX\n" +
				"end-zg\n" +
				"zg-sl\n" +
				"zg-pj\n" +
				"pj-he\n" +
				"RW-he\n" +
				"fs-DX\n" +
				"pj-RW\n" +
				"zg-RW\n" +
				"start-pj\n" +
				"he-WI\n" +
				"zg-he\n" +
				"pj-fs\n" +
				"start-RW");
		assertEquals(3509, ex.doPart2());
	}
}