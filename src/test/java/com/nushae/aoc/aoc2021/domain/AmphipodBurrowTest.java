package com.nushae.aoc.aoc2021.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AmphipodBurrowTest {

	@ParameterizedTest
	@MethodSource("argumentsForHasOnly")
	public void testHasOnly(boolean expected, String room, char letter) {
		assertEquals(expected, AmphipodBurrow.hasOnly(room, letter));
	}
	private static Stream<Arguments> argumentsForHasOnly() {
		return Stream.of(
				Arguments.of(true, "AAAA", 'A'),
				Arguments.of(true, "....", 'C'),
				Arguments.of(false, ".AB.", 'B'),
				Arguments.of(false, "ABCD", 'D')
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForMoveCosts")
	public void testMoveCosts(int[] expectedCosts, String input) {
		System.out.println(input);
		AmphipodBurrow burrow = new AmphipodBurrow(input);
		Map<AmphipodBurrow, Long> nextMoves = burrow.getAdjacentNodes();
		assertEquals(expectedCosts.length, nextMoves.size());
		expectedCosts = Arrays.stream(expectedCosts).sorted().toArray();
		int movNum = 0;
		for (Map.Entry<AmphipodBurrow, Long> move : nextMoves.entrySet().stream().sorted(Comparator.comparingLong(Map.Entry::getValue)).collect(Collectors.toList())) {
			assertEquals(expectedCosts[movNum++], move.getValue());
		}
	}
	private static Stream<Arguments> argumentsForMoveCosts() {
		return Stream.of(
				// single move into destination room. These starting states have just one legal move
				Arguments.of(new int[] {4}, "A.(..).(..).(..).(..).."),
				Arguments.of(new int[] {3}, ".A(..).(..).(..).(..).."),
				Arguments.of(new int[] {3}, "..(..)A(..).(..).(..).."),
				Arguments.of(new int[] {5}, "..(..).(..)A(..).(..).."),
				Arguments.of(new int[] {7}, "..(..).(..).(..)A(..).."),
				Arguments.of(new int[] {9}, "..(..).(..).(..).(..)A."),
				Arguments.of(new int[] {10}, "..(..).(..).(..).(..).A"),
				Arguments.of(new int[] {3}, "A.(.A).(..).(..).(..).."),
				Arguments.of(new int[] {2}, ".A(.A).(..).(..).(..).."),
				Arguments.of(new int[] {2}, "..(.A)A(..).(..).(..).."),
				Arguments.of(new int[] {4}, "..(.A).(..)A(..).(..).."),
				Arguments.of(new int[] {6}, "..(.A).(..).(..)A(..).."),
				Arguments.of(new int[] {8}, "..(.A).(..).(..).(..)A."),
				Arguments.of(new int[] {9}, "..(.A).(..).(..).(..).A"),
				Arguments.of(new int[] {60}, "B.(..).(..).(..).(..).."),
				Arguments.of(new int[] {50}, ".B(..).(..).(..).(..).."),
				Arguments.of(new int[] {30}, "..(..)B(..).(..).(..).."),
				Arguments.of(new int[] {30}, "..(..).(..)B(..).(..).."),
				Arguments.of(new int[] {50}, "..(..).(..).(..)B(..).."),
				Arguments.of(new int[] {70}, "..(..).(..).(..).(..)B."),
				Arguments.of(new int[] {80}, "..(..).(..).(..).(..).B"),
				Arguments.of(new int[] {800}, "C.(..).(..).(..).(..).."),
				Arguments.of(new int[] {700}, ".C(..).(..).(..).(..).."),
				Arguments.of(new int[] {500}, "..(..)C(..).(..).(..).."),
				Arguments.of(new int[] {300}, "..(..).(..)C(..).(..).."),
				Arguments.of(new int[] {300}, "..(..).(..).(..)C(..).."),
				Arguments.of(new int[] {500}, "..(..).(..).(..).(..)C."),
				Arguments.of(new int[] {600}, "..(..).(..).(..).(..).C"),
				Arguments.of(new int[] {10000}, "D.(..).(..).(..).(..).."),
				Arguments.of(new int[] {9000}, ".D(..).(..).(..).(..).."),
				Arguments.of(new int[] {7000}, "..(..)D(..).(..).(..).."),
				Arguments.of(new int[] {5000}, "..(..).(..)D(..).(..).."),
				Arguments.of(new int[] {3000}, "..(..).(..).(..)D(..).."),
				Arguments.of(new int[] {3000}, "..(..).(..).(..).(..)D."),
				Arguments.of(new int[] {4000}, "..(..).(..).(..).(..).D"),

				// moves out of a room to a destination room. These starting states have just one legal move
				Arguments.of(new int[] {9}, "..(..).(..).(..).(AB).."),
				Arguments.of(new int[] {10}, "..(..).(..).(..).(.A).."),
				Arguments.of(new int[] {50}, "..(..).(..).(BC).(..).."),
				Arguments.of(new int[] {60}, "..(..).(..).(.B).(..).."),
				Arguments.of(new int[] {500}, "..(..).(CD).(..).(..).."),
				Arguments.of(new int[] {600}, "..(..).(.C).(..).(..).."),
				Arguments.of(new int[] {9000}, "..(DA).(..).(..).(..).."),
				Arguments.of(new int[] {10000}, "..(.D).(..).(..).(..).."),

				// Moves out of a room into the hallway
				Arguments.of(new int[] {2, 2, 3, 4, 6, 8, 9, 20, 20, 40, 40, 50, 60, 70, 200, 200, 400, 400, 500, 600, 700, 2000, 2000, 3000, 4000, 6000, 8000, 9000}, "..(DA).(CD).(BC).(AB).."),
				Arguments.of(new int[] {3, 3, 4, 5, 7, 9, 10, 30, 30, 50, 50, 60, 70, 80, 300, 300, 500, 500, 600, 700, 800, 3000, 3000, 4000, 5000, 7000, 9000, 10000}, "..(.D).(.C).(.B).(.A)..")
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForGenerateMoves")
	public void testGenerateMoves(int expectedNumberOfMoves, int expectedTotalEnergy, String input) {
		AmphipodBurrow burrow = new AmphipodBurrow(input);
		Map<AmphipodBurrow, Long> nextMoves = burrow.getAdjacentNodes();
		int totalEnergy = 0;
		for (Map.Entry<AmphipodBurrow, Long> move : nextMoves.entrySet().stream().sorted(Comparator.comparingLong(Map.Entry::getValue)).collect(Collectors.toList())) {
			System.out.println(input + " -> " + move.getKey() + " = " + move.getValue());
			totalEnergy += move.getValue();
		}
		assertEquals(expectedNumberOfMoves, nextMoves.size());
		assertEquals(expectedTotalEnergy, totalEnergy);
	}
	private static Stream<Arguments> argumentsForGenerateMoves() {
		return Stream.of(
				// some random positions
				Arguments.of( 3,  2250, "AB(..).(..)C(.C)D(.D)AB"), // Only the leftmost B, C, or D can be moved (home)
				Arguments.of( 3,  4070, "AB(..).(..)D(BC).(.D)AC"), // leftmost B home, leftmost D home, rightmost B to hallway
				Arguments.of( 5,  6550, ".A(DD).(CC).(..)B(BA).."), // rightmost B to 2 hallway spots, leftmost D to 2 hallway spots, leftmost C home
				Arguments.of( 1,  9000, ".A(.D).(BB).(CC).(.D)A."), // leftmost D home
				// starting state for part 1
				Arguments.of(28, 37370, "..(DD).(CC).(AB).(BA)..")  // each leftmost letter in a room to all 7 hallway spots
		);
	}
}