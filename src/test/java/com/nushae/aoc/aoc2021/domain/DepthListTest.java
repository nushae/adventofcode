package com.nushae.aoc.aoc2021.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DepthListTest {

	@ParameterizedTest
	@MethodSource("argumentsForToString")
	public void testToString(String input) {
		assertEquals(input, new DepthList(input).toString());
	}
	private static Stream<Arguments> argumentsForToString() {
		return Stream.of(
				Arguments.of("[1,2]"),
				Arguments.of("[[1,2],3]"),
				Arguments.of("[9,[8,7]]"),
				Arguments.of("[[1,9],[8,5]]"),
				Arguments.of("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]"),
				Arguments.of("[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]"),
				Arguments.of("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]")
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForMagnitude")
	public void testMagnitude(String input, int magnitude) {
		assertEquals(magnitude, new DepthList(input).magnitude());
	}
	private static Stream<Arguments> argumentsForMagnitude() {
		return Stream.of(
				Arguments.of("[1,2]", 7),
				Arguments.of("[[1,2],3]", 27),
				Arguments.of("[9,[8,7]]", 103),
				Arguments.of("[[1,9],[8,5]]", 131),
				Arguments.of("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]", 1443),
				Arguments.of("[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]", 1875),
				Arguments.of("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]", 2763),
				Arguments.of("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488)
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForReduce")
	public void testReduce(String input, String result) {
		assertEquals(result, new DepthList(input).reduce().toString());
	}
	private static Stream<Arguments> argumentsForReduce() {
		return Stream.of(
				Arguments.of("[1,1]", "[1,1]"), // no reduction
				Arguments.of("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]"), // single explode
				Arguments.of("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]"), // single explode
				Arguments.of("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]"), // single explode
				Arguments.of("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"), // double explode
				Arguments.of("[[3,13],7]", "[[3,[6,7]],7]"), // single split
				Arguments.of("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"), // full reduction
				Arguments.of("[[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]],[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]]", "[[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]]") // from example
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForAdd")
	public void testAdd(String input1, String input2, String result) {
		assertEquals(result, new DepthList(input1).add(new DepthList(input2)).toString());
	}
	private static Stream<Arguments> argumentsForAdd() {
		return Stream.of(
				Arguments.of("[1,1]", "[1,1]", "[[1,1],[1,1]]"),
				Arguments.of("[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"),
				Arguments.of("[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]", "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]", "[[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]]")
		);
	}
}