package com.nushae.aoc.aoc2021.domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CuboidTest {

	@ParameterizedTest
	@MethodSource("argumentsForVolume")
	public void testVolume(int expected, int lowX, int highX, int lowY, int highY, int lowZ, int highZ) {
		Cuboid cuboid = new Cuboid(lowX, highX, lowY, highY, lowZ, highZ, true);
		assertEquals(expected, cuboid.volume());
	}
	private static Stream<Arguments> argumentsForVolume() {
		return Stream.of(
				Arguments.of(1, 0, 0, 0, 0, 0, 0),
				Arguments.of(125, 0, 4, 0, 4, 0, 4),
				Arguments.of(15, -4, -2, -1, -1, 0, 4)
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForVolumeComplex")
	public void testVolumeComplex(int expected, int lowX, int highX, int lowY, int highY, int lowZ, int highZ, int lowX2, int highX2, int lowY2, int highY2, int lowZ2, int highZ2) {
		Cuboid cuboid = new Cuboid(lowX, highX, lowY, highY, lowZ, highZ, true);
		Cuboid cuboid2 = new Cuboid(lowX2, highX2, lowY2, highY2, lowZ2, highZ2, true);
		cuboid.remove(cuboid2);
		assertEquals(expected, cuboid.volume());
	}
	private static Stream<Arguments> argumentsForVolumeComplex() {
		return Stream.of(
				Arguments.of(1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1),
				Arguments.of(6, -4, -2, -1, -1, 0, 4, -10, 10, -10, 10, -10, 2),
				Arguments.of(124, 0, 4, 0, 4, 0, 4, 1, 1, 1, 1, 1, 1),
				Arguments.of(0, 0, 0, 0, 0, 0, 0, -1, 1, -1, 1, -1, 1)
		);
	}

	@ParameterizedTest
	@MethodSource("argumentsForIntersect")
	public void testIntersect(String expected, int lowX, int highX, int lowY, int highY, int lowZ, int highZ, int lowX2, int highX2, int lowY2, int highY2, int lowZ2, int highZ2) {
		Cuboid cuboid = new Cuboid(lowX, highX, lowY, highY, lowZ, highZ, true);
		Cuboid cuboid2 = new Cuboid(lowX2, highX2, lowY2, highY2, lowZ2, highZ2, true);
		if (expected == null) {
			assertNull(cuboid.intersect(cuboid2));
			assertNull(cuboid2.intersect(cuboid));
		} else {
			assertEquals(expected, cuboid.intersect(cuboid2).toString());
			assertEquals(expected, cuboid2.intersect(cuboid).toString());
		}
	}
	private static Stream<Arguments> argumentsForIntersect() {
		return Stream.of(
				Arguments.of("on [0-0,0-0,0-0]", 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0),
				Arguments.of("on [0-2,0-2,2-2]", 0, 2, 0, 2, 0, 2,    0, 2, 0, 2, 2, 4),
				Arguments.of(null, 0, 0, 0, 0, 0, 0,    1, 1, 1, 1, 1, 1)
		);
	}

}