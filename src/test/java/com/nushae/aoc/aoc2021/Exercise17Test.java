package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	public void testPart1() {
		Exercise17 ex = new Exercise17("target area: x=20..30, y=-10..-5");
		assertEquals(45, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise17 ex = new Exercise17("target area: x=20..30, y=-10..-5");
		assertEquals(112, ex.doPart2());
	}
}