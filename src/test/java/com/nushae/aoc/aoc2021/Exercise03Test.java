package com.nushae.aoc.aoc2021;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@Test
	public void testPart1() {
		Exercise03 ex = new Exercise03("00100\n" +
				"11110\n" +
				"10110\n" +
				"10111\n" +
				"10101\n" +
				"01111\n" +
				"00111\n" +
				"11100\n" +
				"10000\n" +
				"11001\n" +
				"00010\n" +
				"01010");
		assertEquals(198, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise03 ex = new Exercise03("00100\n" +
				"11110\n" +
				"10110\n" +
				"10111\n" +
				"10101\n" +
				"01111\n" +
				"00111\n" +
				"11100\n" +
				"10000\n" +
				"11001\n" +
				"00010\n" +
				"01010");
		assertEquals(230, ex.doPart2());
	}

}