package com.nushae.aoc.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BigFractionTest {

	@ParameterizedTest
	@MethodSource("valueSource")
	void valueOf(int e, int d, BigFraction expected) {
		assertEquals(expected, BigFraction.valueOf(e, d));
	}

	public static Stream<Arguments> valueSource() {
		return Stream.of(
				Arguments.of(1, 3, new BigFraction(BigInteger.ONE, BigInteger.valueOf(3))),
				Arguments.of(2, 6, new BigFraction(BigInteger.ONE, BigInteger.valueOf(3))),
				Arguments.of(9, 17, new BigFraction(BigInteger.valueOf(9), BigInteger.valueOf(17))),
				Arguments.of(-9, 17, new BigFraction(BigInteger.valueOf(-9), BigInteger.valueOf(17))),
				Arguments.of(9, -17, new BigFraction(BigInteger.valueOf(-9), BigInteger.valueOf(17))),
				Arguments.of(-9, -17, new BigFraction(BigInteger.valueOf(9), BigInteger.valueOf(17)))
		);
	}

	@ParameterizedTest
	@MethodSource("addSource")
	void testAdd(BigFraction left, BigFraction right, BigFraction expected) {
		assertEquals(expected, left.add(right));
	}

	public static Stream<Arguments> addSource() {
		return Stream.of(
				Arguments.of(BigFraction.THIRD, BigFraction.unitFractionOf(4), BigFraction.valueOf(7, 12)),
				Arguments.of(BigFraction.valueOf(2, 3), BigFraction.THIRD, BigFraction.ONE),
				Arguments.of(BigFraction.HALF, BigFraction.THIRD, BigFraction.valueOf(5, 6)),
				Arguments.of(BigFraction.PERCENT, BigFraction.valueOf(9, 100), BigFraction.TENTH),
				Arguments.of(BigFraction.valueOf(11, 6), BigFraction.valueOf(9, 17), BigFraction.valueOf(241, 102))
		);
	}

	@ParameterizedTest
	@MethodSource("subtractSource")
	void testSubtract(BigFraction left, BigFraction right, BigFraction expected) {
		assertEquals(expected, left.subtract(right));
	}

	public static Stream<Arguments> subtractSource() {
		return Stream.of(
				Arguments.of(BigFraction.THIRD, BigFraction.valueOf(1, 4), BigFraction.valueOf(1, 12)),
				Arguments.of(BigFraction.valueOf(2, 3), BigFraction.THIRD, BigFraction.THIRD),
				Arguments.of(BigFraction.valueOf(4, 9), BigFraction.valueOf(1, 9), BigFraction.THIRD),
				Arguments.of(BigFraction.valueOf(11, 6), BigFraction.valueOf(9, 17), BigFraction.valueOf(133, 102))
		);
	}

	@ParameterizedTest
	@MethodSource("multiplySource")
	void testMultiply(BigFraction left, BigFraction right, BigFraction expected) {
		assertEquals(expected, left.multiply(right));
	}

	public static Stream<Arguments> multiplySource() {
		return Stream.of(
				Arguments.of(BigFraction.THIRD, BigFraction.valueOf(3, 7), BigFraction.valueOf(1, 7)),
				Arguments.of(BigFraction.valueOf(2, 3), BigFraction.THIRD, BigFraction.valueOf(2, 9)),
				Arguments.of(BigFraction.valueOf(11, 6), BigFraction.valueOf(9, 17), BigFraction.valueOf(33, 34))
		);
	}

	@ParameterizedTest
	@MethodSource("divideSource")
	void testDivide(BigFraction left, BigFraction right, BigFraction expected) {
		assertEquals(expected, left.divide(right));
	}

	public static Stream<Arguments> divideSource() {
		return Stream.of(
				Arguments.of(BigFraction.THIRD, BigFraction.valueOf(3, 7), BigFraction.valueOf(7, 9)),
				Arguments.of(BigFraction.valueOf(2, 3), BigFraction.THIRD, BigFraction.valueOf(2)),
				Arguments.of(BigFraction.valueOf(11, 6), BigFraction.valueOf(9, 17), BigFraction.valueOf(187, 54))
		);
	}

	@Test
	void compareTo() {
	}

	@ParameterizedTest
	@MethodSource("intValueSource")
	void testIntValue(long e, long d, int expected) {
		assertEquals(expected, BigFraction.valueOf(e, d).intValue());
	}

	public static Stream<Arguments> intValueSource() {
		return Stream.of(
				Arguments.of(1, 3, 0),
				Arguments.of(2, 6, 0),
				Arguments.of(17, 9, 1),
				Arguments.of(-17, 9, -1),
				Arguments.of(17, -9, -1),
				Arguments.of(-17, -9, 1)
		);
	}

	@ParameterizedTest
	@MethodSource("longValueSource")
	void testLongValue(long e, long d, long expected) {
		assertEquals(expected, BigFraction.valueOf(e, d).longValue());
	}

	public static Stream<Arguments> longValueSource() {
		return Stream.of(
				Arguments.of(1, 3, 0),
				Arguments.of(21234567890L, 2, 10617283945L)
		);
	}

	@ParameterizedTest
	@MethodSource("toStringSource")
	void testToString(long e, long d, String expected) {
		assertEquals(expected, BigFraction.valueOf(e, d).toString());
	}

	public static Stream<Arguments> toStringSource() {
		return Stream.of(
				Arguments.of(1, 3, "1/3"),
				Arguments.of(2, 6, "1/3"),
				Arguments.of(18, -2, "-9"),
				Arguments.of(17, 9, "17/9"),
				Arguments.of(17, -9, "-17/9")
		);
	}
}