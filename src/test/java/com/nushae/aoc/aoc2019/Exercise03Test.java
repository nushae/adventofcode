package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@Test
	void testExample1() {
		Exercise03 ex = new Exercise03("R8,U5,L5,D3\n" +
				"U7,R6,D4,L4");
		assertEquals(6, ex.doPart1());
		assertEquals(30, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise03 ex = new Exercise03("R75,D30,R83,U83,L12,D49,R71,U7,L72\n" +
				"U62,R66,U55,R34,D71,R55,D58,R83");
		assertEquals(159, ex.doPart1());
		assertEquals(610, ex.doPart2());
	}

	@Test
	void testExample3() {
		Exercise03 ex = new Exercise03("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\n" +
				"U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
		assertEquals(135, ex.doPart1());
		assertEquals(410, ex.doPart2());
	}
}