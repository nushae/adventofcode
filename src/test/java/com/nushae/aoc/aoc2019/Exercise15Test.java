package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.Direction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testDirectionRotR() {
		assertEquals(Direction.EAST, Direction.NORTH.rotR());
		assertEquals(Direction.SOUTH, Direction.EAST.rotR());
		assertEquals(Direction.WEST, Direction.SOUTH.rotR());
		assertEquals(Direction.NORTH, Direction.WEST.rotR());
	}

	@Test
	void testDirectionOpp() {
		assertEquals(Direction.SOUTH, Direction.NORTH.opp());
		assertEquals(Direction.WEST, Direction.EAST.opp());
		assertEquals(Direction.NORTH, Direction.SOUTH.opp());
		assertEquals(Direction.EAST, Direction.WEST.opp());
	}

	@Test
	void testDirectionRotL() {
		assertEquals(Direction.WEST, Direction.NORTH.rotL());
		assertEquals(Direction.NORTH, Direction.EAST.rotL());
		assertEquals(Direction.EAST, Direction.SOUTH.rotL());
		assertEquals(Direction.SOUTH, Direction.WEST.rotL());
	}
}
