package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.KeyMazeNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@BeforeEach
	void cleanSearchNode() {
		KeyMazeNode.reInit();
	}

	@Test
	void testMaze1() {
		Exercise18 ex = new Exercise18("#########\n" +
				"#b.A.@.a#\n" +
				"#########");
		assertEquals(8, ex.doPart1());
	}

	@Test
	void testMaze2() {
		Exercise18 ex = new Exercise18("########################\n" +
				"#f.D.E.e.C.b.A.@.a.B.c.#\n" +
				"######################.#\n" +
				"#d.....................#\n" +
				"########################");

		assertEquals(86, ex.doPart1());
	}

	@Test
	void testMaze3() {
		Exercise18 ex = new Exercise18("########################\n" +
				"#...............b.C.D.f#\n" +
				"#.######################\n" +
				"#.....@.a.B.c.d.A.e.F.g#\n" +
				"########################");

		assertEquals(132, ex.doPart1());
	}

	@Test
	void testMaze4() {
		Exercise18 ex = new Exercise18("#################\n" +
				"#i.G..c...e..H.p#\n" +
				"########.########\n" +
				"#j.A..b...f..D.o#\n" +
				"########@########\n" +
				"#k.E..a...g..B.n#\n" +
				"########.########\n" +
				"#l.F..d...h..C.m#\n" +
				"#################");

		assertEquals(136, ex.doPart1());
	}

	@Test
	void testMaze5() {
		Exercise18 ex = new Exercise18("########################\n" +
				"#@..............ac.GI.b#\n" +
				"###d#e#f################\n" +
				"###A#B#C################\n" +
				"###g#h#i################\n" +
				"########################");

		assertEquals(81, ex.doPart1());
	}
}