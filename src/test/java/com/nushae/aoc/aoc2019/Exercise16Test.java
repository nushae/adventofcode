package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	void testExample1() {
		assertEquals("01029498", Exercise16.doFFTAsIs("12345678", 0, 8, 4));
		assertEquals("01029498", Exercise16.doFFTOptimized("12345678", 0, 8, 4));
	}

	@Test
	void testExample2() {
		assertEquals("24176176", Exercise16.doFFTAsIs("80871224585914546619083218645595", 0, 8, 100));
		assertEquals("24176176", Exercise16.doFFTOptimized("80871224585914546619083218645595", 0, 8, 100));
	}

	@Test
	void testExample3() {
		assertEquals("73745418", Exercise16.doFFTAsIs("19617804207202209144916044189917", 0, 8, 100));
		assertEquals("73745418", Exercise16.doFFTOptimized("19617804207202209144916044189917", 0, 8, 100));
	}

	@Test
	void testExample4() {
		assertEquals("52432133", Exercise16.doFFTAsIs("69317163492948606335995924319873", 0, 8, 100));
		assertEquals("52432133", Exercise16.doFFTOptimized("69317163492948606335995924319873", 0, 8, 100));
	}

	@Test
	void testExample5() {
		String longInput = Exercise16.longify("03036732577212944063491565474664");
		long start = System.currentTimeMillis();
		String result1 = Exercise16.doFFTOptimized(longInput, 0, 8, 1);
		System.out.println(result1);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
		start = System.currentTimeMillis();
		String result2 = Exercise16.doFFTAsIs(longInput, 0, 8, 1);
		System.out.println(result2);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
		assertEquals(result1, result2);
	}
}