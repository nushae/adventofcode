package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise01Test {

	@Test
	void testExample1() {
		Exercise01 ex = new Exercise01("12\n" +
				"14\n" +
				"1969\n" +
				"100756");
		assertEquals(34241, ex.doPart1());
		assertEquals(51316, ex.doPart2());
	}
}