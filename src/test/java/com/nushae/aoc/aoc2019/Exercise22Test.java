package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.file.Paths;
import java.util.stream.Stream;

import static com.nushae.aoc.util.MathUtil.bezout_coeff;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	void testInverseCutOldStyle() {
		int decksize = 11;
		for (int i = 0; i < decksize; i++) {
			for (int cut = 0; cut < decksize; cut++) {
				assertEquals(i, Exercise22.cut(Exercise22.cut(i, cut, decksize), -cut, decksize));
			}
		}
	}

	@Test
	void testInverseReverseOldStyle() {
		int decksize = 11;
		for (int i = 0; i < decksize; i++) {
			assertEquals(i, Exercise22.rev(Exercise22.rev(i, decksize), decksize));
		}
	}

	@Test
	void testInverseIncrementOldStyle() {
		int decksize = 11; // must be prime!
		for (int i = 0; i < decksize; i++) {
			for (int incr = 2; incr < decksize; incr++) { // skip 1 because we need coprime numbers and 1 is never coprime with anything
				assertEquals(i, Exercise22.incr(Exercise22.incr(i, incr, decksize), bezout_coeff(incr, decksize)[0], decksize));
			}
		}
	}

	@Test
	void testInverseCutFlattened() {
		Exercise22 ex = new Exercise22(10007, "cut 3627\n" +
				"cut -1397\n" +
				"cut 1908\n" +
				"cut 8923");
		assertEquals(ex.doPart1Slow(1234, 1), ex.doPart1(1234, 1));
		assertEquals(ex.doPart1Slow(1234, 7), ex.doPart1(1234, 7));
		assertEquals(ex.doPart2Slow(1234, 1), ex.doPart2(1234, 1));
		assertEquals(ex.doPart2Slow(1234, 7), ex.doPart2(1234, 7));
		assertEquals(1234, ex.doPart2Slow(ex.doPart1(1234, 1), 1));
		assertEquals(1234, ex.doPart2Slow(ex.doPart1(1234, 7), 7));
		assertEquals(1234, ex.doPart2(ex.doPart1(1234, 1), 1));
		assertEquals(1234, ex.doPart2(ex.doPart1(1234, 7), 7));
	}

	@Test
	void testInverseReverseFlattened() {
		Exercise22 ex = new Exercise22(10007, "deal into new stack\n" +
				"cut 3197\n" +
				"deal into new stack\n" +
				"cut 8923");
		assertEquals(ex.doPart1Slow(1234, 1), ex.doPart1(1234, 1));
		assertEquals(ex.doPart1Slow(1234, 7), ex.doPart1(1234, 7));
		assertEquals(ex.doPart2Slow(1234, 1), ex.doPart2(1234, 1));
		assertEquals(ex.doPart2Slow(1234, 7), ex.doPart2(1234, 7));
		assertEquals(1234, ex.doPart2Slow(ex.doPart1(1234, 1), 1));
		assertEquals(1234, ex.doPart2Slow(ex.doPart1(1234, 7), 7));
		assertEquals(1234, ex.doPart2(ex.doPart1(1234, 1), 1));
		assertEquals(1234, ex.doPart2(ex.doPart1(1234, 7), 7));
	}

	@Test
	void testInverseIncrementFlattened() {
		Exercise22 ex = new Exercise22(10007, "deal with increment 33\n" +
				"deal with increment 68\n" +
				"deal with increment 53");
		assertEquals(ex.doPart1Slow(1234, 1), ex.doPart1(1234, 1));
		assertEquals(ex.doPart1Slow(1234, 7), ex.doPart1(1234, 7));
		assertEquals(ex.doPart2Slow(1234, 1), ex.doPart2(1234, 1));
		assertEquals(ex.doPart2Slow(1234, 7), ex.doPart2(1234, 7));
		assertEquals(1234, ex.doPart2Slow(ex.doPart1(1234, 1), 1));
		assertEquals(1234, ex.doPart2Slow(ex.doPart1(1234, 7), 7));
		assertEquals(1234, ex.doPart2(ex.doPart1(1234, 1), 1));
		assertEquals(1234, ex.doPart2(ex.doPart1(1234, 7), 7));
	}

	@ParameterizedTest
	@MethodSource("argumentsForInverseWithPart1")
	void testInverseWithPart1(long input, long SHUFFLES, boolean PART_1_SLOW, boolean PART_2_SLOW, long expectedFirst, long expectedSecond) {
		Exercise22 ex = new Exercise22(10007, Paths.get("D:/projects/adventofcode/target/classes/com/nushae/aoc/aoc2019/ex22/input.txt"));

		long start = System.currentTimeMillis();
		long firstPos = PART_1_SLOW ? ex.doPart1Slow(input, SHUFFLES) : ex.doPart1(input, SHUFFLES);
		System.out.println(input + " ends up in position " + firstPos + " after " + SHUFFLES + (PART_1_SLOW ? " slow" : " flattened") + " shuffle(s)");
		assertEquals(expectedFirst, firstPos);
		long secondPos = PART_1_SLOW ? ex.doPart1Slow(firstPos, SHUFFLES) : ex.doPart1(firstPos, SHUFFLES);
		System.out.println(firstPos + " ends up in position " + secondPos + " after another " + SHUFFLES + (PART_1_SLOW ? " slow" : " flattened") + " shuffle(s)");
		assertEquals(expectedSecond, secondPos);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// verify reverse shuffle based on part 1:
		start = System.currentTimeMillis();
		long shouldBeFirstPos = PART_2_SLOW ? ex.doPart2Slow(secondPos, SHUFFLES) : ex.doPart2(secondPos, SHUFFLES);
		System.out.println("The card in position " + secondPos + " after " + SHUFFLES + (PART_2_SLOW ? " slow" : " flattened") + " shuffle(s) was originally in position " + shouldBeFirstPos);
		assertEquals(expectedFirst, shouldBeFirstPos);
		long shouldBeInput = PART_2_SLOW ? ex.doPart2Slow(shouldBeFirstPos, SHUFFLES) : ex.doPart2(shouldBeFirstPos, SHUFFLES);
		System.out.println("The card in position " + shouldBeFirstPos + " after " + SHUFFLES + (PART_2_SLOW ? " slow" : " flattened") + " shuffle(s) was originally in position " + shouldBeInput);
		assertEquals(input, shouldBeInput);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}

	private static Stream<Arguments> argumentsForInverseWithPart1() {
		return Stream.of(
				Arguments.of(2019, 1, true, true, 8775, 575),
				Arguments.of(2019, 1, true, false, 8775, 575),
				Arguments.of(2019, 1, false, true, 8775, 575),
				Arguments.of(2019, 1, false, false, 8775, 575),
				Arguments.of(2019, 7, false, false, 9508, 1446),
				Arguments.of(2019, 3197, false, false, 7880, 8002)
		);
	}
}