package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise06Test {

	@Test
	void testExample1() {
		Exercise06 ex = new Exercise06("A)B\n" +
				"B)C\n" +
				"C)D\n" +
				"D)E\n" +
				"E)F");
		assertEquals(15, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise06 ex = new Exercise06("COM)B\n" +
				"B)C\n" +
				"C)D\n" +
				"D)E\n" +
				"E)F\n" +
				"B)G\n" +
				"G)H\n" +
				"D)I\n" +
				"E)J\n" +
				"J)K\n" +
				"K)L");
		assertEquals(42, ex.doPart1());
	}

	@Test
	void testExample3() {
		Exercise06 ex = new Exercise06("COM)B\n" +
				"B)C\n" +
				"C)D\n" +
				"D)E\n" +
				"E)F\n" +
				"B)G\n" +
				"G)H\n" +
				"D)I\n" +
				"E)J\n" +
				"J)K\n" +
				"K)L\n" +
				"K)YOU\n" +
				"I)SAN");
		assertEquals(54, ex.doPart1());
		assertEquals(4, ex.doPart2());
	}
}