package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise02Test {
	@Test
	void testExample1() {
		Exercise02 ex = new Exercise02("1,9,10,3,2,3,11,0,99,30,40,50");
		assertEquals("3500,9,10,70,2,3,11,0,99,30,40,50", ex.runProgramNoInputs());
	}

	@Test
	void testExample2() {
		Exercise02 ex = new Exercise02("1,0,0,0,99");
		assertEquals("2,0,0,0,99", ex.runProgramNoInputs());
	}

	@Test
	void testExample3() {
		Exercise02 ex = new Exercise02("2,3,0,3,99");
		assertEquals("2,3,0,6,99", ex.runProgramNoInputs());
	}

	@Test
	void testExample4() {
		Exercise02 ex = new Exercise02("2,4,4,5,99,0");
		assertEquals("2,4,4,5,99,9801", ex.runProgramNoInputs());
	}

	@Test
	void testExample5() {
		Exercise02 ex = new Exercise02("1,1,1,4,99,5,6,0,99");
		assertEquals("30,1,1,4,2,5,6,0,99", ex.runProgramNoInputs());
	}
}