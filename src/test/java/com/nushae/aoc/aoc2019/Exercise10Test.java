package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	void testLocationExample1() {
		Exercise10 ex = new Exercise10(".#..#\n" +
				".....\n" +
				"#####\n" +
				"....#\n" +
				"...##\n");
		assertEquals(8, ex.doPart1());
	}

	@Test
	void testLocationExample2() {
		Exercise10 ex = new Exercise10("......#.#.\n" +
				"#..#.#....\n" +
				"..#######.\n" +
				".#.#.###..\n" +
				".#..#.....\n" +
				"..#....#.#\n" +
				"#..#....#.\n" +
				".##.#..###\n" +
				"##...#..#.\n" +
				".#....####\n");
		assertEquals(33, ex.doPart1());
	}

	@Test
	void testLocationExample3() {
		Exercise10 ex = new Exercise10("#.#...#.#.\n" +
				".###....#.\n" +
				".#....#...\n" +
				"##.#.#.#.#\n" +
				"....#.#.#.\n" +
				".##..###.#\n" +
				"..#...##..\n" +
				"..##....##\n" +
				"......#...\n" +
				".####.###.\n");
		assertEquals(35, ex.doPart1());
	}

	@Test
	void testLocationExample4() {
		Exercise10 ex = new Exercise10(".#..#..###\n" +
				"####.###.#\n" +
				"....###.#.\n" +
				"..###.##.#\n" +
				"##.##.#.#.\n" +
				"....###..#\n" +
				"..#.#..#.#\n" +
				"#..#.#.###\n" +
				".##...##.#\n" +
				".....#.#..\n");
		assertEquals(41, ex.doPart1());
	}

	@Test
	void testLocationExample5() {
		Exercise10 ex = new Exercise10(".#..##.###...#######\n" +
				"##.############..##.\n" +
				".#.######.########.#\n" +
				".###.#######.####.#.\n" +
				"#####.##.#.##.###.##\n" +
				"..#####..#.#########\n" +
				"####################\n" +
				"#.####....###.#.#.##\n" +
				"##.#################\n" +
				"#####.##.###..####..\n" +
				"..######..##.#######\n" +
				"####.##.####...##..#\n" +
				".#####..#.######.###\n" +
				"##...#.##########...\n" +
				"#.##########.#######\n" +
				".####.#.###.###.#.##\n" +
				"....##.##.###..#####\n" +
				".#.#.###########.###\n" +
				"#.#.#.#####.####.###\n" +
				"###.##.####.##.#..##\n");
		assertEquals(210, ex.doPart1());
	}

	@Test
	void testZapExample1() {
		Exercise10 ex = new Exercise10(".#....#####...#..\n" +
				"##...##.#####..##\n" +
				"##...#...#.#####.\n" +
				"..#.....#...###..\n" +
				"..#.#.....#....##");
		assertEquals(1000, ex.doPart2(4));
		assertEquals(1403, ex.doPart2(36));
		assertEquals(-1, ex.doPart2(37));
	}
}