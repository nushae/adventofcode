package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise24Test {

	@Test
	void testPart1EmptyGrid() {
		boolean[][] grid = new boolean[5][5];
		boolean[][] expected = new boolean[5][5];
		boolean[][] result = Exercise24.doGeneration(grid);
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				assertEquals(expected[i][j], result[i][j]);
			}
		}
	}

	@Test
	void testPart1ExampleGrid() {
		//....#        #..#.
		//#..#.        ####.
		//#..##   ->   ###.#
		//..#..        ##.##
		//#....        .##..
		boolean[][] grid = loadGridFromString(
				"....#",
				"#..#.",
				"#..##",
				"..#..",
				"#....");
		boolean[][] expected = loadGridFromString(
				"#..#.",
				"####.",
				"###.#",
				"##.##",
				".##..");
		boolean[][] result = Exercise24.doGeneration(grid);
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				assertEquals(expected[i][j], result[i][j]);
			}
		}
	}

	@Test
	void testDonutExample() {
		List<boolean[][]> grids = new ArrayList<>();
		grids.add(loadGridFromString("....#\n" +
				"#..#.\n" +
				"#..##\n" +
				"..#..\n" +
				"#...."));
		List<boolean[][]> expected = new ArrayList<>();
		expected.add(loadGridFromString("####.\n" +
				"#..#.\n" +
				"#..#.\n" +
				"####.\n" +
				"....."));
		expected.add(loadGridFromString(".###.\n" +
				"#..#.\n" +
				"#....\n" +
				"##.#.\n" +
				"....."));
		expected.add(loadGridFromString("..###\n" +
				".....\n" +
				"#....\n" +
				"#....\n" +
				"#...#"));
		expected.add(loadGridFromString("###..\n" +
				"##.#.\n" +
				"#....\n" +
				".#.##\n" +
				"#.#.."));
		expected.add(loadGridFromString(".##..\n" +
				"#..##\n" +
				"....#\n" +
				"##.##\n" +
				"#####"));
		expected.add(loadGridFromString(".#...\n" +
				".#.##\n" +
				".#...\n" +
				".....\n" +
				"....."));
		expected.add(loadGridFromString("#..##\n" +
				"...##\n" +
				".....\n" +
				"...#.\n" +
				".####"));
		expected.add(loadGridFromString(".#.##\n" +
				"....#\n" +
				"....#\n" +
				"...##\n" +
				".###."));
		expected.add(loadGridFromString("#.#..\n" +
				".#...\n" +
				".....\n" +
				".#...\n" +
				"#.#.."));
		expected.add(loadGridFromString("...#.\n" +
				"...##\n" +
				".....\n" +
				"...##\n" +
				"...#."));
		expected.add(loadGridFromString("..#..\n" +
				".#.#.\n" +
				"....#\n" +
				".#.#.\n" +
				"..#.."));

		List<boolean[][]> result = Exercise24.donutGeneration(grids);
		for (int m=0; m < 9; m++) {
			result = Exercise24.donutGeneration(result);
		}
		assertEquals(expected.size(), result.size());
		print(result);
		for (int g=0; g< result.size(); g++) {
			boolean[][] expectedGrid = expected.get(g);
			boolean[][] resultGrid = result.get(g);
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					assertEquals(expectedGrid[i][j], resultGrid[i][j], "Grid " + g + ", i,j = " + i + "," + j);
				}
			}
		}
	}

	private static boolean[][] loadGridFromString(String input) {
		return loadGridFromString(input.split("\n"));
	}
	private static boolean[][] loadGridFromString(String... lines) {
		boolean[][] result = new boolean[5][5];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				result[i][j] = lines[j].charAt(i) == '#';
			}
		}
		return result;
	}

	private static void print(List<boolean[][]> result) {
		System.out.println("(innermost)");
		for (boolean[][] donut : result) {
			for (int j=0; j<5; j++) {
				for (int i=0; i<5; i++) {
					System.out.print(donut[i][j] ? '#' : '.');
				}
				System.out.println();
			}
			System.out.println();
		}
		System.out.println("(outermost)");
	}
}