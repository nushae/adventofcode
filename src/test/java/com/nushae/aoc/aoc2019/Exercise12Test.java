package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.Moon;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise12Test {

	@Test
	void testRunSimulationA() {
		Exercise12 e12 = new Exercise12(
				new Moon(-1, 0, 2),
				new Moon(2, -10, -7),
				new Moon(4, -8, 8),
				new Moon(3, 5, -1)
		);
		assertEquals(179, e12.doPart1(10));
		assertEquals("pos=<x=  2, y=  1, z= -3>, vel=<x= -3, y= -2, z=  1>\n" +
				"pos=<x=  1, y= -8, z=  0>, vel=<x= -1, y=  1, z=  3>\n" +
				"pos=<x=  3, y= -6, z=  1>, vel=<x=  3, y=  2, z= -3>\n" +
				"pos=<x=  2, y=  0, z=  4>, vel=<x=  1, y= -1, z= -1>\n",
				e12.toString()
		);
	}

	@Test
	void testRunSimulationB() {
		Exercise12 e12 = new Exercise12(
				new Moon(-8, -10, 0),
				new Moon(5, 5, 10),
				new Moon(2, -7, 3),
				new Moon(9, -8, -3)
		);
		assertEquals(1940, e12.doPart1(100));
		assertEquals("pos=<x=  8, y=-12, z= -9>, vel=<x= -7, y=  3, z=  0>\n" +
				"pos=<x= 13, y= 16, z= -3>, vel=<x=  3, y=-11, z= -5>\n" +
				"pos=<x=-29, y=-11, z= -1>, vel=<x= -3, y=  7, z=  4>\n" +
				"pos=<x= 16, y=-13, z= 23>, vel=<x=  7, y=  1, z=  1>\n",
				e12.toString()
		);
	}

	@Test
	void testFindStartStateA() {
		Exercise12 e12 = new Exercise12(
				new Moon(-1, 0, 2),
				new Moon(2, -10, -7),
				new Moon(4, -8, 8),
				new Moon(3, 5, -1)
		);
		assertEquals(BigInteger.valueOf(2772), e12.doPart2());
		assertEquals("pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>\n" +
				"pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>\n" +
				"pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>\n" +
				"pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>\n",
				e12.toString());
	}

	@Test
	void testFindStartStateB() {
		Exercise12 e12 = new Exercise12(
				new Moon(-8, -10, 0),
				new Moon(5, 5, 10),
				new Moon(2, -7, 3),
				new Moon(9, -8, -3)
		);
		assertEquals(BigInteger.valueOf(4686774924L), e12.doPart2());
		assertEquals("pos=<x= -8, y=-10, z=  0>, vel=<x=  0, y=  0, z=  0>\n" +
				"pos=<x=  5, y=  5, z= 10>, vel=<x=  0, y=  0, z=  0>\n" +
				"pos=<x=  2, y= -7, z=  3>, vel=<x=  0, y=  0, z=  0>\n" +
				"pos=<x=  9, y= -8, z= -3>, vel=<x=  0, y=  0, z=  0>\n",
				e12.toString());
	}
}