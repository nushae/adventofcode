package com.nushae.aoc.aoc2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise08Test {

	@Test
	void testExample1() {
		Exercise08 ex = new Exercise08("123452789012");
		assertEquals(2, ex.doPart1(3, 2));
	}

	@Test
	void testExample2() {
		Exercise08 ex = new Exercise08("0222112222120000");
		assertEquals(4, ex.doPart1(2, 2));
		assertEquals(" #\n# \n", ex.doPart2(2, 2));
	}
}