package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.StringOutputReceiver;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise09Test {
	@Test
	void testExampleA() {
		IntCodeInterpreter itp = new IntCodeInterpreter();
		StringOutputReceiver otp = new StringOutputReceiver();
		itp.setOutputReceiver(otp);
		itp.loadProgramFromString("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99");
		itp.reRunProgram();

		assertEquals("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99", otp.getOutputs());
	}

	@Test
	void testExampleB() {
		IntCodeInterpreter itp = new IntCodeInterpreter();
		StringOutputReceiver otp = new StringOutputReceiver();
		itp.setOutputReceiver(otp);
		itp.loadProgramFromString("1102,34915192,34915192,7,4,7,99,0");
		itp.reRunProgram();

		assertEquals("1219070632396864", otp.getOutputs());
	}

	@Test
	void testExampleC() {
		IntCodeInterpreter itp = new IntCodeInterpreter();
		StringOutputReceiver otp = new StringOutputReceiver();
		itp.setOutputReceiver(otp);
		itp.loadProgramFromString("104,1125899906842624,99");
		itp.reRunProgram();

		assertEquals("1125899906842624", otp.getOutputs());
	}
}