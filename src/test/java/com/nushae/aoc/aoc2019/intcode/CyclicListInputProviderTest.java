package com.nushae.aoc.aoc2019.intcode;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CyclicListInputProviderTest {

	@Test
	void setValues() {
		CyclicListInputProvider clip = new CyclicListInputProvider(BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3));
		assertEquals(BigInteger.ONE, clip.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(2), clip.nextValue(BigInteger.ZERO));
		clip.setValues(Arrays.asList(BigInteger.valueOf(4), BigInteger.valueOf(5), BigInteger.valueOf(6)));
		assertEquals(BigInteger.valueOf(4), clip.nextValue(BigInteger.ZERO));
	}

	@Test
	void reset() {
		CyclicListInputProvider clip = new CyclicListInputProvider(BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3));
		assertEquals(BigInteger.ONE, clip.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(2), clip.nextValue(BigInteger.ZERO));
		clip.reset();
		assertEquals(BigInteger.ONE, clip.nextValue(BigInteger.ZERO));
	}

	@Test
	void testListCycles() {
		CyclicListInputProvider clip = new CyclicListInputProvider(BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3));
		assertEquals(BigInteger.ONE, clip.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(2), clip.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(3), clip.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.ONE, clip.nextValue(BigInteger.ZERO));
	}
}