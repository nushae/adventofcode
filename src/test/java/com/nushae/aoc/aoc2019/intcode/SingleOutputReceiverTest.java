package com.nushae.aoc.aoc2019.intcode;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SingleOutputReceiverTest {

	@Test
	void testValuePreservations() {
		SingleOutputReceiver sor = new SingleOutputReceiver();
		sor.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(4));
		assertEquals(BigInteger.valueOf(4), sor.getLastOutput());
		assertEquals(BigInteger.valueOf(4), sor.getLastOutput());
		sor.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(5));
		assertEquals(BigInteger.valueOf(5), sor.getLastOutput());
	}

	@Test
	void testNoOutputYet() {
		SingleOutputReceiver sor = new SingleOutputReceiver();
		assertThrows(IllegalStateException.class, sor::getLastOutput);
	}
}