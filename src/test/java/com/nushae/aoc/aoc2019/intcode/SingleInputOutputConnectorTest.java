package com.nushae.aoc.aoc2019.intcode;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SingleInputOutputConnectorTest {

	@Test
	void testValueSequenceMultipleInputsBeforeOutput() {
		SingleInputOutputConnector sioc = new SingleInputOutputConnector(BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3));
		assertEquals(BigInteger.ONE, sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(2), sioc.nextValue(BigInteger.ZERO));
		sioc.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(4));
		assertEquals(BigInteger.valueOf(3), sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(4), sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(4), sioc.nextValue(BigInteger.ZERO));
		sioc.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(5));
		assertEquals(BigInteger.valueOf(5), sioc.nextValue(BigInteger.ZERO));

		sioc = new SingleInputOutputConnector(BigInteger.valueOf(6));
		assertEquals(BigInteger.valueOf(6), sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.ZERO, sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.ZERO, sioc.nextValue(BigInteger.ZERO));
		sioc.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(7));
		assertEquals(BigInteger.valueOf(7), sioc.nextValue(BigInteger.ZERO));
	}

	@Test
	void testValueSequenceOutputBeforeInput() {
		SingleInputOutputConnector sioc = new SingleInputOutputConnector(BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3));
		sioc.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(4));
		assertEquals(BigInteger.ONE, sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(2), sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(3), sioc.nextValue(BigInteger.ZERO));
		assertEquals(BigInteger.valueOf(4), sioc.nextValue(BigInteger.ZERO));
	}

	@Test
	void getLastOutput() {
		SingleInputOutputConnector sioc = new SingleInputOutputConnector(BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3));
		sioc.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(4));
		assertEquals(BigInteger.valueOf(4), sioc.getLastOutput());

		sioc = new SingleInputOutputConnector(BigInteger.valueOf(5));
		assertThrows(IllegalStateException.class, sioc::getLastOutput);
		sioc.acceptOutput(BigInteger.ZERO, BigInteger.valueOf(6));
		assertEquals(BigInteger.valueOf(6), sioc.getLastOutput());
	}
}