package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise23Test {

	@Test
	void testExample1() {
		Exercise23 ex = new Exercise23(3, 8, 9, 1, 2, 5, 4, 6, 7);
		assertEquals("92658374", ex.doPart1(10));

		ex = new Exercise23(3, 8, 9, 1, 2, 5, 4, 6, 7);
		assertEquals(BigInteger.valueOf(149245887792L), ex.doPart2());
	}
}