package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@Test
	void testExample1() {
		Exercise18 ex = new Exercise18("1 + 2 * 3 + 4 * 5 + 6");
		assertEquals(BigInteger.valueOf(71), ex.doPart1());
		assertEquals(BigInteger.valueOf(231), ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise18 ex = new Exercise18("1 + (2 * 3) + (4 * (5 + 6))\n"
				+ "2 * 3 + (4 * 5)\n"
				+ "5 + (8 * 3 + 9 + 3 * 4 * 3)\n"
				+ "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))\n"
				+ "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2");
		assertEquals(BigInteger.valueOf(51+26+437+12240+13632), ex.doPart1());
		assertEquals(BigInteger.valueOf(51+46+1445+669060+23340), ex.doPart2());
	}
}