package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise05Test {

	@Test
	void testToBSPNumber() {
		assertEquals(44, Exercise05.toBSPNumber("B", "F", "FBFBBFF"));
		assertEquals(5, Exercise05.toBSPNumber("R" , "L", "RLR"));

		assertEquals(70, Exercise05.toBSPNumber("B", "F", "BFFFBBF"));
		assertEquals(7, Exercise05.toBSPNumber("R" , "L", "RRR"));

		assertEquals(14, Exercise05.toBSPNumber("B", "F", "FFFBBBF"));
		assertEquals(7, Exercise05.toBSPNumber("R" , "L", "RRR"));

		assertEquals(102, Exercise05.toBSPNumber("B", "F", "BBFFBBF"));
		assertEquals(4, Exercise05.toBSPNumber("R" , "L", "RLL"));
	}

	@Test
	void testExample1() {
		Exercise05 e5 = new Exercise05("FBFBBFFRLR");
		assertEquals(357, e5.getHighestID());
	}

	@Test
	void testExample2() {
		Exercise05 e5 = new Exercise05("BFFFBBFRRR");
		assertEquals(567, e5.getHighestID());
	}

	@Test
	void testExample3() {
		Exercise05 e5 = new Exercise05("FFFBBBFRRR");
		assertEquals(119, e5.getHighestID());
	}

	@Test
	void testExample4() {
		Exercise05 e5 = new Exercise05("BBFFBBFRLL");
		assertEquals(820, e5.getHighestID());
	}

	@Test
	void testFindFirstFreeID() {
		Exercise05 e5 = new Exercise05("FFFBBBFLLL\nFFFBBBFLLR\nFFFBBBFLRR\nFFFBBBFRLL\nFFFBBBFRLR\nFFFBBBFRRR");
		assertEquals(119, e5.getHighestID());
		assertEquals(114, e5.getFirstFreeID());
	}
}