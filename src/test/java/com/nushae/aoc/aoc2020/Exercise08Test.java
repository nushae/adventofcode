package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise08Test {

	@Test
	void testExample1() {
		Exercise08 ex = new Exercise08("nop +0\n" +
				"acc +1\n" +
				"jmp +4\n" +
				"acc +3\n" +
				"jmp -3\n" +
				"acc -99\n" +
				"acc +1\n" +
				"jmp -4\n" +
				"acc +6\n");
		assertEquals(5, ex.executeUntilLoop());
		assertEquals(8, ex.executeWithFix());
	}
}