package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise02Test {

	@Test
	void testExample1() {
		Exercise02 e2 = new Exercise02(
			"1-3 a: abcde\n" +
			"1-3 b: cdefg\n" +
			"2-9 c: ccccccccc\n" +
			"3-5 d: ddabdcefgd"
		);

		assertEquals(3, e2.countValidPasswords());
		assertEquals(2, e2.countValidPasswords2());
	}

}