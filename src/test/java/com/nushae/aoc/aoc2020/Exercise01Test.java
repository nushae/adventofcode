package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise01Test {

	@Test
	void testExample1() {
		Exercise01 e1 = new Exercise01("1721\n" +
				"979\n" +
				"366\n" +
				"299\n" +
				"675\n" +
				"1456");
		// expect 514579 resp 241861950

		assertEquals(514579, e1.doPart1(2020));
		assertEquals(241861950, e1.doPart2(2020));
	}
}