package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	void testExample1() {
		Exercise22 ex = new Exercise22("Player 1:\n" +
				"9\n" +
				"2\n" +
				"6\n" +
				"3\n" +
				"1\n" +
				"\n" +
				"Player 2:\n" +
				"5\n" +
				"8\n" +
				"4\n" +
				"7\n" +
				"10");
		assertEquals(BigInteger.valueOf(-306), ex.doPart1());
		assertEquals(BigInteger.valueOf(-291), ex.doPart2());
	}
}