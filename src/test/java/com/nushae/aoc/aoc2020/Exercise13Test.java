package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise13Test {

	@Test
	void testExample1() {
		Exercise13 ex = new Exercise13("939\n" +
				"7,13,x,x,59,x,31,19");
		assertEquals(295, ex.doPart1());
		assertEquals(BigInteger.valueOf(1068781), ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise13 ex = new Exercise13("0\n17,x,13,19");
		assertEquals(BigInteger.valueOf(3417), ex.doPart2());
	}

	@Test
	void testExample3() {
		Exercise13 ex = new Exercise13("0\n67,7,59,61");
		assertEquals(BigInteger.valueOf(754018), ex.doPart2());
	}

	@Test
	void testExample4() {
		Exercise13 ex = new Exercise13("0\n67,x,7,59,61");
		assertEquals(BigInteger.valueOf(779210), ex.doPart2());
	}

	@Test
	void testExample5() {
		Exercise13 ex = new Exercise13("0\n67,7,x,59,61");
		assertEquals(BigInteger.valueOf(1261476), ex.doPart2());
	}

	@Test
	void testExample6() {
		Exercise13 ex = new Exercise13("0\n1789,37,47,1889");
		assertEquals(BigInteger.valueOf(1202161486), ex.doPart2());
	}
}