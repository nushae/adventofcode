package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {
	@Test
	void testExample1() {
		Exercise03 e3 = new Exercise03(
		"..##.......\n" +
			"#...#...#..\n" +
			".#....#..#.\n" +
			"..#.#...#.#\n" +
			".#...##..#.\n" +
			"..#.##.....\n" +
			".#.#.#....#\n" +
			".#........#\n" +
			"#.##...#...\n" +
			"#...##....#\n" +
			".#..#...#.#"
		);

		assertEquals(7, e3.countTrees(3, 1));
	}
}