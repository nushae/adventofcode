package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise24Test {

	@Test
	void testExample1() {
		Exercise24 ex = new Exercise24("sesenwnenenewseeswwswswwnenewsewsw\n" +
				"neeenesenwnwwswnenewnwwsewnenwseswesw\n" +
				"seswneswswsenwwnwse\n" +
				"nwnwneseeswswnenewneswwnewseswneseene\n" +
				"swweswneswnenwsewnwneneseenw\n" +
				"eesenwseswswnenwswnwnwsewwnwsene\n" +
				"sewnenenenesenwsewnenwwwse\n" +
				"wenwwweseeeweswwwnwwe\n" +
				"wsweesenenewnwwnwsenewsenwwsesesenwne\n" +
				"neeswseenwwswnwswswnw\n" +
				"nenwswwsewswnenenewsenwsenwnesesenew\n" +
				"enewnwewneswsewnwswenweswnenwsenwsw\n" +
				"sweneswneswneneenwnewenewwneswswnese\n" +
				"swwesenesewenwneswnwwneseswwne\n" +
				"enesenwswwswneneswsenwnewswseenwsese\n" +
				"wnwnesenesenenwwnenwsewesewsesesew\n" +
				"nenewswnwewswnenesenwnesewesw\n" +
				"eneswnwswnwsenenwnwnwwseeswneewsenese\n" +
				"neswnwewnwnwseenwseesewsenwsweewe\n" +
				"wseweeenwnesenwwwswnew");
		assertEquals(10, ex.doPart1());
		assertEquals( 2208, ex.doPart2(100));
	}

	@Test
	void testExample2() {
		Exercise24 ex = new Exercise24("e\n" +
				"ee\n" +
				"eee");
		assertEquals(3, ex.doPart1());
		assertEquals(7, ex.doPart2(1));

	}
}