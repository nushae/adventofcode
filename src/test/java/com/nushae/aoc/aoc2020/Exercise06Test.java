package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise06Test {

	@Test
	void testExample1() {
		Exercise06 ex = new Exercise06("abcx\n" +
				"abcy\n" +
				"abcz");

		assertEquals(6, ex.countQuestionsUnion());
		assertEquals(3, ex.countQuestionsIntersection());
	}

	@Test
	void testExample2() {
		Exercise06 ex = new Exercise06("abc\n\n" +
				"a\nb\nc\n\n" +
				"ab\nac\n\n" +
				"a\na\na\na\n\n" +
				"b");

		assertEquals(11, ex.countQuestionsUnion());
		assertEquals(6, ex.countQuestionsIntersection());
	}
}