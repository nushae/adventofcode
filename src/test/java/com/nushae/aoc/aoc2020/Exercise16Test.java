package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	void testExample1() {
		Exercise16 ex = new Exercise16("class: 1-3 or 5-7\n" +
				"departure: 6-11 or 33-44\n" +
				"departure2: 13-40 or 45-50\n" +
				"\n" +
				"your ticket:\n" +
				"7,1,14\n" +
				"\n" +
				"nearby tickets:\n" +
				"7,3,47\n" +
				"40,4,50\n" +
				"55,2,20\n" +
				"38,6,12");
		assertEquals(71, ex.doPart1());
		assertEquals(98, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise16 ex = new Exercise16("class: 0-1 or 4-19\n" +
				"departure: 0-5 or 8-19\n" +
				"seat: 0-13 or 16-19\n" +
				"\n" +
				"your ticket:\n" +
				"11,12,13\n" +
				"\n" +
				"nearby tickets:\n" +
				"3,9,18\n" +
				"15,1,5\n" +
				"5,14,9");
		assertEquals(0, ex.doPart1());
		assertEquals(11, ex.doPart2());
	}
}