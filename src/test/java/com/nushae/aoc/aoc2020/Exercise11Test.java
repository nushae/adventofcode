package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise11Test {

	@Test
	void testExample1() {
		Exercise11 ex = new Exercise11("L.LL.LL.LL\n" +
				"LLLLLLL.LL\n" +
				"L.L.L..L..\n" +
				"LLLL.LL.LL\n" +
				"L.LL.LL.LL\n" +
				"L.LLLLL.LL\n" +
				"..L.L.....\n" +
				"LLLLLLLLLL\n" +
				"L.LLLLLL.L\n" +
				"L.LLLLL.LL");
		assertEquals(37, ex.findOccupiedAfterStabilized(4, false));
		// reset hack:
		ex = new Exercise11("L.LL.LL.LL\n" +
				"LLLLLLL.LL\n" +
				"L.L.L..L..\n" +
				"LLLL.LL.LL\n" +
				"L.LL.LL.LL\n" +
				"L.LLLLL.LL\n" +
				"..L.L.....\n" +
				"LLLLLLLLLL\n" +
				"L.LLLLLL.L\n" +
				"L.LLLLL.LL");
		assertEquals(26, ex.findOccupiedAfterStabilized(5, true));
	}

	@Test
	void testUpdateNeighboursLOS() {
		Exercise11 ex = new Exercise11(".......#.\n" +
				"...#.....\n" +
				".#.......\n" +
				".........\n" +
				"..#L....#\n" +
				"....#....\n" +
				".........\n" +
				"#........\n" +
				"...#.....");
		ex.findOccupiedAfterStabilized(5, true);
		HashMap<Point, Integer> tets = new HashMap<>();
		ex.updateNeighbours(new Point(3, 4), tets);
		assertEquals(8, tets.size());

		ex = new Exercise11(".............\n" +
				".L.L.#.#.#.#.\n" +
				".............");
		ex.findOccupiedAfterStabilized(5, true);
		tets = new HashMap<>();
		ex.updateNeighbours(new Point(1, 1), tets);
		assertEquals(1, tets.size());

		ex = new Exercise11(".##.##.\n" +
				"#.#.#.#\n" +
				"##...##\n" +
				"...L...\n" +
				"##...##\n" +
				"#.#.#.#\n" +
				".##.##.");
		ex.findOccupiedAfterStabilized(5, true);
		tets = new HashMap<>();
		ex.updateNeighbours(new Point(3, 3), tets);
		assertEquals(0, tets.size());
	}
}