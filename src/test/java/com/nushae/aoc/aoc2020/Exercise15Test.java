package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testExample1() {
		Exercise15 ex = new Exercise15("0,3,6");
		long start = System.currentTimeMillis();
		assertEquals(436, ex.findNthNumberCalled(2020));
		assertEquals(175594, ex.findNthNumberCalled(30000000));
	}

	@Test
	void testExample2() {
		Exercise15 ex = new Exercise15("1,3,2");
		assertEquals(1, ex.findNthNumberCalled(2020));
		assertEquals(2578, ex.findNthNumberCalled(30000000));
	}

	@Test
	void testExample3() {
		Exercise15 ex = new Exercise15("2,1,3");
		assertEquals(10, ex.findNthNumberCalled(2020));
		assertEquals(3544142, ex.findNthNumberCalled(30000000));
	}

	@Test
	void testExample4() {
		Exercise15 ex = new Exercise15("1,2,3");
		assertEquals(27, ex.findNthNumberCalled(2020));
		assertEquals(261214, ex.findNthNumberCalled(30000000));
	}

	@Test
	void testExample5() {
		Exercise15 ex = new Exercise15("2,3,1");
		assertEquals(78, ex.findNthNumberCalled(2020));
		assertEquals(6895259, ex.findNthNumberCalled(30000000));
	}

	@Test
	void testExample6() {
		Exercise15 ex = new Exercise15("3,2,1");
		assertEquals(438, ex.findNthNumberCalled(2020));
		assertEquals(18, ex.findNthNumberCalled(30000000));
	}

	@Test
	void testExample7() {
		Exercise15 ex = new Exercise15("3,1,2");
		assertEquals(1836, ex.findNthNumberCalled(2020));
		assertEquals(362, ex.findNthNumberCalled(30000000));
	}
}