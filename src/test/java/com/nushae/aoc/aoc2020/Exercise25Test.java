package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise25Test {

	@Test
	void testExample1() {
		Exercise25 ex = new Exercise25(17807724, 5764801);
		assertEquals(14897079, ex.doPart1());
	}
}