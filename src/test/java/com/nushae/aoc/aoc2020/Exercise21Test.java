package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise21Test {

	@Test
	void testExample1() {
		Exercise21 ex = new Exercise21("mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\n" +
				"trh fvjkl sbzzf mxmxvkd (contains dairy)\n" +
				"sqjhc fvjkl (contains soy)\n" +
				"sqjhc mxmxvkd sbzzf (contains fish)");
		assertEquals(5, ex.doPart1());
		assertEquals("mxmxvkd,sqjhc,fvjkl", ex.doPart2());
	}
}