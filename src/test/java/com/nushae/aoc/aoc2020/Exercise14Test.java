package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	void testExample1() {
		Exercise14 ex = new Exercise14("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\n" +
				"mem[8] = 11\n" +
				"mem[7] = 101\n" +
				"mem[8] = 0");
		assertEquals(BigInteger.valueOf(165), ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise14 ex = new Exercise14("mask = 000000000000000000000000000000X1001X\n" +
				"mem[42] = 100\n" +
				"mask = 00000000000000000000000000000000X0XX\n" +
				"mem[26] = 1");
		assertEquals(BigInteger.valueOf(208), ex.doPart2());
	}
}