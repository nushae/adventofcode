package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise12Test {

	@Test
	void testExample1() {
		Exercise12 ex = new Exercise12("F10\n" +
				"N3\n" +
				"F7\n" +
				"R90\n" +
				"F11");
		assertEquals(25, ex.moveBoat(false));
	}

	@Test
	void testExample2() {
		Exercise12 ex = new Exercise12("F10\n" +
				"N3\n" +
				"F7\n" +
				"R90\n" +
				"F11");
		assertEquals(286, ex.moveBoat(true));
	}
}