package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	void testExample1() {
		Exercise17 ex = new Exercise17(".#.\n" +
				"..#\n" +
				"###");
		assertEquals(5, ex.doGameOfLife(2));
		assertEquals(112, ex.doGameOfLife(3));
		assertEquals(848, ex.doGameOfLife(4));
	}

	@Test
	void testExample2() {
		Exercise17 ex = new Exercise17("###");
		assertEquals(3, ex.doGameOfLife(2));
		assertEquals(112, ex.doGameOfLife(3));
		assertEquals(312, ex.doGameOfLife(4));
	}
}