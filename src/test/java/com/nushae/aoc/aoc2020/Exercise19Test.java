package com.nushae.aoc.aoc2020;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise19Test {

	@Test
	void testExample1() {
		Exercise19 ex = new Exercise19("0: 4 1 5\n" +
				"1: 2 3 | 3 2\n" +
				"2: 4 4 | 5 5\n" +
				"3: 4 5 | 5 4\n" +
				"4: \"a\"\n" +
				"5: \"b\"\n" +
				"\n" +
				"ababbb\n" +
				"bababa\n" +
				"abbbab\n" +
				"aaabbb\n" +
				"aaaabbb");
		assertEquals(2, ex.doPart1(0));
	}
}