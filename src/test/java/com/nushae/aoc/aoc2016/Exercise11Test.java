package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.D11State;
import com.nushae.aoc.domain.SearchNode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Exercise11Test {

	@ParameterizedTest
	@MethodSource("provideArgumentsForNextMoves")
	void testNextMovesSmart(String startstate, Set<String> expected) {
		System.out.println(startstate);
		D11State poep = new D11State(startstate);
		System.out.println(poep);
		List<String> resRep = new ArrayList<>();
		Map<SearchNode, Long> result = poep.getAdjacentNodesSmart();
		for (SearchNode res : result.keySet()) {
			resRep.add(((D11State) res).rep());
		}
		assertEquals(expected.size(), result.size());
		System.out.println("Expected values");
		for (String exp : expected) {
			System.out.println((resRep.contains(exp) ? " " : "*") + exp);
		}
		System.out.println("Actual values");
		for (String res : resRep) {
			System.out.println((expected.contains(res) ? " " : "*") + res);
		}
		for (String res : resRep) {
			assertTrue(expected.contains(res), res + " not among expected results");
		}
	}

	@ParameterizedTest
	@MethodSource("provideArgumentsForNextMoves")
	void testNextMoves(String startstate, Set<String> expected) {
		System.out.println(startstate);
		D11State poep = new D11State(startstate);
		System.out.println(poep);
		List<String> resRep = new ArrayList<>();
		Map<SearchNode, Long> result = poep.getAdjacentNodesSmart();
		for(SearchNode res : result.keySet()) {
			resRep.add(((D11State) res).rep());
		}
		assertEquals(expected.size(), result.size());
		System.out.println("Expected values");
		for (String exp : expected) {
			System.out.println((resRep.contains(exp) ? " " : "*") + exp);
		}
		System.out.println("Actual values");
		for (String res : resRep) {
			System.out.println((expected.contains(res) ? " " : "*") + res);
		}
		for (String res : resRep) {
			assertTrue(expected.contains(res), res + " not among expected results");
		}
	}

	private static Stream<Arguments> provideArgumentsForNextMoves() {
		return Stream.of(
				Arguments.of("1-1(Ppr)-2(GcoGcuGplGru)-3(McoMcuMplMru)-4()", new TreeSet<>(Arrays.asList(
						"2-1()-2(GcoGcuGplGruPpr)-3(McoMcuMplMru)-4()",
						"2-1(Mpr)-2(GcoGcuGplGprGru)-3(McoMcuMplMru)-4()"))),
				Arguments.of("2-1()-2(GcoGcuGplGruPpr)-3(McoMcuMplMru)-4()", new TreeSet<>(Arrays.asList(
						"1-1(Gcu)-2(GcoGplGruPpr)-3(McoMcuMplMru)-4()",
						"1-1(Mpr)-2(GcoGcuGplGprGru)-3(McoMcuMplMru)-4()",
						"1-1(Gru)-2(GcoGcuGplPpr)-3(McoMcuMplMru)-4()",
						"1-1(Ppr)-2(GcoGcuGplGru)-3(McoMcuMplMru)-4()",
						"1-1(GcoGru)-2(GcuGplPpr)-3(McoMcuMplMru)-4()",
						"1-1(Gco)-2(GcuGplGruPpr)-3(McoMcuMplMru)-4()",
						"1-1(GplGru)-2(GcoGcuPpr)-3(McoMcuMplMru)-4()",
						"1-1(GcuGru)-2(GcoGplPpr)-3(McoMcuMplMru)-4()",
						"1-1(GcoGpl)-2(GcuGruPpr)-3(McoMcuMplMru)-4()",
						"1-1(Gpl)-2(GcoGcuGruPpr)-3(McoMcuMplMru)-4()",
						"3-1()-2(GcoGcuGplGprGru)-3(McoMcuMplMprMru)-4()",
						"1-1(GcuGpl)-2(GcoGruPpr)-3(McoMcuMplMru)-4()",
						"1-1(GcoGcu)-2(GplGruPpr)-3(McoMcuMplMru)-4()"))),
				Arguments.of("2-1(Mli)-2(Phy)-3(Gli)-4()", new TreeSet<>(Arrays.asList(
						"1-1(MhyMli)-2(Ghy)-3(Gli)-4()",
						"3-1(Mli)-2()-3(GliPhy)-4()",
						"3-1(Mli)-2(Mhy)-3(GhyGli)-4()"))),
				Arguments.of("3-1(Mli)-2()-3(GliPhy)-4()", new TreeSet<>(Arrays.asList(
						"2-1(Mli)-2(Mhy)-3(GhyGli)-4()",
						"2-1(Mli)-2(Phy)-3(Gli)-4()",
						"2-1(Mli)-2(GhyGli)-3(Mhy)-4()",
						"2-1(Mli)-2(Gli)-3(Phy)-4()",
						"4-1(Mli)-2()-3(Gli)-4(Phy)",
						"4-1(Mli)-2()-3(Phy)-4(Gli)",
						"4-1(Mli)-2()-3(GhyGli)-4(Mhy)",
						"4-1(Mli)-2()-3(Mhy)-4(GhyGli)"))),
				Arguments.of("3-1()-2(GcoGcuGplGprGru)-3(McoMcuMplMprMru)-4()", new TreeSet<>(Arrays.asList(
						"2-1()-2(GcuGplGprGruPco)-3(McuMplMprMru)-4()",
						"2-1()-2(GcoGplGprGruPcu)-3(McoMplMprMru)-4()",
						"2-1()-2(GcoGcuGprGruPpl)-3(McoMcuMprMru)-4()",
						"2-1()-2(GcoGcuGplGruPpr)-3(McoMcuMplMru)-4()",
						"2-1()-2(GcoGcuGplGprPru)-3(McoMcuMplMpr)-4()",
						"2-1()-2(GplGprGruPcoPcu)-3(MplMprMru)-4()",
						"2-1()-2(GcuGprGruPcoPpl)-3(McuMprMru)-4()",
						"2-1()-2(GcuGplGruPcoPpr)-3(McuMplMru)-4()",
						"2-1()-2(GcuGplGprPcoPru)-3(McuMplMpr)-4()",
						"2-1()-2(GcoGprGruPcuPpl)-3(McoMprMru)-4()",
						"2-1()-2(GcoGplGruPcuPpr)-3(McoMplMru)-4()",
						"2-1()-2(GcoGplGprPcuPru)-3(McoMplMpr)-4()",
						"2-1()-2(GcoGcuGruPplPpr)-3(McoMcuMru)-4()",
						"2-1()-2(GcoGcuGprPplPru)-3(McoMcuMpr)-4()",
						"2-1()-2(GcoGcuGplPprPru)-3(McoMcuMpl)-4()",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMcuMpl)-4(MprMru)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMcuMpr)-4(MplMru)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMcuMru)-4(MplMpr)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMplMpr)-4(McuMru)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMplMru)-4(McuMpr)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMprMru)-4(McuMpl)",
						"4-1()-2(GcoGcuGplGprGru)-3(McuMplMpr)-4(McoMru)",
						"4-1()-2(GcoGcuGplGprGru)-3(McuMplMru)-4(McoMpr)",
						"4-1()-2(GcoGcuGplGprGru)-3(McuMprMru)-4(McoMpl)",
						"4-1()-2(GcoGcuGplGprGru)-3(MplMprMru)-4(McoMcu)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMcuMplMpr)-4(Mru)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMcuMplMru)-4(Mpr)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMcuMprMru)-4(Mpl)",
						"4-1()-2(GcoGcuGplGprGru)-3(McoMplMprMru)-4(Mcu)",
						"4-1()-2(GcoGcuGplGprGru)-3(McuMplMprMru)-4(Mco)")))
		);
	}

	@Test
	void testExample1() {
		Exercise11 ex = new Exercise11("The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.\n" +
				"The second floor contains a hydrogen generator.\n" +
				"The third floor contains a lithium generator.\n" +
				"The fourth floor contains nothing relevant.");
		assertEquals(11, ex.doPart1(false));
	}
}