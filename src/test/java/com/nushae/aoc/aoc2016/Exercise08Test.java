package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise08Test {

	@Test
	public void testPart1() {
		Exercise08 ex = new Exercise08("rect 49x5\n" +
				"rotate column x=0 by 2");
		assertEquals(245, ex.doPart1());
	}
}