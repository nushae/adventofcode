package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise01Test {
	@Test
	public void testPart1() {
		Exercise01 ex = new Exercise01("R8, R4, R4, R8");
		assertEquals(8, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise01 ex = new Exercise01("R8, R4, R4, R8");
		assertEquals(4, ex.doPart2());
	}

}