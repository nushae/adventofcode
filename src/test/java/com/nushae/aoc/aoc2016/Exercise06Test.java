package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise06Test {

	@Test
	public void testPart1() {
		Exercise06 ex = new Exercise06("eedadn\n" +
				"drvtee\n" +
				"eandsr\n" +
				"raavrd\n" +
				"atevrs\n" +
				"tsrnev\n" +
				"sdttsa\n" +
				"rasrtv\n" +
				"nssdts\n" +
				"ntnada\n" +
				"svetve\n" +
				"tesnvt\n" +
				"vntsnd\n" +
				"vrdear\n" +
				"dvrsen\n" +
				"enarar");
		assertEquals("easter", ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise06 ex = new Exercise06("eedadn\n" +
				"drvtee\n" +
				"eandsr\n" +
				"raavrd\n" +
				"atevrs\n" +
				"tsrnev\n" +
				"sdttsa\n" +
				"rasrtv\n" +
				"nssdts\n" +
				"ntnada\n" +
				"svetve\n" +
				"tesnvt\n" +
				"vntsnd\n" +
				"vrdear\n" +
				"dvrsen\n" +
				"enarar");
		assertEquals("advent", ex.doPart2());
	}

}