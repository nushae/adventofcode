package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	void testExample1() {
		Exercise16 ex = new Exercise16();
		assertEquals("01100", ex.doPart1("10000", 20));
	}

	@Test
	void testChecksumEfficient() {
		Exercise16 ex = new Exercise16();
		assertEquals("100101100", ex.checksumEfficient("001010001011110101"));
	}

}