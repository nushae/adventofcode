package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise02Test {

	@Test
	public void testPart1() {
		Exercise02 ex = new Exercise02("ULL\n" +
				"RRDDD\n" +
				"LURDL\n" +
				"UUUUD");
		assertEquals("1985", ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise02 ex = new Exercise02("ULL\n" +
				"RRDDD\n" +
				"LURDL\n" +
				"UUUUD");
		assertEquals("5DB3", ex.doPart2());
	}
}