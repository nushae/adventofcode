package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise07Test {

	@Test
	public void testPart1() {
		Exercise07 ex = new Exercise07("abba[mnop]qrst\n" + // OK (abba outside square brackets).
				"abcd[bddb]xyyx\n" +                             // NOK (bddb is within square brackets, even though xyyx is outside square brackets).
				"aaaa[qwer]tyui\n" +                             // NOK (aaaa is invalid; the interior characters must be different).
				"ioxxoj[asdfgh]zxcvbn\n" +                       // OK (oxxo is outside square brackets, even though it's within a larger string).
				"wysextplwqpvipxdv[srzvtwbfzqtspxnethm]syqbzgtboxxzpwr[kljvjjkjyojzrstfgrw]obdhcczonzvbfby[svotajtpttohxsh]cooktbyumlpxostt");
		assertEquals(2, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise07 ex = new Exercise07("aba[bab]xyz\n" + // OK (aba outside square brackets with corresponding bab within square brackets).
				"xyx[xyx]xyx\n" +                             // NOK (xyx, but no corresponding yxy).
				"aaa[kek]eke\n" +                             // OK (eke in supernet with corresponding kek in hypernet; the aaa sequence is not related, because the interior character must be different).
				"zazbz[bzb]cdb"                               // OK (zaz has no corresponding aza, but zbz has a corresponding bzb, even though zaz and zbz overlap).
		);
		assertEquals(3, ex.doPart2());
	}
}