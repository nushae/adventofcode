package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	public void testPart1() {
		Exercise10 ex = new Exercise10("value 5 goes to bot 2\n" +
				"bot 2 gives low to bot 1 and high to bot 0\n" +
				"value 3 goes to bot 1\n" +
				"bot 1 gives low to output 1 and high to bot 0\n" +
				"bot 0 gives low to output 2 and high to output 0\n" +
				"value 2 goes to bot 2");
		assertEquals(2, ex.doPart1(2, 5));
	}

	@Test
	public void testPart2() {
		Exercise10 ex = new Exercise10("value 5 goes to bot 2\n" +
				"bot 2 gives low to bot 1 and high to bot 0\n" +
				"value 3 goes to bot 1\n" +
				"bot 1 gives low to output 1 and high to bot 0\n" +
				"bot 0 gives low to output 2 and high to output 0\n" +
				"value 2 goes to bot 2");
		assertEquals(5, ex.doPart2(0));
		assertEquals(2, ex.doPart2(1));
		assertEquals(3, ex.doPart2(2));
		assertEquals(6, ex.doPart2(1, 2, 4));
	}
}