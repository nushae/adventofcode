package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@Test
	public void testPart1() {
		Exercise03 ex = new Exercise03("101 301 501\n" +
				"102 302 502\n" +
				"103 303 503\n" +
				"201 401 601\n" +
				"202 402 602\n" +
				"203 403 603");
		assertEquals(3, ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise03 ex = new Exercise03("3 301 501\n" +
				"4 302 502\n" +
				"15 303 503\n" +
				"201 401 6\n" +
				"202 402 8\n" +
				"203 403 100");
		assertEquals(4, ex.doPart2());
	}
}