package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise05Test {

	@Test
	public void testPart1() {
		Exercise05 ex = new Exercise05("abc");
		assertEquals("18f47a30", ex.doPart1());
	}

	@Test
	public void testPart2() {
		Exercise05 ex = new Exercise05("abc");
		assertEquals("05ace8e3", ex.doPart2());
	}
}