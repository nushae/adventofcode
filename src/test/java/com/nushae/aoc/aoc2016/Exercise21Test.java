package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise21Test {

	@Test
	void testExample1() {
		Exercise21 ex = new Exercise21("swap position 4 with position 0\n" +
				"swap letter d with letter b\n" +
				"reverse positions 0 through 4\n" +
				"rotate left 1 step\n" +
				"move position 1 to position 4\n" +
				"move position 3 to position 0\n" +
				"rotate based on position of letter b\n" +
				"rotate based on position of letter d");

		assertEquals("decab", ex.doPart1("abcde", false));
		// note that the unscrambling only works for 8-letter sequences, so can't be tested here
	}
}