package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise04Test {

	@Test
	public void testPart1() {
		Exercise04 ex = new Exercise04("aaaaa-bbb-z-y-x-123[abxyz]\n" +
				"a-b-c-d-e-f-g-h-987[abcde]\n" +
				"not-a-real-room-404[oarel]\n" +
				"totally-real-room-200[decoy]");
		assertEquals(1514, ex.doPart1());
	}
}