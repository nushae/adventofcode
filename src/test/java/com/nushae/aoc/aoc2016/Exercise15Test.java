package com.nushae.aoc.aoc2016;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testExample1() {
		Exercise15 ex = new Exercise15("Disc #1 has 5 positions; at time=0, it is at position 4.\n" +
				"Disc #2 has 2 positions; at time=0, it is at position 1.");
		assertEquals(5, ex.doPart1());
	}
}