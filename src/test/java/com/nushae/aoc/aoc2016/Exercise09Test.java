package com.nushae.aoc.aoc2016;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise09Test {

	@ParameterizedTest
	@MethodSource("provideArgumentsForPart1")
	public void testPart1(String decompress, int expectedLength) {
		Exercise09 ex = new Exercise09(decompress);
		assertEquals(expectedLength, ex.doPart1());
	}
	private static Stream<Arguments> provideArgumentsForPart1() {
		return Stream.of(
				Arguments.of("ADVENT", 6),
				Arguments.of("A(1x5)BC", 7),
				Arguments.of("(3x3)XYZ", 9),
				Arguments.of("A(2x2)BCD(2x2)EFG", 11),
				Arguments.of("(6x1)(1x3)A", 6),
				Arguments.of("X(8x2)(3x3)ABCY", 18)
		);
	}

	@ParameterizedTest
	@MethodSource("provideArgumentsForPart2")
	public void testPart2(String decompress, int expectedLength) {
		Exercise09 ex = new Exercise09(decompress);
		assertEquals(expectedLength, ex.doPart2());
	}
	private static Stream<Arguments> provideArgumentsForPart2() {
		return Stream.of(
				Arguments.of("(3x3)XYZ", 9),
				Arguments.of("X(8x2)(3x3)ABCY", 20),
				Arguments.of("(27x12)(20x12)(13x14)(7x10)(1x12)A", 241920),
				Arguments.of("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN", 445)
		);
	}
}