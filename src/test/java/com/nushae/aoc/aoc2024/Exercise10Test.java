package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	void testExample1() {
		Exercise10 ex = new Exercise10("89010123\n" +
				"78121874\n" +
				"87430965\n" +
				"96549874\n" +
				"45678903\n" +
				"32019012\n" +
				"01329801\n" +
				"10456732");
		ex.processInput();
		assertEquals(36, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise10 ex = new Exercise10("89010123\n" +
				"78121874\n" +
				"87430965\n" +
				"96549874\n" +
				"45678903\n" +
				"32019012\n" +
				"01329801\n" +
				"10456732");
		ex.processInput();
		assertEquals(81, ex.doPart2());
	}
}
