package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise20Test {

	@Test
	void testCalculateCheats() {
		Exercise20 ex = new Exercise20("###############\n" +
				"#...#...#.....#\n" +
				"#.#.#.#.#.###.#\n" +
				"#S#...#.#.#...#\n" +
				"#######.#.#.###\n" +
				"#######.#.#...#\n" +
				"#######.#.###.#\n" +
				"###..E#...#...#\n" +
				"###.#######.###\n" +
				"#...###...#...#\n" +
				"#.#####.#.###.#\n" +
				"#.#...#.#.#...#\n" +
				"#.#.#.#.#.#.###\n" +
				"#...#...#...###\n" +
				"###############");
		ex.processInput();
		assertEquals(44, ex.calculateCheats(2, 2));
		assertEquals(30, ex.calculateCheats(4, 2));
		assertEquals(16, ex.calculateCheats(6, 2));
		assertEquals(14, ex.calculateCheats(8, 2));
		assertEquals(10, ex.calculateCheats(10, 2));
		assertEquals(8, ex.calculateCheats(12, 2));
		assertEquals(5, ex.calculateCheats(20, 2));
		assertEquals(4, ex.calculateCheats(36, 2));
		assertEquals(3, ex.calculateCheats(38, 2));
		assertEquals(2, ex.calculateCheats(40, 2));
		assertEquals(1, ex.calculateCheats(64, 2));

		assertEquals(285, ex.calculateCheats(50, 20));
		assertEquals(253, ex.calculateCheats(52, 20));
		assertEquals(222, ex.calculateCheats(54, 20));
		assertEquals(193, ex.calculateCheats(56, 20));
		assertEquals(154, ex.calculateCheats(58, 20));
		assertEquals(129, ex.calculateCheats(60, 20));
		assertEquals(106, ex.calculateCheats(62, 20));
		assertEquals(86, ex.calculateCheats(64, 20));
		assertEquals(67, ex.calculateCheats(66, 20));
		assertEquals(55, ex.calculateCheats(68, 20));
		assertEquals(41, ex.calculateCheats(70, 20));
		assertEquals(29, ex.calculateCheats(72, 20));
		assertEquals(7, ex.calculateCheats(74, 20));
		assertEquals(3, ex.calculateCheats(76, 20));
	}
}
