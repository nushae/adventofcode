package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise08Test {

	@Test
	void testExample1() {
		Exercise08 ex = new Exercise08("............\n" +
				"........0...\n" +
				".....0......\n" +
				".......0....\n" +
				"....0.......\n" +
				"......A.....\n" +
				"............\n" +
				"............\n" +
				"........A...\n" +
				".........A..\n" +
				"............\n" +
				"............");
		ex.processInput();
		assertEquals(14, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise08 ex = new Exercise08("............\n" +
				"........0...\n" +
				".....0......\n" +
				".......0....\n" +
				"....0.......\n" +
				"......A.....\n" +
				"............\n" +
				"............\n" +
				"........A...\n" +
				".........A..\n" +
				"............\n" +
				"............");
		ex.processInput();
		assertEquals(34, ex.doPart2());
	}
}