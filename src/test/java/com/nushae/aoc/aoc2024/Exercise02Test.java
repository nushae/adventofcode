package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise02Test {

	@Test
	void testExample1() {
		Exercise02 ex = new Exercise02("7 6 4 2 1\n" +
				"1 2 7 8 9\n" +
				"9 7 6 2 1\n" +
				"1 3 2 4 5\n" +
				"8 6 4 4 1\n" +
				"1 3 6 7 9");
		ex.processInput();
		assertEquals(2, ex.doPart1());
		assertEquals(4, ex.doPart2());
	}

	@Test
	void testEdgeCases1() {
		Exercise02 ex = new Exercise02("20 17 18 21 23 25\n" + // ok, ignore first
				"17 18 21 23 25 22"                                  // ok, ignore last
		);
		ex.processInput();
		assertEquals(0, ex.doPart1());
		assertEquals(2, ex.doPart2());
	}

	@Test
	void testEdgeCases2() {
		Exercise02 ex = new Exercise02("17 17 18 21 23 25\n" + // ok, ignore first or second
				"17 18 21 23 25 25"                                  // ok, ignore penultimate or last
		);
		ex.processInput();
		assertEquals(0, ex.doPart1());
		assertEquals(2, ex.doPart2());
	}

	@Test
	void testEdgeCases3() {
		Exercise02 ex = new Exercise02("95 93 91 89 87\n" + // ok
				"1 3 5 7 9"                                       // ok
		);
		ex.processInput();
		assertEquals(2, ex.doPart2Smart());
	}
}