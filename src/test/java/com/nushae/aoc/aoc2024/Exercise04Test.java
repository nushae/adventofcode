package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise04Test {

	@Test
	void testExample1() {
		Exercise04 ex = new Exercise04("MMMSXXMASM\n" +
				"MSAMXMSMSA\n" +
				"AMXSXMAAMM\n" +
				"MSAMASMSMX\n" +
				"XMASAMXAMM\n" +
				"XXAMMXXAMA\n" +
				"SMSMSASXSS\n" +
				"SAXAMASAAA\n" +
				"MAMMMXMMMM\n" +
				"MXMXAXMASX");
		ex.processInput();
		assertEquals(18, ex.doPart1());
		assertEquals(9, ex.doPart2());
	}
}