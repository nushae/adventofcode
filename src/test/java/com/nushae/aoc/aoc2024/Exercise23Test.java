package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise23Test {

	@Test
	void testExample1() {
		Exercise23 ex = new Exercise23("kh-tc\n" +
				"qp-kh\n" +
				"de-cg\n" +
				"ka-co\n" +
				"yn-aq\n" +
				"qp-ub\n" +
				"cg-tb\n" +
				"vc-aq\n" +
				"tb-ka\n" +
				"wh-tc\n" +
				"yn-cg\n" +
				"kh-ub\n" +
				"ta-co\n" +
				"de-co\n" +
				"tc-td\n" +
				"tb-wq\n" +
				"wh-td\n" +
				"ta-ka\n" +
				"td-qp\n" +
				"aq-cg\n" +
				"wq-ub\n" +
				"ub-vc\n" +
				"de-ta\n" +
				"wq-aq\n" +
				"wq-vc\n" +
				"wh-yn\n" +
				"ka-de\n" +
				"kh-ta\n" +
				"co-tc\n" +
				"wh-qp\n" +
				"tb-vc\n" +
				"td-yn");
		ex.processInput();
		assertEquals(7, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise23 ex = new Exercise23("kh-tc\n" +
				"qp-kh\n" +
				"de-cg\n" +
				"ka-co\n" +
				"yn-aq\n" +
				"qp-ub\n" +
				"cg-tb\n" +
				"vc-aq\n" +
				"tb-ka\n" +
				"wh-tc\n" +
				"yn-cg\n" +
				"kh-ub\n" +
				"ta-co\n" +
				"de-co\n" +
				"tc-td\n" +
				"tb-wq\n" +
				"wh-td\n" +
				"ta-ka\n" +
				"td-qp\n" +
				"aq-cg\n" +
				"wq-ub\n" +
				"ub-vc\n" +
				"de-ta\n" +
				"wq-aq\n" +
				"wq-vc\n" +
				"wh-yn\n" +
				"ka-de\n" +
				"kh-ta\n" +
				"co-tc\n" +
				"wh-qp\n" +
				"tb-vc\n" +
				"td-yn");
		ex.processInput();
		assertEquals("", ex.doPart2());
	}

	@Test
	void testExample2Small() {
		Exercise23 ex = new Exercise23("kh-tc\n" +
				"tc-ab\n" +
				"de-cg");
		ex.processInput();
		assertEquals("abkhtc", ex.doPart2());
	}

}
