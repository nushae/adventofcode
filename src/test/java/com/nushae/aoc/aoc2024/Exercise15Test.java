package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testExample1() {
		Exercise15 ex = new Exercise15("########\n" +
				"#..O.O.#\n" +
				"##@.O..#\n" +
				"#...O..#\n" +
				"#.#.O..#\n" +
				"#...O..#\n" +
				"#......#\n" +
				"########\n" +
				"\n" +
				"<^^>>>vv<v>>v<<");
		ex.processInput();
		assertEquals(2028, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise15 ex = new Exercise15("##########\n" +
				"#..O..O.O#\n" +
				"#......O.#\n" +
				"#.OO..O.O#\n" +
				"#..O@..O.#\n" +
				"#O#..O...#\n" +
				"#O..O..O.#\n" +
				"#.OO.O.OO#\n" +
				"#....O...#\n" +
				"##########\n" +
				"\n" +
				"<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^" +
				"vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v" +
				"><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<" +
				"<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^" +
				"^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><" +
				"^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^" +
				">^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^" +
				"<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>" +
				"^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>" +
				"v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^");
		ex.processInput();
		assertEquals(10092, ex.doPart1());
		assertEquals(9021, ex.doPart2());
	}

	@Test
	void testExample3() {
		Exercise15 ex = new Exercise15("#######\n" +
				"#...#.#\n" +
				"#.....#\n" +
				"#..OO@#\n" +
				"#..O..#\n" +
				"#.....#\n" +
				"#######\n" +
				"\n" +
				"<vv<<^^<<^^");
		ex.processInput();
		assertEquals(105 + 207 + 306, ex.doPart2());
	}
}
