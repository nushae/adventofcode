package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise21Test {

	@Test
	void testExample1() {
		Exercise21 ex = new Exercise21("029A\n" +
				"980A\n" +
				"179A\n" +
				"456A\n" +
				"379A");
		assertEquals(126384, ex.doPart1());
	}
}
