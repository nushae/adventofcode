package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	void testExample1() {
		Exercise14 ex = new Exercise14("p=0,4 v=3,-3\n" +
				"p=6,3 v=-1,-3\n" +
				"p=10,3 v=-1,2\n" +
				"p=2,0 v=2,-1\n" +
				"p=0,0 v=1,3\n" +
				"p=3,0 v=-2,-2\n" +
				"p=7,6 v=-1,-3\n" +
				"p=3,0 v=-1,-2\n" +
				"p=9,3 v=2,3\n" +
				"p=7,3 v=-1,2\n" +
				"p=2,4 v=2,-3\n" +
				"p=9,5 v=-3,-3");
		ex.processInputWithPatterns();
		assertEquals(12, ex.doPart1(100, 11, 7));
	}
}
