package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise05Test {

	@Test
	void testExample1() {
		Exercise05 ex = new Exercise05("47|53\n" +
				"97|13\n" +
				"97|61\n" +
				"97|47\n" +
				"75|29\n" +
				"61|13\n" +
				"75|53\n" +
				"29|13\n" +
				"97|29\n" +
				"53|29\n" +
				"61|53\n" +
				"97|53\n" +
				"61|29\n" +
				"47|13\n" +
				"75|47\n" +
				"97|75\n" +
				"47|61\n" +
				"75|61\n" +
				"47|29\n" +
				"75|13\n" +
				"53|13\n" +
				"\n" +
				"75,47,61,53,29\n" +
				"97,61,53,29,13\n" +
				"75,29,13\n" +
				"75,97,47,61,53\n" +
				"61,13,29\n" +
				"97,13,75,29,47");
		ex.processInput();
		assertEquals(143, ex.doPart1());
		assertEquals(123, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise05 ex = new Exercise05("1|2\n" +
				"2|3\n" +
				"3|4\n" +
				"4|5\n" +
				"5|6\n" +
				"6|7\n" +
				"7|8\n" +
				"8|9\n" +
				"9|10\n" +
				"10|1\n" +
				"\n" +
				"1,3,9\n" + // wrong -> 9, 1, 3 -> 1
				"1,2,3\n" + // correct -> 2
				"10,1,2");  // correct -> 1
		ex.processInput();
		assertEquals(3, ex.doPart1());
		assertEquals(1, ex.doPart2());
	}
}