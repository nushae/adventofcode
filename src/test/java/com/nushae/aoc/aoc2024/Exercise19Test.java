package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise19Test {

	@Test
	void testExample1() {
		Exercise19 ex = new Exercise19("r, wr, b, g, bwu, rb, gb, br\n" +
				"\n" +
				"brwrr\n" +
				"bggr\n" +
				"gbbr\n" +
				"rrbgbr\n" +
				"ubwu\n" +
				"bwurrg\n" +
				"brgr\n" +
				"bbrgwb");
		ex.processInput();
		assertEquals(6, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise19 ex = new Exercise19("r, wr, b, g, bwu, rb, gb, br\n" +
				"\n" +
				"brwrr\n" +
				"bggr\n" +
				"gbbr\n" +
				"rrbgbr\n" +
				"ubwu\n" +
				"bwurrg\n" +
				"brgr\n" +
				"bbrgwb");
		ex.processInput();
		assertEquals(16, ex.doPart2());
	}
}
