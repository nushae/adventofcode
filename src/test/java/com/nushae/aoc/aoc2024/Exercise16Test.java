package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	void testExample1() {
		Exercise16 ex = new Exercise16("###############\n" +
				"#.......#....E#\n" +
				"#.#.###.#.###.#\n" +
				"#.....#.#...#.#\n" +
				"#.###.#####.#.#\n" +
				"#.#.#.......#.#\n" +
				"#.#.#####.###.#\n" +
				"#...........#.#\n" +
				"###.#.#####.#.#\n" +
				"#...#.....#.#.#\n" +
				"#.#.#.###.#.#.#\n" +
				"#.....#...#.#.#\n" +
				"#.###.#.#.#.#.#\n" +
				"#S..#.....#...#\n" +
				"###############");
		ex.processInput();
		assertEquals(7036, ex.doPart1());
		assertEquals(45, ex.doPart2(7036));
	}

	@Test
	void testExample2() {
		Exercise16 ex = new Exercise16("#################\n" +
				"#...#...#...#..E#\n" +
				"#.#.#.#.#.#.#.#.#\n" +
				"#.#.#.#...#...#.#\n" +
				"#.#.#.#.###.#.#.#\n" +
				"#...#.#.#.....#.#\n" +
				"#.#.#.#.#.#####.#\n" +
				"#.#...#.#.#.....#\n" +
				"#.#.#####.#.###.#\n" +
				"#.#.#.......#...#\n" +
				"#.#.###.#####.###\n" +
				"#.#.#...#.....#.#\n" +
				"#.#.#.#####.###.#\n" +
				"#.#.#.........#.#\n" +
				"#.#.#.#########.#\n" +
				"#S#.............#\n" +
				"#################");
		ex.processInput();
		assertEquals(11048, ex.doPart1());
		assertEquals(64, ex.doPart2(11048));
	}

	@Test
	void testPossibleCounterExample() {
		Exercise16 ex = new Exercise16("######\n" +
				"##...##\n" +
				"#S.#.E#\n" +
				"##...##\n" +
				"#######\n"
		);
		ex.processInput();
		long shortest = ex.doPart1();
		assertEquals(4006, shortest);
		assertEquals(10, ex.doPart2(shortest));
	}
}
