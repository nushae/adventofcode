package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise25Test {

	@Test
	void testExample1() {
		Exercise25 ex = new Exercise25("#####\n" +
				".####\n" +
				".####\n" +
				".####\n" +
				".#.#.\n" +
				".#...\n" +
				".....\n" +
				"\n" +
				"#####\n" +
				"##.##\n" +
				".#.##\n" +
				"...##\n" +
				"...#.\n" +
				"...#.\n" +
				".....\n" +
				"\n" +
				".....\n" +
				"#....\n" +
				"#....\n" +
				"#...#\n" +
				"#.#.#\n" +
				"#.###\n" +
				"#####\n" +
				"\n" +
				".....\n" +
				".....\n" +
				"#.#..\n" +
				"###..\n" +
				"###.#\n" +
				"###.#\n" +
				"#####\n" +
				"\n" +
				".....\n" +
				".....\n" +
				".....\n" +
				"#....\n" +
				"#.#..\n" +
				"#.#.#\n" +
				"#####");
		ex.processInput();
		assertEquals(3, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise25 ex = new Exercise25("");
		ex.processInput();
		assertEquals(2024, ex.doPart1());
	}
}
