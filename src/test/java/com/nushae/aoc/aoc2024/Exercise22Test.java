package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	void testExample1() {
		Exercise22 ex = new Exercise22("1\n" +
				"10\n" +
				"100\n" +
				"2024");
		ex.processInput();
		assertEquals(37327623L, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise22 ex = new Exercise22("1\n" +
				"2\n" +
				"3\n" +
				"2024");
		ex.processInput();
		ex.doPart1();
		assertEquals(23, ex.doPart2());
	}
}
