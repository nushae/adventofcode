package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	void testExample1() {
		Exercise17 ex = new Exercise17("Register A: 729\n" +
				"Register B: 0\n" +
				"Register C: 0\n" +
				"\n" +
				"Program: 0,1,5,4,3,0");
		ex.processInput();
		assertEquals("4,6,3,5,6,3,5,2,1,0", ex.doPart1());
	}

	@Test
	void testMyInput() {
		Exercise17 ex = new Exercise17("Register A: 35200350\n" +
				"Register B: 0\n" +
				"Register C: 0\n" +
				"\n" +
				"Program: 2,4,1,2,7,5,4,7,1,3,5,5,0,3,3,0");
		ex.processInput();
		assertEquals("2,7,4,7,2,1,7,5,1", ex.doPart1());
		assertEquals(37221274271220L, ex.doPart2());
	}

	@Test
	void testInput2() {
		Exercise17 ex = new Exercise17("Register A: 66171486\n" +
				"Register B: 0\n" +
				"Register C: 0\n" +
				"\n" +
				"Program: 2,4,1,6,7,5,4,6,1,4,5,5,0,3,3,0");
		ex.processInput();
		assertEquals("2,3,6,2,1,6,1,2,1", ex.doPart1());
		assertEquals(90938893795561L, ex.doPart2());
	}

	@Test
	void testInput3() {
		Exercise17 ex = new Exercise17("Register A: 41644071\n" +
				"Register B: 0\n" +
				"Register C: 0\n" +
				"\n" +
				"Program: 2,4,1,2,7,5,1,7,4,4,0,3,5,5,3,0");
		ex.processInput();
		assertEquals("3,1,5,3,7,4,2,7,5", ex.doPart1());
		assertEquals(190593310997519L, ex.doPart2());
	}
}
