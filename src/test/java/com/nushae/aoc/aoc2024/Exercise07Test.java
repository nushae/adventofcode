package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise07Test {

	@Test
	void testExample1() {
		Exercise07 ex = new Exercise07("190: 10 19\n" +
				"3267: 81 40 27\n" +
				"83: 17 5\n" +
				"156: 15 6\n" +
				"7290: 6 8 6 15\n" +
				"161011: 16 10 13\n" +
				"192: 17 8 14\n" +
				"21037: 9 7 18 13\n" +
				"292: 11 6 16 20");
		ex.processInput();
		assertEquals(3749, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise07 ex = new Exercise07("190: 10 19\n" +
				"3267: 81 40 27\n" +
				"83: 17 5\n" +
				"156: 15 6\n" +
				"7290: 6 8 6 15\n" +
				"161011: 16 10 13\n" +
				"192: 17 8 14\n" +
				"21037: 9 7 18 13\n" +
				"292: 11 6 16 20");
		ex.processInput();
		assertEquals(11387, ex.doPart2());
	}
}