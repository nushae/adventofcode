package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise12Test {

	@Test
	void testExample1() {
		Exercise12 ex = new Exercise12("OOOOO\n" +
				"OXOXO\n" +
				"OOOOO\n" +
				"OXOXO\n" +
				"OOOOO");
		ex.processInput();
		assertEquals(772, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise12 ex = new Exercise12("RRRRIICCFF\n" +
				"RRRRIICCCF\n" +
				"VVRRRCCFFF\n" +
				"VVRCCCJFFF\n" +
				"VVVVCJJCFE\n" +
				"VVIVCCJJEE\n" +
				"VVIIICJJEE\n" +
				"MIIIIIJJEE\n" +
				"MIIISIJEEE\n" +
				"MMMISSJEEE");
		ex.processInput();
		assertEquals(1930, ex.doPart1());
	}

	@Test
	void testExample3() {
		Exercise12 ex = new Exercise12("AAAA\n" +
				"BBCD\n" +
				"BBCC\n" +
				"EEEC");
		ex.processInput();
		assertEquals(80, ex.doPart2());
	}

	@Test
	void testExample4() {
		Exercise12 ex = new Exercise12("OOOOO\n" +
				"OXOXO\n" +
				"OOOOO\n" +
				"OXOXO\n" +
				"OOOOO");
		ex.processInput();
		assertEquals(436, ex.doPart2());
	}

	@Test
	void testExample5() {
		Exercise12 ex = new Exercise12("AAAAAA\n" +
				"AAABBA\n" +
				"AAABBA\n" +
				"ABBAAA\n" +
				"ABBAAA\n" +
				"AAAAAA");
		ex.processInput();
		assertEquals(368, ex.doPart2());
	}
	@Test

	void testExample6() {
		Exercise12 ex = new Exercise12("RRRRIICCFF\n" +
				"RRRRIICCCF\n" +
				"VVRRRCCFFF\n" +
				"VVRCCCJFFF\n" +
				"VVVVCJJCFE\n" +
				"VVIVCCJJEE\n" +
				"VVIIICJJEE\n" +
				"MIIIIIJJEE\n" +
				"MIIISIJEEE\n" +
				"MMMISSJEEE");
		ex.processInput();
		assertEquals(1206, ex.doPart2());
	}
}
