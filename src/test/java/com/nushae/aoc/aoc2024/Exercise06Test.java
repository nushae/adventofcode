package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise06Test {

	@Test
	void testExample1() {
		Exercise06 ex = new Exercise06("....#.....\n" +
				".........#\n" +
				"..........\n" +
				"..#.......\n" +
				".......#..\n" +
				"..........\n" +
				".#..^.....\n" +
				"........#.\n" +
				"#.........\n" +
				"......#...");
		ex.processInput();
		assertEquals(41, ex.doPart1());
		assertEquals(6, ex.doPart2());
	}

	@Test
	void testExampleLoop() {
		Exercise06 ex = new Exercise06("....#.....\n" +
				".........#\n" +
				"..........\n" +
				"..#.......\n" +
				".......#..\n" +
				"..........\n" +
				".#.#^.....\n" +
				"........#.\n" +
				"#.........\n" +
				"......#...");
		ex.processInput();
		assertEquals(-7009, ex.doPart1());
		// assertEquals(6, ex.doPart2());
	}
}