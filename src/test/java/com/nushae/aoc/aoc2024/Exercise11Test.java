package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise11Test {

	@Test
	void testExample1() {
		Exercise11 ex = new Exercise11("125 17");
		ex.processInput();
		assertEquals(55312, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise11 ex = new Exercise11("125 17");
		ex.processInput();
		assertEquals(65601038650482L, ex.doPart2());
	}
}
