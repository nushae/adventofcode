package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise09Test {

	@Test
	void testExample1() {
		Exercise09 ex = new Exercise09("12345");
		ex.processInput();
		assertEquals(60, ex.doPart1());
		assertEquals(132, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise09 ex = new Exercise09("2333133121414131402");
		ex.processInput();
		assertEquals(1928, ex.doPart1());
		assertEquals(2858, ex.doPart2());
	}
}
