package com.nushae.aoc.aoc2024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@Test
	void testExample1() {
		Exercise03 ex = new Exercise03("xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))");
		assertEquals(161, ex.doPart1());
		assertEquals(48, ex.doPart2());
	}
}