package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise12Test {

	@ParameterizedTest
	@MethodSource("inputForPart1")
	void testExamples(String input, long expected) {
		Exercise12 ex = new Exercise12(input);
		ex.processInput();
		assertEquals(expected, ex.doPart1());
	}

	static Stream<Arguments> inputForPart1() {
		return Stream.of(
				Arguments.of("???.### 1,1,3", 1),
				Arguments.of(".??..??...?##. 1,1,3", 4),
				Arguments.of("?#?#?#?#?#?#?#? 1,3,1,6", 1),
				Arguments.of("????.#...#... 4,1,1", 1),
				Arguments.of("????.######..#####. 1,6,5", 4),
				Arguments.of("?###???????? 3,2,1", 10),
				Arguments.of(".??#?????.. 1,2,3", 1),
				Arguments.of("????..???.?..?#?. 1,1", 8),
				Arguments.of("????..???.?..?#?.?????..???.?..?#?.?????..???.?..?#?.?????..???.?..?#?.?????..???.?..?#?. 1,1,1,1,1,1,1,1,1,1", 783297)
		);
	}

	@Test
	void testPart1() {
		Exercise12 ex = new Exercise12("???.### 1,1,3\n" +
				".??..??...?##. 1,1,3\n" +
				"?#?#?#?#?#?#?#? 1,3,1,6\n" +
				"????.#...#... 4,1,1\n" +
				"????.######..#####. 1,6,5\n" +
				"?###???????? 3,2,1");
		ex.processInput();
		assertEquals(21, ex.doPart1());
	}
}