package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise13Test {

	@Test
	void testExample() {
		Exercise13 ex = new Exercise13("#.##..##.\n" +
				"..#.##.#.\n" +
				"##......#\n" +
				"##......#\n" +
				"..#.##.#.\n" +
				"..##..##.\n" +
				"#.#.##.#.");
		ex.processInput();
		assertEquals(5, ex.doPart1());
		assertEquals(300, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise13 ex = new Exercise13("#...##..#\n" +
				"#....#..#\n" +
				"..##..###\n" +
				"#####.##.\n" +
				"#####.##.\n" +
				"..##..###\n" +
				"#....#..#");
		ex.processInput();
		assertEquals(400, ex.doPart1());
		assertEquals(100, ex.doPart2());
	}
}