package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise15Test {

	@Test
	void testHash() {
		assertEquals(52, Exercise15.calcHash("HASH"));
	}

	@Test
	void testExample() {
		Exercise15 ex = new Exercise15("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7");
		assertEquals(1320, ex.doPart1());
		assertEquals(145, ex.doPart2());
	}
}