package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise08Test {

	@Test
	void testExample1() {
		Exercise08 ex = new Exercise08("RL\n" +
				"\n" +
				"AAA = (BBB, CCC)\n" +
				"BBB = (DDD, EEE)\n" +
				"CCC = (ZZZ, GGG)\n" +
				"DDD = (DDD, DDD)\n" +
				"EEE = (EEE, EEE)\n" +
				"GGG = (GGG, GGG)\n" +
				"ZZZ = (ZZZ, ZZZ)");

		ex.processWithPattern();

		assertEquals(2, ex.doPart1());
		assertEquals(0, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise08 ex = new Exercise08("LLR\n" +
				"\n" +
				"AAA = (BBB, BBB)\n" +
				"BBB = (AAA, ZZZ)\n" +
				"ZZZ = (ZZZ, ZZZ)");
		ex.processWithPattern();

		assertEquals(6, ex.doPart1());
		assertEquals(0, ex.doPart2());
	}


}