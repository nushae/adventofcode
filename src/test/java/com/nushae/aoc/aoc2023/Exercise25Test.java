package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise25Test {

	@Test
	void testExample1() {
		Exercise25 ex = new Exercise25("jqt: rhn xhk nvd\n" +
				"rsh: frs pzl lsr\n" +
				"xhk: hfx\n" +
				"cmg: qnr nvd lhk bvb\n" +
				"rhn: xhk bvb hfx\n" +
				"bvb: xhk hfx\n" +
				"pzl: lsr hfx nvd\n" +
				"qnr: nvd\n" +
				"ntq: jqt hfx bvb xhk\n" +
				"nvd: lhk\n" +
				"lsr: lhk\n" +
				"rzs: qnr cmg lsr rsh\n" +
				"frs: qnr lhk lsr");
		ex.processInput();
		assertEquals(54, ex.doPart1());
	}
}