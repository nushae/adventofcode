package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise21Test {

	@Test
	void testExample1() {
		Exercise21 ex = new Exercise21("...........\n" +
				".....###.#.\n" +
				".###.##..#.\n" +
				"..#.#...#..\n" +
				"....#.#....\n" +
				".##..S####.\n" +
				".##..#...#.\n" +
				".......##..\n" +
				".##.#.####.\n" +
				".##..##.##.\n" +
				"...........");
		assertEquals(16, ex.doPart1(6));
		assertEquals(6536, ex.doPart2Automated(100)); // hardcoded version will naturally fail
	}
}