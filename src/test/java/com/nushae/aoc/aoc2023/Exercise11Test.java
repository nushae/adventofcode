package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise11Test {

	@Test
	void testExample1() {
		Exercise11 ex = new Exercise11("...#......\n" +
				".......#..\n" +
				"#.........\n" +
				"..........\n" +
				"......#...\n" +
				".#........\n" +
				".........#\n" +
				"..........\n" +
				".......#..\n" +
				"#...#.....");

		assertEquals(374, ex.calcDistances(2));
		assertEquals(1030, ex.calcDistances(10));
		assertEquals(8410, ex.calcDistances(100));
	}
}