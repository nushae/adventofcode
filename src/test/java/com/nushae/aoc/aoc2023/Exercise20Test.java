package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise20Test {

	@Test
	void testExample1() {
		Exercise20 ex = new Exercise20("broadcaster -> a, b, c\n" +
				"%a -> b\n" +
				"%b -> c\n" +
				"%c -> inv\n" +
				"&inv -> a");
		assertEquals(32000000, ex.doPart1());
	}

	@Test
	void testExample2() {
		Exercise20 ex = new Exercise20("broadcaster -> a\n" +
				"%a -> inv, con\n" +
				"&inv -> b\n" +
				"%b -> con\n" +
				"&con -> output");
		assertEquals(11687500, ex.doPart1());
	}
}