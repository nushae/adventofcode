package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise18Test {

	@ParameterizedTest
	@MethodSource("curvesForShoelace")
	void testShoelaceCounterClockWise1(String input, long expectedPart1, long expectedPart2) {
		Exercise18 ex = new Exercise18(input);
		ex.processWithPattern();
		assertEquals(expectedPart1, ex.doPart1());
		assertEquals(expectedPart2, ex.doPart2());
	}

	static Stream<Arguments> curvesForShoelace() {
		return Stream.of(
				Arguments.of("U 2 (#000023)\n" +
						"L 2 (#000022)\n" +
						"D 2 (#000021)\n" +
						"R 2 (#000020)", 9, 9),
				Arguments.of("U 2 (#000023)\n" +
						"R 2 (#000020)\n" +
						"D 2 (#000021)\n" +
						"L 2 (#000022)", 9, 9),
				Arguments.of("U 3 (#000033)\n" +
						"L 3 (#000032)\n" +
						"U 2 (#000023)\n" +
						"L 4 (#000042)\n" +
						"D 5 (#000051)\n" +
						"R 7 (#000070)", 42, 42),
				Arguments.of("D 3 (#000031)\n" +
						"L 3 (#000032)\n" +
						"D 2 (#000021)\n" +
						"L 4 (#000042)\n" +
						"U 5 (#000053)\n" +
						"R 7 (#000070)", 42, 42)
		);
	}

	@Test
	void testExample() {
		Exercise18 ex = new Exercise18("R 6 (#70c710)\n" +
				"D 5 (#0dc571)\n" +
				"L 2 (#5713f0)\n" +
				"D 2 (#d2c081)\n" +
				"R 2 (#59c680)\n" +
				"D 2 (#411b91)\n" +
				"L 5 (#8ceee2)\n" +
				"U 2 (#caa173)\n" +
				"L 1 (#1b58a2)\n" +
				"U 2 (#caa171)\n" +
				"R 2 (#7807d2)\n" +
				"U 3 (#a77fa3)\n" +
				"L 2 (#015232)\n" +
				"U 2 (#7a21e3)");
		ex.processWithPattern();
		assertEquals(62, ex.doPart1());
		assertEquals(952408144115L, ex.doPart2());
	}
}