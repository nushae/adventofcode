package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise22Test {

	@Test
	void testExample1() {
		Exercise22 ex = new Exercise22("1,0,1~1,2,1\n" +
				"0,0,2~2,0,2\n" +
				"0,2,3~2,2,3\n" +
				"0,0,4~0,2,4\n" +
				"2,0,5~2,2,5\n" +
				"0,1,6~2,1,6\n" +
				"1,1,8~1,1,9");
		ex.processInput();
		assertEquals(5, ex.doPart1());
		assertEquals(7, ex.doPart2());
	}
}