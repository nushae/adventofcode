package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise06Test {

	@Test
	void testBetterResults() {
		assertEquals(4, Exercise06.countBetterResults(7, 9));
		assertEquals(8, Exercise06.countBetterResults(15, 40));
		assertEquals(9, Exercise06.countBetterResults(30, 200));
	}

	@Test
	void testExample1() {
		Exercise06 ex = new Exercise06(new Point(7, 9), new Point(15, 40), new Point(30, 200));
		assertEquals(288, ex.doPart1());
		assertEquals(71503, ex.doPart2(71530, 940200));
	}

}