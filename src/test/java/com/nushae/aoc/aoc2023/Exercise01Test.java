package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise01Test {

	@Test
	void testBothPartsOverlapLeft() {
		Exercise01 ex = new Exercise01("ztwone1nine");
		assertEquals(11, ex.doPart(false));
		assertEquals(29, ex.doPart(true));
	}

	@Test
	void testBothPartsOverlapRight() {
		Exercise01 ex = new Exercise01("ztwoone1nineight");
		assertEquals(11, ex.doPart(false));
		assertEquals(28, ex.doPart(true));
	}

	@Test
	void testBothPartsDigitWord() {
		Exercise01 ex = new Exercise01("zt3woone1eight");
		assertEquals(31, ex.doPart(false));
		assertEquals(38, ex.doPart(true));
	}

	@Test
	void testBothPartsWordDigit() {
		Exercise01 ex = new Exercise01("ztwo3one1eight7");
		assertEquals(37, ex.doPart(false));
		assertEquals(27, ex.doPart(true));
	}

	@Test
	void testBothPartsComplete() {
		Exercise01 ex = new Exercise01("two1nine\n" +
				"eightwo7three\n" +
				"abcone2threexyz\n" +
				"x8twone3four\n" +
				"4nineeightseven2\n" +
				"zoneight234\n" +
				"7pqrstsixteen\n" +
				"12threevgh7r830fourtwone");
		assertEquals(346, ex.doPart(false));
		assertEquals(352, ex.doPart(true));
	}
}