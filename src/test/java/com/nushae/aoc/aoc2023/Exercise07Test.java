package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise07Test {

	@Test
	void testExample() {
		Exercise07 ex = new Exercise07("32T3K 765\n" +
				"T55J5 684\n" +
				"KK677 28\n" +
				"KTJJT 220\n" +
				"QQQJA 483");
		assertEquals(765 + 220*2 + 28*3 + 684*4 + 483*5, ex.calculateWinnings(true));
		assertEquals(765 + 28*2 + 684*3 + 483*4 + 220*5, ex.calculateWinnings(false));
	}

	@ParameterizedTest
	@MethodSource("comparisonsWithJokers")
	void testCompare(String left, String right, int expected) {
		Exercise07 ex = new Exercise07("");
		assertEquals(expected, ex.pokerCompare(left, right, true));
	}
	private static Stream<Arguments> comparisonsWithJokers() {
		return Stream.of(
				// these 4 cases helped me find a copy/paste bug (lChar -> rChar)
				Arguments.of("22J33", "222AA", -1),
				Arguments.of("3TAAJ", "QQK6Q", -1),
				Arguments.of("888AA", "AJAJK", -1),
				Arguments.of("JJJJJ", "TJTTJ", -1),
				// these 6 cases helped me figure out that I renumbered the rankings-with-jokers wrong
				Arguments.of("22JJ2", "222J2", -1),
				Arguments.of("J2J2J", "22JJ2", -1),
				Arguments.of("J2J2J", "222J2", -1),
				Arguments.of("JJJJJ", "22JJ2", -1),
				Arguments.of("JJJJJ", "J2J2J", -1),
				Arguments.of("JJJJJ", "222J2", -1)
		);
	}
}