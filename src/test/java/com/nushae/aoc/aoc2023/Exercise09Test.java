package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise09Test {

	@Test
	void testExample1() {
		Exercise09 ex = new Exercise09("0 3 6 9 12 15");

		ex.processInput();

		assertEquals(18, ex.doPart1());
		assertEquals(-3, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise09 ex = new Exercise09("1   3   6  10  15  21");

		ex.processInput();

		assertEquals(28, ex.doPart1());
		assertEquals(0, ex.doPart2());
	}
	@Test
	void testExample3() {
		Exercise09 ex = new Exercise09("10  13  16  21  30  45");

		ex.processInput();

		assertEquals(68, ex.doPart1());
		assertEquals(5, ex.doPart2());
	}

	@Test
	void testExample4() {
		Exercise09 ex = new Exercise09("0 3 6 9 12 15\n" +
				"1 3 6 10 15 21\n" +
				"10 13 16 21 30 45");

		ex.processInput();

		assertEquals(18 + 28 + 68, ex.doPart1());
		assertEquals(-3 + 5, ex.doPart2());
	}

	@Test
	void testExample5() {
		Exercise09 ex = new Exercise09("12 18 16 2 -28 -78 -152 -254 -388 -558 -768 -1022 -1324 -1678 -2088 -2558 -3092 -3694 -4368 -5118 -5948");

		ex.processInput();

		assertEquals(-6862, ex.doPart1());
		assertEquals(2, ex.doPart2());
	}
}