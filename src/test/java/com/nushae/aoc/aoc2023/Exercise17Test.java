package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise17Test {

	@Test
	void testExample() {
		Exercise17 ex = new Exercise17("2413432311323\n" +
				"3215453535623\n" +
				"3255245654254\n" +
				"3446585845452\n" +
				"4546657867536\n" +
				"1438598798454\n" +
				"4457876987766\n" +
				"3637877979653\n" +
				"4654967986887\n" +
				"4564679986453\n" +
				"1224686865563\n" +
				"2546548887735\n" +
				"4322674655533");
		assertEquals(102, ex.doPart1());
		assertEquals(94, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise17 ex = new Exercise17("111111111111\n" +
				"999999999991\n" +
				"999999999991\n" +
				"999999999991\n" +
				"999999999991");
		assertEquals(59, ex.doPart1());
		assertEquals(71, ex.doPart2());
	}
}