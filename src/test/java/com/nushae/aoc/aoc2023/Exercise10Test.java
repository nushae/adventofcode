package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise10Test {

	@Test
	void testExample1() {
		Exercise10 ex = new Exercise10("-L|F7\n" +
				"7S-7|\n" +
				"L|7||\n" +
				"-L-J|\n" +
				"L|-JF");

		ex.processInput();

		assertEquals(4, ex.doPart1());
		assertEquals(1, ex.doPart2());
	}

	@Test
	void testExample2() {
		Exercise10 ex = new Exercise10("7-F7-\n" +
				".FJ|7\n" +
				"SJLL7\n" +
				"|F--J\n" +
				"LJ.LJ");

		ex.processInput();

		assertEquals(8, ex.doPart1());
		assertEquals(1, ex.doPart2());
	}

	@Test
	void testExample3() {
		Exercise10 ex = new Exercise10("FF7FSF7F7F7F7F7F---7\n" +
				"L|LJ||||||||||||F--J\n" +
				"FL-7LJLJ||||||LJL-77\n" +
				"F--JF--7||LJLJ7F7FJ-\n" +
				"L---JF-JLJ.||-FJLJJ7\n" +
				"|F|F-JF---7F7-L7L|7|\n" +
				"|FFJF7L7F-JF7|JL---7\n" +
				"7-L-JL7||F7|L7F-7F7|\n" +
				"L.L7LFJ|||||FJL7||LJ\n" +
				"L7JLJL-JLJLJL--JLJ.L");

		ex.processInput();

		assertEquals(80, ex.doPart1());
		assertEquals(10, ex.doPart2());
	}
}