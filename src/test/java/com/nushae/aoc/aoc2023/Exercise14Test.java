package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise14Test {

	@Test
	void testCountLoad() {
		Exercise14 ex = new Exercise14(".\n" +
				".\n" +
				".\n" +
				"O");
		ex.processInput();
		assertEquals(4, ex.doPart1());
	}

	@Test
	void testMovement() {
		Exercise14 ex = new Exercise14(".\n" +
				".\n" +
				".\n" +
				"O");
		ex.processInput();
		assertEquals(4, ex.doPart1());
	}

	@Test
	void testMovement2() {
		Exercise14 ex = new Exercise14(".\n" +
				"#\n" +
				".\n" +
				"O");
		ex.processInput();
		assertEquals(2, ex.doPart1());
	}

	@Test
	void testExample() {
		Exercise14 ex = new Exercise14("O....#....\n" +
				"O.OO#....#\n" +
				".....##...\n" +
				"OO.#O....O\n" +
				".O.....O#.\n" +
				"O.#..O.#.#\n" +
				"..O..#O..O\n" +
				".......O..\n" +
				"#....###..\n" +
				"#OO..#....");
		ex.processInput();
		assertEquals(136, ex.doPart1());
		assertEquals(64, ex.doPart2());
	}
}