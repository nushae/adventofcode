package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static com.nushae.aoc.util.MathUtil.EAST;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise16Test {

	@Test
	void testExample() {
		Exercise16 ex = new Exercise16(".|...\\....\n" +
				"|.-.\\.....\n" +
				".....|-...\n" +
				"........|.\n" +
				"..........\n" +
				".........\\\n" +
				"..../.\\\\..\n" +
				".-.-/..|..\n" +
				".|....-|.\\\n" +
				"..//.|....");
		assertEquals(46, ex.doPart1(new Point(0, 0), EAST));
		assertEquals(0, ex.doPart2());
	}
}