package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise03Test {

	@Test
	void edgeCase01() {
		Exercise03 ex = new Exercise03("*467..114..");
		assertEquals(467, ex.doPart1());
	}

	@Test
	void edgeCase02() {
		Exercise03 ex = new Exercise03(".467..114/.");
		assertEquals(114, ex.doPart1());
	}

	@Test
	void edgeCase03() {
		Exercise03 ex = new Exercise03("$.........\n" + ".476..154.");
		assertEquals(476, ex.doPart1());
	}

	@Test
	void edgeCase04() {
		Exercise03 ex = new Exercise03("....@.....\n" + ".476..154.");
		assertEquals(476, ex.doPart1());
	}

	@Test
	void edgeCase05() {
		Exercise03 ex = new Exercise03(".....$...\n" + ".476..154");
		assertEquals(154, ex.doPart1());
	}

	@Test
	void edgeCase06() {
		Exercise03 ex = new Exercise03(".........\n" + ".476..154");
		assertEquals(0, ex.doPart1());
	}

	@Test
	void testExample1() {
		Exercise03 ex = new Exercise03("467..114..\n" +
				"...*......\n" +
				"..35..633.\n" +
				"......#...\n" +
				"617*......\n" +
				".....+.58.\n" +
				"..592.....\n" +
				"......755.\n" +
				"...$.*....\n" +
				".664.598..");
		assertEquals(4361, ex.doPart1());
		assertEquals(467835, ex.doPart2());
	}

}