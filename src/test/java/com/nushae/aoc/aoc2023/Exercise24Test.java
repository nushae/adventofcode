package com.nushae.aoc.aoc2023;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercise24Test {

	@Test
	void testExample1() {
		Exercise24 ex = new Exercise24("19, 13, 30 @ -2,  1, -2\n" +
				"18, 19, 22 @ -1, -1, -2\n" +
				"20, 25, 34 @ -2, -2, -4\n" +
				"12, 31, 28 @ -1, -2, -1\n" +
				"20, 19, 15 @  1, -5, -3");
		ex.processInput();
		assertEquals(2, ex.doPart1(7, 27, 7, 27));
		assertEquals(47, ex.doPart2());
	}
}