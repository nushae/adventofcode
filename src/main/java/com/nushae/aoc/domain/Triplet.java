package com.nushae.aoc.domain;

import lombok.Getter;

import java.util.Objects;

@Getter
public class Triplet<T, U, V> {
	private final T left;
	private final U mid;
	private final V right;

	public Triplet(T l, U m, V r) {
		left = l;
		mid = m;
		right = r;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || getClass() != o.getClass()) return false;
		Triplet<?, ?, ?> triplet = (Triplet<?, ?, ?>) o;
		return Objects.equals(left, triplet.left) && Objects.equals(mid, triplet.mid) && Objects.equals(right, triplet.right);
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, mid, right);
	}

	@Override
	public String toString() {
		return "[" + left + ", " + mid + ", " + right + "]";
	}
}
