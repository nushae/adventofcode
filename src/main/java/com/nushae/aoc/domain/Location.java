package com.nushae.aoc.domain;

import lombok.Getter;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

/**
 * Coordinate class for use with the CharGrid class. (0, 0) is top left of the CharGrid.
 */
@Getter
public class Location {
	final int x;
	final int y;
	final CharGrid context; // if null, this instance represents a relative vector

	public Location(int x, int y, CharGrid context) {
		this.x = x;
		this.y = y;
		this.context = context;
	}

	// Context dependent methods.

	public boolean inContext() {
		if (context == null) {
			throw new IllegalStateException("Context is null");
		}
		return context.inGrid(x, y);
	}

	public char resolve() {
		if (context == null) {
			throw new IllegalStateException("Context is null");
		}
		return context.getOrDefault(x, y);
	}

	public char getPeriodic() {
		if (context == null) {
			throw new IllegalStateException("Context is null");
		}
		return context.getWrapped(x, y);
	}

	public void set(char c) {
		if (context == null) {
			throw new IllegalStateException("Context is null");
		}
		context.set(x, y, c);
	}

	public Location reduceToContext() {
		if (context == null) {
			throw new IllegalStateException("Context is null");
		}
		return new Location(
				Math.floorMod(x, context.getWidth()),
				Math.floorMod(y, context.getHeight()),
				context
		);
	}

	// context independent methods

	public int crossProd(Location other) {
		return this.x * other.y - this.y * other.x;
	}
	public int dotProd(Location other) {
		return this.x * other.x + this.y * other.y;
	}
	public Location scale(int scalar) {
		return new Location(x * scalar, y * scalar, context);
	}
	public Location add(Location delta) {
		return new Location(x + delta.x, y + delta.y, context);
	}
	public Location add(int dx, int dy) {
		return new Location(x + dx, y + dy, context);
	}
	public Location linear(Location other, int scalar) {
		return add(other.scale(scalar));
	}
	public long manhattan(Location other) {
		return Math.abs(x - other.x) + Math.abs(y - other.y);
	}

	// grid navigation methods

	public List<Location> getNeighbours() {
		if (context == null) {
			return getNeighboursIgnoreContext();
		}
		List<Location> result = new ArrayList<>();
		for (Point delta : ORTHO_DIRS) {
			Location translated = this.add(delta.x, delta.y);
			if (translated.inContext()) {
				result.add(translated);
			}
		}
		return result;
	}

	public List<Location> getNeighboursIgnoreContext() {
		List<Location> result = new ArrayList<>();
		for (Point delta : ORTHO_DIRS) {
			result.add(this.add(delta.x, delta.y));
		}
		return result;
	}

	public List<Location> getNeighboursReduceToContext() {
		if (context == null) {
			return getNeighboursIgnoreContext();
		}
		List<Location> result = new ArrayList<>();
		for (Point delta : ORTHO_DIRS) {
			result.add(this.add(delta.x, delta.y).reduceToContext());
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Location other = (Location) o;
		return x == other.x && y == other.y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public String toString() {
		return String.format("(%d, %d)", x, y);
	}
}
