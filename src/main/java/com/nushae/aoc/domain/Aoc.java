package com.nushae.aoc.domain;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aoc {
	int year();
	int day();
	String title();
	String[] keywords() default {};
}
