package com.nushae.aoc.domain;

import lombok.Getter;

import java.util.Objects;

@Getter
public class Pair<T, U> {
	private final T left;
	private final U right;

	public Pair(T l, U r) {
		left = l;
		right = r;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Pair<?, ?> pair = (Pair<?, ?>) o;
		return Objects.equals(left, pair.left) && Objects.equals(right, pair.right);
	}

	@Override
	public int hashCode() {
		return Objects.hash(left, right);
	}

	@Override
	public String toString() {
		return "[" + left + ", " + right + "]";
	}
}
