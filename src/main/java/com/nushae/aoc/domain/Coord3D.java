package com.nushae.aoc.domain;

/**
 * TODO: turn into a proper vector package
 * Simple representation of int based 3D coordinates.
 *
 * YES, I know of the existence of javafx.geometry.Point3D.
 * I have different needs and this fulfills them.
 */
public record Coord3D(long x, long y, long z) {

	public static final Coord3D[] ORTHO_NEIGHBOURS = {
			new Coord3D(0,0,1),
			new Coord3D(0,0,-1),
			new Coord3D(0,1,0),
			new Coord3D(0,-1,0),
			new Coord3D(1,0,0),
			new Coord3D(-1,0,0),
	};

	/**
	 * Rotate this 3d coordinate times * 90 degrees around the x-axis.
	 * This can be done simply by rearranging coordinates and/or flipping signs.
	 * eg. rotating (1, 2, 3) 90 degrees around x produces (1, -3, 2), in other words (x, y, z) -> (x, -z, y)
	 *
	 * @param times number of times to rotate around x by 90 degrees
	 * @return the resulting 3D coordinate
	 */
	public Coord3D rotX90(int times) {
		Coord3D result = new Coord3D(x, y, z);
		for (int i=0; i < times % 4; i++) {
			result = new Coord3D(result.x, -result.z, result.y);
		}
		return result;
	}

	/**
	 * Rotate this 3d coordinate times * 90 degrees around the y-axis.
	 * This can be done simply by rearranging coordinates and/or flipping signs.
	 * eg. rotating (1, 2, 3) 90 degrees around y produces (-3, 2, 1), in other words (x, y, z) -> (-z, y, x)
	 *
	 * @param times number of times to rotate around y by 90 degrees
	 * @return the resulting 3D coordinate
	 */
	public Coord3D rotY90(int times) {
		Coord3D result = new Coord3D(x, y, z);
		for (int i=0; i < times % 4; i++) {
			result = new Coord3D(-result.z, result.y, result.x);
		}
		return result;
	}

	/**
	 * Rotate this 3d coordinate times * 90 degrees around the z-axis.
	 * This can be done simply by rearranging coordinates and/or flipping signs.
	 * eg. rotating (1, 2, 3) 90 degrees around z produces (-2, 1, 3), in other words (x, y, z) -> (-y, x, z)
	 *
	 * @param times number of times to rotate around z by 90 degrees
	 * @return the resulting 3D coordinate
	 */
	public Coord3D rotZ90(int times) {
		Coord3D result = new Coord3D(x, y, z);
		for (int i=0; i < times % 4; i++) {
			result = new Coord3D(-result.y, result.x, result.z);
		}
		return result;
	}

	/**
	 * calculate the manhattan distance from this coordinate to the passed coordinate
	 *
	 * @param other 3D coordinate to calculate distance to
	 * @return manhattan distance
	 */
	public long manhattan(Coord3D other) {
		return Math.abs(x - other.x) +  Math.abs(y - other.y) + Math.abs(z - other.z);
	}

	/**
	 * 	 * calculate the sum of this coordinate and another coordinate (as tough they are vectors) and return the result as a new coordinate
	 * @param other Coord3D to add to this
	 * @return the sum
	 */
	public Coord3D add(Coord3D other) {
		return new Coord3D(this.x + other.x, this.y + other.y, this.z + other.z);
	}

	@Override
	public String toString() {
		return "<" + x + "," + y + "," + z + ">";
	}
}
