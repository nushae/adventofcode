package com.nushae.aoc.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {
	@Getter
	String name;
	ArrayList<TreeNode<T>> children;
	@Getter
	@Setter
	TreeNode<T> parent;
	@Getter
	@Setter
	private int depth;
	@Getter
	@Setter
	private T value;

	public TreeNode(String name, T value) {
		this.name = name;
		children = new ArrayList<>();
		this.value = value;
		depth = -1;
	}

	public void addChild(TreeNode<T> child) {
		children.add(child);
	}

	public TreeNode<T> getParentNode() {
		return parent;
	}

	public List<TreeNode<T>> getChildren() {
		return children;
	}

	public boolean isLeaf() {
		return children.isEmpty();
	}

	public boolean isInternal() {
		return !children.isEmpty();
	}

	/**
	 * flatten this tree in preorder, and return the resulting list of TreeNodes
	 * @return List<TreeNode> preorder-flattening of this tree
	 */
	public List<TreeNode<T>> getPreOrder() {
		return preOrderNoLoops(new ArrayList<>());
	}
	private List<TreeNode<T>> preOrderNoLoops(List<TreeNode<T>> alreadyFound) {
		List<TreeNode<T>> result = new ArrayList<>(alreadyFound);
		result.add(this);
		for (TreeNode<T> tn : children) {
			if (!result.contains(tn)) {
				result = tn.preOrderNoLoops(result);
			}
		}
		return result;
	}

	/**
	 * flatten this tree in postorder, and return the resulting list of TreeNodes
	 * @return List<TreeNode> postorder-flattening of this tree
	 */
	public List<TreeNode<T>> getPostOrder() {
		ArrayList<TreeNode<T>> result = new ArrayList<>();
		for (TreeNode<T> tn : children) {
			result.addAll(tn.getPostOrder());
		}
		result.add(this);
		return result;
	}

	/**
	 * Create an indented String representation of this tree, with one node per line
	 *
	 * @return an indented String representation of this tree
	 */
	public String toIndentedString() {
		return indentedRep("", new StringBuilder()).toString();
	}

	private StringBuilder indentedRep(String indent, StringBuilder soFar) {
		soFar.append(indent).append(this.name).append(" = ").append(this.value).append("\n");
		for (TreeNode<T> tn : this.getChildren()) {
			soFar = tn.indentedRep(indent + "  ", soFar);
		}
		return soFar;
	}
}