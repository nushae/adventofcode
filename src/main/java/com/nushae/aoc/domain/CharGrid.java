package com.nushae.aoc.domain;

import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CharGrid {
    private final char[][] myGrid;
    private final char defaultChar;

    public CharGrid(CharGrid copyFrom) {
        this.myGrid = new char[copyFrom.myGrid.length][];
        for (int y = 0; y < myGrid.length; y++) {
            myGrid[y] = Arrays.copyOf(copyFrom.myGrid[y], copyFrom.myGrid[y].length);
        }
        this.defaultChar = copyFrom.defaultChar;
    }

    public CharGrid(char[][] initial, char defaultChar) {
        this.myGrid = initial;
        this.defaultChar = defaultChar;
    }

    public CharGrid(String[] initial, char defaultChar) {
        int width = Arrays.stream(initial).mapToInt(String::length).max().orElse(0);
        this.myGrid = new char[initial.length][width];
        for (int y = 0; y < initial.length; y++) {
            Arrays.fill(this.myGrid[y], defaultChar);
            for (int x = 0; x < initial[y].length(); x++) {
                this.myGrid[y][x] = initial[y].charAt(x);
            }
        }
        this.defaultChar = defaultChar;
    }

    public CharGrid(String[] initial) {
        this(initial, ' ');
    }

    public CharGrid(String initial, char defaultChar) {
        this(initial.split("\n"), defaultChar);
    }

    public CharGrid(String initial) {
        this(initial.split("\n"));
    }

    public CharGrid(List<String> initial) {
        this(initial.toArray(new String[0]), ' ');
    }

    public CharGrid(List<String> initial, char defaultChar) {
        this(initial.toArray(new String[0]), defaultChar);
    }

    public CharGrid(int height, int width, char defaultChar) {
        this.myGrid = new char[height][width];
        for (int y = 0; y < height; y++) {
            Arrays.fill(this.myGrid[y], defaultChar);
        }
        this.defaultChar = defaultChar;
    }

    public CharGrid(int height, int width, Set<Point> initial, char empty, char filled) {
        this(height, width, empty);
        for (Point loc : initial) {
            myGrid[loc.y][loc.x] = filled;
        }
    }

    public CharGrid(int height, int width) {
        this(height, width, '.');
    }

    public int getWidth() {
        return myGrid[0].length;
    }

    public int getHeight() {
        return myGrid.length;
    }

    public boolean inGrid(int x, int y) {
        return (x >= 0 && y >= 0 && x < getWidth() && y < getHeight());
    }

    public char get(int x, int y) {
        return myGrid[y][x];
    }

    public char getEmpty() {
        return defaultChar;
    }

    public char getOrDefault(int x, int y) {
        if (!inGrid(x, y)) {
            return defaultChar;
        } else {
            return myGrid[y][x];
        }
    }

    public char getWrapped(int x, int y) {
        return myGrid[Math.floorMod(y, getHeight())][Math.floorMod(x, getWidth())];
    }

    public void set(int x, int y, char c) {
        myGrid[y][x] = c;
    }

    public String getRow(int y) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int x = 0; x < myGrid[y].length; x++) {
            stringBuilder.append(myGrid[y][x]);
        }
        return stringBuilder.toString();
    }

    public String getCol(int x) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char[] chars : myGrid) {
            stringBuilder.append(chars[x]);
        }
        return stringBuilder.toString();
    }

    public CharGrid transpose() {
        String[] s = new String[getWidth()];
        for (int i = 0; i < getWidth(); i++) {
            s[i] = getCol(i);
        }
        return new CharGrid(s);
    }

    public CharGrid flipHorizontal() {
        char[][] newContent = new char[getHeight()][getWidth()];
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                newContent[y][x] = myGrid[y][getWidth() - x - 1];
            }
        }
        return new CharGrid(newContent, defaultChar);
    }

    public CharGrid flipVertical() {
        char[][] newContent = new char[getHeight()][getWidth()];
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                newContent[y][x] = myGrid[getHeight() - y - 1][x];
            }
        }
        return new CharGrid(newContent, defaultChar);
    }

    public Optional<Location> findFirst(char c) {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                if (myGrid[y][x] == c) {
                    return Optional.of(new Location(x, y, this));
                }
            }
        }
        return Optional.empty();
    }

    public boolean occursAt(int x, int y, CharGrid subgrid) {
        if (!inGrid(x + subgrid.getWidth() -1, y + subgrid.getHeight() - 1)) {
            return false;
        }
        for (int i = 0; i < subgrid.getWidth(); i++) {
            for (int j = 0; j < subgrid.getHeight(); j++) {
                if (subgrid.get(i, j) != '.' && subgrid.get(i, j) != get(x + i, y + j)) {
                    return false;
                }
            }
        }
        return true;
    }

    public Optional<Location> findFirst(CharGrid subgrid) {
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                if (occursAt(x, y, subgrid)) {
                    return Optional.of(new Location(x, y, this));
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CharGrid that = (CharGrid) o;
        return defaultChar == that.defaultChar && Arrays.deepEquals(myGrid, that.myGrid);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(defaultChar);
        result = 31 * result + Arrays.deepHashCode(myGrid);
        return result;
    }

    @Override
    public String toString() {
        return IntStream.range(0, myGrid.length)
                .mapToObj(this::getRow)
                .collect(Collectors.joining("\n"));
    }
}
