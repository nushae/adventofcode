package com.nushae.aoc.domain;

import java.util.Map;

public interface SearchNode {

	/**
	 * Returns a map of all nodes adjacent to this one. Values in the map are the distances to these nodes.
	 * For searches that don't use distance set distance to 1. If you implement this by dynamically generating
	 * adjacent nodes (ie for infinite, lazily evaluated graphs), you must ensure that if SearchNodes sA1 and sA2
	 * returned by different calls to adjacentNodes() both represent the same graph node A, then sA1 == sA2.
	 *
	 * @return Map of adjacent nodes with the distances to them
	 */
	Map<SearchNode, Long> getAdjacentNodes();

	/**
	 * Return a String representation of this node's state. For any two SearchNodes that are to be deemed the same,
	 * this representation must be equal. This is useful for when SearchNodes are generated dynamically
	 * and their equality (for the purposes of the search) must somehow depend on their internal state.
	 *
	 * @return String state representation of this SearchNode
	 */
	String getState();

	/**
	 * Sets the parent SearchNode, ie the node that this SearchNode was reached from during a search.
	 *
	 * @param parent the SearchNode to set as parent
	 */
	void setParent(SearchNode parent);

	/**
	 * Returns the parent SearchNode, ie the node that this SearchNode was reached from during the search.
	 * The chain of parent nodes defines the path from start to goal.
	 *
	 * @return the parent SearchNode, or null if there is no parent.
	 */
	SearchNode getParent();
}
