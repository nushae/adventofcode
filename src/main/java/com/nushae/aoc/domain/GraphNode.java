package com.nushae.aoc.domain;

import java.awt.*;
import java.util.Map;

public interface GraphNode<T> {
	/**
	 * Returns a map of all nodes adjacent to this one. Values in the map are the distances to these nodes.
	 *
	 * @return Map of adjacent nodes with the distances to them
	 */
	Map<? extends GraphNode<T>, Long> getAdjacentNodes();

	/**
	 * Adds a GraphNode to this one as an adjacent node, with given distance
	 *
	 * @param neighbour new neighbour to add
	 * @param distanceToNeighbour distance to that neighbour
	 */
	void addNeighbour(GraphNode<T> neighbour, long distanceToNeighbour);

	/**
	 * Returns the payload of this GraphNode.
	 *
	 * @return payload
	 */
	T getPayload();

	/**
	 * Sets the payload of this node
	 *
	 * @param payload the payload
	 */
	void setPayload(T payload);

	Point getLocation();

	/**
	 * Returns the name of this node
	 *
	 * @return name
	 */
	String getName();

	/**
	 * Sets the name of this node
	 *
	 * @param name the name
	 */
	void setName(String name);
}
