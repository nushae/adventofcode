package com.nushae.aoc.domain;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Generic weighted directed graph node class for use in graph algorithms, for finite graphs that are statically defined
 */
public class BasicGraphNode<T> implements GraphNode<T> {
	private String name;
	private Point location; // optional link to a grid
	private final Map<GraphNode<T>, Long> adjacentNodes;
	private T payload;

	public BasicGraphNode(String name, T payload) {
		this.name = name;
		adjacentNodes = new HashMap<>();
		this.payload = payload;
	}

	public BasicGraphNode(String name, Point loc, T payload) {
		this.name = name;
		this.location = loc;
		adjacentNodes = new HashMap<>();
		this.payload = payload;
	}

	/**
	 * Returns all nodes in the graph reachable from this one. If the graph is not a disconnected graph this will
	 * return all nodes in the graph
	 * @return Set of all GraphNodes reachable from this one
	 */
	public Set<GraphNode<T>> getExtent() {
		Set<GraphNode<T>> extent = new HashSet<>();
		reachMore(extent);
		return extent;
	}

	private void reachMore(Set<GraphNode<T>> alreadyReached) {
		alreadyReached.add(this);
		for (GraphNode<T> adjacent : adjacentNodes.keySet()) {
			if (!alreadyReached.contains(adjacent)) {
				BasicGraphNode<T> adj = (BasicGraphNode<T>) adjacent;
				adj.reachMore(alreadyReached);
			}
		}
	}

	/**
	 * Returns the distance to the passed GraphNode, or Long.MAX_VALUE if that node is not adjacent to this one.
	 *
	 * @param neighbour the node to return the distance to
	 * @return distance to node or Long.MAX_VALUE if node is not adjacent
	 */
	public long getDistanceTo(BasicGraphNode<T> neighbour) {
		return adjacentNodes.getOrDefault(neighbour, Long.MAX_VALUE);
	}

	public Point getLocation() {
		return location;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public T getPayload() {
		return payload;
	}

	@Override
	public void setPayload(T payload) {
		this.payload = payload;
	}

	@Override
	public void addNeighbour(GraphNode<T> neighbour, long distanceToNeighbour) {
		adjacentNodes.put(neighbour, distanceToNeighbour);
	}

	@Override
	public Map<GraphNode<T>, Long> getAdjacentNodes() {
		return adjacentNodes;
	}

	public String toString() {
		return name + "[" + (location != null ? "(" +location.x + "," + location.y + ") " : "") + payload.toString() + "]";
	}
}
