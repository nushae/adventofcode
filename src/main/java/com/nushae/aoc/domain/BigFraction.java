package com.nushae.aoc.domain;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;
import java.util.Objects;

@Getter
public class BigFraction extends Number implements Comparable<BigFraction> {

	public static final BigFraction ZERO = BigFraction.valueOf(0, 1);
	public static final BigFraction ONE = BigFraction.valueOf(1, 1);
	public static final BigFraction HALF = BigFraction.valueOf(1, 2);
	public static final BigFraction THIRD = BigFraction.valueOf(1, 3);
	public static final BigFraction TENTH = BigFraction.valueOf(1, 10);
	public static final BigFraction PERCENT = BigFraction.valueOf(1, 100);

	private final BigInteger enumerator;
	private final BigInteger denominator;

	public BigFraction(BigInteger number) {
		this (number, BigInteger.ONE);
	}

	public BigFraction(@NotNull BigInteger a, @NotNull BigInteger b) {
		if (b.equals(BigInteger.ZERO)) {
			throw new IllegalArgumentException("Division by zero");
		}
		if (a.equals(BigInteger.ZERO)) {
			enumerator = a;
			denominator = BigInteger.ONE;
			return;
		}
		int sign = b.signum();
		BigInteger ggd = a.gcd(b);
		enumerator = a.divide(ggd).multiply(BigInteger.valueOf(sign));
		denominator = b.divide(ggd).multiply(BigInteger.valueOf(sign));
	}

	public static BigFraction valueOf(long a) {
		return new BigFraction(BigInteger.valueOf(a));
	}

	public static BigFraction valueOf(long a, long b) {
		return new BigFraction(BigInteger.valueOf(a), BigInteger.valueOf(b));
	}

	public static BigFraction unitFractionOf(long a) {
		return BigFraction.valueOf(1, a);
	}

	public BigFraction reciprocal() {
		return new BigFraction(denominator, enumerator);
	}

	public boolean isWholeNumber() {
		return BigInteger.ONE.equals(denominator);
	}

	// a/b + c/d = ad/bd + bc/bd = (ad+bc)/bd
	public BigFraction add(BigFraction other) {
		return new BigFraction(
				this.enumerator.multiply(other.denominator).add(other.enumerator.multiply(this.denominator)),
				this.denominator.multiply(other.denominator)
		);
	}

	// a/b - c/d = ad/bd - bc/bd = (ad-bc)/bd
	public BigFraction subtract(BigFraction other) {
		return new BigFraction(
				this.enumerator.multiply(other.denominator).subtract(other.enumerator.multiply(this.denominator)),
				this.denominator.multiply(other.denominator)
		);
	}

	// a/b * c/d = ab/cd
	public BigFraction multiply(BigFraction other) {
		return new BigFraction(
				this.enumerator.multiply(other.enumerator),
				this.denominator.multiply(other.denominator));
	}

	// a/b / c/d = a/b * d/c = ad/bc
	public BigFraction divide(BigFraction other) {
		return new BigFraction(
				this.enumerator.multiply(other.denominator),
				this.denominator.multiply(other.enumerator));
	}

	@Override
	public int compareTo(BigFraction o) {
		BigFraction diff = this.subtract(o);
		return diff.denominator.multiply(diff.enumerator).compareTo(BigInteger.ZERO);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BigFraction that = (BigFraction) o;
		return enumerator.equals(that.enumerator) && denominator.equals(that.denominator);
	}

	@Override
	public int hashCode() {
		return Objects.hash(enumerator, denominator);
	}

	@Override
	public int intValue() {
		return enumerator.divide(denominator).intValue();
	}

	@Override
	public long longValue() {
		return enumerator.divide(denominator).longValue();
	}

	// not happy with this approach, for now it will do
	@Override
	public float floatValue() {
		return enumerator.floatValue() / denominator.floatValue();
	}

	// not happy with this approach, for now it will do
	@Override
	public double doubleValue() {
		return enumerator.doubleValue() / denominator.doubleValue();
	}

	@Override
	public String toString() {
		return this.enumerator + (this.denominator.equals(BigInteger.ONE) ? "" : ("/" + this.denominator));
	}
}
