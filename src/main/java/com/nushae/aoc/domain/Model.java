package com.nushae.aoc.domain;

import java.util.List;

public interface Model<T> {
	List<T> getElements();
}
