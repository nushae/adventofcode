package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.loadInputFromData;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

@Log4j2
public class Exercise14 {

	List<String> lines;

	public Exercise14() {
		lines = new ArrayList<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long result = 0;
		for (int i=0; i< 128; i++) {
			Exercise10 knothasher = new Exercise10(256, lines.get(0) + "-" + i);
			result += new BigInteger(knothasher.doPart2(), 16).bitCount();
		}
		return result;
	}

	public long doPart2() {
		char[][] grid = new char[128][128];
		for (int i=0; i< 128; i++) {
			Exercise10 knothasher = new Exercise10(256, lines.get(0) + "-" + i);
			String row = new BigInteger(knothasher.doPart2(), 16).toString(2);
			row = StringUtils.repeat('0', 128 - row.length()) + row;
			for (int c = 0; c < 128; c++) {
				grid[c][i] = row.charAt(c) == '1' ? '#' : '.';
			}
		}
		// repeatedly find the topleftmost '#' and erase all # in the connected area, until all have been erased
		long result = 0;
		int x = 0;
		int y = 0;
		while (x < 128) {
			if (grid[x][y] != '#') {
				y++;
				if (y > 127) {
					y = 0;
					x++;
				}
			} else {
				result++;
				floodFill(grid, x, y);
			}
		}
		return result;
	}

	public void floodFill(char[][] grid, int x, int y) {
		grid[x][y] = 'A'; // anything unequal to '.' and '#' is fine
		for (Point p : ORTHO_DIRS) {
			int newX = x + p.x;
			int newY = y + p.y;
			if (newX >=0 && newX < 128 && newY >= 0 && newY < 128 && grid[newX][newY] == '#') {
				floodFill(grid, newX, newY);
			}
		}
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14("ffayrhll");

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
