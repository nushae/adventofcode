package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
public class Exercise03 {
	//   F   F   F   F   E
	//   G   B   B   A   E
	//   G   C   1   A   E
	//   G   C   D   D   E
	//   G   H   H   H   H
	//   (D = 3, H = 5, etc)
	// The bottom right corner of each ring is an odd square X^2. Note that X^2+4X+4 = (X+2)^2.
	// In other words, there are 4 strips of X+1 squares that form the next ring.
	//
	// Let's say '1' is at (0,0). What are the coordinates for N?
	//
	// First, root = the largest ODD root <= N, sits in the bottom-right corner of the outermost complete ring with
	// coordinates (floor(root / 2), - floor(root / 2)). Let's call that point (X, -X)
	// Each side of the next ring is root+1 squares, with the eastern side (squares root*root + 1 through root*root + root+1) having coordinates
	// (X+1, -X+0) ... (X+1, -X+root). So if N - root*root <= root+1, use this.
	// For the next root+1 numbers the coordinates are (X,-X+root) ... (X-root, -X+root)
	// for the next root+1 numbers the coordinates are (X-root, -X+root+1) ... (X-root, -X+1)
	// for the last root+1 numbers the coordinates are (X-root, -X) ... (X, -X)

	Map<Point, Integer> squares;

	public long doPart1(int square) {
		Point coords = makeCoords(square);
		return manhattanDistance(coords.x, coords.y);
	}

	public long doPart2(int square)	{
		squares = new HashMap<>();
		int num = 2;
		int value = 1;
		squares.put(new Point(0, 0), 1);
		while (value <= square) {
			Point loc = makeCoords(num);
			System.out.println(num + " -> " + loc);
			value = 0;
			for (Point dir : KING_DIRS) {
				value += squares.getOrDefault(new Point(loc.x + dir.x, loc.y + dir.y), 0);
			}
			squares.put(loc, value);
			num++;
		}
		System.out.println();
		for (Map.Entry<Point, Integer> entry : squares.entrySet()) {
			System.out.println(entry.getKey() + " -> " + entry.getValue());
		}
		return value;
	}

	/**
	 * finds the coordinates of the Nth square on the spiral, the 1st square being at 0,0, the second at (1,0) and spiraling clockwise
	 *
	 * @param number int the number of the square
	 * @return Point the coordinates of that square
	 */
	public Point makeCoords(int number) {
		int root = (int) Math.sqrt(number);
		if (root % 2 == 0) { // must be the largest ODD root <= square
			root--;
		}
		int bottomRight = root*root;
		int remainder = number - bottomRight;
		int rotates = (remainder - 1) / (root + 1);
		int distance = (remainder - 1) % (root + 1)  + 1;
		return rot90R(rotates, new Point(root/2 + (remainder == 0 ? 0 : 1), -root/2 + distance - (remainder == 0 ? 0 : 1)));
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(265149));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(265149));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
