package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise04 {

	ArrayList<String> lines;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long result = 0;
		for (String passphrase : lines) {
			Set<String> unique = new HashSet<>();
			List<String> all = new ArrayList<>();
			for (String word : passphrase.split("\\s+")) {
				unique.add(word);
				all.add(word);
			}
			result += (unique.size() == all.size() ? 1 : 0);
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		for (String passphrase : lines) {
			List<String> words = new ArrayList<>();
			for (String word : passphrase.split("\\s+")) {
				words.add(word.chars().sorted().collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString());
			}
			boolean valid = true;
			for (int i=0; valid && i<words.size()-1; i++) {
				for (int j=i+1; valid && j<words.size(); j++) {
					valid = !words.get(i).equals(words.get(j));
				}
			}
			result += valid ? 1 : 0;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2017, 4));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
