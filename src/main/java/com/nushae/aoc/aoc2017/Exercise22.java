package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.rot90L;
import static com.nushae.aoc.util.MathUtil.rot90R;

@Log4j2
public class Exercise22 {

	List<String> lines;
	Map<Point, Character> cluster;
	Point location;
	Point dir;
	int minX;
	int maxX;
	int minY;
	int maxY;

	public Exercise22() {
		lines = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		int size = lines.size();
		cluster = new HashMap<>();
		minX = -size/2;
		minY = -size/2;
		maxX = size/2;
		maxY = size/2;
		for (int row = 0; row < size; row++) {
			String line = lines.get(row);
			for (int col = 0; col < size; col ++) {
				cluster.put(new Point(col - size/2, row - size/2), line.charAt(col));
			}
		}
	}

	// The display method reveals the virus enters a repetitive pattern towards the bottom-right after about 4400-4500 generations
	public long doPart1(int bursts) {
		processInput();
		dir = new Point(0, -1);
		location = new Point(0,0);
		long result = 0;
		for (int i = 0; i < bursts; i++) {
			if (cluster.getOrDefault(location, '.') == '#') {
				dir = rot90R(1, dir);
				cluster.put(location, '.');
			} else {
				dir = rot90L(1, dir);
				cluster.put(location, '#');
				result++;
			}
			minX = Math.min(minX, location.x);
			minY = Math.min(minY, location.y);
			maxX = Math.max(maxX, location.x);
			maxY = Math.max(maxY, location.y);
			location = new Point(location.x + dir.x, location.y + dir.y);
		}
		displayCluster();
		return result;
	}

	// new rules! This set does not appear to result in a repetitive pattern as before...
	public long doPart2(int bursts) {
		processInput();
		dir = new Point(0, -1);
		location = new Point(0,0);
		long result = 0;
		for (int i = 0; i < bursts; i++) {
			switch (cluster.getOrDefault(location, '.')) {
				case '#' -> {
					dir = rot90R(1, dir);
					cluster.put(location, '+');
				} case '.' -> {
					dir = rot90L(1, dir);
					cluster.put(location, '-');
				} case '+' -> {
					dir = rot90L(2, dir);
					cluster.put(location, '.');
				} case '-' -> {
					cluster.put(location, '#');
					result++;
				}
			}
			minX = Math.min(minX, location.x);
			minY = Math.min(minY, location.y);
			maxX = Math.max(maxX, location.x);
			maxY = Math.max(maxY, location.y);
			location = new Point(location.x + dir.x, location.y + dir.y);
		}
		displayCluster();
		return result;
	}

	public void displayCluster() {
		for (int row = minY; row <= maxY; row++) {
			for (int col = minX; col <= maxX; col++) {
				if (location.x == col && location.y == row) {
					switch (cluster.getOrDefault(new Point(col, row), '.')) {
						case '.':
							System.out.print('O');
							break;
						case '#':
							System.out.print('X');
							break;
						case '+':
							System.out.print('F');
							break;
						case '-':
							System.out.print('W');
							break;
					}
				} else {
					System.out.print(cluster.getOrDefault(new Point(col, row), '.'));
				}
			}
			System.out.println();
		}
		System.out.println("Facing: " + dir);
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(aocPath(2017, 22));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(10000));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(10000000));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
