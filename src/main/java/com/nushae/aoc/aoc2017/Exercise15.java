package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Exercise15 {

	public long doPart1(long genANumber, long genBNumber) {
		long result = 0;
		for (int i=0; i < 40000000; i++) {
			genANumber = genANumber * 16807 % 2147483647;
			genBNumber = genBNumber * 48271 % 2147483647;
			if (genANumber % 65536 == genBNumber % 65536) {
				result++;
			}
		}
		return result;
	}

	public long doPart2(long genANumber, long genBNumber) {
		long result = 0;
		for (int i=0; i < 5000000; i++) {
			do {
				genANumber = genANumber * 16807 % 2147483647;
			} while (genANumber % 4 != 0);
			do {
				genBNumber = genBNumber * 48271 % 2147483647;
			} while (genBNumber % 8 != 0);
			if (genANumber % 65536 == genBNumber % 65536) {
				result++;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(512, 191));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(512, 191));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
