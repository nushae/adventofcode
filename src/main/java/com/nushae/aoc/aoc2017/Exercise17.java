package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Exercise17 {

	public long doPart1(int steps) {
		List<Integer> numbers = new ArrayList<>();
		int count = 1;
		numbers.add(0);
		int position = 0;
		while (count <= 2017) {
			position = (position + steps + 1) % numbers.size();
			numbers.add(position, count);
			count++;
		}
		return numbers.get((position+1) % numbers.size());
	}

	public long doPart2(int steps) {
		int count = 1;
		int listsize = 1;
		int position = 0;
		long numberAfterZero = 0;
		while (count <= 50000000) {
			position = (position + steps + 1) % listsize;
			listsize++; // simulate insert of number -> virtual size increases
			// make sure we 'keep the 0 at the front' by moving position to the back if it is ever 0. This way we don't
			// really need to maintain a list, just the size of the (virtual) list, and whenever we add a number at
			// position 1, we remember just that number, since it's the only one we care about.
			if (position == 0) {
				position = listsize-1;
			};
			if (position == 1) { // remember number
				numberAfterZero = count;
			}
			count++;
		}
		return numberAfterZero;
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(386));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(386));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
