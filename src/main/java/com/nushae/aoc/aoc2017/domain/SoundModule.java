package com.nushae.aoc.aoc2017.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SoundModule {
	private final Map<Character, Long> registers;
	List<String> program;
	private long lastFreq;
	private int instructionPointer;

	public SoundModule() {
		registers = new HashMap<>();
		program = new ArrayList<>();
		lastFreq = 0;
		instructionPointer = 0;
	}

	public void setProgram(List<String> instr) {
		program = new ArrayList<>();
		program.addAll(instr);
		lastFreq = 0;
		instructionPointer = 0;
	}

	public boolean nextInstruction() {
		if (0 <= instructionPointer && program.size() > instructionPointer) {
			String stmt = program.get(instructionPointer);
			String[] parts = stmt.split("\\s+");
			switch (parts[0]) {
				case "snd": // play sound
					lastFreq = getValue(parts[1]);
					break;
				case "set": // set register
					registers.put(parts[1].charAt(0), getValue(parts[2]));
					break;
				case "add": // increase register
					registers.put(parts[1].charAt(0), getValue(parts[1]) + getValue(parts[2]));
					break;
				case "mul": // multiply register
					registers.put(parts[1].charAt(0), getValue(parts[1]) * getValue(parts[2]));
					break;
				case "mod": // modulo register
					registers.put(parts[1].charAt(0), getValue(parts[1]) % getValue(parts[2]));
					break;
				case "rcv": // recover if nonzero
					if (getValue(parts[1]) != 0) {
						return true;
					}
					break;
				case "jgz": // jump if greater than zero
					if (getValue(parts[1]) > 0) {
						instructionPointer += getValue(parts[2]) - 1;
					}
					break;
			}
			instructionPointer++;
			return false;
		} else {
			return true;
		}
	}

	public long runProgram() {
		lastFreq = 0;
		instructionPointer = 0;
		while (!nextInstruction()) {
			// NOP!
		}
		return lastFreq;
	}

	private long getValue(String input) {
		char x = input.charAt(0);
		if (x == '-' || x >= '0' && x <= '9') { // number
			return Long.parseLong(input);
		} else {
			return registers.getOrDefault(x, 0L);
		}
	}
}
