package com.nushae.aoc.aoc2017.domain;

public interface DanceInstruction {
	String apply(String input);
}
