package com.nushae.aoc.aoc2017.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PartnerInstruction implements DanceInstruction {
	public static final Pattern REPRESENTATION = Pattern.compile("p([a-z])/([a-z])");
	private final char p1;
	private final char p2;

	public PartnerInstruction(String rep) {
		Matcher m = REPRESENTATION.matcher(rep);
		if (m.find()) {
			p1 = m.group(1).charAt(0);
			p2 = m.group(2).charAt(0);
		} else {
			throw new IllegalArgumentException("This does not represent an Exchange instruction: '" + rep + "'");
		}
	}

	public String apply(String input) {
		int p1pos = input.indexOf(p1);
		int p2pos = input.indexOf(p2);
		int pos1 = Math.min(p1pos, p2pos);
		int pos2 = Math.max(p1pos, p2pos);
		if (pos1 >=0 && pos1 != pos2) {
			return (pos1 > 0 ? input.substring(0, pos1) : "") +
					input.charAt(pos2) +
					(pos1+1 < pos2 ? input.substring(pos1 + 1, pos2) : "") +
					input.charAt(pos1) +
					input.substring(pos2 + 1);
		} else {
			System.out.println(pos1 + " / " + pos2 + " for input length " + input.length() + " so not partnering '"+p1+"' and '"+p2+"' in '"+input+"'.");
			return input;
		}
	}
}
