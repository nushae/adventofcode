package com.nushae.aoc.aoc2017.domain;

import java.util.Map;

public class Instruction {
	public String onRegister;
	public int incOrDec;
	public long amount;
	public String condReg;
	public long condValue;
	public String condOp;

	public boolean evalCond(Map<String, Long> registers) {
		long regVal = registers.getOrDefault(condReg, 0L);
		switch (condOp) {
			case "<":
				return (regVal < condValue);
			case ">":
				return (regVal > condValue);
			case "<=":
				return (regVal <= condValue);
			case ">=":
				return (regVal >= condValue);
			case "!=":
				return (regVal != condValue);
			case "==":
				return (regVal == condValue);
		}
		return false;
	}

	public long apply(Map<String, Long> registers) {
		if (evalCond(registers)) {
			registers.put(onRegister, registers.getOrDefault(onRegister, 0L) + incOrDec * amount);
			return registers.get(onRegister);
		}
		return Long.MIN_VALUE;
	}
}
