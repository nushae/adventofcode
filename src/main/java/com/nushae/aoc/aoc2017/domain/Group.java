package com.nushae.aoc.aoc2017.domain;

import java.util.ArrayList;
import java.util.List;

public class Group implements DataStreamElement {
	public List<DataStreamElement> children = new ArrayList<>();

	public void addChild(DataStreamElement dse) {
		children.add(dse);
	}

	public long totalScore(int depth) {
		long result = depth;
		for (DataStreamElement dse : children) {
			result += dse.totalScore(depth + 1);
		}
		return result;
	}

	public long totalGarbageChars() {
		long result = 0;
		for (DataStreamElement dse : children) {
			result += dse.totalGarbageChars();
		}
		return result;
	}
}
