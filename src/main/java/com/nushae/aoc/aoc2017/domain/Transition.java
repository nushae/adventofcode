package com.nushae.aoc.aoc2017.domain;

// state, input -> output, move, new state
// in other words a constant 5 tuple
public class Transition {
	public final String oldState;
	public final int input;
	public final int output;
	public final String newState;
	public final boolean moveLeft;

	public Transition(String oldState, int input, String newState, int output, boolean moveLeft) {
		this.oldState = oldState;
		this.newState = newState;
		this.input = input;
		this.output = output;
		this.moveLeft = moveLeft;
	}

	@Override
	public String toString() {
		return oldState + ", " + input + " -> " + output + ", " + (moveLeft ? "<" : ">") + newState;
	}
}
