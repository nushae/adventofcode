package com.nushae.aoc.aoc2017.domain;

public class Garbage implements DataStreamElement {
	public String contents;

	public long totalScore(int depth) {
		return 0L;
	}

	public long totalGarbageChars() {
		return contents.length();
	}
}
