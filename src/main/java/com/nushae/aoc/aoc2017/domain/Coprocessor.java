package com.nushae.aoc.aoc2017.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Coprocessor {
	private Map<Character, Long> registers;
	private long mulUsed;
	List<String> program;
	private int instructionPointer;

	public Coprocessor() {
		registers = new HashMap<>();
		program = new ArrayList<>();
		instructionPointer = 0;
		mulUsed = 0;
	}

	public void setProgram(List<String> instr) {
		program = new ArrayList<>();
		program.addAll(instr);
		instructionPointer = 0;
		mulUsed = 0;
	}

	public boolean nextInstruction() {
		if (0 <= instructionPointer && program.size() > instructionPointer) {
			String stmt = program.get(instructionPointer);
			String[] parts = stmt.split("\\s+");
			switch (parts[0]) {
				case "set": // set register
					registers.put(parts[1].charAt(0), getValue(parts[2]));
					break;
				case "sub": // increase register
					registers.put(parts[1].charAt(0), getValue(parts[1]) - getValue(parts[2]));
					break;
				case "mul": // multiply register
					registers.put(parts[1].charAt(0), getValue(parts[1]) * getValue(parts[2]));
					mulUsed++;
					break;
				case "jnz": // jump if greater than zero
					if (getValue(parts[1]) != 0) {
						instructionPointer += getValue(parts[2]) - 1;
					}
					break;
			}
			instructionPointer++;
			return false;
		} else {
			return true;
		}
	}

	public long runProgram() {
		registers = new HashMap<>();
		instructionPointer = 0;
		mulUsed = 0;
		while (!nextInstruction()) {
			// NOP!
		}
		return mulUsed;
	}

	private long getValue(String input) {
		char x = input.charAt(0);
		if (x == '-' || x >= '0' && x <= '9') { // number
			return Long.parseLong(input);
		} else {
			return registers.getOrDefault(x, 0L);
		}
	}
}
