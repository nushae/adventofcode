package com.nushae.aoc.aoc2017.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpinInstruction implements DanceInstruction {
	public static final Pattern REPRESENTATION = Pattern.compile("s(\\d+)");
	private final int spinamount;

	public SpinInstruction(String rep) {
		Matcher m = REPRESENTATION.matcher(rep);
		if (m.find()) {
			spinamount = Integer.parseInt(m.group(1));
		} else {
			throw new IllegalArgumentException("This does not represent a Spin instruction: '" + rep + "'");
		}
	}

	public String apply(String input) {
		if (spinamount < input.length()) {
			return input.substring(input.length() - spinamount) + input.substring(0, input.length() - spinamount);
		} else {
			System.out.println("Spin amount >= input length; not spinning.");
			return input;
		}
	}
}
