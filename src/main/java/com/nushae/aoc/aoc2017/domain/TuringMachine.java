package com.nushae.aoc.aoc2017.domain;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TuringMachine {
	private final Map<Integer, Integer> tape; // default is 0
	private final Set<String> states;
	private final Map<String, Transition> transitions;

	public TuringMachine() {
		tape = new HashMap<>();
		states = new HashSet<>();
		transitions = new HashMap<>();
	}

	public void addTransition(Transition t) {
		transitions.put(t.oldState + t.input, t);
	}

	public void addState(String state) {
		states.add(state);
	}

	public long runWithDiagnostic(String state, int steps) {
		String currentState = state;
		int cursor = 0;
		for (int i = 0; i < steps; i++) {
			Transition t = transitions.get(currentState + tape.getOrDefault(cursor, 0));
			tape.put(cursor, t.output);
			cursor = t.moveLeft ? cursor - 1 : cursor + 1;
			currentState = t.newState;
		}
		long result = 0;
		for (int v : tape.values()) {
			result += v;
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("States:\n");
		sb.append("[").append(StringUtils.join(states, " ")).append("]\n");
		sb.append("Transitions:\n");
		for (Transition t : transitions.values()) {
			sb.append(t).append("\n");
		}
		return sb.toString();
	}
}
