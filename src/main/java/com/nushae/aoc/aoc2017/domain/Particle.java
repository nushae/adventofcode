package com.nushae.aoc.aoc2017.domain;

import org.apache.commons.lang3.StringUtils;

public class Particle {
	private final long[] initialPosition;
	private final long[] initialVelocity;
	private final long[] initialAcceleration;
	private final long[] currentPosition;
	private final long[] currentVelocity;
	private final long[] currentAcceleration;

	public Particle(int x, int y, int z, int vx, int vy, int vz, int ax, int ay, int az) {
		initialPosition = new long[3];
		initialPosition[0] = x;
		initialPosition[1] = y;
		initialPosition[2] = z;
		initialVelocity = new long[3];
		initialVelocity[0] = vx;
		initialVelocity[1] = vy;
		initialVelocity[2] = vz;
		initialAcceleration = new long[3];
		initialAcceleration[0] = ax;
		initialAcceleration[1] = ay;
		initialAcceleration[2] = az;
		currentPosition = initialPosition;
		currentVelocity = initialVelocity;
		currentAcceleration = initialAcceleration;
	}

	public void update() {
		for (int i = 0; i < 3; i++) {
			currentVelocity[i] += currentAcceleration[i];
			currentPosition[i] += currentVelocity[i];
		}
	}

	public long[] getPos() {
		return currentPosition;
	}

	public long predictDistance(long t) {
		long effectivePositionX = initialAcceleration[0] * t * t / 2 + initialVelocity[0] * t + initialPosition[0];
		long effectivePositionY = initialAcceleration[1] * t * t / 2 + initialVelocity[1] * t + initialPosition[1];
		long effectivePositionZ = initialAcceleration[2] * t * t / 2 + initialVelocity[2] * t + initialPosition[2];
		return Math.abs(effectivePositionX) + Math.abs(effectivePositionY) + Math.abs(effectivePositionZ);
	}

	public long getManhattanDist() {
		return Math.abs(currentPosition[0]) + Math.abs(currentPosition[1]) + Math.abs(currentPosition[2]);
	}

	@Override
	public String toString() {
		return StringUtils.join(currentPosition, ',') + " - " + StringUtils.join(currentVelocity, ',') + " - " + StringUtils.join(currentAcceleration, ',') + "[" + StringUtils.join(initialPosition, ',') + " - " + StringUtils.join(initialVelocity, ',') + " - " + StringUtils.join(initialAcceleration, ',') + "]";
	}
}
