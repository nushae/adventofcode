package com.nushae.aoc.aoc2017.domain;

public interface DataStreamElement {
	long totalScore(int depth);

	long totalGarbageChars();
}
