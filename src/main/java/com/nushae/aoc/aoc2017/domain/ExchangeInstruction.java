package com.nushae.aoc.aoc2017.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExchangeInstruction implements DanceInstruction {
	public static final Pattern REPRESENTATION = Pattern.compile("x(\\d+)/(\\d+)");
	private final int pos1;
	private final int pos2;
	private String rep;

	public ExchangeInstruction(String rep) {
		this.rep = rep;
		Matcher m = REPRESENTATION.matcher(rep);
		if (m.find()) {
			int a = Integer.parseInt(m.group(1));
			int b = Integer.parseInt(m.group(2));
			pos1 = Math.min(a, b);
			pos2 = Math.max(a, b);
		} else {
			throw new IllegalArgumentException("This does not represent an Exchange instruction: '" + rep + "'");
		}
	}

	public String apply(String input) {
		if (pos1 != pos2 && pos2 < input.length()) {
			return (pos1 > 0 ? input.substring(0, pos1) : "") +
					input.charAt(pos2) +
					(pos1+1 < pos2 ? input.substring(pos1 + 1, pos2) : "") +
					input.charAt(pos1) +
					input.substring(pos2 + 1);
		} else {
			return input;
		}
	}
}
