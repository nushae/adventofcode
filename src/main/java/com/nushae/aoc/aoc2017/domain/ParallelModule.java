package com.nushae.aoc.aoc2017.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParallelModule {
	private Map<Character, Long> registers;
	private final long ID;
	private long sent;
	private List<Long> otherQueue;
	private List<Long> queue;
	List<String> program;
	private long lastFreq;
	private int instructionPointer;

	public ParallelModule(int programID) {
		ID = programID;
		registers = new HashMap<>();
		registers.put('p', ID);
		program = new ArrayList<>();
		queue = new ArrayList<>();
		lastFreq = 0;
		instructionPointer = 0;
		sent = 0;
	}

	public void setProgram(List<String> instr) {
		program = new ArrayList<>();
		program.addAll(instr);
		queue = new ArrayList<>();
		registers = new HashMap<>();
		registers.put('p', ID);
		lastFreq = 0;
		instructionPointer = 0;
		sent = 0;
	}

	public void setReceiverQueue(List<Long> queue) {
		this.otherQueue = queue;
	}

	public long getSent() {
		return sent;
	}

	public List<Long> getQueue() {
		return queue;
	}

	public boolean nextInstruction() {
		if (0 <= instructionPointer && program.size() > instructionPointer) {
			String stmt = program.get(instructionPointer);
			String[] parts = stmt.split("\\s+");
			switch (parts[0]) {
				case "snd": // send
					otherQueue.add(getValue(parts[1]));
					sent++;
					break;
				case "set": // set register
					registers.put(parts[1].charAt(0), getValue(parts[2]));
					break;
				case "add": // increase register
					registers.put(parts[1].charAt(0), getValue(parts[1]) + getValue(parts[2]));
					break;
				case "mul": // multiply register
					registers.put(parts[1].charAt(0), getValue(parts[1]) * getValue(parts[2]));
					break;
				case "mod": // modulo register
					registers.put(parts[1].charAt(0), getValue(parts[1]) % getValue(parts[2]));
					break;
				case "rcv": // receive
					if (queue.size() > 0) {
						registers.put(parts[1].charAt(0), queue.remove(0));
					} else {
						return true; // awaiting input
					}
					break;
				case "jgz": // jump if greater than zero
					if (getValue(parts[1]) > 0) {
						instructionPointer += getValue(parts[2]) - 1;
					}
					break;
			}
			instructionPointer++;
			return false;
		} else {
			return true; // not awaiting input but halted
		}
	}

	private long getValue(String input) {
		char x = input.charAt(0);
		if (x == '-' || x >= '0' && x <= '9') { // number
			return Long.parseLong(input);
		} else {
			return registers.getOrDefault(x, 0L);
		}
	}
}
