package com.nushae.aoc.aoc2017.domain;

import java.util.HashSet;
import java.util.Set;

public class Bridge {
	private final int lowPort;
	private final int highPort;
	private boolean lowUsed;
	private boolean highUsed;

	public Bridge(int port1, int port2) {
		lowPort = Math.min(port1, port2);
		highPort = Math.max(port1, port2);
	}

	public boolean hasFreePort(int port) {
		return !lowUsed && lowPort == port || !highUsed && highPort == port;
	}

	public void usePort(int port) {
		if (lowPort != port && highPort != port) {
			throw new IllegalArgumentException("This bridge does not have this port: " + port);
		}
		if (!lowUsed && lowPort == port) {
			lowUsed = true;
		} else if (!highUsed && highPort == port) {
			highUsed = true;
		} else {
			throw new IllegalArgumentException("All ports already used");
		}
	}

	public void unusePort(int port) {
		if (lowPort != port && highPort != port) {
			throw new IllegalArgumentException("This bridge does not have this port: " + port);
		}
		if (highUsed && highPort == port) {
			highUsed = false;
		} else if (lowUsed && lowPort == port) {
			lowUsed = false;
		} else {
			throw new IllegalArgumentException("No ports in use");
		}
	}

	public Set<Integer> getFreePorts() {
		Set<Integer> result = new HashSet<>();
		if (!lowUsed) {
			result.add(lowPort);
		}
		if (!highUsed) {
			result.add(highPort);
		}
		return result;
	}

	@Override
	public String toString() {
		return lowPort + "/" + highPort;
	}
}
