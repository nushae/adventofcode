package com.nushae.aoc.aoc2017.domain;

import java.util.ArrayList;
import java.util.List;

public class DiscProg {
	public DiscProg parent = null;
	public List<DiscProg> children;
	public int weight;
	public String name;

	public DiscProg(String name, int weight) {
		children = new ArrayList<>();
		this.weight = weight;
		this.name = name;
	}

	public int reBalance() {
		DiscProg curProg = null;
		int curWt = 0;
		int i = 0;
		for (; i < children.size(); i++) {
			if (i == 0) {
				curProg = children.get(i);
				curWt = curProg.totalWeight();
			} else {
				DiscProg test = children.get(i);
				int testWt = test.totalWeight();
				if (curWt != testWt) { // FOUND, now which one is it...
					if (i > 1 || curWt == children.get(i + 1).totalWeight()) { // found, and it's this one
						curProg = test;
					}
					// i == 1 && curWt != children.get(i+1).totalWeight() -> it's curProg
					break;
				}
			}
		}
		if (i < children.size()) {
			return curProg.reBalance();
		}
		// all were same weight, so it's me :(
		for (DiscProg dp : parent.children) {
			if (!dp.name.equals(name)) {
				return weight - (totalWeight() - dp.totalWeight());
			}
		}
		return -1;
	}

	public boolean isBalanced() {
		boolean result = true;
		int wt = -1;
		for (DiscProg child : children) {
			if (wt < 0) {
				wt = child.totalWeight();
			} else {
				result = wt == child.totalWeight();
			}
		}
		return result;
	}

	public int totalWeight() {
		int result = weight;
		for (DiscProg dp : children) {
			result += dp.totalWeight();
		}
		return result;
	}
}
