package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.Instruction;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise08 {

	private static final Pattern INSTRUCTION = Pattern.compile("([a-z]+) (inc|dec) (-?\\d+) if ([a-z]+) (> |< |>= |<= |!= |== )(-?\\d+)");

	ArrayList<String> lines;
	ArrayList<Instruction> instructions;
	Map<String, Long> registers;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		instructions = new ArrayList<>();
		for (String line : lines) {
			Matcher m = INSTRUCTION.matcher(line);
			if (m.find()) {
				Instruction inst = new Instruction();
				inst.onRegister = m.group(1);
				inst.incOrDec = "inc".equals(m.group(2)) ? 1 : -1;
				inst.amount = Long.parseLong(m.group(3));
				inst.condReg = m.group(4);
				inst.condOp = m.group(5).trim();
				inst.condValue = Long.parseLong(m.group(6));
				instructions.add(inst);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		processInput();
		registers = new HashMap<>();
		for (Instruction inst : instructions) {
			inst.apply(registers);
		}
		long maxVal = Long.MIN_VALUE;
		for (Map.Entry<String, Long> entry : registers.entrySet()) {
			maxVal = Math.max(maxVal, entry.getValue());
		}
		return maxVal;
	}

	public long doPart2() {
		processInput();
		registers = new HashMap<>();
		long maxVal = Long.MIN_VALUE;
		for (Instruction inst : instructions) {
			maxVal = Math.max(maxVal, inst.apply(registers));
		}
		return maxVal;
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2017, 8));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
