package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise05 {

	ArrayList<String> lines;
	ArrayList<Integer> numbers;

	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		numbers = new ArrayList<>();
		for (String line : lines) {
			numbers.add(Integer.parseInt(line));
		}
	}

	public long doPart1() {
		long result = 0;
		processInput();
		int pointer = 0;
		while(pointer >=0 && pointer < numbers.size()) {
			int jump = numbers.get(pointer);
			numbers.set(pointer, jump + 1);
			pointer += jump;
			result++;
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		processInput();
		int pointer = 0;
		while(pointer >=0 && pointer < numbers.size()) {
			int jump = numbers.get(pointer);
			if (jump >= 3) {
				numbers.set(pointer, jump - 1);
			} else {
				numbers.set(pointer, jump + 1);
			}
			pointer += jump;
			result++;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2017, 5));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
