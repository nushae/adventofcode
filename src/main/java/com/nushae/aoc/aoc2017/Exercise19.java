package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.rot90R;

@Log4j2
public class Exercise19 {

	List<String> lines;
	char[][] grid;
	int width;
	int height;

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		width = -1;
		for (String line : lines) {
			width = Math.max(width, line.length());
		}
		height = lines.size();
		grid = new char[width][height];
		for (int r = 0 ; r < lines.size(); r++) {
			String line = lines.get(r);
			for (int c = 0; c < line.length(); c++) {
				grid[c][r] = line.charAt(c);
			}
		}
	}

	public String doPart1() {
		processInput();
		int x;
		for (x = 0; x < width; x++) {
			if (grid[x][0] == '|') {
				break;
			}
		}
		return traceRoute(x, 0, new Point(0, 1)); // +y = down
	}

	public String traceRoute(int x, int y, Point direction) {
		StringBuilder result = new StringBuilder();
		long steps = 0;
		while (grid[x][y] != ' ') {
			if (grid[x][y] >= 'A' && grid[x][y] <= 'Z') {
				result.append(grid[x][y]);
			}
			if (grid[x][y] == '+') { // need to turn
				direction = rot90R(1, direction);
				if (grid[x+direction.x][y+direction.y] == ' ') {
					direction = rot90R(2, direction);
				}
			}
			x += direction.x;
			y += direction.y;
			steps++;
		}
		return result + " (" + steps + " steps)";
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2017, 19));

		// calculates parts 1 and 2 in parallel
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
