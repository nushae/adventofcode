package com.nushae.aoc.aoc2017;

import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise12 {

	private static final Pattern CONNECTIONS = Pattern.compile("(\\d+) <-> (.*)");

	ArrayList<String> lines;
	Map<Integer, GraphNode<Integer>> graphNodes;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		graphNodes = new HashMap<>();
		for (String line : lines) {
			Matcher m = CONNECTIONS.matcher(line);
			if (m.find()) {
				int nodeNum = Integer.parseInt(m.group(1));
				BasicGraphNode<Integer> node = (BasicGraphNode<Integer>) graphNodes.getOrDefault(nodeNum, new BasicGraphNode<>(""+nodeNum, nodeNum));
				graphNodes.put(nodeNum, node);
				for (String adjacent : m.group(2).split(",")) {
					int adjNum = Integer.parseInt(adjacent.trim());
					BasicGraphNode<Integer> adj = (BasicGraphNode<Integer>) graphNodes.getOrDefault(adjNum, new BasicGraphNode<>(""+adjNum, adjNum));
					node.addNeighbour(adj, 1);
					graphNodes.put(adjNum, adj);
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		processWithPattern();
		return(((BasicGraphNode<Integer>) graphNodes.get(0)).getExtent().size());
	}

	public long doPart2() {
		processWithPattern();
		long groups = 0;
		Set<GraphNode<Integer>> found = new HashSet<>();
		for (Map.Entry<Integer,GraphNode<Integer>> entry : graphNodes.entrySet()) {
			if (!found.contains(entry.getValue())) {
				found.addAll(((BasicGraphNode<Integer>) entry.getValue()).getExtent());
				groups++;
			}
		}
		return groups;
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2017, 12));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
