package com.nushae.aoc.aoc2017;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2017, day = 13, title="Packet Scanners", keywords = {"modulo", "visualized", "animated"})
public class Exercise13 extends JPanel {

	private static final Pattern SCANNER_DEF = Pattern.compile("(\\d+):\\s(\\d+)");
	private static final int SIZE = 10;
	private static final int MARGIN = 2;
	public static final int SPEED = 0; // 150 to animate, may not want to do this for part 2 ;)

	ArrayList<String> lines;
	Map<Integer, Point> scanners; // layer -> (actual position, range); position < 0 means moving up, real position = abs(position)
	int totalLayers = 0;
	int maxRange = 0;
	int myPos = 7;
	boolean caught;

	public Exercise13() {
		lines = new ArrayList<>();
		scanners = new HashMap<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		scanners = new HashMap<>();
		totalLayers = 0;
		for (String line : lines) {
			Matcher m = SCANNER_DEF.matcher(line);
			if (m.find()) {
				int layer = Integer.parseInt(m.group(1));
				int range = Integer.parseInt(m.group(2))-1; // we count from 0 in House Wentzler
				scanners.put(layer, new Point(0, range));
				totalLayers = Math.max(totalLayers, layer+1); // compensate for counting from 0
				maxRange = Math.max(maxRange, range);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	// For part 1, we do it the slow, circuitous (heh) way.
	public long doPart1() {
		processInput();
		if (SPEED > 0) {
			return severitySlow(0, false);
		}
		return severity(0);
	}

	// Part 2 will not end in a reasonable time if we do it like part 1, so cleverness ensues:
	// First we note that the exact scanner position for every tick doesn't really matter, just when they are on the top
	// row (position == 0). For range == 3 (positions 0, 1, 2), we have position == 0 at tick 0, 4, 8, 12, etc; for
	// range == 5 (positions 0, 1, 2, 3, 4) we have position == 0 at tick 0, 8, 16, etc. So The unsafe delays for the
	// scanner in layer 0 are at tick 2*N*(scanner.range-1) for N > 0 (we can move into layer 0 at tick 0 because the
	// scanner is already there).
	// Second, since we move from layer to layer at one step per tick, it's a bad run if scanner 1 is at position 0 on
	// tick delay+1, scanner 2 at 0 at delay+2, etc. So it's unsafe if for any scanner, N>0, 2*N*(scanner.range-1) == delay + layer.
	// In other words, the unsafe delays for any scanner are 2*N*(scanner.range-1)-scanner.layer. We accept only a delay
	// that is unequal to all of these values.
	// So for layer 0, this means delay != 2*N*(scanner.range-1), or, delay % (scanner.range-1)*2 != 0
	// For layer 1: delay + 1 != 2*N*(scanner.range-1), or, delay + 1 % (scanner.range-1)*2 != 0
	// In general: delay + scanner.layer != 2*N*(scanner.range-1), or, delay + scanner.layer % (scanner.range-1)*2 != 0
	// since we shifted range to the 0..N-1 range upon input, the formula used will be delay + scanner.layer % (scanner.range)*2 != 0
	// There's probably an even cleverer way involving inverting the conditions to use the chinese remainder theorem,
	// but I can't be arsed rn, plus brute forcing with the above strategy is plenty fast.
	public long doPart2() {
		processInput();
		caught = true;
		int delay = 0;
		for (delay = 0; delay < 10000000; delay++) { // increase bound until we don't find the bound but something lower
			caught = false;
			for(Map.Entry<Integer, Point> scanner : scanners.entrySet()) {
				caught = (scanner.getKey() + delay) % (scanner.getValue().y*2) == 0;
				if (caught) {
					break;
				}
			}
			if (!caught) {
				return delay;
			}
		}
		return delay;
	}

	// the slow circuitous way
	public long severitySlow(int delay, boolean PRINTSTATE) {
		long score = 0;
		caught = false;
		for (int pico = 0; pico < totalLayers + delay; pico++) {
			if (PRINTSTATE) {
				System.out.println("Picosecond " + pico + ":");
			}
			myPos = pico - delay;
			if (PRINTSTATE) {
				printState();
			}
			// check for caughtnessitude
			if (scanners.containsKey(myPos)) {
				if (scanners.get(myPos).x == 0) { // caught
					// If we are caught in layer 0, we get severity 0;
					// therefore, in part 2 we can't use "score != 0" to detect whether we were caught.
					// We use the global boolean caught as a workaround
					caught = true;
					long severity = (long) myPos * (scanners.get(myPos).y+1); // severity = depth * range
					if (PRINTSTATE) {
						System.out.println("Caught in layer " + myPos + " with severity " + severity + "\n");
					}
					score += severity;
				}
			}
			// update scanner positions:
			for (Point scanner : scanners.values()) {
				scanner.x++;
				if (scanner.x == scanner.y) {
					scanner.x = -scanner.x;
				}
			}
			// done, the rest is optional display stuff
			if (PRINTSTATE) {
				printState();
			}
			updateView();
		}
		return score;
	}

	// the direct way
	public long severity(int delay) {
		int result = 0;
		for (Map.Entry<Integer, Point> scanner : scanners.entrySet()) {
			int layer = scanner.getKey();
			int range = scanner.getValue().y;
			if ((layer + delay) % (range * 2) == 0) {
				result += (range + 1) * layer; // the +1 is to compensate for the counting from 0, remember
			}
		}
		return result;
	}

	public void printState() {
		for (int i = 0; i < totalLayers; i++) {
			if (i > 0) {
				System.out.print(" ");
			}
			System.out.printf(" %02d", i);
		}
		System.out.println();
		for (int row = 0; row <= maxRange; row++) {
			for (int col = 0; col < totalLayers; col++) {
				if (col > 0) {
					System.out.print(" ");
				}
				if (row == 0 && myPos == col) {
					System.out.print("(");
				} else if (scanners.containsKey(col) && row <= scanners.get(col).y) {
					System.out.print("[");
				} else if (row == 0 && !scanners.containsKey(col)) {
					System.out.print(".");
				} else {
					System.out.print(" ");
				}
				if (scanners.containsKey(col) && Math.abs(scanners.get(col).x) == row) {
					System.out.print("S");
				} else if (row == 0 && !scanners.containsKey(col)) {
					System.out.print(".");
				} else {
					System.out.print(" ");
				}
				if (row == 0 && myPos == col) {
					System.out.print(")");
				} else if (scanners.containsKey(col) && row <= scanners.get(col).y) {
					System.out.print("]");
				} else if (row == 0 && !scanners.containsKey(col)) {
					System.out.print(".");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	public void updateView() {
		if (SPEED > 0) {
			repaint();
			try {
				Thread.sleep(SPEED);
			} catch (InterruptedException e) {
				// NOP
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		Map<Integer, Point> temp = new HashMap<>(scanners);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.white);
		g2.fillRect(0, 0, getWidth(), getHeight());
		int step = SIZE + MARGIN;
		int xO = (getWidth() - step - totalLayers * step + MARGIN)/2;
		int yO = (getHeight() - step - (maxRange+1) * step + MARGIN)/2;
		for (int i = 0; i < totalLayers; i++) {
			if (temp.containsKey(i)) {
				for (int j = 0; j <= temp.get(i).y; j++) {
					if (j == Math.abs(temp.get(i).x)) {
						g2.setColor(Color.red);
					} else {
						g2.setColor(Color.gray);
					}
					g2.fillRect(xO + step * i, yO + step * j, SIZE - 1, SIZE - 1);
					g2.setColor(Color.black);
					g2.drawRect(xO + step * i, yO + step * j, SIZE - 1, SIZE - 1);
				}
			} else {
				g2.setColor(Color.black);
				g2.fillOval(xO + step * i + (SIZE-MARGIN)/2, yO + (SIZE-MARGIN)/2, MARGIN, MARGIN);
			}
		}
		if (myPos >= 0) {
			g2.setColor(Color.magenta);
			g2.fillRect(xO + myPos * step, yO - step, SIZE-1, SIZE -1);
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2017, 13));

		if (SPEED > 0) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
		}

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// System.out.println(ex.severitySlow(3941460, false)); // should be 0.

		// part 2 - should find 3941460
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
