package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise21 {

	List<String> lines;
	Map<String, String> ruleBook;

	public Exercise21() {
		lines = new ArrayList<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
	}

	// The algorithm ensures that only 16 of the 3x3 rules will ever be used for part 1,
	// but since I don't know part 2 at the time of writing this, I'm just parsing everything. A lot will go unused in other words.
	// Addendum: after seeing part 2, turns out we don't actually need the other rules... Oh well.
	private void processInput() {
		ruleBook = new HashMap<>();
		for (String line : lines) {
			String[] patterns = line.split(" => ");
			// make all variations of left side; leftside.length == square's dimension
			// one reflection and three rotations => 8 versions
			// since there are 2^4 2x2 possibilities, and 2^9 3x3, this will make a managable list of 528 patterns.
			String[] leftside = patterns[0].split("/");
			char[][] square = new char[leftside.length][leftside.length];
			for (int x = 0; x < leftside.length; x++) {
				for (int y = 0; y < leftside.length; y++) {
					square[x][y] = leftside[y].charAt(x);
				}
			}
			Set<String> reps = new HashSet<>();
			reps.add(makeRep(square));
			reps.add(makeRep(makeSymmetry(square, 0, 1)));
			reps.add(makeRep(makeSymmetry(square, 0, 2)));
			reps.add(makeRep(makeSymmetry(square, 0, 3)));
			reps.add(makeRep(makeSymmetry(square, 1, 0)));
			reps.add(makeRep(makeSymmetry(square, 1, 1)));
			reps.add(makeRep(makeSymmetry(square, 1, 2)));
			reps.add(makeRep(makeSymmetry(square, 1, 3)));
			for (String rep : reps) {
				ruleBook.put(rep, patterns[1]);
			}
		}
	}

	char[][] makeSymmetry(char[][] square, int reflections, int rotations) {
		boolean reflect = reflections % 2 == 1;
		rotations = rotations % 4;
		char[][] result = new char[square.length][square.length];
		// reflection, if any
		for (int x = 0; x < square.length; x++) {
			for (int y = 0; y < square.length; y++) {
				if (reflect) {
					result[x][y] = square[square.length-x-1][y];
				} else {
					result[x][y] = square[x][y];
				}
			}
		}
		// rotation(s) if any
		for (int r = 0; r < rotations; r++) {
			char[][] tmp = new char[square.length][square.length];
			for (int x = 0; x < square.length; x++) {
				for (int y = 0; y < square.length; y++) {
					tmp[x][y] = result[square.length-y-1][x];
				}
			}
			result = tmp;
		}
		return result;
	}

	String makeRep(char[][] square) {
		StringBuilder rep = new StringBuilder();
		for (int y = 0; y < square.length; y++) {
			if (rep.length() > 0) {
				rep.append('/');
			}
			for (char[] column : square) {
				rep.append(column[y]);
			}
		}
		return rep.toString();
	}

	// Same code, 5 iterations for part 1, 18 for part 2. The following comment was written
	// before I knew what part 2 would be.

	// I expect it to become unmanageable for larger numbers of iterations, especially in
	// terms of memory... So part 2 might just go there. Here is some analysis of how it
	// could be done smarter since we only want the pixel-count, not the actual art:
	//
	// A 3x3 turns into a 4x4 which is really just 4 independent 2x2's for later iterations,
	// which each will turn into a 3x3 again. So, starting with a single 3x3, rather than N
	// iterations going back and forth between 2x2 and 3x3, we'll really have N/2 'iterations'
	// that turn each 3x3 into 4 new 3x3's (by applying the 2x2 rule immediately followed by
	// the corresponding 3x3 rule). Since there are only at most 16 2x2 different rules,
	// there are ever only at most 16 different 3x3's that can appear in the art. In other
	// words, we can just keep track of each of these 16 possible 3x3's and how often they
	// appear in the art, and let the new collapsed rules operate on the counts, for a massive
	// reduction of memory needs.
	public long doArt(int iterations) {
		processInput();
		String[] art = new String[3];
		art[0] = ".#.";
		art[1] = "..#";
		art[2] = "###";
		for (int i = 0; i < iterations; i++) {
			art = iteration(art);
		}
		long result = 0;
		for (String s : art) {
			for (int j = 0; j < art.length; j++) {
				result += s.charAt(j) == '#' ? 1 : 0;
			}
		}
		return result;
	}

	String[] iteration(String[] input) {
		int squareSize;
		if (input.length % 2 == 0) {
			squareSize = 2;
		} else if (input.length % 3 == 0) {
			squareSize = 3;
		} else {
			System.out.println("Not divisible by 2 or 3...");
			return input;
		}
		String[] result = new String[(input.length / squareSize) * (squareSize + 1)];
		char[][] square = new char[squareSize][squareSize];
		for (int y = 0; y < input.length / squareSize; y++) {
			StringBuilder[] newRows = new StringBuilder[squareSize + 1];
			for (int i = 0; i < squareSize + 1; i++) {
				newRows[i] = new StringBuilder();
			}
			for (int x=0; x < input.length; x += squareSize) {
				for (int i=0; i < squareSize; i++) {
					for (int j = 0; j < squareSize; j++) {
						square[i][j] = input[y*squareSize + j].charAt(x + i);
					}
				}
				String[] reprows = ruleBook.get(makeRep(square)).split("/");
				for (int i=0; i < squareSize + 1; i++) {
					newRows[i].append(reprows[i]);
				}
			}
			for (int i = 0; i < squareSize+1; i++) {
				result[y * (squareSize+1) + i] = newRows[i].toString();
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2017, 21));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doArt(5));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doArt(18));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
