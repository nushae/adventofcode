package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.Transition;
import com.nushae.aoc.aoc2017.domain.TuringMachine;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise25 {

	List<String> lines;
	TuringMachine alan;
	String startState;
	int steps;

	public Exercise25() {
		lines = new ArrayList<>();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise25(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		alan = new TuringMachine();
		// line 1 and 2 are general instructions
		startState = lines.get(0).substring("Begin in state ".length(), lines.get(0).length()-1);
		steps = Integer.parseInt(lines.get(1).substring("Perform a diagnostic checksum after ".length(), lines.get(1).length()-7));
		for (int i = 3; i < lines.size(); i += 10) {
			String line = lines.get(i);
			String oldState = line.substring("In state ".length(), line.length()-1);
			alan.addState(oldState);
			// line i+1 should be "If the current value is 0:"
			line = lines.get(i+2); // write
			int output = Integer.parseInt(line.substring("    - Write the value ".length(), line.length()-1));
			line = lines.get(i+3); // move
			boolean moveLeft = "left".equals(line.substring("    - Move one slot to the ".length(), line.length()-1));
			line = lines.get(i+4); // newstate
			String newState = line.substring("    - Continue with state ".length(), line.length()-1);
			alan.addTransition(new Transition(oldState, 0, newState, output, moveLeft));
			// line i+5 should be "If the current value is 1:"
			line = lines.get(i+6); // write
			output = Integer.parseInt(line.substring("    - Write the value ".length(), line.length()-1));
			line = lines.get(i+7); // move
			moveLeft = "left".equals(line.substring("    - Move one slot to the ".length(), line.length()-1));
			line = lines.get(i+8); // newstate
			newState = line.substring("    - Continue with state ".length(), line.length()-1);
			alan.addTransition(new Transition(oldState, 1, newState, output, moveLeft));
			// line i+9 is empty if present
		}
		System.out.println(alan);
		System.out.println("Turing Machine starts in state " + startState);
		System.out.println("and calculates checksum after " + steps + " steps.");
	}

	public long doPart1() {
		processInput();
		return alan.runWithDiagnostic(startState, steps);
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2017, 25));

		// part 1 (as usual, no part 2)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
