package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise11 {

	//              ___                 To represent hex coordinates treat them as a 3D point (x, y, z).
	//        +z   / 0 \    -y          The 3D axes are aligned with the (edges of) the hexes. The constraint is that
	//          \ /     \  /            z + y + z = 0 in every hex, so z can be derived from x and y as z = -x -y.
	//        ___/       \___           Here we have: x is the | column, y is the \ row, and z is the / row.
	//       /-1 \       / 1 \          Manhattan distance from (0, 0, 0) to  (x, y , z) in such a grid is:
	//      /     \1  -1/     \         (abs(x) + abs(y) + abs(z)) / 2 (for a real 3D grid you wouldn't divide by 2)
	//     /       \___/       \
	//     \       / 0 \       /
	//      \1   0/  x  \0  -1/
	//  -x - \___/   .   \___/  - +x
	//       /-1 \  z y  / 1 \
	//      /     \0   0/     \
	//     /       \___/       \
	//     \       / 0 \       /
	//      \0   1/     \-1  0/
	//       \___/       \___/
	//           \       /
	//         /  \-1  1/  \
	//       +y    \___/    -z
	public final static Map<String, Point> DIRS = Map.of(
		"n", new Point(0, -1), //  z = -x - y = 1
		"ne", new Point(1, -1), // z = -x - y = 0
		"se", new Point(1, 0), //  z = -x - y = -1
		"s", new Point(0, 1), //   z = -x - y = -1
		"sw", new Point(-1, 1), // z = -x - y = 0
		"nw", new Point(-1, 0) //  z = -x - y = 1
	);

	ArrayList<String> lines;

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		int x =0;
		int y = 0;
		int z = 0;
		for (String step : lines.get(0).split(",")) {
			Point dir = DIRS.get(step);
			x += dir.x;
			y += dir.y;
			z += -dir.x - dir.y;
		}
		return (Math.abs(x)+Math.abs(y)+Math.abs(z))/2L;
	}

	public long doPart2() {
		int x =0;
		int y = 0;
		int z = 0;
		long maxDist = Long.MIN_VALUE;
		for (String step : lines.get(0).split(",")) {
			Point dir = DIRS.get(step);
			x += dir.x;
			y += dir.y;
			z += -dir.x - dir.y;
			maxDist = Math.max(maxDist, (Math.abs(x)+Math.abs(y)+Math.abs(z))/2L);
		}
		return maxDist;
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2017, 11));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
