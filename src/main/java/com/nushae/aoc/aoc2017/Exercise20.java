package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.Particle;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise20 {

	private static final Pattern VECTORS = Pattern.compile("p=<(-?\\d+),(-?\\d+),(-?\\d+)>, v=<(-?\\d+),(-?\\d+),(-?\\d+)>, a=<(-?\\d+),(-?\\d+),(-?\\d+)>");

	List<String> lines;
	Map<Integer, Particle> particles;
	int pcount;

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		particles = new HashMap<>();
		pcount = 0;
		for (String line : lines) {
			Matcher m = VECTORS.matcher(line);
			if (m.find()) {
				Particle p = new Particle(
						Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)),
						Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)), Integer.parseInt(m.group(6)),
						Integer.parseInt(m.group(7)), Integer.parseInt(m.group(8)), Integer.parseInt(m.group(9))
				);
				particles.put(pcount++, p);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		processWithPattern();
		long result = 0;
		long minDist = Long.MAX_VALUE;
		for (int i=0; i < pcount; i++) {
			long d = particles.get(i).predictDistance(1000000L);
			if (d < minDist) {
				minDist = d;
				result = i;
			}
		}
		return result;
	}

	public long doPart2() {
		processWithPattern();
		// limit has been found by repeatedly running,
		// submitting answer, and *10-ing limit, until accepted
		for (int i = 0; i < 10000 && particles.size() > 1; i++) {
			for (Particle p : particles.values()) {
				p.update();
			}
			// remove collisions
			Set<Integer> remove = new HashSet<>();
			for (int p = 0; p < pcount - 1; p++) {
				if (particles.containsKey(p)) {
					long[] pos1 = particles.get(p).getPos();
					for (int q = p + 1; q < pcount; q++) {
						if (particles.containsKey(q)) {
							long[] pos2 = particles.get(q).getPos();
							boolean collision = true;
							for (int j = 0; collision && j < 3; j++) {
								collision = pos1[j] == pos2[j];
							}
							if (collision) {
								remove.add(p);
								remove.add(q);
							}
						}
					}
				}
			}
			for (int p : remove) {
				particles.remove(p);
			}
		}
		return particles.size();
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2017, 20));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
