package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.ParallelModule;
import com.nushae.aoc.aoc2017.domain.SoundModule;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise18 {

	SoundModule piano;
	ParallelModule[] programs;
	List<String> lines;

	public Exercise18() {
		lines = new ArrayList<>();
		piano = new SoundModule();
		programs = new ParallelModule[2];
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		piano.setProgram(lines);
		return piano.runProgram();
	}

	public long doPart2() {
		programs[0] = new ParallelModule(0);
		programs[1] = new ParallelModule(1);
		programs[0].setProgram(lines);
		programs[1].setProgram(lines);
		programs[0].setReceiverQueue(programs[1].getQueue());
		programs[1].setReceiverQueue(programs[0].getQueue());
		// simulate parallelism by having each program attempt to progress one instruction; any program waiting for
		// input will just not do anything until it has input.
		boolean p0blocked;
		boolean p1blocked;
		do {
			p0blocked = programs[0].nextInstruction();
			p1blocked = programs[1].nextInstruction();
		} while (!p0blocked || !p1blocked);
		// both stopped or deadlocked - report back with sent value
		return programs[1].getSent();
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2017, 18));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
