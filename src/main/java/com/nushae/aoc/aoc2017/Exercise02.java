package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise02 {

	ArrayList<String> lines;
	ArrayList<ArrayList<Long>> spreadsheet;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		spreadsheet = new ArrayList<>();
		for (String line : lines) {
			ArrayList<Long> row = new ArrayList<>();
			for (String num : line.split("\\s+")) {
				row.add(Long.parseLong(num.trim()));
			}
			spreadsheet.add(row);
		}
	}

	public long doPart1() {
		processInput();
		long result = 0;
		for (ArrayList<Long> row : spreadsheet) {
			long min = Long.MAX_VALUE;
			long max = Long.MIN_VALUE;
			for (long val : row) {
				min = Math.min(val, min);
				max = Math.max(val, max);
			}
			result += max - min;
		}
		return result;
	}

	public long doPart2() {
		processInput();
		long result = 0;
		for (ArrayList<Long> row : spreadsheet) {
			long a = 1;
			long b = 1;
outer:		for (int i = 0; i < row.size()-1; i++) {
				for (int j = i+1; j < row.size(); j++) {
					a = Math.max(row.get(i), row.get(j));
					b = Math.min(row.get(i), row.get(j));
					if (a % b == 0) {
						System.out.println(a + " / " + b);
						break outer;
					}
				}
			}
			result += a / b;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2017, 2));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
