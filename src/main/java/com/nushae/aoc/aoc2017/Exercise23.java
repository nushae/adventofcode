package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.Coprocessor;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.isPrime;

@Log4j2
public class Exercise23 {

	List<String> lines;
	Coprocessor redFive;

	public Exercise23() {
		lines = new ArrayList<>();
		redFive = new Coprocessor();
	}

	public Exercise23(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise23(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		redFive.setProgram(lines);
		return redFive.runProgram();
	}

	// set register a to 1, and.... Yeah let's analyse that. First off, g is never assigned to anything, so is a helper
	// register so and it can be directly replaced with the expressions assigned to it. If we also replace the jnz that
	// implement if/else we get:
	// set b 93           |  if (a == 0) {b = 93; c = 93;} else {b = 109300; c = 126300;}
	// set c b            |  NOP
	// jnz a 2            |  NOP
	// jnz 1 5            |  NOP
	// mul b 100          |  NOP
	// sub b -100000      |  NOP
	// set c b            |  NOP
	// sub c -17000       |  NOP
	// set f 1            |  f = true;
	// set d 2            |  d = 2;
	// set e 2            |  e = 2;
	// set g d            |  if (b == d * e) { f = false} in other words: f &= ( b != d * e)
	// mul g e            |  NOP
	// sub g b            |  NOP
	// jnz g 2            |  NOP
	// set f 0            |  NOP
	// sub e -1           |  e++
	// set g e            |  if (e != b) goto 12
	// sub g b            |  NOP
	// jnz g -8           |  NOP
	// sub d -1           |  d++
	// set g d            |  if (d != b) goto 11
	// sub g b            |  NOP
	// jnz g -13          |  NOP
	// jnz f 2            |  if (!f) {h++}
	// sub h -1           |  NOP
	// set g b            |  NOP
	// sub g c            |  NOP
	// jnz g 2            |  if (b != c) { b += 17; goto 9;}
	// jnz 1 3            |  HALT
	// sub b -17          |  NOP
	// jnz 1 -23          |  NOP
	// The remaining jnz represent loops. We get:
	// if (a == 0) {
	//   b = 93;
	//   c = 93; // equal, so we will 'loop' 1x
	// } else {
	//   b = 109300;
	//   c = 126300; // 17000 larger, so we will loop 1001x
	// }
	// for (; b <= c; b += 17) {
	//   f = true;
	//   for (d = 2; d < b; d++) {
	//     for (e = 2; e < b; e++) {
	//       f &= (b != d*e)
	//     }
	//   }
	//   if (!f) {h++}
	// }
	// In other words, the code determines the number of composite numbers of the form 109300 + N*17 for N = 0..1000
	public long doPart2() {
		long result = 0;
		for (int n = 0; n <= 1000; n++) {
			int testMe = 109300 + n*17;
			result += isPrime(testMe) ? 0 : 1;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(aocPath(2017, 23));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
