package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.DiscProg;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise07 {

	private static final Pattern DISC_PROG = Pattern.compile("([a-z]+) \\((\\d+)\\)(.*)");

	ArrayList<String> lines;
	Map<String, DiscProg> allProgs;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		allProgs = new HashMap<>();
		for (String line : lines) {
			Matcher m = DISC_PROG.matcher(line);
			if (m.find()) {
				allProgs.put(m.group(1), new DiscProg(m.group(1), Integer.parseInt(m.group(2))));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public DiscProg makeProgTree() {
		processWithPattern();
		// make random DiscProg the root for now:
		DiscProg rootNode = allProgs.get(lines.get(0).substring(0, lines.get(0).indexOf(' ')));
		for (String line : lines) {
			DiscProg curProg = allProgs.get(line.substring(0, line.indexOf(' ')));
			line = line.substring(line.indexOf(')')+1);
			if (line.length() > 0) { // sub-progs
				for (String progName : line.substring(4).split(", ")) {
					DiscProg childProg = allProgs.get(progName);
					curProg.children.add(childProg);
					childProg.parent = curProg;
				}
			}
		}
		while (rootNode.parent != null) {
			rootNode = rootNode.parent;
		}
		return rootNode;
	}

	public long findNewWeight(DiscProg root) {
		return root.reBalance();
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2017, 7));

		// part 1
		long start = System.currentTimeMillis();
		DiscProg root = ex.makeProgTree();
		System.out.println(root.name);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.findNewWeight(root));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
