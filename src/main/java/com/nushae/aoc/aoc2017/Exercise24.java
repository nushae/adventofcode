package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.Bridge;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise24 {

	List<String> lines;
	List<Bridge> bridges;
	int maxLength;
	int maxLengthStrength;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		bridges = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split("/");
			bridges.add(new Bridge(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])));
		}
	}

	public long doPart1() {
		processInput();
		Set<Bridge> available = new HashSet<>(bridges);
		return strongestBridge(0, available);
	}

	public long doPart2() {
		processInput();
		Set<Bridge> available = new HashSet<>(bridges);
		maxLength = 0;
		maxLengthStrength = 0;
		longestBridge(0, 0, 0, available);
		return maxLengthStrength;
	}

	public long strongestBridge(int nextPort, Set<Bridge> available) {
		long result = 0;
		Set<Bridge> nextAvailable = new HashSet<>(available);
		for (Bridge b : available) {
			if (b.hasFreePort(nextPort)) {
				nextAvailable.remove(b);
				b.usePort(nextPort);
				for (int port : b.getFreePorts()) { // loop of 1 :/
					result = Math.max(result, port + nextPort + strongestBridge(port, nextAvailable));
				}
				b.unusePort(nextPort);
				nextAvailable.add(b);
			}
		}
		return result;
	}

	public long longestBridge(int lengthSoFar, int strengthSoFar, int nextPort, Set<Bridge> available) {
		long result = 0;
		Set<Bridge> nextAvailable = new HashSet<>(available);
		for (Bridge b : available) {
			if (b.hasFreePort(nextPort)) {
				nextAvailable.remove(b);
				b.usePort(nextPort);
				for (int port : b.getFreePorts()) { // loop of 1 :/
					result = Math.max(result, port + nextPort + longestBridge(lengthSoFar + 1, nextPort + port + strengthSoFar, port, nextAvailable));
				}
				b.unusePort(nextPort);
				nextAvailable.add(b);
			}
		}
		if (result == 0) { // no next step, so look at length
			if (lengthSoFar > maxLength || lengthSoFar == maxLength && strengthSoFar > maxLengthStrength) {
				maxLength = lengthSoFar;
				maxLengthStrength = strengthSoFar;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2017, 24));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
