package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.DanceInstruction;
import com.nushae.aoc.aoc2017.domain.ExchangeInstruction;
import com.nushae.aoc.aoc2017.domain.PartnerInstruction;
import com.nushae.aoc.aoc2017.domain.SpinInstruction;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise16 {

	List<String> lines;
	List<DanceInstruction> steps;
	String lineUp;

	public Exercise16(String lineUp) {
		this.lineUp = lineUp;
		lines = new ArrayList<>();
	}

	public Exercise16(String lineUp, Path path) {
		this(lineUp);
		loadInputFromFile(lines, path);
	}

	public Exercise16(String lineUp, String data) {
		this(lineUp);
		loadInputFromData(lines, data);
	}

	private void processInput() {
		steps = new ArrayList<>();
		for (String line : lines) {
			for (String instruction : line.split(",")) {
				steps.add(switch(instruction.charAt(0)) {
					case 's' -> new SpinInstruction(instruction);
					case 'x' -> new ExchangeInstruction(instruction);
					case 'p' -> new PartnerInstruction(instruction);
					default -> throw new IllegalArgumentException("Unknown instruction: " + instruction);
				});
			}
		}
	}

	public String doPart1() {
		processInput();
		return doDance(lineUp);
	}

	public String doPart2() {
		processInput();
		Map<String, Long> olderSteps = new HashMap<>();
		String result = lineUp;
		olderSteps.put(result, 0L); // if we don't do this it takes one cycle more if the starting position is part of the cycle
		for (long i = 1; i <= 1000000000; i++) {
			result = doDance(result);
			if (olderSteps.containsKey(result)) {
				long cycleLength = i-olderSteps.get(result);
				long stepInCycle = (1000000000-i) % cycleLength;
				for (Map.Entry<String, Long> stored : olderSteps.entrySet()) {
					if (stored.getValue() == stepInCycle) {
						return stored.getKey();
					}
				}
			} else {
				olderSteps.put(result, i);
			}
		}
		return result;
	}

	public String doDance(String input) {
		String result = input;
		for (DanceInstruction step : steps) {
			result = step.apply(result);
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16("abcdefghijklmnop", aocPath(2017, 16));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
