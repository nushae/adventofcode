package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.loadInputFromData;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
public class Exercise10 {

	ArrayList<String> lines;
	ArrayList<Integer> numbers;
	StringBuilder circle;
	int circleSize;

	public Exercise10(int circleSize) {
		lines = new ArrayList<>();
		this.circleSize = circleSize % 257; // we can't handle larger sizes
	}

	public Exercise10(int circleSize, Path path) {
		this(circleSize);
		loadInputFromFile(lines, path);
	}

	public Exercise10(int circleSize, String data) {
		this(circleSize);
		loadInputFromData(lines, data);
	}

	private void processInput(boolean forPart1) {
		numbers = new ArrayList<>();
		circle = new StringBuilder();
		for (int c = 0; c < circleSize; c++) {
			circle.append((char) c);
		}
		if (forPart1) {
			for (String num : lines.get(0).split(",")) {
				numbers.add(Integer.parseInt(num));
			}
		} else {
			for (int i = 0; i < lines.get(0).length(); i++)  {
				numbers.add((int) lines.get(0).charAt(i));
			}
			// standard suffix:
			numbers.add(17);
			numbers.add(31);
			numbers.add(73);
			numbers.add(47);
			numbers.add(23);
		}
	}

	public long doPart1() {
		processInput(true);
		circle = runHash(1, circle);
		return (long) circle.charAt(0) * (long) circle.charAt(1);
	}

	public String doPart2() {
		processInput(false);
		circle = runHash(64, circle);
		StringBuilder denseHash = new StringBuilder();
		for (int i = 0; i < 16; i++) {
			int block = circle.charAt(16*i);
			for (int j = 1; j < 16; j++) {
				block = block ^ (int) circle.charAt(i*16+j);
			}
			String hexString = BigInteger.valueOf(block).toString(16);
			if (hexString.length() < 2) {
				denseHash.append("0");
			}
			denseHash.append(hexString);
		}
		return denseHash.toString();
	}

	/**
	 * Run one round of the circleKnot hashing function, and return the new circle. As a side effect, skipSize will
	 * increase.
	 *
	 * @param input starting circle
	 * @return resulting new circle
	 */
	public StringBuilder runHash(int rounds, StringBuilder input) {
		StringBuilder circle = new StringBuilder(input);
		int first = 0;
		int skipSize = 0;
		for (int r =0; r < rounds; r++) {
			for (int num : numbers) {
				StringBuilder rev = new StringBuilder(circle.substring(0, num));
				rev.reverse();
				rev.append(circle.substring(num));
				circle = rev;
				int skip = (num + skipSize) % circleSize;
				if (skip > 0) {
					circle.append(circle.substring(0, skip));
					circle.delete(0, skip);
				}
				first = (first + circleSize - skip) % circleSize;
				skipSize++;
			}
		}
		if (first > 0) {
			circle.append(circle.substring(0, first));
			circle.delete(0, first);
		}
		return circle;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(256, "46,41,212,83,1,255,157,65,139,52,39,254,2,86,0,204");

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
