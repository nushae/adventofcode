package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
public class Exercise06 {

	int[] banks = new int[16];
	Map<String, Long> states;

	public Exercise06(String data) {
		int i=0;
		states = new HashMap<>();
		StringBuilder key = new StringBuilder();
		for (String num : data.split("\\s+")) {
			if (i>0) {
				key.append("-");
			}
			int number = Integer.parseInt(num.trim());
			key.append(number);
			banks[i++] = number;
		}
		states.put(key.toString(), 0L);
	}

	public long findLoop() {
		long result = 0L;
		while(true) {
			int index = 0;
			int max = Integer.MIN_VALUE;
			for (int i =0; i < 16; i++) {
				if (banks[i] > max) {
					index = i;
					max = banks[i];
				}
			}
			banks[index] = 0;
			index = (index + 1) % 16;
			while (max > 0) {
				banks[index]++;
				max--;
				index = (index + 1) % 16;
			}
			result++;
			StringBuilder key = new StringBuilder();
			for (int i=0; i< 16; i++) {
				if (i > 0) {
					key.append("-");
				}
				key.append(banks[i]);
			}
			if (states.containsKey(key.toString())) {
				System.out.println("Loop length: " + (result - states.get(key.toString())));
				break;
			} else {
				states.put(key.toString(), result);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06("0\t5\t10\t0\t11\t14\t13\t4\t11\t8\t8\t7\t1\t4\t12\t11");

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.findLoop());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
