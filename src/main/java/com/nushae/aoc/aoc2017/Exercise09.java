package com.nushae.aoc.aoc2017;

import com.nushae.aoc.aoc2017.domain.DataStreamElement;
import com.nushae.aoc.aoc2017.domain.Garbage;
import com.nushae.aoc.aoc2017.domain.Group;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise09 {

	ArrayList<String> lines;
	StringBuilder input;
	Group root;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		input = new StringBuilder(lines.get(0));
		root = parseGroup();
	}

	Group parseGroup() {
		Group result;
		if (input.charAt(0) == '{') {
			eat(1);
			result = new Group();
			if (input.charAt(0) != '}') { // nonempty group
				result.addChild(parseDSE());
				while (input.charAt(0) == ',') {
					eat(1);
					result.addChild(parseDSE());
				} // should have an '}' now
			}
			if (input.charAt(0) == '}') {
				eat(1);
			} else {
				throw new IllegalArgumentException("Expected ',' or '}' but found " + input.toString());
			}
		} else {
			throw new IllegalArgumentException("Expected '{' but found " + input.toString());
		}
		return result;
	}

	DataStreamElement parseDSE() {
		if (input.charAt(0) == '{') {
			return parseGroup();
		} else {
			return parseGarbage();
		}
	}

	Garbage parseGarbage() {
		if (input.charAt(0) == '<') {
			Garbage garb = new Garbage();
			eat(1);
			StringBuilder contents = new StringBuilder();
			while(input.charAt(0) != '>') {
				contents.append(eat(1));
			}
			eat(1);
			garb.contents = contents.toString();
			return garb;
		} else {
			throw new IllegalArgumentException("Expected '<' but found " + input.toString());
		}
	}

	public String eat(int characters) {
		StringBuilder result = new StringBuilder();
		for (int i=0; i < characters; i++) {
			while (input.charAt(0) == '!') {
				input.delete(0, 2);
			}
			result.append(input.charAt(0));
			input.delete(0, 1);
		}
		if (input.length() > 0) {
			while (input.charAt(0) == '!') {
				input.delete(0, 2);
			}
		}
		return result.toString();
	}

	public long doPart1() {
		processInput();
		return root.totalScore(1);
	}

	public long doPart2() {
		processInput();
		return root.totalGarbageChars();
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2017, 9));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
