package com.nushae.aoc.aoc2017;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise01 {

	ArrayList<String> lines;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long result = 0;
		String captcha = lines.get(0);
		int len = captcha.length();
		for (int i = 0; i < len; i++) {
			result += captcha.charAt(i) == captcha.charAt((i + 1) % len) ? (captcha.charAt(i) - '0') : 0;
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		String captcha = lines.get(0);
		int len = captcha.length();
		for (int i = 0; i < len; i++) {
			result += captcha.charAt(i) == captcha.charAt((i + len / 2) % len) ? (captcha.charAt(i) - '0') : 0;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2017, 1));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
