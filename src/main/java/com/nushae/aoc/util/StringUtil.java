package com.nushae.aoc.util;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.join;

public class StringUtil {

	private StringUtil() {
		// util class, no construction!
		// This class contains various String related utilities and algorithms
	}

	// ==================== String-as-Set Utilities ====================

	/**
	 * Computes the intersection of two sets represented by Strings. Duplicates will be ignored
	 *
	 * @param a String representing a set of letters.
	 * @param b String representing a set of letters.
	 * @return a String representing the intersection of these sets
	 */
	public static String intersection(String a, String b) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < b.length(); i++) {
			String letter = b.substring(i, i + 1);
			if (a.contains(letter) && !result.toString().contains(letter)) {
				result.append(letter);
			}
		}
		return join(Arrays.stream(result.toString().split("")).distinct().sorted().toArray(), "");
	}

	/**
	 * Computes the union of two sets represented by Strings. Duplicates will be ignored
	 *
	 * @param a String representing a set of letters.
	 * @param b String representing a set of letters.
	 * @return a String representing the union of these sets
	 */
	public static String union(String a, String b) {
		StringBuilder result = new StringBuilder(a);
		for (int i = 0; i < b.length(); i++) {
			String letter = b.substring(i, i + 1);
			if (!result.toString().contains(letter)) {
				result.append(letter);
			}
		}
		return join(Arrays.stream(result.toString().split("")).distinct().sorted().toArray(), "");
	}

	// ==================== String utilities ====================

	/**
	 * Swaps the characters at positions a and b of the given String and returns the resulting string, provided
	 * <code>0 <= a < input.length && 0 <= b < input.length</code>, otherwise input is returned.
	 *
	 * @param input the string to modify
	 * @param a the position of the first character to swap
	 * @param b the position of the second character to swap
	 * @return the result of swapping the two chars
	 */
	public static String swapPos(String input, int a, int b) {
		int pos1 = Math.min(a, b);
		int pos2 = Math.max(a, b);
		if (pos1 != pos2 && pos1 >= 0 && pos2 < input.length()) {
			return (pos1 > 0 ? input.substring(0, pos1) : "") +
					input.charAt(pos2) +
					(pos1+1 < pos2 ? input.substring(pos1 + 1, pos2) : "") +
					input.charAt(pos1) +
					input.substring(pos2 + 1);
		} else {
			return input;
		}
	}

	// ==================== String[] utilities ====================

	/**
	 * Convert a String[] array with N lines of length M into one with M lines of length N by reflecting it in the x=y
	 * diagonal. eg. The array [ABCD, WXYZ] will become the array [AW, BX, CY, DZ].
	 * The method deals with incomplete data as follows:
	 * Any row in the input data that is completely empty (not even spaces) is skipped
	 * Any row in the input that is not empty but shorter than the longest row is treated as though the remainder has spaces.
	 *
	 * @param input an NxM String[] array with Q>=0 empty rows
	 * @return an (M-Q)xN String[] array with columns and rows transposed
	 */
	public static String[] transpose(String[] input) {
		int m = -1;
		for (String s : input) {
			if (s != null) {
				m = Math.max(m, s.length());
			}
		}
		String[] output = new String[m];
		for (int i=0; i < m; i++) {
			StringBuilder column = new StringBuilder();
			for (String s : input) {
				if (s != null && !s.isEmpty()) {
					if (s.length() > i) {
						column.append(s.charAt(i));
					} else {
						column.append(" ");
					}
				}
			}
			output[i] = column.toString();
		}
		return output;
	}
}
