package com.nushae.aoc.util;

import com.nushae.aoc.domain.BigFraction;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.math.BigInteger;
import java.util.List;
import java.util.*;
import java.util.stream.LongStream;

import static com.nushae.aoc.util.AlgoUtil.swapElements;
import static java.math.BigInteger.ZERO;

@Log4j2
public class MathUtil {

	// ########## number/word translations

	public static final Map<String, String> WORD_TO_DIGIT = Map.of(
		"zero", "0",
		"one", "1",
		"two", "2",
		"three", "3",
		"four", "4",
		"five", "5",
		"six", "6",
		"seven", "7",
		"eight", "8",
		"nine", "9"
	);

	public static final Map<String, String> DIGIT_TO_WORD = Map.of(
		"0", "zero",
		"1", "one",
		"2", "two",
		"3", "three",
		"4", "four",
		"5", "five",
		"6", "six",
		"7", "seven",
		"8", "eight",
		"9", "nine"
	);

	// ########## useful when doing grid searches (esp. in combination with sum() )

	public static final Point NORTH = new Point(0,-1);
	public static final Point NORTHEAST = new Point(1,-1);
	public static final Point EAST = new Point(1,0);
	public static final Point SOUTHEAST = new Point(1,1);
	public static final Point SOUTH = new Point(0,1);
	public static final Point SOUTHWEST = new Point(-1,1);
	public static final Point WEST = new Point(-1,0);
	public static final Point NORTHWEST = new Point(-1,-1);
	// Presets for iteration
	public static final List<Point> ORTHO_DIRS = Arrays.asList(NORTH, EAST, SOUTH, WEST);
	public static final List<Point> ORTHO_DIRS_LEXICOGRAPHIC = Arrays.asList(NORTH, WEST, EAST, SOUTH);
	public static final List<Point> DIAG_DIRS = Arrays.asList(NORTHEAST, SOUTHEAST, SOUTHWEST, NORTHWEST);
	public static final List<Point> DIAG_DIRS_LEXICOGRAPHIC = Arrays.asList(NORTHWEST, NORTHEAST, SOUTHWEST, SOUTHEAST);
	public static final List<Point> KING_DIRS = Arrays.asList(NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST);
	public static final List<Point> KING_DIRS_LEXICOGRAPHIC = Arrays.asList(NORTHWEST, NORTH, NORTHEAST, WEST, EAST, SOUTHWEST, SOUTH, SOUTHEAST);
	// Handy conversion maps
	public static final Map<Character, Point> CHAR_TO_DIR = Map.of('^', NORTH, '>', EAST, 'v', SOUTH, '<', WEST);
	public static final Map<Point, Character> DIR_TO_CHAR = Map.of(NORTH, '^', EAST, '>', SOUTH, 'v', WEST, '<');
	public static final Map<String, Point> WORD_TO_DIR = Map.of("north", NORTH, "northeast", NORTHEAST, "east", EAST, "southeast", SOUTHEAST, "south", SOUTH, "southwest", SOUTHWEST, "west", WEST, "northwest", NORTHWEST);
	public static final Map<Point, String> DIR_TO_WORD = Map.of(NORTH, "north", NORTHEAST, "northeast", EAST, "east", SOUTHEAST, "southeast", SOUTH, "south", SOUTHWEST, "southwest", WEST, "west", NORTHWEST, "northwest");

	// ########## a handy prime list; yields the first 1.5M or so primes

	public static final List<Integer> PRIMELIST = new ArrayList<>();
	static {
		boolean[] primes = new boolean[25000000];
		Arrays.fill(primes, true);
		for (int p = 2; p * p < 25000000; p++) {
			if (primes[p]) {
				for (int i = p * 2; i < 25000000; i += p) {
					primes[i] = false;
				}
			}
		}
		for (int i = 2; i < 25000000; i++) {
			if (primes[i]) {
				PRIMELIST.add(i);
			}
		}
	}

	// ########## MANHATTAN OH HOW I LOVE THEE

	/**
	 * <p>Calculate the manhattan distance from (0, 0) to (x, y).</p>
	 *
	 * @param x the x-coordinate
	 * @param y the y-coordinate
	 * @return the manhattan distance from (0, 0) to (x, y)
	 */
	public static int manhattanDistance(int x, int y) {
		return manhattanDistance(0, 0, x, y);
	}
	/**
	 * <p>Calculate the manhattan distance from (x0, y0) to (x1, y1).</p>
	 *
	 * @param x0 first x-coordinate
	 * @param y0 first y-coordinate
	 * @param x1 second x-coordinate
	 * @param y1 second y-coordinate
	 * @return the manhattan distance from (x0, y0) to (x1, y1)
	 */
	public static int manhattanDistance(int x0, int y0, int x1, int y1) {
		return Math.abs(x0 - x1) + Math.abs(y0 - y1);
	}
	/**
	 * <p>Calculate the manhattan distance from (0, 0) to (x, y).</p>
	 *
	 * @param x the x-coordinate
	 * @param y the y-coordinate
	 * @return the manhattan distance from (0, 0) to (x, y)
	 */
	public static long manhattanDistance(long x, long y) {
		return manhattanDistance(0L, 0L, x, y);
	}
	/**
	 * <p>Calculate the manhattan distance from (x0, y0) to (x1, y1).</p>
	 *
	 * @param x0 first x-coordinate
	 * @param y0 first y-coordinate
	 * @param x1 second x-coordinate
	 * @param y1 second y-coordinate
	 * @return the manhattan distance from (x0, y0) to (x1, y1)
	 */
	public static long manhattanDistance(long x0, long y0, long x1, long y1) {
		return Math.abs(x0 - x1) + Math.abs(y0 - y1);
	}

	/**
	 * <p>Calculate the manhattan distance from (0, 0) to p = (x, y).</p>
	 *
	 * @param p Point (x, y)
	 * @return the manhattan distance from (0, 0) to p
	 */
	public static int manhattanDistance(Point p) {
		return manhattanDistance(p.x, p.y);
	}

	/**
	 * <p>Calculate the manhattan distance from p0 = (x0, y0) to p1 = (x1, y1).</p>
	 *
	 * @param p0 Point (x0, y0)
	 * @param p1 Point (x1, y1)
	 * @return the manhattan distance between the points
	 */
	public static int manhattanDistance (Point p0, Point p1) {
		return manhattanDistance(p0.x, p0.y, p1.x, p1.y);
	}

	/**
	 * Return all Points that are manhattan distance <code>radius</code> from <code>center</code>.
	 *
	 * @param center the center of the ring
	 * @param radius the radius of the ring
	 * @return Set of all Points at manhattan distance radius from center
	 */
	public static Set<Point> manhattanRing(Point center, int radius) {
		if (radius == 0) {
			return Collections.singleton(center);
		}
		Set<Point> result = new HashSet<>();
		for (int i = 0; i < radius; i++) {
			result.add(new Point(center.x-radius+i, center.y-i)); // NW quarter, clockwise
			result.add(new Point(center.x+i, center.y-radius+i)); // NE quarter, clockwise
			result.add(new Point(center.x+radius-i, center.y+i)); // SE quarter, clockwise
			result.add(new Point(center.x-i, center.y+radius-i)); // SW quarter, clockwise
		}
		return result;
	}

	// ########## vector math using Point as vectors

	public static int crossProd(Point p, Point q) {
		return p.x * q.y - p.y * q.x;
	}
	public static int dotProd(Point p, Point q) {
		return p.x * q.x + p.y * q.y;
	}
	public static Point scale(Point p, int scalar) {
		return new Point(p.x * scalar, p.y * scalar);
	}
	public static Point diff(Point lhs, Point rhs) {
		return new Point(lhs.x - rhs.x, lhs.y - rhs.y);
	}
	public static Point sum(Point lhs, Point rhs) {
		return new Point(lhs.x + rhs.x, lhs.y + rhs.y);
	}
	public static Point linear(Point a, Point b, int times) {
		return sum(a, scale(b, times));
	}

	/**
	 * In a coordinate system with (0,0) at the top left, and positive x to the right, and positive y downward,
	 * rotate the input Point <code>p</code> by 90 degrees counterclockwise <code>value</code> times. (For a coordinate
	 * system with (0,0) at the bottom left and positive x to the right and positive y upward, the rotation would
	 * be <em>clockwise</em>)
	 *
	 * @param value number of times to rotate
	 * @param p input point
	 * @return p rotated counterclockwise value times
	 */
	public static Point rot90L(int value, Point p) {
		for (int i = 0; i<value; i++) {
			p = new Point(p.y, -p.x);
		}
		return p;
	}

	/**
	 * In a coordinate system with (0,0) at the top left, and positive x to the right, and positive y downward,
	 * rotate the input Point <code>p</code> by 90 degrees clockwise <code>value</code> times. (For a coordinate
	 * system with (0,0) at the bottom left and positive x to the right and positive y upward, the rotation would
	 * be <em>counterclockwise</em>)
	 *
	 * @param value number of times to rotate
	 * @param p input point
	 * @return p rotated clockwise value times
	 */
	public static Point rot90R(int value, Point p) {
		for (int i = 0; i<value; i++) {
			p = new Point(-p.y, p.x);
		}
		return p;
	}

	/**
	 * Reflect the input Point <code>p</code> in the line y = x.
	 *
	 * @param p the input Point p
	 * @return the reflection of p in y=x
	 */
	public static Point refYX(Point p) {
		return new Point(p.y, p.x);
	}

	/**
	 * Reflect the input Point <code>p</code> in the line y = -x.
	 *
	 * @param p the input Point p
	 * @return the reflection of p in y=x
	 */
	public static Point refYnegX(Point p) {
		return new Point(-p.y, -p.x);
	}

	// ########## useful functions to pass as references

	public static int identity(int n) {
		return n;
	}
	public static long identity(long n) {
		return n;
	}
	public static int square(int n) {
		return n*n;
	}
	public static long square(long n) {
		return n*n;
	}
	public static int triangle(int n) {
		return n*(n+1)/2;
	}
	public static long triangle(long n) {
		return n*(n+1)/2;
	}

	// ########## discrete math tools

	public static int gcd(int a, int b) {
		return b == 0 ? a : gcd(b, a % b);
	}
	// Non-recursive to avoid stack overflows.
	public static long gcd(long a, long b) {
		if (a == 0) return b;
		if (b == 0) return a;
		a = Math.abs(a);
		b = Math.abs(b);
		while (a != b) {
			if (a > b) {
				a = a - b;
			} else {
				b = b - a;
			}
		}
		return a;
	}
	public static int lcm(int a, int b) {
		return a == 0 && b == 0 ? 0 : Math.abs(a) * Math.abs(b) / gcd(a, b);
	}
	public static long lcm(long a, long b) {
		return a == 0 && b == 0 ? 0 : Math.abs(a) * Math.abs(b) / gcd(a, b);
	}
	public static BigInteger lcm(BigInteger a, BigInteger b) {
		return ZERO.equals(a) && ZERO.equals(b) ? ZERO : a.abs().multiply(b.abs()).divide(a.gcd(b));
	}

	/**
	 * Calculate n factorial for 0 <= n <= 20 (larger doesn't fit in long)
	 */
	public static long factorial(int N) {
		if (N > 20 || N < 0) throw new IllegalArgumentException(N + " is outside the 'long' range for a factorial");
		return LongStream.rangeClosed(2, N).reduce(1, (a, b) -> a * b);
	}

	/**
	 * Calculate the prime factorization of N and return it as a map of factor -> power pairs
	 *
	 * @param N the number to factorize
	 * @return a map of factor -> power pairs
	 */
	public static Map<Long, Long> factorization(long N) {
		Map<Long, Long> factors = new HashMap<>();
		int p = 0;
		while (N > 1) {
			if (PRIMELIST.contains(N)) { // prime!
				factors.put(N, factors.getOrDefault(N, 0L) + 1);
				N = 1;
			} else {
				long prime = PRIMELIST.get(p);
				if (N % prime == 0) {
					N = N / prime;
					factors.put(prime, factors.getOrDefault(prime, 0L) + 1);
				} else {
					p++;
				}
			}
		}
		return factors;
	}

	/**
	 * <p>Calculate the sum of all divisors of a number N.</p>
	 *
	 * @param N a number
	 * @return the sum of its divisors
	 */
	public static long sumOfDivisors(long N) {
		Map<Long, Long> factors = factorization(N);
		long result = 1;
		for (Map.Entry<Long, Long> entry : factors.entrySet()) {
			long powerSum = 0;
			for (int i = 0; i <= entry.getValue(); i++) {
				powerSum += (long) Math.pow(entry.getKey(), i);
			}
			result *= powerSum;
		}
		return result;
	}

	/**
	 * <p>Tests a number &le; 2500000 for primeness.</p>
	 *
	 * @param testMe number to be tested for primeness
	 * @return true iff testMe is prime
	 */
	public static boolean isPrime(int testMe) {
		if (testMe > 25000000) {
			throw new IllegalArgumentException("Number out of range of my prime list (must be <= 25000000: " + testMe);
		}
		return PRIMELIST.contains(testMe);
	}

	/**
	 * <p>Computes the Bezout Coefficients m and n for longs a and b so that m * a + n * b = gcd(a, b).<br/>
	 * For co-primes a and b, m is the modular multiplicative inverse of a mod b, and n that of b mod a, also handy.</p>
	 *
	 * @param a the first long
	 * @param b the second long
	 * @return long[] (m,n) where m*a + n*b == gcd(a, b);
	 */
	public static long[] bezout_coeff(long a, long b) {
		long[] result = new long[2];
		long old_r = a;
		long r = b;
		long old_s = 1;
		long s = 0;
		long old_t = 0;
		long t = 1;
		while (r != 0) {
			long quotient = old_r / r;
			long tmp = r;
			r = old_r - quotient * r;
			old_r = tmp;
			tmp = s;
			s = old_s - quotient * s;
			old_s = tmp;
			tmp = t;
			t = old_t - quotient * t;
			old_t = tmp;
		}
		result[0] = old_s;
		result[1] = old_t;
		return result;
	}

	/**
	 * <p>Calculate the solution X to the Chinese Remainder Theorem for inputs n<sub>0</sub> ... n<sub>k</sub> and their corresponding
	 * remainders a<sub>0</sub> ... a<sub>k</sub>. Recall:</p>
	 *
	 * <p>Given N = &Prod;(n<sub>i</sub>) for a series of integers n<sub>i</sub> that are pairwise co-prime, then if 0 &le; a<sub>i</sub> &lt; n<sub>i</sub> are
	 * integers, there is precisely one integer X so that 0 &le; X &lt; N and X <b>mod</b> n<sub>i</sub> == a<sub>i</sub> for every i.</p>
	 *
	 * <p>First consider a<sub>0</sub> and a<sub>1</sub>:<br/>
	 * x <b>mod</b> n<sub>0</sub> = a<sub>0</sub> &and; x <b>mod</b> n<sub>1</sub> = a<sub>1</sub>.<br/>
	 * Since n<sub>0</sub> and n<sub>1</sub> are co-prime, there are Bezout coefficients m<sub>0</sub and m<sub>1</sub> so that m<sub>0</sub> * n<sub>0</sub> + m<sub>1</sub> * n<sub>1</sub> = 1.<br/>
	 * Then a<sub>0</sub> * m<sub>1</sub> * n<sub>1</sub> + a<sub>1</sub> * m<sub>0</sub> * n<sub>0</sub> is a solution considering just these two numbers.<br/>
	 * So are all numbers that differ from this one by a multiple of m<sub>0</sub> * m<sub>1</sub>.<br/>
	 * Call the smallest positive such number a<sub>01</sub>.</p>
	 *
	 * <p>We have:<br/>
	 * x mod (n<sub>0</sub> * n<sub>1</sub>) = a<sub>01</sub>.<br/>
	 * So now we have reduced the list a<sub>0</sub>, a<sub>1</sub>, ..., a<sub>k</sub> to the list a<sub>01</sub>, a<sub>2</sub>, ..., a<sub>k</sub>,<br/>
	 * and the list n<sub>0</sub>, n<sub>1</sub>, ..., n<sub>k</sub> to the list n<sub>0</sub> * n<sub>1</sub>, n<sub>2</sub>, ..., n<sub>k</sub> with the same solution x.<br/>
	 * We recursively repeat this until there is only one a remaining.</p>
	 *
	 * <p>Since the product of n<sub>i</sub> will grow quite large for large amounts of n, and so may the bezout coefficients, only
	 * usable for small lists of (small) n; beware overflow.</p>
	 */
	public static long crt(List<Long> n, List<Long> a) {
		if (n.size() != a.size()) {
			throw new IllegalArgumentException("The number of n's and a's is not equal");
		}
		if (n.size() < 2) {
			throw new IllegalArgumentException("We do need at least 2 numbers to do this...");
		}
		// we assume that a_i < n_i for all i, and that all n_i are co-prime. If not: results are undefined.
		long n0 = n.get(0);
		long n1 = n.get(1);
		long[] b = bezout_coeff(n0, n1);
		long reducedA = (a.get(0) * b[1] * n1 + a.get(1) * b[0] * n0) % (n0*n1);
		while (reducedA < 0) {
			reducedA += n0*n1;
		}
		long newN = n0 * n1;
		if (n.size() > 2) { // recurse
			List<Long> newNList = new ArrayList<>(n.subList(2, n.size()));
			newNList.add(newN);
			List<Long> newAList = new ArrayList<>(a.subList(2, a.size()));
			newAList.add(reducedA);
			return crt(newNList, newAList);
		} else {
			return reducedA;
		}
	}

	/**
	 * <p>Calculate base<sup>exponent</sup> mod modulus</p>
	 *
	 * @param base the base
	 * @param exponent the exponent
	 * @param modulus the modulus
	 * @return Math.pow(base, exponent) % modulus
	 */
	public static long modPow(long base, long exponent, long modulus) {
		long S = base;
		long P = 1;
		while (exponent > 1) {
			if (exponent % 2 == 0) {
				exponent = exponent / 2;
				S = S * S % modulus;
			} else {
				exponent--;
				P = P * S % modulus;
			}
		}
		return P * S % modulus;
	}

	/**
	 * <p>Calculate base<sup>exponent</sup> mod modulus</p>
	 *
	 * @param base the base
	 * @param exponent the exponent
	 * @param modulus the modulus
	 * @return Math.pow(base, exponent) % modulus
	 */
	public static BigInteger modPow(BigInteger base, BigInteger exponent, BigInteger modulus) {
		BigInteger S = base;
		BigInteger P = BigInteger.ONE;
		while (exponent.compareTo(BigInteger.ONE) > 0) {
			if (exponent.mod(BigInteger.valueOf(2)).compareTo(ZERO) == 0) {
				exponent = exponent.divide(BigInteger.valueOf(2));
				S = S.multiply(S).mod(modulus);
			} else {
				exponent = exponent.subtract(BigInteger.ONE);
				P = P.multiply(S).mod(modulus);
			}
		}
		return P.multiply(S).mod(modulus);
	}


	/**
	 * <p>Gaussian reduction ("matrix sweep") algorithm, which solves for any system of N linear equations with N
	 * unknowns. Such equations have the form:</p>
	 * <pre>
	 * f11 * X1 + f21 * X2 + f31 * X3 + ... + fN1 * XN = C1
	 * ...
	 * f1M * X1 + f2M * X2 + f3M * X3 + ... + fNM * XN = CM
	 * </pre>
	 * <p>Where the Fnm and Cm are known constants, and the Xn are the unknowns.</p>
	 *
	 * <p>Such a system can be represented by a matrix with N rows and N+1 columns.
	 * Gaussian reduction reduces this to a diagonal matrix of the form:</p>
	 * <pre>
	 * g11 * X1                            = D1
	 *         g22 * X2                    = D2
	 *                    ...
	 *                            gNN * XN = DN
	 * </pre>
	 * <p>Since we have gn * xn = Dn, xn = Dn / gn. The xn are returned in a 1-dimensional array.</p>
	 *
	 * @param input BigFraction[][] array containing the linear equation coefficients and equation values
	 * @return BigRaction[] array containing the values of all the unknowns
	 */
	public static BigFraction[] gaussianReduce(BigFraction[][] input) {
		int rows = input.length;
		int columns = input[0].length;
		if (rows + 1 != columns) {
			throw new IllegalArgumentException("You must pass in a matrix with one more column than the number of rows");
		}
		List<List<BigFraction>> factors = new ArrayList<>();
		for (BigFraction[] row : input) {
			factors.add(new ArrayList<>(Arrays.asList(row).subList(0, columns)));
		}
		List<List<BigFraction>> result = new ArrayList<>();
		for (int pivot = 0; pivot < rows; pivot++) {
			log.debug("Current pivot #: " + pivot);
			for (int pivotRow = pivot; pivotRow < rows; pivotRow++) {
				if (factors.get(pivotRow).get(pivot).equals(BigFraction.ZERO)) {
					continue;
				}
				if (pivot != pivotRow) {
					factors = swapElements(factors, pivot, pivotRow);
					log.debug("Swapping rows " + pivotRow + " and " + pivot + " to put next pivot in topmost row.");
				}
				log.debug("Pivot value: " + factors.get(pivot).get(pivot));
				break;
			}
			for (int sweepRow = 0; sweepRow < rows; sweepRow++) {
				result.add(new ArrayList<>());
				BigFraction factor = factors.get(sweepRow).get(pivot).divide(factors.get(pivot).get(pivot));
				log.debug("Factor for row: " + sweepRow + " is: " + factors.get(sweepRow).get(pivot) + " / " + factors.get(pivot).get(pivot) + " = " + factor);
				for (int col = 0; col < factors.get(0).size(); col++) {
					if (sweepRow != pivot) {
						if (col == pivot) {
							log.debug("Value for row: " + sweepRow + ", col: " + col + " will be 0");
							result.get(sweepRow).add(BigFraction.ZERO);
						} else {
							BigFraction reduced = factors.get(sweepRow).get(col).subtract(factor.multiply(factors.get(pivot).get(col)));
							log.debug("Value for row: " + sweepRow + ", col: " + col + " will be " + factors.get(sweepRow).get(col) + " - " + factors.get(pivot).get(col) + " * " + factor + " = " + reduced);
							result.get(sweepRow).add(reduced);
						}
					} else {
						result.get(pivot).add(factors.get(pivot).get(col));
					}
				}
			}
			factors = result;
			result = new ArrayList<>();
		}
		BigFraction[] answers = new BigFraction[rows];
		for (int i = 0; i < rows; i++) {
			answers[i] = factors.get(i).get(columns-1).divide(factors.get(i).get(i));
		}
		return answers;
	}

	// ########## trig functions and utils

	/**
	 * <p>Convert an angle in radians to its equivalent in positive degrees; ie the resulting angle will be 0 &le; angle &lt; 360</p>
	 *
	 * @param radians input angle in radians
	 * @return equivalent angle in positive degrees
	 */
	public static double toPositiveDegrees(double radians) {
		double result = Math.toDegrees(radians);
		while (result < 0) {
			result += 360;
		}
		return result;
	}

	// ########## binary stuff

	/**
	 * <p>Converts a binary number to a reflected gray code.</p>
	 *
	 * @param num the binary number
	 * @return the corresponding gray code
	 */
	public static int toGrayCode(int num) {
		return num ^ (num >> 1);
	}

	/**
	 * <p>Converts a reflected gray code to a binary number.</p>
	 *
	 * @param gray the gray code
	 * @return the corresponding binary number
	 */
	public static int fromGrayCode(int gray) {
		int mask = gray;
		while (mask != 0) {
			mask >>= 1;
			gray ^= mask;
		}
		return gray;
	}
}
