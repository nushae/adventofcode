package com.nushae.aoc.util;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Log4j2
public class IOUtil {

	public static String AOC_BASE = Objects.requireNonNull(IOUtil.class.getResource("..")).getPath().substring(1);

	public static Path aocPath(int year, int ex) {
		return aocPath(year, ex, "input.txt");
	}

	public static Path aocPath(int year, int ex, String customFileName) {
		return Paths.get(AOC_BASE + String.format("aoc%d/ex%02d/%s", year, ex, customFileName));
	}

	public static Path lbPath(int year, String name) {
		return Paths.get(AOC_BASE + String.format("aoc%d/leaderboards/%s.json", year, name));
	}

	public static void loadInputFromFile(List<String> into, Path path) {
		try (Stream<String> stream = Files.lines(path)) {
			stream.forEach(into::add);
		} catch (IOException e) {
			log.warn("Problem loading file: " + path);
			if (e instanceof NoSuchFileException) {
				log.warn("File does not exist. Returning empty input");
			}
		}
	}

	public static void loadInputFromData(List<String> into, String data) {
		into.addAll(Arrays.asList(data.split("\n")));
	}
}
