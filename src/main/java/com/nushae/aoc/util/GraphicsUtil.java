package com.nushae.aoc.util;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GraphicsUtil {

	public static final Font DEFAULT_FONT = new Font("Dubai", Font.BOLD, 14);

	private GraphicsUtil() {
		// util class, no construction!
		// This class contains utilities for working with colors and often used graphics operations
	}

	/**
	 * Given two colors a and b, interpolate <code>extraColors</code> additional colors in between and return the
	 * resulting range of colors a List. The list wsill include a as the first and b as the last element.
	 *
	 * @param a first color
	 * @param b second color
	 * @param extraColors number of extra colors to interpolate
	 * @return List containing extraColors+2 colors
	 */
	public static java.util.List<Color> interpolate(Color a, Color b, int extraColors) {
		double deltaR = (b.getRed() - a.getRed()) / (extraColors + 1.0);
		double deltaG = (b.getGreen() - a.getGreen()) / (extraColors + 1.0);
		double deltaB = (b.getBlue() - a.getBlue()) / (extraColors + 1.0);
		List<Color> result = new ArrayList<>();
		result.add(a);
		for (int i = 1; i <= extraColors; i++) {
			result.add(new Color((int) (a.getRed() + deltaR * i), (int) (a.getGreen() + deltaG * i), (int) (a.getBlue() + deltaB * i)));
		}
		result.add(b);
		return result;
	}

	/**
	 * Draws a text onto the passed Graphics2D in a font, centered horizontally and vertically on the passed rectangle.
	 * The rectangle is only used for alignment, so its size does not need to match the size of the text
	 *
	 * @param g2 the Graphics2D to draw onto
	 * @param text The text to draw
	 * @param rect the rectangle to align to
	 * @param font the font to use for the text
	 */
	public static void drawCenteredString(Graphics2D g2, String text, Rectangle rect, Font font) {
		drawAlignedString(g2, text, rect, font, SwingConstants.CENTER);
	}

	/**
	 * Draws a text onto the passed Graphics2D in a font, with the given alignment relative to the passed rectangle.
	 * The rectangle is only used for alignment, so its size does not need to match the size of the text
	 *
	 * @param g2 the Graphics2D to draw onto
	 * @param text The text to draw
	 * @param rect the rectangle to align to
	 * @param font the font to use for the text
	 * @param alignment any SwingConstants alignment constant
	 */
	public static void drawAlignedString(Graphics2D g2, String text, Rectangle rect, Font font, int alignment) {
		FontMetrics metrics = g2.getFontMetrics(font);
		int x;
		if (alignment == SwingConstants.WEST || alignment == SwingConstants.NORTH_WEST || alignment == SwingConstants.SOUTH_WEST) {
			// Left aligned X for the text
			x = rect.x;
		} else if (alignment == SwingConstants.CENTER || alignment == SwingConstants.NORTH || alignment == SwingConstants.SOUTH) {
			// Centered X for the text
			x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
		} else if (alignment == SwingConstants.EAST || alignment == SwingConstants.NORTH_EAST || alignment == SwingConstants.SOUTH_EAST) {
			// Right aligned X for the text
			x = rect.x + rect.width - metrics.stringWidth(text);
		} else {
			throw new IllegalArgumentException("Use SwingConstants compass directions or CENTER");
		}
		int y;
		if (alignment == SwingConstants.NORTH_WEST || alignment == SwingConstants.NORTH || alignment == SwingConstants.NORTH_EAST) {
			// Top Y for the text (note we add the ascent, as in java 2d 0 is top of the screen)
			y = rect.y + metrics.getAscent();
		} else if (alignment == SwingConstants.WEST || alignment == SwingConstants.CENTER || alignment == SwingConstants.EAST) {
			// Centered Y for the text
			y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
		} else {
			// Bottom Y for the text
			y = rect.y + (rect.height - metrics.getHeight()) + metrics.getAscent();
		}
		g2.setFont(font);
		g2.drawString(text, x, y);
	}
}
