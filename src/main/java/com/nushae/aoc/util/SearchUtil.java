package com.nushae.aoc.util;

import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.domain.Pair;
import com.nushae.aoc.domain.SearchNode;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

@Log4j2
public class SearchUtil {

	// ================= DEPTH FIRST SEARCH =================

	/**
	 * Depth First Search algorithm (aka echaustive search, aka flood fill). Returns the first node satisfying goal
	 * but with the path from start to goal traced by its parent node(s), or null if no match found.
	 * Works with the {@link SearchNode} interface.
	 *
	 * @param start SearchNode to start from
	 * @param goal Predicate defining SearchNode to find
	 * @return SearchNode satisfying goal or null if not found
	 */
	public static SearchNode dfs(SearchNode start, Predicate<SearchNode> goal) {
		Deque<SearchNode> Q = new ArrayDeque<>();
		Set<SearchNode> visited = new HashSet<>();
		Q.add(start);
		visited.add(start);
		while (!Q.isEmpty()) {
			SearchNode current = Q.poll();
			if (goal.test(current)) {
				return current;
			}
			for (SearchNode neighbour : current.getAdjacentNodes().keySet()) {
				if (!visited.contains(neighbour)) {
					neighbour.setParent(current); // for path tracing
					visited.add(neighbour);
					Q.add(neighbour);
				}
			}
		}
		return null;
	}

	/**
	 * Depth First Search algorithm (aka exhaustive search, aka flood fill). Returns the first {@link Location}
	 * reachable from start that satisfies goal, or null if none exist in the grid.
	 * Works with the {@link CharGrid} class.
	 *
	 * @param start Location to start the search from
	 * @param goal Predicate&lt;Location&gt; that needs to be satisfied by the goal Location
	 * @return the first Location found satisfying goal, or null
	 */
	public static Location dfs(Location start, Predicate<Location> goal) {
		Deque<Location> Q = new ArrayDeque<>();
		Set<Location> visited = new HashSet<>();
		Q.add(start);
		visited.add(start);
		while (!Q.isEmpty()) {
			Location current = Q.poll();
			if (goal.test(current)) {
				return current;
			}
			for (Location neighbour : current.getNeighbours()) {
				if (!visited.contains(neighbour)) {
					visited.add(neighbour);
					Q.add(neighbour);
				}
			}
		}
		return null;
	}

	/**
	 * Depth First Search algorithm (aka exhaustive search, aka flood fill). Variation that returns all {@link Location}s reachable from
	 * start that satisfy goal, as well as, for each visited {@link Location}, a count of unique paths to it.
	 * Works with the {@link CharGrid} class.
	 *
	 * @param start Location to start the search from
	 * @param goal Predicate&lt;Location&gt; that needs to be satisfied by the goal Location
	 * @return Pair&lt;Set&lt;Location&gt;, Map&lt;Location, Long&gt;&gt; containing all goal Locations found and a path count for each visited Location
	 */
	public static Pair<Set<Location>, Map<Location, Long>> dfsAllGoalsWithPathCounts(Location start, Predicate<Location> goal) {
		Deque<Location> Q = new ArrayDeque<>();
		Map<Location, Long> visited = new HashMap<>(); // also record number of distinct paths (so far) to the visited location
		Set<Location> reachableGoals = new HashSet<>();
		Q.add(start);
		visited.put(start, 1L);
		while (!Q.isEmpty()) {
			Location current = Q.poll();
			if (goal.test(current)) {
				reachableGoals.add(current);
				continue;
			}
			for (Location neighbour : current.getNeighbours()) {
				if (!visited.containsKey(neighbour)) {
					visited.put(neighbour, visited.get(current));
					Q.add(neighbour);
				} else {
					visited.put(neighbour, visited.get(current) + visited.get(neighbour));
				}
			}
		}
		return new Pair<>(reachableGoals, visited);
	}

	// ================= DIJKSTRA'S SHORTEST PATH =================

	/**
	 * Dijkstra's Shortest Path algorithm. Returns the first node satisfying goal but with the path from start to goal
	 * traced by its parent node(s), or null if no match found. Works with the {@link SearchNode} interface.
	 *
	 * @param start SearchNode to start from
	 * @param goal SearchNode to find
	 * @return goal or null
	 */
	public static SearchNode dsp(SearchNode start, Predicate<SearchNode> goal) {
		Set<SearchNode> visited = new HashSet<>();
		Map<SearchNode, Long> bestDistances = new HashMap<>();
		Set<SearchNode> queue = new HashSet<>();
		bestDistances.put(start, 0L);
		queue.add(start);
		while (!queue.isEmpty()) {
			SearchNode minNode = queue
					.stream()
					.min((a, b) -> (int) Math.signum(bestDistances.getOrDefault(a, Long.MAX_VALUE) - bestDistances.getOrDefault(b, Long.MAX_VALUE)))
					.get();
			long minDist = bestDistances.get(minNode);
			log.debug(minNode + " has BestDistance " + minDist + " so far");
			if (goal.test(minNode)) {
				log.debug(minNode + " has BestDistance " + bestDistances.get(minNode) + " for target");
				return minNode;
			}
			visited.add(minNode);
			queue.remove(minNode);
			for (Map.Entry<SearchNode, Long> ne : minNode.getAdjacentNodes().entrySet()) {
				SearchNode neighbour = ne.getKey();
				if (!visited.contains(neighbour)) {
					long val = ne.getValue();
					queue.add(neighbour);
					long tentative = minDist + val;
					if (tentative < bestDistances.getOrDefault(neighbour, Long.MAX_VALUE)) {
						bestDistances.put(neighbour, tentative);
						neighbour.setParent(minNode); // for path tracing
					}
				}
			}
		}
		return null;
	}

	/**
	 * Dijkstra's Shortest Path algorithm. Returns the first {@link Location} satisfying goal including the length of the path from
	 * start to goal, or special value ((-1, -1, null), -1) if no match found.
	 * Works with the {@link CharGrid} class.
	 *
	 * @param start Location to start from
	 * @param walls Set of characters that represent walls (ie impassable locations)
	 * @param goal Predicate&lt;Location&gt; to satisfy / find
	 * @return a Pair&lt;Location, Long&gt; containing the goal location (or -1, -1 if not found) as well as the path length to it (or -1 if not found)
	 */
	public static Pair<Location, Long> dsp(Location start, Set<Character> walls, Predicate<Location> goal) {
		Set<Location> visited = new HashSet<>();
		Map<Location, Long> bestDistances = new HashMap<>();
		Set<Location> queue = new HashSet<>();
		bestDistances.put(start, 0L);
		queue.add(start);
		while (!queue.isEmpty()) {
			Location minNode = queue
					.stream()
					.min((a, b) -> (int) Math.signum(bestDistances.getOrDefault(a, Long.MAX_VALUE) - bestDistances.getOrDefault(b, Long.MAX_VALUE)))
					.get();
			long minDist = bestDistances.get(minNode);
			log.debug(minNode + " has BestDistance " + minDist + " so far");
			if (goal.test(minNode)) {
				log.debug(minNode + " has BestDistance " + bestDistances.get(minNode) + " for target");
				return new Pair<>(minNode, minDist);
			}
			visited.add(minNode);
			queue.remove(minNode);
			for (Location neighbour : minNode.getNeighbours()) {
				if (!visited.contains(neighbour) && !walls.contains(neighbour.resolve())) {
					long val = 1; // in a standard CharGrid all distances are 1
					queue.add(neighbour);
					long tentative = minDist + val;
					if (tentative < bestDistances.getOrDefault(neighbour, Long.MAX_VALUE)) {
						bestDistances.put(neighbour, tentative);
					}
				}
			}
		}
		return new Pair<>(new Location(-1, -1, null), -1L);
	}

	public static List<Location> dspWithPath(Location start, Set<Character> walls, Predicate<Location> goal) {
		Map<Location, Location> previous = new HashMap<>();
		previous.put(start, null);
		Set<Location> visited = new HashSet<>();
		Map<Location, Long> bestDistances = new HashMap<>();
		Set<Location> queue = new HashSet<>();
		bestDistances.put(start, 0L);
		queue.add(start);
		while (!queue.isEmpty()) {
			Location minNode = queue
					.stream()
					.min((a, b) -> (int) Math.signum(bestDistances.getOrDefault(a, Long.MAX_VALUE) - bestDistances.getOrDefault(b, Long.MAX_VALUE)))
					.get();
			long minDist = bestDistances.get(minNode);
			log.debug(minNode + " has BestDistance " + minDist + " so far");
			if (goal.test(minNode)) {
				log.debug(minNode + " has BestDistance " + bestDistances.get(minNode) + " for target");
				List<Location> result = new ArrayList<>();

				while (minNode != null) {
					result.add(0, minNode);
					minNode = previous.get(minNode);
				}
				return result;
			}
			visited.add(minNode);
			queue.remove(minNode);
			for (Location neighbour : minNode.getNeighbours()) {
				if (!visited.contains(neighbour) && !walls.contains(neighbour.resolve())) {
					long val = 1; // in a standard CharGrid all distances are 1
					queue.add(neighbour);
					long tentative = minDist + val;
					if (tentative < bestDistances.getOrDefault(neighbour, Long.MAX_VALUE)) {
						bestDistances.put(neighbour, tentative);
						previous.put(neighbour, minNode);
					}
				}
			}
		}
		return Collections.emptyList();
	}

	// ================= BINARY SEARCH =================

	/**
	 * Open Ended Binary Search algorithm. Starting from lowerBound with ascendingFunction(lowerBound) <= soughtValue,
	 * but with no upperBound given, find the lowest x so that ascendingFunction(x) == soughtValue.
	 * (An upper bound is chosen by repeatedly doubling it until ascendingFunction(upperBound) > soughtValue or Long.MAX_VALUE is reached
	 *
	 * @param lowerBound the starting value to search from
	 * @param soughtValue the sought value
	 * @param ascendingFunction the ascending function
	 * @return x so that ascendingFunction(x) = soughtValue, or -1 if not found
	 */
	public static long openEndedBinarySearch(long lowerBound, long soughtValue, Function<Long, Long> ascendingFunction) {
		// find large enough upper bound:
		long upperBound = lowerBound + 1;
		while (ascendingFunction.apply(upperBound) <= soughtValue && upperBound <= Long.MAX_VALUE/2) {
			upperBound = upperBound * 2;
		}
		return binarySearch(lowerBound, upperBound, soughtValue, ascendingFunction);
	}

	/**
	 * Binary Search algorithm. Starting from lowerBound with ascendingFunction(lowerBound) <= soughtValue,
	 * and upperBound with ascendingFunction(upperBound) > soughtValue, find the lowest x so that ascendingFunction(x) == soughtValue.
	 *
	 * @param lowerBound the lowest value to test
	 * @param lowerBound the highest value to test
	 * @param soughtValue the sought value
	 * @param ascendingFunction the ascending function
	 * @return x so that ascendingFunction(x) == soughtValue, or -1 if not found
	 */
	public static long binarySearch(long lowerBound, long upperBound, long soughtValue, Function<Long, Long> ascendingFunction) {
		long result = -1;
		while (lowerBound <= upperBound) {
			long tryValue = lowerBound + (upperBound - lowerBound) / 2;
			result = ascendingFunction.apply(tryValue);
			if (result > soughtValue) {
				upperBound = tryValue - 1;
			} else if (result < soughtValue){
				lowerBound = tryValue + 1;
			} else {
				return tryValue;
			}
		}
		return -1; // not found
	}

	/**
	 * Generalized Binary Search algorithm. Finds the highest value between lowerBound and upperBound for which resultTooHigh is
	 * false, assuming that resultTooHigh is an 'ascending predicate' (ie it never returns true for values lower than
	 * any value that returns false).
	 *
	 * For the classic use case use the predicate <code>value -> ascendingFunction(value) > soughtValue</code>.
	 *
	 * @param lowerBound lowest value to test
	 * @param upperBound highest value to test
	 * @param resultTooHigh ascending predicate
	 * @return highest value for which the predicate is still false
	 */
	public static long binarySearch(long lowerBound, long upperBound, Predicate<Long> resultTooHigh) {
		while (lowerBound + 1 < upperBound) {
			long tryValue = lowerBound + (upperBound - lowerBound) / 2;
			if (resultTooHigh.test(tryValue)) {
				upperBound = tryValue;
			} else {
				lowerBound = tryValue;
			}
		}
		return lowerBound;
	}
}
