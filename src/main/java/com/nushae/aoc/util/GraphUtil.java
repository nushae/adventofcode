package com.nushae.aoc.util;

import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

/**
 * Utility functions to work with graphs and search them
 */
@Log4j2
public class GraphUtil {
	/**
	 * Given a startNode in a connected graph, find the shortest distance to the node that has payload targetPayload.
	 * If multiple nodes happen to have the same payload, the distance to the first encountered is returned
	 *
	 * @param startNode the startnode
	 * @param targetPayload the payLoad to reach
	 * @param <T> the type of the payload
	 * @return long the total weight of the lowest-weight path from startNode to the node with targetPayload
	 */
	public static <T> long findShortestDistanceInGraph(GraphNode<T> startNode, T targetPayload) {
		long result = 0;
		java.util.List<? extends GraphNode<T>> path = findShortestPath(startNode, n -> n.getPayload().equals(targetPayload));
		for (int i = 0; i < path.size() - 1; i++) {
			GraphNode<T> current = path.get(i);
			GraphNode<T> next = path.get(i+1);
			result += (current.getAdjacentNodes().getOrDefault(next, 0L));
		}
		return result;
	}

	/**
	 * Given a startNode in a connected graph, find the least-weight path to the first node encountered that satisfies
	 * goalPredicate.
	 *
	 * @param startNode the startnode
	 * @param goalPredicate a predicate over the payload, identifying the goal(s)
	 * @param <T> the type of the payload
	 * @return List<GraphNode<T>> the path, or an empty list if no such path exists
	 */
	public static <T> java.util.List<? extends GraphNode<T>> findShortestPath(GraphNode<T> startNode, Predicate<GraphNode<T>> goalPredicate) {
		java.util.List<GraphNode<T>> result = new ArrayList<>();
		HashSet<GraphNode<T>> visited = new HashSet<>();
		Map<GraphNode<T>, Long> bestDistances = new HashMap<>();
		Map<GraphNode<T>, GraphNode<T>> prev = new HashMap<>();
		Set<GraphNode<T>> queue = new HashSet<>();
		bestDistances.put(startNode, 0L);
		queue.add(startNode);
		while (!queue.isEmpty()) {
			GraphNode<T> minNode = queue
					.stream()
					.min((a, b) -> (int) Math.signum(bestDistances.getOrDefault(a, Long.MAX_VALUE) - bestDistances.getOrDefault(b, Long.MAX_VALUE)))
					.get();
			long minDist = bestDistances.get(minNode);
			visited.add(minNode);
			queue.remove(minNode);
			if (goalPredicate.test(minNode)) {
				result.add(minNode);
				while (prev.containsKey(minNode)) {
					minNode = prev.get(minNode);
					result.add(0, minNode);
				}
				return result;
			}
			for (Map.Entry<? extends GraphNode<T>, Long> ne : minNode.getAdjacentNodes().entrySet()) {
				GraphNode<T> neighbour = ne.getKey();
				if (!visited.contains(neighbour)) {
					long val = ne.getValue();
					queue.add(neighbour);
					long tentative = minDist + val;
					if (tentative < bestDistances.getOrDefault(neighbour, Long.MAX_VALUE)) {
						bestDistances.put(neighbour, tentative);
						prev.put(neighbour, minNode);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Given a rectangular grid[width][height] with grid[x][y] = "weight for entering node from any orthogonal neighbour",
	 * convert this grid into a weighted directed graph with unnamed nodes. Return the GraphNode corresponding to (0, 0).
	 *
	 * @param grid representing an orthogonally connected weighted graph
	 * @return topleft graphnode of corresponding directed weighted graph
	 */
	public static BasicGraphNode<Point> weightGridToGraph(int[][] grid) {
		int width = grid.length;
		int height = grid[0].length;
		Map<Point, BasicGraphNode<Point>> nodes = new HashMap<>();
		for (int x = 0; x < width; x++) {
			for (int y=0; y < height; y++) {
				Point coords = new Point(x, y);
				BasicGraphNode<Point> node = new BasicGraphNode<>("", coords, coords);
				nodes.put(coords, node);
				for (Point dir : ORTHO_DIRS) {
					Point neighbour = new Point(coords.x + dir.x, coords.y + dir.y);
					if (nodes.containsKey(neighbour)) {
						node.addNeighbour(nodes.get(neighbour), grid[neighbour.x][neighbour.y]);
						nodes.get(neighbour).addNeighbour(node, grid[coords.x][coords.y]);
					}
				}
			}
		}
		return nodes.get(new Point(0,0));
	}

	/**
	 * Given a rectangular grid[width][height] with grid[x][y] = "payload for this grid cell", and orthogonally
	 * adjacent grid cells considered connected, convert this grid into an undirected unweighted graph with
	 * unnamed nodes.
	 * Nodes with a payload that is in verboten are considered impassable and will not be added.
	 * Return the GraphNode that has payLoad rootPayload, or the one corresponding to (0, 0) if
	 * rootPayLoad is null or no GraphNode has that payload.
	 *
	 * NOT USABLE FOR DISCONNECTED GRAPHS - assumes there is a single starting payload!
	 *
	 * @param grid representing an orthogonally connected graph with payloads
	 * @param rootPayLoad the payload of the root node, or null if the rootNode should be (0, 0)
	 * @param verboten array of payloads that are considered impassable (and thus not part of the graph)
	 * @return the rootNode as defined by rootPayload
	 */
	public static <T> BasicGraphNode<T> gridToGraph(T[][] grid, T rootPayLoad, Set<T> verboten) {
		int width = grid.length;
		int height = grid[0].length;
		int rootX = 0;
		int rootY = 0;
		Map<Point, BasicGraphNode<T>> nodes = new HashMap<>();
		for (int x = 0; x < width; x++) {
			for (int y=0; y < height; y++) {
				if (null != rootPayLoad && grid[x][y] == rootPayLoad) {
					rootX = x;
					rootY = y;
				}
				if (!verboten.contains(grid[x][y])) {
					Point coords = new Point(x, y);
					BasicGraphNode<T> node = new BasicGraphNode<>("", coords, grid[x][y]);
					nodes.put(coords, node);
					for (Point dir : ORTHO_DIRS) {
						Point neighbour = new Point(coords.x + dir.x, coords.y + dir.y);
						if (nodes.containsKey(neighbour)) {
							node.addNeighbour(nodes.get(neighbour), 1);
							nodes.get(neighbour).addNeighbour(node, 1);
						}
					}
				}
			}
		}
		return nodes.get(new Point(rootX, rootY));
	}

	/**
	 * Given a rectangular grid[width][height] and a starting location Point (x,y), and orthogonally
	 * adjacent grid cells considered connected, convert the reachable part of this grid into an undirected unweighted
	 * graph with unnamed nodes.
	 * Nodes with a payload that is in verboten are considered impassable and will not be added.
	 * Return the GraphNode that has location x,y.
	 *
	 * Usable for disconnected grids, call once with a starting location in each subgrid
	 *
	 * @param grid representing an orthogonally connected graph with payloads
	 * @param verboten array of payloads that are considered impassable (and thus not part of the graph)
	 * @param rootLocation the x,y coordinate of the node to use as root node
	 * @return the rootNode as defined by rootLocation
	 */
	public static <T> BasicGraphNode<T> gridToGraph(T[][] grid, Set<T> verboten, Point rootLocation) {
		int width = grid.length;
		int height = grid[0].length;
		int rootX = 0;
		int rootY = 0;
		Map<Point, BasicGraphNode<T>> nodes = new HashMap<>();
		for (int x = 0; x < width; x++) {
			for (int y=0; y < height; y++) {
				if (rootLocation.x == x && rootLocation.y == y) {
					rootX = x;
					rootY = y;
				}
				if (!verboten.contains(grid[x][y])) {
					Point coords = new Point(x, y);
					BasicGraphNode<T> node = new BasicGraphNode<>("", coords, grid[x][y]);
					nodes.put(coords, node);
					for (Point dir : ORTHO_DIRS) {
						Point neighbour = new Point(coords.x + dir.x, coords.y + dir.y);
						if (nodes.containsKey(neighbour)) {
							node.addNeighbour(nodes.get(neighbour), 1);
							nodes.get(neighbour).addNeighbour(node, 1);
						}
					}
				}
			}
		}
		return nodes.get(new Point(rootX, rootY));
	}

	/**
	 * Finds the node with the given (x, y) location within graph. Returns null if no such node exists.
	 * Beware - graphs not derived from grids will likely not have locations set!
	 *
	 * @param graph starting node for the graph to traverse
	 * @param location location of the node to find
	 * @param <T> the payload type of the graph
	 * @return the node with location if it exists, or null
	 */
	public static <T> BasicGraphNode<T> findGraphNodeWithLocation(BasicGraphNode<T> graph, Point location) {
		Set<GraphNode<T>> visited = new HashSet<>();
		List<GraphNode<T>> queue = new ArrayList<>();
		visited.add(graph);
		queue.add(graph);
		while (!queue.isEmpty()) {
			BasicGraphNode<T> current = (BasicGraphNode<T>) queue.remove(0);
			if (!location.equals(current.getLocation())) {
				for (GraphNode<T> adjacent : current.getAdjacentNodes().keySet()) {
					if (!visited.contains(adjacent)) {
						visited.add(adjacent);
						queue.add(adjacent);
					}
				}
			} else {
				return current;
			}
		}
		return null;
	}

	// =========================== SIMPLIFIED GRAPHS =========================
	//
 	// In practice, the graphs in AOC almost always have a payload of a built-in type (or String), with all neighbours
	// at a distance 1, so the GraphNode interface is a bit of an overkill.
 	// Every method below this line makes use of a simplified graph structure over T that is represented by
	// the neighbour-function Map <T, Set<T>>

	/**
	 * Bron-Kerbosch algorithm for finding all cliques (fully connected subgraphs) in a graph.
	 * @param neighbours Map<T, Set<T>> map of all neighbours for each node in the graph
	 * @return Set<Set<T>> set of all cliques in the graph
	 * @param <T>
	 */
	public static <T> Set<Set<T>> findCliques(Map<T, Set<T>> neighbours) {
		return bronKerbosch(neighbours, new HashSet<>(), new HashSet<>(neighbours.keySet()), new HashSet<>());
	}
	private static <T> Set<Set<T>> bronKerbosch(Map<T, Set<T>> neighbours, Set<T> allInClique, Set<T> someInClique, Set<T> noneInClique) {
		Set<Set<T>> result = new HashSet<>();
		if (someInClique.isEmpty() && noneInClique.isEmpty()) {
			result.add(allInClique);
			return result;
		}
		Iterator<T> its = someInClique.iterator();
		while (its.hasNext()) {
			T vertex = its.next();
			Set<T> newR = new HashSet<>(allInClique);
			newR.add(vertex);
			Set<T> newP = new HashSet<>();
			Set<T> newX = new HashSet<>();
			for (T n : neighbours.get(vertex)) {
				if (someInClique.contains(n)) {
					newP.add(n);
				}
				if (noneInClique.contains(n)) {
					newX.add(n);
				}
			}
			result.addAll(bronKerbosch(neighbours, newR, newP, newX));
			its.remove();
			noneInClique.add(vertex);
		}
		return result;
	}

	/**
	 * Draws a visualization of the graph represented by <code>neighbours</code> on g2, by placing the nodes equidistant around a circle.
	 * Edges and nodes all get a default coloring.
	 *
	 * @param g2 the Graphics2D to draw on
	 * @param width the width of the canvas
	 * @param height the height of the canvas
	 * @param neighbours a Map of neighbours for each node
	 * @param <T> the node type of the graph
	 */
	public static <T> void circularVisualize(Graphics2D g2, int width, int height, Map<T, Set<T>> neighbours) {
		circularVisualizeWithCliquesAndColors(g2, width, height, neighbours, Collections.emptyMap(), Collections.emptyMap());
	}

	/**
	 * Draws a visualization of the graph represented by <code>neighbours</code> on g2, by placing the nodes equidistant around a circle.
	 * Edges and nodes all get a default coloring.
	 * Edge and node coloring is done on the basis of clique size in the graph - the <code>largestClique</code> map should contain a value for
	 * each edge (as a Pair&lt;T, T&gt; (e1, e2)) as well as each node (as Pair&lt;T, T&gt; (n, n)). Any edge or node not found in the map will
	 * get a default color so you can pass an empty map to get an uncolored graph.
	 *
	 * @param g2 the Graphics2D to draw on
	 * @param width the width of the canvas
	 * @param height the height of the canvas
	 * @param neighbours a Map of neighbours for each node
	 * @param largestClique a Map of largest clique sizes for each node and edge
	 * @param cliqueColors a Map of colors to use for each clique size
	 * @param <T> the node type of the graph
	 */
	public static <T> void circularVisualizeWithCliquesAndColors(Graphics2D g2, int width, int height, Map<T, Set<T>> neighbours, Map<Pair<T, T>, Integer> largestClique, Map<Integer, Color> cliqueColors) {
		Map<T, Point> nodePositions = new HashMap<>();
		int count = 0;
		int maxradius = (Math.min(width, height) - 20) / 2;
		int offsetX = (width - 2 * maxradius) / 2;
		int offsetY = (height - 2 * maxradius) / 2;
		for (T node : neighbours.keySet()) {
			double degrees = 2 * Math.PI / neighbours.size() * count++;
			nodePositions.put(node, new Point(offsetX + (int) Math.round(maxradius + Math.cos(degrees) * maxradius), offsetY + (int) Math.round(maxradius + Math.sin(degrees) * maxradius)));
		}
		// now draw everything
		List<T> verticesInOrder = new ArrayList<>(neighbours.keySet());
		for (int v1 = 0; v1 < verticesInOrder.size() - 1; v1++) {
			T vertex1 = verticesInOrder.get(v1);
			for (int v2 = v1 + 1; v2 < verticesInOrder.size(); v2++) {
				T vertex2 = verticesInOrder.get(v2);
				if (neighbours.getOrDefault(vertex1, Collections.emptySet()).contains(vertex2) || neighbours.getOrDefault(vertex2, Collections.emptySet()).contains(vertex1)) {
					int largest = largestClique.getOrDefault(new Pair<>(vertex1, vertex2), largestClique.getOrDefault(new Pair<>(vertex2, vertex1), 0));
					g2.setColor(cliqueColors.getOrDefault(largest, Color.gray));
					g2.drawLine(nodePositions.get(vertex1).x, nodePositions.get(vertex1).y, nodePositions.get(vertex2).x, nodePositions.get(vertex2).y);
				}
			}
		}
		for (Map.Entry<T, Point> nodePosition : nodePositions.entrySet()) {
			Point position = nodePosition.getValue();
			// vertex color is the color for the largest clique this node is a part of (if provided)
			g2.setColor(cliqueColors.getOrDefault(largestClique.getOrDefault(new Pair<>(nodePosition.getKey(), nodePosition.getKey()), 0), new Color(255, 196, 196)));
			g2.fillOval(position.x - 4, position.y - 4, 8, 8);
			g2.setColor(Color.black);
			g2.drawOval(position.x - 4, position.y - 4, 8, 8);
		}
	}

	/**
	 * Draws a visualization of the graph represented by <code>neighbours</code> on g2, using a force-directed algorithm to position the nodes.
	 * Edges and nodes all get a default coloring.
	 *
	 * @param g2 the Graphics2D to draw on
	 * @param width the width of the canvas
	 * @param height the height of the canvas
	 * @param neighbours a Map of neighbours for each node
	 * @param <T> the node type of the graph
	 */
	public static <T> void forceDirectedVisualize(Graphics2D g2, long width, long height, Map<T, Set<T>> neighbours) {
		forceDirectedVisualizeWithCliquesAndColors(g2, width, height, neighbours, Collections.emptyMap(), Collections.emptyMap());
	}

	/**
	 * Draws a visualization of the graph represented by <code>neighbours</code> on g2, using a force-directed algorithm to position the nodes.
	 * Edge and node coloring is done on the basis of clique size in the graph - the <code>largestClique</code> map should contain a value for
	 * each edge (as a Pair&lt;T, T&gt; (e1, e2)) as well as each node (as Pair&lt;T, T&gt; (n, n)). Any edge or node not found in the map will
	 * get a default color so you can pass an empty map to get an uncolored graph.
	 *
	 * @param g2 the Graphics2D to draw on
	 * @param width the width of the canvas
	 * @param height the height of the canvas
	 * @param neighbours a Map of neighbours for each node
	 * @param largestClique a Map of largest clique sizes for each node and edge
	 * @param cliqueColors a Map of colors to use for each clique size
	 * @param <T> the node type of the graph
	 */
	public static <T> void forceDirectedVisualizeWithCliquesAndColors(Graphics2D g2, long width, long height, Map<T, Set<T>> neighbours, Map<Pair<T, T>, Integer> largestClique, Map<Integer, Color> cliqueColors) {
		log.debug("Initialization.");
		long area = width * height;
		double t = Math.sqrt(Math.max(width, height));
		Set<Pair<T, T>> edges = new HashSet<>();
		List<T> verticesInOrder = new ArrayList<>(neighbours.keySet());
		log.debug("Enumerating edges.");
		for (int v1 = 0; v1 < verticesInOrder.size() - 1; v1++) {
			T vertex1 = verticesInOrder.get(v1);
			for (int v2 = v1 + 1; v2 < verticesInOrder.size(); v2++) {
				T vertex2 = verticesInOrder.get(v2);
				if (neighbours.getOrDefault(vertex1, Collections.emptySet()).contains(vertex2) || neighbours.getOrDefault(vertex2, Collections.emptySet()).contains(vertex1)) {
					edges.add(new Pair<>(vertex1, vertex2));
				}
			}
		}
		log.debug("Graph has " + edges.size() + " edges.");
		Map<T, Pair<Double, Double>> vPos = new HashMap<>();
		Map<T, Pair<Double, Double>> vDisp = new HashMap<>();
		log.debug("Generating random starting positions for " + verticesInOrder.size() + " nodes.");
		for (T key : neighbours.keySet()) {
			Pair<Double, Double> randomPos = new Pair<>(3 * width / 8 + Math.random() * width / 4, 3 * height / 8 + Math.random() * height / 4);
			vPos.put(key, randomPos);
		}
		double k = Math.sqrt(1.0 * area / neighbours.size());
		Function<Double, Double> fAttractive = x -> x * x / k;
		Function<Double, Double> fRepulsive = x -> k * k / x;
		int i = 0;
		while (t > 1) {
			i++;
			log.debug("Iteration: " + (i + 1) + ". Force parameter K = " + k + ". Temperature t = " + t);
			// calc repulsive forces
			for (T v : vPos.keySet()) {
				Pair<Double, Double> vDisplacement = new Pair<>(0.0, 0.0);
				for (T u : vPos.keySet()) {
					if (!v.equals(u)) {
						Pair<Double, Double> vPosition = vPos.get(v);
						Pair<Double, Double> uPosition = vPos.get(u);
						Pair<Double, Double> delta = new Pair<>(vPosition.getLeft() - uPosition.getLeft(), vPosition.getRight() - uPosition.getRight());
						double deltaLength = Math.sqrt(delta.getLeft() * delta.getLeft() + delta.getRight() * delta.getRight());
						vDisplacement = new Pair<>(vDisplacement.getLeft() + delta.getLeft() / deltaLength * fRepulsive.apply(deltaLength), vDisplacement.getRight() + delta.getRight() / deltaLength * fRepulsive.apply(deltaLength));
					}
					vDisp.put(v, vDisplacement);
				}
			}
			// calc attractive forces
			for (Pair<T, T> edge : edges) {
				T v = edge.getLeft();
				T u = edge.getRight();
				Pair<Double, Double> delta = new Pair<>(vPos.get(v).getLeft() - vPos.get(u).getLeft(), vPos.get(v).getRight() - vPos.get(u).getRight());
				double deltaLength = Math.sqrt(delta.getLeft() * delta.getLeft() + delta.getRight() * delta.getRight());
				Pair<Double, Double> vDisplacement = vDisp.getOrDefault(v, new Pair<>(0.0, 0.0));
				vDisplacement = new Pair<>(vDisplacement.getLeft() - delta.getLeft() / deltaLength * fAttractive.apply(deltaLength), vDisplacement.getRight() - delta.getRight() / deltaLength * fAttractive.apply(deltaLength));
				Pair<Double, Double> uDisplacement = vDisp.getOrDefault(v, new Pair<>(0.0, 0.0));
				uDisplacement = new Pair<>(uDisplacement.getLeft() + delta.getLeft() / deltaLength * fAttractive.apply(deltaLength), uDisplacement.getRight() + delta.getRight() / deltaLength * fAttractive.apply(deltaLength));
				vDisp.put(v, vDisplacement);
				vDisp.put(u, uDisplacement);
			}
			// limit max displacement to temperature and keep within frame
			for (T v : vPos.keySet()) {
				Pair<Double, Double> vDisplacement = vDisp.get(v);
				double vdLength = Math.sqrt(vDisplacement.getLeft() * vDisplacement.getLeft() + vDisplacement.getRight() * vDisplacement.getRight());
				Pair<Double, Double> vPosition = vPos.get(v);
				vPosition = new Pair<>(
						Math.min(width, Math.max(0, vPosition.getLeft() + vDisplacement.getLeft()/vdLength * Math.min(vdLength, t))),
						Math.min(height, Math.max(0, vPosition.getRight() + vDisplacement.getRight()/vdLength * Math.min(vdLength, t)))
				);
				vPos.put(v, vPosition);
			}
			// reduce temperature as layout approaches better configuration
			t = t * 0.9;
		}
		// now draw the vertices and edges
		for(Pair<T, T> edge : edges) {
			// edge color is the color for the largest clique both vertices in this edge are part of (if provided)
			int largest = largestClique.getOrDefault(edge, largestClique.getOrDefault(new Pair<>(edge.getRight(), edge.getLeft()), 0));
			g2.setColor(cliqueColors.getOrDefault(largest, Color.gray));
			g2.drawLine((int) Math.round(vPos.get(edge.getLeft()).getLeft()), (int) Math.round(vPos.get(edge.getLeft()).getRight()), (int) Math.round(vPos.get(edge.getRight()).getLeft()), (int) Math.round(vPos.get(edge.getRight()).getRight()));

		}
		for (Map.Entry<T, Pair<Double, Double>> nodePosition : vPos.entrySet()) {
			Pair<Double, Double> position = nodePosition.getValue();
			// vertex color is the color for the largest clique this node is a part of (if provided)
			g2.setColor(cliqueColors.getOrDefault(largestClique.getOrDefault(new Pair<>(nodePosition.getKey(), nodePosition.getKey()), 0), new Color(255, 196, 196)));
			g2.fillOval((int) Math.round(position.getLeft() - 4), (int) Math.round(position.getRight() - 4), 8, 8);
			g2.setColor(Color.black);
			g2.drawOval((int) Math.round(position.getLeft() - 4), (int) Math.round(position.getRight() - 4), 8, 8);
		}
	}
}
