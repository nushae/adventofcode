package com.nushae.aoc.util;

import lombok.extern.log4j.Log4j2;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.nushae.aoc.util.MathUtil.factorial;
import static java.lang.Math.max;

@Log4j2
public class AlgoUtil {

	private AlgoUtil() {
		// util class, no construction!
		// This class contains various nifty algorithms and support for algorithms
	}

	// ======================= list Manipulation and Analysis =======================

	/**
	 * return a list which is equal to the passed list, but with elements i and j (0..N-1) swapped, list must have at
	 * least max(i, j) + 1 elements or an IllegalArgumentException will be thrown
	 *
	 * @param list the source list
	 * @param elmA the first element's position
	 * @param elmB the second element's position
	 * @param <T> List subtype
	 * @return the reordered list
	 */
	public static <T> List<T> swapElements(List<T> list, int elmA, int elmB) {
		if (elmA >= list.size() || elmB >= list.size()) {
			throw new IllegalArgumentException("Can't swap elements " + elmA + " and " + elmB + " in a list with only " + list.size() + " rows, dummy");
		}
		List<T> result = new ArrayList<>(list);
		result.set(elmA, list.get(elmB));
		result.set(elmB, list.get(elmA));
		return result;
	}

	/**
	 * <p>An unoptimized implementation of the longest common subsequence algorithm.</p>
	 *
	 * @param left a list of items
	 * @param right another list of items
	 * @param <T> the type of items (must be comparable)
	 * @return the longest common subsequence between left and right
	 */
	public static <T> List<T> longestCommonSubsequence(List<T> left, List<T> right) {
		List<T> result = new ArrayList<>();

		if (left == null || right == null) {
			return result;
		}

		int[][] lcsTable = new int[left.size() + 1][right.size() + 1];
		for (int i = 0; i < left.size() + 1; i++) {
			for (int j = 0; j < right.size() + 1; j++) {
				if (i ==0 || j == 0) {
					lcsTable[i][j] = 0;
				} else if (left.get(i - 1).equals(right.get(j - 1))) {
					lcsTable[i][j] = lcsTable[i - 1][j - 1] + 1;
				} else {
					lcsTable[i][j] = max(lcsTable[i-1][j], lcsTable[i][j-1]);
				}
			}
		}

		for (int i = left.size(), j = right.size(); i > 0 && j > 0;) {
			if (left.get(i - 1).equals(right.get(j - 1))) {
				result.add(0, left.get(i - 1));
				i--;
				j--;
			} else if (lcsTable[i-1][j] > lcsTable[i][j-1]) {
				i--;
			} else {
				j--;
			}
		}

		return result;
	}

	// ============================== Other Utilities ===============================

	/**
	 * Calculate the MD5 hash for input
	 *
	 * @param input String
	 * @return MD5 hash
	 */
	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			return DatatypeConverter.printHexBinary(md.digest()).toLowerCase();
		} catch (NoSuchAlgorithmException e) {
			// should never occur
		}
		return "";
	}

	// ==================== Permutations and Combinations ================

	private static <T> List<T> permutationHelper(long index, LinkedList<T> in, List<T> out) {
		if (in.isEmpty()) {
			return out;
		}
		long subFactorial = factorial(in.size() - 1);
		out.add(in.remove((int) (index / subFactorial)));
		return permutationHelper(index % subFactorial, in, out);
	}

	/**
	 * Returns the lexicographically index-th permutation of the passed List items
	 * @param index lexicographical index of the permutation
	 * @param items List of items to permute
	 * @return the index-th permutation
	 * @param <T> the subtype of the list of items
	 */
	public static <T> List<T> permutation(long index, List<T> items) {
		return permutationHelper(index, new LinkedList<>(Objects.requireNonNull(items)), new ArrayList<>());
	}

	/**
	 * Returns a Stream of all permutations of the passed items, in lexicographical order
	 *
	 * @param items items to permute
	 * @return Stream&lt;List&lt;T&gt;&gt; of all permutations
	 * @param <T> The List subtype
	 */
	@SafeVarargs
	public static <T> Stream<List<T>> allPermutations(T... items) {
		return allPermutations(Arrays.asList(items));
	}

	/**
	 * Returns a Stream of all permutations of the passed List, in lexicographical order
	 *
	 * @param itemList List&lt;T&gt; of the items to permute
	 * @return Stream&lt;List&lt;T&gt;&gt; of all permutations
	 * @param <T> The List subtype
	 */
	public static <T> Stream<List<T>> allPermutations(List<T> itemList) {
		return LongStream.range(0, factorial(itemList.size()))
				.mapToObj(index -> permutation(index, itemList));
	}

	/**
	 * Generate a stream of all possibilities to choose count elements from a given array arr, in lexicographical order.
	 *
	 * @param count the size of the combinations
	 * @param arr the array of items
	 * @param <T> the type of the items
	 * @return a stream of all combinations of size count taken from arr
	 */
	public static <T> Stream<List<T>> allCombinations(int count, T[] arr) {
		if (count < 1 || count > arr.length) {
			throw new IllegalArgumentException("Combination size must be within the range of the array");
		}
		final long N = (long) Math.pow(2, arr.length);

		return StreamSupport.stream(new Spliterators.AbstractSpliterator<>(N, Spliterator.SIZED) {
			long i = 1;

			@Override
			public boolean tryAdvance(Consumer<? super List<T>> action) {
				while (Long.bitCount(i) != count) {
					i++;
				}
				if (i < N) {
					List<T> out = new ArrayList<>(Long.bitCount(i));
					for (int bit = 0; bit < arr.length; bit++) {
						if ((i & (1L << bit)) != 0) {
							out.add(arr[bit]);
						}
					}
					action.accept(out);
					i++;
					return true;
				} else {
					return false;
				}
			}
		}, false);
	}

	/**
	 * Generate a stream of all combinations of any size from a given array, in lexicographical order.
	 *
	 * @param arr the array of items
	 * @param <T> the type of the items
	 * @return a stream of all combinations of any size taken from arr
	 */
	public static <T> Stream<List<T>> allCombinations(T[] arr) {
		final long N = (long) Math.pow(2, arr.length);

		return StreamSupport.stream(new Spliterators.AbstractSpliterator<>(N, Spliterator.SIZED) {
			long i = 1;

			@Override
			public boolean tryAdvance(Consumer<? super List<T>> action) {
				if (i < N) {
					List<T> out = new ArrayList<>(Long.bitCount(i));
					for (int bit = 0; bit < arr.length; bit++) {
						if ((i & (1L << bit)) != 0) {
							out.add(arr[bit]);
						}
					}
					action.accept(out);
					i++;
					return true;
				} else {
					return false;
				}
			}
		}, false);
	}
}
