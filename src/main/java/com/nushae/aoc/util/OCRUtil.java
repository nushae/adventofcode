package com.nushae.aoc.util;

import java.util.Map;

public class OCRUtil {

	public static final String PIXEL_ON = "█";
	public static final String PIXEL_OFF = "░";

	/**
	 * <p><strong>4x6 letter forms</strong></p>
	 * <p><em>Used in 2016 day 8; 2019 Day 8 and 11; 2021 Day 13; 2022 Day 10.</em></p>
	 *
	 * <p>Every letter, except Y, fits the 4x6 box, and letters are separated by a single column of empty pixels. Rows
	 * are separated by a row of empty pixels too.</p>
	 *
	 * <p>The font appears to be kept monospaced - 'I' uses only the 2nd, 3rd, and 4th column (1st column is empty),
	 * while 'Y' is 5 wide but intrudes into the separator column that follows it.</p>
	 *
	 * <p> If you consider the extra whitespace columns and rows to be part of the characters, the characters are
	 * 'glued together' and are really 5x7 letter forms. Some of the puzzles that use this font output no empty column
	 * after the last letter, some do; the same applies to the separating empty row of pixels between rows. You can in
	 * general, for M rows of N characters, expect a pixel block of [5N or (5N - 1)] by [7M or (7M - 1)].</p>
	 *
	 * <p>You can either take this into account as you interpret the characters (complex) or simply adjust the matrix
	 * to be 5N by 7M. This table assumes the latter - each character includes the empty column behind it and the empty
	 * row underneath it.</p>
	 *
	 * <p>&rarr; If you get "?" for your letters, it might be somethign new; use drawCRT() to check.</p>
	 *
	 * <p>Supported (so far encountered) letters: ABC-EFGHIJKL--OP-RS-U---YZ</p>
	 */
	public static final Map<String,Character> CRT_FONT_4X6 = Map.ofEntries(
			Map.entry("00000000000000000000000000000000000", ' '),
			Map.entry("01100100101001011110100101001000000", 'A'),
			Map.entry("11100100101110010010100101110000000", 'B'),
			Map.entry("01100100101000010000100100110000000", 'C'),
			Map.entry("11110100001110010000100001111000000", 'E'),
			Map.entry("11110100001110010000100001000000000", 'F'),
			Map.entry("01100100101000010110100100111000000", 'G'),
			Map.entry("10010100101111010010100101001000000", 'H'),
			Map.entry("01110001000010000100001000111000000", 'I'),
			Map.entry("00110000100001000010100100110000000", 'J'),
			Map.entry("10010101001100010100101001001000000", 'K'),
			Map.entry("10000100001000010000100001111000000", 'L'),
			Map.entry("01100100101001010010100100110000000", 'O'),
			Map.entry("11100100101001011100100001000000000", 'P'),
			Map.entry("11100100101001011100101001001000000", 'R'),
			Map.entry("01110100001000001100000101110000000", 'S'),
			Map.entry("10010100101001010010100100110000000", 'U'),
			Map.entry("10001100010101000100001000010000000", 'Y'),
			Map.entry("11110000100010001000100001111000000", 'Z')
	);

	/**
	 * <p><strong>6x10 letter forms</strong></p>
	 * <p><em>Used in 2018 Day 10.</em></p>
	 *
	 * <p>Every letter fits the 6x10 box, and letters are separated by a single column of empty pixels. Rows
	 * are separated by a row of empty pixels too.</p>
	 *
	 * <p> If you consider the extra whitespace columns and rows to be part of the characters, the characters are
	 * 'glued together' and are really 7x11 letter forms. Puzzles that use this font may or may not output the empty
	 * column after the last letter; the same applies to the separating empty row of pixels under the last row of
	 * letters. You can in general, for M rows of N characters, expect a pixel block of [7N or (7N - 1)] by [11M or (11M - 1)].</p>
	 *
	 * <p>You can either take this into account as you interpret the characters (complex) or simply adjust the matrix
	 * to be 7N by 11M. This table assumes the latter - each character includes the empty column behind it and the empty
	 * row underneath it.</p>
	 *
	 * <p>&rarr; If you get "?" for your letters, it might be somethign new; use drawCRT() to check.</p>
	 *
	 * <p>Supported (so far encountered) letters: ABC-EFGHIJKL--OP-RS-U---YZ</p>
	 */
	public static final Map<String,Character> CRT_FONT_6X10 = Map.ofEntries(
			Map.entry("00000000000000000000000000000000000000000000000000000000000000000000000000000", ' '),
			Map.entry("00110000100100100001010000101000010111111010000101000010100001010000100000000", 'A'),
			Map.entry("11111001000010100001010000101111100100001010000101000010100001011111000000000", 'B'),
			Map.entry("01111001000010100000010000001000000100000010000001000000100001001111000000000", 'C'),
			Map.entry("11111101000000100000010000001111100100000010000001000000100000011111100000000", 'E'),
			Map.entry("11111101000000100000010000001111100100000010000001000000100000010000000000000", 'F'),
			Map.entry("01111001000010100000010000001000000100111010000101000010100011001110100000000", 'G'),
			Map.entry("10000101000010100001010000101111110100001010000101000010100001010000100000000", 'H'),
			Map.entry("00011100000100000010000001000000100000010000001001000100100010001110000000000", 'J'),
			Map.entry("10000101000100100100010100001100000110000010100001001000100010010000100000000", 'K'),
			Map.entry("10000001000000100000010000001000000100000010000001000000100000011111100000000", 'L'),
			Map.entry("10000101100010110001010100101010010100101010010101000110100011010000100000000", 'N'),
			Map.entry("11111001000010100001010000101111100100000010000001000000100000010000000000000", 'P'),
			Map.entry("11111001000010100001010000101111100100100010001001000100100001010000100000000", 'R'),
			Map.entry("10000101000010010010001001000011000001100001001000100100100001010000100000000", 'X'),
			Map.entry("11111100000010000001000001000001000001000001000001000000100000011111100000000", 'Z')
	);

	/**
	 * <p>Generate a String that contains the letters (assumed) in the passed grid. The grid must be of the correct
	 * dimensions (7 * rows x 5 * letters) - letters are assumed to be 1 pixel apart, trailing and bottom pixels may be
	 * omitted (so 7 * rows - 1 x 5 * letters - 1 is also acceptable). Unrecognized letters become "?"</p>
	 *
	 * @param grid a boolean [7*M (-1?)][5*N (-1?)] grid of booleans (true === 'on')
	 * @return the grid represented as a String of letters, each row separated by a newline
	 */
	public static String crt4x6AsString(boolean[][] grid) {
		// [rows][cols]
		int height = grid.length + (grid.length % 7 != 0 ? 1 : 0);
		int width = grid[0].length + (grid[0].length % 5 != 0 ? 1 : 0);
		boolean[][] lettersAdjusted = new boolean[height][width];
		for (int row = 0; row < grid.length; row++) {
			System.arraycopy(grid[row], 0, lettersAdjusted[row], 0, grid[0].length);
		}
		StringBuilder word = new StringBuilder();
		for (int c = 0; c < height / 7; c++) {
			for (int letter = 0; letter < width / 5; letter++) {
				StringBuilder stb = new StringBuilder();
				for (int row = 0; row < 7; row++) {
					String pixels = (lettersAdjusted[7*c + row][letter * 5] ? "1" : "0") +
							(lettersAdjusted[7*c + row][letter * 5 + 1] ? "1" : "0") +
							(lettersAdjusted[7*c + row][letter * 5 + 2] ? "1" : "0") +
							(lettersAdjusted[7*c + row][letter * 5 + 3] ? "1" : "0") +
							(lettersAdjusted[7*c + row][letter * 5 + 4] ? "1" : "0");
					stb.append(pixels);
				}
				if (CRT_FONT_4X6.containsKey(stb.toString())) {
					word.append(CRT_FONT_4X6.get(stb.toString()));
				} else {
					word.append("?");
				}
			}
			word.append("\n");
		}
		word.deleteCharAt(word.length()-1); // remove trailing \n because I prefer that
		return word.toString();
	}

	/**
	 * <p>Generate a String that contains the letters in the passed boolean[][] grid. The grid must be of the
	 * correct dimensions (11 * rows x 7 * letters) - letters are assumed to be 1 pixel apart, trailing and bottom pixels may be
	 * omitted (so 11 * rows - 1 x 7 * letters - 1 is also acceptable). Unrecognized letters become "?".</p>
	 *
	 * @param grid a boolean[11*M (-1?)][7*N (-1?)] grid (true === 'on')
	 * @return the grid represented as a String of letters, each row separated by a newline
	 */
	public static String crt6x10AsString(boolean[][] grid) {
		// [rows][cols] (yeah I know, not handy)
		int height = grid.length + (grid.length % 11 != 0 ? 1 : 0);
		int width = grid[0].length + (grid[0].length % 7 != 0 ? 1 : 0);
		boolean[][] lettersAdjusted = new boolean[height][width];
		for (int row = 0; row < grid.length; row++) {
			System.arraycopy(grid[row], 0, lettersAdjusted[row], 0, grid[0].length);
		}
		StringBuilder word = new StringBuilder();
		for (int c = 0; c < height / 11; c++) {
			for (int letter = 0; letter < width / 7; letter++) {
				StringBuilder stb = new StringBuilder();
				for (int row = 0; row < 11; row++) {
					String pixels = (lettersAdjusted[11*c + row][letter * 7] ? "1" : "0") +
							(lettersAdjusted[11*c + row][letter * 7 + 1] ? "1" : "0") +
							(lettersAdjusted[11*c + row][letter * 7 + 2] ? "1" : "0") +
							(lettersAdjusted[11*c + row][letter * 7 + 3] ? "1" : "0") +
							(lettersAdjusted[11*c + row][letter * 7 + 4] ? "1" : "0") +
							(lettersAdjusted[11*c + row][letter * 7 + 5] ? "1" : "0") +
							(lettersAdjusted[11*c + row][letter * 7 + 6] ? "1" : "0");
					stb.append(pixels);
				}
				if (CRT_FONT_6X10.containsKey(stb.toString())) {
					word.append(CRT_FONT_6X10.get(stb.toString()));
				} else {
					word.append("?");
				}
			}
			word.append("\n");
		}
		word.deleteCharAt(word.length()-1); // remove trailing \n because I prefer that
		return word.toString();
	}

	/**
	 * <p>Display the passed boolean[rows][cols] as a block of pixels, rows high and cols wide; true === 'on'</p>
	 *
	 * @param grid of booleans representing crt pixels
	 */
	public static String drawCRT(boolean[][] grid) {
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < grid.length; y++) {
			for (int x = 0; x < grid[0].length; x++) {
				sb.append(grid[y][x] ? PIXEL_ON : PIXEL_OFF);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}