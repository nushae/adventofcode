package com.nushae.aoc.util;

import com.nushae.aoc.domain.Triplet;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class ConvertUtil {

	private ConvertUtil() {
		// util class, no construction!
		// This class contains various conversion utils
	}

	public static String humanTime(long millis) {
		List<Triplet<Long, String, Boolean>> chunks = new ArrayList<>();
		chunks.add(new Triplet<>(24*3600000L, " day", true));
		chunks.add(new Triplet<>(3600000L, " hour", true));
		chunks.add(new Triplet<>(60000L, " minute", true));
		chunks.add(new Triplet<>(1000L, "s", false));
		chunks.add(new Triplet<>(1L, "ms", false));
		String result = "";
		for (Triplet<Long, String, Boolean> chunk : chunks) {
			long val = millis / chunk.getLeft();
			if (val > 0) {
				millis = millis % chunk.getLeft();
				result += (result.isEmpty() ? "" : ", " + (millis == 0 ? "and " : "")) + val + chunk.getMid() + (chunk.getRight() && val != 1 ? "s" : "");
			}
		}
		return result;
	}
}
