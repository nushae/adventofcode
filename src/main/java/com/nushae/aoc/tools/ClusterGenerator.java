package com.nushae.aoc.tools;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * generates a list of edges for a graph that has every clique size up to a certain limit, as well as some random edges running between cliques
 */
public class ClusterGenerator {

	public static void main(String[] args) {
		int clustersize = 3;
		Set<String> nodenames = new HashSet<>();
		for (char cluster = 'a'; cluster <= 'k'; cluster++) {
			for (int left = 0; left < clustersize - 1; left++) {
				for (int right = left + 1; right < clustersize; right++) {
					String leftName = cluster + StringUtils.leftPad("" + left, 2, "0");
					String rightName = cluster + StringUtils.leftPad("" + right, 2, "0");
					nodenames.add(leftName);
					nodenames.add(rightName);
					System.out.println(leftName + "-" + rightName);
				}
			}
			clustersize++;
		}
		List<String> nodes = new ArrayList<>(nodenames);
		Collections.sort(nodes);
		for (int left = 0; left < nodes.size() - 1; left++) {
			String leftName = nodes.get(left);
			for (int right = left + 1; right < nodes.size(); right++) {
				String rightName = nodes.get(right);
				if (leftName.charAt(0) != rightName.charAt(0) && Math.random() < 0.01) {
					System.out.println(nodes.get(left) + "-" + nodes.get(right));
				}
			}
		}
	}
}
