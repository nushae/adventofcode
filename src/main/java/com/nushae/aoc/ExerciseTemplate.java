package com.nushae.aoc;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2025, day = 0, title="", keywords = {})
public class ExerciseTemplate {

	private static final Pattern DUMMY = Pattern.compile("");

	List<String> lines;
	List<Long> numbers;

	public ExerciseTemplate() {
		lines = new ArrayList<>();
	}

	public ExerciseTemplate(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public ExerciseTemplate(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		numbers = new ArrayList<>();
		for (var line : lines) {
			// do stuff
		}
	}

	public void processInputWithPatterns() {
		for (var line : lines) {
			var m = DUMMY.matcher(line);
			if (m.find()) {
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		var accum = 0L;
		return accum;
	}

	public long doPart2() {
		var accum = 0L;
		return accum;
	}

	public static void main(String[] args) {
		// var ex = new ExerciseTemplate("");
		var ex = new ExerciseTemplate(aocPath(2025, -1));

		// Process input
		var start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		// ex.processInputWithPatterns();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
