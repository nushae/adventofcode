package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.WristDevice;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise19 {

	WristDevice appleWatch = new WristDevice();
	List<String> lines;

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		appleWatch.loadProgramFromData(lines);
		return appleWatch.runProgram(0,0,0,0,0, 0).a;
	}

	public long doPart2() {
		return doCode(1,0,0,0,0, 0); // interpreter would run for > 48 hrs
	}

	/* Code analysis
     * 00 addi 2 16 2    {2} += 16                      jmp 17
     *
     * 01 seti 1 1 1     [1] = 1                        [1] = 1
     * 02 seti 1 4 3     [3] = 1                        [3] = 1
     * 03 mulr 1 3 5     [5] = [1] * [3]                if [1] * [3] == [4] then [0] += [1]
     * 04 eqrr 5 4 5     [5] = [5] == [4] ? 1 : 0       NOP
     * 05 addr 5 2 2     {2} += [5]                     NOP
     *
     * 06 addi 2 1 2     {2} += 1                       NOP
     *
     * 07 addr 1 0 0     [0] += [1]                     NOP
     * 08 addi 3 1 3     [3] += 1                       [3]++
     * 09 gtrr 3 4 5     [5] = [3] > [4] ? 1 : 0        if [3] <= [4] then jmp 3
     * 10 addr 2 5 2     {2} += [5]                     NOP
     *
     * 11 seti 2 4 2     {2} = 2                        NOP
     *
     * 12 addi 1 1 1     [1] += 1                       [1]++
     * 13 gtrr 1 4 5     [5] = [1] > [4] ? 1 : 0        if [1] <= [4] then jmp 2 else HALT
     * 14 addr 5 2 2     {2} += [5]                     NOP
     *
     * 15 seti 1 0 2     {2} = 1                        NOP
     *
     * 16 mulr 2 2 2     {2} *= [2]                     NOP
     *
     * 17 addi 4 2 4     [4] += 2                       [4] = ([4] + 2)^2 * 209 + 39
     * 18 mulr 4 4 4     [4] *= [4]                     NOP
     * 19 mulr 2 4 4     [4] *= [2]                     NOP
     * 20 muli 4 11 4    [4] *= 11                      NOP
     * 21 addi 5 1 5     [5] += 1                       NOP
     * 22 mulr 5 2 5     [5] *= [2]                     NOP
     * 23 addi 5 17 5    [5] += 17                      NOP
     * 24 addr 4 5 4     [4] += [5]                     NOP
     * 25 addr 2 0 2     {2} += [0]                     if [0] == 0 then jmp 1 else [4] += 10550400; [0] = 0; jmp 1
     *
     * 26 seti 0 9 2     {2} = 0                        NOP
     *
     * 27 setr 2 3 5     [5] = [2]                      NOP
     * 28 mulr 5 2 5     [5] *= [2]                     NOP
     * 29 addr 2 5 5     [5] += [2]                     NOP
     * 30 mulr 2 5 5     [5] *= [2]                     NOP
     * 31 muli 5 14 5    [5] *= 14                      NOP
     * 32 mulr 5 2 5     [5] *= [2]                     NOP
     *
     * 33 addr 4 5 4     [4] += [5]                     NOP
     * 34 seti 0 9 0     [0] = 0                        NOP
	 * 35 seti 0 6 2     {2} = 0                        NOP
	 *
	 * So we have two nested loops using [1] and [3] as loop counters, that together add up all the divisors of [4] into [0]
	 * [5] is used for boolean logic and temporary values so can be eliminated completely; so can [2] after 'decompiling'
	 * The nested loop can be tightened to a single loop using [4] % [1] == 0 as test.
	 */
	public long doCode(long reg0, long reg1, long reg2, long reg3, long reg4, long reg5) {
		// if reg0 > 1 the result is undefined
		reg4 = 875; // part 1: sum of divisors of 855 = 1248
		if (reg0 == 1) {
			reg0 = 0;
			reg4 = 10551275; // part 2: sum of divisors of 10551275 = 14952912
		}
		for (reg1 = 1; reg1 <= reg4; reg1++) {
			if (reg4 % reg1 == 0) {
				reg0 += reg1;
			}
		}
		return reg0;
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2018, 19));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println("Part 1 with interpreter: " + ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		start = System.currentTimeMillis();
		System.out.println("Part 1 with doCode: " + ex.doCode(0,0,0,0,0,0));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println("Part 2 with doCode: " + ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
