package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.FighterGroup;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise24 {

	private static final boolean REPORT = false;

	private static final Pattern FIGHTER_GROUP = Pattern.compile("(\\d+) units each with (\\d+) hit points([ (].*\\)?)with an attack that does (\\d+) ([a-z]+) damage at initiative (\\d+)");
	private static final Pattern IMMUNITIES_WEAKNESSES = Pattern.compile("(immune|weak) to ([a-z, ]+)");

	List<String> lines;
	List<FighterGroup> defenders;
	List<FighterGroup> infecters;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern(long boost) {
		boolean defenderList = true;
		defenders = new ArrayList<>();
		infecters = new ArrayList<>();
		int num = 0;
		int id = 0;
		for (String line : lines) {
			if ("Immune System:".equals(line)) {
				continue;
			}
			if ("".equals(line)) {
				id++;
				continue;
			}
			if ("Infection:".equals(line)) {
				defenderList = false;
				num = 0;
				id++;
				continue;
			}
			Matcher m = FIGHTER_GROUP.matcher(line);
			if (m.find()) {
				num++;
				id++;
				FighterGroup f = new FighterGroup(id, num, defenderList, Long.parseLong(m.group(1)), Long.parseLong(m.group(2)), Integer.parseInt(m.group(6)), Long.parseLong(m.group(4)) + (defenderList ? boost : 0), m.group(5));
				if (!" ".equals(m.group(3))) {
					for (String iw : m.group(3).split("; ")) {
						Matcher m2 = IMMUNITIES_WEAKNESSES.matcher(iw);
						if (m2.find()) {
							for (String tp : m2.group(2).split(", ")) {
								if ("weak".equals(m2.group(1))) {
									f.addWeakness(tp);
								} else {
									f.addImmunity(tp);
								}
							}
						} else {
							log.warn("Don't know how to parse '" + iw + "'");
						}
					}
				}
				if (defenderList) {
					defenders.add(f);
				} else {
					infecters.add(f);
				}
			} else {
				log.warn("Don't know how to parse '" + line + "'");
			}
		}
		if (REPORT) {
			System.out.println("Immune System:");
			for (FighterGroup fg : defenders) {
				System.out.println(fg);
			}
			System.out.println();
			System.out.println("Infection:");
			for (FighterGroup fg : infecters) {
				System.out.println(fg);
			}
		}
		log.debug("Input processed.");
	}

	public void statusReport() {
		System.out.println("Immune System:");
		boolean groupsRemain = false;
		for (FighterGroup f : defenders) {
			if (f.units > 0) {
				groupsRemain = true;
				System.out.println(f.name + " contains " + f.units + " units");
			}
		}
		if (!groupsRemain) {
			System.out.println("No groups remain.");
		}
		System.out.println("Infection:");
		groupsRemain = false;
		for (FighterGroup f : infecters) {
			if (f.units > 0) {
				groupsRemain = true;
				System.out.println(f.name + " contains " + f.units + " units");
			}
		}
		if (!groupsRemain) {
			System.out.println("No groups remain.");
		}
	}

	public long doPart1() {
		return Math.abs(doFight(true, 0));
	}

	public long doPart2() {
		int lowerBound = 0;
		// find large enough upper bound:
		int upperBound = 1;
		int boost = 1;
		while ((doFight(false, boost)) <= 0) {
			upperBound = upperBound * 2;
			boost = upperBound;
		}
		// now do binary search.
		// Technically we are not dealing with an ascending function, but we CAN say that there is a value N
		// such that for all n < N, f(n) <=0, and for all values m >= N, f(m) > 0. And that is enough for our purposes.
		long result = -1;
		while (lowerBound < upperBound) {
			boost = lowerBound + (upperBound - lowerBound) / 2;
			result = doFight(false, boost);
			if (result > 0) { // defenders won, try lower boost
				upperBound = boost-1;
			} else { // defenders did not win (includes stalemate), raise boost
				lowerBound = boost+1;
			}
		}
		return result;
	}

	/**
	 * Perform the fight, boosting Immune System units with boost extra dmg. (For part 1, use boost == 0.)
	 * Returns the total remaining units; the total will be negative if the defenders lost, positive if they won,
	 * and 0 if the fight is inconclusive
	 * There can be boosts for which neither group wins (eg, all survivors deal too little damage to kill a single unit)
	 * The simplest solution for this is track the total units remaining in the fight after each round and if that
	 * no longer changes we're in stalemate -> abort fight.
	 *
	 * @param boost amount to boost the attack value sof the defenders with
	 * @return total remaining units; negative if defenders lost, positive if they won, 0 if stalemate
	 */
	public long doFight(boolean reportRounds, long boost) {
		processWithPattern(boost); // reparse for every fight, since we add in the boost into the unit stats.
		if (reportRounds) System.out.println("Boost: " + boost);
		List<FighterGroup> allParticipants = new ArrayList<>(infecters);
		allParticipants.addAll(defenders);
		// These two will represent the actual groups fighting and dying, and defenders / infecters will function as lookup tables
		Set<Integer> defenderIndexes = IntStream.range(0, defenders.size()).boxed().collect(Collectors.toSet());
		Set<Integer> infecterIndexes = IntStream.range(0, infecters.size()).boxed().collect(Collectors.toSet());
		int round = 1;
		long previousTotalUnits = 0;
		long totalUnits = 0;
		if (reportRounds) System.out.println();
		while (defenderIndexes.size() > 0 && infecterIndexes.size() > 0) { // do another attack round
			allParticipants.sort((a, b) -> 100 * (int) (b.effPow() - a.effPow()) + b.ini() - a.ini()); // effective pow, then ini, both hi to low
			if (reportRounds) System.out.println("Attack round " + round);
			if (reportRounds) System.out.println();
			if (reportRounds) statusReport();
			// target selection:
			Set<Integer> unselectedDefenderTargets = new HashSet<>(defenderIndexes);
			Set<Integer> unselectedInfecterTargets = new HashSet<>(infecterIndexes);
			Map<FighterGroup, Integer> selectedTargets = new HashMap<>();
			for (FighterGroup currentGroup : allParticipants) {
				if (currentGroup.units > 0) { // skip eliminated groups
					totalUnits += currentGroup.units;
					Set<Integer> targetIndexes = (currentGroup.isDefender() ? unselectedInfecterTargets : unselectedDefenderTargets);
					List<FighterGroup> targets = currentGroup.isDefender() ? infecters : defenders;
					long bestDmg = -1L;
					int bestTargetIndex = -1;
					for (int targetIndex : targetIndexes) {
						FighterGroup target = targets.get(targetIndex);
						if (!target.immune.contains(currentGroup.damageType)) { // skip immune targets
							long effDmg = target.effDmgReceived(currentGroup.effPow(), currentGroup.damageType);
							log.debug(currentGroup.name + "=" + currentGroup.id + " would deal " + target.name + "=" + target.id + " (" + target.effPow() + "/"+ target.ini() + ") " + effDmg + " damage");
							if (effDmg > bestDmg ||	effDmg == bestDmg && (bestTargetIndex < 0 || target.isBetterTarget(targets.get(bestTargetIndex)))) {
								bestDmg = effDmg;
								bestTargetIndex = targetIndex;
							}
						}
					}
					if (bestTargetIndex >= 0) { // we actually selected a target, store it
						log.debug(currentGroup.id + " selects " + allParticipants.get(bestTargetIndex).id);
						targetIndexes.remove(bestTargetIndex);
						selectedTargets.put(currentGroup, bestTargetIndex);
					}
				}
			}
			// fights:
			if (reportRounds) System.out.println();
			if (reportRounds) System.out.println("Attacks:");
			for (Map.Entry<FighterGroup, Integer> entry : selectedTargets.entrySet().stream().sorted((entrya, entryb) -> entryb.getKey().ini() - entrya.getKey().ini()).collect(Collectors.toList())) {
				FighterGroup a = entry.getKey();
				if (a.units > 0) { // could be killed during this loop already
					int bIndex = entry.getValue();
					FighterGroup b = (a.isDefender() ? infecters : defenders).get(bIndex);
					long killed = b.receiveDmg(a.effPow(), a.damageType);
					totalUnits -= killed;
					if (reportRounds) System.out.print(a.name + " attacks " + b.name + ", killing " + killed + " units");
					if (b.units < 1) {
						if (reportRounds) System.out.println(" (group eliminated)");
						if (b.isDefender()) {
							defenderIndexes.remove(bIndex);
						} else {
							infecterIndexes.remove(bIndex);
						}
					} else {
						if (reportRounds) System.out.println();
					}
				}
			}
			if (reportRounds) System.out.println();
			if (reportRounds) System.out.println("Attack round " + round + " complete, " + defenderIndexes.size() + " defender(s) remaining; " + infecterIndexes.size() + " attacker(s) remaining.");
			if (reportRounds) System.out.println();
			long total = 0;
			for (FighterGroup fg : allParticipants) {
				total += fg.units;
			}
			if (previousTotalUnits == totalUnits) {
				return 0;
			}
			previousTotalUnits = totalUnits;
			totalUnits = 0;
			round++;
		}
		return previousTotalUnits * (defenderIndexes.size() == 0 ? -1 : 1);
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2018, 24));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
