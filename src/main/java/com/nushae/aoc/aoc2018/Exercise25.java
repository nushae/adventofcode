package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Coord4D;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise25 {

	private static final boolean REPORT = false;
	private static final Pattern COORD_4D = Pattern.compile("(-?\\d+),(-?\\d+),(-?\\d+),(-?\\d+)");

	List<String> lines;
	List<Coord4D> coordinates;

	public Exercise25() {
		lines = new ArrayList<>();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise25(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		coordinates = new ArrayList<>();
		for (String line : lines) {
			Matcher m = COORD_4D.matcher(line);
			if (m.find()) {
				coordinates.add(new Coord4D(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4))));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		if (REPORT) {
			for (Coord4D c : coordinates) {
				System.out.println(c);
			}
			log.info("Input processed. " + coordinates.size() + " coordinates read.");
		}
	}

	public long doPart1() {
		processWithPattern();
		// Look, we're really building a (probably) disconnected graph here, with each subgraph == a constellation.
		// So, we simply connect any two coordinates that are 'adjacent' (ie manhattan distance <= 3) and then find
		// out how many sub-graphs there are!
		for (int i = 0; i < coordinates.size()-1; i++) {
			Coord4D left = coordinates.get(i);
			for (int j = i+1; j < coordinates.size(); j++) {
				Coord4D right = coordinates.get(j);
				if (left.manhattan(right) <= 3) {
					left.addNeighbour(right);
					right.addNeighbour(left);
				}
			}
		}
		// now count sub-graphs
		long constellations = 0;
		while (coordinates.size() > 0) {
			Set<Coord4D> constellation = coordinates.get(0).extent();
			coordinates.removeAll(constellation);
			constellations++;
		}
		return constellations;
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2018, 25));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// As is tradition, no part 2
	}
}
