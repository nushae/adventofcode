package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise17 {

	private static final Pattern RANGE = Pattern.compile("([xy])=(\\d+), ([xy])=(\\d+)\\.\\.(\\d+)");

	List<String> lines;
	char[][] grid;
	int minX;
	int maxX;
	int minY;
	int maxY;
	int width;
	int height;

	public Exercise17() {
		lines = new ArrayList<>();
	}

	public Exercise17(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise17(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		minX = Integer.MAX_VALUE;
		maxX = Integer.MIN_VALUE;
		minY = Integer.MAX_VALUE;
		maxY = Integer.MIN_VALUE;
		for (String line : lines) {
			Matcher m = RANGE.matcher(line);
			if (m.find()) {
				if ("x".equals(m.group(1))) {
					minX = Math.min(minX, Integer.parseInt(m.group(2)));
					maxX = Math.max(maxX, Integer.parseInt(m.group(2)));
					minY = Math.min(minY, Integer.parseInt(m.group(4)));
					maxY = Math.max(maxY, Integer.parseInt(m.group(5)));
				} else {
					minX = Math.min(minX, Integer.parseInt(m.group(4)));
					maxX = Math.max(maxX, Integer.parseInt(m.group(5)));
					minY = Math.min(minY, Integer.parseInt(m.group(2)));
					maxY = Math.max(maxY, Integer.parseInt(m.group(2)));
				}
			}
		}
		// minimize the area stored, but make sure there is one column of empty space on the left and the right.
		minX--;
		maxX++;
		width = maxX - minX + 1;
		height = maxY - minY + 1;
		grid = new char[width][height];
		for (int i=0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				grid[i][j] = '.';
			}
		}
		for (String line : lines) {
			Matcher m = RANGE.matcher(line);
			if (m.find()) {
				int loX;
				int hiX;
				int loY;
				int hiY;
				if ("x".equals(m.group(1))) {
					loX = Integer.parseInt(m.group(2));
					hiX = loX;
					loY = Integer.parseInt(m.group(4));
					hiY = Integer.parseInt(m.group(5));
				} else {
					loX = Integer.parseInt(m.group(4));
					hiX = Integer.parseInt(m.group(5));
					loY = Integer.parseInt(m.group(2));
					hiY = loY;
				}
				for (int x = loX; x <= hiX; x++) {
					for (int y = loY; y <= hiY; y++) {
						grid[x-minX][y-minY] = '#';
					}
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public Point waterCount() {
		processWithPattern();
		int count = 0;
		int stillCount = 0;
		List<Point> nextPoints = new ArrayList<>();
		nextPoints.add(new Point(500-minX, 0));
		while (!nextPoints.isEmpty()) {
			Point nextPoint = nextPoints.remove(0);
			int nextY = findNextClayer(nextPoint.x, nextPoint.y);
			if (nextY < height) {
				nextPoints.addAll(fillLayer(nextPoint.x, nextY));
			}
		}
		fixWater();
		// done
		System.out.printf("%n%nx=%d-%d / y=%d-%d -> %dx%d%n", minX, maxX, minY, maxY, maxX-minX+1, maxY-minY+1);
		System.out.println(StringUtils.repeat(' ', 500-minX+"0000: ".length()) + '+');
		for (int y = 0; y < height; y++) {
			System.out.printf("%04d: ", y+minY);
			for (int x = 0; x < width; x++) {
				if (grid[x][y] == '|') {
					count++;
				}
				if (grid[x][y] == '~') {
					count++;
					stillCount++;
				}
				System.out.print(grid[x][y]);
			}
			System.out.println();
		}
		return new Point(count, stillCount);
	}

	// my solution has a curious feature where sometimes | will remain in a triangular formation inside the still water, like so:
	//          |
	// |||||||||||||||||||
	// |#~~~~~~~|~|~|~~~#|
	// |#~~~~~~~~|~|~~~~#|
	// |#~~~~~~~~~|~~~~~#|
	// |#~~~~~~~~~~~~~~~#|
	// |#~~~~#~~~#~~~~~~#|
	// |#~~~~#~~~#~~~~~~#|
	// |#~~~~#~~~#~~~~~~#|
	// |#~~~~#####~~~~~~#|
	// |#~~~~~~~~~~~~~~~#|
	// |#~~~~~~~~~~~~~~~#|
	// |#~~~~~~~~~~~~~~~#|
	// |#~~~~~~~~~~~~~~~#|
	// |#################|
	// For part 1 this is no issue, but part 2 asks for only the still water (~) so rather than bughunt and lose time
	// we add a routine that replaces all | immediately to the right or left of a '~' with a '~' (ugh, but fast)
	public void fixWater() {
		for (int x=0; x<width; x++) {
			for (int y = 0; y < height; y++) {
				if (grid[x][y] == '|' && (x > 0 && grid[x-1][y] == '~' || x < width-1 && grid[x+1][y] == '~')) {
					grid[x][y] = '~';
				}
			}
		}
	}

	// find the next clay layer (clayer, get it) from x,y going straight down. If return-val > height, no more clay and done.
	// water can rest on water or clay, so test for both (ignore | because those layers have already spilled and are covered ground)
	public int findNextClayer(int fromX, int fromY) {
		while (fromY < height && grid[fromX][fromY] != '#' && grid[fromX][fromY] != '~') {
			// testing for '|' prevents going over ground we already covered
			if (fromY < height - 1 && grid[fromX][fromY] == '|' && grid[fromX][fromY+1] == '~') { // definitely an area some other water source has filled
				return maxY;
			}
			grid[fromX][fromY] = '|';
			fromY++;
		}
		if (fromY >= height) {
			return maxY; // any number large enough will stop the process
		}
		return fromY-1;
	}

	// fill current layer and return the spill point(s)
	public List<Point> fillLayer(int fromX, int fromY) {
		List<Point> result = new ArrayList<>();
		boolean leftSettle;
		boolean rightSettle;
		int leftX;
		int rightX;
		do {
			grid[fromX][fromY] = '|'; // (since we skip it in edge cases where the water rises above the 'from' point)
			leftX = fromX-1;
			rightX = fromX+1;
			// leftward seek.
			while (leftX >= 0 && (grid[leftX][fromY] == '.' || grid[leftX][fromY] == '|') && (grid[leftX][fromY + 1] == '#' || grid[leftX][fromY + 1] == '~')) {
				grid[leftX][fromY] = '|';
				leftX--;
			}
			// either leftX < 0, leftX, fromY+1 != '#'/'~', or (leftX, fromY) == '#'. Only in the latter case can the water settle (on this side)
			leftSettle = (leftX >= 0 && grid[leftX][fromY] == '#');
			// rightward seek, as above.
			while (rightX < width && (grid[rightX][fromY] == '.' || grid[rightX][fromY] == '|') && (grid[rightX][fromY + 1] == '#' ||grid[rightX][fromY + 1] == '~')) {
				grid[rightX][fromY] = '|';
				rightX++;
			}
			rightSettle = (rightX < width && grid[rightX][fromY] == '#');
			if (leftSettle && rightSettle) {
				for (int x = leftX + 1; x < rightX; x++) {
					grid[x][fromY] = '~';
				}
				fromY--;
			}
		} while (leftSettle && rightSettle);
		if (!leftSettle && leftX >= 0) { // recurse
			result.add(new Point(leftX, fromY));
		}
		if (!rightSettle && rightX < width) { // recurse
			result.add(new Point(rightX, fromY));
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17(aocPath(2018, 17));

		// part 1 and part 2 are both done by a single routine that returns new Point(part1sol, part2sol)
		long start = System.currentTimeMillis();
		Point sols = ex.waterCount();
		System.out.println("Part 1: " + sols.x);
		System.out.println("Part 2: " + sols.y);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
