package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Circle;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class Exercise09 {

	public long doGame(int numplayers, int lastMarble) {
		Circle<Integer> circle = new Circle<>();
		long[] scores = new long[numplayers];
		long hiScore = -1;
		int currentPlayer = 0;
		circle.add(0);
		for (int nextMarble = 1; nextMarble <= lastMarble; nextMarble++) {
			if (nextMarble % 23 == 0) {
				circle.rotate(-7);
				scores[currentPlayer] += nextMarble + circle.pop();
				hiScore = Math.max(hiScore, scores[currentPlayer]);
			} else {
				circle.rotate(2);
				circle.addLast(nextMarble);
			}
			currentPlayer = (currentPlayer + 1) % numplayers;
		}
		return hiScore;
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doGame(403, 71920));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doGame(403, 7192000));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
