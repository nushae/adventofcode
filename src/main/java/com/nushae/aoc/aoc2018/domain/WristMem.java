package com.nushae.aoc.aoc2018.domain;

// used in exercise 16 and 19
public class WristMem {
	public int a = 0;
	public int b = 0;
	public int c = 0;
	public int d = 0;
	public int e = 0;
	public int f = 0;

	public WristMem() {}

	public WristMem(int a, int b, int c, int d) {
		this(a, b, c, d, 0, 0);
	}

	public WristMem(int a, int b, int c, int d, int e, int f) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
	}

	public int get(int idx) {
		switch (idx) {
			case 0:
				return a;
			case 1:
				return b;
			case 2:
				return c;
			case 3:
				return d;
			case 4:
				return e;
			case 5:
				return f;
			default:
				throw new IllegalArgumentException("out of 0-5 range: " + idx);
		}
	}

	public void set(int idx, int val) {
		switch (idx) {
			case 0:
				a = val;
				break;
			case 1:
				b = val;
				break;
			case 2:
				c = val;
				break;
			case 3:
				d = val;
				break;
			case 4:
				e = val;
				break;
			case 5:
				f = val;
				break;
			default:
				throw new IllegalArgumentException("out of 0-5 range: " + idx);
		}
	}
}
