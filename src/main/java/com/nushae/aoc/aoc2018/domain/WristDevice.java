package com.nushae.aoc.aoc2018.domain;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
public class WristDevice {
	public static final List<String> DEFAULT_OPCODES = Arrays.asList("addi", "addr", "bani", "banr", "bori", "borr", "eqir", "eqri", "eqrr", "gtir", "gtri", "gtrr", "muli", "mulr", "seti", "setr");

	int instructionPointer;
	int boundRegister;
	private List<WristMem> program;
	private WristMem registers;
	private Map<Integer, String> opcodes;
	private Map<String, Integer> numCodes;

	public WristDevice() {
		setOpcodes(DEFAULT_OPCODES);
		clearMem();
	}

	public void clearMem() {
		program = new ArrayList<>();
		registers = new WristMem();
		boundRegister = -1;
		instructionPointer = 0;
	}

	public void loadProgramFromFile(Path path) {
		List<String> lines = new ArrayList<>();
		loadInputFromFile(lines, path);
		loadProgramFromData(lines);
	}

	public void loadProgramFromData(List<String> lines) {
		clearMem();
		for (String line : lines) {
			String[] parts = line.trim().split(" ");
			if (parts[0].startsWith("#")) { // this is only supposed to be the first line, if any
				setBoundRegister(Integer.parseInt(parts[1]));
				log.debug("Bound register was set to " + parts[1] + " and became: " + boundRegister);
			} else {
				if (parts[0].matches("\\d+")) { // numeric opcode, we're good
					program.add(new WristMem(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3])));
				} else { // translate string opcode back to numeric one
					program.add(new WristMem(numCodes.get(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3])));
				}
			}
		}
		log.debug("Program size: " + program.size());
	}

	public void setOpcodes(List<String> opcodeOverrules) {
		opcodes = new HashMap<>();
		numCodes = new HashMap<>();
		for (int i =0; i < opcodeOverrules.size(); i++) {
			opcodes.put(i, opcodeOverrules.get(i));
			numCodes.put(opcodeOverrules.get(i), i);
		}
	}

	public void setBoundRegister(int reg) {
		if (reg >=0 && reg < 6) {
			boundRegister = reg;
		} else {
			throw new IllegalArgumentException("Register outside valid range 0-5: " + reg);
		}
	}

	public WristMem execute(WristMem regs, String opcode, int par1, int par2, int par3) {
		WristMem result = new WristMem(regs.a, regs.b, regs.c, regs.d, regs.e, regs.f);
		switch (opcode) {
			case "addr": // add reg(par1) to reg(par2) store in reg(par3)
				par2 = regs.get(par2);
			case "addi": // add reg(par1) to par2 store in reg(par3)
				result.set(par3, regs.get(par1) + par2);
				break;
			case "mulr": // multiply reg(par1) by reg(par2) store in reg(par3)
				par2 = regs.get(par2);
			case "muli": // multiply reg(par1) by par2 store in reg(par3)
				result.set(par3, regs.get(par1) * par2);
				break;
			case "banr": // bitwise and reg(par1) with reg(par2) store in reg(par3)
				par2 = regs.get(par2);
			case "bani": // bitwise and reg(par1) with par2 store in reg(par3)
				result.set(par3, regs.get(par1) & par2);
				break;
			case "borr": // bitwise or reg(par1) with reg(par2) store in reg(par3)
				par2 = regs.get(par2);
			case "bori": // bitwise or reg(par1) with par2 store in reg(par3)
				result.set(par3, regs.get(par1) | par2);
				break;
			case "setr": // store reg(par1) in reg(par3)
				par1 = regs.get(par1);
			case "seti": // store par1 in reg(par3)
				result.set(par3, par1);
				break;
			case "gtir": // store 1 in reg(par3) if par1 > reg(par2), 0 otherwise
			case "gtri": // store 1 in reg(par3) if reg(par1) > par2, 0 otherwise
			case "gtrr": // store 1 in reg(par3) if reg(par1) > reg(par2), 0 otherwise
				if (opcode.charAt(2) == 'r') { // retrieve reg a
					par1 = regs.get(par1);
				}
				if (opcode.charAt(3) == 'r') { // retrieve reg b
					par2 = regs.get(par2);
				}
				result.set(par3, par1 > par2 ? 1 : 0);
				break;
			case "eqir": // store 1 in reg(par3) if par1 == reg(par2), 0 otherwise
			case "eqri": // store 1 in reg(par3) if reg(par1) == par2, 0 otherwise
			case "eqrr": // store 1 in reg(par3) if reg(par1) == reg(par2), 0 otherwise
				if (opcode.charAt(2) == 'r') { // retrieve reg a
					par1 = regs.get(par1);
				}
				if (opcode.charAt(3) == 'r') { // retrieve reg b
					par2 = regs.get(par2);
				}
				result.set(par3, par1 == par2 ? 1 : 0);
				break;
			default:
				throw new IllegalArgumentException("Unknown opcode :" + opcode);
		}
		return result;
	}

	public WristMem runProgram(int a, int b, int c, int d) {
		return runProgram(a, b, c, d, 0, 0);
	}
	public WristMem runProgram(int a, int b, int c, int d, int e, int f) {
		registers = new WristMem(a, b, c, d, e, f);
		instructionPointer = 0;
		while (instructionPointer >=0 && instructionPointer < program.size()) {
			WristMem parts = program.get(instructionPointer);
			if (boundRegister >= 0) {
				registers.set(boundRegister, instructionPointer);
			}
			registers = execute(registers, opcodes.get(parts.a), parts.b, parts.c, parts.d);
			if (boundRegister >= 0) {
				instructionPointer = registers.get(boundRegister);
			}
			instructionPointer++;
		}
		return registers;
	}

	// Produces a program listing, with the numeric opcodes translated to String ones
	public List<String> programToStringOpcodes() {
		List<String> result = new ArrayList<>();
		if (boundRegister >= 0) {
			result.add("#ip " + boundRegister);
		}
		for (WristMem parts : program) {
			result.add(opcodes.get(parts.a) + " " + parts.b + " " + parts.c + " " + parts.d);
		}
		return result;
	}

	/**
	 * <p>Produces a program listing that looks more like actual statements in a higher language,
	 * using the following rules:</p>
	 * <ol>
	 * <li>Any register <b>r</b> is shown as <b>[r]</b> rather than replaced with variable names (for now);</li>
	 * <li>Instructions are in principle translated into <b>[r] = ARG1 &otimes; ARG2</b>,
	 *     where &otimes; is <b>+</b>, <b>*</b>, <b>&</b>, <b>|</b>, <b>&lt;</b>, <b>&gt;</b>, or <b>==</b>,
	 *     and ARG is either <b>[p]</b> or a literal value <b>q</b></li>
	 * <li>The instruction <b>[r] = [r] &oplus; ARG2</b> (where &oplus; is <b>+</b>, <b>*</b>, <b>&</b>, or <b>|</b>) is collapsed into
	 *     <b>[r] &oplus;= ARG2</b>, eg. <b>[3] += 7</b></li>
	 * <li>&gt;, &lt;, and == are NOT translated into "if (ARG1 OP ARG2) {[r] = 1} else {[r] = 0}"
	 *     but written as "[r] = ARG1 OP ARG2 ? 1 : 0" instead, for readability's sake</li>
	 * <li>This method replaces assignments to the bound register with <b>JMP</b> instructions, and
	 *    other references to the bound register (in expressions) with the line number</li>
	 * <li>Any resulting <b>JMP</b> beyond the beginning or end of the program is replaced with <b>HALT</b></li>
	 * <li>Any resulting <b>JMP &lt;this linenumber&gt;</b> is replaced with <b>NOP</b></li>
	 * </ol>
	 *
	 * @return List&lt;String&gt; containing a simplified, human-readable, version of the program
	 */
	public List<String> programToHumanReadable() {
		List<String> result = new ArrayList<>();
		if (boundRegister >= 0) {
			// #ip 5 means "prior to instruction i, [5] = i. After each instruction, go to instruction [5] + 1"
			result.add("# prior to instruction i, [" + boundRegister + "] = i. After each instruction, jump to instruction [" + boundRegister + "] + 1");
			// Some consequences of binding register 5 to the ip
			// [5] = N  means 'JMP N+1'
			// [5] += M means 'JMP linenumber + M + 1'
			// [5] = <expr> means 'JMP <expr>+1'
			// furthermore, every occurrence of [5] in an expression can be replaced by the line number of the instruction
			// [3] = [5] means [3] = linenumber
			// [3] = [5] * 7 means [3] = 7 * linenumber
			// etc.
			// In particular, [5] = [5] & [5] and [5] = [5] | [5] are obfuscated NOP instructions, and so is [5] = N for N == linenumber
		}
		int digits = (int) Math.floor(Math.log10(program.size())) + 1;
		int lineNum = 0;
		for (WristMem parts : program) {
			String translation = "";
			String t = "[" + parts.d + "]";
			String operator = "";
			boolean secondReg = false;
			boolean suppressFirstArg = false;
			switch (opcodes.get(parts.a)) {
				case "addr": // add reg(par1) to reg(par2) store in reg(par3)
					if (parts.c == parts.d) { // swap b and c
						parts.b = parts.b + parts.c;
						parts.c = parts.b - parts.c;
						parts.b = parts.b - parts.c;
					}
					secondReg = true;
				case "addi": // add reg(par1) to par2 store in reg(par3)
					if (parts.b == parts.d) { // +=
						operator = " += ";
						suppressFirstArg = true;
					} else {
						operator = " + ";
					}
					break;
				case "mulr": // multiply reg(par1) by reg(par2) store in reg(par3)
					if (parts.c == parts.d) { // swap b and c
						parts.b = parts.b + parts.c;
						parts.c = parts.b - parts.c;
						parts.b = parts.b - parts.c;
					}
					secondReg = true;
				case "muli": // multiply reg(par1) by par2 store in reg(par3)
					if (parts.b == parts.d) { // +=
						operator = " *= ";
						suppressFirstArg = true;
					} else {
						operator = " * ";
					}
					break;
				case "banr": // bitwise and reg(par1) with reg(par2) store in reg(par3)
					if (parts.c == parts.d) { // swap b and c
						parts.b = parts.b + parts.c;
						parts.c = parts.b - parts.c;
						parts.b = parts.b - parts.c;
					}
					secondReg = true;
				case "bani": // bitwise and reg(par1) with par2 store in reg(par3)
					if (parts.b == parts.d) { // +=
						operator = " &= ";
						suppressFirstArg = true;
					} else {
						operator = " & ";
					}
					break;
				case "borr": // bitwise or reg(par1) with reg(par2) store in reg(par3)
					if (parts.c == parts.d) { // swap b and c
						parts.b = parts.b + parts.c;
						parts.c = parts.b - parts.c;
						parts.b = parts.b - parts.c;
					}
					secondReg = true;
				case "bori": // bitwise or reg(par1) with par2 store in reg(par3)
					if (parts.b == parts.d) { // +=
						operator = " |= ";
						suppressFirstArg = true;
					} else {
						operator = " | ";
					}
					break;
				case "setr": // store reg(par1) in reg(par3)
					if (parts.d == boundRegister) {
						if (parts.b == boundRegister) {
							translation = "NOP";
						} else {
							translation = "JMP [" + parts.b + "]";
						}
					} else {
						if (parts.b == boundRegister) {
							translation = t + " = " + lineNum;
						} else {
							translation = t + " = [" + parts.b + "]";
						}
					}
					break;
				case "seti": // store par1 in reg(par3)
					if (parts.d == boundRegister) {
						if (parts.b == lineNum) { // effective NOP, make explicit
							translation = "NOP";
						} else {
							translation = "JMP " + (parts.b + 1);
						}
					} else {
						translation = t + " = " + parts.b;
					}
					break;
				case "gtir": // store 1 in reg(par3) if reg(par2) < par1, 0 otherwise
					// swap par1 and par2 so it becomes "ltri" -> store 1 in reg(par3) if reg(par1) < par2, 0 otherwise
					parts.b = parts.b + parts.c;
					parts.c = parts.b - parts.c;
					parts.b = parts.b - parts.c;
					operator = " < ";
					break;
				case "gtrr": // store 1 in reg(par3) if reg(par1) > reg(par2), 0 otherwise
					secondReg = true;
				case "gtri": // store 1 in reg(par3) if reg(par1) > par2, 0 otherwise
					operator = " > ";
					break;
				case "eqrr": // store 1 in reg(par3) if reg(par1) == reg(par2), 0 otherwise
					secondReg = true;
					operator = " == ";
					break;
				case "eqir": // store 1 in reg(par3) if par1 == reg(par2), 0 otherwise
					parts.b = parts.b + parts.c;
					parts.c = parts.b - parts.c;
					parts.b = parts.b - parts.c;
				case "eqri": // store 1 in reg(par3) if reg(par1) == par2, 0 otherwise
					operator = " == ";
					break;
				default:
					translation = "# unrecognised instruction '" + opcodes.get(parts.a) + " " + parts.b + " " + parts.c + " " + parts.d + "'";
			}
			if ("".equals(translation)) {
				if (parts.d == boundRegister) {
					if (" == ".equals(operator) || " > ".equals(operator) || " < ".equals(operator)) {
						translation = "JMP " + (lineNum+2) + " if [" + parts.b + "]" + operator + (secondReg ? "[" : "") + parts.c + (secondReg ? "]" : "");
					} else if (" += ".equals(operator) && !secondReg) { // [ipreg] += const -> absolute jump
						translation = "JMP " + (lineNum + 1 + parts.c);
					} else if (" += ".equals(operator)) { // [ipreg] += [reg] -> special case, we move the +1 into the JMP number
						translation = "JMP " + (lineNum + 1) + " + [" + parts.c + "]";
					} else if (operator.length() == 4) { // one of *=, &=, |= -> weirdly calculated jump
						if (parts.c == boundRegister) { // obfuscated absolute jump / NOP
							if ("*".equals(operator.substring(1,2))) {
								int target = lineNum * lineNum + 1;
								if (target > program.size()) {
									translation = "HALT";
								} else {
									translation = "JMP " + (lineNum * lineNum + 1);
								}
							} else {
								translation = "NOP"; // never even realised this before!
							}
						} else {
							translation = "JMP " + lineNum + operator.substring(0, 2) + " " + (secondReg ? "[" : "") + parts.c + (secondReg ? "]" : "") + " + 1";
						}
					} else {
						translation = "JMP [" + parts.b + "]" + operator + (secondReg ? "[" : "") + parts.c + (secondReg ? "]" : "");
					}
				} else {
					// note that if the second arg is a register and that register is also bound to the ip, we actually
					// have an obfuscated literal value: the line number.
					translation = t + (suppressFirstArg ? "" : " = [" + parts.b + "]") + operator + (secondReg ? ((parts.c == boundRegister) ? lineNum : "[" + parts.c + "]") : parts.c);
				}
			}
			result.add(String.format("%0" + digits + "d: ", lineNum) + translation);
			lineNum++;
		}
		return result;
	}

	/**
	 * <p>First produces a program listing that looks more like actual statements in a higher language, via
	 * {@link #programToHumanReadable()}, then optimizes it further by collapsing expressions and eliminating useless
	 * statements. Additional rules used:</p>
	 *
	 * @return List&lt;String&gt; with an optimized, human readable, version of the program
	 */
	public List<String> programToOptimized() {
		List<String> result = programToHumanReadable();
		return result;
	}
}
