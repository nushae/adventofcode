package com.nushae.aoc.aoc2018.domain;

import com.nushae.aoc.domain.SearchNode;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

public class GridLocation implements SearchNode {

	private static final Map<String, GridLocation> nodes = new HashMap<>();
	private static String impassable;
	private static char[][] grid;

	public final int x;
	public final int y;
	private SearchNode parent;

	public GridLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public static void reInit(char[][] gridRef, String impassableChars) {
		nodes.clear();
		grid = gridRef;
		impassable = impassableChars;
	}

	@Override
	public Map<SearchNode, Long> getAdjacentNodes() {
		Map<SearchNode, Long> result = new HashMap<>();
		for (Point dir : ORTHO_DIRS) {
			int newX = x + dir.x;
			int newY = y + dir.y;
			if (newX >= 0 && newX < grid.length && newY >= 0 && newY <= grid[0].length && impassable.indexOf(grid[newX][newY]) < 0) {
				result.put(nodes.computeIfAbsent(newX + "," + newY, k -> new GridLocation(newX, newY)), 1L);
			}
		}
		return result;
	}

	@Override
	public String getState() {
		return x + "," + y;
	}

	@Override
	public void setParent(SearchNode parent) {
		this.parent = parent;
	}

	@Override
	public SearchNode getParent() {
		return parent;
	}
}
