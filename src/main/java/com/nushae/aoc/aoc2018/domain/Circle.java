package com.nushae.aoc.aoc2018.domain;

import java.util.ArrayDeque;

public class Circle<T> extends ArrayDeque<T> {
	public void rotate(int amount) {
		if (amount == 0) {
			return;
		}
		if (amount > 0) {
			for (int i = 0; i < amount; i++) {
				T t = this.removeLast();
				this.addFirst(t);
			}
		} else {
			for (int i = 0; i < -amount - 1; i++) {
				T t = this.pop();
				this.addLast(t);
			}
		}
	}
}
