package com.nushae.aoc.aoc2018.domain;

import java.util.Set;
import java.util.TreeSet;

public class FighterGroup {
	public final String name;
	public final int id;
	private final boolean defender;
	public long units;
	public long hitpoints;
	private final int initiative;
	public Set<String> immune;
	public Set<String> weak;
	public long damageAmount;
	public String damageType;

	public FighterGroup(int id, int num, boolean defender, long units, long hitpoints, int initiative, long dmgAmopunt, String dmgType) {
		this.id = id;
		this.name = (defender ? "Immune System" : "Infection") + " Group " + num;
		this.defender = defender;
		this.units = units;
		this.hitpoints = hitpoints;
		this.initiative = initiative;
		this.damageAmount = dmgAmopunt;
		this.damageType = dmgType;
		immune = new TreeSet<>();
		weak = new TreeSet<>();
	}

	public void addImmunity(String type) {
		immune.add(type);
	}

	public void addWeakness(String type) {
		weak.add(type);
	}

	public boolean isDefender() {
		return defender;
	}

	public long effPow() {
		return damageAmount * units;
	}

	public int ini() {
		return initiative;
	}

	public long effDmgReceived(long actualDmg, String type) {
		if (immune.contains(type)) {
			return 0;
		} else if (weak.contains(type)) {
			return actualDmg * 2;
		}
		return actualDmg;
	}

	public long receiveDmg(long actualDmg, String type) {
		long dmg = effDmgReceived(actualDmg, type);
		long unitsLost = Math.min(units, dmg / hitpoints);
		units -= unitsLost;
		return unitsLost;
	}

	public boolean isBetterTarget(FighterGroup other) {
		return effPow() > other.effPow() || effPow() == other.effPow() && initiative > other.initiative;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (!immune.isEmpty()) {
			sb.append("immune to");
			for (String tp : immune) {
				sb.append(" ").append(tp).append(",");
			}
			sb.delete(sb.length() - 1, sb.length());
		}
		if (!weak.isEmpty()) {
			if (sb.length() > 0) {
				sb.append("; ");
			}
			sb.append("weak to");
			for (String tp : weak) {
				sb.append(" ").append(tp).append(",");
			}
			sb.delete(sb.length() - 1, sb.length());
		}
		if (sb.length() > 0) {
			sb.insert(0, " (");
			sb.append(")");
		}
		return String.format("%d units each with %d hit points%s with an attack that does %d %s damage at initiative %d", units, hitpoints, sb, damageAmount, damageType, initiative);
	}
}
