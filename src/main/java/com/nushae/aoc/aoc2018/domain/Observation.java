package com.nushae.aoc.aoc2018.domain;

import java.util.ArrayList;
import java.util.List;

public class Observation {
	private int worstMinute;
	private int worstAmount;
	private int totalAsleep;
	private final int[] timesAsleep;
	private final List<Shift> shifts;

	public Observation() {
		timesAsleep = new int[60];
		shifts = new ArrayList<>();
		totalAsleep = 0;
		worstMinute = -1;
		worstAmount = -1;
	}

	public void addShift(Shift s) {
		shifts.add(s);
		for (int i = 0; i < 60; i++) {
			if (s.asleepAt(i)) {
				timesAsleep[i]++;
				if (timesAsleep[i] > worstAmount) {
					worstAmount = timesAsleep[i];
					worstMinute = i;
				}
				totalAsleep++;
			}
		}
	}

	public int sleepiestMinute() {
		return worstMinute;
	}

	public int mostFrequentAsleep() {
		return worstAmount;
	}

	public int timesAsleepAt(int minute) {
		if (minute < 0 || minute > 59) {
			return 0;
		}
		return timesAsleep[minute];
	}

	public int getTotalAsleep() {
		return totalAsleep;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Shift s : shifts) {
			result.append(String.format("%02d-%02d       %s%n", s.getMonth(), s.getDay(), s));
		}
		result.append(String.format("Total: %-4d ", totalAsleep));
		for (int i = 0; i < worstMinute; i++) {
			result.append(" ");
		}
		result.append("^\n");
		return result.toString();
	}
}
