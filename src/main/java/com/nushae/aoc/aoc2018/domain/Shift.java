package com.nushae.aoc.aoc2018.domain;

import lombok.Getter;

public class Shift {
	@Getter
	private final int month;
	@Getter
	private final int day;
	boolean[] asleep;

	public Shift(int year, int month, int day, int hour, int minute) {
		if (hour == 23) {
			minute = 0;
		}
		this.day = day;
		this.month = month;
		asleep = new boolean[60];
	}

	public void fallAsleep(int hour, int minute) {
		if (hour == 23) {
			minute = 0;
		}
		if (!asleep[minute]) {
			for (int i = minute; i < 60; i++) {
				asleep[i] = true;
			}
		}
	}

	public void wakeUp(int hour, int minute) {
		if (hour == 23) {
			minute = 0;
		}
		if (asleep[minute]) {
			for (int i = minute; i < 60; i++) {
				asleep[i] = false;
			}
		}
	}

	public boolean asleepAt(int minute) {
		if (minute < 0 || minute > 59) {
			return false;
		} else {
			return (asleep[minute]);
		}
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < 60; i++) {
			output.append(asleep[i] ? "#" : ".");
		}
		return output.toString();
	}
}
