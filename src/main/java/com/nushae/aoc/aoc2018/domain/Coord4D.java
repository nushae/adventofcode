package com.nushae.aoc.aoc2018.domain;

import java.util.HashSet;
import java.util.Set;

public class Coord4D {
	public final int x;
	public final int y;
	public final int z;
	public final int t;
	public Set<Coord4D> neighbours;

	public Coord4D(int x, int y, int z, int t) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.t = t;
		this.neighbours = new HashSet<>();
	}

	public void addNeighbour(Coord4D other) {
		neighbours.add(other);
	}

	public int manhattan(Coord4D other) {
		return Math.abs(x - other.x) + Math.abs(y - other.y) + Math.abs(z - other.z) + Math.abs(t - other.t);
	}

	/**
	 * Return the set of nodes that are in the extent of this subgraph
	 *
	 * @return extent of this subgraph
	 */
	public Set<Coord4D> extent() {
		Set<Coord4D> result = new HashSet<>();
		reachMore(result);
		return result;
	}

	private void reachMore(Set<Coord4D> alreadyReached) {
		alreadyReached.add(this);
		for (Coord4D adjacent : neighbours) {
			if (!alreadyReached.contains(adjacent)) {
				adjacent.reachMore(alreadyReached);
			}
		}
	}

	@Override
	public String toString() {
		return "[" + x + "," + y + "," + z + "," + t + "]";
	}
}
