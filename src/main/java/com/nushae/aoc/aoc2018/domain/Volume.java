package com.nushae.aoc.aoc2018.domain;

import com.nushae.aoc.domain.Coord3D;

import java.util.HashSet;
import java.util.Set;

public class Volume {
	private long value;
	public final Coord3D botSE;
	public final Coord3D topNW;

	public Volume(Coord3D topNW, Coord3D botSE) {
		this.topNW = topNW;
		this.botSE = botSE;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public long getValue() {
		return value;
	}

	/**
	 * Return the volume of this volume...
	 *
	 * @return well you know
	 */
	public long getSize() {
		return (topNW.x - botSE.x + 1) *
				(topNW.y - botSE.y + 1) *
				(topNW.z - botSE.z + 1);
	}

	public long distanceToCenter() {
		return new Coord3D(topNW.x - botSE.x, topNW.y - botSE.y, topNW.z - botSE.z).manhattan(new Coord3D(0, 0, 0));
	}

	/**
	 * Split this volume into (up to) 8 subvolumes
	 *
	 * @return set of subvolumes
	 */
	public Set<Volume> split() {
		Set<Volume> result = new HashSet<>();
		Coord3D center = new Coord3D((topNW.x + botSE.x) / 2, (topNW.y + botSE.y) / 2, (topNW.z + botSE.z) / 2);
		for (int i = 0; i < 2; i++) {
			if (i == 0 || topNW.x != botSE.x) {
				for (int j = 0; j < 2; j++) {
					if (j == 0 || topNW.y != botSE.y) {
						for (int k = 0; k < 2; k++) {
							if (k == 0 || topNW.z != botSE.z) {
								Coord3D tpNW = new Coord3D(i == 0 ? center.x : topNW.x, j == 0 ? center.y : topNW.y, k == 0 ? center.z : topNW.z);
								Coord3D btSE = new Coord3D(i == 0 ? botSE.x : center.x + 1, j == 0 ? botSE.y : center.y + 1, k == 0 ? botSE.z : center.z + 1);
								result.add(new Volume(tpNW, btSE));
							}
						}
					}
				}
			}
		}
		return result;
	}
}
