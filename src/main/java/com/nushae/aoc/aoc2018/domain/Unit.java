package com.nushae.aoc.aoc2018.domain;

import lombok.Getter;
import lombok.Setter;

public class Unit {
	@Getter
	public int x;
	@Getter
	public int y;
	public final boolean isElf;
	@Getter
	@Setter
	private int hitPoints;
	@Getter
	private final int attackPower;

	public Unit(int x, int y, boolean isElf) {
		this(x, y, isElf, 3);
	}

	public Unit(int x, int y, boolean isElf, int attackPower) {
		this.x = x;
		this.y = y;
		this.isElf = isElf;
		this.hitPoints = 200;
		this.attackPower = attackPower;
	}

	@Override
	public int hashCode() {
		return (x + "," + y).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Unit other = (Unit) obj;
		return x == other.x && y == other.y;
	}

	@Override
	public String toString() {
		return (isElf ? "Elf" : "Goblin") + " at (" + x + "," + y + ")";
	}
}
