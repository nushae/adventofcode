package com.nushae.aoc.aoc2018.domain;

public class LightPoint {
	public int x;
	public int y;
	private final int vx;
	private final int vy;

	public LightPoint(int x, int y, int vx, int vy) {
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
	}

	public void move(int times) {
		x += times * vx;
		y += times * vy;
	}
}
