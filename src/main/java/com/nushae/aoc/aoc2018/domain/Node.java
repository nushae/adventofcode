package com.nushae.aoc.aoc2018.domain;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private final List<Node> children;
	private final List<Integer> metadata;

	public Node() {
		children = new ArrayList<>();
		metadata = new ArrayList<>();
	}

	public void addChild(Node n) {
		children.add(n);
	}

	public void addMeta(int i) {
		metadata.add(i);
	}

	public int metaSum() {
		int result = 0;
		for (int i : metadata) {
			result += i;
		}
		for (Node n : children) {
			result += n.metaSum();
		}
		return result;
	}

	public long value() {
		long result = 0;
		if (children.size() > 0) {
			for (int m : metadata) {
				if (m > 0 && m <= children.size()) {
					result += children.get(m - 1).value();
				}
			}
		} else {
			result = metaSum();
		}
		return result;
	}
}
