package com.nushae.aoc.aoc2018.domain;

import java.util.ArrayList;
import java.util.List;

public class Step {
	public String name;
	private List<Step> requires;
	private boolean isExecuted;

	public Step(String name) {
		requires = new ArrayList<>();
		this.name = name;
		isExecuted = false;
	}

	public void addRequirement(Step other) {
		requires.add(other);
	}

	public void execute() {
		if (isReady()) {
			isExecuted = true;
		}
	}

	public boolean isReady() {
		boolean ready = true;
		for (Step required : requires) {
			ready = ready && required.isExecuted();
		}
		return ready;
	}

	public boolean isExecuted() {
		return isExecuted;
	}
}
