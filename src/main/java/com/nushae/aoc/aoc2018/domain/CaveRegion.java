package com.nushae.aoc.aoc2018.domain;

import com.nushae.aoc.aoc2018.Exercise22;
import com.nushae.aoc.domain.SearchNode;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

public class CaveRegion implements SearchNode {

	// cache the created nodes to avoid creating them again, which breaks the search
	private static final Map<String, CaveRegion> nodes = new HashMap<>();

	private SearchNode parent;
	private final Point location;
	final int targetX;
	final int targetY;
	private final int currentTool;
	Map<Point, Integer> types;

	public CaveRegion(int x, int y, int targetX, int targetY, int tool, Map<Point, Integer> types) {
		location = new Point(x, y);
		currentTool = tool;
		this.types = types;
		this.targetX = targetX;
		this.targetY = targetY;
	}

	public Point getLocation() {
		return location;
	}

	@Override
	public Map<SearchNode, Long> getAdjacentNodes() {
		Map<SearchNode, Long> result = new HashMap<>();
		for (Point dir : ORTHO_DIRS) { // at most 4
			final int newX = location.x + dir.x;
			final int newY = location.y + dir.y;
			if (newX >= 0 && newY >= 0 && newX < targetX * 3 && newY < targetY * 3) { // upper bounds copied from doPart2, see there
				long costToReach = 1L; // base cost for moving
				int tool = currentTool;
				// do not swap tools for adjacent cells with same type. EVER.
				//
				// terrain type is predefined as: rocky = 0, wet = 1, narrow = 2
				// We numbered the tools cleverly so we can do a trick:
				// from/to types   required tool
				//   .   =   |       N   T   C
				// +---+---+---+   +---+---+---+
				// |   | 1 | 2 |   | 0 |   |   |   new tool = 3 - fromType - toType
				// +---+---+---+   +---+---+---+
				// | 0 |   | 2 |   |   | 1 |   |   new tool = 3 - fromType - toType
				// +---+---+---+   +---+---+---+
				// | 0 | 1 |  |   |   |   | 2 |   new tool = 3 - fromType - toType
				// +---+---+---+   +---+---+---+
				int fromType = types.get(location);
				int toType = types.get(new Point(newX, newY));
				if (fromType != toType) {
					tool = 3 - fromType - toType;
					if (tool != currentTool) {
						costToReach += 7L; // added cost for swapping
					}
				}
				if (newX == targetX && newY == targetY && tool != Exercise22.TOOL_TORCH) {
					tool = Exercise22.TOOL_TORCH; // switch to torch at end
					costToReach += 7L; // added cost for switch to torch at end
				}
				String ref = newX + "," + newY + "/" + tool;
				int finalTool = tool;
				nodes.computeIfAbsent(ref, k -> new CaveRegion(newX, newY, targetX, targetY, finalTool, types));
				result.put(nodes.get(ref), costToReach);
			}
		}
		return result; // at most 4
	}

	@Override
	public String getState() {
		return location.x + "," + location.y + "/" + currentTool;
	}

	@Override
	public void setParent(SearchNode parent) {
		this.parent = parent;
	}

	@Override
	public SearchNode getParent() {
		return parent;
	}

	@Override
	public int hashCode() {
		return getState().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		CaveRegion other = (CaveRegion) obj;

		return getState().equals(other.getState());
	}
}
