package com.nushae.aoc.aoc2018.domain;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Routexp {
	private String literal = null;
	private List<Routexp> parts;
	private boolean serial; // if true: a++b++c etc; if false: a|b|c etc (never both at once)

	public Routexp(String literal) {
		this.literal = literal;
	}

	public Routexp(boolean serial, List<Routexp> parts) {
		this.serial = serial;
		this.parts = parts;
	}

	public boolean isLiteral() {
		return null != literal;
	}

	public boolean isSerial() {
		return literal == null && serial;
	}

	public boolean isParallel() {
		return literal == null && !serial;
	}

	public Set<Point> apply(Set<Point> currentLocs, Map<Point, Character> mapAccumulator) {
		Set<Point> result = new HashSet<>();
		if (isLiteral()) {
			for (Point currentLoc : currentLocs) {
				for (int i = 0; i < literal.length(); i++) {
					switch (literal.charAt(i)) {
						case 'N':
							currentLoc = new Point(currentLoc.x, currentLoc.y - 1);
							mapAccumulator.put(currentLoc, '-');
							currentLoc = new Point(currentLoc.x, currentLoc.y - 1);
							if (!mapAccumulator.containsKey(currentLoc)) {
								mapAccumulator.put(currentLoc, '.');
							}
							break;
						case 'E':
							currentLoc = new Point(currentLoc.x + 1, currentLoc.y);
							mapAccumulator.put(currentLoc, '|');
							currentLoc = new Point(currentLoc.x + 1, currentLoc.y);
							if (!mapAccumulator.containsKey(currentLoc)) {
								mapAccumulator.put(currentLoc, '.');
							}
							break;
						case 'S':
							currentLoc = new Point(currentLoc.x, currentLoc.y + 1);
							mapAccumulator.put(currentLoc, '-');
							currentLoc = new Point(currentLoc.x, currentLoc.y + 1);
							if (!mapAccumulator.containsKey(currentLoc)) {
								mapAccumulator.put(currentLoc, '.');
							}
							break;
						case 'W':
							currentLoc = new Point(currentLoc.x - 1, currentLoc.y);
							mapAccumulator.put(currentLoc, '|');
							currentLoc = new Point(currentLoc.x - 1, currentLoc.y);
							if (!mapAccumulator.containsKey(currentLoc)) {
								mapAccumulator.put(currentLoc, '.');
							}
							break;
					}
				}
				result.add(currentLoc);
			}
		} else if (isParallel()) {
			for (Routexp part : parts) {
				result.addAll(part.apply(currentLocs, mapAccumulator));
			}
		} else {
			result.addAll(currentLocs);
			for (Routexp part : parts) {
				result = part.apply(result, mapAccumulator);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		if (isLiteral()) {
			if (literal.isEmpty()) {
				return "<EMPTY>";
			}
			return literal;
		}
		StringBuilder partList = new StringBuilder();
		if (isSerial()) {
			for (Routexp part : parts) {
				if (partList.length() == 0) {
					partList.append("[");
				} else {
					partList.append("+");
				}
				partList.append(part.toString());
			}
			partList.append("]");
		} else { // parallel
			for (Routexp part : parts) {
				if (partList.length() == 0) {
					partList.append("{");
				} else {
					partList.append("|");
				}
				partList.append(part.toString());
			}
			partList.append("}");
		}
		return partList.toString();
	}
}
