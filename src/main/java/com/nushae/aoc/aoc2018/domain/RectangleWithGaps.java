package com.nushae.aoc.aoc2018.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.ArrayList;

@Log4j2
public class RectangleWithGaps {

	@Getter
	@Setter
	private Rectangle extent;
	private java.util.List<RectangleWithGaps> gaps;

	public RectangleWithGaps(Rectangle extent) {
		gaps = new ArrayList<>();
		this.extent = extent;
	}

	public void subtract(Rectangle other) {
		Rectangle overlap = extent.intersection(other);
		if (!overlap.isEmpty()) {
			for (RectangleWithGaps gap : gaps) {
				gap.subtract(overlap);
			}
			gaps.add(new RectangleWithGaps(overlap));
		}
	}

	public int area() {
		int result = extent.width * extent.height;
		for (RectangleWithGaps gap : gaps) {
			result -= gap.area();
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(extent.toString());
		return result.toString();
	}
}
