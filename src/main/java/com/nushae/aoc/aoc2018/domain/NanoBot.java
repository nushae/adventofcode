package com.nushae.aoc.aoc2018.domain;

import com.nushae.aoc.domain.Coord3D;

public class NanoBot {
	public final Coord3D pos;
	public final int range;

	public NanoBot(int x, int y, int z, int range) {
		pos = new Coord3D(x, y, z);
		this.range = range;
	}

	/**
	 * Does the passed Coordinate3D fall within the range of this bot?
	 *
	 * @param otherPos Coord3D to test
	 * @return true iff otherPos falls within this bots range
	 */
	public boolean inRange(Coord3D otherPos) {
		return range >= pos.manhattan(otherPos);
	}

	/**
	 * Does the cuboid with opposing corners topNW and bottomSE (partially) overlap with the area in range of this bot?
	 *
	 * @param topNW    top-north-west corner of the cuboid
	 * @param bottomSE bottom-south-east corner of the cuboid
	 * @return true iff any point in the cuboid falls within the range of this bot
	 */
	public boolean rangeOverlaps(Coord3D topNW, Coord3D bottomSE) {
		// the range of the bot defines a 'manhattan distance sphere' so we do cuboid-sphere collision:
		// 1) find line between centers of cuboid and sphere
		// 2) find point on box surface that lies on this line -> this is the point of the cuboid closest to the bot
		// 3) check if this point falls in range of bot, if not -> no collision
		return inRange(new Coord3D(
				pos.x <= bottomSE.x ? bottomSE.x : Math.min(pos.x, topNW.x),
				pos.y <= bottomSE.y ? bottomSE.y : Math.min(pos.y, topNW.y),
				pos.z <= bottomSE.z ? bottomSE.z : Math.min(pos.z, topNW.z)));
	}
}
