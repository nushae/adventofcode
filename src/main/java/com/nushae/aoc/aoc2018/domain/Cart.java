package com.nushae.aoc.aoc2018.domain;

import java.awt.*;

import static com.nushae.aoc.util.MathUtil.*;

public class Cart {
	public int x;
	public int y;
	private boolean crashed = false;
	public Point dir;
	private int nextDir = 0; // left, straight, right = 0, 1, 2

	public Cart(int x, int y, int dx, int dy) {
		this.x = x;
		this.y = y;
		this.dir = new Point(dx, dy);
	}

	public void crash() {
		crashed = true;
	}

	public boolean isCrashed() {
		return crashed;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void move(char[][] grid) {
		x = x + dir.x;
		y = y + dir.y;
		switch (grid[x][y]) {
			case '-': // W - E
			case '|': // N - S
				// NOP; continue as is
				break;
			case '7': // W - S:
			case '\'': // N - E: in these cases reflect dir off x = y (aka dx = dy, dy = dx)
				dir = refYX(dir);
				break;
			case 'J': // W - N:
			case ',': // E - S: in these cases reflect off x = -y (aka dx = -dy, dy = -dx)
				dir = refYnegX(dir);
				break;
			case '+': // crossing: turn according to nextDir
				dir = rot90L(1, dir);
				dir = rot90R(nextDir, dir);
				nextDir = (nextDir + 1) % 3;
				break;
		}
	}
}
