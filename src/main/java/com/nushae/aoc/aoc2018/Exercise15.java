package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.GridLocation;
import com.nushae.aoc.aoc2018.domain.Unit;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.SearchNode;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS_LEXICOGRAPHIC;
import static com.nushae.aoc.util.SearchUtil.dsp;

@Log4j2
@Aoc(year = 2018, day = 15, title="Beverage Bandits", keywords = {"grid", "game", "shortest-path"})
public class Exercise15 {

	private static final boolean REPORT = false;

	List<String> lines;
	char[][] grid;
	int width;
	int height;
	int elfUnits;
	int goblinUnits;
	Map<Point, Unit> units;
	List<Unit> allElves;
	List<Unit> allGoblins;

	public Exercise15() {
		lines = new ArrayList<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput(int elfBoost) {
		width = lines.get(0).length();
		height = lines.size();
		grid = new char[width][height];
		units = new HashMap<>();
		elfUnits = 0;
		goblinUnits = 0;
		allElves = new ArrayList<>();
		allGoblins = new ArrayList<>();
		for (int i=0; i< lines.size(); i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				grid[j][i] = line.charAt(j);
				if (grid[j][i] == 'E') { // elf unit
					Unit unit = new Unit(j, i, true, 3 + elfBoost);
					units.put(new Point(j, i), unit);
					allElves.add(unit);
					elfUnits++;
				} else if (grid[j][i] == 'G') { // goblin unit
					Unit unit = new Unit(j, i, false);
					units.put(new Point(j, i), unit);
					allGoblins.add(unit);
					goblinUnits++;
				}
			}
		}
		if (REPORT) {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					System.out.print(grid[x][y]);
				}
				System.out.println();
			}
			System.out.println(elfUnits + " elf units.");
			System.out.println(goblinUnits + " goblin units.");
			for (Point p : units.keySet().stream().sorted(Comparator.comparingDouble(Point::getY).thenComparingDouble(Point::getX)).collect(Collectors.toList())) {
				System.out.println(units.get(p));
			}
		}
	}

	public long doPart1() {
		int rounds = 0;
		while (elfUnits > 0 && goblinUnits > 0) {
			if (doFightRound()) {
				rounds++;
			}
		}
		long result = 0;
		for (Unit u : units.values()) {
			result += u.getHitPoints();
		}
		return rounds * result;
	}

	public long doPart2() {
		int startingElves;
		int rounds;
		int lowerBound = 4;
		int upperBound = 5;
		int elfBoost;
		do {
			log.debug("Upper bound: " + upperBound);
			processInput(upperBound);
			startingElves = elfUnits;
			rounds = 0;
			while (elfUnits > 0 && goblinUnits > 0) {
				if (doFightRound()) {
					rounds++;
				}
			}
			if (startingElves != elfUnits) { // increase
				upperBound = upperBound * 2;
			}
		} while (startingElves != elfUnits);
		while (lowerBound < upperBound) {
			elfBoost = (lowerBound + upperBound) / 2;
			log.debug("Trying elf boost: " + elfBoost);
			processInput(elfBoost);
			startingElves = elfUnits;
			rounds = 0;
			while (elfUnits > 0 && goblinUnits > 0) {
				if (doFightRound()) {
					rounds++;
				}
			}
			if (startingElves != elfUnits) { // increase
				lowerBound = elfBoost + 1;
			} else {
				upperBound = elfBoost;
			}
		} // lowerBound is now the correct boost, but we may have just done a run with the wrong boost
		processInput(lowerBound);
		rounds = 0;
		while (elfUnits > 0 && goblinUnits > 0) {
			if (doFightRound()) {
				rounds++;
			}
		}
		long result = 0;
		for (Unit u : units.values()) {
			result += u.getHitPoints();
		}
		log.debug(lowerBound + ": " + rounds + " * " + result + " = " + rounds * result);
		return rounds * result;
	}

	// Once again a problem that is not extremely well specified. The confusing factors here are:
	// 1) the 'reading order' tie breaking; we solve this with clever looping
	// 2) the move vs move+attack vs attack that is described rather poorly, with "unit ends its turn" in weird places.
	//    I assume what is meant is:
	//    a) move if you are not already next to an enemy (= not in range) and there is at least one reachable enemy
	//    b) then, attack if you are next to an enemy (= in range)
	// 3) Finally, there is the notion of a 'full' combat round. I took this to mean 'the enemies ran out before the
	//    last unit took an action'
	public boolean doFightRound() {
		List<Point> startingPositions = units.keySet().stream().sorted(Comparator.comparingDouble(Point::getY).thenComparingDouble(Point::getX)).collect(Collectors.toList());
		for (Point atkLoc : startingPositions) {
			if (units.containsKey(atkLoc)) { // unit still alive
				Unit attacker = units.get(atkLoc);
				log.debug("Determining actions for " + attacker);
				if ((attacker.isElf ? goblinUnits : elfUnits) == 0) {
					log.debug("No targets found, aborting combat round.");
					return false;
				}
				// see if we can skip directly to attacking
				Unit defender = null;
				for (Point dir : ORTHO_DIRS_LEXICOGRAPHIC) {
					int newX = attacker.x + dir.x;
					int newY = attacker.y + dir.y;
					if (0 <= newX && newX < width && 0 <= newY && newY < height && grid[newX][newY] == (attacker.isElf ? 'G' : 'E')) {
						if (defender == null || defender.getHitPoints() > units.get(new Point(newX, newY)).getHitPoints()) {
							defender = units.get(new Point(newX, newY));
						}
					}
				}
				if (defender == null) { // apparently not, so try to move first
					List<Unit> enemies = attacker.isElf ? allGoblins : allElves;
					Set<Point> targetLocations = new HashSet<>();
					for (Unit enemy : enemies.stream().sorted(Comparator.comparingInt(Unit::getY).thenComparingInt(Unit::getX)).collect(Collectors.toList())) {
						if (enemy.isElf != attacker.isElf) {
							for (Point q : ORTHO_DIRS_LEXICOGRAPHIC) {
								int newX = enemy.x + q.x;
								int newY = enemy.y + q.y;
								if (0 <= newX && newX < width && 0 <= newY && newY < height && grid[newX][newY] == '.') {
									log.debug("Adding possible target location: " + newX + "," + newY);
									targetLocations.add(new Point(newX, newY));
								}
							}
						}
					}
					// 1) Identify all possible targets for this unit, and for each that has reachable open adjacent spots,
					//   determine the closest of these spots. If two spots are equally close, take the first in reading order.
					//   We satisfy the 'reading order' tiebreaker (both for the first step to take and the destination spot)
					//   as follows:
					//   a) loop over the spots adjacent to the current unit in reading order (N -> W -> E -> S)
					//   b) find the shortest path to any reachable target-adjacent spot (looping over them in reading order too);
					//   c) replace current-best-choice with new shortest path ONLY if strictly shorter
					int bestPath = Integer.MAX_VALUE;
					GridLocation bestStep = null;
					for (Point dir : ORTHO_DIRS_LEXICOGRAPHIC) {
						int newX = atkLoc.x + dir.x;
						int newY = atkLoc.y + dir.y;
						if (0 <= newX && newX < width && 0 <= newY && newY < height && grid[newX][newY] == '.') {
							Point p = new Point(newX, newY);
							for (Point q : targetLocations.stream().sorted(Comparator.comparingDouble(Point::getY).thenComparingDouble(Point::getX)).collect(Collectors.toList())) {
								log.debug("Calculating shortest path from " + p + " to " + q);
								GridLocation.reInit(grid, "#EG");
								SearchNode path = dsp(new GridLocation(p.x, p.y), n -> (q.x + "," + q.y).equals(n.getState()));
								int pathLen = 0;
								SearchNode nextStep = null;
								while (path != null) {
									pathLen++;
									nextStep = path;
									path = path.getParent();
								}
								if (pathLen > 0 && pathLen < bestPath) {
									log.debug("Best length found (" + pathLen + ") is an improvement vs " + bestPath + ", so keeping this new result");
									bestPath = pathLen;
									bestStep = (GridLocation) nextStep;
								} else {
									log.debug("Best length found (" + pathLen + ") is not better than what we already had.");
								}
							}
						}
					}
					if (bestStep != null) { // execute move
						log.debug("Move " + attacker + " to " + bestStep.x + "," + bestStep.y);
						grid[atkLoc.x][atkLoc.y] = '.';
						grid[bestStep.x][bestStep.y] = attacker.isElf ? 'E' : 'G';
						attacker.x = bestStep.x;
						attacker.y = bestStep.y;
						units.remove(atkLoc);
						units.put(new Point(bestStep.x, bestStep.y), attacker);
					} else {
						log.debug("Don't move " + attacker);
					}
					// 3) now try to determine the target for our attack again
					for (Point dir : ORTHO_DIRS_LEXICOGRAPHIC) {
						int newX = attacker.x + dir.x;
						int newY = attacker.y + dir.y;
						if (0 <= newX && newX < width && 0 <= newY && newY < height && grid[newX][newY] == (attacker.isElf ? 'G' : 'E')) {
							if (defender == null || defender.getHitPoints() > units.get(new Point(newX, newY)).getHitPoints()) {
								defender = units.get(new Point(newX, newY));
							}
						}
					}
				}
				if (defender != null) { // ATTACK!!
					log.debug("Attack " + defender);
					defender.setHitPoints(defender.getHitPoints() - attacker.getAttackPower());
					if (defender.getHitPoints() < 1) {
						log.debug("Defender dies (removed from combat)");
						grid[defender.x][defender.y] = '.';
						if (defender.isElf) {
							allElves.remove(defender);
							elfUnits--;
						} else {
							allGoblins.remove(defender);
							goblinUnits--;
						}
						units.remove(new Point(defender.x, defender.y));
					}
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2018, 15));

		// Process input (3ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput(0);
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 216270 (22.221s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 59339 (131.4s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
