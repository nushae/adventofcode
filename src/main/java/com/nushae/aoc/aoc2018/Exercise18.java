package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.KING_DIRS;

@Log4j2
public class Exercise18 {

	List<String> lines;
	char[][] grid;
	int width;
	int height;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		width = lines.get(0).length();
		height = lines.size();
		grid = new char[width+2][height+2];
		for (char[] sub : grid) {
			Arrays.fill(sub, '.');
		}
		for (int y = 0; y < height; y++) {
			String line = lines.get(y);
			for (int x = 0; x < width; x++) {
				grid[x+1][y+1] = line.charAt(x);
			}
		}
	}

	public long doPart1() {
		processInput();
		for (int m = 0; m < 10; m++) {
			grid = doGeneration(grid);
		}
		return countResourceVal(grid, true);
	}

	// Brute force will take too long so (using the original bound) we look for repeat states, using total resource
	// value as a 'hash'. We ignore repeats at unequal intervals because that is not a cycle, but a 'hash collision'.
	// So our heuristic will be "several successive states must be a repeat with equal cycle length" where 'several' is
	// technically equal to the cycle length, but in practice 3 is sufficient.
	// Then the loop bound is tightened to the first gen larger than the current one where (gen-first) % cycle == (1000000000-first) % cycle.
	// Et voila speed achieved.
	public long doPart2() {
		processInput();
		List<Long> prevVals = new ArrayList<>();
		int prevDist = -1;
		int prevprevDist = -1; // technically the number of repeats must be equal to the cycle length but 3 is sufficient
		long m;
		long targetGen = 1000000000;
		for (m = 0; m < targetGen; m++) {
			grid = doGeneration(grid);
			long resourceVal = countResourceVal(grid, false);
			if (prevVals.contains(resourceVal)) {
				int location;
				for (location = prevVals.size()-1; prevVals.get(location) != resourceVal; location--);
				int dist = (prevVals.size() - location);
				System.out.println("Repeat state at gen " + (m+1) + " with distance " + dist);
				if (dist == prevDist && dist == prevprevDist) {
					System.out.println((m+1) + " is the third consecutive repeat state with a distance of " + dist);
					// ie m-2-dist was the first one; since we count from 0, we must add 1.
					System.out.println("So " + (m - 1 - dist) + " was the first cycle's first generation (" + (m - 1 + dist) + " will be the next)");
					System.out.println("(1000000000 - " + (m - 1 - dist) + ") % " + dist + " = " + (1000000000 - (m-1-dist)) % dist);
					// and the next ones will be m - 2 + N*dist + (1000000000 - (m-1-dist)) % dist, for any N >= 0
					targetGen = m - 2 + (1000000000 - (m - 1 - dist)) % dist;
					if (targetGen < m) { // if the mod value is low enough we could be past it in this cycle
						targetGen += dist;
					}
					// then continue generating new generations until gen = targetGen
					System.out.println("So target gen = " + targetGen);
					break;
				}
				prevprevDist = prevDist;
				prevDist = dist;
			}
			prevVals.add(resourceVal);
		}
		for (; m < targetGen; m++) {
			grid = doGeneration(grid);
		}
		return countResourceVal(grid, true);
	}

	public char[][] doGeneration(char[][] input) {
		char[][] newGen = new char[width+2][height+2];
		for (char[] sub : newGen) {
			Arrays.fill(sub, '.');
		}
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int wood = 0;
				int lumber = 0;
				for (Point dir : KING_DIRS) {
					switch (input[1 + x + dir.x][1 + y + dir.y]) {
						case '|' -> wood++;
						case '#' -> lumber++;
					}
				}
				if (grid[1+x][1+y] == '.') {
					if (wood >= 3) {
						newGen[1+x][1+y] = '|';
					} else {
						newGen[1+x][1+y] = '.';
					}
				}
				if (grid[1+x][1+y] == '|') {
					if (lumber >= 3) {
						newGen[1+x][1+y] = '#';
					} else {
						newGen[1+x][1+y] = '|';
					}
				}
				if (grid[1+x][1+y] == '#') {
					if (wood > 0 && lumber > 0) {
						newGen[1+x][1+y] = '#';
					} else {
						newGen[1+x][1+y] = '.';
					}
				}
			}
		}
		return newGen;
	}

	public long countResourceVal(char[][] grid, boolean print) {
		long wood = 0;
		long lumber = 0;
		for (int y=0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (print) System.out.print(grid[1+x][1+y]);
				if (grid[1+x][1+y] == '|') {
					wood++;
				} else if (grid[1+x][1+y] == '#') {
					lumber++;
				}
			}
			if (print) System.out.println();
		}
		return wood * lumber;
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2018, 18));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
