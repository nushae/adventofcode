package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.NanoBot;
import com.nushae.aoc.aoc2018.domain.Volume;
import com.nushae.aoc.domain.Coord3D;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise23 {

	private static final Pattern NANO_BOT = Pattern.compile("pos=<(-?\\d+),(-?\\d+),(-?\\d+)>, r=(\\d+)");

	List<String> lines;
	List<NanoBot> bots;
	Coord3D extentTopNW;
	Coord3D extentBottomSE;
	NanoBot maxN;
	int maxR;

	public Exercise23() {
		lines = new ArrayList<>();
	}

	public Exercise23(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise23(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		bots = new ArrayList<>();
		extentTopNW = new Coord3D(0, 0, 0);
		extentBottomSE = new Coord3D(0, 0, 0);
		maxR = -1;
		maxN = null;
		for (String line : lines) {
			Matcher m = NANO_BOT.matcher(line);
			if (m.find()) {
				int r = Integer.parseInt(m.group(4));
				NanoBot n = new NanoBot(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), r);
				if (r > maxR) {
					maxR = r;
					maxN = n;
				}
				extentTopNW = new Coord3D(Math.max(extentTopNW.x, n.pos.x), Math.max(extentTopNW.y, n.pos.y), Math.max(extentTopNW.z, n.pos.z));
				extentBottomSE = new Coord3D(Math.min(extentBottomSE.x, n.pos.x), Math.min(extentBottomSE.y, n.pos.y), Math.min(extentBottomSE.z, n.pos.z));
				bots.add(n);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		System.out.println("Data processed.");
		System.out.println(bots.size() + " bots, largest range is " + maxR + ", extent = " + extentTopNW + "-" + extentBottomSE);

	}

	public long doPart1() {
		processWithPattern();
		long result = 0;
		for (NanoBot n : bots) {
			if (maxN.inRange(n.pos)) {
				result++;
			}
		}
		return result;
	}

	public long doPart2() {
		processWithPattern();
		TreeSet<Volume> Q = new TreeSet<>(Comparator.comparingLong(Volume::getValue)
				.thenComparing(Comparator.comparingLong(Volume::getSize).reversed())
				.thenComparing(Comparator.comparingLong(Volume::distanceToCenter).reversed()));
		Volume choice = new Volume(extentTopNW, extentBottomSE);
		choice.setValue(bots.size());
		Q.add(choice);
		while ((choice = Q.pollLast()) != null) {
			System.out.println("Current choice is "  + choice.botSE + "-" + choice.topNW + " with volume " + choice.getSize() + " and value " + choice.getValue());
			if (choice.getSize() == 1) {
				break;
			}
			for (Volume v : choice.split()) {
				System.out.print("  Checking " + v.botSE + "-" + v.topNW + "... ");
				long count = countBotsOverlappingWith(v.topNW, v.botSE);
				System.out.println(count + " bots.");
				v.setValue(count);
				Q.add(v);
			}
		}
		if (choice != null) {
			return choice.topNW.manhattan(new Coord3D(0, 0, 0));
		} else {
			return -1;
		}
	}

	public long countBotsOverlappingWith(Coord3D topNW, Coord3D botSE) {
		long result = 0;
		for (NanoBot bot : bots) {
			if (bot.rangeOverlaps(topNW, botSE)) {
				result++;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(aocPath(2018, 23));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
