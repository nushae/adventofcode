package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise02 {

	ArrayList<String> lines;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long pairs = 0;
		long triples = 0;
		for (String line : lines) {
			int[] letterfreq = new int[26];
			for (int c=0; c<line.length(); c++) {
				letterfreq[line.charAt(c) - 'a']++;
			}
			boolean pair = false;
			boolean triple = false;
			for (int i=0; !(pair && triple) && i < 26; i++) {
				if (letterfreq[i] == 2) {
					pair = true;
				} else if (letterfreq[i] == 3) {
					triple = true;
				}
			}
			if (pair) {
				pairs++;
			}
			if (triple) {
				triples++;
			}
		}
		return pairs * triples;
	}

	public String doPart2() {
		for (int i = 0; i < lines.size()-1; i++) {
			for (int j = i+1; j < lines.size(); j++) {
				String left = lines.get(i);
				String right = lines.get(j);
				for (int k=0; k < Math.min(left.length(), right.length()); k++) {
					String leftcut = left.substring(0, k) + left.substring(k+1);
					if (leftcut.equals(right.substring(0, k) + right.substring(k+1))) {
						return leftcut;
					}
				}
			}
		}
		return "";
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2018, 2));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
