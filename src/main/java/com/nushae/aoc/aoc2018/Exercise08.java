package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Node;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise08 {

	ArrayList<String> lines;
	ArrayList<Integer> input;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		input = new ArrayList<>();
		for (String line : lines) {
			for (String s : line.split("\\s")) {
				input.add(Integer.parseInt(s));
			}
		}
		log.info("Read " + input.size() + " numbers.");
	}

	public long doPart1() {
		processInput();
		return parseNode().metaSum();
	}

	public long doPart2() {
		processInput();
		return parseNode().value();
	}

	public Node parseNode() {
		Node n = new Node();
		int childCount = eat();
		int metaCount = eat();
		for (int i =0; i < childCount; i++) {
			n.addChild(parseNode());
		}
		for (int i = 0; i < metaCount; i++) {
			n.addMeta(eat());
		}
		return n;
	}

	public int eat() {
		return input.remove(0);
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2018, 8));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
