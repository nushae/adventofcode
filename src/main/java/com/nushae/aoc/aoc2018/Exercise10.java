package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.LightPoint;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise10 {

	private static final Pattern LIGHT_POINT = Pattern.compile("position=<\\s*(-?\\d+),\\s*(-?\\d+)>\\svelocity=<\\s*(-?\\d+),\\s*(-?\\d+)>");

	List<String> lines;
	List<LightPoint> lightPoints;
	int minX;
	int maxX;
	int minY;
	int maxY;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		lightPoints = new ArrayList<>();
		minX = Integer.MAX_VALUE;
		minY = Integer.MAX_VALUE;
		maxX = Integer.MIN_VALUE;
		maxY = Integer.MIN_VALUE;
		for (String line : lines) {
			Matcher m = LIGHT_POINT.matcher(line);
			if (m.find()) {
				LightPoint p = new LightPoint(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)));
				minX = Math.min(p.x, minX);
				minY = Math.min(p.y, minY);
				maxX = Math.max(p.x, maxX);
				maxY = Math.max(p.y, maxY);
				lightPoints.add(p);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long getMessage() {
		processInput();
		long time = 0;
		while (true) {
			int currentSpan = Math.abs(maxY - minY);
			minX = Integer.MAX_VALUE;
			minY = Integer.MAX_VALUE;
			maxX = Integer.MIN_VALUE;
			maxY = Integer.MIN_VALUE;
			for (LightPoint p : lightPoints) {
				p.move(1);
				minX = Math.min(p.x, minX);
				minY = Math.min(p.y, minY);
				maxX = Math.max(p.x, maxX);
				maxY = Math.max(p.y, maxY);
			}
			time++;
			if (Math.abs(maxY - minY) > currentSpan) { // upflank, points moving apart again
				minX = Integer.MAX_VALUE;
				minY = Integer.MAX_VALUE;
				maxX = Integer.MIN_VALUE;
				maxY = Integer.MIN_VALUE;
				for (LightPoint p : lightPoints) {
					p.move(-1); // backup one cycle to recreate picture
					minX = Math.min(p.x, minX);
					minY = Math.min(p.y, minY);
					maxX = Math.max(p.x, maxX);
					maxY = Math.max(p.y, maxY);
				}
				time--;
				break;
			}
		}
		boolean[][] message = new boolean[maxX - minX + 1][maxY - minY + 1];
		for (LightPoint p : lightPoints) {
			message[p.x-minX][p.y-minY] = true;
		}
		for (int y = 0; y < message[0].length; y++) {
			for (boolean[] booleans : message) {
				System.out.print(booleans[y] ? "#" : ".");
			}
			System.out.println();
		}
		return time;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2018, 10));

		// part 1 & 2
		long start = System.currentTimeMillis();
		System.out.println(ex.getMessage());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
