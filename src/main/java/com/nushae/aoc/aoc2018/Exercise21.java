package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

@Log4j2
public class Exercise21 {

	// Well well well, a WristDevice puzzle where we don't actually need the WristDevice.
	// (I strongly advise against running the program on the WristDevice, NOT EVEN with the correct answer in reg0

	public static long doPart1() {
		return runProgramTil0(false, true, -1);
	}

	public static long doPart2() {
		return runProgramTil0(false, false, -1);
	}

	// run with (false, anyValue, answer) to emulate the program as intended;
	// run with (true, true, -1) to find the answer for part 1;
	// run with (true, false, -1) to find the answer for part 2;
	public static long runProgramTil0(boolean research, boolean part1, long reg0) {
		long result = -1;
		BigInteger B65899 = BigInteger.valueOf(65899);
		BigInteger B16777215 = BigInteger.valueOf(16777215);
		Set<Long> answers = new HashSet<>();
		long lastAdded = -1;
		// b and d are no longer used when all expressions are inlined
		long reg2;
		long reg4;
		// program transcription, original statements in comments:
		reg2 = 0;                                                         // 05: [2] = 0
		while (true) {
			reg4 = reg2 | 65536;                                          // 06: [4] = [2] | 65536
			reg2 =  6718165;                                              // 07: [2] = 6718165
			while (true) {
				// Need to use BigInteger to avoid overflow               // 08: [3] = [4] & 255
				reg2 = BigInteger.valueOf((reg2 + (reg4 & 255)))          // 09: [2] += [3]
						.and(B16777215)                                   // 10: [2] &= 16777215
						.multiply(B65899)                                 // 11: [2] *= 65899
						.and(B16777215).longValueExact();                 // 12: [2] &= 16777215
				;                                                         // 13: [3] = [4] < 256
				if (reg4 < 256) { break; }                                // 14: JMP 15 + [3]
				;                                                         // 15: JMP 17
				;                                                         // 16: JMP 28
				// d = 0; // loop just calculates e / 256                 // 17: [3] = 0
				// while(true) {                                          // 18: [1] = [3] + 1
				//	;                                                     // 19: [1] *= 256
				//	;                                                     // 20: [1] = [1] > [4]
				//	if (((d + 1) * 256) > e) {break;}                     // 21: JMP 22 + [1]
				//	;                                                     // 22: JMP 24
				//	;                                                     // 23: JMP 26
				// 	d++;                                                  // 24: [3] += 1
				// }                                                      // 25: JMP 18
				reg4 = reg4 / 256;                                        // 26: [4] = [3]
			}                                                             // 27: JMP 8
			if (research) {
				if (part1) {
					return reg2;
				} else {
					// Assume that c starts looping over several values (pseudorandom routines usually do)
					// At some point it will look like (eg):
					// A B C D B C D B C D ...
					// while B is the first value that repeats, D would lead to more instructions executed before halting
					if (answers.contains(reg2)) {
						return lastAdded;
					} else {
						System.out.println(reg2);
						answers.add(reg2);
						lastAdded = reg2;
					}
				}
			}
			;                                                             // 28: [3] = [2] == [0]
			if (reg2 == reg0) { break; }                                  // 29: JMP 30 + [3]
		}                                                                 // 30: JMP 6
		return result;
	}

	public static void main(String[] args) {

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(Exercise21.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(Exercise21.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
