package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Step;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise07 {

	private static final Pattern PRECONDITION = Pattern.compile("Step ([A-Z]) must be finished before step ([A-Z]) can begin.");

	List<String> lines;
	Map<String, Step> steps;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		steps = new TreeMap<>();
		for (String line : lines) {
			Matcher m = PRECONDITION.matcher(line);
			if (m.find()) {
				Step stp = steps.computeIfAbsent(m.group(2), k -> new Step(m.group(2)));
				Step req = steps.computeIfAbsent(m.group(1), k -> new Step(m.group(1)));
				stp.addRequirement(req);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public String doPart1() {
		processInput();
		showTree();
		StringBuilder result = new StringBuilder();
		List<String> stepList = new ArrayList<>(steps.keySet());
		while (!stepList.isEmpty()) {
			for (String key : stepList) {
				if (steps.get(key).isReady()) {
					result.append(key);
					steps.get(key).execute();
					stepList.remove(key);
					break;
				}
			}
		}
		return result.toString();
	}

	public long doPart2() {
		processInput();
		int[] workers = new int[5];
		String[] activeTasks = new String[5];
		long secondsPassed = 0;
		int lastTask = 1;
		List<String> stepList = new ArrayList<>(steps.keySet());
		while (!stepList.isEmpty() || lastTask > 0) {
			// first handle all idle workers and if possible give them tasks, at the 'start' of the second
			for (int i = 0; i < 5; i++) {
				if (workers[i] == 0) { // idle worker, finish task if any, and choose next if possible
					if (activeTasks[i] != null) {
						steps.get(activeTasks[i]).execute();
						activeTasks[i] = null;
					}
					for (String key : stepList) {
						if (steps.get(key).isReady()) {
							int taskTime = key.charAt(0) - 'A' + 61; // A = 61, B = 62, etc
							activeTasks[i] = steps.get(key).name;
							stepList.remove(activeTasks[i]);
							workers[i] = taskTime;
							lastTask = workers[i];
							System.out.println("Second " + secondsPassed + ": Worker " + i + " received new task " + steps.get(key).name + " with time " + lastTask);
							break;
						}
					}
				}
			}
			// now have all non-idle workers progress on their task by one second
			for (int i = 0; i < 5; i++) {
				if (workers[i] > 0) {
					workers[i]--;
					if (workers[i] > 0 ) {
						System.out.println("Second " + secondsPassed + ": Worker " + i + " still has " + workers[i] + " seconds to go");
					} else {
						System.out.println("Second " + secondsPassed + ": Worker " + i + " completed task " + steps.get(activeTasks[i]).name + " at the end of of second " + secondsPassed);
					}
				}
			}
			lastTask--;
			secondsPassed++;
		}
		return secondsPassed;
	}

	public void showTree() {

	}
	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2018, 7));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
