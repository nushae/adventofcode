package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Exercise11 {

	long[][] summedArea; // see https://en.wikipedia.org/wiki/Summed-area_table

	public void makeSummedArea(int serialNumber) {
		summedArea = new long[300][300];
		for (int x = 0; x < 300; x++) {
			for (int y = 0; y < 300; y++) {
				summedArea[x][y] = (x > 0 ? summedArea[x-1][y] : 0) +
						(y > 0 ? summedArea[x][y-1] : 0) -
						(x > 0 && y > 0 ? summedArea[x-1][y-1] : 0) +
						powerLevel(x+1, y+1, serialNumber);
			}
		}
	}

	public long doPart1() {
		long maxPow = -40;
		int maxX = 0;
		int maxY = 0;
		for (int x = 0; x < 298; x++) {
			for (int y = 0; y < 298; y++) {
				long pow = (x > 0 && y > 0 ? summedArea[x-1][y-1] : 0)
						+ summedArea[x+2][y+2]
						- (y > 0 ? summedArea[x+2][y-1] : 0)
						- (x > 0 ? summedArea[x-1][y+2] : 0);
				if (pow > maxPow) {
					maxPow = pow;
					maxX = x+1;
					maxY = y+1;
				}
			}
		}
		System.out.println(maxX + ", " + maxY);
		return maxPow;
	}

	public long doPart2() {
		long maxPow = 30;
		int maxX = 243;
		int maxY = 64;
		int maxSize = 3;
		for (int size = 4; size <= 300; size++) {
			for (int x = 0; x < 301 - size; x++) {
				for (int y = 0; y < 301 - size; y++) {
					long pow = (x > 0 && y > 0 ? summedArea[x-1][y-1] : 0)
							+ summedArea[x+size-1][y+size-1]
							- (y > 0 ? summedArea[x+size-1][y-1] : 0)
							- (x > 0 ? summedArea[x-1][y+size-1] : 0);
					if (pow > maxPow) {
						maxPow = pow;
						maxX = x+1;
						maxY = y+1;
						maxSize = size;
					}
				}
			}
		}
		System.out.println(maxX + "," + maxY + "," + maxSize);
		return maxPow;
	}

	public int powerLevel(int x, int y, int serial) {
		return (((x + 10) * y + serial) * (x + 10) % 1000) / 100 - 5;
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11();

		// shared preparation
		long start = System.currentTimeMillis();
		ex.makeSummedArea(5468);
		System.out.println("Prep took " + (System.currentTimeMillis() - start) + "ms");

		// part 1
		start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
