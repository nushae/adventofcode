package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Cart;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
public class Exercise13 extends JPanel {

	private static final boolean VISUAL = false;

	List<String> lines;
	List<Cart> carts;
	int numCarts;
	char[][] grid;
	int height;
	int width;
	Point crash;

	public Exercise13() {
		lines = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		try (Stream<String> stream = Files.lines(path)) {
			stream.forEach(line -> {
				width = Math.max(width, line.length());
				lines.add(line);
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		height = lines.size();
	}

	public Exercise13(String data) {
		this();
		for (String line : data.split("\n")) {
			width = Math.max(width, line.length());
			lines.add(line);
		}
		height = lines.size();
	}

	private void processInput() {
		grid = new char[width][height];
		carts = new ArrayList<>();
		crash = null;
		for (int i = 0; i < height; i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				grid[j][i] = line.charAt(j);
				if (grid[j][i] == ' ') {
					grid[j][i] = '.';
				} else if (grid[j][i] == '/') {
					if (i == 0 || grid[j][i - 1] != '+' && grid[j][i - 1] != '|') {
						grid[j][i] = ',';
					} else {
						grid[j][i] = 'J';
					}
				} else if (grid[j][i] == '\\') {
					if (i == 0 || grid[j][i - 1] != '+' && grid[j][i - 1] != '|') {
						grid[j][i] = '7';
					} else {
						grid[j][i] = '\'';
					}
				} else if (grid[j][i] == '<') {
					carts.add(new Cart(j, i, -1, 0));
					grid[j][i] = '-';
				} else if (grid[j][i] == '>') {
					carts.add(new Cart(j, i, 1, 0));
					grid[j][i] = '-';
				} else if (grid[j][i] == 'v') {
					carts.add(new Cart(j, i, 0, 1));
					grid[j][i] = '|';
				} else if (grid[j][i] == '^') {
					carts.add(new Cart(j, i, 0, -1));
					grid[j][i] = '|';
				}
			}
		}
		numCarts = carts.size();
		System.out.println(width + " x " + height);
	}

	public Point doPart1() {
		processInput();
		while (true) {
			carts.sort(Comparator.comparing(Cart::getY).thenComparing(Cart::getX));
			for (int c = 0; c < carts.size(); c++) {
				Cart cart = carts.get(c);
				cart.move(grid);
				// test after every individual move or carts might move 'through' one another
				for (int d = 0; d < carts.size(); d++) {
					if (c != d) {
						Cart other = carts.get(d);
						if (cart.x == other.x && cart.y == other.y) {
							crash = new Point(cart.x, cart.y);
							if (VISUAL) updateView();
							return new Point(cart.x, cart.y);
						}
					}
				}
			}
			if (VISUAL) updateView();
		}
	}

	public Point doPart2() {
		processInput();
		int step = 0;
		while (true) {
			carts.sort(Comparator.comparing(Cart::getY).thenComparing(Cart::getX));
			for (int c = 0; c < carts.size(); c++) {
				Cart cart = carts.get(c);
				if (!cart.isCrashed()) {
					cart.move(grid);
					// test after every individual move or carts might move 'through' one another
					for (int d = 0; d < carts.size(); d++) {
						if (c != d) {
							Cart other = carts.get(d);
							if (!other.isCrashed()) {
								if (cart.x == other.x && cart.y == other.y) {
									cart.crash();
									other.crash();
									System.out.println(cart.x + "," + cart.y);
									numCarts -= 2;
									if (VISUAL) updateView();
									if (numCarts < 2) {
										for (Cart leftOver : carts) {
											if (!leftOver.isCrashed()) {
												return new Point(leftOver.x, leftOver.y);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (VISUAL) updateView();
		}
	}

	public void updateView() {
		repaint();
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			// NOP
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.white);
		g2.fillRect(0, 0, getWidth(), getHeight());
		int FACTOR = Math.min(getWidth() / width, getHeight() / height);
		int offsetX = (getWidth() - width * FACTOR) / 2;
		int offsetY = (getHeight() - height * FACTOR) / 2;
		g2.setColor(Color.blue);
		g2.drawRect(offsetX, offsetY, FACTOR * width, FACTOR * height);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int x1 = offsetX + FACTOR * x + FACTOR * 5 / 8;
				int y1 = offsetY + FACTOR * y + FACTOR * 5 / 8;
				switch (grid[x][y]) {
					case '.' -> {
						g2.setColor((x + y) % 2 == 0 ? Color.white : Color.lightGray);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR);
					} case '|' -> {
						g2.setColor(Color.black);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR);
						g2.fillRect(x1, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR);
					} case '-' -> {
						g2.setColor(Color.black);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR * 3 / 8);
						g2.fillRect(offsetX + FACTOR * x, y1, FACTOR, FACTOR * 3 / 8);
					} case '7' -> {
						g2.setColor(Color.black);
						g2.fillRect(offsetX + FACTOR * x, y1, FACTOR * 3 / 8, FACTOR * 3 / 8);
						g2.fillRect(x1, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR * 3 / 8);
					} case '\'' -> {
						g2.setColor(Color.black);
						g2.fillRect(x1, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR * 3 / 8);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR);
						g2.fillRect(offsetX + FACTOR * x, y1, FACTOR, FACTOR * 3 / 8);
					} case 'J' -> {
						g2.setColor(Color.black);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR * 3 / 8);
						g2.fillRect(x1, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR);
						g2.fillRect(offsetX + FACTOR * x, y1, FACTOR, FACTOR * 3 / 8);
					} case ',' -> {
						g2.setColor(Color.black);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR * 3 / 8);
						g2.fillRect(x1, y1, FACTOR * 3 / 8, FACTOR * 3 / 8);
					} case '+' -> {
						g2.setColor(Color.black);
						g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR * 3 / 8);
						g2.fillRect(x1, offsetY + FACTOR * y, FACTOR * 3 / 8, FACTOR * 3 / 8);
						g2.fillRect(offsetX + FACTOR * x, y1, FACTOR * 3 / 8, FACTOR * 3 / 8);
						g2.fillRect(x1, y1, FACTOR * 3 / 8, FACTOR * 3 / 8);
					}
				}
			}
		}

		for (Cart cart : carts) {
			if (!cart.isCrashed()) {
				g2.setColor(Color.cyan);
				g2.fillRect(offsetX + FACTOR * cart.x + (1 - cart.dir.x) * FACTOR / 8, offsetY + FACTOR * cart.y + (1 - cart.dir.y) * FACTOR / 8, FACTOR * 6 / 8, FACTOR * 6 / 8);
				g2.fillRect(offsetX + FACTOR * cart.x + FACTOR * 2 / 8 + cart.dir.x * FACTOR / 8, offsetY + FACTOR * cart.y + FACTOR * 2 / 8 + cart.dir.y * FACTOR / 8, FACTOR * 4 / 8, FACTOR * 4 / 8);
				g2.fillRect(offsetX + FACTOR * cart.x + FACTOR * 3 / 8 + cart.dir.x * 3 * FACTOR / 8, offsetY + FACTOR * cart.y + FACTOR * 3 / 8 + cart.dir.y * 3 * FACTOR / 8, FACTOR * 2 / 8, FACTOR * 2 / 8);
			}
		}

		if (crash != null) {
			g2.setColor(Color.red);
			g2.fillRect(offsetX + FACTOR * crash.x + FACTOR / 8, offsetY + FACTOR * crash.y + FACTOR / 8, FACTOR * 6 / 8, FACTOR * 6 / 8);
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1216, 1239));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2018, 13));

		if (VISUAL) SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
