package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.WristDevice;
import com.nushae.aoc.aoc2018.domain.WristMem;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise16 {

	WristDevice appleWatch = new WristDevice();
	List<String> lines;
	List<String> samples;
	List<String> program;

	public Exercise16() {
		lines = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		program = new ArrayList<>();
		samples = new ArrayList<>();
		String line = "";
		int i;
		for (i = 0; i < lines.size(); i += 4) {
			line = lines.get(i); // before
			if (line.isEmpty()) {
				break;
			}
			StringBuilder sample = new StringBuilder();
			for (String num : line.substring(9, line.length()-1).split(", ")) {
				sample.append(num).append(" ");
			}
			sample.append(lines.get(i+1)); // instruction
			for (String num : lines.get(i+2).substring(9, line.length()-1).split(", ")) {
				sample.append(" ").append(num);
			}
			samples.add(sample.toString());
		}
		while (i < lines.size()) {
			line = lines.get(i);
			if (!line.isEmpty()) {
				program.add(line);
			}
			i++;
		}
	}

	public long doPart1() {
		processInput();
		long count = 0;
		Set<String> opcodeOptions = new HashSet<>(Arrays.asList("addi", "addr", "bani", "banr", "bori", "borr", "eqir", "eqri", "eqrr", "gtir", "gtri", "gtrr", "muli", "mulr", "seti", "setr"));
		for (String sample : samples) {
			int sampleCount = 0;
			String[] parts = sample.split(" ");
			WristMem before = new WristMem(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
			int par1 = Integer.parseInt(parts[5]);
			int par2 = Integer.parseInt(parts[6]);
			int par3 = Integer.parseInt(parts[7]);
			WristMem after = new WristMem(Integer.parseInt(parts[8]), Integer.parseInt(parts[9]), Integer.parseInt(parts[10]), Integer.parseInt(parts[11]));
			for (String opcode : opcodeOptions) {
				WristMem result = appleWatch.execute(before, opcode, par1, par2, par3);
				if (result.a == after.a && result.b == after.b && result.c == after.c && result.d == after.d) {
					sampleCount++;
				}
			}
			if (sampleCount >= 3) {
				count++;
			}
		}
		return count;
	}

	public long doPart2() {
		processInput();
		Map<Integer, Set<String>> opcodeOptions = new HashMap<>();
		Map<Integer, String> opcodeSet = new HashMap<>();
		for (int i = 0; i < 16; i++) {
			opcodeOptions.put(i, new HashSet<>(Arrays.asList("addi", "addr", "bani", "banr", "bori", "borr", "eqir", "eqri", "eqrr", "gtir", "gtri", "gtrr", "muli", "mulr", "seti", "setr")));
		}
		int found = 0;
		for (String sample : samples) {
			String[] parts = sample.split(" ");
			WristMem before = new WristMem(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
			int opcodeNum = Integer.parseInt(parts[4]);
			int par1 = Integer.parseInt(parts[5]);
			int par2 = Integer.parseInt(parts[6]);
			int par3 = Integer.parseInt(parts[7]);
			if (opcodeOptions.containsKey(opcodeNum)) {
				WristMem after = new WristMem(Integer.parseInt(parts[8]), Integer.parseInt(parts[9]), Integer.parseInt(parts[10]), Integer.parseInt(parts[11]));
				Set<String> options = opcodeOptions.get(opcodeNum);
				Set<String> newOptions = new HashSet<>();
				for (String opcode : options) {
					WristMem result = appleWatch.execute(before, opcode, par1, par2, par3);
					if (result.a == after.a && result.b == after.b && result.c == after.c && result.d == after.d) {
						newOptions.add(opcode);
					}
				}
				opcodeOptions.put(opcodeNum, newOptions);
				Map<Integer, Set<String>> newOpcodeOptions = new HashMap<>();
				for (Map.Entry<Integer, Set<String>> ops : opcodeOptions.entrySet()) {
					if (ops.getValue().size() == 1) {
						opcodeSet.put(ops.getKey(), ops.getValue().stream().findFirst().get());
					} else {
						newOpcodeOptions.put(ops.getKey(), ops.getValue());
					}
				}
				opcodeOptions = newOpcodeOptions;
				for (int i: opcodeSet.keySet()) {
					for (int j : opcodeOptions.keySet()) {
						opcodeOptions.get(j).remove(opcodeSet.get(i));
					}
				}
				if (opcodeSet.size() > 15) {
					break;
				}
			}
		}

		for (int i = 0; i < 16; i++) {
			System.out.printf("%02d -> %s%n", i, opcodeSet.get(i));
		}
		appleWatch.setOpcodes(new ArrayList<>(opcodeSet.values()));
		appleWatch.loadProgramFromData(program);

		return appleWatch.runProgram(0, 0, 0, 0).a;
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2018, 16));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
