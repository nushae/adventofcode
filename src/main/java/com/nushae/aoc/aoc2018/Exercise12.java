package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise12 {

	private static final Pattern TRANSITION = Pattern.compile("([.#]{5}) => ([.#])");

	List<String> lines;
	TreeSet<Integer> numbers;
	Map<String, Boolean> rules;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		numbers = new TreeSet<>();
		rules = new HashMap<>();
		for (String line : lines) {
			if (line.length() > 15) { // initial state
				for (int i = 0; i < line.length() - 15; i++) {
					if (line.charAt(i+15) == '#') {
						numbers.add(i);
					}
				}
			} else if (line.length() > 0) { // not empty
				Matcher m = TRANSITION.matcher(line);
				if (m.find()) {
					rules.put(m.group(1), "#".equals(m.group(2)));
				} else {
					log.warn("Don't know how to parse " + line);
				}
			}
		}
	}

	public long doPart1() {
		processInput();
		for (int generation = 0; generation < 20; generation++) {
			numbers = doGeneration(numbers);
		}
		long result = 0;
		for (int num : numbers) {
			result += num;
		}
		return result;
	}

	public long doPart2() {
		processInput();
		// Turns out if you run the following loop for a bit, the pattern stabilizes and moves one to the right each generation.
		// The pattern becomes (eg):
		// Generation: 153
		// Lowest: 71 / Highest: 248
		// #...#...#...#...#...#...#...#...#.####...#...#...#...#...#...#...#...#...#...#...#...#...#...#...#...#...#..####...#...#...#...#...#...#...#...#...#...#...#...#...#...#...#..####
		// Sum: 4865
		for (long generation = 1; generation <= 50000000000L; generation++) {
			String old = numberString(numbers);
			numbers = doGeneration(numbers);
			String next = numberString(numbers);
			if (old.equals(next)) {
				System.out.printf("First time the patterns stays the same is in generation: %d%n", generation - 1);
				System.out.printf("Lowest: %d / Highest: %d%n", numbers.first() - 1, numbers.last() - 1);
				System.out.println(next);
				long total = total(numbers);
				System.out.println(total);
				long extra = (50000000000L - generation);
				System.out.println("After " + extra + " more generation(s) these numbers will all be " + extra + " higher, for a total of " + (extra * numbers.size() + total));
				return total + extra * numbers.size();
			}
		}
		return total(numbers);
	}

	public TreeSet<Integer> doGeneration(TreeSet<Integer> numbers) {
		TreeSet<Integer> newNumbers = new TreeSet<>();
		for (int i = numbers.first() - 4; i <= numbers.last()+4; i++) {
			StringBuilder pattern = new StringBuilder();
			for (int j = i - 2; j <= i + 2; j++) {
				pattern.append(numbers.contains(j) ? "#" : ".");
			}
			if (rules.getOrDefault(pattern.toString(), false)) {
				newNumbers.add(i);
			}
		}
		return newNumbers;
	}

	public String numberString(TreeSet<Integer> numbers) {
		StringBuilder result = new StringBuilder();
		for (int i = numbers.first(); i <= numbers.last(); i++) {
			result.append(numbers.contains(i) ? "#" : ".");
		}
		return result.toString();
	}

	public static long total(Collection<Integer> numbers) {
		long result = 0;
		for (int num : numbers) {
			result += num;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2018, 12));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
