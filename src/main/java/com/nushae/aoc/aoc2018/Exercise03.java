package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.RectangleWithGaps;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise03 extends JPanel {

	private static final Pattern FABRIC_CLAIM = Pattern.compile("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)");

	java.util.List<String> lines;
	java.util.List<Rectangle> claims;
	Set<Integer> ids;
	RectangleWithGaps fabric;
	int minX = Integer.MAX_VALUE;
	int minY = Integer.MAX_VALUE;
	int maxX = Integer.MIN_VALUE;
	int maxY = Integer.MIN_VALUE;
	long nonoverlapping = -1;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		claims = new ArrayList<>();
		ids = new HashSet<>();
		int i = 1;
		for (String line : lines) {
			Matcher m = FABRIC_CLAIM.matcher(line);
			if (m.find()) {
				// id's turn out to run sequentially from 1 so we don't need to parse these
				ids.add(i++);
				int x = Integer.parseInt(m.group(2));
				int y = Integer.parseInt(m.group(3));
				int w = Integer.parseInt(m.group(4));
				int h = Integer.parseInt(m.group(5));
				minX = Math.min(minX, x);
				minY = Math.min(minY, y);
				maxX = Math.max(maxX, x+w);
				maxY = Math.max(maxY, y+h);
				claims.add(new Rectangle(x, y, w, h));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		System.out.println(claims.size() + " rectangles on a " + minX + ", " + minY + " - " + maxX + ", " + maxY + " piece of fabric");
		fabric = new RectangleWithGaps(new Rectangle(minX, minY, maxX-minX, maxY-minY));
	}


	public long doPart1() {
		processInput();
		long result = fabric.area();
		for (int left = 0; left < claims.size()-1; left++) {
			for (int right = left+1; right < claims.size(); right++) {
				Rectangle overlap = claims.get(left).intersection(claims.get(right));
				if (!overlap.isEmpty()) {
					fabric.subtract(overlap);
					ids.remove(left + 1);
					ids.remove(right + 1);
				}
			}
		}
		return result - fabric.area();
	}

	public long doPart2() {
		processInput();
		for (int left = 0; ids.size() > 1 && left < claims.size()-1; left++) {
			for (int right = left+1; ids.size() > 1 && right < claims.size(); right++) {
				Rectangle overlap = claims.get(left).intersection(claims.get(right));
				if (!overlap.isEmpty()) {
					ids.remove(left+1);
					ids.remove(right+1);
				}
			}
		}
		nonoverlapping = ids.stream().findFirst().orElse(-1);
		repaint();
		return nonoverlapping;
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2018, 3));

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.white);
		g2.fillRect(0, 0, getWidth(), getHeight());
		int offsetX = (getWidth() - fabric.getExtent().width) / 2;
		int offsetY = (getHeight() - fabric.getExtent().height) / 2;
		g2.setColor(Color.lightGray);
		g2.fillRect(offsetX + fabric.getExtent().x, offsetY + fabric.getExtent().y, fabric.getExtent().width, fabric.getExtent().height);
		for (int i = 0; i < claims.size(); i++) {
			Rectangle claim = claims.get(i);
			if (i+1 == nonoverlapping) {
				g2.setColor(Color.yellow);
				g2.fillRect(offsetX + claim.x, offsetY + claim.y, claim.width, claim.height);
			}
			g2.setColor(Color.blue);
			g2.drawRect(offsetX + claim.x, offsetY + claim.y, claim.width, claim.height);
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
