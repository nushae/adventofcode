package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.CaveRegion;
import com.nushae.aoc.domain.SearchNode;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.SearchUtil.dsp;

@Log4j2
public class Exercise22 {

	public static final int DEPTH = 3339;
	public static final Point TARGET = new Point(10, 715);
	public static final int TOOL_NEITHER = 0; // These values were chosen specifically to make tool swaps easier to calculate
	public static final int TOOL_TORCH = 1;
	public static final int TOOL_CLIMBING = 2;

	Map<Point, Long> indexes;
	Map<Point, Long> erosions;
	Map<Point, Integer> types;

	public long doPart1(int depth, Point target, Point area) {
		long result = 0;
		indexes = new HashMap<>();
		erosions = new HashMap<>();
		types = new HashMap<>();
		for (int y = 0; y <= area.y; y++) {
			for (int x = 0; x <= area.x; x++) {
				Point region = new Point(x, y);
				long index;
				if (x == 0 && y == 0 || x == target.x && y == target.y) {
					index = 0;
				} else if (x == 0) {
					index = y * 48271L;
				} else if (y == 0) {
					index = x * 16807L;
				} else {
					long left = erosions.get(new Point(x-1, y));
					long above = erosions.get(new Point(x, y-1));
					index = left * above;
				}
				indexes.put(region, index);
				long erosion = (index + depth) % 20183L;
				erosions.put(region, erosion);
				int type = (int) (erosion % 3);
				types. put(region, type);
				if (x <= target.x && y <= target.y) {
					result += type;
				}
			}
		}
		return result;
	}

	public long doPart2(int depth, Point mouth, int tool, Point target, Point area) {
		doPart1(depth, new Point(target.x, target.y), area); // x3 = probably sufficiently large
		CaveRegion start = new CaveRegion(mouth.x, mouth.y, target.x, target.y, tool, types);
		SearchNode result = dsp(start, node -> node.getState().startsWith(target.x + "," + target.y + "/"));
		System.out.println("Best path, (X) indicates tool change:");
		System.out.println("From                         To                         Move-info");
		System.out.println("Type  Ok tools  State        Type  Ok Tools  State      (X)  Cost");
		System.out.println("----  --------  ---------    ----  --------  ---------  ---  ----");
		StringBuilder pathOutput = new StringBuilder();
		long pathLen = 0;
		while (result != null) { // To know the path, you have to walk the path. In reverse.
			if (result.getParent() != null) {
				for (Map.Entry<SearchNode, Long> nb : result.getParent().getAdjacentNodes().entrySet()) {
					if (nb.getKey().equals(result)) {
						pathOutput.insert(0, String.format(
								"%14s  %9s -> %14s  %9s %4s   %2d\n",
								makeTerrainString(types.get(((CaveRegion) result.getParent()).getLocation())),
								result.getParent().getState(),
								makeTerrainString(types.get(((CaveRegion) result).getLocation())),
								result.getState(),
								nb.getValue() >= 8 ? "(X)" : "",
								nb.getValue()));
						pathLen += nb.getValue();
					}
				}
			}
			result = result.getParent();
		}
		System.out.println(pathOutput);
		return pathLen;
	}

	private String makeTerrainString(int terrainType) {
		switch (terrainType) {
			case 0 -> {
				return " .   C=" + TOOL_CLIMBING + ", T=" + TOOL_TORCH;
			}
			case 1 -> {
				return " =   C=" + TOOL_CLIMBING + ", N=" + TOOL_NEITHER;
			}
			case 2 -> {
				return " |   N=" + TOOL_NEITHER + ", T=" + TOOL_TORCH;
			}
			default -> throw new IllegalArgumentException("Not a type: " + terrainType);
		}
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(DEPTH, TARGET , TARGET));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(DEPTH, new Point(0, 0), TOOL_TORCH, TARGET, new Point(TARGET.x * 3, TARGET.y * 3)));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
