package com.nushae.aoc.aoc2018;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2018, day = 1, title="Chronal Calibration", keywords = {"list-processing", "sum", "loop-detection"})
public class Exercise01 {

	private ArrayList<String> lines;
	private ArrayList<Long> numbers;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		numbers = new ArrayList<>();
		for (String line : lines) {
			numbers.add(Long.parseLong(line));
		}
	}

	public long doPart1() {
		processInput();
		return numbers.stream().mapToLong(x -> x).sum();
	}

	public long doPart2() {
		processInput();
		Set<Long> visited = new HashSet<>();
		long result = 0;
		visited.add(0L);
		int index = 0;
		while (true) {
			result += numbers.get(index);
			if (visited.contains(result)) {
				break;
			} else {
				visited.add(result);
			}
			index = (index + 1) % numbers.size();
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2018, 1));

		// part 1 - 510 (0.003s)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 69074 (0.02s)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
