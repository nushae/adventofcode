package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Observation;
import com.nushae.aoc.aoc2018.domain.Shift;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise04 {

	private static final Pattern OBSERVATION = Pattern.compile("\\[(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d) (\\d\\d):(\\d\\d)] (Guard #(\\d+) begins shift|falls asleep|wakes up)");

	List<String> lines;
	Map<Integer, Observation> observations;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		lines = lines.stream().sorted().collect(Collectors.toList());
		observations = new HashMap<>();
		Shift s = null;
		int guardId = -1;
		for (String line : lines) {
			Matcher m = OBSERVATION.matcher(line);
			if (m.find()) {
				if (m.group(6).startsWith("Guard")) { // new guard, begin new observation and store current shift in previous (if any)
					if (s != null) {
						observations.computeIfAbsent(guardId, k -> new Observation()).addShift(s);
					}
					guardId = Integer.parseInt(m.group(7));
					s = new Shift(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
				} else if (m.group(6).startsWith("falls")) {
					if (s != null) {
						s.fallAsleep(Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
					} else {
						throw new IllegalArgumentException("Expected shift start, but encountered " + line);
					}
				} else { // wakes
					if (s != null) {
						s.wakeUp(Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
					} else {
						throw new IllegalArgumentException("Expected shift start, but encountered " + line);
					}
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		if (s != null) {
			observations.computeIfAbsent(guardId, k -> new Observation()).addShift(s);
		}
	}

	public void printOverview() {
		System.out.println(observations.size() + " guards observed:");
		for (Map.Entry<Integer, Observation> observation : observations.entrySet()) {
			int id = observation.getKey();
			System.out.println("            Minute");
			System.out.printf("#%-8d   000000000011111111112222222222333333333344444444445555555555%n", id);
			System.out.println("            012345678901234567890123456789012345678901234567890123456789");
			System.out.println(observation.getValue().toString());
		}
	}

	public long doPart1() {
		processInput();
		printOverview();
		int sleepiestGuard = -1;
		int sleepiestMinute = -1;
		int maxTotalAsleep = -1;
		for (Map.Entry<Integer, Observation> observation : observations.entrySet()) {
			Observation obs = observation.getValue();
			if (obs.getTotalAsleep() > maxTotalAsleep) {
				maxTotalAsleep = obs.getTotalAsleep();
				sleepiestGuard = observation.getKey();
				sleepiestMinute = obs.sleepiestMinute();
			}
		}
		return (long) sleepiestGuard * sleepiestMinute;
	}

	public long doPart2() {
		//processInput() not needed provided you run part 1 first
		int sleepiestGuard = -1;
		int sleepiestMinute = -1;
		int maxMostFrequentAsleep = -1;
		for (Map.Entry<Integer, Observation> observation : observations.entrySet()) {
			Observation obs = observation.getValue();
			if (obs.mostFrequentAsleep() > maxMostFrequentAsleep) {
				maxMostFrequentAsleep = obs.mostFrequentAsleep();
				sleepiestGuard = observation.getKey();
				sleepiestMinute = obs.sleepiestMinute();
			}
		}
		return (long) sleepiestGuard * sleepiestMinute;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2018, 4));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
