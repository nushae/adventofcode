package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.manhattanDistance;

@Log4j2
public class Exercise06 extends JPanel {

	private static final Pattern POINT = Pattern.compile("(\\d+), (\\d+)");
	private static final Color[] COLORS = {
			new Color(255, 0, 0),
			new Color(240, 16, 0),
			new Color(224, 32, 0),
			new Color(208, 48, 0),
			new Color(192, 64, 0),
			new Color(176, 80, 0),
			new Color(160, 96, 0),
			new Color(144, 112, 0),
			new Color(128, 128, 0),
			new Color(112, 144, 0),
			new Color(96, 160, 0),
			new Color(80, 176, 0),
			new Color(64, 192, 0),
			new Color(48, 208, 0),
			new Color(32, 224, 0),
			new Color(16, 240, 0),
			new Color(0, 255, 0),
			new Color(0, 240, 16),
			new Color(0, 224, 32),
			new Color(0, 208, 48),
			new Color(0, 192, 64),
			new Color(0, 176, 80),
			new Color(0, 160, 96),
			new Color(0, 144, 112),
			new Color(0, 128, 128),
			new Color(0, 112, 144),
			new Color(0, 96, 160),
			new Color(0, 80, 176),
			new Color(0, 64, 192),
			new Color(0, 48, 208),
			new Color(0, 32, 224),
			new Color(0, 16, 240),
			new Color(0, 0, 255),
			new Color(16, 0, 240),
			new Color(32, 0, 224),
			new Color(48, 0, 208),
			new Color(64, 0, 192),
			new Color(80, 0, 176),
			new Color(96, 0, 160),
			new Color(112, 0, 144),
			new Color(128, 0, 128),
			new Color(144, 0, 112),
			new Color(160, 0, 96),
			new Color(176, 0, 80),
			new Color(192, 0, 64),
			new Color(208, 0, 48),
			new Color(224, 0, 32),
			new Color(240, 0, 16),
			new Color(255, 0, 0),
			new Color(100, 100, 100)
	};
	List<String> lines;
	Map<Point, Integer> coordinates;
	int minX = Integer.MAX_VALUE;
	int minY = Integer.MAX_VALUE;
	int maxX = Integer.MIN_VALUE;
	int maxY = Integer.MIN_VALUE;
	int width;
	int height;
	Point centerPoint = null;
	int[][] grid;
	Set<Point> areaPoints;
	Map<Integer, Integer> regions;

	public Exercise06() {
		lines = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		coordinates = new HashMap<>();
		int count = 0;
		long totalX = 0;
		long totalY = 0;
		for (String line : lines) {
			Matcher m = POINT.matcher(line);
			if (m.find()) {
				Point p = new Point(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
				coordinates.put(p, count++);
				minX = Math.min(minX, p.x);
				minY = Math.min(minY, p.y);
				maxX = Math.max(maxX, p.x);
				maxY = Math.max(maxY, p.y);
				totalX += p.x;
				totalY += p.y;
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		width = maxX - minX + 1;
		height = maxY - minY + 1;
		centerPoint = new Point((int)(totalX / coordinates.size()), (int) (totalY / coordinates.size()));
		System.out.printf("%d x %d and %d points with center %s%n", width, height, coordinates.size(), centerPoint);
	}

	public long doPart1() {
		processInput();
		regions = new HashMap<>();
		grid = new int[width][height];
		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				long bestDist = width + height;
				int bestRegion = -1;
				for (Point p : coordinates.keySet()) {
					long dist = manhattanDistance(x, y, p.x, p.y);
					if (dist < bestDist) {
						bestDist = dist;
						bestRegion = coordinates.get(p);
					} else if (dist == bestDist) { // 2 equidistant points
						bestRegion = -1;
					}
				}
				grid[x - minX][y - minY] = bestRegion;
				regions.put(bestRegion, regions.getOrDefault(bestRegion, 0)  + 1);
			}
		}
		repaint();
		// remove areas that hit the edge -> infinite areas
		for (int x = minX; x <= maxX; x++) {
			regions.remove(grid[x - minX][0]);
			regions.remove(grid[x - minX][maxY - minY]);
		}
		for (int y = minY; y <= maxY; y++) {
			regions.remove(grid[0][y - minY]);
			regions.remove(grid[maxX - minX][y - minY]);
		}
		repaint();
		long result = 0;
		for (int size : regions.values()) {
			result = Math.max(result, size);
		}
		return result;
	}

	public long doPart2(long limit) {
		areaPoints = new HashSet<>();
		long area = 0;
		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				Point p = new Point(x, y);
				if (totalDistance(p) < limit) {
					area++;
					areaPoints.add(p);
				}
			}
		}
		return area;
	}

	public long totalDistance(Point location) {
		long total = 0;
		for (Point p : coordinates.keySet()) {
			total += manhattanDistance(location, p);
		}
		return total;
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.white);
		g2.fillRect(0, 0, getWidth(), getHeight());
		int width = maxX - minX + 1;
		int height = maxY - minY + 1;
		int FACTOR = Math.min((getWidth() - 50) / width, (getHeight() - 50)/ height);
		int offsetX = (getWidth() - width * FACTOR) / 2 - FACTOR * minX;
		int offsetY = (getHeight() - height * FACTOR) / 2 - FACTOR * minY;
		g2.setColor(Color.blue);
		g2.drawRect(offsetX + FACTOR * minX, offsetY + FACTOR * minY, FACTOR * width, FACTOR * height);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Point p = new Point(x+minX, y+minY);
				if (areaPoints != null && areaPoints.contains(p)) {
					g2.setColor(Color.magenta);
					g2.fillRect(offsetX + FACTOR * (x + minX), offsetY + FACTOR * (y + minY), FACTOR, FACTOR);
				}
				if (grid[x][y] < 0) {
					g2.setColor(Color.black);
					g2.fillOval(offsetX + FACTOR * (x + minX), offsetY + FACTOR * (y + minY), FACTOR, FACTOR);
				} else if (regions.containsKey(grid[x][y])){
					g2.setColor(COLORS[grid[x][y]]);
					g2.fillOval(offsetX + FACTOR * (x + minX), offsetY + FACTOR * (y + minY), FACTOR, FACTOR);
				}
			}
		}
		for (Point p: coordinates.keySet()) {
			g2.setColor(Color.red);
			g2.fillOval(offsetX + FACTOR * p.x, offsetY + FACTOR * p.y, FACTOR, FACTOR);
		}

		if (centerPoint != null) {
			g2.setColor(Color.cyan);
			g2.drawOval(offsetX + FACTOR * centerPoint.x - FACTOR, offsetY + FACTOR * centerPoint.y - FACTOR, 2 * FACTOR, 2 * FACTOR);
			g2.fillOval(offsetX + FACTOR * centerPoint.x, offsetY + FACTOR * centerPoint.y, FACTOR, FACTOR);

		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2018, 6));
		// Exercise06 ex = new Exercise06("2, 3\n 9, 7");

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// Note, this is about Voronoi diagrams / Voronoi cells but we don't need the algorithm
		// Because we have a limited grid size we can suffice by just calculating the region value for each pixel in an O(N^2) loop

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(10000));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
