package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Exercise14 {

	List<Integer> numbers;

	public long doPart1(int recipes) {
		numbers = new ArrayList<>();
		numbers.add(3);
		numbers.add(7);
		StringBuilder result = new StringBuilder("37");
		int digits = 2;
		int elf1 = 0;
		int elf2 = 1;
		while (digits < recipes + 10) {
			int total = numbers.get(elf1) + numbers.get(elf2);
			if (total < 10) {
				numbers.add(total);
				result.append(total);
				digits++;
			} else {
				numbers.add(total / 10);
				result.append(total / 10);
				digits++;
				numbers.add(total % 10);
				result.append(total % 10);
				digits++;
			}
			elf1 = (elf1 + 1 + numbers.get(elf1)) % numbers.size();
			elf2 = (elf2 + 1 + numbers.get(elf2)) % numbers.size();
		}
		return Long.parseLong(result.substring(recipes));
	}

	public long doPart2(int recipes) {
		String input = "" + recipes;
		numbers = new ArrayList<>();
		numbers.add(3);
		numbers.add(7);
		StringBuilder result = new StringBuilder("----37"); // add ---- to avoid issues with result.substring()
		int elf1 = 0;
		int elf2 = 1;
		while (!result.substring(result.length()-6).equals(input)) {
			int total = numbers.get(elf1) + numbers.get(elf2);
			if (total < 10) {
				numbers.add(total);
				result.append(total);
			} else {
				numbers.add(total / 10);
				result.append(total / 10);
				if (result.substring(result.length()-6).equals(input)) {
					return result.length()-10;
				}
				numbers.add(total % 10);
				result.append(total % 10);
			}
			elf1 = (elf1 + 1 + numbers.get(elf1)) % numbers.size();
			elf2 = (elf2 + 1 + numbers.get(elf2)) % numbers.size();
		}
		return result.length()-10; // -10 because of the ----
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(260321));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(260321));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
