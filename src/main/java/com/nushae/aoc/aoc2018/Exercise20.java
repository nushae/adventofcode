package com.nushae.aoc.aoc2018;

import com.nushae.aoc.aoc2018.domain.Routexp;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

@Log4j2
public class Exercise20 extends JPanel {

	private static final boolean VISUAL = true;

	List<String> lines;
	String input;
	Routexp route;
	Map<Point, Character> spaces;
	Map<Point, Integer> bestDistances;
	int minX;
	int maxX;
	int minY;
	int maxY;
	int width;
	int height;

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		input = lines.get(0).substring(1, lines.get(0).length()-1);
		System.out.println(input);
		route = parseExpr();
	}

	public Routexp parseExpr() {
		if (input.charAt(0) == '(') {
			return parseParallel();
		} else {
			return parseSerial();
		}
	}

	public Routexp parseParallel() {
		List<Routexp> parts = new ArrayList<>();
		do {
			eat(1); // either ( (for first part) or | (for later parts)
			parts.add(parseExpr());
		} while (input.charAt(0) == '|');
		if (input.charAt(0) == ')') {
			eat(1);
			return new Routexp(false, parts);
		} else {
			throw new IllegalArgumentException("Expected a ')' in the input: " + input);
		}

	}

	public Routexp parseSerial() {
		List<Routexp> parts = new ArrayList<>();
		while (input.length() > 0 && input.charAt(0) != '|' && input.charAt(0) != ')') {
			if (input.charAt(0) == '(') {
				parts.add(parseParallel());
			} else {
				parts.add(parseLiteral());
			}
		}
		if (parts.size() > 1) {
			return new Routexp(true, parts);
		} else if (parts.size() == 0) {
			return new Routexp(""); // empty part
		} else {
			return parts.get(0);
		}
	}

	public Routexp parseLiteral() {
		StringBuilder symbols = new StringBuilder();
		while (input.length() > 0 && "NEWS".contains(input.substring(0, 1))) {
			symbols.append(eat(1));
		}
		return new Routexp(symbols.toString());
	}

	public String eat(int num) {
		if (input.length() >= num) {
			String result = input.substring(0, num);
			input = input.substring(num);
			return result;
		} else {
			throw new IllegalArgumentException("Not enough input left to eat " + num + " chars: " + input);
		}
	}

	public long doPart1() {
		processInput();
		// System.out.println(route.toString());
		spaces =  new HashMap<>();
		spaces.put(new Point(0, 0), 'X');
		route.apply(Collections.singleton(new Point(0, 0)), spaces);
		System.out.println(spaces.size());
		minX = Integer.MAX_VALUE;
		maxX = Integer.MIN_VALUE;
		minY = Integer.MAX_VALUE;
		maxY = Integer.MIN_VALUE;
		for (Point p : spaces.keySet()) {
			minX = Math.min(minX, p.x);
			maxX = Math.max(maxX, p.x);
			minY = Math.min(minY, p.y);
			maxY = Math.max(maxY, p.y);
		}
		minX--;
		maxX++;
		minY--;
		maxY++;
		width = maxX - minX + 1;
		height = maxY - minY + 1;
		System.out.println(minX + ".." + maxX + " = " + width + "    x    " + minY + ".." + maxY + " = " + height);
		for (int x = minX; x <= maxX; x++) {
			for (int y = minY; y <= maxY; y++) {
				Point p = new Point(x, y);
				if (!spaces.containsKey(p)) {
					spaces.put(p, '#');
				}
			}
		}
		if (VISUAL) repaint();

		// Now, at last we count the longest path... With dijkstra, iterative version

		List<Point> path = new ArrayList<>();
		Map<Point, Point> prev = new HashMap<>();
		bestDistances = new HashMap<>(); // best distances are remembered for part 2
		bestDistances.put(new Point(0, 0), 0);
		Set<Point> queue = new HashSet<>();
		Set<Point> visited = new HashSet<>();
		queue.add(new Point(0, 0));
		visited.add(new Point(0, 0));
		long result = 0;
		while (queue.size() > 0) {
			Point minNode = null;
			int minDist = Integer.MAX_VALUE;
			for (Point n : queue) {
				if (minDist > bestDistances.getOrDefault(n, Integer.MAX_VALUE)) {
					minDist = bestDistances.getOrDefault(n, Integer.MAX_VALUE);
					minNode = n;
				}
			}
			if (minNode != null) {
				visited.add(minNode);
				queue.remove(minNode);
				for (Point p : ORTHO_DIRS) {
					Point neighbour = new Point(minNode.x + p.x, minNode.y + p.y);
					if (spaces.containsKey(neighbour) && (spaces.get(neighbour) == '|' || spaces.get(neighbour) == '-')) {
						neighbour = new Point(neighbour.x + p.x, neighbour.y + p.y);
						if (!visited.contains(neighbour)) {
							queue.add(neighbour);
							int tentative = minDist + 1; // distance to reach is always 1
							if (tentative < bestDistances.getOrDefault(neighbour, Integer.MAX_VALUE)) {
								bestDistances.put(neighbour, tentative);
								result = Math.max(result, tentative); // only remember distances actually put in the graph
								prev.put(neighbour, minNode);
							}
						}
					}
				}
			}
		}
		return result;
	}

	public long doPart2() {
		// part 2 depends on part 1, so run that first.
		long result = 0;
		for (Map.Entry<Point, Integer> de : bestDistances.entrySet()) {
			if (de.getValue() > 999) {
				result++;
			}
		}
		return result;
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.white);
		g2.fillRect(0, 0, getWidth(), getHeight());
		int FACTOR = Math.min(getWidth() / width, getHeight() / height);
		int offsetX = (getWidth() - width * FACTOR) / 2 - minX * FACTOR;
		int offsetY = (getHeight() - height * FACTOR) / 2 - minY * FACTOR;

		for (Map.Entry<Point, Character> space : spaces.entrySet()) {
			int x = space.getKey().x;
			int y = space.getKey().y;
			char c = space.getValue();
			int x1 = offsetX + FACTOR * x + FACTOR * 5 / 8;
			int y1 = offsetY + FACTOR * y + FACTOR * 5 / 8;
			switch (c) {
				case '.' -> {
					g2.setColor((x + y) % 2 == 0 ? Color.white : Color.lightGray);
					g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR);
				} case '|' -> {
					g2.setColor(Color.black);
					g2.fillRect(offsetX + FACTOR * x + FACTOR * 3 / 8, offsetY + FACTOR * y, FACTOR * 2 / 8, FACTOR);
				} case '-' -> {
					g2.setColor(Color.black);
					g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y + FACTOR * 3 / 8, FACTOR, FACTOR * 2 / 8);
				} case '#' -> {
					g2.setColor(Color.black);
					g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR);
				} case 'X' -> {
					g2.setColor(Color.orange);
					g2.fillRect(offsetX + FACTOR * x, offsetY + FACTOR * y, FACTOR, FACTOR);
				}
			}
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1216, 1239));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2018, 20));

		if (VISUAL) SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
