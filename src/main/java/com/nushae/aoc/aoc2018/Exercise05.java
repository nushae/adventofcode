package com.nushae.aoc.aoc2018;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
public class Exercise05 {

	private static final String ANNIHILATE = "aA|Aa|bB|Bb|cC|Cc|dD|Dd|eE|Ee|fF|Ff|gG|Gg|hH|Hh|iI|Ii|jJ|Jj|kK|Kk|lL|Ll|mM|Mm|nN|Nn|oO|Oo|pP|Pp|qQ|Qq|rR|Rr|sS|Ss|tT|Tt|uU|Uu|vV|Vv|wW|Ww|xX|Xx|yY|Yy|zZ|Zz";

	String polymer;

	public Exercise05(Path path) {
		try {
			polymer = Files.readAllLines(path).get(0);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public Exercise05(String data) {
		polymer = data;
	}

	public long doPart1() {
		return reactFully(polymer);
	}

	public long doPart2() {
		long bestLength = Long.MAX_VALUE;
		for (int i = 0; i < 26; i++) {
			bestLength = Math.min(bestLength, reactFully(polymer.replaceAll((char) ('a'+i) + "|" + (char) ('A' + i), "")));
		}
		return bestLength;
	}

	public long reactFully(String input) {
		long length = 0;
		while (length != input.length()) {
			length = input.length();
			input = input.replaceAll(ANNIHILATE, "");
		}
		return length;
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2018, 5));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
