package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 17, title="Chronospatial Computer", keywords = {"interpreter", "quine", "reverse-engineer"})
public class Exercise17 {

	List<String> lines;
	// 3-bit computer:
	long registerAOrig, registerA;
	long registerBOrig, registerB;
	long registerCOrig, registerC;
	List<Integer> programOrig, program;
	int iPointer;
	String outputRegister = "";

	public Exercise17() {
		lines = new ArrayList<>();
	}

	public Exercise17(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise17(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		registerAOrig = Long.parseLong(lines.get(0).substring("Register A: ".length()));
		registerBOrig = Long.parseLong(lines.get(1).substring("Register B: ".length()));
		registerCOrig = Long.parseLong(lines.get(2).substring("Register C: ".length()));
		programOrig = new ArrayList<>();
		for(String part : lines.get(4).substring("Program: ".length()).split(",")) {
			programOrig.add(Integer.parseInt(part));
		}
	}

	public String doPart1() {
		runProgram();
		return outputRegister;
	}

	public long runProgram() {
		iPointer = 0;
		outputRegister = "";
		registerA = registerAOrig;
		registerB = registerBOrig;
		registerC = registerCOrig;
		program = new ArrayList<>(programOrig);
		while (iPointer < program.size()) {
			int opcode = program.get(iPointer);
			iPointer++;
			if (iPointer >= program.size()) {
				return -1;
			}
			int operand = program.get(iPointer);
			switch (opcode) {
				case 0 -> {
					registerA = (long) (registerA / Math.pow(2, combo(operand)));
					iPointer++;
				}
				case 1 -> {
					registerB = registerB ^ operand;
					iPointer++;
				}
				case 2 -> {
					registerB = (combo(operand) % 8);
					iPointer++;
				}
				case 3 -> {
					if (registerA != 0) {
						iPointer = operand;
					}
				}
				case 4 -> {
					registerB = registerB ^ registerC;
					iPointer++;
				}
				case 5 -> {
					outPut((int) combo(operand) % 8);
					iPointer++;
				}
				case 6 -> {
					registerB = (long) (registerA / Math.pow(2, combo(operand)));
					iPointer++;
				}
				case 7 -> {
					registerC = (long) (registerA / Math.pow(2, combo(operand)));
					iPointer++;
				}
			}
		}
		return -1;
	}

	public long combo(int operand) {
		if (operand >= 0 && operand < 4) {
			return operand;
		} else if (operand == 4) {
			return registerA;
		} else if (operand == 5) {
			return registerB;
		} else if (operand == 6) {
			return registerC;
		}
		throw new IllegalArgumentException("7 should not occur in valid programs " + iPointer);
	}

	public void outPut(int value) {
		outputRegister += (!outputRegister.isEmpty() ? "," : "") + value;
	}

	// Some analysis for part 2.
	// You could just try every number from 0 upwards to find the correct A, but as we will see that takes QUITE a while.
	// so let's examine the program and see what we can see.
	// Let's translate the numbers to opcodes first:
	// 2,4,1,2,7,5,1,7,4,4,0,3,5,5,3,0  [Abel]
	//    op1^    op2^   ^ignored
	// 2,4,1,2,7,5,4,7,1,3,5,5,0,3,3,0  [mine]
    // 2,4,1,6,7,5,4,6,1,4,5,5,0,3,3,0  [Morris]
	//    op1^       ^   ^op2
	//           ignored
	//
 	// [   Mine  ]    [ Morris  ]    [  Abel   ]
	// BST 4   2,4    BST 4   2,4    BST 4   2,4
	// BXL 2   1,2    BXL 6   1,6    BXL 2   1,2
	// CDV 5   7,5    CDV 5   7,5    CDV 5   7,5
	// BXC 7   4,7    BXC 6   4,6    BXL 7   1,7   | these 2 can be done in any order
	// BXL 3   1,3    BXL 4   1,4    BXC 4   4,4   |
	// OUT 5   5,5    OUT 5   5,5    ADV 3   0,3
	// ADV 3   0,3    ADV 3   0,3    OUT 5   5,5
	// JNZ 0   3,0    JNZ 0   3,0    JNZ 0   3,0
	//
	// so:
	// registerA = INITIAL;
	// registerB = 0;
	// registerC = 0;
	// while (registerA != 0) {
	//   registerB = registerA % 8;                 B = last 3-bit chunk of A
	//   registerB = registerB xor 2 (=op1);        flip middle bit of B
	//   registerC = registerA / 2^registerB;       C = A minus its last B digits
	//   registerB = registerB xor registerC;       B = B xor C
	//   registerB = registerB xor 3 (=op2);        flip last 2 bits of B
	//   output(registerB % 8);                     OUT last 3 bits of B (this must be the desired number from the program)
	//   registerA = registerA / 2^3;               remove last 3-bit chunk from A
	// }
	//
 	// Since we must produce the program, we produce 16 outputs and A will be at least 16 3-bit chunks long.
	// That's not really bruteforceable. So let's try to construct A one chunk at a time.
	// Note that when producing the final output digit, 0, A will have only one chunk of 3 bits remaining, possibly with extra unused digits in front.
	// Let's call this chunk cba
	//
	// Working backwards:
	// output last 3 bits of B                       Must produce the desired output 0, so B must end in 000
	// flip last 2 bits of B                         So before the flip, B must have ended in 011
	// B = B xor C                                   This xor operation must result in B ending in 011.
	//
	// Let's put a pin in this, and work our way forward from the front:
	// B = chunk                                     B contains cba
	// flip middle B bit                             B now contains cBa (convention - capitals are flipped versions of lowercase letters)
	// C = A / 2^B                                   This removes cBa (0..7) bits from the end of A, and then A (and C) must end in three specific bits XYZ
	// B = B xor C                                   cBa xor C will only affect the last 3 bits of C, which we discovered must end up as 011. So C must start out ending in cBa xor 011 = cbA. In other words, XYZ = cbA
	//
	// Now putting everything together, this means:
	//
	// We need a value for cba so that if we remove the last cBa bits from A, A ends in cbA
	// this cbA value is dependent on the desired output being 000
	//
	// In general:
	// We need a value for [chunk] so that if we remove the last ([chunk] xor [op1]) bits from A, A ends in ([chunk] xor [op1]) xor ([desired output] xor [op2]).
	// xor is associative, say op3 = op1 xor op2 (== 1 in our case), so we can combine as follows:
	//
	// We need a value for [chunk] so that if we remove the last ([chunk] xor 2) bits from A, A ends in ([chunk] xor [desired output] xor 1).
	// --> Generalized: We need a value for [chunk] so that if we remove the last ([chunk] xor op1) bits from A, A ends in ([chunk] xor [desired output] xor op3).
	//
	// In a table: (Remember A = ...... [chunk], and output = 000)
	// chunk    chunk xor 2    digits to remove    then A must end in: chunk xor output xor 1 = chunk xor 1    Actual bits at A/2^(chunk xor 2)
	// -----    -----------    ----------------    --------------------------------------------------------    --------------------------------
	//   000            010                   2    001                                                         ..c == ..0 (no match no matter what we choose for ..)
	//   001            011                   3    000    <-------------------- MATCH -------------------->    ... == 000
	//   010            000                   0    011                                                         cba == 010
	//   011            001                   1    010                                                         .cb == .01 (no match no matter what we choose for .)
	//   100            110                   6    101    <-------------------- MATCH -------------------->    ... == 101
	//   101            111                   7    100    <-------------------- MATCH -------------------->    ... == 100
	//   110            100                   4    111    <-------------------- MATCH -------------------->    ... == 111
	//   111            101                   5    110    <-------------------- MATCH -------------------->    ... == 110
	// The dots represent nonexistent digits to the left of A, so these can be chosen freely (we can add leading bits to A after all)
	// Using only leading zeroes for now, we find that the first chunk must be 001.
	//
 	// Now we repeat with the newly constructed chunk added to A, for the next-to-last output:
	//
	// A = ...000 001 [chunk], output = 011
	// chunk    chunk xor 2    digits to remove    then A must end in: chunk xor output xor 1 = chunk xor 2    Actual bits at A/2^(chunk xor 2)
	// -----    -----------    ----------------    --------------------------------------------------------    --------------------------------
	//   000            010                   2    010    <-------------------- MATCH -------------------->    01c == 010
	//   001            011                   3    011                                                         001
	//   010            000                   0    000                                                         cba == 010
	//   011            001                   1    001                                                         1cb == 101
	//   100            110                   6    110                                                         000
	//   101            111                   7    111    <-------------------- MATCH -------------------->    ... == 111
	//   110            100                   4    100                                                         000
	//   111            101                   5    101                                                         000
	// We have two possible matches now; the first one needs no extra leading digits, and the second requires us to put 3 extra 1's in front of A
	// Sticking with only leading zeroes for now, we find that the second chunk must be 000.
	//
	// This process appears to work, but:
	// 1) sometimes we have multiple options for our chunk
	// 2) we may also find none of the 8 options work
	// Therefore we need to recurse so we can do simple backtracking.
	public long doPart2() {
		// It turns out that, for my input at least, you can start with 8 leading zeroes to find the solution.
		// But just in case...
		// Am I paranoid? You all think I am, don't you
		for (int leadingDigits = 0; leadingDigits < 256; leadingDigits++) {
			int op1 = -1, op2 = -1;
			int i = 0;
			while (op2 < 0 && i < programOrig.size()) {
				if (programOrig.get(i) == 1) { // BXL, extract operand
					if (op1 < 0) {
						op1 = programOrig.get(i + 1);
					} else {
						op2 = programOrig.get(i + 1);
					}
				}
				i+=2;
			}
			long result = findSolution(op1, op2, 0, programOrig);
			if (result > 0) {
				return result;
			}
		}
		return -1; // should never happen
	}

	public long findSolution(int op1, int op2, long aSoFar, List<Integer> prog) {
		List<Integer> remainingProg = new ArrayList<>(prog);
		if (remainingProg.isEmpty()) {
			return aSoFar;
		}
		int bMustEndWith = remainingProg.get(remainingProg.size() - 1) ^ op2;
		for (int block = 0; block < 8; block++) {
			int cBa = block ^ op1;
			int cMustEndWith = cBa ^ bMustEndWith;
			long tempA = 8 * aSoFar + block;
			int remainder = (int) ((tempA >> cBa) % 8);
			if (remainder == cMustEndWith) {
				int tmp = remainingProg.remove(remainingProg.size() - 1);
				long result = findSolution(op1, op2, tempA, remainingProg);
				remainingProg.add(tmp);
				if (result > 0) { // WORKS!
					return result;
				}
			}
		}
		return -1; // no solution in this branch
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17(aocPath(2024, 17));

		// Process input (1ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - "2,7,4,7,2,1,7,5,1" (<1ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 37221274271220 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
