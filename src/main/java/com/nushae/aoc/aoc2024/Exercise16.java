package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2024, day = 16, title="Reindeer Maze", keywords = {"grid", "shortest-path", "dfs", "pruning"})
public class Exercise16 {

	List<String> lines;
	CharGrid grid;
	Location start, end;
	Map<Pair<Location, Point>, Long> bestDistances;

	public Exercise16() {
		lines = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		grid = new CharGrid(lines);
		start = grid.findFirst('S').get();
		end = grid.findFirst('E').get();
		// Dead end passages can never be on the shortest path to the end, since they are not ON the path to the end.
		// So we can fill in all empty cells that have 3 orthogonal wall neighbours to speed things up. While this
		// makes the input processing slower, it speeds up the shortest path algorithm more.
		while (preprocess() > 0); // yay side effects :)
	}

	public long preprocess() {
		long result = 0;
		for (int i = 0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				if (grid.get(i, j) == '.') {
					int n = 0;
					for (Point dir : ORTHO_DIRS) {
						n += grid.get(i + dir.x, j + dir.y) == '#' ? 1 : 0;
					}
					if (n > 2) {
						grid.set(i , j, '#');
						result++;
					}
				}
			}
		}
		return result;
	}

	public long doPart1() {
		return shortestPath(start, end);
	}

	// DEPENDS ON PART 1 BEING RUN
	public long doPart2(long bestPath) {
		// use this line for the slow version... it's fun (well it will probably throw a stackoverflow)
		// return allBestPaths(start, EAST, Collections.emptySet(), 0L, bestPath).size();
		return allBestPathsReverse(bestPath).size();
	}

	public Map<Pair<Location, Point>, Integer> getNeighboursWeighted(Location loc, Point dir) {
		Map<Pair<Location, Point>, Integer> result = new HashMap<>();
		for (Point neigh : ORTHO_DIRS) {
			if (rot90L(2, dir).equals(neigh)) {
				continue;
			}
			int wt;
			if (dir.equals(neigh)) {
				wt = 1;
			} else {
				wt = 1001;
			}
			if (loc.add(neigh.x, neigh.y).inContext() && loc.resolve() != '#') {
				result.put(new Pair<>(loc.add(neigh.x, neigh.y), neigh), wt);
			}
		}
		return result;
	}

	public long shortestPath(Location start, Location end) {
		Set<Pair<Location, Point>> visited = new HashSet<>();
		bestDistances = new HashMap<>();
		Set<Pair<Location, Point>> queue = new HashSet<>();
		bestDistances.put(new Pair<>(start, EAST), 0L);
		queue.add(new Pair<>(start, EAST));
		while (!queue.isEmpty()) {
			Pair<Location, Point> minNode = queue
					.stream()
					.min((a, b) -> (int) Math.signum(bestDistances.getOrDefault(a, Long.MAX_VALUE) - bestDistances.getOrDefault(b, Long.MAX_VALUE)))
					.get();
			long minDist = bestDistances.get(minNode);
			if (minNode.getLeft().equals(end)) {
				return minDist;
			}
			visited.add(minNode);
			queue.remove(minNode);
			for (Map.Entry<Pair<Location, Point>, Integer> ne : getNeighboursWeighted(minNode.getLeft(), minNode.getRight()).entrySet()) {
				Pair<Location, Point> neighbour = ne.getKey();
				if (!visited.contains(neighbour)) {
					long val = ne.getValue();
					queue.add(neighbour);
					long tentative = minDist + val;
					if (tentative < bestDistances.getOrDefault(neighbour, Long.MAX_VALUE)) {
						bestDistances.put(neighbour, tentative);
					}
				}
			}
		}
		return -1;
	}

	// Preserved for posterity (and a laugh), my first approach, which works for the examples but is WAY too slow for the input
	// This basically does an exhaustive search for all paths to the goal, but prunes them whenever the cost exceeds the best path length.
	public Set<Location> allBestPaths(Location from, Point withDir, Set<Location> soFar, long scoreSoFar, long bestPath) {
		Set<Location> result = new HashSet<>();
		if (from.resolve() == 'E' && scoreSoFar <= bestPath) {
			result.add(from);
		} else {
			for (Point dir : ORTHO_DIRS) {
				if (rot90L(2, dir).equals(withDir)) {
					continue;
				}
				Location fromNew = from.add(dir.x, dir.y);
				if (fromNew.resolve() != '#') {
					int wt;
					if (dir.equals(withDir)) {
						wt = 1;
					} else {
						wt = 1001;
					}
					if (scoreSoFar + wt <= bestPath) {
						Set<Location> soFarNew = new HashSet<>(soFar);
						soFarNew.add(from);
						Set<Location> found = allBestPaths(fromNew, dir, soFarNew, scoreSoFar + wt, bestPath);
						if (!found.isEmpty()) {
							result.add(from);
							result.addAll(found);
						}
					}
				}
			}
		}
		return result;
	}

	// Because of how Dijkstra works, any cell on 'a' shortest path will always be visited during the dijkstra search...
	// This means the 'best distance' value for each visited location is usable as a pruning heuristic. Since the values
	// for locations near the start will all be low, and not really help with pruning, a much more effective approach is
	// to search backwards from the goal location.
	// It's laughable how much faster this approach is...
	public Set<Location> allBestPathsReverse(long bestPath) {
		Set<Pair<Location, Point>> result = new HashSet<>();
		Deque<Pair<Location, Point>> Q = new ArrayDeque<>();
		for (Point dir : ORTHO_DIRS) {
			Pair<Location, Point> pair = new Pair<>(end, dir);
			if (bestDistances.getOrDefault(pair, Long.MAX_VALUE) == bestPath) {
				result.add(pair);
				Q.add(pair);
			}
		}
		while(!Q.isEmpty()) {
			Pair<Location, Point> current = Q.poll();
			long currentScore = bestDistances.get(current);
			Point facing = current.getRight();
			for (Point prevDir : ORTHO_DIRS) {
				if (rot90L(2, prevDir).equals(facing)) {
					continue;
				}
				Location previous = current.getLeft().add(-facing.x, -facing.y);
				int wt;
				if (prevDir.equals(facing)) { // backwards, no turn
					wt = 1;
				} else { // backwards with turn
					wt = 1001;
				}
				if (currentScore - wt >= 0) {
					Pair<Location, Point>prevPair = new Pair<>(previous, prevDir);
					if (bestDistances.getOrDefault(prevPair, Long.MAX_VALUE) == currentScore - wt && !result.contains(prevPair)) {
						result.add(prevPair);
						Q.add(prevPair);
					}
				}
			}
		}
		Set<Location> onBestPaths = new HashSet<>();
		for (Pair<Location, Point> pair : result) {
			onBestPaths.add(pair.getLeft());
		}
		return onBestPaths;
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2024, 16));

		// Process input (with dead-end filling: 332ms, without: 6ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 94444 (with dead-end filling: 1.594s, without: 2.549s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		long bestPath = ex.doPart1();
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 502 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(bestPath));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
