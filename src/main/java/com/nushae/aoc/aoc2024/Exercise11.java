package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 11, title="Plutonian Pebbles", keywords = {"cellular-automaton"})
public class Exercise11 {

	List<String> lines;
	Map<Long, Long> stoneTally; // description says order doesn't change, but order isn't used -> just count the stones

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		stoneTally = new HashMap<>();
		for (String line : lines) {
			String[] parts = line.split(" ");
			for (String part : parts) {
				long num = Long.parseLong(part);
				stoneTally.put(num, stoneTally.getOrDefault(num, 0L) + 1);
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		return blinkLots(stoneTally, 25);
	}

	public long doPart2() {
		return blinkLots(stoneTally, 75);
	}

	public long blinkLots(Map<Long, Long> input, int times) {
		Map<Long, Long> result = new HashMap<>(input);
		while (times > 0) {
			result = blinkOnce(result);
			times--;
		}
		long accum = 0;
		for (long key : result.keySet()) {
			accum += result.get(key);// nope
		}
		return accum;
	}

	public Map<Long, Long> blinkOnce(Map<Long, Long> input) {
		Map<Long, Long> result = new HashMap<>();
		for (long stone : input.keySet()) {
			if (stone == 0L) {
				result.put(1L, result.getOrDefault(1L, 0L) + input.get(stone));
			} else if (String.valueOf(stone).length() % 2 == 0) {
				String stoneStr = String.valueOf(stone);
				long leftStone = Long.parseLong(stoneStr.substring(0, stoneStr.length() / 2));
				long rightStone = Long.parseLong(stoneStr.substring(stoneStr.length() / 2));
				result.put(leftStone, result.getOrDefault(leftStone, 0L) + input.get(stone));
				result.put(rightStone, result.getOrDefault(rightStone, 0L) + input.get(stone));
			} else {
				long newStone = stone * 2024;
				result.put(newStone, result.getOrDefault(newStone, 0L) + input.get(stone));
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2024, 11));

		// Process input (2ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 191690 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 228651922369703 (54ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
