package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 3, title="Mull It Over", keywords = {"regex"})
public class Exercise03 {

	private static final Pattern INST = Pattern.compile("(mul)\\((\\d{1,3}+),(\\d{1,3})\\)|(do)\\(\\)|(don't)\\(\\)");

	private record Instruction(String name, long left, long right, boolean doOrDont) {
		Instruction(String name, long left, long right) {
			this(name, left, right, false); // boolean value is irrelevant
		}
		Instruction(String name, boolean doOrDont) {
			this(name, 0, 0, doOrDont);
		}
	}

	private List<String> lines;
	private List<Instruction> instructions;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		instructions = new ArrayList<>();
		for (String line : lines) {
			Matcher m = INST.matcher(line);
			while (m.find()) {
				if ("mul".equals(m.group(1))) {
					instructions.add(new Instruction(m.group(1), Long.parseLong(m.group(2)), Long.parseLong(m.group(3))));
				} else {
					instructions.add(new Instruction("switch", "do".equals(m.group(4))));
				}
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (Instruction inst : instructions) {
			if ("mul".equals(inst.name)) {
				accum += inst.left * inst.right;
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		boolean process = true;
		for (Instruction inst : instructions) {
			if ("mul".equals(inst.name) && process) {
				accum += inst.left * inst.right;
			} else {
				process = inst.doOrDont;
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2024, 3));

		// Process input
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 160672468 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 84893551 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
