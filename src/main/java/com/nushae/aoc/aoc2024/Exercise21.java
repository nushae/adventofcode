package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.domain.Pair;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static java.lang.Math.abs;

@Log4j2
@Aoc(year = 2024, day = 21, title="Keypad Conundrum", keywords = {"recursion", "shortest-path", "memoization"})
public class Exercise21 {

	List<String> lines;
	Map<String, Long> memoize;
	Map<String, BigInteger> memoizeExtreme;
	CharGrid dirKeys = new CharGrid(".^A\n<v>");
	CharGrid numKeys = new CharGrid("789\n456\n123\n.0A");
	@Setter
	boolean universal = false;

	public Exercise21() {
		lines = new ArrayList<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void setDirKeys(String layout) {
		dirKeys = new CharGrid(layout);
	}

	public void setNumKeys(String layout) {
		numKeys = new CharGrid(layout);
	}

	public long doPart1() {
		memoize = new HashMap<>();
		return processCodes(4);
	}

	// good for up to 38 keypads
	public long doPart2() {
		memoize = new HashMap<>();
		return processCodes(27);
	}

	long processCodes(int totalKeypads) {
		long result = 0;
		for (String code : lines) {
			long pathLength = findNextSequenceToPress(code, totalKeypads, true);
			long numerical = Long.parseLong(code.substring(0, code.length() - 1));
			log.debug(code + ": " + pathLength + " * " + numerical);
			result += pathLength * numerical;
		}
		return result;
	}

	// I realised immediately that the best paths across the keypads were those that grouped horizontal and vertical
	// movement together, since that meant less movement by the robot arm and thus fewer key presses on the outer keyboards.
	// However, using this, I kept getting too low answers for the examples... I even checked one by hand and I still
	// got a lower answer. "I must have missed something in the description..." checked the puzzle again, carefully,
	// and there it was: "In particular, if a robot arm is ever aimed at a gap where no button is present on the keypad,
	// even for an instant, the robot will panic unrecoverably."
	// I thought I could simply always take the path that avoids the empty space (on the numPad that would be
	// "vertical, then horizontal", while on the dirPad it would be "horizontal, then vertical") but this too gave me
	// (some) incorrect answers.
	// A little research revealed that the two paths along the edge of the rectangle do NOT always give the same score!
	// conclusion: we must try both options when they are both possible...
	// After submission, I added a more generic approach that would work with any keyboard, hence the introduction of the
	// 'universal' boolean flag
	public long findNextSequenceToPress(String keyPresses, int keypadId, boolean innerMostRobot) {
		if (keypadId == 1) {
			return keyPresses.length();
		}
		long result = 0;
		Location start = (innerMostRobot ? numKeys : dirKeys).findFirst('A').get();
		for (int i = 0; i < keyPresses.length(); i++) {
			Location end = (innerMostRobot ? numKeys : dirKeys).findFirst(keyPresses.charAt(i)).get();
			result += universal ? findBestPathOnDirPadUniversal(start, end, keypadId) : findBestPathOnDirPad(start, end, keypadId, innerMostRobot);
			start = end;
		}
		return result;
	}

	// The following simplification leverages the fact that the given keypads have the empty spot in a corner.
	// This means we never need to consider a U shaped path; we only go in (stepped) diagonals. Given that,
	// it is best to alternate between directions as little as possible: a sequence of >>^ will be better than >^>
	// because the latter involves more movement of the next outer robot arm. The former will yield vAA<^A>A (8) while
	// the latter results in vA<^a>vA^A (10).
	//
	// So we want to do all the horizontal movement, then all the vertical movement, or vice versa.
	//
	// Formally, we only need to consider the two paths that run along the edge of the rectangle between start and end.
	// Additionally, if one of these two paths passes over the empty location it must be ignored. We can leverage the
	// fact that the empty spot is always in the corner of the keyboard. This means a lot of paths automatically cannot
	// pass over the empty spot, and those that do, will have it at the point where the path turns a corner.
	// The x-coordinate of the corner that might be empty is always min(start.x, end.x),
	// while the y-coordinate is either min(start.y, end.y) (dirPad) or max(start.y, end.y) (numPad).
	//
	// We now have three cases:
	// 1) (dx == 0 || dy == 0) purely vertical or horizontal movement: never passes over empty spot, always one option - hor + ver + "A"
	// 2) [(min(start.x, end.x), numPad ? max(start.y, end.y) : min(start.x, end.x)] is empty: one option, namely
	//    dx < 0 ? ver + hor + "A" : hor + ver + "A", which avoids the empty spot.
	// 3) else: two options, ver + hor + "A" and hor + ver + "A", neither of which visit the empty spot.
	public long findBestPathOnDirPad(Location start, Location end, int keypadId, boolean forNumpad) {
		String k = makeCacheKey(start, end, keypadId);
		if (memoize.containsKey(k)) {
			return memoize.get(k);
		}
		int dx = end.getX() - start.getX();
		int dy = end.getY() - start.getY();
		long dist;
		String verticalPart = StringUtils.repeat(dy < 0 ? "^" : dy > 0 ? "v" : "", abs(dy));
		String horizontalPart = StringUtils.repeat(dx < 0 ? "<" : dx > 0 ? ">" : "", abs(dx));
		if (Math.min(end.getX(), start.getX()) == 0 && (forNumpad ? Math.max(end.getY(), start.getY()) : Math.min(end.getY(), start.getY())) == (forNumpad ? 3 : 0)) {
			dist = findNextSequenceToPress((dx < 0) ? verticalPart + horizontalPart + "A" : horizontalPart + verticalPart + "A", keypadId - 1, false);
		} else {
			dist = findNextSequenceToPress(horizontalPart + verticalPart + "A", keypadId - 1, false);
			if (dx != 0 && dy != 0) {
				dist = Math.min(dist, findNextSequenceToPress(verticalPart + horizontalPart + "A", keypadId - 1, false));
			}
		}
		memoize.put(k, dist);
		return dist;
	}

	// Created after submission - say we have a really weird keyboard that doesn't allow us to leverage the location of
	// the empty spot... for instance because it is in the center... now we also have to consider U-shaped paths.
	// We now need a more generic dfs-with-avoid-empty-locations approach
	public long findBestPathOnDirPadUniversal(Location loc1, Location loc2, int keypadId) {
		String k = makeCacheKey(loc1, loc2, keypadId);
		if (memoize.containsKey(k)) {
			return memoize.get(k);
		}
		Deque<Pair<Location, String>> Q = new ArrayDeque<>();
		Q.add(new Pair<>(loc1, ""));
		long result = Long.MAX_VALUE;
		while (!Q.isEmpty()) {
			Pair<Location, String> current = Q.poll();
			Location currentLoc = current.getLeft();
			String currentPath = current.getRight();
			if (currentLoc.equals(loc2)) {
				long dist = findNextSequenceToPress(currentPath + "A", keypadId - 1, false);
				result = Math.min(result, dist);
				continue;
			}
			if (currentLoc.resolve() == '.') { // avoid this space at all cost
				continue;
			}
			if (currentLoc.getX() > loc2.getX()) {
				Q.add(new Pair<>(currentLoc.add(-1, 0), currentPath + "<"));
			} else if (current.getLeft().getX() < loc2.getX()) {
				Q.add(new Pair<>(currentLoc.add(1, 0), currentPath + ">"));
			}
			if (currentLoc.getY() > loc2.getY()) {
				Q.add(new Pair<>(currentLoc.add(0, -1), currentPath + "^"));
			} else if (current.getLeft().getY() < loc2.getY()) {
				Q.add(new Pair<>(currentLoc.add(0, 1), currentPath + "v"));
			}
		}
		memoize.put(k, result);
		return result;
	}

	String makeCacheKey(Location l1, Location l2, int r) {
		return r + l1.toString() + l2.toString();
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2024, 21));

		ex.setNumKeys("789\n456\n123\n.0A");
		ex.setDirKeys(".^A\n<v>");
		ex.setUniversal(false);
		// to test the more generic approach, use the following alternative
		// ex.setNumKeys("789\n5.6\n234\n01A");
		// ex.setDirKeys("^.A\n<v>");
		// ex.setUniversal(true);

		// Process input not needed

		// part 1 - 224326 (12ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 279638326609472 (11ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}

	// use instead of doPart2() when you need more than 38 keypads (you madman)
	public BigInteger doPart2Extreme(int keypads) {
		memoizeExtreme = new HashMap<>();
		return processCodesExtreme(keypads);
	}
	BigInteger processCodesExtreme(int totalKeypads) {
		BigInteger result = BigInteger.ZERO;
		for (String code : lines) {
			BigInteger pathLength = findNextSequenceToPressExtreme(code, totalKeypads, true);
			long numerical = Long.parseLong(code.substring(0, code.length() - 1));
			log.debug(code + ": " + pathLength + " * " + numerical);
			result = result.add(pathLength.multiply(BigInteger.valueOf(numerical)));
		}
		return result;
	}
	public BigInteger findNextSequenceToPressExtreme(String keyPresses, int keypadId, boolean isHuman) {
		if (keypadId == 1) {
			return BigInteger.valueOf(keyPresses.length());
		}
		BigInteger result = BigInteger.ZERO;
		Location start = (isHuman ? numKeys : dirKeys).findFirst('A').get();
		for (int i = 0; i < keyPresses.length(); i++) {
			Location end = (isHuman ? numKeys : dirKeys).findFirst(keyPresses.charAt(i)).get();
			result = result.add(findBestPathOnDirPadExtreme(start, end, keypadId, isHuman));
			start = end;
		}
		return result;
	}
	public BigInteger findBestPathOnDirPadExtreme(Location start, Location end, int keypadId, boolean forNumpad) {
		String k = makeCacheKey(start, end, keypadId);
		if (memoizeExtreme.containsKey(k)) {
			return memoizeExtreme.get(k);
		}
		int dx = end.getX() - start.getX();
		int dy = end.getY() - start.getY();
		BigInteger dist;
		String verticalPart = StringUtils.repeat(dy < 0 ? "^" : dy > 0 ? "v" : "", abs(dy));
		String horizontalPart = StringUtils.repeat(dx < 0 ? "<" : dx > 0 ? ">" : "", abs(dx));
		if (Math.min(end.getX(), start.getX()) == 0 && (forNumpad ? Math.max(end.getY(), start.getY()) : Math.min(end.getY(), start.getY())) == (forNumpad ? 3 : 0)) {
			dist = findNextSequenceToPressExtreme((dx < 0) ? verticalPart + horizontalPart + "A" : horizontalPart + verticalPart + "A", keypadId - 1, false);
		} else {
			dist = findNextSequenceToPressExtreme(horizontalPart + verticalPart + "A", keypadId - 1, false);
			if (dx != 0 && dy != 0) {
				dist = dist.min(findNextSequenceToPressExtreme(verticalPart + horizontalPart + "A", keypadId - 1, false));
			}
		}
		memoizeExtreme.put(k, dist);
		return dist;
	}
}
