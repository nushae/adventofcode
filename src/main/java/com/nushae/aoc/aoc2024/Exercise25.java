package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 25, title="Code Chronicle", keywords = {"grid", "pattern-matching", "visualized"})
public class Exercise25 extends JPanel {

	public static final boolean VISUAL = true; // maybe don't do this with the actual input, but use a smaller input instead

	public static final Color FILLED_EDGE = Color.black;
	public static final Color FILLED_KEY_OK = new Color(128, 196, 128);
	public static final Color FILLED_LOCK_OK = new Color(0, 128, 0);
	public static final Color FILLED_KEY_NOK = Color.red;
	public static final Color FILLED_LOCK_NOK = new Color(255, 128, 128);
	public static final Color FILLED_BOTH_NOK = new Color(160, 0, 0);
	public static final Color EMPTY = new Color(225, 225, 225);
	public static final int MARGIN = 1;

	List<String> lines;
	List<int[]> locks, keys;
	Set<Point> validCombos;
	int blockWidth;
	int blockHeight;

	public Exercise25() {
		lines = new ArrayList<>();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise25(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		if (!lines.get(lines.size() - 1).isEmpty()) {
			lines.add("");
		}
		locks = new ArrayList<>();
		keys = new ArrayList<>();
		List<String> current = new ArrayList<>();
		for (String line : lines) {
			if (line.isEmpty()) { // save and reset
				CharGrid grid = new CharGrid(current);
				blockHeight = grid.getHeight();
				blockWidth = grid.getWidth();
				current = new ArrayList<>();
				int[] asInts = toColHeight(grid);
				if (StringUtils.repeat('#', blockWidth).equals(grid.getRow(0))) { // top row filled -> key
					locks.add(asInts);
				} else {
					keys.add(asInts);
				}
				continue;
			}
			current.add(line);
		}
		log.debug("Total keys: " + keys.size() + ". Total locks: " + locks.size());
		log.debug("Block WxH is: " + blockWidth + "x" + blockHeight);
	}

	public int[] toColHeight(CharGrid grid) {
		int[] result = new int[grid.getWidth()];
		for (int c = 0; c < grid.getWidth(); c++) {
			int count = 0;
			for (int r = 0; r < grid.getHeight(); r++) {
				count += grid.get(c, r) == '#' ? 1 : 0;
			}
			result[c] = count;
		}
		return result;
	}

	public long doPart1() {
		validCombos = new HashSet<>(); // used for visualization
		for (int lockIdx = 0; lockIdx < locks.size(); lockIdx++){
			int[] lock = locks.get(lockIdx);
			for (int keyIdx = 0; keyIdx < keys.size(); keyIdx++){
				int[] key = keys.get(keyIdx);
				boolean ok = true;
				int col = 0;
				for (; ok && col < blockWidth; col++) {
					ok = lock[col] + key[col] <= blockHeight;
				}
				if (ok) {
					validCombos.add(new Point(lockIdx, keyIdx));
				}
			}
		}
		return validCombos.size();
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		int pixelSize = getBestPixelSize();
		for (int lock = 0; lock < locks.size(); lock++) {
			paintLock(g, pixelSize, lock);
		}
		for (int key = 0; key < keys.size(); key++) {
			paintKey(g, pixelSize, key);
		}
		for (int lock = 0; lock < locks.size(); lock++) {
			for (int key = 0; key < keys.size(); key++) {
				paintLockKey(g, pixelSize, lock, key);
			}
		}
	}

	// Calculate largest pixelSize at which we can show everything with a gap of 2 pixels between blocks
	// EDGE + MARGIN + BLOCK + MARGIN + BLOCK + ... + MARGIN + BLOCK = (MARGIN + BLOCK) * blocks + EDGE
	public int getBestPixelSize() {
		Insets insets = this.getRootPane().getParent().getInsets();
		log.debug(insets);
		int lostWidth = insets.left + insets.right;
		int lostHeight = insets.top + insets.bottom;
		int idealPixelSizeHor = getWidth() / (keys.size() * (blockWidth + MARGIN) + blockWidth);
		int idealPixelSizeVer = getHeight() / (locks.size() * (blockHeight + MARGIN) + blockHeight);
		int pixelSize = Math.max(1, Math.min(idealPixelSizeHor, idealPixelSizeVer));
		log.debug(String.format("View width x height: %d x %d (window width x height: %d x %d)", getWidth(), getHeight(), getRootPane().getParent().getWidth(), getRootPane().getParent().getHeight()));
		log.debug(String.format("We have %d pixels available horizontally per block, with block-width %d, so largest pixel width would be %d. Need window width %d or greater for pixelSize %d.",  (getWidth() - keys.size() * 2) / (keys.size() + 1), blockWidth, idealPixelSizeHor, lostWidth + pixelSize * (keys.size() * (blockWidth + MARGIN) + blockWidth), pixelSize));
		log.debug(String.format("We have %d pixels available vertically per block, with block-height %d, so largest pixel height would be %d. Need window height %d or greater for pixelSize %d.",  (getHeight() - locks.size() * 2) / (locks.size() + 1), blockHeight, idealPixelSizeVer, lostHeight + pixelSize * (locks.size() * (blockHeight + MARGIN) + blockHeight), pixelSize));
		return pixelSize;
	}

	public void paintLock(Graphics g, int pixelSize, int lock) {
		int[] lockHeights = locks.get(lock);
		for (int x = 0; x < blockWidth; x++) {
			for (int y = 0; y < blockHeight; y++) {
				if (y < lockHeights[x]) {
					g.setColor(FILLED_EDGE);
				} else {
					g.setColor(EMPTY);
				}
				g.fillRect(x * pixelSize, ((lock + 1) * (blockHeight + MARGIN) + y) * pixelSize, pixelSize, pixelSize);
			}
		}
	}

	public void paintKey(Graphics g, int pixelSize, int key) {
		int[] keyHeights = keys.get(key);
		for (int x = 0; x < blockWidth; x++) {
			for (int y = 0; y < blockHeight; y++) {
				if (blockHeight - y - 1 < keyHeights[x]) {
					g.setColor(FILLED_EDGE);
				} else {
					g.setColor(EMPTY);
				}
				g.fillRect(((key + 1) * (blockWidth + MARGIN) + x) * pixelSize, y * pixelSize, pixelSize, pixelSize);
			}
		}
	}

	public void paintLockKey(Graphics g, int pixelSize, int lock, int key) {
		int[] lockHeights = locks.get(lock);
		int[] keyHeights = keys.get(key);
		for (int x = 0; x < blockWidth; x++) {
			for (int y = 0; y < blockHeight; y++) {
				if (lockHeights[x] > y && keyHeights[x] > blockHeight - y - 1) {
					g.setColor(FILLED_BOTH_NOK);
				} else {
					if (lockHeights[x] > y) {
						g.setColor(validCombos.contains(new Point(lock, key)) ? FILLED_LOCK_OK : FILLED_LOCK_NOK);
					} else if (keyHeights[x] > blockHeight - y - 1) {
						g.setColor(validCombos.contains(new Point(lock, key)) ? FILLED_KEY_OK : FILLED_KEY_NOK);
					} else {
						g.setColor(EMPTY);
					}
				}
				g.fillRect(((key + 1) * (blockWidth + MARGIN) + x) * pixelSize, ((lock + 1) * (blockHeight + MARGIN) + y) * pixelSize, pixelSize, pixelSize);
			}
		}
	}

	private static void createAndShowGUI(JPanel panel) {
		final JFrame frame = new JFrame();
		// frame.setSize(new Dimension(1868 + 6, 1468 + 29)); // adjusted for insets on my system / input_for_vis_small.txt. Ymmv!
		// frame.setSize(new Dimension(1610 + 6, 1610 + 29)); // adjusted for insets on my system / tiny_input.txt. Ymmv!
		frame.setSize(new Dimension(1505 + 6, 2007 + 29)); // adjusted for insets on my system / input.txt. Ymmv!
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.add(panel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true); // insets are now nonzero
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2024, 25));
		// Exercise25 ex = new Exercise25(aocPath(2024, 25, "input_for_vis_small.txt"));
		// Exercise25 ex = new Exercise25(aocPath(2024, 25, "tiny_input.txt"));

		// Process input (13ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 3127 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// as is tradition, there is no part 2
		// Merry Christmas!!

		if (VISUAL) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
		}
	}
}
