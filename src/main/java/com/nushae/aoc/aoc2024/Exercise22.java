package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 22, title="Monkey Market", keywords = {"pseudorandom-sequence", "sliding-window", "maximum"})
public class Exercise22 {

	private record DeltaWindow(int delta1, int delta2, int delta3, int delta4) {
		public static DeltaWindow from(List<Integer> deltas) {
			return new DeltaWindow(deltas.get(0), deltas.get(1), deltas.get(2), deltas.get(3));
		}
	}

	List<String> lines;
	List<Long> secretNumbers;
	Map<DeltaWindow, Integer> totalBestPricesPerWindow; // directly using List<Integer> as key doesn't work :(
	Map<Integer, List<Integer>> pricesPerBuyer;

	public Exercise22() {
		lines = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		secretNumbers = new ArrayList<>();
		for (String line : lines) {
			secretNumbers.add(Long.parseLong(line));
		}
	}

	// also generates necessary data for part2!!
	public long doPart1() {
		long accum = 0;
		pricesPerBuyer = new HashMap<>();
		totalBestPricesPerWindow = new HashMap<>();
		for (int buyer = 0; buyer < secretNumbers.size(); buyer++) {
			Set<DeltaWindow> firstOccurrenceForThisBuyer = new HashSet<>(); // ONLY the first occurrence of a change window is ever used, dummy! Reading skillz yo
			List<Integer> deltaWindow = new ArrayList<>();
			List<Integer> prices = new ArrayList<>();
			long current = secretNumbers.get(buyer);
			for (int i = 0; i < 2000; i++) {
				long previous = current;
				current = evolve(current);
				int currentPrice = (int) (current % 10);
				prices.add(currentPrice);
				deltaWindow.add(currentPrice - (int) (previous % 10));
				if (deltaWindow.size() > 4) {
					deltaWindow.remove(0);
					DeltaWindow window = DeltaWindow.from(deltaWindow);
					if (!firstOccurrenceForThisBuyer.contains(window)) {
						totalBestPricesPerWindow.put(window, totalBestPricesPerWindow.getOrDefault(window, 0) + currentPrice);
						firstOccurrenceForThisBuyer.add(window);
					}
				}
			}
			pricesPerBuyer.put(buyer, prices);
			accum += current;
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (int total : totalBestPricesPerWindow.values()) {
			accum = Math.max(accum, total);
		}
		return accum;
	}

	public static long evolve(long s) {
		s = mix(s, s * 64);
		s = prune(s);

		s = mix(s, s / 32);
		s = prune(s);

		s = mix(s, s * 2048);
		s = prune(s);

		return s;
	}

	public static long mix(long num1, long num2) {
		return num1 ^ num2;
	}

	public static long prune(long num) {
		return num % 16777216;
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(aocPath(2024, 22));

		// Process input (3ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 19847565303 (957ms, includes most of part 2)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2250 (6ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
