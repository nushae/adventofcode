package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 19, title="Linen Layout", keywords = {"patterns", "memoization"})
public class Exercise19 {

	List<String> lines;
	String[] available;
	List<String> desired;
	Map<String, Long> memoize;

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		available = lines.get(0).split(", ");
		desired = new ArrayList<>();
		for (int i = 2; i < lines.size(); i++) {
			desired.add(lines.get(i));
		}
		memoize = new HashMap<>();
	}

	public long doPart1() {
		long accum = 0;
		for (String towel : desired) {
			accum += countPossibilities(towel) > 0 ? 1 : 0;
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (String towel : desired) {
			accum += countPossibilities(towel);
		}
		return accum;
	}

	public long countPossibilities(String towel) {
		if (memoize.containsKey(towel)) {
			return memoize.get(towel);
		}
		long result = 0;
		for (String part : available) {
			if (towel.equals(part)) {
				result++;
			} else if (towel.startsWith(part)) {
				result += countPossibilities(towel.substring(part.length()));
			}
		}
		memoize.put(towel, result);
		return result;
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2024, 19));

		// Process input
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
