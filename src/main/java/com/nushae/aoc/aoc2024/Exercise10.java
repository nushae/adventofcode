package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.SearchUtil.dfsAllGoalsWithPathCounts;

@Log4j2
@Aoc(year = 2024, day = 10, title="Hoof It", keywords = {"grid", "dfs"})
public class Exercise10 {

	List<String> lines;
	CharGrid grid;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		grid = new CharGrid(lines);
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (int i = 0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				if (grid.get(i, j) == '0') {
					accum += dfsAllGoalsWithPathCounts(new Location(i, j, grid), (Location a) -> a.resolve() == '9').getLeft().size();
				}
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (int i = 0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				if (grid.get(i, j) == '0') {
					Pair<Set<Location>, Map<Location, Long>> result = dfsAllGoalsWithPathCounts(new Location(i, j, grid), (Location a) -> a.resolve() == '9');
					for (Location loc : result.getLeft()) {
						accum += result.getRight().get(loc);
					}
				}
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2024, 10));

		// Process input (5ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 825 (223ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1805 (7ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
