package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.view.SymbolGridPanel;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 8, title="Resonant Collinearity", keywords = {"grid", "line-algorithm", "visualized", "interactive"})
public class Exercise08 extends MouseAdapter {

	private static final boolean VISUAL = true;

	List<String> lines;
	Map<Character, List<Location>> antennas;
	CharGrid grid;
	SymbolGridPanel displayPanel;

	public Exercise08() {
		lines = new ArrayList<>();
		antennas = new HashMap<>();
		grid = new CharGrid(50, 50, '.');
		displayPanel = new SymbolGridPanel(grid, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
		displayPanel.addMouseListener(this);
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		antennas = new HashMap<>();
		grid = new CharGrid(lines, '.');
		for (int i=0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				if (grid.get(i, j) != '.') {
					antennas.computeIfAbsent(grid.get(i, j), a -> new ArrayList<>()).add(new Location(i, j, grid));
				}
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		return calculate(false).size();
	}

	public long doPart2() {
		return calculate(true).size();
	}

	public Set<Location> calculate(boolean part2) {
		Set<Location> antinodes = new HashSet<>();
		for (Character c : antennas.keySet()) {
			List<Location> ants = antennas.get(c);
			if (ants.size() > 1) {
				for (int ci1 = 0; ci1 < ants.size()-1; ci1++) {
					for (int ci2 = ci1 + 1; ci2 < ants.size(); ci2++) {
						Location newPoint1 = ants.get(ci1);
						Location newPoint2 = ants.get(ci2);
						int dx = newPoint1.getX() - newPoint2.getX();
						int dy = newPoint1.getY() - newPoint2.getY();
						if (part2) {
							antinodes.add(newPoint1);
							antinodes.add(newPoint2);
						}
						do {
							newPoint1 = newPoint1.add(dx, dy);
							if (newPoint1.inContext()) {
								antinodes.add(newPoint1);
							}
						} while (part2 && newPoint1.inContext());
						do {
							newPoint2 = newPoint2.add(-dx, -dy);
							if (newPoint2.inContext()) {
								antinodes.add(newPoint2);
							}
						} while (part2 && newPoint2.inContext());
					}
				}
			}
		}
		return antinodes;
	}

	private static void createAndShowGUI(JPanel displayPanel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1000));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.add(displayPanel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 1) {
			// left click:
			Location click = displayPanel.pixelToLocation(e.getX(), e.getY());
			if (click != null) { // clicked in grid
				if (click.resolve() != grid.getEmpty()) { // clicked on antenna -> clear cell
					antennas.get(click.resolve()).remove(click);
					click.set(grid.getEmpty());
				}
				if (displayPanel.getActiveSymbol() != 0) { // place new antenna symbol, if any
					click.set(displayPanel.getActiveSymbol());
					antennas.computeIfAbsent(click.resolve(), k -> new ArrayList<>()).add(click);
				}
				displayPanel.setHighlights(calculate(true));
			} else {
				char c = displayPanel.pixelToAlphabet(e.getX(), e.getY());
				if (c == displayPanel.getActiveSymbol()) {
					displayPanel.setActiveSymbol((char) 0);
				} else if (c != 0) {
					displayPanel.setActiveSymbol(c);
				}
				displayPanel.refresh();
			}
		} else if (e.getButton() == MouseEvent.BUTTON3 && e.getClickCount() == 1) {
			Location click = displayPanel.pixelToLocation(e.getX(), e.getY());
			if (click != null) {
				if (click.resolve() != grid.getEmpty()) {
					antennas.get(click.resolve()).remove(click);
					click.set(grid.getEmpty());
				}
			}
			displayPanel.setHighlights(calculate(true));
		}
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2024, 8));

		// Process input (5ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 409 (<1ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1308 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		if (VISUAL) {
			ex.displayPanel.setGrid(ex.grid);
			ex.displayPanel.setHighlights(ex.calculate(true));

			SwingUtilities.invokeLater(() -> createAndShowGUI(ex.displayPanel));
		}


	}
}
