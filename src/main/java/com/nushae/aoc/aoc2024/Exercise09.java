package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 9, title="Disk Fragmenter", keywords = {"list-processing", "checksum"})
public class Exercise09 {

	private List<String> lines;
	private List<Pair<Long, Long>> numbers; // id, length; with id == -1 -> empty space

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		numbers = new ArrayList<>();
		long idx = 0;
		for (String line : lines) {
			String[] parts = line.split("");
			for (String part : parts) {
				numbers.add(new Pair<>((idx % 2 == 0) ? idx / 2 : -1, Long.parseLong(part)));
				idx++;
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		return checksum(defrag(numbers));
	}

	public long doPart2() {
		return checksum(defragWhole(numbers));
	}

	private List<Pair<Long, Long>> defrag(List<Pair<Long, Long>> input) {
		List<Pair<Long, Long>> result = new ArrayList<>();
		int leftIdx = 1;
		result.add(input.get(0));
		long space = input.get(leftIdx).getRight();
		for (int rightIdx = input.size() - 1; rightIdx - 1 > leftIdx;
			 rightIdx -= 2) {
			long numToMove = input.get(rightIdx).getRight();
			long numId = input.get(rightIdx).getLeft();
			while (numToMove > 0) {
				if (space > numToMove) { // fits in current block of space, with leftovers
					space -= numToMove;
					result.add(new Pair<>(numId, numToMove));
					numToMove = 0;
				} else { // space <= numToMove -> uses up current block of space, with possible leftovers
					if (space > 0) { // space left in current position
						result.add(new Pair<>(numId, space));
						numToMove -= space;
					}
					if (leftIdx < rightIdx - 1) {
						result.add(input.get(++leftIdx));
						space = input.get(++leftIdx).getRight();
					} else { // add remainder
						result.add(new Pair<>(numId, numToMove));
						break;
					}
				}
			}
		}
		return result;
	}

	private List<Pair<Long, Long>> defragWhole(List<Pair<Long, Long>> input) {
		List<Pair<Long, Long>> result = new ArrayList<>(input);
		int idx = input.size() - 1;
		long fileId = input.get(idx).getLeft();
		while (fileId > 0) {
			result = attemptToMove(result, idx);
			fileId--;
			while (result.get(idx).getLeft() != fileId) {
				idx--;
			}
		}
		return result;
	}

	private List<Pair<Long, Long>> attemptToMove(List<Pair<Long, Long>> input, int idx) {
		List<Pair<Long, Long>> result = new ArrayList<>();
		boolean moved = false;
		Pair<Long, Long> toMove = input.get(idx);

		for (int i = 0; i < idx; i++) {
			Pair<Long, Long> current = input.get(i);
			if (!moved && current.getLeft() == -1 && current.getRight() >= toMove.getRight()) {
				moved = true;
				result.add(toMove);
				long leftOverSpace = current.getRight() - toMove.getRight();
				if (leftOverSpace > 0) {
					result.add(new Pair<>(-1L, leftOverSpace));
				}
			} else {
				result.add(current);
			}
		}

		if (moved) {
			result.add(new Pair<>(-1L, toMove.getRight()));
		} else {
			result.add(toMove);
		}

		for (int i = idx + 1; i < input.size(); i++) {
			result.add(input.get(i));
		}

		return result;
	}

	private long checksum(List<Pair<Long, Long>> input) {
		long curPos = 0;
		long accum = 0;
		for (Pair<Long, Long> num : input) {
			if (num.getLeft() > 0) {
				accum += num.getLeft() * calc(curPos, num.getRight());
			}
			curPos += num.getRight();
		}
		return accum;
	}

	private long calc(long start, long length) {
		return start * length + length * (length - 1) / 2;
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2024, 9));

		// Process input (18ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 6384282079460 (4ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 6408966547049 (1.14s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}