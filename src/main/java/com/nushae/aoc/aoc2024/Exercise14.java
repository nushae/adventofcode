package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.GraphicsUtil.DEFAULT_FONT;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 14, title="Restroom Redoubt", keywords = {"grid", "extrapolation", "find-frame", "visualized", "animated"})
public class Exercise14 extends JPanel {

	private static final Pattern ROBOT = Pattern.compile("p=(\\d+),(\\d+) v=(-?\\d+),(-?\\d+)");
	private static final boolean ANIMATE = true;
	private static final int[] SPEEDS = new int[] {1024, 512, 256, 128, 64, 32};
	private static final int MARGIN = 4; // margin in cells around the grid in visualization

	private static final boolean PRINTFRAME = false;

	static class Robot {
		int x, y, vx, vy;
		public Robot(int x, int y, int vx, int vy) {
			this.x = x;
			this.y = y;
			this.vx = vx;
			this.vy = vy;
		}

		public Point predictNewPositionPeriodic(int steps, int width, int height) {
			return new Point(((x + steps * vx) % width + width) % width, ((y + steps* vy) % height + height) % height);
		}

		public void moveOneStepPeriodic(int width, int height) {
			x = ((x + vx) % width + width) % width;
			y = ((y + vy) % height + height) % height;
		}

		@Override
		public boolean equals(Object o) {
			if (o == null || getClass() != o.getClass()) return false;
			Robot robot = (Robot) o;
			return x == robot.x && y == robot.y && vx == robot.vx && vy == robot.vy;
		}

		@Override
		public int hashCode() {
			return Objects.hash(x, y, vx, vy);
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName() + "{" +
					"x=" + x +
					", y=" + y +
					", vx=" + vx +
					", vy=" + vy +
					"}";
		}
	}

	List<String> lines;
	List<Robot> robots;
	int clumpSize;
	Set<Point> model; // for visualization
	int modelWidth;
	int modelHeight;
	String modelFooter;

	public Exercise14() {
		lines = new ArrayList<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInputWithPatterns() {
		robots = new ArrayList<>();
		for (String line : lines) {
			Matcher m = ROBOT.matcher(line);
			if (m.find()) {
				robots.add(new Robot(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4))));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		clumpSize = (int) Math.floor(Math.sqrt(robots.size())); // a reasonable starting clump size for part 2
	}

	public long doPart1(int steps, int width, int height) {
		int[] quad = new int[4];
		for (Robot r : robots) {
			Point result = r.predictNewPositionPeriodic(steps, width, height);
			if (result.x < width / 2) {
				if (result.y < height / 2) {
					quad[0]++;
				} else if (result.y > height / 2) {
					quad[1]++;
				}
			} else if (result.x > width / 2) {
				if (result.y < height / 2) {
					quad[2]++;
				} else if (result.y > height / 2) {
					quad[3]++;
				}
			}
		}
		return (long) quad[0] * quad[1] * quad[2] * quad[3];
	}

	public long doPart2(int width, int height) {
		Set<Point> INITIAL = new HashSet<>();
		for (Robot r : robots) {
			INITIAL.add(new Point(r.x, r.y));
		}
		while (clumpSize > 1) {
			long steps = 0;
			log.info("Finding clump of size: " + clumpSize);
			Set<Set<Point>> pastPositions = new HashSet<>();
			pastPositions.add(INITIAL);
			while (true) {
				Set<Point> robpos = new HashSet<>();
				for (Robot r : robots) {
					r.moveOneStepPeriodic(width, height);
					robpos.add(new Point(r.x, r.y));
				}
				steps++;
				// tree will probably be some solid drawing made up of #, so find a frame with a large enough clump of robots
				if (testGrid(robpos)) {
					if (PRINTFRAME) display(width, height, steps, robpos);
					log.info("Found!");
					return steps;
				}
				// make sure we don't loop forever if we miss the tree
				if (pastPositions.contains(robpos)) {
					log.info("Nothing found and repeat position at step: " + steps);
					clumpSize -= 2;
					break;
				}
				pastPositions.add(robpos);
			}
		}
		return -1; // shouldn't happen
	}

	public void runRobotsVisual(int width, int height) {
		modelWidth = width;
		modelHeight = height;
		Set<Point> INITIAL = new HashSet<>();
		for (Robot r : robots) {
			INITIAL.add(new Point(r.x, r.y));
		}
		List<Set<Point>> pastPositions = new ArrayList<>();
		pastPositions.add(INITIAL);
		int step = 0;
		int focusStep = -1000;
		while (true) {
			Set<Point> robpos = new HashSet<>();
			for (Robot r : robots) {
				r.moveOneStepPeriodic(width, height);
				robpos.add(new Point(r.x, r.y));
			}
			step++;
			if (testGrid(robpos)) {
				System.out.println(step);
				focusStep = step;
			}
			model = new HashSet<>(robpos);
			updateView(step, focusStep);
			if (pastPositions.contains(robpos)) {
				break;
			}
			pastPositions.add(robpos);
		}
		while(true) {
			step = 0;
			for (Set<Point> robpos : pastPositions) {
				model = new HashSet<>(robpos);
				updateView(step, focusStep);
				step++;
			}
		}
	}

	public boolean testGrid(Set<Point> robpos) {
		int accum = 0;
		for (Point p : robpos) {
			accum += (
					robpos.contains(new Point(p.x + 1, p.y)) &&
					robpos.contains(new Point(p.x - 1, p.y)) &&
					robpos.contains(new Point(p.x, p.y + 1)) &&
					robpos.contains(new Point(p.x, p.y-1))
			) ? 1 : 0;
		}
		return accum >= clumpSize;
	}

	public void display(long width, long height, long steps, Set<Point> locations) {
		System.out.println(steps + ":");
		for (int j = 0; j < height; j++) {
			for (int i = 0 ; i < width; i++) {
				if (locations.contains(new Point(i, j))) {
					if (i == width / 2 || j == height / 2) {
						System.out.print("*");
					} else {
						System.out.print("#");
					}
				} else {
					if (i == width / 2 || j == height / 2) {
						System.out.print("+");
					} else {
						System.out.print(".");
					}
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	public void updateView(int step, int focusStep) {
		int speed = 1;
		if (focusStep > 0) {
			int log2 = (int) Math.ceil(Math.log(1 + Math.abs(focusStep - step))/Math.log(2));
			if (log2 < SPEEDS.length) {
				speed = SPEEDS[log2];
			}
		}
		modelFooter = step + " [" + (focusStep > 0 ? focusStep : "?") + "] " + speed;
		if (ANIMATE) {
			repaint();
			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {
				// NOP
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		if (model != null) { // wait till ready
			g.setColor(Color.white);
			g.fillRect(0, 0, getWidth(), getHeight());
			Graphics2D g2 = (Graphics2D) g;
			int SIZE = Math.min(getWidth() / (modelWidth + 2 * MARGIN), getHeight() / (modelHeight + 2 * MARGIN));
			int xO = (getWidth() - modelWidth * SIZE) / 2;
			int yO = (getHeight() - modelHeight * SIZE) / 2;

			for (Point p : model) {
				g2.setColor(Color.black);
				g2.fillRect(xO + p.x * SIZE, yO + p.y * SIZE, SIZE - 1, SIZE - 1);
			}
			drawCenteredString(g2, modelFooter, new Rectangle(getWidth() / 2 - 5, getHeight() - MARGIN / 2 * SIZE - 5, 10, 10), DEFAULT_FONT);
		}
	}

	private static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 900));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2024, 14));

		// Process input (5ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInputWithPatterns();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		if (ANIMATE) { // just animate the robots forever
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
			ex.runRobotsVisual(101, 103);
		} else { // run the actual exercises

			// part 1 - 230686500 (<1ms)
			start = System.currentTimeMillis();
			log.info("Part 1:");
			log.info(ex.doPart1(100, 101, 103));
			log.info("Took " + (System.currentTimeMillis() - start) + "ms");

			// part 2 - 7672 (1.452s)
			start = System.currentTimeMillis();
			log.info("Part 2:");
			log.info(ex.doPart2(101, 103));
			log.info("Took " + (System.currentTimeMillis() - start) + "ms");
		}
	}
}
