package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 4, title="Ceres Search", keywords = {"grid", "word-search", "pattern-matching"})
public class Exercise04 {

	private List<String> lines;
	private CharGrid grid;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		grid = new CharGrid(lines, '.');
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (int col = 0; col < grid.getWidth(); col++) {
			for (int row = 0; row < grid.getHeight(); row++) {
				accum += grid.occursAt(col, row, new CharGrid("X...\n.M..\n..A.\n...S")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("S...\n.A..\n..M.\n...X")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("...X\n..M.\n.A..\nS...")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("...S\n..A.\n.M..\nX...")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("XMAS")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("SAMX")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("X\nM\nA\nS")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("S\nA\nM\nX")) ? 1 : 0;
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (int col = 0; col < grid.getWidth(); col++) {
			for (int row = 0; row < grid.getHeight(); row++) {
				accum += grid.occursAt(col, row, new CharGrid("M.S\n.A.\nM.S")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("S.S\n.A.\nM.M")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("S.M\n.A.\nS.M")) ? 1 : 0;
				accum += grid.occursAt(col, row, new CharGrid("M.M\n.A.\nS.S")) ? 1 : 0;
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2024, 4));

		// Process input (7ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 2578 (108ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1972 (37ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
