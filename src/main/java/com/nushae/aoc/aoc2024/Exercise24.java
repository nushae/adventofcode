package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 24, title="Crossed Wires", keywords = {"logic-gates", "full-adder", "binary-tree", "structure-matching", "recursion"})
public class Exercise24 {

	private static final Pattern GATE = Pattern.compile("([a-z0-9]+) (AND|OR|XOR) ([a-z0-9]+) -> ([a-z0-9]+)");
	private static final int AND = 0;
	private static final int OR = 1;
	private static final int XOR = 2;

	record LogicGate(int type, String leftWire, String rightWire, String outputWire) {

		// is this logic gate an XOR(xN, yN)?
		boolean isLeafXor(int inputNumber) {
			String xName = "x" + StringUtils.leftPad(Integer.toString(inputNumber), 2, "0");
			String yName = "y" + StringUtils.leftPad(Integer.toString(inputNumber), 2, "0");
			return type == XOR && (xName.equals(leftWire) && yName.equals(rightWire) || xName.equals(rightWire) && yName.equals(leftWire));
		}

		// is this logic gate an AND(xN, yN)?
		boolean isLeafAnd(int inputNumber) {
			String xName = "x" + StringUtils.leftPad(Integer.toString(inputNumber), 2, "0");
			String yName = "y" + StringUtils.leftPad(Integer.toString(inputNumber), 2, "0");
			return type == AND && (xName.equals(leftWire) && yName.equals(rightWire) || xName.equals(rightWire) && yName.equals(leftWire));
		}

		@Override
		public String toString() {
			return outputWire + ":" + (type == AND ? "AND" : type == OR ? "OR" : "XOR") + "(" + leftWire + "," + rightWire + ")";
		}
	}

	List<String> lines;
	Map<String, Boolean> wires;
	Set<LogicGate> logicGates;
	Map<String, LogicGate> gatesByOutput;
	int totalZ;
	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		totalZ = 0;
		wires = new HashMap<>(); // only for part 1
		logicGates = new HashSet<>(); // only for part 1
		gatesByOutput = new HashMap<>(); // only for part 2
		boolean inputWires = true;
		for (String line : lines) {
			if (line.isEmpty()) {
				inputWires = false;
				continue;
			}
			if (inputWires) {
				String[] parts = line.split(": ");
				wires.put(parts[0], Integer.parseInt(parts[1]) == 1);
			} else {
				Matcher m = GATE.matcher(line);
				if (m.find()) {
					int type = "AND".equals(m.group(2)) ? AND : "OR".equals(m.group(2)) ? OR : XOR;
					LogicGate lg = new LogicGate(type, m.group(1), m.group(3), m.group(4));
					logicGates.add(lg);
					gatesByOutput.put(m.group(4), lg);
					if (m.group(4).startsWith("z")) {
						totalZ++;
					}
				} else {
					log.warn("Don't know how to parse " + line);
				}
			}
		}
		// analysis for part 2:
//		for (String outputWire: gatesByOutput.keySet()) {
//			log.debug(treeToString(outputWire)); // use treeToIndentedString to see more structure, take care to use only for the smaller trees
//		}
	}

	public String treeToString(String output) {
		if (output.startsWith("x") || output.startsWith("y")) {
			return output;
		}
		LogicGate currentGate = gatesByOutput.get(output);
		String operator = currentGate.type == AND ? "and" : currentGate.type == OR ? "or" : "xor";
		String left = currentGate.leftWire;
		String right = currentGate.rightWire;
		return output + ":" + operator + "(" +
				("xyz".contains(left.substring(0, 1)) ? left : treeToString(left)) +
				("xyz".contains(right.substring(0, 1)) ? right : treeToString(right)) + ")";
	}
	public String treeToIndentedString(String output, String indent) {
		if (output.startsWith("x") || output.startsWith("y")) {
			return output;
		}
		LogicGate currentGate = gatesByOutput.get(output);
		String operator = currentGate.type == AND ? "and" : currentGate.type == OR ? "or" : "xor";
		String left = currentGate.leftWire;
		String right = currentGate.rightWire;
		return indent + output + ":" + operator + "(" +
				("xyz".contains(left.substring(0, 1)) ? left + ", " : "\n" + treeToIndentedString(left, indent + "  ") + ",\n") +
				("xyz".contains(right.substring(0, 1)) ? right + ")" : treeToIndentedString(right, indent + "  ") + "\n" + indent + ")");
	}

	public long doPart1() {
		while (!logicGates.isEmpty()) {
			Iterator<LogicGate> itr = logicGates.iterator();
			while (itr.hasNext()) {
				LogicGate gate = itr.next();
				if (wires.containsKey(gate.leftWire) && wires.containsKey(gate.rightWire)) {
					boolean leftVal = wires.get(gate.leftWire);
					boolean rightVal = wires.get(gate.rightWire);
					boolean result;
					switch (gate.type) {
						case AND:
							result = leftVal && rightVal;
							break;
						case OR:
							result = leftVal || rightVal;
							break;
						default:
							result = leftVal != rightVal;
					}
					wires.put(gate.outputWire(), result);
					itr.remove();
				}
			}
		}
		return Long.parseLong(getBinary("z"), 2);
	}

	String getBinary(String prefix) {
		String binaryString = "";
		for (String key : wires.keySet().stream().filter(str -> str.startsWith(prefix)).sorted(Comparator.reverseOrder()).toList()) {
			binaryString += wires.get(key) ? "1" : "0";
		}
		return binaryString;
	}

	// Observations for part 2 -
	// 1) each output has a tree of gates feeding into it
	// 2) structure is similar for each output wire except z00 and zMAX, for now we will leave these out of consideration (unless we don't find 8 wires)
	// 3) This structure shows a repeated pattern that either has XOR or AND as top level operator:
	// eg. z07:
	// xor(                        <-- call this treeChunk(7)
	//   xor(x07, y07),
	//   or(
	//     and(y06, x06),
	//     and(                    <-- call this treeChunk(6)
	//       xor(y06, x06),
	//       or(
	//         and(y05, x05),
	//         and(                <-- call this treeChunk(5)
	//           xor(y05, x05),
	//           ... [innermost treeChunk(1) will deviate and be and(xor(x01, y01), and(x00, y00)) ]
	//	       )
	//	     )
	//     )
	//   )
	// )
	// 4) Swapped wires should create deviations from the typical structure
	// 5) A mutually recursive approach suggests itself:
	//    - verify children in treeChunk(N) (one child will be an OR)
	//    - verify children inside OR (one child will be treeChunk(N-1))
	//    - verify children inside treeChunk(N-1), etc...
	public String doPart2() {
		Set<String> suspectWires = new HashSet<>();
		for (int wireNumber = 0; wireNumber < totalZ; wireNumber++) {
			if (wireNumber > 0 && wireNumber != totalZ - 1) { // outer bits have deviating structure, skip them for now
				String wireName = "z" + StringUtils.leftPad(Integer.toString(wireNumber), 2, "0");
				LogicGate current = gatesByOutput.get(wireName);
				log.debug("Examining " + wireName);
				Set<String> result = verifyTreeChunk(current, wireNumber);
				log.debug("Found " + result + " for " + wireName);
				suspectWires.addAll(result);
			}
		}
		return StringUtils.join(suspectWires.stream().sorted().collect(Collectors.toList()), ",");
	}

	// treeChunk(N) should be:
	// xor/and(
	//   xor(xN, yN),
	//   or(N) -> recurse
	// )
	// but treeChunk(1) is:
	// and(
	//   xor(x01, y01),
	//   and(x00, y00)
	// )
	Set<String> verifyTreeChunk(LogicGate current, int wireNumber) {
		String wireName = current.outputWire;
		if (wireName.startsWith("z") && current.type != 2 || !wireName.startsWith("z") && current.type != 0) { // top level, should be XOR for z-outputs, AND otherwise
			log.debug("Adding " + wireName + " because top level should be " + (wireName.startsWith("z") ? "XOR" : "AND") + " but is " + current.type);
			return Collections.singleton(current.outputWire);
		}
		String left = current.leftWire;
		String right = current.rightWire;
		LogicGate leftGate = gatesByOutput.get(left);
		LogicGate rightGate = gatesByOutput.get(right);
		if (wireNumber == 1) { // deviating structure
			if (leftGate.isLeafXor(1)) {
				if (rightGate.isLeafAnd(0)) {
					return Collections.emptySet();
				} else {
					log.debug("Adding " + right + " because deepest level isn't ok");
					return Collections.singleton(right);
				}
			}
			if (rightGate.isLeafXor(1)) {
				if (leftGate.isLeafAnd(0)) {
					return Collections.emptySet();
				} else {
					log.debug("Adding " + left + " because deepest level isn't ok");
					return Collections.singleton(left);
				}
			}
			// BOTH are suspect (I doubt this will happen)
			Set<String> result = new HashSet<>();
			log.debug("Adding " + left + " and " + right + " because neither are ok at deepest level");
			result.add(left);
			result.add(right);
			return result;
		} else {
			// we want to see this structure, for N = wireNumber:
			// one input tree = xor(xN, yN)
			// one input tree = or(N-1)
			Set<String> result = new HashSet<>();
			if (leftGate.isLeafXor(wireNumber)) {
				if (rightGate.type == OR) {
					return verifyOr(rightGate, wireNumber - 1);
				}
				log.debug("Adding " + right + " because we have an XOR child but no OR");
				return Collections.singleton(right);
			} else if (rightGate.isLeafXor(wireNumber)) {
				if (leftGate.type == OR) {
					return verifyOr(leftGate, wireNumber - 1);
				}
				log.debug("Adding " + left + " because we have an XOR child but no OR");
				return Collections.singleton(left);
			} else { // no xor(N), check for OR, if present, other input must be swapped
				if (leftGate.type == OR) {
					log.debug("Adding " + right + " because XOR " + wireNumber + " - 1 is missing but OR is present");
					result.add(right);
					result.addAll(verifyOr(leftGate, wireNumber - 1));
				} else if (rightGate.type == OR) {
					log.debug("Adding " + left + " because XOR " + wireNumber + " - 1 is missing but OR is present");
					result.add(left);
					result.addAll(verifyOr(rightGate, wireNumber - 1));
				} else {
					log.debug("Adding " + left + " and " + right + " because XOR " + wireNumber + " - 1 and OR are missing");
					result.add(left);
					result.add(right);
				}
			}
			return result;
		}
	}

	// or(N) should be:
	// or(
	//   and(xN, yN),
	//   treeChunk(N) -> recurse
	// )
	Set<String> verifyOr(LogicGate current, int wireNumber) {
		String left = current.leftWire;
		String right = current.rightWire;
		LogicGate leftGate = gatesByOutput.get(left);
		LogicGate rightGate = gatesByOutput.get(right);
		if (leftGate.isLeafAnd(wireNumber)) {
			return verifyTreeChunk(rightGate, wireNumber);
		} else if (rightGate.isLeafAnd(wireNumber)) {
			return verifyTreeChunk(leftGate, wireNumber);
		}
		Set<String> result = new HashSet<>();
		if (leftGate.type != AND) {
			log.debug("Adding " + left + " because child is not an AND");
			result.add(left);
		}
		if (rightGate.type != AND) {
			log.debug("Adding " + right + " because child is not an AND");
			result.add(right);
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2024, 24));

		// Process input (5ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 48063513640678 (5ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - "hqh,mmk,pvb,qdq,vkq,z11,z24,z38" (8ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
