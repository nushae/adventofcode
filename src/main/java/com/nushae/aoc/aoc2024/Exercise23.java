package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.GraphUtil.circularVisualizeWithCliquesAndColors;
import static com.nushae.aoc.util.GraphUtil.findCliques;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 23, title="LAN Party", keywords = {"graph", "clique", "bron-kerbosch"})
public class Exercise23 extends JPanel {

	public static final boolean VISUAL = false; // maybe don't do this with the actual input
	public static final int VIEW_WIDTH = 1200; // window width for graph visualization
	public static final int VIEW_HEIGHT = 1200; // window height for graph visualization
	public static final Map<Integer, Color> CLIQUE_COLORS = Map.ofEntries(
			Map.entry(3, new Color(143, 0, 255)),
			Map.entry(4, new Color(95, 0, 255)),
			Map.entry(5, new Color(47, 0, 255)),
			Map.entry(6, new Color(0, 0, 255)),
			Map.entry(7, new Color(0, 42, 170)),
			Map.entry(8, new Color(0, 85, 85)),
			Map.entry(9, new Color(0, 128, 0)),
			Map.entry(10, new Color(85, 170, 0)),
			Map.entry(11, new Color(170, 212, 0)),
			Map.entry(12, new Color(255, 255, 0)),
			Map.entry(13, new Color(255, 170, 0)),
			Map.entry(14, new Color(255, 85, 0)),
			Map.entry(15, new Color(255, 0, 0))
	);
	public static final Map<Integer, Color> SIMPLE_COLORS = Map.of(
			13, new Color(0, 255, 0),
			14, new Color(255, 0, 0),
			15, new Color(0, 0, 255)
	);

	List<String> lines;
	List<String> nodeNames;
	// the most basic implementation of a graph is a map node -> neighbours.
	// In this case all we need is the name of the node, so we use String as our node type
	Map<String, Set<String>> neighbours;
	Map<Pair<String, String>, Integer> largestCommonClique; // for visualization only

	BufferedImage image;

	public Exercise23() {
		lines = new ArrayList<>();
	}

	public Exercise23(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise23(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		neighbours = new HashMap<>();
		Set<String> names = new HashSet<>();
		for (String line : lines) {
			String[] parts = line.split("-");
			names.add(parts[0]);
			names.add(parts[1]);
			neighbours.computeIfAbsent(parts[0], k -> new HashSet<>()).add(parts[1]);
			neighbours.computeIfAbsent(parts[1], k -> new HashSet<>()).add(parts[0]);
		}
		nodeNames = new ArrayList<>(names);
	}

	public long doPart1() {
		long accum = 0;
		for (int first = 0; first < nodeNames.size() - 2; first++) {
			String firstName = nodeNames.get(first);
			for (int second = first + 1; second < nodeNames.size() - 1; second++) {
				String secondName = nodeNames.get(second);
				if (neighbours.get(firstName).contains(secondName)) { // connected
					for (int third = second + 1; third < nodeNames.size(); third++) {
						String thirdName = nodeNames.get(third);
						if (thirdName.startsWith("t") || firstName.startsWith("t") || secondName.startsWith("t")) {
							if (neighbours.get(firstName).contains(thirdName) && neighbours.get(secondName).contains(thirdName)) {
								accum++;
							}
						}
					}
				}
			}
		}
		return accum;
	}

	public String doPart2() {
		largestCommonClique = new HashMap<>();
		Set<String> maxSet = Collections.emptySet();
		Set<String> minSet = new HashSet<>();
		for (Set<String> current : findCliques(neighbours)) {
			if (VISUAL) {
				List<String> nodesInClique = new ArrayList<>(current);
				nodesInClique.sort(Comparator.naturalOrder());
				for (int left = 0; left < nodesInClique.size() - 1; left++) {
					for (int right = left + 1; right < nodesInClique.size(); right++) {
						largestCommonClique.put(new Pair<>(nodesInClique.get(left), nodesInClique.get(right)), current.size());
					}
				}
				for (String node : current) {
					largestCommonClique.put(new Pair<>(node, node), Math.max(current.size(), largestCommonClique.getOrDefault(new Pair<>(node, node), 0)));
				}
			}
			if (current.size() > maxSet.size()) {
				maxSet = current;
			}
			if (minSet.isEmpty() || current.size() < minSet.size()) {
				minSet = current;
			}
		}
		System.out.println(minSet.size());
		List<String> result = new ArrayList<>(maxSet);
		result.sort(Comparator.naturalOrder());
		return StringUtils.join(result, ",");
	}

	public void drawGraph() {
		image = new BufferedImage(VIEW_WIDTH, VIEW_HEIGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		g2.setColor(Color.white);
		g2.fillRect(0, 0, VIEW_WIDTH, VIEW_HEIGHT);
		// let's say this method is a.... work in progress...
		// forceDirectedVisualizeWithCliquesAndColors(g2, VIEW_WIDTH, VIEW_HEIGHT, neighbours, largestCommonClique, CLIQUE_COLORS);
		circularVisualizeWithCliquesAndColors(g2, VIEW_WIDTH, VIEW_HEIGHT, neighbours, largestCommonClique, CLIQUE_COLORS);
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		if (image != null) {
			g.drawImage(image, 0, 0, null);
		}
	}

	private static void createAndShowGUI(JPanel panel) {
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(VIEW_WIDTH + 6, VIEW_HEIGHT + 29));
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(aocPath(2024, 23, "input.txt"));

		// Process input (7ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 1064 (21ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - "aq,cc,ea,gc,jo,od,pa,rg,rv,ub,ul,vr,yy" (196ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		if (VISUAL) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
			ex.drawGraph();
		}
	}
}
