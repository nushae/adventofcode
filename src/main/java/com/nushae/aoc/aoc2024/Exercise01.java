package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 1, title="Historian Hysteria", keywords = {"list-processing", "sorting"})
public class Exercise01 {
	List<String> lines;
	List<Long> numbers1, numbers2;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		numbers1 = new ArrayList<>();
		numbers2 = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(" +");
			numbers1.add(Long.parseLong(parts[0]));
			numbers2.add(Long.parseLong(parts[1]));
		}
		numbers1.sort(Comparator.naturalOrder());
		numbers2.sort(Comparator.naturalOrder());
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (int i = 0; i < numbers1.size(); i++) {
			accum += Math.abs(numbers1.get(i) - numbers2.get(i));
		}
		return accum;
	}

	// One could take the sortedness of the lists into account but why bother, brute force is fast enough
	public long doPart2() {
		long accum = 0;
		for (long num : numbers1) {
			accum += num * count(num);
		}
		return accum;
	}

	private long count(long input) {
		long accum = 0;
		for (long number : numbers2) {
			if (input == number) {
				accum++;
			} else if (input < number) {
				return accum;
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2024, 1));

		// Process input (22ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1151792 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 21790168 (6ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
