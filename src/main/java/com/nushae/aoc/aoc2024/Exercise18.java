package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.SearchUtil.binarySearch;
import static com.nushae.aoc.util.SearchUtil.dsp;

@Log4j2
@Aoc(year = 2024, day = 18, title="RAM Run", keywords = {"shortest-path", "binary-search"})
public class Exercise18 {

	List<String> lines;
	List<Point> bytesInOrder;
	int maxX;
	int maxY;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		bytesInOrder = new ArrayList<>();
		maxX = 0;
		maxY = 0;
		for (String line : lines) {
			String[] parts = line.split(",");
			Point p = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
			bytesInOrder.add(p);
			maxX = Math.max(maxX, p.x);
			maxY = Math.max(maxY, p.y);
		}
	}

	public long doPart1(long limit) {
		Set<Point> subSet = new HashSet<>();
		for (int i = 0; i < limit + 1; i++) {
			subSet.add(bytesInOrder.get(i));
		}
		CharGrid first = new CharGrid(maxX + 1, maxY + 1, subSet, '.', '#');
		return dsp(new Location(0, 0, first), Collections.singleton('#'), loc -> loc.getX() == maxX && loc.getY() == maxY).getRight();
	}

	public String doPart2() {
		// binary search will find the highest limit for which we can still find a path
		int limit = (int) binarySearch(0, bytesInOrder.size(), value -> doPart1(value) < 0); // reuse for a better future
		Point p = bytesInOrder.get(limit + 1);
		return p.x + "," + p.y;
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2024, 18));

		// Process input (9ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 370 (509ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(1024));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - "65,6" (489ms - how is part 2 faster than part 1 when it CALLS part 1 multiple times?!)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
