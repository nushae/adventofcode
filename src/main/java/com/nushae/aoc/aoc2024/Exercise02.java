package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 2, title="Red-Nosed Reports", keywords = {"list-processing", "flanks"})
public class Exercise02 {

	List<String> lines;
	List<Long> numbers;
	List<List<Long>> reports;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		numbers = new ArrayList<>();
		reports = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(" ");
			List<Long> newNums = new ArrayList<>();
			for (String s : parts) {
				newNums.add(Long.parseLong(s));
			}
			reports.add(newNums);
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (List<Long> report : reports) {
			boolean result = levelsAreSafe(report, 0);
			accum += result ? 1 : 0;
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		long idx = 0;
		for (List<Long> report : reports) {
			boolean result = isItSafeWithDampener(report);
			if (result) {
				System.out.println(idx);
			}
			accum += result ? 1 : 0;
			if (idx == 163) {
				System.out.println(report);
			}
			idx++;
		}
		return accum;
	}

	public long doPart2Smart() {
		long accum = 0;
		int idx = 0;
		for (List<Long> report : reports) {
			boolean result = levelsAreSafe(report, 1);
			if (!result) {
				long tmp = report.remove(0);
				result = levelsAreSafe(report, 0);
				report.add(0, tmp);
			}
			if (result) {
				System.out.println(idx);
			}
			accum += result ? 1 : 0;
			idx++;
		}
		return accum;
	}

	// currently fails if to-be-ignored value is first in list
	private boolean levelsAreSafe(List<Long> input, int tolerance) {
		long cache = input.get(0);
		long sgn = (long) Math.signum(input.get(1) - input.get(0));
		int skipped = 0;
		for (int i = 1; i < input.size(); i++) {
			long diff = input.get(i) - cache;
			long diffSign = (long) Math.signum(diff);
			diff = Math.abs(diff);
			if (diffSign != sgn || diff < 1 || diff > 3) {
				if (skipped < tolerance) {
					skipped++;
				} else {
					return false;
				}
			} else {
				cache = input.get(i);
			}
		}
		return true;
	}

	// crude but it does the job
	private boolean isItSafe(List<Long> input) {
			long sgn = (long) Math.signum(input.get(0) - input.get(1));
			if (sgn != 0) {
				for (int i = 1; i < input.size(); i++) {
					long diff = input.get(i - 1) - input.get(i);
					long diffSign = (long) Math.signum(diff);
					diff = Math.abs(diff);
					if (diffSign != sgn || diff < 1 || diff > 3) {
						return false;
					}
				}
				return true;
			}
			return false;
	}

	// Brute force ftw :/
	private boolean isItSafeWithDampener(List<Long> input) {
		for (int i = 0; i < input.size(); i++) {
			long skip = input.remove(i);
			if (isItSafe(input)) {
				return true;
			}
			input.add(i, skip);
		}
		return false;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2024, 2));

		// Process input (6ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 686 (4ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 717 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
