package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.CHAR_TO_DIR;
import static com.nushae.aoc.util.MathUtil.rot90R;

@Log4j2
@Aoc(year = 2024, day = 6, title="Guard Gallivant", keywords = {"grid", "pathing", "loop-detection"})
public class Exercise06 {

	List<String> lines;
	CharGrid grid;

	public Exercise06() {
		lines = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		grid = new CharGrid(lines, '.');
		log.info("Input processed.");
	}

	public long doPart1() {
		return routeLengthOrLoop(grid);
	}

	// This normally fails, but hey what the heck, might be fast enough...
	public long doPart2() {
		long accum = 0;
		for (int x = 0; x < grid.getWidth(); x++) {
			for (int y = 0; y < grid.getHeight(); y++) {
				if (grid.get(x, y) != '#' && grid.get(x, y) != '^') {
					CharGrid testGrid = new CharGrid(grid);
					testGrid.set(x, y, '#');
					if (routeLengthOrLoop(testGrid) == -7009) {
						accum++;
					}
				}
			}
		}
		return accum;
	}

	public long routeLengthOrLoop(CharGrid g) {
		Set<Pair<Location, Point>> steps = new HashSet<>();
		Set<Location> visited = new HashSet<>();
		Optional<Location> guard = g.findFirst('^');
		if (guard.isPresent()) {
			Location current = guard.get();
			Point dir = CHAR_TO_DIR.get(current.resolve());
			steps.add(new Pair<>(current, dir));
			while (current.add(dir.x, dir.y).inContext()) {
				if (current.add(dir.x, dir.y).resolve() != '#') {
					current = current.add(dir.x, dir.y);
					if (!steps.contains(new Pair<>(current, dir))) {
						steps.add(new Pair<>(current, dir));
						visited.add(current);
					} else {
						return -7009; // 'LOOP'
					}
				} else {
					dir = rot90R(1, dir);
				}
			}
		}
		return visited.size();
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2024, 6));

		// Process input (5ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 5317 (441ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1831 (9.882s) >_>
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
