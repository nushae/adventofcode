package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 12, title="Garden Groups", keywords = {"flood-fill", "area", "perimeter"})
public class Exercise12 {

	List<String> lines;
	CharGrid grid;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		grid = new CharGrid(lines, '.');
	}

	public long doPart1() {
		long accum = 0;
		Set<Location> visited = new HashSet<>();
		for (int i = 0; i < grid.getWidth() * grid.getHeight(); i++) {
			Location current = new Location(i % grid.getWidth(), i / grid.getWidth(), grid);
			if (!visited.contains(current)) {
				Pair<Set<Location>, List<Location>> result = getArea(current);
				visited.addAll(result.getLeft());
				accum += result.getLeft().size() * result.getRight().size();
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		Set<Location> visited = new HashSet<>();
		for (int i = 0; i < grid.getWidth() * grid.getHeight(); i++) {
			Location current = new Location(i % grid.getWidth(), i / grid.getWidth(), grid);
			if (!visited.contains(current)) {
				Set<Location> result = getArea(current).getLeft();
				visited.addAll(result);
				accum += result.size() * countEdges(result);
			}
		}
		return accum;
	}

	public Pair<Set<Location>, List<Location>> getArea(Location loc) {
		Set<Location> area = new HashSet<>();
		List<Location> rim = new ArrayList<>(); // tricksy!!
		Deque<Location> Q = new ArrayDeque<>();
		Q.add(loc);
		area.add(loc);
		while (!Q.isEmpty()) {
			Location current = Q.poll();
			for (Location neighbour : current.getNeighboursIgnoreContext()) {
				if (!area.contains(neighbour)) {
					if (neighbour.resolve() == loc.resolve()) {
						Q.add(neighbour);
						area.add(neighbour);
					} else {
						rim.add(neighbour);
					}
				}
			}
		}
		return new Pair<>(area, rim);
	}

	public long countEdges(Set<Location> area) {
		CharGrid topEdges = new CharGrid(grid.getWidth() + 2, grid.getHeight() + 2, '.');
		CharGrid botEdges = new CharGrid(grid.getWidth() + 2, grid.getHeight() + 2, '.');
		CharGrid leftEdges = new CharGrid(grid.getWidth() + 2, grid.getHeight() + 2, '.');
		CharGrid rightEdges = new CharGrid(grid.getWidth() + 2, grid.getHeight() + 2, '.');
		for (Location loc : area) {
			for (Location neighbour : loc.getNeighboursIgnoreContext()) {
				if (loc.resolve() != neighbour.resolve()) {
					// add neighbour to appropriate grid
					if (loc.getY() - neighbour.getY() > 0) {
						topEdges.set(neighbour.getX() + 1, neighbour.getY() + 1, '#');
					} else if(loc.getX() - neighbour.getX() > 0) {
						rightEdges.set(neighbour.getX() + 1, neighbour.getY() + 1, '#');
					} else if(loc.getY() - neighbour.getY() < 0) {
						botEdges.set(neighbour.getX() + 1, neighbour.getY() + 1, '#');
					} else {
						leftEdges.set(neighbour.getX() + 1, neighbour.getY() + 1, '#');
					}
				}
			}
		}
		return countAreas(topEdges) + countAreas(rightEdges) + countAreas(botEdges) + countAreas(leftEdges);
	}

	long countAreas(CharGrid input) {
		long accum = 0;
		Set<Location> visited = new HashSet<>();
		for (int i = 0; i < input.getWidth() * input.getHeight(); i++) {
			Location current = new Location(i % input.getWidth(), i / input.getWidth(), input);
			if (!visited.contains(current)) {
				if (current.resolve() == input.getEmpty()) {
					visited.add(current);
					continue;
				}
				visited.addAll(getArea(current).getLeft());
				accum++;
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2024, 12));

		// Process input (5ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 148582 (323ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 914966 (2.626s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
