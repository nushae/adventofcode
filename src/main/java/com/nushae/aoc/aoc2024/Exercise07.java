package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 7, title="Bridge Repair", keywords = {"dfs", "equations"})
public class Exercise07 {

	List<String> lines;
	List<Pair<Long, List<Long>>> equations;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		equations = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(": ");
			List<Long> values = new ArrayList<>();
			long total = Long.parseLong(parts[0]);
			parts = parts[1].split(" ");
			for (String part : parts) {
				values.add(Long.parseLong(part));
			}
			equations.add(new Pair<>(total, values));
		}
		log.info("Input processed - " + equations.size() + " equations.");
	}

	public long doPart1() {
		return calculate(false);
	}

	public long doPart2() {
		return calculate(true);
	}

	public long calculate(boolean part2) {
		long accum = 0;
		for (Pair<Long, List<Long>> eq : equations) {
			if (testEquation(eq.getLeft(), eq.getRight(), eq.getRight().size() - 1, part2)) {
				accum += eq.getLeft();
			}
		}
		return accum;

	}

	// since the equation is evaluated left to right, the rightmost operation is always evaluated last.
	// So we can simply keep "undoing" the rightmost operation until we have none remaining
	// total should be 0 at that point (otherwise reject equation)
	public boolean testEquation(long total, List<Long> values, int lastIndex, boolean concatToo) {
		if (total < 0) {
			return false;
		}
		if (lastIndex < 0) {
			return total == 0;
		}
		long v = values.get(lastIndex);
		if (concatToo) { // try concatenate
			String totalString = "" + total;
			String valueString = "" + v;
			if (totalString.endsWith(valueString)) {
				String newTotal = totalString.substring(0, totalString.length() - valueString.length());
				if (testEquation(newTotal.isEmpty() ? 0 : Long.parseLong(newTotal), values, lastIndex - 1, concatToo)) {
					return true;
				}
			}
		}
		if ((total / v) * v == total) { // try multiply
			if (testEquation(total / v, values, lastIndex - 1, concatToo)) {
				return true;
			}
		}
		// try add
		return testEquation(total - v, values, lastIndex - 1, concatToo);
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2024, 7));

		// Process input (9ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 3245122495150 (7ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 105517128211543 (9ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
