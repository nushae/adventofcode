package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2024, day = 5, title="Print Queue", keywords = {"sorting"})
public class Exercise05 {
	List<String> lines;
	List<Pair<Integer, Integer>> rules;
	List<List<Integer>> updates;
	Set<Integer> pageNumbers;

	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		rules = new ArrayList<>();
		updates = new ArrayList<>();
		pageNumbers = new HashSet<>();
		for (String line : lines) {
			if (line.contains("|")) {
				String[] parts = line.split("\\|");
				int left = Integer.parseInt(parts[0]);
				int right = Integer.parseInt(parts[1]);
				pageNumbers.add(left);
				pageNumbers.add(right);
				rules.add(new Pair<>(left, right));
			} else if (!"".equals(line)) {
				String[] parts = line.split(",");
				List<Integer> update = new ArrayList<>();
				for (String s : parts) {
					update.add(Integer.parseInt(s));
				}
				updates.add(update);
			}
		}
		log.info("Found " + pageNumbers.size() + " page numbers, that would be ordered as follows:");
		log.info(fixUpdate(new ArrayList<>(pageNumbers)));
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (List<Integer> update : updates) {
			accum += testUpdate(update);
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (List<Integer> update : updates) {
			if (testUpdate(update) == 0) {
				update = fixUpdate(update);
				accum += update.get(update.size()/2);
			}
		}
		return accum;
	}

	public int testUpdate(List<Integer> input) {
		boolean ok = true;
		for (int i = 0; ok && i < input.size() - 1; i++) {
			ok = rules.contains(new Pair<>(input.get(i), input.get(i + 1)));
		}
		if (ok) {
			return input.get(input.size()/2);
		}
		return 0;
	}

	public List<Integer> fixUpdate(List<Integer> input) {
		List<Integer> result = new ArrayList<>();
		for (int num : input) {
			result = insertInorder(result, num);
		}
		return result;
	}

	// Look, man, it's still brute force week
	List<Integer> insertInorder(List<Integer> input, int num) {
		List<Integer> result = new ArrayList<>();
		if (input.size() == 0) {
			result.add(num);
		} else {
			if (rules.contains(new Pair<>(num, input.get(0)))) {
				result.add(num);
				result.addAll(input);
			} else if (rules.contains(new Pair<>(input.get(input.size() - 1), num))) {
				result.addAll(input);
				result.add(num);
			} else {
				result.add(input.get(0));
				for (int i = 0; i < input.size() - 1; i++) {
					if (rules.contains(new Pair<>(input.get(i), num)) && rules.contains(new Pair<>(num, input.get(i + 1)))) {
						result.add(num);
					}
					result.add(input.get(i+1));
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2024, 5));

		// Process input (1ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 4924 (7ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 6085 (31ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
