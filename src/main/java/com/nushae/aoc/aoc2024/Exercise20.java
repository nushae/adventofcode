package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.view.ColorGridPanel;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.SearchUtil.dspWithPath;

@Log4j2
@Aoc(year = 2024, day = 20, title="Race Condition", keywords = {"grid", "maze", "shortcuts", "shortest-path", "visualized"})
public class Exercise20 extends JComponent {

	private static final boolean VISUAL = true;
	private static final Map<Character, Color> palette = Map.of(
		'.', Color.gray,
		'#', Color.black,
		'S', Color.orange,
		'E', Color.green,
		's', Color.red,
		'e', Color.red
	);

	List<String> lines;
	CharGrid maze;
	Location start, end;
	List<Location> pathNoCheats;
	long benchmark;
	long bestPath;
	Location bestS;
	Location bestE;

	ColorGridPanel displayPanel;

	public Exercise20() {
		lines = new ArrayList<>();
		maze = new CharGrid(50, 50, '.');
		displayPanel = new ColorGridPanel(maze, palette);
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		maze = new CharGrid(lines);
		start = maze.findFirst('S').get();
		end = maze.findFirst('E').get();
		pathNoCheats = dspWithPath(start, Collections.singleton('#'), loc -> loc.equals(end));
		benchmark = pathNoCheats.size() - 1;
	}

	public long doPart1() {
		return calculateCheats(100, 2);
	}

	public long doPart2() {
		return calculateCheats(100, 20);
	}

	public long calculateCheats(long minimumSaved, long maxCheatLen) {
		long accum = 0;
		bestPath = Long.MAX_VALUE;
		// find two points cheatS and cheatE on the benchmark path that are no further than maxCheatLen apart
		for (int i = 0; i < pathNoCheats.size() - 1; i++) {
			Location cheatS = pathNoCheats.get(i);
			for (int j = i + 1; j < pathNoCheats.size(); j++) {
				Location cheatE = pathNoCheats.get(j);
				long cheatLen = cheatS.manhattan(cheatE);
				if (cheatLen <= maxCheatLen) { // short enough cheat
					// path-with-cheat will be: S -> cheatS -> cheatE -> E
					// We're supposed to check if dsp(S, cheatS) + cheatLen + dsp(cheatE, E) <= benchmark - minimumSaved
					// But since we already found the benchmark path and cheatS and cheatE are always on it, we can simply calculate these two dsp results from that path:
					// dsp(S, cheatS) = i
					// dsp(cheatE, E) = benchmark - j
					long cheatyPathLength = i + cheatLen + benchmark - j;
					accum += (cheatyPathLength  <= benchmark - minimumSaved) ? 1 : 0;
					if (VISUAL && bestPath > cheatyPathLength) {
						bestS = cheatS;
						bestE = cheatE;
						bestPath = cheatyPathLength;
					}
				}
			}
		}
		return accum;
	}

	// for the visual representation
	void setCheatPath() {
		Set<Location> cheats = new HashSet<>();
		for (int  i = 0; i <= Math.abs(bestS.getX() - bestE.getX()); i++) {
			int pointX = Math.min(bestS.getX(), bestE.getX()) + i;
			int pointY = bestS.getY();
			cheats.add(new Location(pointX, pointY, maze));
		}
		for (int i = 0; i < Math.abs(bestS.getY() - bestE.getY()); i++) {
			int pointX = bestE.getX();
			int pointY = Math.min(bestS.getY(), bestE.getY()) + i;
			cheats.add(new Location(pointX, pointY, maze));
		}
		displayPanel.setHighlightColor(Color.yellow);
		displayPanel.setHighlights(cheats);
		displayPanel.refresh();
	}

	private static void createAndShowGUI(JPanel displayPanel) {
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1000));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.add(displayPanel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2024, 20));

		// Process input (includes the single needed dsp call; 522ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1286 (223ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 989316 (194ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		if (VISUAL) {
			ex.displayPanel.setGrid(ex.maze);
			ex.setCheatPath();
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex.displayPanel));
		}
	}
}
