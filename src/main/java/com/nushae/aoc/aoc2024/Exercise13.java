package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BigFraction;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.gaussianReduce;

@Log4j2
@Aoc(year = 2024, day = 13, title="Claw Contraption", keywords = {"gaussian-reduction"})
public class Exercise13 {

	private static final Pattern BUTTON = Pattern.compile("Button .: X\\+(\\d+), Y\\+(\\d+)");
	private static final Pattern PRIZE = Pattern.compile("Prize: X=(\\d+), Y=(\\d+)");

	private record MachineDefinition(long x, long y, long xA, long yA, long xB, long yB) {
		public BigFraction[][] toEquations(boolean part2) {
			return new BigFraction[][] {
					{BigFraction.valueOf(xA), BigFraction.valueOf(xB), BigFraction.valueOf(x).add(BigFraction.valueOf(part2 ? 10000000000000L : 0))},
					{BigFraction.valueOf(yA), BigFraction.valueOf(yB), BigFraction.valueOf(y).add(BigFraction.valueOf(part2 ? 10000000000000L : 0))}
			};
		}
	}

	List<String> lines;
	List<MachineDefinition> machineDefinitions;

	public Exercise13() {
		lines = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInputWithPatterns() {
		machineDefinitions = new ArrayList<>();
		long x = 0, y = 0, xA = 0, yA = 0, xB = 0, yB = 0;
		for (int i = 0; i < lines.size(); i += 4) {
			String line1 = lines.get(i);
			String line2 = lines.get(i + 1);
			String line3 = lines.get(i + 2);
			Matcher m = BUTTON.matcher(line1);
			if (m.find()) {
				xA = Long.parseLong(m.group(1));
				yA = Long.parseLong(m.group(2));
			} else {
				log.warn("[A] Don't know how to parse " + line1);
			}
			m = BUTTON.matcher(line2);
			if (m.find()) {
				xB = Long.parseLong(m.group(1));
				yB = Long.parseLong(m.group(2));
			} else {
				log.warn("[B] Don't know how to parse " + line2);
			}
			m = PRIZE.matcher(line3);
			if (m.find()) {
				x = Long.parseLong(m.group(1));
				y = Long.parseLong(m.group(2));
			} else {
				log.warn("[Prize] Don't know how to parse " + line3);
			}
			machineDefinitions.add(new MachineDefinition(x, y, xA, yA, xB, yB));
		}
	}

	public long doPart1() {
		return doButtons(false);
	}

	public long doPart2() {
		return doButtons(true);
	}

	public long doButtons(boolean part2) {
		long accum = 0;
		for (MachineDefinition cl : machineDefinitions) {
			BigFraction[] result = gaussianReduce(cl.toEquations(part2));
			if (result[0].isWholeNumber() && result[1].isWholeNumber()) {
				accum += result[0].longValue() * 3 + result[1].longValue();
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2024, 13));

		// Process input (6ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInputWithPatterns();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 33427 (38ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 91649162972270 (47ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
