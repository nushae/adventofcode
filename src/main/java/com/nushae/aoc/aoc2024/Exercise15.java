package com.nushae.aoc.aoc2024;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import com.nushae.aoc.util.GifSequenceWriter;
import lombok.extern.log4j.Log4j2;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.GraphicsUtil.DEFAULT_FONT;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2024, day = 15, title="Warehouse Woes", keywords = {"grid", "sokoban", "visualized", "animated"})
public class Exercise15 extends JPanel {

	private static final int SPEED = 0; // set to > 0 to visually animate the bot's movement
	private static final int MARGIN = 4; // margin in cells around the grid in visualization
	private static final boolean WRITE_GIF = false; // if you set this to true, you end up with a 500+ MB gif file after just over 45 mins, so don't do it
	public static final int VIEW_WIDTH = 1200; // window width AND output gif width
	public static final int VIEW_HEIGHT = 900; // window height AND output gif height

	List<String> lines;
	String instructions = "";
	CharGrid grid;
	CharGrid gridWide;
	Location botLoc;
	Location botLocWide;
	CharGrid model; // for visual representation
	String modelHeader;
	String modelFooter;
	List<CharGrid> moves;

	public Exercise15() {
		lines = new ArrayList<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
		postProcess();
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
		postProcess();
	}

	// embrace thy paranoia
	private void postProcess() {
		while (!"".equals(lines.get(lines.size() - 1))) {
			instructions = lines.remove(lines.size() - 1) + instructions;
		}
		lines.remove(lines.size() - 1);
	}

	public void processInput() {
		grid = new CharGrid(lines);
		botLoc = grid.findFirst('@').get();
		gridWide = new CharGrid(grid.getHeight(), grid.getWidth() * 2, '.');
		for (int i = 0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				char c = grid.get(i, j);
				if (c == '@') {
					gridWide.set(i * 2, j, '@');
					gridWide.set(i * 2 + 1, j, '.');
				} else if (c == 'O') {
					gridWide.set(i * 2, j, '[');
					gridWide.set(i * 2 + 1, j, ']');
				} else {
					gridWide.set(i * 2, j, c);
					gridWide.set(i * 2 + 1, j, c);
				}
			}
		}
		botLocWide = gridWide.findFirst('@').get();
	}

	public long doPart1() {
		for (int i = 0; i < instructions.length(); i++) {
			grid = moveBot(grid, CHAR_TO_DIR.get(instructions.charAt(i)));
			updateView(false, i);
		}
		long accum = 0;
		for (int i = 0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				if (grid.get(i, j) == 'O') {
					accum += i + 100L * j;
				}
			}
		}
		return accum;
	}

	public long doPart2() {
		moves = new ArrayList<>();
		moves.add(gridWide);
		for (int i = 0; i < instructions.length(); i++) {
			gridWide = doubleTimeBot(gridWide, CHAR_TO_DIR.get(instructions.charAt(i)));
			moves.add(gridWide);
			updateView(true, i);
		}
		writeOutputGif();
		long accum = 0;
		for (int i = 0; i < gridWide.getWidth(); i++) {
			for (int j = 0; j < gridWide.getHeight(); j++) {
				if (gridWide.get(i, j) == '[') {
					accum += i + 100L * j;
				}
			}
		}
		return accum;
	}

	CharGrid moveBot(CharGrid grid, Point dir) {
		CharGrid newGrid = new CharGrid(grid);
		int x = botLoc.getX() + dir.x;
		int y = botLoc.getY() + dir.y;
		while (newGrid.get(x, y) == 'O') {
			x += dir.x;
			y += dir.y;
		}
		if (newGrid.get(x, y) == '.') { // room to shift
			char prevChar;
			do {
				prevChar = newGrid.get(x - dir.x, y - dir.y);
				newGrid.set(x, y, prevChar);
				x -= dir.x;
				y -= dir.y;
				newGrid.set(x, y, '.');
			} while (prevChar != '@');
			botLoc = botLoc.add(dir.x, dir.y);
		}
		return newGrid;
	}

	// Let's just say part 2 took a bit more... methoding...
	// I'm so sorry for the mess, but I really didn't see a way to make this more elegant :(

	CharGrid doubleTimeBot(CharGrid grid, Point dir) {
		// For readability, I extracted methods for horizontal and vertical movement
		if (dir == NORTH || dir == SOUTH) {
			return doubleTimeVertical(grid, dir); // complex :(
		}
		return doubleTimeHorizontal(grid, dir); // 'simple'
	}

	CharGrid doubleTimeHorizontal(CharGrid grid, Point dir) {
		// like part 1... kinda
		CharGrid newGrid = new CharGrid(grid);
		int x = botLocWide.getX() + dir.x;
		int y = botLocWide.getY();
		while (newGrid.get(x, y) == '[' || newGrid.get(x, y) == ']') {
			x += 2 * dir.x;
		}
		if (newGrid.get(x, y) == '.') { // room to shift
			char prevChar;
			do {
				prevChar = newGrid.get(x - dir.x, y - dir.y);
				newGrid.set(x, y, prevChar);
				x -= dir.x;
				y -= dir.y;
				if (prevChar != '@') {
					prevChar = newGrid.get(x - dir.x, y - dir.y);
					newGrid.set(x, y, prevChar);
					x -= dir.x;
					y -= dir.y;
				}
				newGrid.set(x, y, '.');
			} while (prevChar != '@');
			botLocWide = botLocWide.add(dir.x, dir.y);
		}
		return newGrid;
	}

	CharGrid doubleTimeVertical(CharGrid grid, Point dir) {
		CharGrid newGrid = new CharGrid(grid);
		int x = botLocWide.getX();
		int y = botLocWide.getY() + dir.y;
		if (newGrid.get(x, y) == '[' || newGrid.get(x, y) == ']') { // check boxes for movability
			Set<Location> movableBoxes = findMovableBoxes(newGrid, x, y, dir.y);
			if (!movableBoxes.isEmpty()) {
				for (Location boxLoc : movableBoxes) { // erase moved boxes
					newGrid.set(boxLoc.getX(), boxLoc.getY(), '.');
					newGrid.set(boxLoc.getX() + 1, boxLoc.getY(), '.');
				}
				for (Location boxLoc : movableBoxes) { // place boxes
					newGrid.set(boxLoc.getX(), boxLoc.getY() + dir.y, '[');
					newGrid.set(boxLoc.getX() + 1, boxLoc.getY() + dir.y, ']');
				}
			}
		}
		if (newGrid.get(x, y) == '.') {
			newGrid.set(x, y - dir.y, '.');
			newGrid.set(x, y, '@');
			botLocWide = botLocWide.add(dir.x, dir.y);
		}
		return newGrid;
	}

	// recursively find all movable boxes.
	// if blocked returns empty set, otherwise the set of all left coordinates of boxes to be moved
	static Set<Location> findMovableBoxes(CharGrid grid, int x, int y, int diry) {
		if (grid.get(x, y) == ']') { // place us under left side
			x--;
		}
		// if wall above this box, return null == we can't move
		if (grid.get(x, y + diry) == '#' || grid.get(x + 1, y + diry) == '#') {
			return Collections.emptySet(); // can't move
		}
		if (grid.get(x, y + diry) == '.' && grid.get(x + 1, y + diry) == '.') {
			Set<Location> result = new HashSet<>();
			result.add(new Location(x, y, grid));
			return result;
		}
		// recurse:
		Set<Location> leftResult = new HashSet<>();
		if (grid.get(x, y + diry) == '[' || grid.get(x, y + diry) == ']') { // box above left side
			leftResult = findMovableBoxes(grid, x, y + diry, diry);
			if (leftResult.isEmpty()) {
				return leftResult;
			}
		}
		Set<Location> rightResult = new HashSet<>();
		if (grid.get(x + 1, y + diry) == '[') { // box above right side
			rightResult = findMovableBoxes(grid, x + 1, y + diry, diry);
			if (rightResult.isEmpty()) {
				return rightResult;
			}
		}
		leftResult.addAll(rightResult);
		leftResult.add(new Location(x, y, grid));
		return leftResult;
	}

	public void updateView(boolean part2, int step) {
		if (SPEED > 0) {
			model = new CharGrid(part2 ? gridWide : grid);
			modelHeader = "Part " + (part2 ? "2" : "1");
			modelFooter = "Step " + (step + 1) + "/" + instructions.length();
			repaint();
			try {
				Thread.sleep(SPEED);
			} catch (InterruptedException e) {
				// NOP
			}
		}
	}

	private void writeOutputGif() {
		if (WRITE_GIF) {
			try {
				ImageOutputStream output = new FileImageOutputStream(new File("D:\\scratch\\2024_15_02.gif"));
				GifSequenceWriter writer = new GifSequenceWriter(output, BufferedImage.TYPE_INT_RGB, 10, true);
				for (int i = 0; i < moves.size(); i++) {
					if (i % 1000 == 0) log.info("Frames written to gif: " + i + " / " + moves.size());
					BufferedImage img = new BufferedImage(VIEW_WIDTH, VIEW_HEIGHT, BufferedImage.TYPE_INT_RGB);
					model = moves.get(i);
					modelHeader = "Part 2";
					modelFooter = "Step " + (i == 0 ? "[start]" : i) + "/" + moves.size();
					paintComponent(img.getGraphics());
					writer.writeToSequence(img);
				}
				writer.close();
				output.close();
			} catch (IOException e) {
				// whatever
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		if (model != null) { // wait till ready
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.white);
			g2.fillRect(0, 0, VIEW_WIDTH, VIEW_HEIGHT);
			int SIZE = Math.min(VIEW_WIDTH / (model.getWidth() + 2 * MARGIN), VIEW_HEIGHT / (model.getHeight() + 2 * MARGIN));
			int xO = (VIEW_WIDTH - model.getWidth() * SIZE) / 2;
			int yO = (VIEW_HEIGHT - model.getHeight() * SIZE) / 2;

			for (int i = 0; i < model.getWidth(); i++) {
				for (int j = 0; j < model.getHeight(); j++) {
					char c = model.get(i, j);
					boolean glue = false;
					switch (c) {
						case '.':
							g2.setColor(Color.lightGray);
							break;
						case '[':
							glue = true;
						case 'O':
						case ']':
							g2.setColor(new Color(0x89, 0x51, 0x29));
							break;
						case '#':
							g2.setColor(Color.black);
							break;
						case '@':
							g2.setColor(Color.red);
					}
					g2.fillRect(xO + i * SIZE, yO + j * SIZE, SIZE - (glue ? 0 : 1), SIZE - 1);
				}
			}
			g2.setColor(Color.red);
			drawCenteredString(g2, modelHeader, new Rectangle(0, 0, VIEW_WIDTH, 10), DEFAULT_FONT);
			drawCenteredString(g2, modelFooter, new Rectangle(0, VIEW_HEIGHT - MARGIN / 2 * SIZE - 5, VIEW_WIDTH, 10), DEFAULT_FONT);
		}
	}

	private static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(VIEW_WIDTH, VIEW_HEIGHT));
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2024, 15));

		if (SPEED > 0) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
		}

		// Process input (6ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1495147 (376ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1524905 (595ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
