package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 10, title = "Adapter Array", keywords = {})
public class Exercise10 {

	ArrayList<String> lines;
	ArrayList<Long> ratings;
	HashMap<Integer, Long> sequences;
	long maxRating;

	public Exercise10() {
		lines = new ArrayList<>();
		ratings = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		long maxNum = Long.MIN_VALUE;
		ratings.add(0L);
		for (String line : lines) {
			long num = Long.parseLong(line);
			maxNum = Math.max(maxNum, num);
			ratings.add(num);
		}
		ratings.add(maxNum+3);
		maxRating = maxNum+3;
		ratings.sort(Long::compareTo);
	}

	public long findRatingDifferences() {
		Map<Long, Long> diffs = new HashMap<>();
		long oldRate = 0;
		for (int i = 1; i < ratings.size(); i++) {
			long diff = ratings.get(i) - ratings.get(i-1);
			diffs.put(diff, diffs.getOrDefault(diff, 0L) + 1);
		}
		return diffs.getOrDefault(1L, 0L) * diffs.getOrDefault(3L, 0L);
	}

	// Use with care. Will not terminate in timely fashion for too long sequences!
	public long findArrangementsNaive(int startIndex, int endIndex) {
		long result = 0;
		long startVal = ratings.get(startIndex);
		for (int i=0; i<3; i++) {
			if (endIndex > startIndex + i) {
				Long rating = ratings.get(startIndex + i);
				if (startVal <= rating && rating <= startVal + 3) {
					if (startIndex == endIndex-1) {
						result++;
					} else {
						long tmp = sequences.getOrDefault(startIndex + i + 1, findArrangementsNaive(startIndex + i + 1, endIndex));
						result += tmp;
					}
				}
			} else {
				break;
			}
		}
		sequences.put(startIndex, result);
		return result;
	}

	// whenever two successive adapters are 3 apart they must both be used. This way we can break up a large sequence
	// into two smaller ones that can (hopefully) be calculated in the naive way
	public BigInteger findArrangementsSmart() {
		BigInteger seqs = BigInteger.ONE;
		int seqStart = 0;
		for (int i = 0; i < ratings.size()-1; i++) {
			if (ratings.get(i+1) - ratings.get(i) == 3) { // 'bridge' - both MUST be used
				sequences = new HashMap<>();
				seqs = seqs.multiply(BigInteger.valueOf(findArrangementsNaive(seqStart, i+1)));
				seqStart = i+1;
			}
		}
		return seqs;
	}

	public BigInteger findTotalArrangements() {
		sequences = new HashMap<>();
		return findArrangementsSmart();
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2020, 10));

		// part 1 - 2100 (<0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.findRatingDifferences());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 16198260678656 (0.001 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.findTotalArrangements());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
