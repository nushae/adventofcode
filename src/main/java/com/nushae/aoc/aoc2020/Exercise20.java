package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.Tile;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 20, title = "Jurassic Jigsaw", keywords = {})
public class Exercise20 {

	public static boolean[][] DRAGON = new boolean[][] {
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false},
			{true, false, false, false, false, true, true, false, false, false, false, true, true, false, false, false, false, true, true, true},
			{false, true, false, false, true, false, false, true, false, false, true, false, false, true, false, false, true, false, false, false}
	};

	ArrayList<String> lines;
	HashMap<Integer, Tile> tiles;
	HashMap<Point, Tile> picture;
	int maxTiles;
	int minX;
	int minY;
	int picWidth;
	int picHeight;
	String searchArea;
	int total;

	public Exercise20() {
		lines = new ArrayList<>();
		tiles = new HashMap<>();
		picture = new HashMap<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		Tile tile = new Tile();
		int num = 0;
		for (String line : lines) {
			if (line.startsWith("Tile")) {
				tile.id = Integer.parseInt(line.split(" ")[1].substring(0,4));
			} else if ("".equals(line)) {
				tile.compile();
				tiles.put(num++, tile);
				tile = new Tile();
			} else { // image data
				tile.addline(line);
			}
		}
		tile.compile();
		tiles.put(num++, tile);
		maxTiles = num;
	}

	// input = 144 = 12x12 tiles, suggests a square layout
	public BigInteger doPart1() {
		minX = 0;
		minY = 0;
		int maxX = 0;
		int maxY = 0;
		Tile baseTile = tiles.get(0);
		picture.put(new Point(0, 0), baseTile);
		tiles.remove(0);
		int loop = 0;
		while (tiles.size() > 0 && loop < 50) {
			for (int tileNum = 0; tileNum < maxTiles; tileNum++) {
				if (tiles.containsKey(tileNum)) {
					Tile tl = tiles.get(tileNum);
					// try out all orientations of tl on every spot bordering a tile already in the picture
					boolean placed = false;
					for (Map.Entry<Point, Tile> picTile : picture.entrySet()) {
						Point p = picTile.getKey();
						Tile t = picTile.getValue();
						for (Tile tlo : tl.getOrients()) {
							if (!picture.containsKey(new Point(p.x + 1, p.y)) && t.matchToRight(tlo)) {
								placed = true;
								picture.put(new Point(p.x + 1, p.y), tlo);
								minX = Math.min(minX, p.x+1);
								minY = Math.min(minY, p.y);
								maxX = Math.max(maxX, p.x+1);
								maxY = Math.max(maxY, p.y);
								tiles.remove(tileNum);
								break;
							} else if (!picture.containsKey(new Point(p.x, p.y + 1)) && t.matchToBottom(tlo)) {
								placed = true;
								picture.put(new Point(p.x, p.y + 1), tlo);
								minX = Math.min(minX, p.x);
								minY = Math.min(minY, p.y+1);
								maxX = Math.max(maxX, p.x);
								maxY = Math.max(maxY, p.y+1);
								tiles.remove(tileNum);
								break;
							} else if (!picture.containsKey(new Point(p.x - 1, p.y)) && t.matchToLeft(tlo)) {
								placed = true;
								picture.put(new Point(p.x - 1, p.y), tlo);
								minX = Math.min(minX, p.x-1);
								minY = Math.min(minY, p.y);
								maxX = Math.max(maxX, p.x-1);
								maxY = Math.max(maxY, p.y);
								tiles.remove(tileNum);
								break;
							} else if (!picture.containsKey(new Point(p.x, p.y - 1)) && t.matchToTop(tlo)) {
								placed = true;
								picture.put(new Point(p.x, p.y - 1), tlo);
								minX = Math.min(minX, p.x);
								minY = Math.min(minY, p.y-1);
								maxX = Math.max(maxX, p.x);
								maxY = Math.max(maxY, p.y-1);
								tiles.remove(tileNum);
								break;
							}
						}
						if (placed) break;
					}
				}
			}
			loop++;
		}
		picWidth = maxX - minX + 1;
		picHeight = maxY - minY + 1;
		return BigInteger.valueOf(picture.get(new Point(minX, minY)).id)
				.multiply(BigInteger.valueOf(picture.get(new Point(maxX, minY)).id))
				.multiply(BigInteger.valueOf(picture.get(new Point(minX, maxY)).id))
				.multiply(BigInteger.valueOf(picture.get(new Point(maxX, maxY)).id));
	}

	// depends on part1!
	public int doPart2() {
		StringBuilder megapic = new StringBuilder();
		for (int y=0; y<picHeight; y++) {
			StringBuilder[] piclines = new StringBuilder[8];
			for (int i=0; i<8; i++) {
				piclines[i] = new StringBuilder();
			}
			for (int x=0; x<picWidth; x++) {
				Tile tl = picture.get(new Point(minX+x, minY+y));
				for (int i=0; i<8; i++) {
					piclines[i].append(tl.lines[i+1], 1, 9);
				}
			}
			for (int i=0; i<8; i++) {
				megapic.append(piclines[i].toString());
			}
		}

		searchArea = megapic.toString();
		total = countHashes(searchArea);

		boolean[][] dragon = DRAGON;
		processPicture(dragon);

		// this is the only one that matches for me:
		dragon = rotRight(dragon);
		processPicture(dragon);

		dragon = rotRight(dragon);
		processPicture(dragon);

		dragon = rotRight(dragon);
		processPicture(dragon);

		dragon = hflip(DRAGON);
		processPicture(dragon);

		dragon = rotRight(dragon);
		processPicture(dragon);

		dragon = rotRight(dragon);
		processPicture(dragon);

		dragon = rotRight(dragon);
		processPicture(dragon);

		return total;
	}

	public void processPicture(boolean[][] dragon) {
		String pattern = makePattern(dragon);
		int size = (dragon.length-1)*picWidth*8 + dragon[0].length;
		for (int i=0; i<searchArea.length()-size; i++) {
			if (searchArea.substring(i, i+size).matches(pattern)) {
				for (int dragRow = 0; dragRow < dragon.length; dragRow++) {
					for (int dragCol = 0; dragCol < dragon[0].length; dragCol++) {
						if (dragon[dragRow][dragCol]) {
							int pos = i+dragRow*picWidth*8+dragCol;
							if ("#".equals(searchArea.substring(pos, pos+1))) {
								searchArea = searchArea.substring(0, pos) + "O" + searchArea.substring(pos+1);
								total--;
							}
						}
					}
				}
			}
		}
	}

	public static boolean[][] hflip(boolean[][] source) {
		boolean[][] result = new boolean[source.length][source[0].length];
		for (int row = 0; row < source.length; row++) {
			for (int col = 0; col < source[0].length; col++) {
				result[row][source[0].length-1-col] = source[row][col];
			}
		}
		return result;
	}

	public static boolean[][] rotRight(boolean[][] source) {
		boolean[][] result = new boolean[source[0].length][source.length];
		for (int row = 0; row < source.length; row++) {
			for (int col = 0; col < source[0].length; col++) {
				result[col][source.length-1-row] = source[row][col];
			}
		}
		return result;
	}

	public String makePattern(boolean[][] source) {
		StringBuilder result = new StringBuilder();
		for (int row = 0; row < source.length; row++) {
			if (row > 0) {
				result.append(".{").append(picWidth*8-source[0].length).append("}");
			}
			for (int col = 0; col < source[0].length; col++) {
				if (source[row][col]) {
					result.append("[#O]");
				} else {
					result.append(".");
				}
			}
		}
		return result.toString();
	}

	public int countHashes(String picture) {
		int result = 0;
		for (int i=0; i<picture.length(); i++) {
			result += (picture.charAt(i) == '#' ? 1 : 0);
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2020, 20));

		// part 1 - 2699020245973 (0.05 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2012 (0.2 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
