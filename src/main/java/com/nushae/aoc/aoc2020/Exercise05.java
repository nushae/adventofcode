package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 5, title = "Binary Boarding", keywords = {})
public class Exercise05 {

	ArrayList<String> lines;
	HashMap<Point, Integer> seats;
	int id;
	int lowest;
	int highest;

	private Exercise05() {
		lines = new ArrayList<>();
		seats = new HashMap<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		for (String line : lines) {
			int num = toBSPNumber("F", "B", line.substring(0, 7));
			int pos = toBSPNumber("L", "R", line.substring(7, 10));
			seats.put(new Point(num, pos), num * 8 + pos);
		}
		int highest = -1;
		int lowest = Integer.MAX_VALUE;
		for (int v : seats.values()) {
			highest = Math.max(highest, v);
			lowest = Math.min(lowest, v);
		}
		this.highest = highest;
		this.lowest = lowest;
	}

	public int getHighestID() {
		return highest;
	}

	public int getFirstFreeID() {
		this.id = lowest-1;
		return seats.values().stream().sorted().filter(this::hasNoPredecessor).findFirst().orElse(0) - 1;
	}

	private boolean hasNoPredecessor(int id) {
		return this.id++ != id-1;
	}

	public static int toBSPNumber(String zero, String one, String num) {
		return Integer.parseInt(num.replaceAll(one, "1").replaceAll(zero, "0"), 2);
	}

	public static void main(String[] args) {
		// key here is to realise a BSP number is just a binary number with F/B or L/R rather than 1/0
		Exercise05 e5 = new Exercise05(aocPath(2020, 5));

		// part 1 - 880 (<0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(e5.getHighestID());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 731 (0.005 sec)
		start = System.currentTimeMillis();
		System.out.println(e5.getFirstFreeID());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
