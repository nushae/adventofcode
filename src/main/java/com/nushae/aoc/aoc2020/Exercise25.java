package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Aoc(year = 2020, day = 25, title = "Combo Breaker", keywords = {}) // WHAT NO ALLITERATION?!
public class Exercise25 {

	public static final long DIVNUM = 20201227; // prime number
	long door;
	long doorLoopSize;
	long card;
	long cardLoopSize;

	public Exercise25(long door, long card) {
		this.door = door;
		this.card = card;
		processInput();
	}

	private void processInput() {
		long subnum = 7;
		long value = 1;
		long loopSize = 0;
		while (value != card) {
			loopSize++;
			value = (value * subnum) % DIVNUM;
		}
		cardLoopSize = loopSize;
		value = 1;
		loopSize = 0;
		while (value != door) {
			loopSize++;
			value = (value * subnum) % DIVNUM;
		}
		doorLoopSize = loopSize;
	}

	public long doPart1() {
		long subnum = card;
		long value = 1;
		long count = 0;
		while (count < doorLoopSize) {
			count++;
			value = (value * subnum) % DIVNUM;
		}
		return value;
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(15733400, 6408062);

		// part 1 - 16457981 (0.014 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// No part 2, Merry Christmas!
	}
}
