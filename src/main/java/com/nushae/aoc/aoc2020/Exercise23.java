package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.Ring;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.util.HashMap;

@Log4j2
@Aoc(year = 2020, day = 23, title = "Crab Cups", keywords = {})
public class Exercise23 {

	HashMap<Integer, Ring<Integer>> labelsToCups;
	Ring<Integer> currentCup;
	int maxCup;

	public Exercise23() {
		labelsToCups = new HashMap<>();
		maxCup = 0;
	}

	public Exercise23(int... cups) {
		this();
		for (int c: cups) {
			Ring<Integer> newCup = new Ring<>(c);
			if (currentCup != null) {
				currentCup.append(newCup);
			}
			currentCup = newCup;
			labelsToCups.put(c, newCup);
			maxCup++;
		}
	}

	public String doPart1(int numMoves) {
		currentCup = currentCup.successor; // point it back at the first cup
		for (int move = 0; move < numMoves; move++) {
			int currentLabel = currentCup.value;
			Ring<Integer> first = currentCup.successor.remove();
			Ring<Integer> second = currentCup.successor.remove();
			Ring<Integer> third = currentCup.successor.remove();
			int destLabel = ((currentLabel - 1 + maxCup -1) % maxCup) + 1;
			Ring<Integer> destCup = labelsToCups.get(destLabel);
			while (destLabel == first.value || destLabel == second.value || destLabel == third.value) {
				destLabel = ((destLabel - 1 + maxCup -1) % maxCup) + 1;
				destCup = labelsToCups.get(destLabel);
			}
			destCup.append(first).append(second).append(third);
			currentCup = currentCup.successor;
		}

		System.out.println();

		StringBuilder result = new StringBuilder();
		Ring<Integer> output = labelsToCups.get(1).successor;
		while (output.value != 1) {
			result.append(output.value);
			output = output.successor;
		}
		return result.toString();
	}

	public BigInteger doPart2() {
		for (int c = maxCup+1; c <= 1000000; c++) {
			Ring<Integer> newCup = new Ring<>(c);
			currentCup.append(newCup);
			currentCup = newCup;
			labelsToCups.put(c, newCup);
		}
		maxCup = 1000000;
		doPart1(10000000);
		BigInteger val1 = BigInteger.valueOf(labelsToCups.get(1).successor.value);
		BigInteger val2 = BigInteger.valueOf(labelsToCups.get(1).successor.successor.value);
		return val1.multiply(val2);
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(1, 2, 3, 4, 8, 7, 5, 9, 6);

		// part 1 - 47598263 (< 0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(100));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 248009574232 (2.2 sec)
		ex = new Exercise23(1, 2, 3, 4, 8, 7, 5, 9, 6);
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
