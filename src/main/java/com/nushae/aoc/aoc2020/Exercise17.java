package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.Tuple;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 17, title = "Conway Cubes", keywords = {})
public class Exercise17 {

	ArrayList<String> lines;
	HashMap<Tuple, Boolean> cubes;
	HashMap<Tuple, Integer> neighbours;

	public Exercise17() {
		lines = new ArrayList<>();
	}

	public Exercise17(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise17(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput(int dimension) {
		cubes = new HashMap<>();
		neighbours = new HashMap<>();
		int y=0;
		for (String line : lines) {
			int x=0;
			for (String character: line.split("")) {
				if ("#".equals(character)) {
					Tuple t = dimension == 2 ? new Tuple(x, y) : dimension == 3 ? new Tuple(x, y, 0) : new Tuple(x, y, 0, 0);
					cubes.put(t, true);
					updateNeighbours(neighbours, t);
				}
				x++;
			}
			y++;
		}
	}

	public void updateNeighbours(HashMap<Tuple, Integer> map, Tuple key) {
		for (int dx = -1; dx < 2; dx++) {
			for (int dy = -1; dy < 2; dy++) {
				if (key.dimension > 2) {
					for (int dz = -1; dz < 2; dz++) {
						if (key.dimension > 3) {
							for (int dw = -1; dw < 2; dw++) {
								if (dx != 0 || dy != 0 || dz != 0 || dw != 0) {
									Tuple p = new Tuple(key.x + dx, key.y + dy, key.z + dz, key.w + dw);
									int n = map.getOrDefault(p, 0);
									map.put(p, n + 1);
								}
							}
						} else {
							if (dx != 0 || dy != 0 || dz != 0) {
								Tuple p = new Tuple(key.x + dx, key.y + dy, key.z + dz);
								int n = map.getOrDefault(p, 0);
								map.put(p, n + 1);
							}
						}
					}
				} else {
					if (dx != 0 || dy != 0) {
						Tuple p = new Tuple(key.x + dx, key.y + dy);
						int n = map.getOrDefault(p, 0);
						map.put(p, n + 1);
					}
				}
			}
		}
	}

	public void oneGeneration() {
		HashMap<Tuple, Boolean> newCubes = new HashMap<>();
		HashMap<Tuple, Integer> newNeighbours = new HashMap<>();
		for (Tuple key: neighbours.keySet()) {
			int value = neighbours.get(key);
			if (cubes.getOrDefault(key, false)) {
				if ((value == 2 || value == 3)) {
					// remain true
					newCubes.put(key, true);
					updateNeighbours(newNeighbours, key);
				}
			} else {
				if (value == 3) {
					newCubes.put(key, true);
					updateNeighbours(newNeighbours, key);
					// make true
				}
			}
		}
		cubes = newCubes;
		neighbours = newNeighbours;
	}

	public int doGameOfLife(int dimension) {
		processInput(dimension);
		for (int gen = 0; gen < 6; gen++) {
			oneGeneration();
		}
		return cubes.size();
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17(aocPath(2020, 17));

		// part 1 - 315 (0.01 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doGameOfLife(3));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1520 (0.5 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doGameOfLife(4));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
