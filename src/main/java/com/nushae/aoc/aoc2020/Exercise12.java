package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2020, day = 12, title = "Rain Risk", keywords = {})
public class Exercise12 {

	ArrayList<String> lines;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public int moveBoat(boolean useWaypoint) {
		Point facing = new Point(1, 0);
		Point wayPoint = new Point(10, -1);
		Point position = new Point(0, 0);
		for (String line : lines) {
			String command = line.substring(0,1);
			int value = Integer.parseInt(line.substring(1));
			switch (command) {
				case "N" -> {
					if (useWaypoint) {
						wayPoint = sum(wayPoint, new Point(0, -value));
					} else {
						position = sum(position, new Point(0, -value));
					}
				}
				case "S" -> {
					if (useWaypoint) {
						wayPoint = sum(wayPoint, new Point(0, value));
					} else {
						position = sum(position, new Point(0, value));
					}
				}
				case "E" -> {
					if (useWaypoint) {
						wayPoint = sum(wayPoint, new Point(value, 0));
					} else {
						position = sum(position, new Point(value, 0));
					}
				}
				case "W" -> {
					if (useWaypoint) {
						wayPoint = sum(wayPoint, new Point(-value, 0));
					} else {
						position = sum(position, new Point(-value, 0));
					}
				}
				case "F" -> {
					if (useWaypoint) {
						position = sum(position, scale(wayPoint, value));
					} else {
						position = sum(position, scale(facing, value));
					}
				}
				case "L" -> {
					facing = rot90L(value / 90, facing);
					wayPoint = rot90L(value / 90, wayPoint);
				}
				case "R" -> {
					facing = rot90R(value / 90, facing);
					wayPoint = rot90R(value / 90, wayPoint);
				}
				default -> throw new IllegalArgumentException("Unknown command: " + command);
			}
		}
		return manhattanDistance(position.x, position.y);

	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2020, 12));

		// part 1 - 938 (0.37 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.moveBoat(false));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 54404 (<0.001 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.moveBoat(true));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
