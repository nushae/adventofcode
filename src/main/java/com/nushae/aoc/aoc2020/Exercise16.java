package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.Rule;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 16, title = "Ticket Translation", keywords = {})
public class Exercise16 {

	private static final int RULES = 1;
	private static final int MY_TICKET = 2;

	ArrayList<String> lines;
	HashMap<String, Rule> rules;
	ArrayList<Integer> my_ticket;
	ArrayList<ArrayList<Integer>> tickets;
	boolean[] validNums;
	HashMap<String, ArrayList<Integer>> matchesPositions = new HashMap<>();

	public Exercise16() {
		lines = new ArrayList<>();
		rules = new HashMap<>();
		my_ticket = new ArrayList<>();
		tickets = new ArrayList<>();
		validNums = new boolean[10000];
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		int state = RULES;
		for (String line : lines) {
			if ("".equals(line)) {
				state++;
				continue;
			}
			if ("your ticket:".equals(line) || "nearby tickets:".equals(line)) {
				continue;
			}
			if (state == RULES) {
				Matcher matcher = Pattern.compile("([^:]*): ([0-9]+)-([0-9]+) or ([0-9]+)-([0-9]+)").matcher(line);
				if (matcher.find()) {
					Rule newRule = new Rule(
							matcher.group(1),
							Integer.parseInt(matcher.group(2)),
							Integer.parseInt(matcher.group(3)),
							Integer.parseInt(matcher.group(4)),
							Integer.parseInt(matcher.group(5))
					);
					rules.put(matcher.group(1), newRule);
					mergeRule(newRule); // for part 1
				}
			} else {
				ArrayList<Integer> numLine = new ArrayList<>();
				for (String num: line.split(",")) {
					numLine.add(Integer.parseInt(num));
				}
				if (state == MY_TICKET) {
					my_ticket = numLine;
				} else {
					tickets.add(numLine);
				}
			}
		}
	}

	public void mergeRule(Rule r) {
		for (int i = r.lower1; i <= r.upper1; i++) {
			validNums[i] = true;
		}
		for (int i = r.lower2; i <= r.upper2; i++) {
			validNums[i] = true;
		}
	}

	public int doPart1() {
		int accumulator = 0;
		for (ArrayList<Integer> ticket: new ArrayList<>(tickets)) {
			for (int number: ticket) {
				if (!validNums[number]) {
					accumulator += number;
					tickets.remove(ticket);
				}
			}
		}
		return accumulator;
	}

	public long doPart2() {
		long accumulator = 1;
		matchesPositions = new HashMap<>();
		for (String ruleName: rules.keySet()) {
			for (int pos = 0; pos < my_ticket.size(); pos++) {
				boolean ruleMatches = true;
				for (ArrayList<Integer> ticket : tickets) {
					ruleMatches = rules.get(ruleName).matches(ticket.get(pos));
					if (!ruleMatches) {
						break;
					}
				}
				if (ruleMatches) {
					ArrayList<Integer> positions = matchesPositions.getOrDefault(ruleName, new ArrayList<>());
					positions.add(pos);
					matchesPositions.put(ruleName, positions);
				}
			}
		}
		HashMap<String, Integer> identifiedRulePositions = new HashMap<>();
		matchesPositions.keySet().stream().sorted(this::byNumberOfMatches).forEach(s -> {
			ArrayList<Integer> positions = new ArrayList<>(matchesPositions.get(s));
			positions.removeAll(identifiedRulePositions.values()); // solvable iff one remains
			identifiedRulePositions.put(s, positions.get(0));
		});
		for (String ruleName : identifiedRulePositions.keySet()) {
			if (ruleName.startsWith("departure")) {
				accumulator *= my_ticket.get(identifiedRulePositions.get(ruleName));
			}
		}
		return accumulator;
	}

	public int byNumberOfMatches(String left, String right) {
		return Integer.compare(matchesPositions.get(left).size(), matchesPositions.get(right).size());
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2020, 16));

		// part 1 - 20048 (0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4810284647569 (0.007 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
