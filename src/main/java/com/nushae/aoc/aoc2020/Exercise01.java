package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 1, title = "Report Repair", keywords = {"bounded-linear-search"})
public class Exercise01 {

	private final List<String>lines;
	private final List<Long> numbers;

	private Exercise01() {
		lines = new ArrayList<>();
		numbers = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		lines.stream().mapToLong(Long::parseLong).forEach(numbers::add);
		numbers.sort(Long::compareTo);
	}

	// O(N^2) solution, improved by sorting list and stopping short when sum exceeds 2020
	long doPart1(long sum) {
		for (int first = 0; first < numbers.size(); first++) {
			long f = numbers.get(first);
			if (numbers.contains(sum-f)) {
				return (sum-f) * f;
			} else if (f > sum) {
				break;
			}
		}
		throw(new IllegalArgumentException("Sum " + sum + " not found"));
	}

	// O(N^3) solution, improved by sorting list and stopping short when sum exceeds 2020
	long doPart2(long sum) {
		for (int first = 0; first < numbers.size(); first++) {
			long f = numbers.get(first);
			for (int second = first+1; second < numbers.size(); second++) {
				long s = numbers.get(second);
				if (f + 2 * s + 1 > sum) {
					break;
				}
				for (int third = second + 1; third < numbers.size(); third++) {
					long t = numbers.get(third);
					if (f + s + t == sum) {
						return f * s * t;
					} else if (f + s + t > sum) {
						break;
					}
				}
			}
		}
		throw(new IllegalArgumentException("Sum 2020 not found"));
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2020, 1));

		// part 1 - 703131 (0.001s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(2020));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 272423970 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(2020));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
