package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 3, title = "Toboggan Trajectory", keywords = {})
public class Exercise03 {

	ArrayList<ArrayList<Boolean>> inputValues;
	int width;
	int height;

	private final List<String> lines;

	public Exercise03() {
		lines = new ArrayList<>();
		inputValues = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		width = Integer.MAX_VALUE;
		lines.forEach(this::processLine);
		height = inputValues.size();
	}

	private void processLine(String s) {
		ArrayList<Boolean> line = new ArrayList<>(Arrays.stream(s.split("")).map("#"::equals).toList());
		width = Math.min(width, line.size());
		inputValues.add(line);
	}

	public void printInput() {
		System.out.println(width + "x" + height);
		for (ArrayList<Boolean> line : inputValues) {
			for (boolean value : line) {
				if (value) {
					System.out.print("#");
				} else {
					System.out.print(".");
				}
			}
			System.out.println();
		}
	}

	public int countTrees(int slopeX, int slopeY) {
		int trees = 0;
		for (int x = 0, y = 0; y + slopeY < inputValues.size(); ) {
			x = (x + slopeX) % width;
			y += slopeY;
			trees += inputValues.get(y).get(x) ? 1 : 0;
		}
		return trees;
	}

	public static void main(String[] args) {
		Exercise03 e3 = new Exercise03(aocPath(2020, 3));

		// part 1 - 272 (0.001 sec)
		long start = System.currentTimeMillis();
		long second = e3.countTrees(3, 1);
		System.out.println(second);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		long first = e3.countTrees(1, 1);
		long third = e3.countTrees(5, 1);
		long fourth = e3.countTrees(7, 1);
		long fifth = e3.countTrees(1, 2);
		System.out.println("\nSlope <1, 1>: " + first);
		System.out.println("Slope <3, 1>: " + second);
		System.out.println("Slope <5, 1>: " + third);
		System.out.println("Slope <7, 1>: " + fourth);
		System.out.println("Slope <1, 2>: " + fifth);

		// part 2 - 3898725600 (0.001 sec)
		System.out.println("\nproduct: " + first*second*third*fourth*fifth);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
