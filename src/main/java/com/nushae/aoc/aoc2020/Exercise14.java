package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 14, title = "Docking Data", keywords = {})
public class Exercise14 {

	ArrayList<String> lines;
	HashMap<Long, BigInteger> memory;
	HashMap<String, BigInteger> memory2;
	String mask;

	public Exercise14() {
		lines = new ArrayList<>();
		memory = new HashMap<>();
		memory2 = new HashMap<>();
		mask = "";
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void readValuesToMemory(boolean addressMask) {
		for (String line : lines) {
			if (line.startsWith("mask")) {
				mask = line.substring(7);
			} else {
				Matcher matcher = Pattern.compile("mem\\[([0-9]+)] = ([0-9]+)").matcher(line);
				while (matcher.find()) {
					if (addressMask) {
						doMem2(matcher.group(1), matcher.group(2));
					} else {
						long address = Long.parseLong(matcher.group(1));
						BigInteger value = applyBitMask(new BigInteger(matcher.group(2)));
						memory.put(address, value);
					}
				}
			}
		}
	}

	public BigInteger applyBitMask(BigInteger startingValue) {
		BigInteger result = BigInteger.ZERO;
		BigInteger power = BigInteger.ONE;
		for (int i = 0; i < mask.length(); i++) {
			String symbol = mask.substring(mask.length()-1-i, mask.length()-i);
			if ("X".equals(symbol)) {
				result = result.add(startingValue.mod(BigInteger.valueOf(2)).multiply(power));
			} else {
				BigInteger bit = new BigInteger(symbol).multiply(power);
				result = result.add(bit);
			}
			power = power.multiply(BigInteger.valueOf(2));
			startingValue = startingValue.divide(BigInteger.valueOf(2));
		}
		return result;
	}

	public void doMem2(String address, String value) {
		StringBuilder addressMask = new StringBuilder();
		address = Integer.toBinaryString(Integer.parseInt(address));
		// merge address and mask
		for (int i = 0; i < mask.length(); i++) {
			String symbolM = mask.substring(mask.length()-1-i, mask.length()-i);
			String symbolA;
			if (i < address.length()) {
				symbolA = address.substring(address.length() - 1 - i, address.length() - i);
			} else {
				symbolA = "0";
			}
			switch (symbolM) {
				case "0" -> addressMask.insert(0, symbolA);
				case "1" -> addressMask.insert(0, "1");
				default -> addressMask.insert(0, "X");
			}
		}
		// generate target addresses
		ArrayList<String> addressesToWrite = new ArrayList<>();
		addressesToWrite.add(addressMask.toString());
		while (addressesToWrite.get(0).contains("X")) {
			ArrayList<String> addyNew = new ArrayList<>();
			int location = addressesToWrite.get(0).indexOf("X");
			for (String addy: addressesToWrite) {
				String first = addy.substring(0,location);
				String second = addy.substring(location+1);
				addyNew.add(first+"1"+second);
				addyNew.add(first+"0"+second);
			}
			addressesToWrite = addyNew;
		}
		// actually write value to addresses
		for (String addy: addressesToWrite) {
			memory2.put(addy, new BigInteger(value));
		}
	}

	public static BigInteger sum(BigInteger left, BigInteger right) {
		return left.add(right);
	}

	public BigInteger doPart1() {
		readValuesToMemory(false);
		return memory.values().stream().reduce(Exercise14::sum).orElse(BigInteger.ZERO);
	}

	public BigInteger doPart2() {
		readValuesToMemory(true);
		return memory2.values().stream().reduce(Exercise14::sum).orElse(BigInteger.ZERO);
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2020, 14));

		// part 1 - 15172047086292 (0.019 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4197941339968 (0.073 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
