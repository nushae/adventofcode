package com.nushae.aoc.aoc2020.gameconsole;

public class State {
	public int pointer, acc;
	public String stateCode, errorMessage;

	public State(int pointer, String code, int acc) {
		this(pointer, code, acc, "");
	}

	public State(int pointer, String code, int acc, String errorMessage) {
		this.pointer = pointer;
		this.acc = acc;
		this.stateCode = code;
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "[" + pointer + "," + stateCode + "," + acc + ("".equals(errorMessage) ? "" : "," + errorMessage) + "]";
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof State)) {
			return false;
		}
		State otherState = (State) other;
		return pointer == otherState.pointer && acc == otherState.acc && stateCode.equals(otherState.stateCode);
	}

	@Override
	public int hashCode() {
		return 37*pointer+13*acc+11*stateCode.hashCode();
	}
}
