package com.nushae.aoc.aoc2020.gameconsole;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameConsole {

	private static final String ACC = "acc";
	private static final String JMP = "jmp";
	private static final String NOP = "nop";
	private static final String STATE_HALT = "hlt";
	private static final String STATE_LOOP = "lop";
	private static final String STATE_ILLEGAL = "ill";

	private static final boolean VERBOSE = false;

	List<Command> commandList;
	int pointer;
	int accumulator;
	HashSet<Integer> visited;

	public GameConsole(ArrayList<String> programlines) {
		commandList = new ArrayList<>();
		for (String line : programlines) {
			Matcher matcher = Pattern.compile("(nop|acc|jmp)\\s([+-][0-9]+)").matcher(line);
			while (matcher.find()) {
				String command = matcher.group(1);
				int arg = Integer.parseInt(matcher.group(2));
				commandList.add(new Command(command, arg));
			}
		}
	}

	public State executeWithLoopDetection() {
		pointer = 0;
		accumulator = 0;
		visited = new HashSet<>();
		while (true) {
			if (pointer >= 0 && pointer < commandList.size()) {
				if (visited.contains(pointer)) { // loop!
					return new State(pointer, STATE_LOOP, accumulator);
				}
				visited.add(pointer);
				Command cmd = commandList.get(pointer);
				switch (cmd.name) {
					case NOP:
						pointer++;
						break;
					case JMP:
						pointer += cmd.arg;
						break;
					case ACC:
						accumulator += cmd.arg;
						pointer++;
						break;
					default:
						return new State(pointer, STATE_ILLEGAL, accumulator, "Unknown command " + cmd);
				}
			} else {
				if (pointer > commandList.size()-1) {
					return new State(pointer, STATE_HALT, accumulator);
				} else {
					return new State(pointer, STATE_ILLEGAL, accumulator, "Command pointer underflow");
				}
			}
		}
	}

	public State executeWithLoopFix() {
		ArrayList<Command> commandListBackup = new ArrayList<>(commandList);
		for (int i=0; i< commandListBackup.size(); i++) {
			Command cmd = commandListBackup.get(i);
			commandList = new ArrayList<>(commandListBackup);
			if (cmd.name.equals(JMP)) {
				commandList.set(i, new Command(NOP, cmd.arg));
			} else if (cmd.name.equals(NOP)) {
				commandList.set(i, new Command(JMP, cmd.arg));
			} else {
				continue;
			}
			State state = executeWithLoopDetection();
			if (state.stateCode.equals(STATE_HALT)) {
				return state;
			} else if (VERBOSE) {
				System.out.println(i + ": " + state);
			}
		}
		return new State(-1, STATE_ILLEGAL, -1, "Program can't be fixed");
	}
}
