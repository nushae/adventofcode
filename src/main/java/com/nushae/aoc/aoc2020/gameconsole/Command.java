package com.nushae.aoc.aoc2020.gameconsole;

public class Command {
	public String name;
	public int arg;

	public Command(String name, int arg) {
		this.name = name;
		this.arg = arg;
	}
}
