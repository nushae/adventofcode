package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 21, title = "Allergen Assessment", keywords = {})
public class Exercise21 {

	ArrayList<String> lines;
	HashMap<String, ArrayList<String>> allergies;
	HashMap<String, String> foundAllergies;
	HashMap<String, Integer> foods;

	public Exercise21() {
		lines = new ArrayList<>();
		allergies = new HashMap<>();
		foundAllergies = new HashMap<>();
		foods = new HashMap<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		for (String line : lines) {
			String[] parts = line.substring(0, line.length()-1).split(" \\(contains ");
			ArrayList<String> frontParts = new ArrayList<>(Arrays.asList(parts[0].split(" ")));
			for (String part: frontParts) {
				foods.put(part, foods.getOrDefault(part, 0)+1);
			}
			for (String allergy: parts[1].split(", ")) {
				ArrayList<String> foodsWithAllergy = merge(allergies.getOrDefault(allergy, new ArrayList<>()), frontParts);
				allergies.put(allergy, foodsWithAllergy);
			}
		}
		while (allergies.size() > 0) {
			HashMap<String, ArrayList<String>> newAllergies = new HashMap<>();
			for (String allergy : allergies.keySet()) {
				ArrayList<String> foods = allergies.get(allergy);
				foods.removeAll(foundAllergies.keySet());
				if (foods.size() == 1) {
					foundAllergies.put(foods.get(0), allergy);
				} else {
					newAllergies.put(allergy, foods);
				}
			}
			allergies = newAllergies;
		}
	}

	public ArrayList<String> merge(ArrayList<String> left, ArrayList<String> right) {
		if (left.size() == 0) return right;
		if (right.size() == 0) return left;
		ArrayList<String> both = new ArrayList<>();
		for (String entry: left) {
			if (right.contains(entry)) {
				both.add(entry);
			}
		}
		return both;
	}

	public int doPart1() {
		int total = 0;
		for (String key: foods.keySet()) {
			if (foundAllergies.containsKey(key)) {
				System.out.println(key + " has " + foundAllergies.get(key));
			} else {
				total += foods.get(key);
			}
		}
		return total;
	}

	public String doPart2() {
		return foundAllergies.keySet().stream().sorted(this::byAllergen).collect(Collectors.joining(","));
	}

	public int byAllergen(String food1, String food2) {
		return foundAllergies.get(food1).compareTo(foundAllergies.get(food2));
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2020, 21));

		// part 1 - 2635 (<0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - xncgqbcp,frkmp,qhqs,qnhjhn,dhsnxr,rzrktx,ntflq,lgnhmx (0.004 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
