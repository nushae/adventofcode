package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 13, title = "Shuttle Search", keywords = {})
public class Exercise13 {

	ArrayList<String> lines;
	ArrayList<Long> busIDs;
	long earliest;
	BigInteger fullProduct;

	public Exercise13() {
		lines = new ArrayList<>();
		busIDs = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		earliest = Long.parseLong(lines.get(0));
		fullProduct = BigInteger.ONE;
		for (String busID : lines.get(1).split(",")) {
			if (!"x".equals(busID)) {
				busIDs.add(Long.parseLong(busID));
				fullProduct = fullProduct.multiply(new BigInteger(busID));
			} else {
				busIDs.add(-1L);
			}
		}
	}

	/**
	 * <p>Given a series of periods [p<sub>0</sub>, ..., p<sub>N-1</sub>] and a starting integer S, finds the smallest integer E
	 * so that: &exist;<sub>0 &le; i &lt; N</sub>(p<sub>i</sub>|E).</p>
	 * <p>Note that for any p<sub>i</sub>, we have that p<sub>i</sub>| (S - S mod p<sub>i</sub>) by definition of the mod operator.</p>
	 * <p>It follows that p<sub>i</sub> | (S - S mod p<sub>i</sub> + p<sub>i</sub>) as well, and will be the smallest integer &ge; S that p<sub>i</sub> divides.</p>
	 * <p>We simply calculate this value for all p<sub>i</sub> and determine their minimum.</p>
	 * @return p<sub>i</sub> * (E-S)
	 */
	public long doPart1() {
		long nearestID = 0;
		long nearestDepart = Integer.MAX_VALUE;
		for (long busID : busIDs) {
			if (busID > 0) {
				long nextDepart = (earliest + busID - (earliest % busID));
				if (nearestDepart > nextDepart) {
					nearestDepart = nextDepart;
					nearestID = busID;
				}
			}
		}
		return nearestID * (nearestDepart - earliest);
	}

	/**
	 * <p>Given a series of N integers [id<sub>0</sub>, ..., id<sub>N-1</sub>] finds the smallest positive integer <code>timestamp</code> so that for all 0&le;i&lt;N, id<sub>i</sub> divides <code>timestamp</code>+i</p>
	 * <p>Derivation of the algorithm:</p>
	 * <p>Note that if we have a partial match P for a, b, c so that a|P &and; b|(P+1) &and; c|(P+2),
	 * it is also true that a|(P+lcm(a,b,c) &and; b|(P+1+lcm(a,b,c)) &and; c|(P+2+lcm(a,b,c)) since each is a divisor of both terms.</p>
	 * <p>This is true for any number of id's, so also for the whole series, and it follows that 0 &le; <code>timestamp</code> &lt; lcm(all id's).</p>
	 * <p>In this particular assignment, whenever an 'x' appears as an id, we can substitute a 1 and the above will still hold.
	 * Furthermore, since all other id's are prime, <code>lcm(id's)</code> reduces to <code>&prod;(id's)</code>.</p>
	 * <p>So we use this as an invariant:</p>
	 * <p>(1) <b><code>(&forall;<sub>0 &le; i &lt; diff</sub>id(i)|(timestamp+i))</code></b> (for 0 &le; diff < N, and where any id 'x' is replaced with a 1)</p>
	 * <p>Now suppose we have <code>0 &lt; diff-1 &lt; N</code>, and <code>id<sub>0</sub>|timestamp &and; ... &and; id<sub>diff-1</sub>|(timestamp+diff-1)</code>.</p>
	 * <ol>
	 *     <li>if <code>id(diff)|(timestamp+diff)</code>, we can increase diff and the invariant remains true.</li>
	 *     <li>if not, we can increase timestamp by the product of id(0) through id(<code>diff</code>-1), keeping the invariant true,
	 * until it does. (This is guaranteed to happen since all id's are prime)</li>
	 * </ol>
	 * <p>We can calculate the product on the fly with a second invariant:</p>
	 * <p>(2) <code>lcmFactors = &prod;(id<sub>0 &le; i &lt; diff</sub>)</code></p>
	 * <p>As we increase <code>diff</code> by 1, <code>lcmFactors</code> must be multiplied by id(<code>diff</code>-1) to keep the invariant true.</p>
	 * <p>Initialization <code>timestamp = 0, diff = 0, lcmFactors = 1</code> establishes both invariants, and <code>diff = N</code>
	 * establishes the answer in <code>timestamp</code>.</p>
	 * @return timestamp
	 */
	public BigInteger doPart2() {
		BigInteger timestamp = BigInteger.ZERO;
		BigInteger lcmFactors = BigInteger.ONE;
		for (int diff = 0; diff < busIDs.size();) {
			if (busIDs.get(diff) < 0) {
				 diff++; // treat x positions as a 1
				 continue;
			}
			BigInteger factor = BigInteger.valueOf(busIDs.get(diff));
			if (timestamp.add(BigInteger.valueOf(diff)).mod(factor).equals(BigInteger.ZERO)) {
				lcmFactors = lcmFactors.multiply(factor); // all factors are prime, so lcm = product
				diff++;
			} else {
				timestamp = timestamp.add(lcmFactors);
			}
		}
		return timestamp;
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2020, 13));

		// part 1 - 2165 (<0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 534035653563227 (0.001 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}