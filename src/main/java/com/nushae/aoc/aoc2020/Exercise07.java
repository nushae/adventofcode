package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.BagContents;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 7, title = "Handy Haversacks", keywords = {})
public class Exercise07 {

	ArrayList<String> lines;
	BagContents bagContents;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		bagContents = new BagContents("root");
		for (String line : lines) {
			String[] bagRule = line.split(" bags contain ");
			BagContents extra;
			String extraName = bagRule[0].trim();
			if (!bagContents.hasChild(extraName)) {
				log.debug("Creating new child " + extraName + ", adding children...");
				extra = new BagContents(extraName);
				bagContents.addChild(1, extra);
			} else {
				log.debug(extraName + " was previously added, adding children...");
				extra = bagContents.getChild(extraName);
			}
			log.debug("->> parsing " + bagRule[1] + "<<-");
			Matcher matcher = Pattern.compile("([0-9]+)\\s+(.*?)\\s+bag[s.,]").matcher(bagRule[1]);
			while (matcher.find()) {
				BagContents child;
				if (!bagContents.hasChild(matcher.group(2))) {
					log.debug("Creating new child " + matcher.group(2));
					child = new BagContents(matcher.group(2));
					bagContents.addChild(1, child);
				} else {
					child = bagContents.getChild(matcher.group(2));
				}
				extra.addChild(Integer.parseInt(matcher.group(1)), child);
			}
		}
	}

	public int calculatePossibleContainersOf(String type) {
		int result = 0;
		for (BagContents bag: bagContents.getChildren()) {
			result += bag.canContain(type) ? 1 : 0;
		}
		return result;
	}

	public int calculateContentSize(String type) {
		for (BagContents child: bagContents.getChildren()) {
			if (type.equals(child.getName())) {
				return child.getMandatoryContentSize() - 1;
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2020, 7));

		// part 1 - 259 (0.051 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.calculatePossibleContainersOf("shiny gold"));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 45018 (<0.001 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.calculateContentSize("shiny gold"));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
