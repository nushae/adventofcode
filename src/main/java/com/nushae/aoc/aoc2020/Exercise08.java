package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.gameconsole.GameConsole;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 8, title = "Handheld Halting", keywords = {})
public class Exercise08 {

	ArrayList<String> lines;
	GameConsole gc;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		gc = new GameConsole(lines);
	}

	public int executeUntilLoop() {
		return gc.executeWithLoopDetection().acc;
	}

	public int executeWithFix() {
		return gc.executeWithLoopFix().acc;
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2020, 8));

		// part 1 - 1727 (0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.executeUntilLoop());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 552 (0.008 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.executeWithFix());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
