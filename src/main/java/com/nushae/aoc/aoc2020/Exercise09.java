package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 9, title = "Encoding Error", keywords = {})
public class Exercise09 {

	ArrayList<String> lines;
	ArrayList<Long> nums;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	public void processInput() {
		nums = new ArrayList<>();
		for (String line : lines) {
			nums.add(Long.parseLong(line));
		}
	}

	public long doPart1(int preamble) {
		ArrayList<Long> numWindow = new ArrayList<>();
		for (long num : nums) {
			if (numWindow.size() < preamble) {
				numWindow.add(num);
			} else {
				boolean added = false;
				for (int i = 0; i<preamble; i++) {
					if (numWindow.contains(num - numWindow.get(i))) { // valid
						numWindow.remove(0);
						numWindow.add(num);
						added = true;
						break;
					}
				}
				if (!added) { // invalid
					return num;
				}
			}
		}
		return -1;
	}

	// just bruteforce it, fuck it.
	public long doPart2(long sum) {
		for (int low=0; low<nums.size()-1; low++) {
			for (int high = low+1; high < nums.size(); high++) {
				long total = 0;
				long minimum = Long.MAX_VALUE;
				long maximum = Long.MIN_VALUE;
				for (int i = low; i < high+1; i++) {
					minimum = Math.min(minimum, nums.get(i));
					maximum = Math.max(maximum, nums.get(i));
					total += nums.get(i);
				}
				if (sum == total) {
					return minimum + maximum;
				}
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2020, 9));

		// part 1 - 29221323 (0.001 sec)
		long start = System.currentTimeMillis();
		long sum = ex.doPart1(25);
		System.out.println(sum);
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4389369 (0.2 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(sum));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
