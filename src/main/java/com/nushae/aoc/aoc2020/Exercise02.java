package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 2, title = "Password Philosophy", keywords = {})
public class Exercise02 {

	private final ArrayList<String> passwords;

	private Exercise02() {
		passwords = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(passwords, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(passwords, data);
	}

	public int countValidPasswords() {
		return passwords.stream()
				.mapToInt(s -> {
					int colonSep = s.indexOf(":");
					String range = s.substring(0, colonSep).trim();
					String password = s.substring(colonSep+1).trim();
					char character = range.charAt(colonSep-1);
					range = range.substring(0, colonSep-1).trim();
					int dashSep = s.indexOf("-");
					int lower = Integer.parseInt(range.substring(0, dashSep));
					int upper = Integer.parseInt(range.substring(dashSep+1));
					return testPassword(lower, upper, character, password);
				}).sum();
	}

	public int countValidPasswords2() {
		return passwords.stream()
				.mapToInt(s -> {
					int colonSep = s.indexOf(":");
					String range = s.substring(0, colonSep).trim();
					String password = s.substring(colonSep+1).trim();
					char character = range.charAt(colonSep-1);
					range = range.substring(0, colonSep-1).trim();
					int dashSep = s.indexOf("-");
					int lower = Integer.parseInt(range.substring(0, dashSep));
					int upper = Integer.parseInt(range.substring(dashSep+1));
					return testPassword2(lower, upper, character, password);
				}).sum();
	}

	public int testPassword(int lower, int upper, char character, String password) {
		int count = 0;
		for (int i=0; i<password.length(); i++) {
			if (password.charAt(i) == character) {
				count++;
			}
		}
		return (lower <= count && count <= upper) ? 1 : 0;
	}

	public int testPassword2(int lower, int upper, char character, String password) {
		return (password.charAt(lower-1) == character ? 1 : 0) + (password.charAt(upper-1) == character ? 1 : 0) == 1 ? 1 : 0;
	}

	public static void main(String[] args) {
		Exercise02 e2 = new Exercise02(aocPath(2020, 2));

		// part 1 - 424 (0.071 sec)
		long start = System.currentTimeMillis();
		System.out.println(e2.countValidPasswords());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 747 (0.001 sec)
		start = System.currentTimeMillis();
		System.out.println(e2.countValidPasswords2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}

}
