package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.Token;
import com.nushae.aoc.aoc2020.domain.Tokenizer;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 18, title = "Operation Order", keywords = {})
public class Exercise18 {

	ArrayList<String> lines;
	Tokenizer tokenizer;
	Token lookahead;
	LinkedList<Token> tokens;
	BigInteger accumulator;
	boolean equalPrecedence;

	public Exercise18() {
		lines = new ArrayList<>();
		tokenizer = new Tokenizer();
		tokenizer.addRecipe("\\+", Token.TokenType.ADD);
		tokenizer.addRecipe("\\*", Token.TokenType.MULTIPLY);
		tokenizer.addRecipe("\\(", Token.TokenType.LBRACK);
		tokenizer.addRecipe("\\)", Token.TokenType.RBRACK);
		tokenizer.addRecipe("[0-9]+", Token.TokenType.NUMBER);
		tokenizer.addRecipe("[ \\t\\x0B\\f\\r]+", Token.TokenType.WHITESPACE);
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		accumulator = BigInteger.ZERO;
		for (String line : lines) {
			tokenizer.tokenize(line);
			accumulator = accumulator.add(parse(tokenizer.getTokens()));
		}
	}

	public BigInteger parse(LinkedList<Token> tokens) {
		BigInteger result;
		this.tokens = (LinkedList<Token>) tokens.clone();
		lookahead = this.tokens.getFirst();
		result = sum();
		if (lookahead != null) {
			error("Remaining tokens at the end...");
		}
		return result;
	}

	private void error(String message) {
		System.out.println(message);
		System.out.println("Remaining input:");
		for (Token tok : tokens) {
			System.out.print(tok.token);
			if (null != tok.sequence) {
				System.out.print("(" + tok.sequence + ")");
			}
			System.out.print(" ");
		}
		System.out.println();
		System.exit(0);
	}

	public BigInteger sum() {
		BigInteger result = factor();
		if (equalPrecedence) {
			while (lookahead != null && (lookahead.token == Token.TokenType.ADD || lookahead.token == Token.TokenType.MULTIPLY)) {
				Token operator = anyOp();
				if (operator.token == Token.TokenType.ADD) {
					result = result.add(term());
				} else {
					result = result.multiply(term());
				}
			}
		} else {
			while (lookahead != null && lookahead.token == Token.TokenType.MULTIPLY) {
				nextToken();
				result = result.multiply(factor());
			}
		}
		return result;
	}

	public BigInteger factor() {
		BigInteger result = term();
		while (lookahead != null && lookahead.token == Token.TokenType.ADD) {
			nextToken();
			result = result.add(term());
		}
		return result;
	}

	public BigInteger term() {
		BigInteger result;
		if (lookahead.token == Token.TokenType.LBRACK) {
			terminal(Token.TokenType.LBRACK);
			result = sum();
			terminal(Token.TokenType.RBRACK);
		} else {
			result = number();
		}
		return result;
	}

	public Token anyOp() {
		Token result = null;
		if (lookahead.token == Token.TokenType.ADD || lookahead.token == Token.TokenType.MULTIPLY) {
			result = lookahead;
			nextToken();
		} else {
			error("Operator expected");
		}
		return result;
	}

	public BigInteger number() {
		BigInteger result = BigInteger.ZERO;
		if (lookahead.token == Token.TokenType.NUMBER) {
			result = new BigInteger(lookahead.sequence);
			nextToken();
		} else {
			error("Expected a NUMBER");
		}
		return result;
	}

	private void terminal(Token.TokenType termtype) {
		if (lookahead.token == termtype) {
			nextToken();
		} else {
			error("Expected a '" + termtype.name + "' token");
		}
	}

	private void nextToken() {
		tokens.pop();
		// ignore whitespace:
		while (tokens.size() > 0 && tokens.getFirst().token == Token.TokenType.WHITESPACE) {
			tokens.pop();
		}
		if (tokens.isEmpty()) {
			lookahead = null;
		} else {
			lookahead = tokens.getFirst();
		}
	}

	public BigInteger doPart1() {
		accumulator = BigInteger.ZERO;
		equalPrecedence = true;
		processInput();
		return accumulator;
	}

	public BigInteger doPart2() {
		accumulator = BigInteger.ZERO;
		equalPrecedence = false;
		processInput();
		return accumulator;
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2020, 18));

		// part 1 - 701339185745 (0.03 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4208490449905 (0.02 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
