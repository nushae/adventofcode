package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 11, title = "Seating System", keywords = {})
public class Exercise11 {

	ArrayList<String> lines;
	int width;
	int height;
	int maxDim;
	HashMap<Point, Boolean> seats;
	HashMap<Point, ArrayList<Point>> neighboursOf;
	boolean stable;
	HashMap<Point, Integer> occupiedNeighboursOf;

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	public void processInput() {
		width = lines.get(0).length();
		height = lines.size();
		maxDim = Math.max(width, height);
		seats = new HashMap<>();
		neighboursOf = new HashMap<>();
		occupiedNeighboursOf = new HashMap<>();
		int y=0;
		for (String line : lines) {
			int x=0;
			for (String character : line.split("")) {
				if ("L".equals(character)) {
					seats.put(new Point(x, y), false);
				} else if ("#".equals(character)) {
					seats.put(new Point(x, y), false);
				}
				x++;
			}
			y++;
		}
	}

	public int findOccupiedAfterStabilized(int limit, boolean useLineOfSight) {
		int occupied;
		if (useLineOfSight) {
			calculateNeighboursLOS();
		} else {
			calculateNeighbours();
		}
		do {
			stable = true;
			occupied = 0;
			HashMap<Point, Integer> occupiedNew = new HashMap<>();
			HashMap<Point, Boolean> seatsNew = new HashMap<>();
			for (Point p : seats.keySet()) {
				int occNeigh = occupiedNeighboursOf.getOrDefault(p, 0);
				if (!seats.get(p) && occNeigh == 0) { // flip to occupied
					seatsNew.put(p, true);
					stable = false;
				} else if (seats.get(p) && occNeigh >= limit) { // flip to empty
					seatsNew.put(p, false);
					stable = false;
				} else {
					seatsNew.put(p, seats.get(p));
				}
				if (seatsNew.get(p)) {
					occupied++;
					updateNeighbours(p, occupiedNew);
				}
			}
			seats = seatsNew;
			occupiedNeighboursOf = occupiedNew;
		} while (!stable);
		return occupied;
	}

	private void calculateNeighboursLOS() {
		for (Point p: seats.keySet()) {
			neighboursOf.put(p, new ArrayList<>());
			for (int dx=-1; dx<2; dx++) {
				for (int dy=-1; dy<2; dy++) {
					if (dx != 0 || dy != 0) {
						for (int i = 1; i < maxDim; i++) {
							int newX = p.x + i * dx;
							int newY = p.y + i * dy;
							Point pn = new Point(newX, newY);
							if (newX >= 0 && newX < width && newY >= 0 && newY < height && seats.containsKey(pn)) {
								ArrayList<Point> nb = neighboursOf.get(p);
								nb.add(pn);
								neighboursOf.put(p, nb);
								break;
							}
						}
					}
				}
			}
		}
	}

	private void calculateNeighbours() {
		for (Point p: seats.keySet()) {
			neighboursOf.put(p, new ArrayList<>());
			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					Point pn = new Point(p.x + i, p.y + j);
					if ((i != 0 || j != 0) && p.x + i >= 0 && p.x + i < width && p.y + j >= 0 && p.y + j < height && seats.containsKey(p)) {
						ArrayList<Point> nb = neighboursOf.get(p);
						nb.add(pn);
						neighboursOf.put(p, nb);
					}
				}
			}
		}
	}

	public void updateNeighbours(Point p, HashMap<Point, Integer> occupiedNew) {
		for (Point pn : neighboursOf.get(p)) {
			occupiedNew.put(pn, occupiedNew.getOrDefault(pn, 0) + 1);
		}
	}

	private void printSeats() {
		for (int y=0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Point p = new Point(x, y);
				System.out.print(!seats.containsKey(p) ? "." : seats.get(p) ? "#" : "L");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2020, 11));

		// part 1 - 2354 (0.27 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.findOccupiedAfterStabilized(4, false));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2072 (0.21 sec)
		ex = new Exercise11(aocPath(2020, 11));
		start = System.currentTimeMillis();
		System.out.println(ex.findOccupiedAfterStabilized(5, true));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
