package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.Passport;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 4, title = "Passport Processing", keywords = {})
public class Exercise04 {

	private final List<String> lines;

	int totalPassports;
	int weakValidPassports;
	int validPassports;
	Passport passport;

	private Exercise04() {
		lines = new ArrayList<>();
		passport = new Passport();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		validPassports = 0;
		weakValidPassports = 0;
		totalPassports = 0;
		passport = new Passport();
		lines.forEach(this::processLine);
		validPassports += passport.validate();
		weakValidPassports += passport.validateWeak();
		System.out.println("Part 1: " + weakValidPassports);
		System.out.println("Part 2: " + validPassports);
	}

	private void processLine(String s) {
		if ("".equals(s)) {
			totalPassports++;
			weakValidPassports += passport.validateWeak();
			validPassports += passport.validate();
			passport = new Passport();
		} else {
			tokenize(s);
		}
	}

	public void tokenize(String s) {
		String[] tokens = s.split("\\s+");
		for (String t : tokens) {
			passport.setProperty(t.split(":"));
		}
	}

	public static void main(String[] args) {
		// part 1 & 2 are done in parallel
		// part 1 - 230
		// part 2 - 156
		// (0.058 sec)
		long start = System.currentTimeMillis();
		Exercise04 e4 = new Exercise04(aocPath(2020, 4));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
