package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.loadInputFromData;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
@Aoc(year = 2020, day = 15, title = "Rambunctious Recitation", keywords = {})
public class Exercise15 {

	ArrayList<String> lines;
	HashMap<Long, long[]> numberHistory;
	long totalNumbers;
	long lastNumSpoken;

	public Exercise15() {
		lines = new ArrayList<>();
		numberHistory = new HashMap<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		for (String line : lines) {
			for (String num: line.split(",")) {
				long n = Long.parseLong(num);
				long[] history = numberHistory.getOrDefault(n, new long[2]);
				lastNumSpoken = n;
				totalNumbers++;
				history[0] = history[1];
				history[1] = totalNumbers;
				numberHistory.put(n, history);
			}
		}
	}

	public long findNthNumberCalled(long order) {
		long diff = -1;
		while (totalNumbers < order) {
			long[] history = numberHistory.get(lastNumSpoken);
			if (history[0] == 0) { // new number
				diff = 0;
			} else {
				diff = history[1] - history[0];
			}
			lastNumSpoken = diff;
			totalNumbers++;
			long[] diffHistory = numberHistory.getOrDefault(diff, new long[2]);
			diffHistory[0] = diffHistory[1];
			diffHistory[1] = totalNumbers;
			numberHistory.put(diff, diffHistory);
			if (totalNumbers == order) break;
		}
		return diff;
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15("1,2,16,19,18,0");

		// part 1 - 536 (0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.findNthNumberCalled(2020));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 24065124 (3.6 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.findNthNumberCalled(30000000));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
