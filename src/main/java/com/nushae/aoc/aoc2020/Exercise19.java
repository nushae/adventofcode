package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 19, title = "Monster Messages", keywords = {})
public class Exercise19 {

	ArrayList<String> lines;
	HashMap<Integer, String> rules;
	ArrayList<String> messages;
	HashMap<Integer, String> ruleCache;

	public Exercise19() {
		lines = new ArrayList<>();
		messages = new ArrayList<>();
		rules = new HashMap<>();
		ruleCache = new HashMap<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		boolean msg = false;
		for (String line : lines) {
			if ("".equals(line)) {
				msg = true;
				continue;
			}
			if (msg) {
				messages.add(line);
				continue;
			}
			// rules
			String[] tmp = line.split(": ");
			String front = tmp[0];
			String back = tmp[1];
			rules.put(Integer.parseInt(front), " " + back + " ");
		}
	}

	public int doPart1(int maxdepth) {
		String ruleZero = "^"+ruleResolve(0, 0, maxdepth)+"$";
		int accum=0;
		for (String msg: messages) {
			if (msg.matches(ruleZero)) {
				accum++;
			}
		}
		return accum;
	}

	public String ruleResolve(int ruleNum, int depth, int maxdepth) {
		if (ruleCache.containsKey(ruleNum)) {
			return ruleCache.get(ruleNum);
		}
		String rule = rules.get(ruleNum);

		// leaf rule
		if (rule.contains("\"")) {
			return rule.replaceAll("\"", "").trim();
		}

		if (depth > maxdepth) {
			// insert nonrecursive version for part 2
			// rule 8
			if (ruleNum == 8) {
				return "42";
			}
			// rule 11
			if (ruleNum == 11) {
				return "42 31";
			}
		}

		// base case
		StringBuilder result = new StringBuilder("(");
		for (String part : rule.trim().split(" ")) {
			if (Character.isDigit(part.charAt(0))) { // number
				result.append(ruleResolve(Integer.parseInt(part), depth+1, maxdepth));
			} else {
				result.append("|");
			}
		}
		result.append(")");
		ruleCache.put(ruleNum, result.toString());
		return result.toString();
	}

	public static void main(String[] args) {
		// part 1 - 111 (0.1 sec)
		long start = System.currentTimeMillis();
		Exercise19 ex = new Exercise19(aocPath(2020, 19));
		System.out.println(ex.doPart1(1));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 343 (0.5 sec)
		ex = new Exercise19(aocPath(2020, 19));
		start = System.currentTimeMillis();
		System.out.println(ex.doPart1(15));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
