package com.nushae.aoc.aoc2020;

import com.nushae.aoc.aoc2020.domain.HexCoordinate;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 24, title = "Lobby Layout", keywords = {})
public class Exercise24 extends JPanel {

	private static final int BLACK = 0;
	private static final int WHITE = 1;
	private static final boolean SHOW_NEIGHBOURS = false;
	private static final boolean ANIMATE = false;
	private static final Font FONT = new Font("Dubai", Font.BOLD, 8);
	private final int HEX_SIZE = 4;
	private final double HEX_SPACING = 0.25;
	private final Point RELATIVE_CENTER = new Point(800, 500);

	ArrayList<String> lines;
	HashMap<HexCoordinate, Integer> tiles;
	HashMap<HexCoordinate, Integer> neighbours;
	int blackTiles;

	public Exercise24() {
		lines = new ArrayList<>();
		neighbours = new HashMap<>();
		tiles = new HashMap<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		blackTiles = 0;
		for (String line : lines) {
			HexCoordinate p = new HexCoordinate(0, 0);
			while (line.length() > 0) {
				if (line.startsWith("e") || line.startsWith("w")) {
					p = p.neighbour(line.substring(0, 1));
					line = line.substring(1);
				} else {
					p = p.neighbour(line.substring(0, 2));
					line = line.substring(2);
				}
			}
			int oldColor = tiles.getOrDefault(p, WHITE);
			tiles.put(p, 1-oldColor);
			if (oldColor == BLACK) {
				blackTiles--;
			} else {
				blackTiles++;
			}
			if (!neighbours.containsKey(p)) {
				neighbours.put(p, 0);
			}
			updateNeighbours(p, 1-oldColor);
		}
	}

	public void updateNeighbours(HexCoordinate p, int toColor) {
		for (String dir: new String[] {"e", "ne", "se", "w", "nw", "sw"}) {
			HexCoordinate pneigh = p.neighbour(dir);
			int nCount = neighbours.getOrDefault(pneigh, 0);
			if (toColor == BLACK) {
				nCount++;
			} else {
				nCount--;
			}
			neighbours.put(pneigh, nCount);
		}
	}

	public int doPart1() {
		return blackTiles;
	}

	// again game of life...
	public int doPart2(int turns) {
		for (int t = 0; t<turns; t++) {
			HashMap<HexCoordinate, Integer> newNeighbours = new HashMap<>();
			for (HexCoordinate p : neighbours.keySet()) {
				int nCount = neighbours.get(p);
				int oldColor = tiles.getOrDefault(p, WHITE);
				if (oldColor == BLACK) {
					if (nCount == 1 || nCount == 2) {  // stays black; only update neighbours
						if (!newNeighbours.containsKey(p)) {
							newNeighbours.put(p, 0);
						}
						for (String dir: new String[] {"e", "ne", "se", "w", "nw", "sw"}) {
							HexCoordinate pneigh = p.neighbour(dir);
							newNeighbours.put(pneigh, newNeighbours.getOrDefault(pneigh, 0)+1);
						}
					} else {
						tiles.put(p, WHITE);
						blackTiles--;
					}
				} else if (oldColor == WHITE && nCount == 2) {
					tiles.put(p, BLACK);
					if (!newNeighbours.containsKey(p)) {
						newNeighbours.put(p, 0);
					}
					blackTiles++;
					for (String dir: new String[] {"e", "ne", "se", "w", "nw", "sw"}) {
						HexCoordinate pneigh = p.neighbour(dir);
						newNeighbours.put(pneigh, newNeighbours.getOrDefault(pneigh, 0)+1);
					}
				}
			}
			neighbours = newNeighbours;
			if (ANIMATE) {
				repaint();
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// NOP
				}
			}
		}
		return blackTiles;
	}

	private Shape makeHexAt(int centerX, int centerY) {
		int[] hexX = new int[6];
		int[] hexY = new int[6];
		for (int i=0; i<6; i++) {
			hexX[i] = (int) Math.round(centerX + HEX_SIZE * Math.cos(i * Math.PI/3));
			hexY[i] = (int) Math.round(centerY + HEX_SIZE * Math.sin(i * Math.PI/3));
		}
		return new Polygon(hexX, hexY, 6);
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		Graphics2D g2 = (Graphics2D) g;
		for (HexCoordinate h: tiles.keySet()) {
			Point p = toPixel(h.x, h.y);
			g2.setColor(tiles.get(h) == BLACK ? Color.black : Color.lightGray);
			g2.fill(makeHexAt(p.x, p.y));
			if (h.x ==0 && h.y ==0) {
				g2.setColor(Color.red);
				g2.draw(makeHexAt(p.x, p.y));
			}
		}

		if (SHOW_NEIGHBOURS) {
			for (HexCoordinate h : neighbours.keySet()) {
				Point p = toPixel(h.x, h.y);
				g2.setColor(Color.green);
				drawCenteredString(g2, "" + neighbours.get(h), new Rectangle(p.x - 2, p.y - 2, 4, 4), FONT);
			}
		}
	}

	private Point toPixel(int hexX, int hexY) {
		int actualSize = (int) Math.round((HEX_SPACING + 1) * HEX_SIZE);
		int pixelX = (int) Math.round(actualSize * ( 3.0/2 * hexX));
		int pixelY = (int) Math.round(actualSize * Math.sqrt(3) * (hexX / 2.0 +  hexY));
		return new Point(RELATIVE_CENTER.x + pixelX, RELATIVE_CENTER.y + pixelY);
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2020, 24));

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1 - 420 (<0.001 sec)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4206 (0.2 sec)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(100));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}

	private static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1600, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
