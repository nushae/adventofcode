package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2020, day = 22, title = "Crab Combat", keywords = {})
public class Exercise22 {

	private static final boolean REPORT = false;

	ArrayList<String> lines;
	ArrayList<ArrayList<Integer>> cards;

	public Exercise22() {
		lines = new ArrayList<>();
		cards = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		ArrayList<Integer> cardList = new ArrayList<>();
		for (String line : lines) {
			if ("".equals(line)) {
				cards.add(cardList);
				cardList = new ArrayList<>();
			} else if (!line.startsWith("P")) {
				cardList.add(Integer.parseInt(line));
			}
		}
		cards.add(cardList);
		if (REPORT) {
			System.out.println("Player 1: " + cards.get(0) + "\nPlayer 2: " + cards.get(1) + "\n");
		}
	}

	public BigInteger calcScore(ArrayList<Integer> cards) {
		BigInteger accum = BigInteger.ZERO;
		int cardNum = cards.size();
		for (int card: cards) {
			accum = accum.add(BigInteger.valueOf(cardNum * card));
			cardNum--;
		}
		return accum;
	}

	public BigInteger doWarGame(ArrayList<ArrayList<Integer>> decks) {
		ArrayList<Integer> player1 = new ArrayList<>(decks.get(0));
		ArrayList<Integer> player2 = new ArrayList<>(decks.get(1));
		while (player1.size() > 0 && player2.size() > 0) {
			if (REPORT) {
				System.out.print(player1 + " " + player2);
			}
			int card1 = player1.get(0);
			player1.remove(0);
			int card2 = player2.get(0);
			player2.remove(0);
			if (card1 > card2) {
				if (REPORT) {
					System.out.println("<");
				}
				player1.add(card1);
				player1.add(card2);
			} else {
				if (REPORT) {
					System.out.println(">");
				}
				player2.add(card2);
				player2.add(card1);
			}
		}
		if (player1.size() > 0) {
			return calcScore(player1);
		}
		return calcScore(player2).negate();
	}

	public BigInteger doRecursiveWarGame(ArrayList<ArrayList<Integer>> decks, int gameDepth) {
		ArrayList<Integer> player1 = new ArrayList<>(decks.get(0));
		ArrayList<Integer> player2 = new ArrayList<>(decks.get(1));
		ArrayList<String> roundStates = new ArrayList<>();
		// one round:
		while (player1.size() > 0 && player2.size() > 0) {
			String roundState = StringUtils.join(player1, ",") + "/" + StringUtils.join(player2, ",");
			if (REPORT) {
				System.out.print("[" + gameDepth + "] = " + roundState);
			}
			if (roundStates.contains(roundState)) {
				if (REPORT) {
					System.out.println(" is repeat, 1 wins");
				}
				return calcScore(player1);
			}
			roundStates.add(roundState);
			int card1 = player1.get(0);
			player1.remove(0);
			int card2 = player2.get(0);
			player2.remove(0);
			if (card1 <= player1.size() && card2 <= player2.size()) { // recurse
				if (REPORT) {
					System.out.println(" recursing...");
				}
				ArrayList<ArrayList<Integer>> newDecks = new ArrayList<>();
				newDecks.add(new ArrayList<>(player1.subList(0, card1)));
				newDecks.add(new ArrayList<>(player2.subList(0, card2)));
				if (doRecursiveWarGame(newDecks, gameDepth+1).compareTo(BigInteger.ZERO) > 0) {
					if (REPORT) {
						System.out.println("[" + gameDepth + "] so 1 wins round");
					}
					player1.add(card1);
					player1.add(card2);
				} else {
					if (REPORT) {
						System.out.println("[" + gameDepth + "] so 2 wins round");
					}
					player2.add(card2);
					player2.add(card1);
				}
			} else {
				if (card1 > card2) {
					if (REPORT) {
						System.out.println(" 1 wins round");
					}
					player1.add(card1);
					player1.add(card2);
				} else {
					if (REPORT) {
						System.out.println(" 2 wins round");
					}
					player2.add(card2);
					player2.add(card1);
				}
			}
		}
		if (player1.size() > 0) {
			return calcScore(player1);
		}
		return calcScore(player2).negate();
	}

	public BigInteger doPart1() {
		return doWarGame(cards);
	}

	public BigInteger doPart2() {
		return doRecursiveWarGame(cards, 1);
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(aocPath(2020, 22));

		// part 1 - 35562 (0.001 sec without REPORT on, 0.009 sec otherwise)
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 34424 (3.2 sec without REPORT on, 9.7 sec otherwise)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
