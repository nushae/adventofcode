package com.nushae.aoc.aoc2020;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.intersection;
import static com.nushae.aoc.util.StringUtil.union;

@Log4j2
@Aoc(year = 2020, day = 6, title = "Custom Customs", keywords = {})
public class Exercise06 {

	ArrayList<String> lines;
	ArrayList<ArrayList<String>> groups;
	ArrayList<String> group;

	public Exercise06() {
		lines = new ArrayList<>();
		groups = new ArrayList<>();
		group = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
		processInput();
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
		processInput();
	}

	private void processInput() {
		for (String line : lines) {
			if ("".equals(line)) {
				groups.add(group);
				group = new ArrayList<>();
				continue;
			}
			group.add(line);
		}
		groups.add(group);
	}

	public int countQuestionsUnion() {
		int accumulator = 0;
		for (ArrayList<String> group : groups) {
			String groupAccumulator = "";
			for (String person : group) {
				groupAccumulator = union(groupAccumulator, person);
			}
			accumulator += groupAccumulator.length();
		}
		return accumulator;
	}

	public int countQuestionsIntersection() {
		int accumulator = 0;
		for (ArrayList<String> group : groups) {
			String groupAccumulator = "abcdefghijklmnopqrstuvwxyz";
			for (String person : group) {
				groupAccumulator = intersection(groupAccumulator, person);
			}
			accumulator += groupAccumulator.length();
		}
		return accumulator;
	}

	public static void main(String[] args) {
		Exercise06 e6 = new Exercise06(aocPath(2020, 6));

		// part 1 - 6170 (0.032 sec)
		long start = System.currentTimeMillis();
		System.out.println(e6.countQuestionsUnion());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2947 (0.011 sec)
		start = System.currentTimeMillis();
		System.out.println(e6.countQuestionsIntersection());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
