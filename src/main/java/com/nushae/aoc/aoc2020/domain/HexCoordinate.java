package com.nushae.aoc.aoc2020.domain;

// Flat topped hex-coordinates
// So e1 = (0, -4), e5 = (0,0), a1 = (-4,0), a5 = (-4,4), e9 = (0,4), i5 = (4,0) and i1 = (4,-4)
public class HexCoordinate {
	public final int x;
	public final int y;

	public HexCoordinate(int column, int row) {
		x = column;
		y = row;
	}

	public HexCoordinate neighbour(String dir) {
		switch (dir) {
			case "ne":
				return new HexCoordinate(x + 1, y);
			case "e":
				return new HexCoordinate(x + 1, y - 1);
			case "se":
				return new HexCoordinate(x, y - 1);
			case "sw":
				return new HexCoordinate(x - 1, y);
			case "w":
				return new HexCoordinate(x - 1, y + 1);
			case "nw":
				return new HexCoordinate(x, y + 1);
			default:
				throw new IllegalArgumentException(("Unknown direction - " + dir));
		}
	}

	public String toString() {
		return "<" + x + "," + y + ">";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof HexCoordinate)) {
			return false;
		}
		HexCoordinate rhs = (HexCoordinate) obj;
		return x == rhs.x && y == rhs.y;
	}

	@Override
	public int hashCode() {
		return 23*x+37*y;
	}
}
