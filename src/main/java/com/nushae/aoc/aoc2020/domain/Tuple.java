package com.nushae.aoc.aoc2020.domain;

public class Tuple {
	public int dimension;
	public int x, y, z, w;

	public Tuple(int x, int y, int z, int w) {
		this.dimension = 4;
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	public Tuple(int x, int y, int z) {
		this.dimension = 3;
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = 0;
	}

	public Tuple(int x, int y) {
		this.dimension = 2;
		this.x = x;
		this.y = y;
		this.z = 0;
		this.w = 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Tuple other = (Tuple) obj;
		if (dimension != other.dimension) {
			return false;
		}
		return x == other.x && y == other.y && z == other.z && w == other.w;
	}


	@Override
	public int hashCode() {
		return 13 * x + 19 * y + 3 * z + 7 * w;
	}
}
