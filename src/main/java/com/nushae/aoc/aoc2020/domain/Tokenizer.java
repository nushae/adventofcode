package com.nushae.aoc.aoc2020.domain;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer {
	private LinkedList<TokenInfo> tokenInfos;
	private LinkedList<Token> tokens;

	private class TokenInfo {
		public final Pattern regex;
		public final Token.TokenType token;

		public TokenInfo(Pattern regex, Token.TokenType token) {
			super();
			this.regex = regex;
			this.token = token;
		}
	}

	public Tokenizer() {
		tokenInfos = new LinkedList<TokenInfo>();
		tokens = new LinkedList<Token>();
	}

	public void addRecipe(String regex, Token.TokenType token) {
		tokenInfos.add(new TokenInfo(Pattern.compile("^(" + regex + ")"), token));
	}

	public LinkedList<Token> getTokens() {
		return tokens;
	}

	public void tokenize(String str) {
		tokens.clear();
		while (!"".equals(str)) {
			boolean match = false;
			for (TokenInfo info : tokenInfos) {
				Matcher m = info.regex.matcher(str);
				if (m.find()) {
					match = true;
					String tok = m.group().trim();
					tokens.add(new Token(info.token, tok));
					str = m.replaceFirst("");
					break;
				}
			}
			if (!match) {
				throw new IllegalArgumentException("Unexpected character in input:\n" + str + "\n^");
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder("[");
		for (Token t : tokens) {
			output.append(t.toString()).append(", ");
		}
		output.delete(output.length()-2, output.length());
		output.append("]");
		return output.toString();
	}
}