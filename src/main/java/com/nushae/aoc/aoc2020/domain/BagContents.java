package com.nushae.aoc.aoc2020.domain;

import java.util.*;

public class BagContents {

	String name;
	HashMap<BagContents, Integer> children;

	public BagContents(String name) {
		this.name = name;
		children = new HashMap<>();
	}

	public void addChild(int count, BagContents child) {
		children.put(child, count);
	}

	public ArrayList<BagContents> getChildren() {
		return new ArrayList<>(children.keySet());
	}

	public boolean canContainDirectly(String type) {
		boolean result = false;
		for (BagContents bag : children.keySet()) {
			result = type.equals(bag.name);
			if (result) break;
		}
		return result;
	}

	public boolean canContain(String type) {
		return reachable(Collections.emptySet(), type);
	}

	private boolean reachable(Set<String> from, String  target) {
		if (from.contains(name)) { // loop detected
			System.out.println("Loop detected");
			return false;
		}
		boolean result = canContainDirectly(target);
		if (!result) { // recurse
			for (BagContents child: children.keySet()) {
				HashSet<String> newFrom = new HashSet<>();
				newFrom.add(name);
				newFrom.addAll(from);
				result = child.reachable(newFrom, target);
				if (result) break;
			}
		}
		return result;
	}

	public int getMandatoryContentSize() {
		int result = 1;
		for (BagContents child : children.keySet()) {
			int tmp = child.getMandatoryContentSize();
			result += children.get(child) * tmp;
		}
		return result;
	}

	public BagContents getChild(String name) {
		for (BagContents child : children.keySet()) {
			if (child.name.equals(name)) {
				return child;
			}
		}
		return null;
	}

	public boolean hasChild(String name) {
		for (BagContents child : children.keySet()) {
			if (child.name.equals(name)) {
				return true;
			}
		}
		return false;
	}

	public String getName() {
		return name;
	}
}