package com.nushae.aoc.aoc2020.domain;

public class Token {
	public enum TokenType {
		LBRACK(1, "left bracket", false),
		NUMBER(2, "number", true),
		RBRACK(3, "right bracket", false),
		ADD(4, "add", false),
		MULTIPLY(4, "multiply", false),
		WHITESPACE(5, "whitespace", false);

		public final String name;
		public final int id;
		public final boolean hasSequence;

		TokenType(int id, String name, boolean storeSequence) {
			this.id = id;
			this.name = name;
			this.hasSequence = storeSequence;
		}
	}

	public final TokenType token;
	public final String sequence;

	public Token(TokenType token, String sequence) {
		super();
		this.token = token;
		if (token.hasSequence) {
			this.sequence = sequence;
		} else {
			this.sequence = null;
		}
	}

	public String toString() {
		return token.name + (token.hasSequence ? "(" + sequence + ")" : "");
	}
}