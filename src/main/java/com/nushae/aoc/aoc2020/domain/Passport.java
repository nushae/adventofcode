package com.nushae.aoc.aoc2020.domain;

import java.util.HashMap;

public class Passport {

	private class Validator {
		public int value;
		public String pattern;

		public Validator(int value, String pattern) {
			this.value = value;
			this.pattern = pattern;
		}
	}

	public static final HashMap<String, Validator> codes = new HashMap<>();
	{
		codes.put("byr", new Validator(1, "(19[2-9][0-9]|200[0-2])"));
		codes.put("iyr", new Validator(2, "20(1[0-9]|20)"));
		codes.put("eyr", new Validator(4, "20(2[0-9]|30)"));
		codes.put("hgt", new Validator(8, "(1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in"));
		codes.put("hcl", new Validator(16, "#[0-9a-f]{6}"));
		codes.put("ecl", new Validator(32, "amb|blu|brn|gry|grn|hzl|oth"));
		codes.put("pid", new Validator(64, "[0-9]{9}"));
		codes.put("cid", new Validator(128, ".*"));
	}

	private int allRequiredPresent;
	private int allRequiredPresentAndValid;

	public Passport() {
		allRequiredPresent = 0;
		allRequiredPresentAndValid = 0;
	}

	// we don't actually store the property.... yet. Exercise 4 doesn't require it, but who knows...
	public void setProperty(String[] keyVal) {
		if (codes.containsKey(keyVal[0])) {
			Validator v = codes.get(keyVal[0]);
			allRequiredPresent |= v.value;
			allRequiredPresentAndValid |= (keyVal[1].matches(v.pattern) ? v.value : 0);
		}
	}

	public int validateWeak() {
		return (allRequiredPresent == 255 || allRequiredPresent == 127) ? 1 : 0;
	}

	public int validate() {
		return (allRequiredPresentAndValid == 255 || allRequiredPresentAndValid == 127) ? 1 : 0;
	}
}
