package com.nushae.aoc.aoc2020.domain;

public class Rule {
	public String name;
	public int lower1, lower2, upper1, upper2;

	public Rule(String name, int lower1, int upper1, int lower2, int upper2) {
		this.name = name;
		this.lower1 = lower1;
		this.upper1 = upper1;
		this.lower2 = lower2;
		this.upper2 = upper2;
	}

	public boolean matches(int test) {
		return lower1 <= test && upper1 >= test || lower2 <= test && upper2 >= test;
	}

	@Override
	public String toString() {
		return name + ": " + lower1 + "-" + upper1 + " / " + lower2 + "-" + upper2;
	}
}
