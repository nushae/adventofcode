package com.nushae.aoc.aoc2020.domain;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class Tile {
	public int id;
	public final String[] lines = new String[10];
	private int linecount = 0;
	public String topBorder;
	public String botBorder;
	public String leftBorder;
	public String rightBorder;
	private ArrayList<Tile> myOrients;

	public void addline(String line) {
		lines[linecount++] = line;
	}

	public void compile() {
		StringBuilder left = new StringBuilder();
		StringBuilder right = new StringBuilder();
		topBorder = lines[0];
		for (String line : lines) {
			left.append(line.charAt(0));
			right.append(line.charAt(9));
		}
		leftBorder = left.toString();
		rightBorder = right.toString();
		botBorder = lines[9];
	}

	public ArrayList<Tile> getOrients() {
		if (myOrients == null) {
			myOrients = new ArrayList<>();
			myOrients.add(this);
			myOrients.add(this.horflip());
			Tile tl = this.rotRight();
			myOrients.add(tl);
			myOrients.add(tl.horflip());
			tl = tl.rotRight();
			myOrients.add(tl);
			myOrients.add(tl.horflip());
			tl = tl.rotRight();
			myOrients.add(tl);
			myOrients.add(tl.horflip());
		}
		return myOrients;
	}

	public Tile horflip() {
		Tile tl = new Tile();
		tl.id = id;
		for (String line : lines) {
			tl.addline(StringUtils.reverse(line));
		}
		tl.compile();
		return tl;
	}

	public Tile rotRight() {
		Tile tl = new Tile();
		tl.id = id;
		StringBuilder rotLine = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				rotLine.append(lines[9 - j].charAt(i));
			}
			tl.addline(rotLine.toString());
			rotLine = new StringBuilder();
		}
		tl.compile();
		return tl;
	}

	public boolean matchToLeft(Tile other) {
		return leftBorder.equals(other.rightBorder);
	}

	public boolean matchToRight(Tile other) {
		return rightBorder.equals(other.leftBorder);
	}

	public boolean matchToTop(Tile other) {
		return topBorder.equals(other.botBorder);
	}

	public boolean matchToBottom(Tile other) {
		return botBorder.equals(other.topBorder);
	}

	public String toString() {
		return StringUtils.join(lines, "\n");
	}
}
