package com.nushae.aoc.aoc2020.domain;

// This class models a series of one or more valued objects that are linked in a ring
public class Ring<T> {
	public Ring<T> predecessor;
	public Ring<T> successor;

	public final T value;

	public Ring(T value) {
		this.value = value;
		predecessor = this;
		successor = this;
	}

	public Ring<T> descendant(int cups) {
		Ring<T> result = this;
		for (int i = 0; i < cups; i++) {
			result = result.successor;
		}
		return result;
	}

	public Ring<T> ancestor(int count) {
		Ring<T> result = this;
		for (int i = 0; i < count; i++) {
			result = result.predecessor;
		}
		return result;
	}

	public Ring<T> remove() {
		predecessor.successor = successor;
		successor.predecessor = predecessor;
		return this;
	}

	public Ring<T> append(Ring<T> newRing) {
		newRing.predecessor = this;
		newRing.successor = successor;
		successor.predecessor = newRing;
		this.successor = newRing;
		return newRing;
	}

	public Ring<T> insert(Ring<T> newRing) {
		newRing.successor = this;
		newRing.predecessor = predecessor;
		predecessor.successor = newRing;
		this.predecessor = newRing;
		return newRing;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder("[").append(value.toString());
		Ring<T> output = this.successor;
		while (!this.equals(output)) {
			result.append(",").append(output.value);
		}
		return result.append("]").toString();
	}
}
