package com.nushae.aoc.view;

import com.nushae.aoc.domain.Model;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;

import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static java.awt.BasicStroke.CAP_BUTT;
import static java.awt.BasicStroke.JOIN_ROUND;

public class ElementDisplayPanel<T extends Element> extends JPanel {

	private static final int WIDTH = 800;

	private static final Color[] COLORS = {
			new Color(0, 0, 255),
			new Color(255, 0, 0),
			new Color(255, 0, 128),
			new Color(255, 0, 255),
			new Color(255, 128, 0),
			new Color(128, 0, 255),
			new Color(0, 255, 0),
			new Color(128, 255, 0),
			new Color(255, 255, 0),
			new Color(0, 255, 255)
	};

	private static final AffineTransform SCALE_TO_PANEL;
	static {
		SCALE_TO_PANEL = new AffineTransform();
		SCALE_TO_PANEL.setToIdentity();
		SCALE_TO_PANEL.scale(WIDTH, WIDTH);
	}

	private static final AffineTransform ROT90;
	static {
		ROT90 = new AffineTransform();
		ROT90.translate(0.5, 0.5);
		ROT90.rotate(Math.toRadians(90));
		ROT90.translate(-0.5, -0.5);
	}

	private static BasicStroke STROKE = new BasicStroke(0.009f, CAP_BUTT, JOIN_ROUND);

	private static final Font FONT_LABELS = new Font("Monospaced", Font.PLAIN, 10);

	private Model<T> myModel;
	private Rectangle gridDimension;
	private AffineTransform gridScale;

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(WIDTH + 51, WIDTH + 51);
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(WIDTH + 51, WIDTH + 51);
	}

	public void setModel(Model<T> m) {
		myModel = m;
	};

	public void setGridDimension(Rectangle boundingBox) {
		int furthestExtent = 5 + Math.max(Math.max(Math.abs(boundingBox.x), Math.abs(boundingBox.x + boundingBox.width)), Math.max(Math.abs(boundingBox.y), Math.abs(boundingBox.y + boundingBox.height)));

		boundingBox.x = -furthestExtent;
		boundingBox.y = -furthestExtent;
		boundingBox.width = 2*furthestExtent;
		boundingBox.height = 2*furthestExtent;

		gridDimension = boundingBox;
		gridScale = new AffineTransform();
		gridScale.setToIdentity();
		gridScale.scale(1.0/boundingBox.getWidth(), 1.0/boundingBox.getHeight());
	}

	@Override
	public void paintComponent(Graphics g) {
		Dimension d = getSize();
		Graphics2D g2 = (Graphics2D) g;
		g2.translate((d.getWidth()-WIDTH-1)/2, (d.getHeight()-WIDTH-1)/2);

		// Background with some lines
		g2.setColor(Color.white);
		g2.fillRect(0, 0, WIDTH + 1, WIDTH + 1);

		for (int i=0; i < WIDTH + 1; i += 50) {
			g2.setColor(new Color(200, 200, 255));
			g2.drawLine(0, i, WIDTH, i);
			g2.drawLine(i, 0, i, WIDTH);
			g2.setColor(Color.black);
			drawCenteredString(g2, "" + (gridDimension.getY() + 1.0*i/WIDTH * gridDimension.getHeight()), new Rectangle(-25, i - 15, 40, 40), FONT_LABELS);
			drawCenteredString(g2, "" + (gridDimension.getX() + 1.0*i/WIDTH * gridDimension.getWidth()), new Rectangle(i - 15, -25, 40, 40), FONT_LABELS);
		}

		g2.transform(SCALE_TO_PANEL);
		g2.transform(gridScale);
		g2.translate(-gridDimension.getX(), -gridDimension.getY());
		g2.setStroke(STROKE);

		g2.fillRect(-1, -1, 2, 2);
		g2.setColor(new Color(128, 128, 128));
		g2.drawLine((int) gridDimension.getX(), 0, (int) (gridDimension.getX() + gridDimension.getWidth()), 0);
		g2.drawLine(0, (int) gridDimension.getY(), 0, (int) (gridDimension.getY() + gridDimension.getHeight()));

		int num = 0;
		for (T t : myModel.getElements()) {
			g2.setColor(COLORS[num++ % 10]);
			t.display(g2);
		}
	}
}
