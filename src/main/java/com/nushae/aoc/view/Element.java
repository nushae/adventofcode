package com.nushae.aoc.view;

import java.awt.*;

public interface Element {
	public void display(Graphics2D g2);
}
