package com.nushae.aoc.view;

import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// A simple view for CharGrids that displays each symbol as a colored square
public class ColorGridPanel extends JPanel {

	private static final Color DEFAULT_GRID_BACKGROUND_COLOR = Color.white;
	private static final Color DEFAULT_GRID_FOREGROUND_COLOR = Color.black;
	private static final Color DEFAULT_HIGHLIGHT_COLOR = Color.red;

	private int gridWidth;
	private int gridHeight;
	private int cellSize;
	private int offsetX;
	private int offsetY;

	@Setter
	private Color backgroundColor;
	@Setter
	private Color foregroundColor;
	@Setter
	private Color highlightColor;
	@Setter
	Map<Character, Color> palette; // colors to use for each symbol
	CharGrid grid; // grid to display on this panel
	@Setter
	Set<Location> highlights;

	public ColorGridPanel(CharGrid initialGrid) {
		this(initialGrid, new HashMap<>());
	}

	public ColorGridPanel(CharGrid initialGrid, Map<Character, Color> palette) {
		grid = initialGrid;
		this.palette = new HashMap<>(palette);
		backgroundColor = DEFAULT_GRID_BACKGROUND_COLOR;
		foregroundColor = DEFAULT_GRID_FOREGROUND_COLOR;
		repaint();
	}

	public void setGrid(CharGrid newGrid) {
		grid = newGrid;
		repaint();
	}

	public void refresh() {
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		if (grid != null) {
			g.setColor(backgroundColor);
			g.fillRect(0, 0, getWidth(), getHeight());
			gridWidth = grid.getWidth();
			gridHeight = grid.getHeight();
			cellSize = Math.min(getWidth() / gridWidth, getHeight() / gridHeight);
			offsetX = (getWidth() - gridWidth * cellSize) / 2;
			offsetY = (getHeight() - gridHeight * cellSize) / 2;

			// content
			for (int i = 0; i < grid.getWidth(); i++) {
				for (int j = 0; j < grid.getHeight(); j++) {
					if (palette.containsKey(grid.get(i, j))) {
						g.setColor(palette.get(grid.get(i, j)));
					} else {
						g.setColor(foregroundColor);
					}
					g.fillRect(offsetX + i * cellSize, offsetY + j * cellSize, cellSize, cellSize);
					if (highlights.contains(new Location(i, j, grid))) {
						g.setColor(highlightColor);
						g.fillRect(offsetX + i * cellSize + cellSize / 3, offsetY + j * cellSize + cellSize / 3, cellSize / 3, cellSize / 3);
					}
				}
			}
		}
	}
}
