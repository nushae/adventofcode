package com.nushae.aoc.view;

import com.nushae.aoc.domain.CharGrid;
import com.nushae.aoc.domain.Location;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;

// A simple view for CharGrids that has the following features:
// * Automatically sized grid cells to available space
// * Supports up to 64 characters to display
// * Allows for certain cells to be highlighted
// * Allows for setting and unsetting cells with selected character(s)
public class SymbolGridPanel extends JPanel {

	private static final Font FONT = new Font("Monospaced", Font.PLAIN, 18);
	private static final Color DEFAULT_GRID_BACKGROUND_COLOR = Color.white;
	private static final Color DEFAULT_GRID_FOREGROUND_COLOR = Color.black;
	private static final Color DEFAULT_GRID_LINES_COLOR = new Color(176, 224, 230);
	private static final Color DEFAULT_GRID_HIGHLIGHT_COLOR = new Color(255, 125, 125);
	public static final Color DEFAULT_INPUT_FOREGROUND_COLOR = new Color(88, 112, 230);
	public static final Color DEFAULT_INPUT_LINES_COLOR = new Color(88, 112, 230);
	public static final Color DEFAULT_INPUT_HIGHLIGHT_COLOR = Color.yellow;

	private int gridWidth;
	private int gridHeight;
	private int cellWidth;
	private int cellHeight;
	@Setter
	private Color backgroundColor;
	@Setter
	private Color foregroundColor;
	@Setter
	private Color highlightColor;
	@Setter
	private Color linesColor;
	@Setter
	private Color inputForegroundColor;
	@Setter
	private Color inputHighlightColor;
	@Setter
	private Color inputLinesColor;

	Set<Location> highlights; // cells to highlight with highlight color background
	CharGrid grid; // grid to display on this panel
	char[] symbols; // symbols available to user to put in the grid

	@Getter
	@Setter
	char activeSymbol = 0;

	public SymbolGridPanel(CharGrid initialGrid) {
		this(initialGrid, "");
	}

	public SymbolGridPanel(CharGrid initialGrid, String alphabet) {
		highlights = new HashSet<>();
		grid = initialGrid;
		symbols = new char[Math.min(alphabet.length(), 64)];
		for (int i = 0; i < symbols.length; i++) {
			symbols[i] = alphabet.charAt(i);
		}
		backgroundColor = DEFAULT_GRID_BACKGROUND_COLOR;
		foregroundColor = DEFAULT_GRID_FOREGROUND_COLOR;
		highlightColor = DEFAULT_GRID_HIGHLIGHT_COLOR;
		linesColor = DEFAULT_GRID_LINES_COLOR;
		inputForegroundColor = DEFAULT_INPUT_FOREGROUND_COLOR;
		inputHighlightColor = DEFAULT_INPUT_HIGHLIGHT_COLOR;
		inputLinesColor = DEFAULT_INPUT_LINES_COLOR;
		repaint();
	}

	public void setGrid(CharGrid newGrid) {
		grid = newGrid;
		repaint();
	}

	public void setHighlights(Set<Location> newHighlights) {
		highlights = new HashSet<>(newHighlights);
		repaint();
	}

	public void addHighlight(int x, int y) {
		if (grid.inGrid(x, y)) {
			highlights.add(new Location(x, y, grid));
			repaint();
		}
	}

	public boolean removeHighlight(int x, int y) {
		boolean success = highlights.remove(new Location(x, y, grid));
		repaint();
		return success;
	}

	public boolean pixelInGrid(int x, int y) {
		return grid.inGrid(x / cellWidth, y / cellHeight);
	}

	public Location pixelToLocation(int x, int y) {
		if (pixelInGrid(x, y)) {
			return new Location(x / cellWidth, y / cellHeight, grid);
		}
		return null;
	}

	public char pixelToAlphabet(int x, int y) {
		int i = (x - (gridWidth + 2) * cellWidth) / cellWidth;
		int j = y / cellHeight;
		int idx = 8 * j + i;
		if (idx >= 0 && idx < symbols.length) {
			return symbols[idx];
		}
		return 0;
	}

	public void refresh() {
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		if (grid != null) {
			g.setColor(backgroundColor);
			g.fillRect(0, 0, getWidth(), getHeight());
			gridWidth = grid.getWidth();
			gridHeight = grid.getHeight();
			// symbol grid is always 8x8 gridcells; we keep a margin of 2 cells between it and the grid
			cellWidth = getWidth() / (10 + gridWidth);
			cellHeight = getHeight() / gridHeight;

			// Display: lines
			g.setColor(linesColor);
			for (int i = 0; i <= gridWidth; i++) {
				g.drawLine(i * cellWidth, 0, i * cellWidth, gridHeight * cellHeight);
			}
			for (int j = 0; j <= gridHeight; j++) {
				g.drawLine(0, j * cellHeight, gridWidth * cellWidth, j * cellHeight);
			}

			// Display: contents
			for (int i = 0; i < grid.getWidth(); i++) {
				for (int j = 0; j < grid.getHeight(); j++) {
					// highlight?
					if (highlights.contains(new Location(i, j, grid))) {
						g.setColor(highlightColor);
						g.fillRect(i * cellWidth + 1, j * cellHeight + 1, cellWidth - 2, cellHeight - 2);
					}
					g.setColor(foregroundColor);
					char c = grid.get(i, j);
					if (c != grid.getEmpty()) {
						drawCenteredString((Graphics2D) g, "" + c, new Rectangle(i * cellWidth, j * cellHeight, cellWidth, cellHeight), FONT);
					}
				}
			}

			// Inputs: lines
			g.setColor(inputLinesColor);
			for (int i = 0; i < 9; i++) {
				g.drawLine((gridWidth + 2) * cellWidth + i * cellWidth, 0, (gridWidth + 2) * cellWidth + i * cellWidth, 8 * cellHeight);
			}
			for (int j = 0; j < 9; j++) {
				g.drawLine((gridWidth + 2) * cellWidth, j * cellHeight, (gridWidth + 10) * cellWidth, j * cellHeight);
			}

			// Inputs: symbols
			for (int i = 0; i < symbols.length; i++) {
				int symX = i % 8;
				int symY = i / 8;
				char sym = symbols[i];
				if (sym != 0) {
					// highlight?
					if (sym == activeSymbol) {
						g.setColor(inputHighlightColor);
						g.fillRect((gridWidth + 2) * cellWidth + symX * cellWidth + 1, symY * cellHeight + 1, cellWidth - 2, cellHeight - 2);
					}
					g.setColor(inputForegroundColor);
					drawCenteredString((Graphics2D) g, "" + sym, new Rectangle((gridWidth + 2) * cellWidth + symX * cellWidth, symY * cellHeight, cellWidth, cellHeight), FONT);
				}
			}
		}
	}
}
