package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 7, title = "Camel Cards", keywords = {"poker", "game", "ranking"})
public class Exercise07 {

	// much more error proof!
	private static final Map<Character, Integer> rank = calculateRanks("23456789TJQKA");
	private static final Map<Character, Integer> jokerRank = calculateRanks("J23456789TQKA");

	private List<String> lines;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private static Map<Character, Integer> calculateRanks(String rankString) {
		Map<Character, Integer> result = new HashMap<>();
		for (int i = 0; i < rankString.length(); i++) {
			result.put(rankString.charAt(i), i + 1);
		}
		return Collections.unmodifiableMap(result);
	}

	private int pokerRanker(String left, String right) {
		return pokerCompare(left.split(" +")[0], right.split(" +")[0], false);
	}

	private int jokerRanker(String left, String right) {
		return pokerCompare(left.split(" +")[0], right.split(" +")[0], true);
	}

	int pokerCompare(String left, String right, boolean jokers) {
		int tiebreaker = 0;
		Map<Character, Integer> leftOccurrences = new HashMap<>();
		Map<Character, Integer> rightOccurrences = new HashMap<>();
		int leftMax = 0;
		int rightMax = 0;
		for (int i = 0; i < 5; i++) {
			char lChar = left.charAt(i);
			char rChar = right.charAt(i);
			leftOccurrences.put(lChar, leftOccurrences.getOrDefault(lChar, 0) + 1);
			if (!jokers || lChar != 'J') {
				leftMax = Math.max(leftMax, leftOccurrences.get(lChar));
			}
			rightOccurrences.put(rChar, rightOccurrences.getOrDefault(rChar, 0) + 1);
			if (!jokers || rChar != 'J') {
				rightMax = Math.max(rightMax, rightOccurrences.get(rChar));
			}
			if (tiebreaker == 0) {
				tiebreaker = jokers ? jokerRank.get(lChar) - jokerRank.get(rChar) : rank.get(lChar) - rank.get(rChar);
			}
		}

		if (jokers) {
			leftMax += leftOccurrences.getOrDefault('J', 0);
			if (leftOccurrences.size() > 1) {
				leftOccurrences.remove('J');
			}
			rightMax += rightOccurrences.getOrDefault('J', 0);
			if (rightOccurrences.size() > 1) {
				rightOccurrences.remove('J');
			}
		}
		int leftKeys = leftOccurrences.size();
		int rightKeys = rightOccurrences.size();

		if (leftMax != rightMax) {
			return (int) Math.signum(leftMax - rightMax);
		} else if (leftKeys != rightKeys) {
			return (int) Math.signum(rightKeys - leftKeys);
		}
		return (int) Math.signum(tiebreaker);
	}

	long calculateWinnings(boolean part1) {
		long accum = 0;
		lines.sort(part1 ? this::pokerRanker : this::jokerRanker);
		long rnk = 1;
		for (String line : lines) {
			accum += rnk * Long.parseLong(line.split(" +")[1]);
			rnk++;
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2023, 7));

		// no input processing, just sorting

		// part 1 - 250957639 (0.064s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.calculateWinnings(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 251515496 (0.025s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.calculateWinnings(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
