package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

// MERRY CHRISTMAS!!

@Log4j2
@Aoc(year = 2023, day = 25, title="Snowverload", keywords = {"graph", "minimum-edge-cut"})
public class Exercise25 extends JPanel {

	private static final boolean VISUAL = true;

	private record Edge(String left, String right) {

		public Edge(String left, String right) {
			if(left.compareTo(right) <= 0) {
				this.left = left;
				this.right = right;
			} else {
				this.left = right;
				this.right = left;
			}
		}

		@Override
		public String toString() {
			return left + '-' + right;
		}
	}

	Map<String, Point> visualNodes;
	Set<String> extent = new HashSet<>();
	Map<String, Set<String>> graph;
	List<String> lines;
	String startNode;
	boolean show;

	public Exercise25() {
		lines = new ArrayList<>();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise25(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		show = false;
		graph = new HashMap<>();
		long count = 0;
		for (String line : lines) {
			String[] parts = line.split(": *");
			String node = parts[0];
			if (graph.isEmpty()) {
				startNode = node;
			}
			for (String adjacent : parts[1].split(" +")) {
				count++;
				graph.computeIfAbsent(node, s -> new HashSet<>()).add(adjacent);
				graph.computeIfAbsent(adjacent, s -> new HashSet<>()).add(node);
			}
		}
		log.info("Input processed. Lines: " + lines.size()  + ", nodes: " + graph.size() + ", edges: " + count);
		show = true;
	}

	// Since we know the minimum cut set is size 3, we can skip the max-flow min-cut algorithm and go with a sort
	// of brute-force algorithm. The general idea:
	//
	// Provided we choose two nodes on different sides of the minimum cut set, any path between these two nodes
	// should contain (at least) one of the edges in the minimum cut set. For such a pair of nodes, breaking that
	// shortest path (by removing the edge that is in the minimum cut) and then finding another path should only
	// work 2 times; after breaking the third path the graph will be disconnected, yielding the answer.
	//
	// We brute-force in two ways:
	// 1) We 'break the path' by trying each edge along it in turn (a path is on average 10-15 edges).
	//    I think removing all edges in the path in one go MIGHT disconnect the graph in an unintended way and yield the
	//    wrong answer; we want to remove JUST the minimum cut. I do think we can assume there is only one set
	//    of three edges to find. But anyway, we remove them one by one just to be sure.
	// 2) We pick a starting node and iterate over the remaining nodes, and for each of them do the three path thing.
	//    I expect it takes only 3 or 4 tries since the graph will probably divide into roughly equal parts.
	public int doPart1() {
		for (String targetNode : graph.keySet()) {
			if(targetNode.equals(startNode)) {
				continue;
			}
			List<String> firstPath = bfsWithRemovedEdges(startNode, targetNode, Collections.emptyList());
			log.debug("First path has " + (firstPath.size()-1) + " edges in it.");
			for (int i = 0; i < firstPath.size() - 1; ++i) {
				Edge edgeInFirstPath = new Edge(firstPath.get(i), firstPath.get(i + 1));
				log.debug("Removing " + edgeInFirstPath + " from the first path from the graph...");
				List<String> secondPath = bfsWithRemovedEdges(startNode, targetNode, Collections.singletonList(edgeInFirstPath));
				log.debug("   Second path has " + (secondPath.size()-1) + " edges in it.");
				for (int j = 0; j < secondPath.size() - 1; ++j) {
					Edge edgeInSecondPath = new Edge(secondPath.get(j), secondPath.get(j + 1));
					log.debug("   Removing " + edgeInSecondPath + " from the second path from the graph...");
					List<String> thirdPath = bfsWithRemovedEdges(startNode, targetNode, Arrays.asList(edgeInFirstPath, edgeInSecondPath));
					log.debug("      Third path has " + (thirdPath.size()-1) + " edges in it.");
					for (int k = 0; k < thirdPath.size() - 1; ++k) {
						Edge edgeInThirdPath = new Edge(thirdPath.get(k), thirdPath.get(k + 1));
						log.debug("      Removing " + edgeInThirdPath + " from the third path from the graph...");
						List<Edge> removedEdgesFinal = Arrays.asList(edgeInFirstPath, edgeInSecondPath, edgeInThirdPath);
						List<String> fourthPath = bfsWithRemovedEdges(startNode, targetNode, removedEdgesFinal);
						log.debug("         Fourth path has " + (fourthPath.size()-1) + " edges in it.");
						if (fourthPath.isEmpty()) {
							log.debug("         So the graph is now disconnected and " + startNode + " and " + targetNode + " are now in different parts");
							log.debug("Minimum cut: " + removedEdgesFinal);
							extent = extentWithRemovedEdges(startNode, removedEdgesFinal);
							int firstSize = extent.size();
							log.debug("First part has " + firstSize + " nodes, second part has " + (graph.size() - firstSize) + " nodes.");
							return firstSize * (graph.size() - firstSize);
						}
					}
				}
			}
		}
		return -1;
	}

	private List<String> bfsWithRemovedEdges(String start, String destinationNode, List<Edge> removedEdges) {
		Map<String, List<String>> pathMap = new HashMap<>();
		pathMap.computeIfAbsent(start, s -> new ArrayList<>()).add(start);

		List<String> queue = new ArrayList<>();
		queue.add(start);
		while(!queue.isEmpty()) {
			String current = queue.remove(0);
			for (String neighbour : graph.get(current)) {
				if (!removedEdges.contains(new Edge(current, neighbour)) && !pathMap.containsKey(neighbour)) {
					List<String> path = new ArrayList<>(pathMap.get(current));
					path.add(neighbour);
					if (neighbour.equals(destinationNode)) {
						return path;
					}
					pathMap.put(neighbour, path);
					queue.add(neighbour);
				}
			}
		}
		return Collections.emptyList();
	}

	private Set<String> extentWithRemovedEdges(String node, List<Edge> removedEdges) {
		Set<String> extent = new HashSet<>();
		extent.add(node);

		List<String> queue = new ArrayList<>();
		queue.add(node);
		while (!queue.isEmpty()) {
			String current = queue.remove(0);
			for (String neighbour : graph.get(current)) {
				if (!removedEdges.contains(new Edge(current, neighbour)) && !extent.contains(neighbour)) {
					extent.add(neighbour);
					queue.add(neighbour);
				}
			}
		}
		return extent;
	}

	// Look it's hard to visualize graphs with 1500+ nodes...
	@Override
	public void paintComponent(Graphics g) {
		if (show) {
			if (visualNodes == null) {
				visualNodes = new HashMap<>();
				int countOne = 0;
				int countTwo = 1;
				int maxradius = (Math.min(getWidth(), getHeight()) - 20)/2;
				for (String nodeName : graph.keySet()) {
					double degrees = 2 * Math.PI / graph.size() * (extent.contains(nodeName) ? countOne++ : -countTwo++);
					visualNodes.put(nodeName, new Point((int) Math.round(maxradius + Math.cos(degrees) * maxradius), (int) Math.round(maxradius + Math.sin(degrees) * maxradius)));
				}
			}
			int circleDim = 4;
			for (String nodeName : visualNodes.keySet()) {
				Point p = visualNodes.get(nodeName);
				g.setColor(Color.black);
				g.fillOval(10 + p.x, 10 + p.y, circleDim, circleDim);
				for (String neighbour : graph.get(nodeName)) {
					Point q = visualNodes.get(neighbour);
					if (extent.contains(nodeName) != extent.contains(neighbour)) {
						g.setColor(Color.red);
					} else {
						if (extent.contains(nodeName)) {
							g.setColor(Color.lightGray);

						} else {
							g.setColor(Color.gray);
						}
					}
					g.drawLine(10 + p.x + circleDim/2, 10 + p.y + circleDim/2, 10 + q.x + circleDim/2, 10 + q.y + circleDim/2);
				}
			}
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1400, 1400));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2023, 25));

		if (VISUAL) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
		}

		// Process input (0.039s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 572000 (0.593s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - there IS no part 2 on day 25 :)
		// Merry Christmas :)
	}
}
