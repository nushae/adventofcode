package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.GraphicsUtil.DEFAULT_FONT;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.CHAR_TO_DIR;

@Log4j2
@Aoc(year = 2023, day = 23, title = "A Long Walk", keywords = {"graph", "subgraph", "pathing", "dfs"}) // see also 2019.18
public class Exercise23 extends JPanel {

	private static final boolean VISUAL = true;

	List<String> lines;
	Character[][] grid;
	Map<Point, Map<Point, Integer>> realEdges;
	int w, h;
	Set<Point> seen;
	Point startPoint;
	Point endPoint;
	boolean show;
	boolean showmaze;

	public Exercise23() {
		lines = new ArrayList<>();
	}

	public Exercise23(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise23(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput(boolean ignoreSlopes) {
		show = false;

		w = lines.get(0).length();
		h = lines.size();
		grid = new Character[w][h];
		for (int y = 0; y < h; y++) {
			String line = lines.get(y);
			for (int x = 0; x < w; x++) {
				grid[x][y] = line.charAt(x);
				if (y ==0 && grid[x][y] == '.') {
					startPoint = new Point(x, y);
				} else if (y == h-1 && grid[x][y] == '.') {
					endPoint = new Point(x, y);
				}
			}
		}

		// now calculate subgraph - vertices are grid locations with > 2 reachable neighbours; edges are between directly connected vertices only

		// build collection of vertices

		Set<Point> realVertices = new HashSet<>();
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				if (grid[x][y] != '#') {
					int neighbours = 0;
					for (Map.Entry<Character, Point> adjacent : CHAR_TO_DIR.entrySet()) {
						Point p = new Point(x + adjacent.getValue().x, y + adjacent.getValue().y);
						neighbours += (p.x >= 0 && p.x < w && p.y >= 0 && p.y < h && grid[p.x][p.y] != '#') ? 1 : 0;
					}
					if (neighbours > 2) { // crossing
						realVertices.add(new Point(x, y));
					}
				}
			}
		}
		realVertices.add(startPoint);
		realVertices.add(endPoint);

		// build collection of (directed) edges

		realEdges = new HashMap<>();
		for (Point src : realVertices) {
			List<Pair<Point, Integer>> queue = new ArrayList<>();
			queue.add(new Pair<>(src, 0));
			Set<Point> visited = new HashSet<>();
			while (!queue.isEmpty()) {
				Pair<Point, Integer> pair = queue.remove(0);
				Point p = pair.getLeft();
				int pathLen = pair.getRight();
				if (!visited.contains(p)) {
					visited.add(p);
					if (realVertices.contains(p) && !src.equals(p)) {
						realEdges.computeIfAbsent(src, x -> new HashMap<>()).put(p, pathLen);
					} else {
						for (Map.Entry<Character, Point> adjacent : CHAR_TO_DIR.entrySet()) {
							Point ap = new Point(p.x+adjacent.getValue().x, p.y + adjacent.getValue().y);
							if (ap.x >= 0 && ap.x < w && ap.y >= 0 && ap.y < h &&
								(ignoreSlopes && grid[ap.x][ap.y] != '#' || grid[ap.x][ap.y] == '.' || grid[ap.x][ap.y] == adjacent.getKey())) {
								queue.add(new Pair<>(ap, pathLen+1));
							}
						}
					}
				}
			}
		}
		show = true;
		repaint();
	}

	public int findAllPaths(int maxSoFar, Point vertex, int distance) {
		if (!seen.contains(vertex)) {
			if (vertex.equals(endPoint)) {
				return Math.max(maxSoFar, distance);
			}
			seen.add(vertex);
			for (Map.Entry<Point, Integer> adjacent : realEdges.get(vertex).entrySet()) {
				maxSoFar = Math.max(maxSoFar, findAllPaths(maxSoFar, adjacent.getKey().getLocation(), distance + adjacent.getValue()));
			}
			seen.remove(vertex);
		}
		return maxSoFar;
	}

	public long doPart1() {
		showmaze = true;
		processInput(false);
		seen = new HashSet<>();
		return findAllPaths(0, startPoint, 0);
	}

	public long doPart2() {
		showmaze = true;
		processInput(true);
		seen = new HashSet<>();
		return findAllPaths(0, startPoint, 0);
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1400, 1400));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g) {
		if (show) {
			final int SIZE = Math.min(getWidth() / w, getHeight() / h);
			final int xOff = (getWidth() - w * SIZE) / 2;
			final int yOff = (getHeight() - h * SIZE) / 2;
			g.setColor(Color.white);
			g.fillRect(0, 0, getWidth(), getHeight());
			if (showmaze) {
				for (int y = 0; y < h; y++) {
					for (int x = 0; x < w; x++) {
						switch (grid[x][y]) {
							case '.':
								g.setColor(Color.lightGray);
								g.fillRect(xOff + x * SIZE + 1, yOff + y * SIZE + 1, SIZE - 2, SIZE - 2);
								break;
							case '#':
								g.setColor(Color.black);
								g.fillRect(xOff + x * SIZE + 1, yOff + y * SIZE + 1, SIZE - 2, SIZE - 2);
								break;
							case '>':
								g.setColor(Color.lightGray);
								g.fillRect(xOff + x * SIZE + 1, yOff + y * SIZE + 1, SIZE - 2, SIZE - 2);
								g.setColor(Color.black);
								for (int i = 0; i < SIZE / 2; i += 3) {
									g.drawLine(xOff + i + x * SIZE + 1, yOff + y * SIZE + 1, xOff + i + x * SIZE + SIZE / 2, yOff + y * SIZE + SIZE / 2);
									g.drawLine(xOff + i + x * SIZE + 1, yOff + y * SIZE + SIZE - 2, xOff + i + x * SIZE + SIZE / 2, yOff + y * SIZE + SIZE / 2);
								}
								break;
							case '<':
								g.setColor(Color.lightGray);
								g.fillRect(xOff + x * SIZE + 1, yOff + y * SIZE + 1, SIZE - 2, SIZE - 2);
								g.setColor(Color.black);
								for (int i = 0; i < SIZE / 2; i += 3) {
									g.drawLine(xOff - i + x * SIZE + SIZE - 2, yOff + y * SIZE + 1, xOff - i + x * SIZE + SIZE / 2, yOff + y * SIZE + SIZE / 2);
									g.drawLine(xOff - i + x * SIZE + SIZE - 2, yOff + y * SIZE + SIZE - 2, xOff - i + x * SIZE + SIZE / 2, yOff + y * SIZE + SIZE / 2);
								}
								break;
							case 'v':
								g.setColor(Color.lightGray);
								g.fillRect(xOff + x * SIZE + 1, yOff + y * SIZE + 1, SIZE - 2, SIZE - 2);
								g.setColor(Color.black);
								for (int i = 0; i < SIZE / 2; i += 3) {
									g.drawLine(xOff + x * SIZE + 1, yOff + i + y * SIZE + 1, xOff + x * SIZE + SIZE / 2, yOff + i + y * SIZE + SIZE / 2);
									g.drawLine(xOff + x * SIZE + SIZE - 2, yOff + i + y * SIZE + 1, xOff + x * SIZE + SIZE / 2, yOff + i + y * SIZE + SIZE / 2);
								}
								break;
							case '^':
								g.setColor(Color.lightGray);
								g.fillRect(xOff + x * SIZE + 1, yOff + y * SIZE + 1, SIZE - 2, SIZE - 2);
								g.setColor(Color.black);
								for (int i = 0; i < SIZE / 2; i += 3) {
									g.drawLine(xOff + x * SIZE + 1, yOff - i + y * SIZE + SIZE - 2, xOff + x * SIZE + SIZE / 2, yOff - i + y * SIZE + SIZE / 2);
									g.drawLine(xOff + x * SIZE + SIZE - 2, yOff - i + y * SIZE + SIZE - 2, xOff + x * SIZE + SIZE / 2, yOff - i + y * SIZE + SIZE / 2);
								}
								break;
						}
					}
				}
			}
			for (Map.Entry<Point, Map<Point, Integer>> entrySRC : realEdges.entrySet()) {
				Point src = entrySRC.getKey();
				for (Map.Entry<Point, Integer> entryDST : entrySRC.getValue().entrySet()) {
					Point dst = entryDST.getKey().getLocation();
					g.setColor(Color.red);
					g.drawLine(xOff + src.x * SIZE + SIZE / 2, yOff + src.y * SIZE + SIZE / 2, xOff + dst.x * SIZE + SIZE / 2, yOff + dst.y * SIZE + SIZE / 2);
					g.fillOval(xOff + src.x * SIZE + SIZE / 4, yOff + src.y * SIZE + SIZE / 4, SIZE / 2, SIZE / 2);
					int circleX = (int) Math.round(xOff + SIZE * src.x + SIZE * (dst.x - src.x - 2.5) / 2.0);
					int circleY = (int) Math.round(yOff + SIZE * src.y + SIZE * (dst.y - src.y - 2.5) / 2.0);
					g.setColor(new Color(255, 255, 255)); // adding alpha looks bad to me
					g.fillOval(circleX, circleY, 3 * SIZE, 3 * SIZE);
					g.setColor(Color.red);
					g.drawOval(circleX, circleY, 3 * SIZE, 3 * SIZE);
					g.setColor(Color.black);
					drawCenteredString((Graphics2D) g, "" + entryDST.getValue(), new Rectangle(xOff + src.x * SIZE + SIZE / 2, yOff + src.y * SIZE + SIZE / 2, (dst.x - src.x) * SIZE, (dst.y - src.y) * SIZE), DEFAULT_FONT);
				}
			}
		}
	}

	public static void main(String[] args) {
		// Exercise23 ex = new Exercise23(aocPath(2023, 23, "smallmaze.txt"));
		Exercise23 ex = new Exercise23(aocPath(2023, 23));

		if (VISUAL) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex));
		}

		// part 1 - 2030 (0.701s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 6390 (3.253s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
