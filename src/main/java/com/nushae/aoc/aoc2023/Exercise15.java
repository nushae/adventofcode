package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import com.nushae.aoc.domain.Triplet;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 15, title="Lens Library", keywords = {"hashing", "map-reduce"})
public class Exercise15 {

	List<String> lines;
	List<String> instr;
	Map<Integer, List<Pair<String, Long>>> boxes;

	public Exercise15() {
		lines = new ArrayList<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		instr = new ArrayList<>();
		long accum = 0;
		for (String line : lines) {
			for (String seq : line.split(",")) {
				accum += calcHash(seq);
				instr.add(seq);
			}
		}
		return accum;
	}

	public long doPart2() {
		// DEPENDS ON PART 1 BEING RUN BEFOREHAND!
		boxes = new HashMap<>();
		for (String instruction : instr) {
			Triplet<String, Character, Long> values = parse(instruction);
			int boxNum = calcHash(values.getLeft());
			if (values.getMid() == '-') {
				removeLens(boxNum, values.getLeft());
			} else {
				putLens(boxNum, values.getLeft(), values.getRight());
			}
		}
		return calcFocus();
	}

	Triplet<String, Character, Long> parse(String instruction) {
		String label;
		char symbol;
		long num = -1;
		if (instruction.endsWith("-")) {
			label = instruction.substring(0, instruction.length()-1);
			symbol = '-';
		} else {
			String[] parts = instruction.split("=");
			label = parts[0];
			symbol = '=';
			num = Long.parseLong(parts[1]);
		}
		return new Triplet<>(label, symbol, num);
	}

	void removeLens(int boxNum, String label) {
		List<Pair<String, Long>> box = boxes.getOrDefault(boxNum, new ArrayList<>());
		String oldBox = box.toString();
		Iterator<Pair<String, Long>> it = box.iterator();
		while (it.hasNext()) {
			Pair<String, Long> p = it.next();
			if (p.getLeft().equals(label)) {
				it.remove();
				break;
			}
		}
		log.debug(String.format("%03d = %s >REM> %s", boxNum, oldBox, box));
	}

	void putLens(int boxNum, String label, long num) {
		List<Pair<String, Long>> box = boxes.getOrDefault(boxNum, new ArrayList<>());
		List<Pair<String, Long>> newBox = new ArrayList<>();
		boolean found = false;
		for (Pair<String, Long> p : box) {
			if (p.getLeft().equals(label)) {
				found = true;
				newBox.add(new Pair<>(label, num));
			} else {
				newBox.add(p);
			}
		}
		if (!found) {
			newBox.add(new Pair<>(label, num));
		}
		log.debug(String.format("%03d = %s >ADD> %s", boxNum, box, newBox));
		boxes.put(boxNum, newBox);
	}

	public static int calcHash(String input) {
		int result = 0;
		for (int i = 0; i < input.length(); i++) {
			result = 17 * (result + (byte) input.charAt(i)) % 256;
		}
		return result;
	}

	public long calcFocus() {
		long accum = 0;
		for (Map.Entry<Integer, List<Pair<String, Long>>> box : boxes.entrySet()) {
			if (box.getValue().size() > 0) {
				log.debug(String.format("%03d -> %s", box.getKey(), box.getValue()));
				long power = box.getKey()+1;
				long slotNum = 1;
				for (Pair<String, Long> lens : box.getValue()) {
					log.debug(String.format("    %s: %d * %d * %d", lens.getLeft(), power, slotNum, lens.getRight()));
					accum += power * slotNum * lens.getRight();
					slotNum++;
				}
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2023, 15));

		// part 1 = input processing - 513214 (0.002s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 258826 (0.056s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
