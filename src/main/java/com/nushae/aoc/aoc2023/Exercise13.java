package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.transpose;

@Log4j2
@Aoc(year = 2023, day = 13, title = "Point of Incidence", keywords = {"grid", "symmetry", "optimization"})
public class Exercise13 {

	List<String> lines;
	List<String[]> blocks;

	public Exercise13() {
		lines = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		blocks = new ArrayList<>();
		List<String> blockLines = new ArrayList<>();
		for (String line : lines) {
			if ("".equals(line)) { // new block
				String[] block = new String[blockLines.size()];
				for (int i = 0; i < block.length; i++) {
					block[i] = blockLines.get(i);
				}
				blocks.add(block);
				blockLines = new ArrayList<>();
			} else {
				blockLines.add(line);
			}
		}
		if (blockLines.size() > 0) {
			String[] block = new String[blockLines.size()];
			for (int i = 0; i < block.length; i++) {
				block[i] = blockLines.get(i);
			}
			blocks.add(block);
		}
		log.info("Input processed: " + blocks.size() + " blocks");
	}

	public long doPart1() {
		return calc(0);
	}

	public long doPart2() {
		return calc(1);
	}

	long calc(int diffsNeeded) {
		long hor = 0;
		long ver = 0;
		for (String[] block : blocks) {
			hor += findLine(block, diffsNeeded);
			ver += findLine(transpose(block), diffsNeeded);
		}
		return 100*hor + ver;
	}

	long findLine(String[] block, int diffsNeeded) {
		for (int row = 1; row < block.length; row++) {
			if (findDiffsAfterReflection(block, row) == diffsNeeded) {
				return row;
			}
		}
		return 0;
	}

	int findDiffsAfterReflection(String[] input, int rowBelowLine) {
		int diffs = 0;
		for (int i = 0; i < Math.min(input.length - rowBelowLine, rowBelowLine); i++) {
			diffs += countDiffs(input[rowBelowLine-i-1], input[rowBelowLine+i]);
		}
		return diffs;
	}

	int countDiffs(String left, String right) {
		int count = 0;
		for (int i = 0; i < left.length(); i++) {
			count += left.charAt(i) != right.charAt(i) ? 1 : 0;
		}
		return count;
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2023, 13));

		// Process input (0.002s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 35521 (0.003s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 34795 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
