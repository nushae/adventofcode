package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2023, day = 18, title="Lavaduct Lagoon", keywords = {"grid", "graph", "pathing", "area"})
public class Exercise18 {

	private static final Pattern CURVE_INSTRUCTIONS = Pattern.compile("([RLUD]) ([0-9]+) (.*)");

	List<String> lines;
	List<Pair<Point, Integer>> instr1;
	List<Pair<Point, Integer>> instr2;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		instr1 = new ArrayList<>();
		instr2 = new ArrayList<>();
		for (String line : lines) {
			Matcher m = CURVE_INSTRUCTIONS.matcher(line);
			if (m.find()) {
				instr1.add(makeInstr(m.group(1), m.group(2)));
				instr2.add(makeInstr(m.group(3)));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		log.info("Input processed (with pattern(s)).");
	}

	public Pair<Point, Integer> makeInstr(String d, String num) {
		Point dir;
		switch (d) {
			case "U" : dir = NORTH; break;
			case "D" : dir = SOUTH; break;
			case "L" : dir = EAST; break;
			case "R":
			default:
				dir = WEST;
		}
		return new Pair<> (dir, Integer.parseInt(num));
	}

	public Pair<Point, Integer> makeInstr(String input) {
		System.out.println(input + " " + input.substring(2,7) + " " + input.charAt(6));
		Point dir;
		switch (input.charAt(7)) {
			case '0':
				dir = EAST;
				break;
			case '1':
				dir = SOUTH;
				break;
			case '2':
				dir = WEST;
				break;
			case '3':
			default:
				dir = NORTH;
		}
		return new Pair<>(dir, Integer.parseInt(input.substring(2,7), 16));
	}

	public long doPart1() {
		return calcArea(instr1);
	}

	public long doPart2() {
		return calcArea(instr2);
	}

	// for a closed curve, we have that there are always 4 more outer corners than inner corners. In essence, any such
	// curve is a rectangle with a number of left/right pairs inserted. Straight squares contribute half, inner corners
	// a quarter, and outer corners three quarters to the area of the 'outer half' of the curve, which is the part not
	// counted by the shoelace formula. Every inner corner is balanced by an outer corner, and both together contribute
	// 1/2 each, so what we are left with is the 4 extra outer corners. Each of those contributes 1/4 more than the half
	// every other square contributes, so we end up with 'half the squares in the curve plus one' as the outer area.
	public long calcArea(List<Pair<Point, Integer>> instructions) {
		List<Point> vertices = new ArrayList<>();
		Point p0 = new Point(0,0);
		vertices.add(p0);
		long curveArea = 0;
		for (Pair<Point, Integer> next : instructions) {
			Point p1 = linear(p0, next.getLeft(), next.getRight());
			vertices.add(p1);
			curveArea += next.getRight();
			p0 = p1;
		}
		return shoelaceArea(vertices) + curveArea / 2 + 1;
	}

	private static long shoelaceArea(List<Point> vertices) {
		int n = vertices.size();
		long area = 0;
		for (int i = 0; i < n - 1; i++) {
			area += ((long) vertices.get(i).x)  * ((long) vertices.get(i + 1).y) - ((long) vertices.get(i + 1).x) * ((long) vertices.get(i).y);
		}
		return Math.abs(area + ((long) vertices.get(n - 1).x) * ((long) vertices.get(0).y) - ((long) vertices.get(0).x * vertices.get(n - 1).y)) / 2;
	}

	public static void main(String[] args) {
		// Exercise18 ex = new Exercise18("");
		Exercise18 ex = new Exercise18(aocPath(2023, 18));

		// Process input (0.351s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 49061 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 92556825427032 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
