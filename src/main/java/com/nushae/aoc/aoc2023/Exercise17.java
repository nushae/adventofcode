package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.SearchNode;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;
import static com.nushae.aoc.util.SearchUtil.dsp;

@Log4j2
@Aoc(year = 2023, day = 17, title="Clumsy Crucible", keywords = {"grid", "shortest-path"})
public class Exercise17 {

	private class Crucible implements SearchNode {

		private SearchNode parent;
		public int heat;
		private final Point lastDir;
		private final Point loc;
		private final int times;

		public Crucible(Point location, int heat, Point lastDir, int times) {
			this.loc = location;
			this.heat = heat;
			this.lastDir = lastDir;
			this.times = times;
		}

		@Override
		public Map<SearchNode, Long> getAdjacentNodes() {
			Map<SearchNode, Long> result = new HashMap<>();
			for (Point dir : ORTHO_DIRS) {
				if (rot90L(2, dir).equals(lastDir)) {
					continue;
				}
				Point p = sum(loc, dir);
				if (p.x >= 0 && p.x < w && p.y >=0 && p.y < h) {
					if (dir.equals(lastDir)) {
						if (times < 3) {
							result.put(new Crucible(p, grid[p.x][p.y], dir, times + 1), (long) grid[p.x][p.y]);
						}
					} else {
						result.put(new Crucible(p, grid[p.x][p.y], dir, 1), (long) grid[p.x][p.y]);
					}
				}
			}
			return result;
		}

		@Override
		public String getState() {
			return loc.toString()+"@"+lastDir.toString()+"@"+times;
		}

		@Override
		public void setParent(SearchNode parent) {
			this.parent = parent;
		}

		@Override
		public SearchNode getParent() {
			return parent;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			Crucible crucible = (Crucible) o;
			return times == crucible.times && lastDir.equals(crucible.lastDir) && loc.equals(crucible.loc);
		}

		@Override
		public int hashCode() {
			return Objects.hash(lastDir, loc, times);
		}

		@Override
		public String toString() {
			return "HeatNode{" +
					"heat=" + heat +
					", lastDir=" + lastDir +
					", loc=" + loc +
					", times=" + times +
					"}";
		}
	}

	private class UltraCrucible implements SearchNode {

		private SearchNode parent;
		public int heat;
		private final Point lastDir;
		private final Point loc;
		private final int times, lower, upper;

		public UltraCrucible(Point location, int heat, Point lastDir, int times, int lower, int upper) {
			this.loc = location;
			this.heat = heat;
			this.lastDir = lastDir;
			this.times = times;
			this.lower = lower;
			this.upper = upper;
		}

		@Override
		public Map<SearchNode, Long> getAdjacentNodes() {
			Map<SearchNode, Long> result = new HashMap<>();
			if (times > 0 && times < lower) { // have to keep going in same dir, we already tested if there was room
				Point p = sum(loc, lastDir);
				result.put(new UltraCrucible(p, grid[p.x][p.y], lastDir, times + 1, lower, upper), (long) grid[p.x][p.y]);
			} else {
				for (Point dir : ORTHO_DIRS) {
					if (rot90L(2, dir).equals(lastDir)) {
						continue;
					}
					Point p = sum(loc, dir);
					if (p.x >= 0 && p.x < w && p.y >= 0 && p.y < h) {
						if (dir.equals(lastDir)) {
							if (times < upper) {
								result.put(new UltraCrucible(p, grid[p.x][p.y], dir, times + 1, lower, upper), (long) grid[p.x][p.y]);
							}
						} else { // only turn in this dir if there is room for lower steps
							Point q = linear(loc, dir, lower);
							if (q.x >= 0 && q.x < w && q.y >= 0 && q.y < h) {
								result.put(new UltraCrucible(p, grid[p.x][p.y], dir, 1, lower, upper), (long) grid[p.x][p.y]);
							}
						}
					}
				}
			}
			return result;
		}

		@Override
		public String getState() {
			return loc.toString()+"@"+lastDir.toString()+"@"+times;
		}

		@Override
		public void setParent(SearchNode parent) {
			this.parent = parent;
		}

		@Override
		public SearchNode getParent() {
			return parent;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			UltraCrucible ultra = (UltraCrucible) o;
			return times == ultra.times && lastDir.equals(ultra.lastDir) && loc.equals(ultra.loc);
		}

		@Override
		public int hashCode() {
			return Objects.hash(lastDir, loc, times);
		}

		@Override
		public String toString() {
			return "HeatNode{" +
					"heat=" + heat +
					", lastDir=" + lastDir +
					", loc=" + loc +
					", times=" + times +
					"}";
		}
	}

	List<String> lines;
	List<Long> numbers;
	int w, h;
	Integer[][] grid;
	Crucible start;
	UltraCrucible start2;

	public Exercise17() {
		lines = new ArrayList<>();
	}

	public Exercise17(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise17(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		numbers = new ArrayList<>();
		w = lines.get(0).length();
		h = lines.size();
		grid = new Integer[w][h];
		for (int j = 0; j < h; j++) {
			for (int i=0; i < w; i++) {
				grid[i][j] = lines.get(j).charAt(i) - '0';
			}
		}
		start = new Crucible(new Point(0, 0), grid[0][0], SOUTH, 0);
		start2 = new UltraCrucible(new Point(0,0), grid[0][0], SOUTH, 0, 4, 10);
	}

	public long doPart1() {
		long accum = 0;
		processInput();
		SearchNode result = dsp(start, s -> s.getState().startsWith("java.awt.Point[x="+(w-1)+",y=" + (h-1)+"]@"));
		StringBuilder route = new StringBuilder();
		while (result != null) {
			Crucible cr = (Crucible) result;
			if (cr.getParent() != null) accum += cr.heat;
			route.insert(0, String.format("[%d, %d]", cr.loc.x, cr.loc.y));
			result = result.getParent();
		}
		log.debug("Route: " + route);
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		processInput();
		SearchNode result = dsp(start2, s -> s.getState().startsWith("java.awt.Point[x="+(w-1)+",y=" + (h-1)+"]@"));
		StringBuilder route = new StringBuilder();
		while (result != null) {
			UltraCrucible uc = (UltraCrucible) result;
			if (uc.getParent() != null) accum += uc.heat;
			route.insert(0, String.format("[%d, %d]", uc.loc.x, uc.loc.y));
			result = result.getParent();
		}
		log.debug("Route: " + route);
		return accum;
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17(aocPath(2023, 17));

		// Process input (0.292s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 694 (41.685s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 829 (6m19.41s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
