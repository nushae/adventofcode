package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.lcm;

@Log4j2
@Aoc(year = 2023, day = 20, title="Pulse Propagation", keywords = {"pathing", "logic", "lcm"})
public class Exercise20 {

	private static class FlipFlopModule {
		boolean state = false;
		public List<String> outputs;
		public final String name;

		public FlipFlopModule(String name, List<String> outputs) {
			this.name = name;
			this.outputs = new ArrayList<>(outputs);
		}

		public Pair<List<String>, Boolean> applyPulse(boolean pulse) {
			List<String> result = new ArrayList<>();
			if (!pulse) {
				state = !state;
				result.addAll(outputs);
			}
			return new Pair<>(result, state);
		}
	}

	private static class ConjunctionModule {
		private final Map<String, Boolean> lastInputs;
		public List<String> outputs;
		public final String name;

		public ConjunctionModule(String name, List<String> inputs, List<String> outputs) {
			this.name = name;
			lastInputs = new HashMap<>();
			for (String s : inputs) {
				lastInputs.put(s, false);
			}
			this.outputs = new ArrayList<>(outputs);
		}

		public ConjunctionModule(String name, List<String> outputs) {
			this(name, new ArrayList<>(), outputs);
		}

		public Pair<List<String>, Boolean> applyPulse(String source, boolean pulse) {
			lastInputs.put(source, pulse);
			boolean outpulse = true;
			for (boolean b : lastInputs.values()) {
				outpulse &= b;
			}
			return new Pair<>(new ArrayList<>(outputs), !outpulse);
		}

		public void addInput(String name) {
			lastInputs.put(name, false);
		}
	}

	Map<String, ConjunctionModule> conModules;
	Map<String, FlipFlopModule> ffModules;
	Map<String, Set<String>> inputNames;
	List<String> watchfor;
	Map<String, Long> cycles;
	List<String> lines;
	List<String> initialSend;
	boolean pulseReceived;

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		conModules = new HashMap<>();
		ffModules = new HashMap<>();
		inputNames = new HashMap<>();
		initialSend = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(" -> ");
			List<String> nextModules = new ArrayList<>(Arrays.asList(parts[1].split(", ")));
			String name;
			if ("broadcaster".equals(parts[0])) {
				name = "broadcaster";
				initialSend = new ArrayList<>(nextModules);
			} else {
				char type = line.charAt(0);
				name = parts[0].substring(1);
				if (type == '%') {
					FlipFlopModule mod = new FlipFlopModule(name, nextModules);
					ffModules.put(name, mod);
				} else {
					ConjunctionModule mod = new ConjunctionModule(name, nextModules);
					conModules.put(name, mod);
				}
			}
			for (String s : nextModules) {
				Set<String> inputs = inputNames.getOrDefault(s, new HashSet<>());
				inputs.add(name);
				inputNames.put(s, inputs);
			}
		}
		for (Map.Entry<String, ConjunctionModule> am : conModules.entrySet()) {
			for (String input : inputNames.getOrDefault(am.getKey(), new HashSet<>())) {
				am.getValue().addInput(input);
			}
		}
		log.debug("FlipFlops: " + ffModules + "\nCOnjunctions: " + conModules);
	}

	public long doPart1() {
		processInput();
		long lowPulsesSent = 0;
		long highPulsesSent = 0;
		watchfor = new ArrayList<>();
		cycles = new HashMap<>();
		for (int i = 0; i < 1000; i++) {
			Pair<Long, Long> result = oneButtonPress(i);
			lowPulsesSent += result.getLeft();
			highPulsesSent += result.getRight();
		}
		return lowPulsesSent * highPulsesSent;
	}

	public long doPart2() {
		processInput();
		// all inputs of the (yes we assume this, but holds for my input) conjuction module that sends to rx must be high -> cycle counting
		watchfor = new ArrayList<>();
		cycles = new HashMap<>();
		for (ConjunctionModule am : conModules.values()) {
			if (am.outputs.contains("rx")) {
				watchfor = new ArrayList<>(inputNames.get(am.name));
				break;
			}
		}
		long buttonPresses = 0;
		pulseReceived = false;
		while (watchfor.size() != cycles.size()) {
			buttonPresses++;
			oneButtonPress(buttonPresses);
		}
		long result = 1;
		for (Long val : cycles.values()) {
			result = lcm(result, val);
		}
		return result;
	}

	public Pair<Long, Long> oneButtonPress(long buttonPresses) {
		long lowPulsesSent = 0;
		long highPulsesSent = 0;
		List<Pair<Pair<String, String>, Boolean>> queue = new ArrayList<>();
		queue.add(new Pair<>(new Pair<>("button", "broadcaster"), false));
		while (!queue.isEmpty()) {
			Pair<Pair<String, String>, Boolean> pulse = queue.remove(0);
			String source = pulse.getLeft().getLeft();
			String dest = pulse.getLeft().getRight();
			boolean signal = pulse.getRight();
			log.debug(String.format("Sending signal '%s' from %s to %s", signal, source, dest));
			if (signal) {
				highPulsesSent++;
			} else {
				if (watchfor.contains(dest)) {
					if (!cycles.containsKey(dest)) {
						cycles.put(dest, buttonPresses);
					}
				}
				lowPulsesSent++;
			}
			Pair<List<String>, Boolean> result;
			if (conModules.containsKey(dest)) {
				result = conModules.get(dest).applyPulse(source, signal);
			} else if (ffModules.containsKey(dest)) {
				result = ffModules.get(dest).applyPulse(signal);
			} else if ("broadcaster".equals(dest)) { // broadcaster
				for (String s : initialSend) {
					queue.add(new Pair<>(new Pair<>("broadcaster", s), false));
				}
				continue;
			} else { // unconnected
				continue;
			}
			for (String res : result.getLeft()) {
				queue.add(new Pair<>(new Pair<>(dest, res), result.getRight()));
			}
		}
		return new Pair<>(lowPulsesSent, highPulsesSent);
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2023, 20));

		// part 1 - 929810733 (0.142s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 231657829136023 (0.671s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
