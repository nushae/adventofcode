package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.WORD_TO_DIGIT;

@Log4j2
@Aoc(year = 2023, day = 1, title = "Trebuchet?!")
public class Exercise01 {
	private List<String> lines;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	long doPart(boolean part2) {
		long accum = 0;
		for (String line : lines) {
			StringBuilder intermediate = new StringBuilder();
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) >= '0' && line.charAt(i) <= '9') {
					intermediate.append(line.charAt(i));
				} else if (part2) {
					for (Map.Entry<String, String> digit : WORD_TO_DIGIT.entrySet()) {
						if (line.startsWith(digit.getKey(), i)) {
							intermediate.append(digit.getValue());
						}
					}
				}
			}
			long num = (intermediate.charAt(0) - '0') * 10 + intermediate.charAt(intermediate.length()-1) - '0';
			accum += num;
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2023, 1));

		// part 1 - 56049 (0.002s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 54530 (0.247s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
