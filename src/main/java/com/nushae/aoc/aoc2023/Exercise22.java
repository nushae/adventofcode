package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import com.nushae.aoc.domain.Triplet;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 22, title="Sand Slabs", keywords = {"tetris"})
public class Exercise22 {

	public static class Brick {
		public Triplet<Long, Long, Long> coord1XYZ;
		public Triplet<Long, Long, Long> coord2XYZ;

		public Brick(Triplet<Long, Long, Long> coord1XYZ, Triplet<Long, Long, Long> coord2XYZ) {
			// make sure coords are sorted by Z, then X, then Y!
			if (coord1XYZ.getRight() < coord2XYZ.getRight()) {
				this.coord1XYZ = coord1XYZ;
				this.coord2XYZ = coord2XYZ;
			} else if (coord2XYZ.getRight() < coord1XYZ.getRight()) {
				this.coord2XYZ = coord1XYZ;
				this.coord1XYZ = coord2XYZ;
			} else if (coord1XYZ.getLeft() < coord2XYZ.getLeft()) {
				this.coord1XYZ = coord1XYZ;
				this.coord2XYZ = coord2XYZ;
			} else if (coord2XYZ.getLeft() < coord1XYZ.getLeft()) {
				this.coord2XYZ = coord1XYZ;
				this.coord1XYZ = coord2XYZ;
			} else if (coord1XYZ.getMid() <= coord2XYZ.getMid()) { // when equal: 1x1 cube
				this.coord1XYZ = coord1XYZ;
				this.coord2XYZ = coord2XYZ;
			} else { // so coord2XYZ.getMid() < coord1XYZ.getMid()
				this.coord2XYZ = coord1XYZ;
				this.coord1XYZ = coord2XYZ;
			}
		}

		public void move(Triplet<Long, Long, Long> deltaXYZ) {
			coord1XYZ = new Triplet<>(coord1XYZ.getLeft() + deltaXYZ.getLeft(), coord1XYZ.getMid() + deltaXYZ.getMid(), coord1XYZ.getRight() + deltaXYZ.getRight());
			coord2XYZ = new Triplet<>(coord2XYZ.getLeft() + deltaXYZ.getLeft(), coord2XYZ.getMid() + deltaXYZ.getMid(), coord2XYZ.getRight() + deltaXYZ.getRight());
		}

		public String toString() {
			return String.format("[%d, %d, %d]-[%d, %d, %d]", coord1XYZ.getLeft(), coord1XYZ.getMid(), coord1XYZ.getRight(), coord2XYZ.getLeft(), coord2XYZ.getMid(), coord2XYZ.getRight());
		}
	}

	List<String> lines;
	List<Brick> bricks;

	public Exercise22() {
		lines = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		bricks = new ArrayList<>();
		for (String line : lines) {
			String[] coords = line.split("~");
			Triplet<Long, Long, Long> coord1 = getCoordinate(coords[0]);
			Triplet<Long, Long, Long> coord2 = getCoordinate(coords[1]);
			bricks.add(new Brick(coord1, coord2));
		}
		log.info("Input processed. - " + lines.size() + " / " + bricks.size());
	}

	public Triplet<Long, Long, Long> getCoordinate(String def) {
		String[] parts = def.split(",");
		return new Triplet<>(Long.parseLong(parts[0]), Long.parseLong(parts[1]), Long.parseLong(parts[2]));
	}

	public Pair<Long, Long> doBothParts() {
		long safe = 0;
		long totalMoved = 0;
		bricks.sort(Comparator.comparingLong((Brick brick) -> brick.coord1XYZ.getRight())
						.thenComparingLong((Brick brick) -> brick.coord1XYZ.getLeft())
						.thenComparingLong((Brick brick) -> brick.coord1XYZ.getMid()));
		bricks = settleBricks(bricks).getRight();
		for (int i=0; i < bricks.size(); i++) {
			log.debug("Removing " + bricks.get(i));
			List<Brick> removeOne = new ArrayList<>(bricks);
			removeOne.remove(i);
			long result = settleBricks(removeOne).getLeft();
			log.debug(result + " brick(s) moved.");
			totalMoved += result;
			if (result == 0) {
				safe++;
			}
		}
		return new Pair<>(safe, totalMoved);
	}

	public long doPart1() {
		return doBothParts().getLeft();
	}

	public long doPart2() {
		return doBothParts().getRight();
	}

	public Pair<Long, List<Brick>> settleBricks(List<Brick> toSettle) {
		List<Brick> result = new ArrayList<>();
		for (Brick old : toSettle) {
			result.add(new Brick(old.coord1XYZ, old.coord2XYZ));
		}
		long moved = 0L;
		Map<Pair<Long, Long>, Long> settledCubes = new HashMap<>();
		log.debug("Starting loop");
		for (Brick b : result) {
			boolean settled = false;
			long dist = Long.MAX_VALUE;
			log.debug("Settling " + b);
			for (long x = Math.min(b.coord1XYZ.getLeft(), b.coord2XYZ.getLeft()); !settled && x <= Math.max(b.coord1XYZ.getLeft(), b.coord2XYZ.getLeft()); x++) {
				for (long y = Math.min(b.coord1XYZ.getMid(), b.coord2XYZ.getMid()); !settled && y <= Math.max(b.coord1XYZ.getMid(), b.coord2XYZ.getMid()); y++) {
					long heightHere = settledCubes.getOrDefault(new Pair<>(x, y), 0L);
					if (b.coord1XYZ.getRight() == heightHere + 1) { // already settled
						dist = 0;
						settled = true;
					} else {
						dist = Math.min(dist, b.coord1XYZ.getRight() - 1 - heightHere);
					}
				}
			}
			log.debug("Dist = " + dist);
			if (dist < 0) {
				log.error(settledCubes + "\n" + b);
				System.exit(0);
			}
			if (dist > 0) {
				moved++;
				b.move(new Triplet<>(0L, 0L, -dist));
			}
			log.debug("Update heights.");
			for (long x = Math.min(b.coord1XYZ.getLeft(), b.coord2XYZ.getLeft()); x <= Math.max(b.coord1XYZ.getLeft(), b.coord2XYZ.getLeft()); x++) {
				for (long y = Math.min(b.coord1XYZ.getMid(), b.coord2XYZ.getMid()); y <= Math.max(b.coord1XYZ.getMid(), b.coord2XYZ.getMid()); y++) {
					settledCubes.put(new Pair<>(x, y), b.coord2XYZ.getRight());
				}
			}
			log.debug("Updated.");
		}
		log.debug("Ending loop");
		return new Pair<>(moved, result);
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(aocPath(2023, 22));

		// Process input
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// both parts - 495 resp 76158 (1.791s)
		start = System.currentTimeMillis();
		Pair<Long, Long> result = ex.doBothParts();
		log.info("Part 1:");
		log.info(result.getLeft());
		log.info("Part 2:");
		log.info(result.getRight());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
