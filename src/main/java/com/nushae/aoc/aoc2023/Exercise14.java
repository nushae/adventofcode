package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2023, day = 14, title = "Parabolic Reflector Dish", keywords = {"grid", "simulation", "cycle-detect"})
public class Exercise14 {

	List<String> lines;
	List<Point> rocks;
	List<Point> cubes;
	int w, h;
	Map<String, Long> cache;

	public Exercise14() {
		lines = new ArrayList<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		rocks = new ArrayList<>();
		cubes = new ArrayList<>();
		w = lines.get(0).length();
		h = lines.size();
		for (int y=0; y < h; y++) {
			String line = lines.get(y);
			for (int x = 0; x < w; x++) {
				char c = line.charAt(x);
				Point p = new Point(x,y);
				if (c == 'O') rocks.add(p);
				if (c == '#') cubes.add(p);
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		processInput();
		List<Point> newPositions = new ArrayList<>(rocks);
		newPositions = tilt(newPositions, NORTH);
		return countLoad(newPositions);
	}

	public long doPart2() {
		processInput();
		cache = new HashMap<>();
		String tmp = null;
		List<Point> newPositions = new ArrayList<>(rocks);
		for (long i = 0; i < 1000000000; i++) {
			for (Point dir : new Point[] {NORTH, WEST, SOUTH, EAST}) {
				newPositions = tilt(newPositions, dir);
			}
			String key = makeKey(newPositions);
			if (cache.containsKey(key)) {
				if (tmp == null) {
					tmp = key;
				} else {
					if (tmp.equals(key)) {
						long first = cache.get(tmp);
						log.debug("seen already @ " + first + ", now @ i =" + i + " cycle = " + (i - first));
						long cycle = (i - first);
						long remainder = (1000000000 - first - 1) % cycle;
						log.debug("remaining at end: " + remainder);
						i = 1000000000 - 1 - remainder;
						log.debug("Advancing i to: " + i);
					}
				}
			}
			cache.put(key, i);
		}
		return countLoad(newPositions);
	}

	public long countLoad(List<Point> pos) {
		long result = 0;
		for (Point p : pos) {
			result += h-p.y;
		}
		return result;
	}

	List<Point> tilt(List<Point> input, Point dir) {
		List<Point> result = new ArrayList<>();
		for (Point p : input) {
			result.add(linear(p, dir, findMaxMovement(input, p, dir)));
		}
		return result;
	}

	int findMaxMovement(List<Point> bouldersInMotion, Point start, Point dir) {
		int result = 0;
		for (int i = 1; i < Math.max(w,h); i++) {
			Point p = linear(start, dir, i);
			if (p.x < 0 || p.x >= w || p.y < 0 || p.y >= h) {
				break;
			}
			if (!cubes.contains(p) && !bouldersInMotion.contains(p)) {
				result ++;
				continue;
			}
			if (cubes.contains(p)) {
				break;
			}
		}
		return result;
	}

	String makeKey(List<Point> input) {
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < h; x++) {
				Point p = new Point(x, y);
				if (input.contains(p)) {
					sb.append(p);
				}
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2023, 14));

		// part 1 - 110565 (0.372s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 89845 (26.51s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
