package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2023, day = 16, title="The Floor Will Be Lava", keywords = {"grid", "pathing"})
public class Exercise16 {

	private final static char EMPTY = '.';
	private final static char VER = '|';
	private final static char HOR = '-';
	private final static char SWNE = '/';
	private final static char NWSE = '\\';
	private final static Map<Character, Map<Point, List<Point>>> NEW_DIRS = Map.of(
		HOR, Map.of(
				NORTH, List.of(EAST, WEST),
				EAST, Collections.singletonList(EAST),
				SOUTH, List.of(EAST, WEST),
				WEST, Collections.singletonList(WEST)
			),
		VER, Map.of(
				NORTH, Collections.singletonList(NORTH),
				EAST,  List.of(NORTH, SOUTH),
				SOUTH, Collections.singletonList(SOUTH),
				WEST, List.of(NORTH, SOUTH)
			),
		NWSE, Map.of(
				NORTH, Collections.singletonList(WEST),
				EAST, Collections.singletonList(SOUTH),
				SOUTH, Collections.singletonList(EAST),
				WEST, Collections.singletonList(NORTH)
			),
		SWNE, Map.of(
					NORTH, Collections.singletonList(EAST),
					EAST, Collections.singletonList(NORTH),
					SOUTH, Collections.singletonList(WEST),
					WEST, Collections.singletonList(SOUTH)
			)
	);

	List<String> lines;
	Set<Pair<Point, Point>> beenHere;
	List<Pair<Point, Point>> queue;
	int w, h;
	char[][] grid;
	boolean[][] lit;

	public Exercise16() {
		lines = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		h = lines.size();
		w = lines.get(0).length();
		grid = new char[w][h];
		for (int y = 0; y < h; y++) {
			String line = lines.get(y);
			for (int x = 0; x < w; x++) {
				grid[x][y] = line.charAt(x);
			}
		}
	}

	public long doPart1(Point loc, Point dir) {
		beenHere = new HashSet<>();
		lit = new boolean[w][h];
		queue = new ArrayList<>();
		queue.add(new Pair<>(loc, dir));
		followBeam();
		long accum = 0;
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				accum += lit[x][y] ? 1 : 0;
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (int i = 0; i < w; i++) {
			for (int j=0; j < h; j++) {
				if (i ==0 || i == w-1 || j ==0 || j == h-1) { // edge
					for (Point dir: ORTHO_DIRS) {
						accum = Math.max(accum, doPart1(new Point(i, j), dir));
					}
				}
			}
		}
		return accum;
	}

	public void followBeam() {
		while (queue.size() > 0) {
			Pair<Point, Point> current = queue.remove(0);
			Point loc = current.getLeft();
			Point dir = current.getRight();
			if (loc.x < 0 || loc.x >= w || loc.y < 0 || loc.y >= h) {
				continue;
			}
			if (beenHere.contains(current)) {
				continue;
			}
			beenHere.add(new Pair<>(loc, dir));
			lit[loc.x][loc.y] = true;
			char c = grid[loc.x][loc.y];
			if (c != EMPTY) {
				List<Point> newDirs = NEW_DIRS.get(c).get(dir);
				if (newDirs.size() == 1) {
					queue.add(new Pair<>(sum(loc, newDirs.get(0)), newDirs.get(0)));
				} else {
					for (Point newDir : newDirs) {
						Pair<Point, Point> p = new Pair<>(sum(loc, newDir), newDir);
						if (!beenHere.contains(p)) {
							queue.add(p);
						}
					}
				}
			} else {
				queue.add(new Pair<>(sum(loc, dir), dir));
			}
		}
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2023, 16));

		// Process input (0.006s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 8249 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(new Point(0, 0), EAST));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 8444 (1.314s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
