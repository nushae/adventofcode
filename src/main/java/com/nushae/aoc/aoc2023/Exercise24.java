package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BigFraction;
import com.nushae.aoc.domain.Pair;
import com.nushae.aoc.domain.Triplet;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.gaussianReduce;

@Log4j2
@Aoc(year = 2023, day = 24, title="Never Tell Me The Odds", keywords = {"vectors", "equations"})
public class Exercise24 {

	List<String> lines;
	List<Pair<Triplet<BigInteger, BigInteger, BigInteger>, Triplet<BigInteger, BigInteger, BigInteger>>> hailStones;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		hailStones = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(" +@ +");
			hailStones.add(new Pair<>(getTriplet(parts[0]), getTriplet(parts[1])));
		}
		System.out.println(lines.size() + " / " + hailStones.size());
	}

	public Triplet<BigInteger, BigInteger, BigInteger> getTriplet(String values) {
		String[] parts = values.split(" *, *");
		return new Triplet<>(new BigInteger(parts[0]), new BigInteger(parts[1]), new BigInteger(parts[2]));
	}

	public long doPart1(long lowerX, long higherX, long lowerY, long higherY) {
		long accum = 0;
		for (int i = 0; i < hailStones.size()-1; i++) {
			for (int j = i+1; j < hailStones.size(); j++) {
				Pair<BigDecimal, BigDecimal> crossing = findFutureCrossingXY(hailStones.get(i), hailStones.get(j));
				if (crossing != null) {
					if (crossing.getLeft().compareTo(BigDecimal.valueOf(lowerX)) >= 0 &&
							crossing.getLeft().compareTo(BigDecimal.valueOf(higherX)) <= 0 &&
							crossing.getRight().compareTo(BigDecimal.valueOf(lowerY)) >= 0 &&
							crossing.getRight().compareTo(BigDecimal.valueOf(higherY)) <= 0) {
						log.debug("   Also within bounds");
						accum++;
					} else {
						log.debug("   But out of bounds");
					}
				}
			}
		}
		return accum;
	}

	Pair<BigDecimal, BigDecimal> findFutureCrossingXY(Pair<Triplet<BigInteger, BigInteger, BigInteger>, Triplet<BigInteger, BigInteger, BigInteger>> first, Pair<Triplet<BigInteger, BigInteger, BigInteger>, Triplet<BigInteger, BigInteger, BigInteger>> second) {
		BigInteger x1 = first.getLeft().getLeft();
		BigInteger y1 = first.getLeft().getMid();
		BigInteger x2 = x1.add(first.getRight().getLeft());
		BigInteger y2 = y1.add(first.getRight().getMid());
		BigInteger x3 = second.getLeft().getLeft();
		BigInteger y3 = second.getLeft().getMid();
		BigInteger x4 = x3.add(second.getRight().getLeft());
		BigInteger y4 = y3.add(second.getRight().getMid());

		log.debug(String.format("(%d, %d)-(%d, %d) vs (%d, %d)-(%d, %d)%n", x1, y1, x2, y2, x3, y3, x4, y4));

		BigInteger denom = x1.subtract(x2).multiply(y3.subtract(y4)).subtract(y1.subtract(y2).multiply(x3.subtract(x4)));

		if (denom.compareTo(BigInteger.ZERO) != 0) {
			BigInteger numer1 = x1.multiply(y2).subtract(x2.multiply(y1)).multiply(x3.subtract(x4)).subtract(x1.subtract(x2).multiply(x3.multiply(y4).subtract(x4.multiply(y3))));
			BigInteger numer2 = x1.multiply(y2).subtract(x2.multiply(y1)).multiply(y3.subtract(y4)).subtract(y1.subtract(y2).multiply(x3.multiply(y4).subtract(x4.multiply(y3))));

			BigDecimal crossX = new BigDecimal(numer1).divide(new BigDecimal(denom), RoundingMode.HALF_UP);
			if (crossX.subtract(new BigDecimal(x1)).signum() == x2.subtract(x1).signum() && crossX.subtract(new BigDecimal(x3)).signum() == x4.subtract(x3).signum()) { // in future
				BigDecimal crossY = new BigDecimal(numer2).divide(new BigDecimal(denom), RoundingMode.HALF_UP);
				if (crossY.subtract(new BigDecimal(y1)).signum() == y2.subtract(y1).signum() && crossY.subtract(new BigDecimal(y3)).signum() == y4.subtract(y3).signum()) { // in future
					log.debug("   Future crossing :)");
					return new Pair<>(crossX, crossY);
				}
			}
			log.debug("   Crossing (partially) in past :(");
		} else {
			log.debug("   Parallel trajectories - no crossing :(");
		}
		return null;
	}

	// DZJIEZUS KWAIST
	// given a stone thrown from p0 = (xs,ys, zs) at velocity v0 = (vx, vy, vz), we have the line p0 + t*v0
	// we need crossings with every hailstone, so we have a lot of equations to solve.
	// Using convention pi = (xi, yi, zi), vi = (vxi, vyi, vzi):
	//
	//     pi + ti*vi = p0 + ti*v0    for all i = 1...N
	//
	// rewrite:
	//
	//     p0 - pi = ti * (vi - v0) = -ti * (v0 - vi)
	//
	// take cross product with (v0 - vi):
	//
	//     (p0 - pi) x (v0 - vi) = -ti * (v0 - vi) x (v0 - vi)
	//
	// since A x A = 0 for all A:
	//
	//     (p0 - pi) x (v0 - vi) = 0
	//     p0 x (v0 - vi) - pi x (v0 - vi) = 0
	// (0) p0 x v0 - p0 x vi - pi x v0 + pi x vi = 0
	//
	// the 'hard' term (p0 x v0) can be eliminated by substituting another equation for it:
	//     p0 x v0 - p0 x v1 - p1 x v0 + p1 x v1 = p0 x v0 - P0 x v2 - p2 x V0 + p2 x v2
	//     p1 x v1 - p0 x v1 - p1 x v0 = p2 x v2 - p0 x v2 - p2 x v0
	//     p1 x v1 - p0 x v1 - p1 x v0 - p2 x v2 + p0 x v2 + p2 x v0 = 0
	// (1) p1 x v1 - p2 x v2 + p0 x (v2 - v1) + (p2 - p1) x v0 = 0
	//
	// repeat with another substitution:
	//     p1 x v1 - p0 x v1 - p1 x V0 = p3 x v3 - p0 x v3 - p3 x v0
	//     p1 x v1 - p0 x v1 - p1 x V0 - p3 x v3 + p0 x v3 + p3 x v0 = 0
	// (2) p1 x v1 - p3 x v3 + p0 x (v3 - v1) + (p3 - p1) x v0 = 0
	//
	// Writing out (1) and (2) per coordinate and resolving the x products:
	//     [x1, y1, z1] x [vx1, vy1, vz1] - [x2, y2, z2] x [vx2, vy2, vz2] + [xs, ys, zs] x [(vx2 - vx1), (vy2 - vy1), (vz2 - vz1)] + [(x2 - x1), (y2 - y1), (z2 - z1)] x [vx, vy, vz] = 0
	// (3) [y1*vz1 - z1*vy1, z1*vx1 - x1*vz1, x1*vy1 - y1*vx1]
	//   - [y2*vz2 - z2*vy2, z2*vx2 - x2*vz2, x2*vy2 - y2*vx2]
	//   + [ys*(vz2 - vz1) - zs*(vy2 - vy1), zs*(vx2 - vx1) - xs*(vz2 - vz1), xs*(vy2 - vy1) - ys*(vx2 - vx1)]
	//   + [vz*(y2 - y1) - vy*(z2 - z1), vx*(z2 - z1) - vz*(x2 - x1), vy*(x2 - x1) - vx*(y2 - y1)] = 0
	// and
	//     [x1, y1, z1] x [vx1, vy1, vz1] - [x3, y3, z3] x [vx3, vy3, vz3] + [xs, ys, zs] x [vx3 - vx1, vy3 - vy1, vz3 - vz1] + [x3 - x1, y3 - y1, z3 - z1] x [vx, vy, vz] = 0
	// (4) [y1*vz1 - z1*vy1, z1*vx1 - x1*vz1, x1*vy1 - y1*vx1]
	//   - [y3*vz3 - z3*vy3, z3*vx3 - x3*vz3, x3*vy3 - y3*vx3]
	//   + [ys*(vz3 - vz1) - zs*(vy3 - vy1), zs*(vx3 - vx1) - xs*(vz3 - vz1), xs*(vy3 - vy1) - ys*(vx3 - vx1)]
	//   + [vz*(y3 - y1) - vy*(z3 - z1), vx*(z3 - z1) - vz*(x3 - x1), vy*(x3 - x1) - vx*(y3 - y1)] = 0
	//
	// (3) and (4) each represent three linear equations, for a total of 6, with 6 unknowns in total:
	// (3a) y1*vz1 - z1*vy1 - y2*vz2 + z2*vy2 + ys*(vz2 - vz1) - zs*(vy2 - vy1) + vz*(y2 - y1) - vy*(z2 - z1) = 0
	// (3b) z1*vx1 - x1*vz1 - z2*vx2 + x2*vz2 + zs*(vx2 - vx1) - xs*(vz2 - vz1) + vx*(z2 - z1) - vz*(x2 - x1) = 0
	// (3c) x1*vy1 - y1*vx1 - x2*vy2 + y2*vx2 + xs*(vy2 - vy1) - ys*(vx2 - vx1) + vy*(x2 - x1) - vx*(y2 - y1) = 0
	// (4a) y1*vz1 - z1*vy1 - y3*vz3 + z3*vy3 + ys*(vz3 - vz1) - zs*(vy3 - vy1) + vz*(y3 - y1) - vy*(z3 - z1) = 0
	// (4b) z1*vx1 - x1*vz1 - z3*vx3 + x3*vz3 + zs*(vx3 - vx1) - xs*(vz3 - vz1) + vx*(z3 - z1) - vz*(x3 - x1) = 0
	// (4c) x1*vy1 - y1*vx1 - x3*vy3 + y3*vx3 + xs*(vy3 - vy1) - ys*(vx3 - vx1) + vy*(x3 - x1) - vx*(y3 - y1) = 0
	//
	// Now we sort all the factors for the unknowns:
	//    (constant part)              g                a                b                c              d              e              f
	// _________________________________   ______________   ______________   ______________   ____________   ____________   ____________
	// y1*vz1 - z1*vy1 - y2*vz2 + z2*vy2                  + ys*(vz2 - vz1) - zs*(vy2 - vy1)                - vy*(z2 - z1) + vz*(y2 - y1) = 0
	// z1*vx1 - x1*vz1 - z2*vx2 + x2*vz2 - xs*(vz2 - vz1)                  + zs*(vx2 - vx1) + vx*(z2 - z1)                - vz*(x2 - x1) = 0
	// x1*vy1 - y1*vx1 - x2*vy2 + y2*vx2 + xs*(vy2 - vy1) - ys*(vx2 - vx1)                  - vx*(y2 - y1) + vy*(x2 - x1)                = 0
	// y1*vz1 - z1*vy1 - y3*vz3 + z3*vy3                  + ys*(vz3 - vz1) - zs*(vy3 - vy1)                - vy*(z3 - z1) + vz*(y3 - y1) = 0
	// z1*vx1 - x1*vz1 - z3*vx3 + x3*vz3 - xs*(vz3 - vz1)                  + zs*(vx3 - vx1) + vx*(z3 - z1)                - vz*(x3 - x1) = 0
	// x1*vy1 - y1*vx1 - x3*vy3 + y3*vx3 + xs*(vy3 - vy1) - ys*(vx3 - vx1)                  - vx*(y3 - y1) + vy*(x3 - x1)                = 0
    //
	// Move over constant part and normalize signs:
	//              a                b                c              d              e              f                                      g
	// ______________   ______________   ______________   ____________   ____________   ____________   ____________________________________
	//                + ys*(vz2 - vz1) + zs*(vy1 - vy2)                + vy*(z1 - z2) + vz*(y2 - y1) = -(y1*vz1 - z1*vy1 - y2*vz2 + z2*vy2)
	// xs*(vz1 - vz2)                  + zs*(vx2 - vx1) + vx*(z2 - z1)                + vz*(x1 - x2) = -(z1*vx1 - x1*vz1 - z2*vx2 + x2*vz2)
	// xs*(vy2 - vy1) + ys*(vx1 - vx2)                  + vx*(y1 - y2) + vy*(x2 - x1)                = -(x1*vy1 - y1*vx1 - x2*vy2 + y2*vx2)
	//                + ys*(vz3 - vz1) + zs*(vy1 - vy3)                + vy*(z1 - z3) + vz*(y3 - y1) = -(y1*vz1 - z1*vy1 - y3*vz3 + z3*vy3)
	// xs*(vz1 - vz3)                  + zs*(vx3 - vx1) + vx*(z3 - z1)                + vz*(x1 - x3) = -(z1*vx1 - x1*vz1 - z3*vx3 + x3*vz3)
	// xs*(vy3 - vy1) + ys*(vx1 - vx3)                  + vx*(y1 - y3) + vy*(x3 - x1)                = -(x1*vy1 - y1*vx1 - x3*vy3 + y3*vx3)
	//
	// simplify to matrix notation:
	//           a                b                c              d              e              f                                         g
	// ___________      ___________      ___________      _________      _________      _________      ____________________________________
	//                  (vz2 - vz1)      (vy1 - vy2)                     (z1 - z2)      (y2 - y1)    = -(y1*vz1 - z1*vy1 - y2*vz2 + z2*vy2)
	// (vz1 - vz2)                       (vx2 - vx1)      (z2 - z1)                     (x1 - x2)    = -(z1*vx1 - x1*vz1 - z2*vx2 + x2*vz2)
	// (vy2 - vy1)      (vx1 - vx2)                       (y1 - y2)      (x2 - x1)                   = -(x1*vy1 - y1*vx1 - x2*vy2 + y2*vx2)
	//                  (vz3 - vz1)      (vy1 - vy3)                     (z1 - z3)      (y3 - y1)    = -(y1*vz1 - z1*vy1 - y3*vz3 + z3*vy3)
	// (vz1 - vz3)                       (vx3 - vx1)      (z3 - z1)                     (x1 - x3)    = -(z1*vx1 - x1*vz1 - z3*vx3 + x3*vz3)
	// (vy3 - vy1)      (vx1 - vx3)                       (y1 - y3)      (x3 - x1)                   = -(x1*vy1 - y1*vx1 - x3*vy3 + y3*vx3)
	//
	// This can then be sweeped to arrive at the answer. Precision is an issue though. Luckily, because all starting
	// values are integers, all operations will yield rational values; so we introduce the BigRational
	public long doPart2() {
		BigInteger x1 = hailStones.get(0).getLeft().getLeft();
		BigInteger y1 = hailStones.get(0).getLeft().getMid();
		BigInteger z1 = hailStones.get(0).getLeft().getRight();
		BigInteger vx1 = hailStones.get(0).getRight().getLeft();
		BigInteger vy1 = hailStones.get(0).getRight().getMid();
		BigInteger vz1 = hailStones.get(0).getRight().getRight();
		BigInteger x2 = hailStones.get(1).getLeft().getLeft();
		BigInteger y2 = hailStones.get(1).getLeft().getMid();
		BigInteger z2 = hailStones.get(1).getLeft().getRight();
		BigInteger vx2 = hailStones.get(1).getRight().getLeft();
		BigInteger vy2 = hailStones.get(1).getRight().getMid();
		BigInteger vz2 = hailStones.get(1).getRight().getRight();
		BigInteger x3 = hailStones.get(2).getLeft().getLeft();
		BigInteger y3 = hailStones.get(2).getLeft().getMid();
		BigInteger z3 = hailStones.get(2).getLeft().getRight();
		BigInteger vx3 = hailStones.get(2).getRight().getLeft();
		BigInteger vy3 = hailStones.get(2).getRight().getMid();
		BigInteger vz3 = hailStones.get(2).getRight().getRight();
		// fill sweep variables:
		BigFraction[][] factors = new BigFraction[6][7];
		// a
		factors[0][0] = BigFraction.ZERO;
		factors[1][0] = new BigFraction(vz1.subtract(vz2));
		factors[2][0] = new BigFraction(vy2.subtract(vy1));
		factors[3][0] = BigFraction.ZERO;
		factors[4][0] = new BigFraction(vz1.subtract(vz3));
		factors[5][0] = new BigFraction(vy3.subtract(vy1));
		// b
		factors[0][1] = new BigFraction(vz2.subtract(vz1));
		factors[1][1] = BigFraction.ZERO;
		factors[2][1] = new BigFraction(vx1.subtract(vx2));
		factors[3][1] = new BigFraction(vz3.subtract(vz1));
		factors[4][1] = BigFraction.ZERO;
		factors[5][1] = new BigFraction(vx1.subtract(vx3));
		// c
		factors[0][2] = new BigFraction(vy1.subtract(vy2));
		factors[1][2] = new BigFraction(vx2.subtract(vx1));
		factors[2][2] = BigFraction.ZERO;
		factors[3][2] = new BigFraction(vy1.subtract(vy3));
		factors[4][2] = new BigFraction(vx3.subtract(vx1));
		factors[5][2] = BigFraction.ZERO;
		// d
		factors[0][3] = BigFraction.ZERO;
		factors[1][3] = new BigFraction(z2.subtract(z1));
		factors[2][3] = new BigFraction(y1.subtract(y2));
		factors[3][3] = BigFraction.ZERO;
		factors[4][3] = new BigFraction(z3.subtract(z1));
		factors[5][3] = new BigFraction(y1.subtract(y3));
		// e
		factors[0][4] = new BigFraction(z1.subtract(z2));
		factors[1][4] = BigFraction.ZERO;
		factors[2][4] = new BigFraction(x2.subtract(x1));
		factors[3][4] = new BigFraction(z1.subtract(z3));
		factors[4][4] = BigFraction.ZERO;
		factors[5][4] = new BigFraction(x3.subtract(x1));
		// f
		factors[0][5] = new BigFraction(y2.subtract(y1));
		factors[1][5] = new BigFraction(x1.subtract(x2));
		factors[2][5] = BigFraction.ZERO;
		factors[3][5] = new BigFraction(y3.subtract(y1));
		factors[4][5] = new BigFraction(x1.subtract(x3));
		factors[5][5] = BigFraction.ZERO;
		// g
		factors[0][6] = new BigFraction(y1.multiply(vz1).subtract(z1.multiply(vy1)).subtract(y2.multiply(vz2)).add(z2.multiply(vy2)).negate());
		factors[1][6] = new BigFraction(z1.multiply(vx1).subtract(x1.multiply(vz1)).subtract(z2.multiply(vx2)).add(x2.multiply(vz2)).negate());
		factors[2][6] = new BigFraction(x1.multiply(vy1).subtract(y1.multiply(vx1)).subtract(x2.multiply(vy2)).add(y2.multiply(vx2)).negate());
		factors[3][6] = new BigFraction(y1.multiply(vz1).subtract(z1.multiply(vy1)).subtract(y3.multiply(vz3)).add(z3.multiply(vy3)).negate());
		factors[4][6] = new BigFraction(z1.multiply(vx1).subtract(x1.multiply(vz1)).subtract(z3.multiply(vx3)).add(x3.multiply(vz3)).negate());
		factors[5][6] = new BigFraction(x1.multiply(vy1).subtract(y1.multiply(vx1)).subtract(x3.multiply(vy3)).add(y3.multiply(vx3)).negate());
		// sweep:
		BigFraction[] result = gaussianReduce(factors);
		return result[0].add(result[1]).add(result[2]).longValue();
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2023, 24));

		// Process input (0.019s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 18651 (0.716s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(200000000000000L, 400000000000000L, 200000000000000L, 400000000000000L));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 546494494317645 (0.014s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
