package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import com.nushae.aoc.domain.Triplet;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;
import static com.nushae.aoc.util.MathUtil.sum;

@Log4j2
@Aoc(year = 2023, day = 21, title="Step Counter", keywords = {"grid", "pathing", "extrapolation", "quadratic-formula", "gaussian-reduction"})
public class Exercise21 {

	List<String> lines;
	int w, h;
	char[][] grid;
	Point start;
	Map<Point, Set<Point>> visitsByMapCopy;

	public Exercise21() {
		lines = new ArrayList<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		w = lines.get(0).length();
		h = lines.size();
		grid = new char[w][h];
		for (int y = 0; y < h; y++) {
			String line = lines.get(y);
			for (int x = 0; x < w; x++) {
				grid[x][y] = line.charAt(x);
				if (grid[x][y] == 'S') {
					grid[x][y] = '.';
					start = new Point(x, y);
				}
			}
		}
	}

	public long doPart1(int steps) {
		Set<Point> queue = new HashSet<>();
		Set<Point> result;
		queue.add(start);
		for (int s = 0; s < steps; s++) {
			result = new HashSet<>();
			for (Point current : queue) {
				for (Point dir : ORTHO_DIRS) {
					Point p = sum(current, dir);
					if (p.x >=0 && p.x < w && p.y >=0 && p.y < h && grid[p.x][p.y] == '.') {
						result.add(p);
					}
				}
			}
			queue = result;
		}
		return queue.size();
	}

	// written after submission, using the submitted research tools
	public long doPart2Automated(int steps) {
		visitsByMapCopy = new HashMap<>();
		Map<Point, Set<Point>> result;
		Set<Point> initial = new HashSet<>();
		initial.add(start);
		visitsByMapCopy.put(start, initial);
		for (int windowsize = w; windowsize > 0; windowsize--) {
			int remainder = steps % windowsize;
			long lastCount = 0;
			List<Pair<Long, Long>> measurements = new ArrayList<>();
			long lastDiff = 0;
			long lastDiffdiff = 0;
			for (int s = 0; measurements.size() < 3 && s < 10*windowsize; s++) {
				result = new HashMap<>();
				for (Point current : visitsByMapCopy.keySet()) {
					for (Point dir : ORTHO_DIRS) {
						Point p = sum(current, dir);
						if (grid[(p.x + w) % w][(p.y + h) % h] == '.') {
							for (Point mapLocation : visitsByMapCopy.get(current)) {
								if (p.x < 0) { // go one map west
									Point p2 = new Point(p.x + w, p.y);
									Set<Point> points = result.getOrDefault(p2, new HashSet<>());
									points.add(new Point(mapLocation.x - 1, mapLocation.y));
									result.put(p2, points);
								} else if (p.x >= w) { // go one map east
									Point p2 = new Point(p.x - w, p.y);
									Set<Point> points = result.getOrDefault(p2, new HashSet<>());
									points.add(new Point(mapLocation.x + 1, mapLocation.y));
									result.put(p2, points);
								} else if (p.y < 0) { // go one map north
									Point p2 = new Point(p.x, p.y + h);
									Set<Point> points = result.getOrDefault(p2, new HashSet<>());
									points.add(new Point(mapLocation.x, mapLocation.y - 1));
									result.put(p2, points);
								} else if (p.y >= h) { // go one map south
									Point p2 = new Point(p.x, p.y - h);
									Set<Point> points = result.getOrDefault(p2, new HashSet<>());
									points.add(new Point(mapLocation.x, mapLocation.y + 1));
									result.put(p2, points);
								} else { // same map
									Set<Point> points = result.getOrDefault(p, new HashSet<>());
									points.add(mapLocation);
									result.put(p, points);
								}
							}
						}
					}
				}
				visitsByMapCopy = result;
				if ((s + 1) % windowsize == remainder) { // extrapolating from this cycle we would end precisely at steps
					long count = count(result);
					long diff = count - lastCount;
					long diffdiff = diff - lastDiff;
					log.debug(String.format("%3d window, %5d steps, %6d total, %6d diff, %6d diffdiff", windowsize, s + 1, count, diff, diffdiff));
					lastCount = count;
					lastDiff = diff;
					if (diffdiff == lastDiffdiff) {
						// lastDiffdiff has stabilized -> it IS a quadratic function!!
						measurements.add(new Pair<>((long) s+1, count));
					}
					lastDiffdiff = diffdiff;
				}
			}
			if (measurements.size() >= 3) {
				// use measurements to determine quadratic function
				// We have: f(x) = A * x^2 + B * x + C = y
				// Three measurements of x and y are enough to determine A, B and C, using standard matrix sweeping
				Triplet<Double, Double, Double> factors = sweep(
						measurements.get(0).getLeft() * measurements.get(0).getLeft(), measurements.get(0).getLeft(), measurements.get(0).getRight(),
						measurements.get(1).getLeft() * measurements.get(1).getLeft(), measurements.get(1).getLeft(), measurements.get(1).getRight(),
						measurements.get(2).getLeft() * measurements.get(2).getLeft(), measurements.get(2).getLeft(), measurements.get(2).getRight());
				return Math.round(factors.getLeft() * steps * steps + factors.getMid() * steps + factors.getRight());
			}
		}
		return count(visitsByMapCopy);
	}

	public long count(Map<Point, Set<Point>> result) {
		long accum = 0;
		for (Point p : result.keySet()) {
			accum += result.get(p).size();
		}
		return accum;
	}

	Triplet<Double, Double, Double> sweep(double a1, double b1, double y1, double a2, double b2, double y2, double a3, double b3, double y3) {
		double c1 = 1;
		double c2 = 1;
		double c3 = 1;
		log.debug("Starting values");
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a1, b1, c1, y1));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a2, b2, c2, y2));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a3, b3, c3, y3));
		// sweep c
		a2 = a2 - c2/c1 * a1;
		b2 = b2 - c2/c1 * b1;
		y2 = y2 - c2/c1 * y1;
		c2 = 0;
		a3 = a3 - c3/c1 * a1;
		b3 = b3 - c3/c1 * b1;
		y3 = y3 - c3/c1 * y1;
		c3 = 0;
		log.debug("After c sweep");
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a1, b1, c1, y1));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a2, b2, c2, y2));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a3, b3, c3, y3));
		// sweep b
		a1 = a1 - b1/b2 * a2;
		c1 = c1 - b1/b2 * c2;
		y1 = y1 - b1/b2 * y2;
		b1 = 0;
		a3 = a3 - b3/b2 * a2;
		c3 = c3 - b3/b2 * c2;
		y3 = y3 - b3/b2 * y2;
		b3 = 0;
		log.debug("After b sweep");
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a1, b1, c1, y1));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a2, b2, c2, y2));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a3, b3, c3, y3));
		// sweep a
		y1 = y1 - a1/a3 * y3;
		b1 = b1 - a1/a3 * b3;
		c1 = c1 - a1/a3 * c3;
		a1 = 0;
		y2 = y2 - a2/a3 * y3;
		b2 = b2 - a2/a3 * b3;
		c2 = c2 - a2/a3 * c3;
		a2 = 0;
		log.debug("After a sweep");
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a1, b1, c1, y1));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a2, b2, c2, y2));
		log.debug(String.format("%10.5f  %10.5f  %10.5f  |  %10.5f%n", a3, b3, c3, y3));
		double a = y3/a3;
		double b = y2/b2;
		double c = y1/c1;
		log.debug("a: " + a +", b: " + b + ", c: " + c);
		return new Triplet<>(a, b, c);
	}

	public long doPart2Hardcoded(int steps) {
		double a = 0.863119864809743;
		double b = 1.7257152846571144;
		double c = 0.14707767617073841;
		return Math.round(a * steps*steps + b * steps + c);
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2023, 21));

		// Process input (0.002s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 3646 (0.299s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(64));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 606188414811259 (Automated: 31.386s; hardcoded: < 0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		//log.info(ex.doPart2Automated(26501365));
		log.info(ex.doPart2Hardcoded(26501365));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
