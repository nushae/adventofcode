package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 3, title = "Gear Ratios", keywords = {"grid", "map-reduce", "filter"})
public class Exercise03 {

	private static final Pattern NUMBER = Pattern.compile("([0-9]+)");

	private List<String> lines;
	private Map<Point, String> numMap;
	private List<Point> cogs;
	private List<Point> symbols;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		numMap = new HashMap<>();
		cogs = new ArrayList<>();
		symbols = new ArrayList<>();
		for (int lineNum = 0; lineNum < lines.size(); lineNum++) {
			String line = lines.get(lineNum);
			// symbols
			for (int i=0; i < line.length(); i++) {
				if (!"0123456789.".contains(""+line.charAt(i))) {
					symbols.add(new Point(i, lineNum));
					if (line.charAt(i) == '*') { // for part 2
						cogs.add(new Point(i, lineNum));
					}
				}
			}
			// numbers
			Matcher m = NUMBER.matcher(line);
			boolean foundAtLeastOne = false;
			while (m.find()) {
				numMap.put(new Point(m.start(1), lineNum), m.group(1));
				foundAtLeastOne = true;
			}
			if (!foundAtLeastOne) {
				log.warn("Don't know how to parse " + line);
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (Map.Entry<Point, String> entry : numMap.entrySet()) {
			for (Point p: symbols) {
				if (isAdjacent(entry, p)) {
					accum += Long.parseLong(entry.getValue());
					break;
				}
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (Point p : cogs) {
			log.debug("Cog: " + p);
			List<Long> nums = findAdjacent(p);
			log.debug("Adjacent numbers: " + nums);
			if (nums.size() == 2) {
				log.debug("Including " + nums.get(0) + " * " + nums.get(1) + " = " + (nums.get(0) * nums.get(1)));
				accum += nums.get(0) * nums.get(1);
			}
		}
		return accum;
	}

	private List<Long> findAdjacent(Point p) {
		List<Long> result = new ArrayList<>();
		for (Map.Entry<Point, String> entry : numMap.entrySet()) {
			if (isAdjacent(entry, p)) {
				log.debug("Was added");
				result.add(Long.parseLong(entry.getValue()));
			}
		}
		return result;
	}

	private static boolean isAdjacent(Map.Entry<Point, String> entry, Point p) {
		Point n = entry.getKey();
		int leftSide = n.x;
		int rightSide = n.x + entry.getValue().length() - 1;
		log.debug("Number: (" + leftSide + "-" + rightSide + ", " + n.y + ")");
		return (Math.abs(n.y - p.y) <= 1 && p.x >= leftSide -1 && p.x <= rightSide + 1);
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2023, 3));

		// Process input (0.015s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 540025 (0.054s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 84584891 (0.038s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
