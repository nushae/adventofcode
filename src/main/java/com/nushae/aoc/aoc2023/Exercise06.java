package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
@Aoc(year = 2023, day = 6, title = "Wait For It", keywords = {"race", "combinatorics", "quadratic-formula"})
public class Exercise06 {

	private List<Point> races;

	public Exercise06(Point... data) {
		races = new ArrayList<>();
		races.addAll(Arrays.asList(data));
	}

	static long countBetterResults(long raceLength, long bestDistance) {
		// number of seconds held H == speed S. So we are looking at a quadratic function over 'time held':
		// -S^2 + L*S - B = 0 are the points where we achieve the same distance as the best race so far;
		// since the factor of the square is negative, everything *between* the two is better.
		if (raceLength*raceLength > 4*bestDistance) { // we have two real solutions
			double discriminant = Math.sqrt(raceLength * raceLength - 4 * bestDistance);
			double nullPoint1 = (-raceLength + discriminant) / -2;
			double nullPoint2 = (-raceLength - discriminant) / -2;

			log.debug(nullPoint1 + " / " + nullPoint2);

			// I didn't come up with EPSILON myself, but I love the pragmatism
			double EPSILON = 0.00000000000000000000000000000000000000000000000000000000000000001;
			long minTime = (long) Math.ceil(Math.max(0, nullPoint1 - EPSILON));
			long maxTime = (long) Math.floor(Math.min(raceLength, nullPoint2 + EPSILON));

			log.debug(minTime + " / " + maxTime);

			return Math.abs(maxTime - minTime  + 1);
		}
		return 0;
	}

	public long doPart1() {
		long accum = 1;
		for (Point p : races) {
			accum *= countBetterResults(p.x, p.y);
		}
		return accum;
	}

	public long doPart2(long time, long dist) {
		return countBetterResults(time, dist);
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(new Point(48, 296), new Point(93, 1928), new Point(85, 1236), new Point(95, 1391));

		// part 1 - 2756160 (0.003s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 34788142 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(48938595L, 296192812361391L));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
