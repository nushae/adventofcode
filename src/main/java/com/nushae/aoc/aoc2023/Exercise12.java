package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 12, title = "Hot Springs", keywords={"combinatorics", "memoization"})
public class Exercise12 {

	private static final char OPERATIONAL = '.';
	private static final char DAMAGED = '#';

	private List<String> lines;
	private List<Pair<String, List<Long>>> parsed1;
	private List<Pair<String, List<Long>>> parsed2;

	private Map<String, Long> cache;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		parsed1 = new ArrayList<>();
		parsed2 = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(" ");
			parsed1.add(new Pair<>(parts[0], Arrays.stream(parts[1].split(",")).map(Long::parseLong).toList()));
			String fiveTimes = line;
			for (int i=0; i < 4; i++) {
				fiveTimes = fiveTimes.replaceAll(" ", "?" + line + ",");
			}
			parts = fiveTimes.split(" ");
			parsed2.add(new Pair<>(parts[0], Arrays.stream(parts[1].split(",")).map(Long::parseLong).toList()));
		}
	}

	public long doPart1() {
		return calculate(parsed1);
	}

	public long doPart2() {
		return calculate(parsed2);
	}

	private long calculate(List<Pair<String, List<Long>>> input) {
		cache = new HashMap<>();
		long accum = 0;
		for (Pair<String, List<Long>> pair : input) {
			accum += count(pair.getLeft() + OPERATIONAL, 0, pair.getRight());
		}
		return accum;
	}

	public long count(String remainingInput, int currentSequenceLength, List<Long> remainingSequences) {
		String key = makeKey(remainingInput, currentSequenceLength, remainingSequences);

		if (cache.containsKey(key)) {
			return cache.get(key);
		}

		// 'leaf' cases
		if ("".equals(remainingInput)) {
			if (currentSequenceLength == 0 && remainingSequences.isEmpty()) {
				cache.put(key, 1L);
				return 1;
			}
			cache.put(key, 0L);
			return 0;
		}

		// recursion cases
		char c = remainingInput.charAt(0);
		long result;
		if (c == OPERATIONAL) {
			result = countOperationalCase(remainingInput, currentSequenceLength, remainingSequences);
		} else if (c == DAMAGED) {
			result = countDamagedCase(remainingInput, currentSequenceLength, remainingSequences);
		} else {
			result = countOperationalCase(remainingInput, currentSequenceLength, remainingSequences) +
					countDamagedCase(remainingInput, currentSequenceLength, remainingSequences);
		}
		cache.put(key, result);
		return result;
	}

	private long countOperationalCase(String remainingInput, int currentSequenceLength, List<Long> remainingSequences) {
		if (currentSequenceLength > 0) {
			if (remainingSequences.get(0) == currentSequenceLength) {
				List<Long> newSequences = new ArrayList<>(remainingSequences);
				newSequences.remove(0);
				return count(remainingInput.substring(1), 0, newSequences);
			} else {
				return 0;
			}
		} else {
			return count(remainingInput.substring(1), 0, remainingSequences);
		}
	}

	private long countDamagedCase(String remainingInput, int currentSequenceLength, List<Long> remainingSequences) {
		if (remainingSequences.isEmpty() || currentSequenceLength >= remainingSequences.get(0)) {
			return 0;
		} else {
			return count(remainingInput.substring(1), currentSequenceLength + 1, remainingSequences);
		}
	}

	private static String makeKey(String s, int i, List<Long> seq) {
		return i + s + StringUtils.join(seq, ",");
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2023, 12));

		// Process input (0.024s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 7407 (0.052s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 30568243604962 (1.124s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
