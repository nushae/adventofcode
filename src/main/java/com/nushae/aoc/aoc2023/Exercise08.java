package com.nushae.aoc.aoc2023;

import com.nushae.aoc.aoc2023.domain.BinTreeNode;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.lcm;

@Log4j2
@Aoc(year = 2023, day = 8, title = "Haunted Wasteland", keywords = {"graph", "cycle-detect", "lcm"})
public class Exercise08 {

	private static final Pattern NODE_DEF = Pattern.compile("([A-Z]+) = \\(([A-Z]+), ([A-Z]+)\\)");

	private List<String> lines;
	private List<BinTreeNode> startNodes;
	private String path;
	private BinTreeNode start;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		final Map<String, BinTreeNode> nodes = new HashMap<>();
		startNodes = new ArrayList<>();
		path = lines.get(0);
		for (int i = 2; i < lines.size(); i++) {
			String line = lines.get(i);
			Matcher m = NODE_DEF.matcher(line);
			if (m.find()) {
				String name = m.group(1);
				String leftName = m.group(2);
				String rightName = m.group(3);
				BinTreeNode topNode = nodes.getOrDefault(name, new BinTreeNode(name));
				nodes.put(name, topNode); // for the self referencing nodes
				BinTreeNode leftChild = nodes.getOrDefault(leftName, new BinTreeNode(leftName));
				nodes.put(leftName, leftChild);
				BinTreeNode rightChild = nodes.getOrDefault(rightName, new BinTreeNode(rightName));
				nodes.put(rightName, rightChild);
				topNode.setLeft(leftChild);
				topNode.setRight(rightChild);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		start = nodes.get("AAA");
		for (String key : nodes.keySet()) {
			if (key.endsWith("A")) {
				startNodes.add(nodes.get(key));
			}
		}
		log.info("Input processed (with pattern(s)).");
		log.info(startNodes.size() + " startNodes found for part 2");
	}

	private void analyzePath(BinTreeNode startNode, Predicate<String> goal) {
		BinTreeNode current = startNode;
		String currentPath = path;
		long steps = 0;
		for (int i=0; i < 1000000; i++) {
			if (goal.test(current.name)) {
				log.info("Steps: " + steps + " / node: " + current.name);
				steps = 0;
			}
			char c = currentPath.charAt(0);
			currentPath = currentPath.substring(1) + c;
			if (c == 'L') {
				log.debug("going L");
				current = current.left;
			} else {
				log.debug("going R");
				current = current.right;
			}
			steps++;
		}
	}

	public void analyze() {
		for (BinTreeNode node : startNodes) {
			analyzePath(node, s -> s.endsWith("Z"));
		}
	}

	public long doPart1() {
		return stepsToReach(start, "ZZZ"::equals);
	}

	public long doPart2() {
		// Hunch: what if the paths are cyclic? -> THEY ARE. So just return the lcm of all the individual path lengths!
		long result = 1;
		for (BinTreeNode node : startNodes) {
			long steps = stepsToReach(node, s -> s.endsWith("Z"));
			log.debug("Found path length: " + steps);
			result = lcm(result, steps);
		}
		return result;
	}

	private long stepsToReach(BinTreeNode startNode, Predicate<String> goal) {
		BinTreeNode current = startNode;
		String currentPath = path;
		long steps = 0;
		while (!goal.test(current.name)) {
			steps++;
			char c = currentPath.charAt(0);
			currentPath = currentPath.substring(1) + c;
			if (c == 'L') {
				log.debug("going L");
				current = current.left;
			} else {
				log.debug("going R");
				current = current.right;
			}
		}
		return steps;
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2023, 8));
		// Exercise08 ex = new Exercise08(aocPath(2023, 8, "colleague.txt"));

		// Process input (0.007s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 19199 (0.023s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// Use this to see why the hunch for part 2 panned out:
		// ex.analyze();

		// part 2 - 13663968099527 (0.285s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
