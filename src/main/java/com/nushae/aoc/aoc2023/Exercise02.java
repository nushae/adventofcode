package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 2, title = "Cube Conundrum", keywords = {"minmax", "filter"})
public class Exercise02 {
	private static final Pattern GAME_LINE = Pattern.compile("Game ([0-9]+): (.*)");

	private static final String KEY_RED = "red";
	private static final String KEY_GREEN = "green";
	private static final String KEY_BLUE = "blue";
	private static final int CUBES_RED = 12;
	private static final int CUBES_GREEN = 13;
	private static final int CUBES_BLUE = 14;

	private List<String> lines;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long calculate(boolean part2) {
		long accum = 0;
		for (String line : lines) {
			Matcher m = GAME_LINE.matcher(line);
			if (m.find()) {
				Map<String, Long> ballCounts = new HashMap<>();
				int id = Integer.parseInt(m.group(1));
				String[] games = m.group(2).split("; ");
				for (String game : games) {
					String[] amounts = game.split(", ");
					for (String amount : amounts) {
						long val = Long.parseLong(amount.split(" ")[0]);
						String key = amount.split(" ")[1];
						ballCounts.put(key, Math.max(ballCounts.getOrDefault(key, -1L), val));
					}
				}
				if (part2) {
					accum += ballCounts.get(KEY_RED) * ballCounts.get(KEY_GREEN) * ballCounts.get(KEY_BLUE);
				} else {
					if (ballCounts.get(KEY_RED) <= CUBES_RED && ballCounts.get(KEY_GREEN) <= CUBES_GREEN && ballCounts.get(KEY_BLUE) <= CUBES_BLUE) {
						accum += id;
					}
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2023, 2));

		// part 1 - 2164 (0.016s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.calculate(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 69929 (0.006s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.calculate(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
