package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 4, title = "Scratchcards", keywords = {"filter", "map-reduce", "sliding-window"})
public class Exercise04 {

	private List<String> lines;
	private Map<Long, Long> matches;
	private Map<Long, Long> allCopies;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		matches = new HashMap<>();
		allCopies = new HashMap<>();
		long lineNum = 0;
		for (String line : lines) {
			lineNum++;
			allCopies.put(lineNum, allCopies.getOrDefault(lineNum, 1L));
			line = line.split(": +")[1];
			String winNums = " " + line.split(" \\| +")[0] + " ";
			String[] myNums = line.split(" \\| +")[1].split(" +");
			long match = 0;
			for (String num : myNums) {
				if (winNums.contains(" " + num + " ")) { // match!!
					match++;
				}
			}
			if (match > 0) {
				long cardCopies = allCopies.get(lineNum);
				log.debug(lineNum + ": " + match + " winning numbers (and for part 2) " + cardCopies + " extra copies of the next " + match + " cards");
				matches.put(lineNum, match);
				for (int i = 1; i <= match; i++) {
					allCopies.put(lineNum+i, allCopies.getOrDefault(lineNum+i, 1L) + cardCopies);
				}
			}
		}
		log.info("Input processed.");
	}

	public long doPart1() {
		long accum = 0;
		for (long num : matches.values()) {
			accum += Math.pow(2, num-1);
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (Long num : allCopies.values()) {
			accum += num;
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2023, 4));

		// Process input (0.035s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 28538 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 9425961 (0.003s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
