package com.nushae.aoc.aoc2023.domain;

public class BinTreeNode {
	public BinTreeNode left, right;
	public String name;

	public BinTreeNode(BinTreeNode left, BinTreeNode right, String name) {
		this.left = left;
		this.right = right;
		this.name = name;
	}

	public BinTreeNode(String name) {
		this.name = name;
	}

	public void setLeft(BinTreeNode left) {
		this.left = left;
	}
	public void setRight(BinTreeNode right) {
		this.right = right;
	}
}

