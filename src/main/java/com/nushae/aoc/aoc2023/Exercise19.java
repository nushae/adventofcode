package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 19, title="Aplenty", keywords = {"filtering", "graph", "ranges", "flattening"})
public class Exercise19 {

	private static final Pattern RULES = Pattern.compile("(.+)\\{(.+)}");

	public record Item(Map<String, Integer> values, long score) {
		public Item(int x, int m, int a, int s) {
			this(Map.of("x", x, "m", m, "a", a, "s", s), x + m + a + s);
		}

		public static Item from(String definition) {
			String[] vals = definition.substring(1, definition.length()-1).split(",");
			Map<String, Integer> values = Map.of(
				vals[0].split("=")[0], Integer.parseInt(vals[0].split("=")[1]),
				vals[1].split("=")[0], Integer.parseInt(vals[1].split("=")[1]),
				vals[2].split("=")[0], Integer.parseInt(vals[2].split("=")[1]),
				vals[3].split("=")[0], Integer.parseInt(vals[3].split("=")[1])
			);
			long result = 0;
			for (int value : values.values()) {
				result += value;
			}
			return new Item(values, result);
		}
	}

	public record Rule(String name, List<Condition> conditions) {

		public static Rule from(String name, String definition) {
			List<Condition> conditions = new ArrayList<>();
			for (String filter : definition.split(",")) {
				conditions.add(Condition.from(filter));
			}
			return new Rule(name, conditions);
		}

		public long apply(Item item, Map<String, Rule> rules) {
			for (Condition cond : conditions) {
				String retVal = cond.apply(item);
				switch (retVal) {
					case "A":
						return item.score;
					case "R":
						return 0L;
					case "":
						continue;
					default:
						return rules.get(retVal).apply(item, rules);
				}
			}
			throw new IllegalArgumentException("Ruleset ended without redirect or accept/reject: " + conditions);
		}
	}

	public record Condition(boolean always, String nextRule, String attribute, boolean greaterthan, int limit) {

		public static Condition from(String filter) {
			if (!filter.contains(":")) {
				return new Condition(true, filter, "", false, -1);
			} else {
				String[] parts = filter.split(":");
				boolean gt = parts[0].contains(">");
				String[] conditionalParts = gt ? parts[0].split(">") : parts[0].split("<");
				return new Condition(false, parts[1], conditionalParts[0], gt, Integer.parseInt(conditionalParts[1]));
			}
		}

		public String apply(Item item) {
			if (always) {
				return nextRule;
			}
			if (greaterthan) {
				return item.values.get(attribute) > limit ? nextRule : "";
			} else {
				return item.values.get(attribute) < limit ? nextRule : "";
			}
		}
	}

	public record ItemLimits(Map<String, Integer> lowerLimits, Map<String, Integer> upperLimits, String nextRule) {

		public ItemLimits(int lower, int upper, String nextRule) {
			this(Map.of("x", lower, "m", lower, "a", lower, "s", lower), Map.of("x", upper, "m", upper, "a", upper, "s", upper), nextRule);
		}

		public List<ItemLimits> applyFilters(String rules) {
			List<ItemLimits> result = new ArrayList<>();
			Map<String, Integer> upper = new HashMap<>(upperLimits);
			Map<String, Integer> lower = new HashMap<>(lowerLimits);
			String[] parts = rules.split(",");
			for (String filter : parts) {
				if (!filter.contains(":")) {
					result.add(new ItemLimits(lower, upper, filter));
				} else { // calc new limits for split off part
					String[] pts = filter.split(":");
					boolean greaterthan = pts[0].contains(">");
					String[] cpt = greaterthan ? pts[0].split(">") : pts[0].split("<");
					String letter = cpt[0];
					int limit = Integer.parseInt(cpt[1]);
					if (greaterthan && limit <= lower.get(letter) || !greaterthan && limit >= upper.get(letter)) { // rule is neutral
						continue;
					}
					if (greaterthan) { // create new lower limit for this letter alone; adjust upper of our existing values
						Map<String, Integer> newLower = new HashMap<>(lower);
						newLower.put(letter, limit+1);
						result.add(new ItemLimits(newLower, upper, pts[1]));
						upper.put(letter, limit);
					} else { // create new upper limit for this letter alone; adjust lower of our existing values
						Map<String, Integer> newUpper = new HashMap<>(upper);
						newUpper.put(letter, limit-1);
						result.add(new ItemLimits(lower, newUpper, pts[1]));
						lower.put(letter, limit);
					}
				}
			}
			return result;
		}

		public long count() {
			long result = 1;
			for (String let : List.of("x", "m", "a", "s")) {
				result *= upperLimits.get(let) - lowerLimits.get(let) + 1;
			}
			return result;
		}
	}

	List<String> lines;
	Map<String, Rule> rules;
	Map<String, String> justTheFilters;
	List<Item> items;

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		var parts = false;
		rules = new HashMap<>();
		items = new ArrayList<>();
		justTheFilters = new HashMap<>();
		for (var line : lines) {
			if ("".equals(line)) {
				parts = true;
				continue;
			}
			if (!parts) {
				var m = RULES.matcher(line);
				if (m.find()) {
					rules.put(m.group(1), Rule.from(m.group(1), m.group(2)));
					justTheFilters.put(m.group(1), m.group(2));
				} else {
					log.warn("Don't know how to parse " + line);
				}
			} else {
				items.add(Item.from(line));
			}
		}
		log.info("Input processed (with pattern(s)).");
	}

	// I genuinely doubt whether using the flattening from part 2 will help here...
	public long doPart1() {
		var accum = 0L;
		var start = rules.get("in");
		for (var item : items) {
			accum += start.apply(item, rules);
		}
		return accum;
	}

	public long doPart2Stoopid(long millis) { // HA HA HA HA HA ha ha ha ...
		var accum = 0L;
		var start = rules.get("in");
		for (var s = 1; s <= 4000; s++) { // 4000 * 222 minutes aka abt 14814 hours
			for (var x = 1; x <= 4000; x++) { // every loop step takes abt 3 seconds, so about 222 minutes for the entire loop
				for (var m = 1; m <= 4000; m++) {
					for (var a = 1; a <= 4000; a++) {
						accum += start.apply(new Item(x, m, a, s), rules) > 0 ? 1 : 0;
					}
				}
				var elapsed = System.currentTimeMillis() - millis;
				log.info(String.format("%03d>  x=%04d  s=%04d%n", elapsed/1000, x, s));
			}
		}
		return accum;
	}

	public long doPart2() {
		var accum = 0L;
		var start = new ItemLimits(1, 4000, "in");
		var queue = new ArrayList<ItemLimits>();
		var accepted = new ArrayList<ItemLimits>();
		queue.add(start);
		while (!queue.isEmpty()) {
			var current = queue.remove(0);
			for (var result : current.applyFilters(justTheFilters.get(current.nextRule))) {
				if ("A".equals(result.nextRule)) {
					accepted.add(result);
				} else if (!"R".equals(result.nextRule)) {
					queue.add(result);
				}
			}
		}
		for (var itl : accepted) {
			accum += itl.count();
		}
		return accum;
	}

	public static void main(String[] args) {
		var ex = new Exercise19(aocPath(2023, 19));

		// Process input (0.011s)
		var start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 386787 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 131029523269531 (0.012s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
