package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 5, title = "If You Give A Seed A Fertilizer", keywords = {"mapping", "intersection", "optimization"})
public class Exercise05 {

	private static final String SEED_TO_SOIL = "seed-to-soil map:";
	private static final String MAPPING_SECTIONS = "@soil-to-fertilizer map:@fertilizer-to-water map:@water-to-light map:@light-to-temperature map:@temperature-to-humidity map:@humidity-to-location map:@";
	private static final String SEED_LABEL = "seeds: ";

	private final List<String> lines;

	private List<Long[]> mappedValues;
	private List<Long[]> sourceValues;

	/*
	 * let's think this through. We start with ranges (for part 1 every range is 1 long and stays that way).
	 * Every time we map, the range can partially overlap one or more map ranges, this means the range will split into multiple parts (only for part 2)
	 * Eventually we end up with a set of ranges that maps to locations. If we immediately convert to destination values, all we need to remember are start and end values of each range
	 */
	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	long doCalculation(boolean part1) {
		List<Long[]> seeds = new ArrayList<>();
		boolean readingSeeds = true;
		for (String line : lines) {
			if ("".equals(line)) {
				continue;
			}
			// handle section starts:
			if (SEED_TO_SOIL.equals(line)) {
				sourceValues = new ArrayList<>();
				sourceValues.addAll(seeds);
				mappedValues = new ArrayList<>();
				log.debug("Starting values for " + line + " " + stringify(sourceValues));
				readingSeeds = false;
				continue;
			}
			if (MAPPING_SECTIONS.contains("@"+line+"@")) {
				sourceValues.addAll(mappedValues);
				log.debug("Starting values for " + line + " " + stringify(sourceValues));
				mappedValues = new ArrayList<>();
				readingSeeds = false;
				continue;
			}
			// handle lines with actual data:
			if (readingSeeds) {
				if (line.startsWith(SEED_LABEL)) {
					seeds.addAll(parseSeedRanges(line, part1));
				}
			} else {
				// mapping algorithm
				// within the same section we stepwise convert (some of ) the ranges in sourceValues to new ranges in mappedValues;
				// any unmapped ranges will remain in sourceValues - they represent ranges that are mapped to the same value
				// and will simply be combined with mappedValues when the next section starts
				String[] vals = line.split(" +");
				applyMapping(Long.parseLong(vals[0]), Long.parseLong(vals[1]), Long.parseLong(vals[2]));
			}
		}
		// simply add everything that has remained unmapped:
		mappedValues.addAll(sourceValues);
		log.debug("Final ranges: " + stringify(mappedValues));
		long lowest = Long.MAX_VALUE;
		for (Long[] location : mappedValues) {
			lowest = Math.min(lowest, location[0]);
		}
		return lowest;
	}

	private List<Long[]> parseSeedRanges(String line, boolean part1) {
		List<Long[]> result = new ArrayList<>();
		if (part1) {
			for (String seed : line.substring(SEED_LABEL.length()).split(" +")) {
				Long[] range = new Long[2];
				range[0] = Long.parseLong(seed);
				range[1] = Long.parseLong(seed);
				result.add(range);
			}
		} else {
			String[] nums = line.substring(7).split(" +");
			for (int i = 0; i < nums.length/2; i++) {
				Long[] range = new Long[2];
				range[0] = Long.parseLong(nums[i*2]);
				range[1] = range[0] + Long.parseLong(nums[i*2+1]);
				result.add(range);
			}
		}
		return result;
	}

	private void applyMapping(long destStart, long srcStart, long length) {
		long shift = destStart - srcStart;
		log.debug("Mapping instruction: [" + srcStart + "," + (srcStart + length - 1) + "] -> [" + destStart + "," + (destStart + length - 1) + "]");
		List<Long[]> sourceValuesCopy = new ArrayList<>(); // to avoid concurrent modification
		Iterator<Long[]> iter = sourceValues.iterator();
		while (iter.hasNext()) {
			Long[] rangeToMap = iter.next();
			if (!(srcStart > rangeToMap[1] || srcStart + length -1 < rangeToMap[0])) { // overlap so some mapping needed
				log.debug("Overlap found for [" + rangeToMap[0] + "," + rangeToMap[1] + "]");
				// the overlapping ranges will essentially split into (at most) three new ranges, the middle of which
				// will always be the overlap
				iter.remove(); // this source range will be replaced at end of loop with its unmapped sections
				long leftMost = Math.min(srcStart, rangeToMap[0]);
				long second = Math.max(srcStart, rangeToMap[0]);
				long third = Math.min(srcStart + length -1, rangeToMap[1]);
				long rightMost = Math.max(srcStart + length -1, rangeToMap[1]);
				// always map middle range:
				Long[] newRange = new Long[2];
				newRange[0] = second + shift;
				newRange[1] = third + shift;
				log.debug("Mapping [" + second + "," + third + "] to [" + newRange[0] + "," + newRange[1] + "]");
				mappedValues.add(newRange);
				if (srcStart > rangeToMap[0]) { //  also return copy of leftmost range to source list:
					newRange = new Long[2];
					newRange[0] = leftMost;
					newRange[1] = second - 1;
					log.debug("Not mapping [" + newRange[0] + "," + newRange[1] + "]");
					sourceValuesCopy.add(newRange);
				}
				if (rangeToMap[1] > srcStart + length -1) { // also return copy of rightmost range to source list
					newRange = new Long[2];
					newRange[0] = third + 1;
					newRange[1] = rightMost;
					log.debug("Not mapping [" + newRange[0] + "," + newRange[1] + "]");
					sourceValuesCopy.add(newRange);
				}
			}
		}
		sourceValues.addAll(sourceValuesCopy);
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2023, 5));

		// No separate processInput this time because the calculation is done in place

		// part 1 - 165788812 (0.013s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doCalculation(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1928058 (0.007s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doCalculation(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}

	private String stringify(List<Long[]> input) {
		return "{" + input.stream().map(a -> "[" + a[0] + "," + a[1] + "]").collect(Collectors.joining(", ")) + "}";
	}
}
