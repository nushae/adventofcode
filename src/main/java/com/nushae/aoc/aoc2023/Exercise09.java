package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2023, day = 9, title = "Mirage Maintenance", keywords = {"sequences", "extrapolation"})
public class Exercise09 {

	private List<String> lines;
	private List<List<Long>> sequences;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		sequences = new ArrayList<>();
		for (String line : lines) {
			List<Long> nums = new ArrayList<>();
			for (String num : line.split(" +")) {
				nums.add(Long.parseLong(num));
			}
			sequences.add(nums);
		}
	}

	public long doPart1() {
		return calc(false);
	}

	public long doPart2() {
		return calc(true);
	}

	public long calc(boolean part2) {
		long accum = 0;
		for (List<Long> seq : sequences) {
			if (part2) Collections.reverse(seq);
			long nextVal = seq.get(seq.size()-1) + predict(seq);
			// for idempotence uncomment the following line
			// if (part2) Collections.reverse(seq);
			accum += nextVal;
		}
		return accum;
	}

	private long predict(List<Long> sequence) {
		long AllZeroes = 0;
		List<Long> diffs = new ArrayList<>();
		for (int i = 1; i < sequence.size(); i++) {
			long diff = sequence.get(i) - sequence.get(i-1);
			AllZeroes += diff;
			diffs.add(diff);
		}
		log.debug(diffs);
		if (AllZeroes != 0) {
			log.debug("(recurse)");
			long newVal = predict(diffs) + diffs.get(diffs.size()-1);
			log.debug("Returning " + newVal);
			return newVal;
		}
		log.debug("Returning 0");
		return 0;
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2023, 9));

		// Process input (0.01s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 2098530125 (0.004s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1016 (0.004s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
