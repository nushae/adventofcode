package com.nushae.aoc.aoc2023;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.manhattanDistance;

@Log4j2
@Aoc(year = 2023, day = 11, title = "Cosmic Expansion", keywords = {"grid", "manhattan", "expansion-factor"})
public class Exercise11 {

	private List<String> lines;
	private List<Pair<Long, Long>> galaxies;

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput(long expansionFactor) {
		expansionFactor--;
		galaxies = new ArrayList<>();
		int[] cols = new int[lines.get(0).length()];
		int[] rows = new int[lines.size()];
		List<Pair<Integer, Integer>> originals = new ArrayList<>();
		for (int y = 0; y < lines.size(); y++) {
			String line = lines.get(y);
			for (int x = 0; x < line.length(); x++) {
				int gal = line.charAt(x) == '#' ? 1 : 0;
				originals.add(new Pair<>(x, y));
				cols[x] += gal;
				rows[y] += gal;
			}
			y++;
		}
		int emptyCols = 0;
		for (int x = 0; x < cols.length; x++) {
			if (cols[x] == 0 ) {
				emptyCols++;
			}
			cols[x] = emptyCols;
		}
		log.debug("Empty cols: " + emptyCols + " - " + Arrays.toString(cols));
		int emptyRows = 0;
		for (int y = 0; y < cols.length; y++) {
			if (rows[y] == 0) {
				emptyRows++;
			}
			rows[y] = emptyRows;
		}
		log.debug("Empty rows: " + emptyRows + " - " + Arrays.toString(rows));
		for (Pair<Integer, Integer> orig : originals) {
			int x = orig.getLeft();
			int y = orig.getRight();
			galaxies.add(new Pair<>(x + expansionFactor * cols[x], y + expansionFactor * rows[y]));
		}
	}

	public long calcDistances(long expansionFactor) {
		long accum = 0;
		processInput(expansionFactor);
		for (int i = 0; i < galaxies.size()-1; i++) {
			for (int j = i+1; j < galaxies.size(); j++) {
				accum += manhattanDistance(galaxies.get(i).getLeft(), galaxies.get(i).getRight(), galaxies.get(j).getLeft(), galaxies.get(j).getRight());
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2023, 11));

		// part 1
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.calcDistances(2));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.calcDistances(1000000L));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
