package com.nushae.aoc.aoc2023;

import com.nushae.aoc.aoc2017.Exercise13;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2023, day = 10, title = "Pipe Maze", keywords = {"grid", "graph", "pathing", "edge-detect"})
public class Exercise10 extends JPanel {

	private static final boolean VISUAL = true; // -> for pixels O_O !!
	private static final Color BG_COLOR_LOOP = new Color(227, 227, 255);
	private static final Color LINE_COLOR_LOOP = Color.blue;
	private static final Color FILL_COLOR_LOOP = Color.white;
	private static final Color BG_COLOR_INSIDE = new Color(255, 227, 227);
	private static final Color LINE_COLOR_INSIDE = Color.red;
	private static final Color FILL_COLOR_INSIDE = Color.white;
	private static final Color BG_COLOR_OUTSIDE = Color.lightGray;
	private static final Color LINE_COLOR_OUTSIDE = Color.black;
	private static final Color FILL_COLOR_OUTSIDE = Color.white;

	private static final int TUBE_WIDTH = 2;
	private static final int TUBE_MARGIN = 2;
	private static final int TILE_SIZE = TUBE_WIDTH + 2  + 2 * TUBE_MARGIN;

	private static final Map<Point, Character> CORNERS = Map.of(
		NORTHEAST, 'L',
		SOUTHEAST, 'F',
		SOUTHWEST, '7',
		NORTHWEST, 'S'
	);

	private List<String> lines;
	private Map<Point, Character> pipes;
	private Point[] s1;
	private Point[] s2;
	// for part 2:
	private List<String> processedLines;
	private List<Point> loopPoints;
	private List<Point> insiders;
	private Character actualS;
	private Map<Character, Image> tiles;
	private boolean ready;
	private boolean evenOddReady;

	public Exercise10() {
		lines = new ArrayList<>();
		tiles = new HashMap<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		pipes = new HashMap<>();
		loopPoints = new ArrayList<>();
		insiders = new ArrayList<>();
		Point start = new Point(0,0);
		int y = 0;
		for (String line : lines) {
			for (int x = 0; x < line.length(); x++) {
				Point loc = new Point(x,y);
				char s = line.charAt(x);
				if ("S-|7FJL".contains(""+s)) {
					pipes.put(loc, s);
					if (s == 'S') {
						start = loc;
					}
				}
			}
			y++;
		}
		loopPoints.add(start);
		for (Point dir : ORTHO_DIRS) {
			Point p = sum(start, dir);
			if (pipes.containsKey(p)) {
				char symbol = pipes.get(p);
				log.debug(start + " + " + dir + " = " + p + " " + symbol);
				if (connects(dir, symbol)) {
					if (s1 == null) {
						log.debug("Filling s1");
						s1 = new Point[] {dir, p};
					} else if (s2 == null) {
						log.debug("Filling s2");
						s2 = new Point[] {dir, p};
						break;
					}
				}
			}
		}
		actualS = CORNERS.get(sum(s1[0], s2[0]));
		if (VISUAL) {
			ready = true;
			repaint();
		}	}

	private boolean connects(Point dir, char dest) {
		if (dir.equals(NORTH)) {
			return "|F7".contains(""+dest);
		} else if (dir.equals(EAST)) {
			return "-7J".contains(""+dest);
		} else if (dir.equals(SOUTH)) {
			return "|JL".contains(""+dest);
		}
		return "-FL".contains(""+dest);
	}

	public long doPart1() {
		// s1 and s2 (dir, loc) are 1 away from S, so initialize steps at 1
		// The loop always contains an even number of nodes (since every left must be balanced by a right, and every up by a down)
		// so we can follow the loop in both directions and where the two meet is the furthest point.
		// Added in during part 2: on the fly we build a collection of all the points on the loop
		int steps = 1;
		while (!s1[1].equals(s2[1])) {
			loopPoints.add(s1[1]);
			loopPoints.add(s2[1]);
			log.debug(s1[0] + "/" + s1[1] + pipes.get(s1[1]) + " vs " + s2[0] + "/" + s2[1] + pipes.get(s2[1]));
			s1 = nextStep(s1);
			s2 = nextStep(s2);
			steps++;
		}
		loopPoints.add(s1[1]);
		return steps;
	}

	private Point[] nextStep(Point[] current) {
		Point ed = exitDir(current[0], pipes.get(current[1]));
		return new Point[] {ed, sum(current[1], ed)};
	}

	private Point exitDir(Point lastStep, Character mySymbol) {
		switch (mySymbol) {
		case '|': return lastStep == NORTH ? NORTH : SOUTH;
		case '-': return lastStep == EAST ? EAST : WEST;
		case '7': return lastStep == NORTH ? WEST : SOUTH;
		case 'J': return lastStep == EAST ? NORTH : WEST;
		case 'L': return lastStep == WEST ? NORTH : EAST;
		case 'F': return lastStep == NORTH ? EAST : SOUTH;
		}
		throw new IllegalArgumentException("Can't determine next step when coming from " + lastStep + " and symbol '" + mySymbol + "'");
	}

	public long doPart2() {
		// -> DEPENDS ON PART 1 for loopPoints to be filled
		processedLines = keepOnlyLoopChars();
		long accum = 0;
		for (int y = 0; y < lines.size(); y++) {
			for (int x = 0; x < lines.get(0).length(); x++) {
				Point p = new Point(x, y);
				if (!loopPoints.contains(p) && isInsideLoop(p)) {
					insiders.add(p);
					accum++;
				}
			}
		}
		evenOddReady = true;
		repaint();
		return accum;
	}

	private List<String> keepOnlyLoopChars() {
		List<String> result = new ArrayList<>();
		for (int y = 0; y < lines.size(); y++) {
			StringBuilder filteredLine = new StringBuilder();
			for (int x = 0; x < lines.get(0).length(); x++) {
				Point p = new Point(x, y);
				filteredLine.append(!loopPoints.contains(p) ? "." : (pipes.get(p) == 'S' ? actualS : pipes.get(p)));
			}
			result.add(filteredLine.toString());
		}
		return result;
	}

	/**
	 * Loop test (even/odd rule): draw a line from a point to 'outside the curve'.
	 * Odd number of crossings with curve == inside curve. This is very suitable here, because a crossing
	 * is simply one of the symbols in our input, an easy test (there are caveats, see below)
	 *
	 * We need to avoid lines that don't actually cross the curve but merely touch it. Since we draw horizontal
	 * rightward lines here, any horizontal part of the curve could potentially be such a 'touch' situation.
	 * These are all the cases:
	 * -> every | always counts as a crossing
	 * -> Every F{-}J counts as a crossing
	 * -> Every L{-}7 counts as a crossing
	 * -> Every L{-}J does NOT
	 * -> Every F{-}7 does NOT
	 * Note we can't encounter, eg. "---7" without a preceding 'F' or 'L' since we're dealing with a loop.
	 *
	 * Using the above we can use a String based solution which is pretty fast (especially compared to the diagonal
	 * version below):
	 * (1) We need a preprocessed the map where all symbols not on the loop are replaced with '.' and S is replaced
	 *     with its actual symbol (done in the calling code)
	 * (2) Then, for point x, y, we take the part of the line that starts just to the right of this point, and in it:
	 * (3) Delete all sequences representing 'non-crossings' (so '.' and L{-}J and F{-}7)
	 * (4) Replace by one character '#' all sequences representing 'crossings' (so '|' and F{-}J and L{-}7)
	 * (5) The length of the result is then the number of crossings.
	 */
	private boolean isInsideLoop(Point p) {
		String test = processedLines.get(p.y).substring(p.x+1);
		test = test.replaceAll("F-*J", "#")
				.replaceAll("F-*7", "")
				.replaceAll("L-*7", "#")
				.replaceAll("L-*J", "")
				.replaceAll("\\.", "");
		return test.length() % 2 == 1;
	}


	/**
	 * Loop test (even/odd rule): draw a line from a point to 'outside the curve'.
	 * Odd number of crossings with curve == inside curve. This is very suitable here, because a crossing
	 * is simply one of the symbols in our input, an easy test (there are caveats, see below)
	 *
	 * We need to avoid lines that don't actually cross the curve but merely touch it. Here we draw lines in the
	 * down/rightward direction so the only 'touch' points are the L and the 7 (possibly the S!), which are skipped.
	 * The map needs no special processing since we only count points on the curve anyway.
	 * However, it is VERY slow compared to the horizontal version above...
	 */
	private boolean isInsideLoopDiagonalVersion(Point p) {
		int crossings = 0;
		for (int d = 1; p.x+d <= lines.get(0).length() || p.y+d <= lines.size(); d++) {
			Point q = new Point(p.x + d, p.y + d);
			if (loopPoints.contains(q) && pipes.get(q) != '7' && pipes.get(q) != 'L' && (pipes.get(q) != 'S' || actualS != '7' && actualS != 'L')) {
				crossings++;
			}
		}
		return crossings % 2 == 1;
	}

	@Override
	public void paintComponent(Graphics g) {
		if (ready) {
			int xOffset = (getWidth() - lines.get(0).length()*TILE_SIZE)/2;
			int yOffset = (getHeight() - lines.size()*TILE_SIZE)/2;
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.white);
			g2.fillRect(0, 0, getWidth(), getHeight());
			for (int x = 0; x < lines.get(0).length(); x++) {
				for (int y=0; y < lines.size(); y++) {
					g2.drawImage(getTile(pipes.getOrDefault(new Point(x, y), '.'), loopPoints.contains(new Point(x, y)), insiders.contains(new Point(x, y))), xOffset + TILE_SIZE*x, yOffset + TILE_SIZE*y, null);
				}
			}
		}
	}

	private Image getTile(char symbol, boolean onLoop, boolean insideLoop) {
		if (onLoop) {
			symbol = (char) (symbol + 32);
		}
		if (evenOddReady && insideLoop) {
			symbol = (char) (symbol + 64);
		}

		if (tiles.get(symbol) == null) {
			Image tile = createImage(TILE_SIZE, TILE_SIZE);
			Graphics gr = tile.getGraphics();
			Color tileBG = onLoop ? BG_COLOR_LOOP : insideLoop ? BG_COLOR_INSIDE : BG_COLOR_OUTSIDE;
			Color lineColor = onLoop ? LINE_COLOR_LOOP : insideLoop ? LINE_COLOR_INSIDE : LINE_COLOR_OUTSIDE;
			Color fillColor = onLoop ? FILL_COLOR_LOOP : insideLoop ? FILL_COLOR_INSIDE : FILL_COLOR_OUTSIDE;
			gr.setColor(tileBG);
			gr.fillRect( 0, 0, TILE_SIZE, TILE_SIZE);
			gr.setColor(lineColor);
			switch (symbol) {
				case '|':
				case '\u009C':
				case '¼':
					gr.drawLine(TUBE_MARGIN,0, TUBE_MARGIN, TILE_SIZE-1);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH,0, TUBE_MARGIN + TUBE_WIDTH, TILE_SIZE-1);
					gr.setColor(fillColor);
					gr.fillRect(TUBE_MARGIN + 1, 0, TUBE_WIDTH - 1, TILE_SIZE);
					break;
				case '-':
				case 'M':
				case 'm':
					gr.drawLine(0, TUBE_MARGIN,TILE_SIZE - 1, TUBE_MARGIN);
					gr.drawLine(0, TUBE_MARGIN + TUBE_WIDTH, TILE_SIZE - 1, TUBE_MARGIN + TUBE_WIDTH);
					gr.setColor(fillColor);
					gr.fillRect(0, TUBE_MARGIN + 1, TILE_SIZE, TUBE_WIDTH - 1);
					break;
				case 'F':
				case 'f':
				case '\u0086':
					gr.drawLine(TUBE_MARGIN, TILE_SIZE - 1, TUBE_MARGIN, TUBE_MARGIN);
					gr.drawLine(TUBE_MARGIN, TUBE_MARGIN, TILE_SIZE-1, TUBE_MARGIN);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, TILE_SIZE - 1, TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN + TUBE_WIDTH);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN + TUBE_WIDTH, TILE_SIZE - 1, TUBE_MARGIN + TUBE_WIDTH);
					gr.setColor(fillColor);
					gr.fillRect(TUBE_MARGIN + 1, TUBE_MARGIN + 1, TUBE_WIDTH - 1, TILE_SIZE);
					gr.fillRect(TUBE_MARGIN + 1, TUBE_MARGIN + 1, TILE_SIZE, TUBE_WIDTH - 1);
					break;
				case 'J':
				case 'j':
				case '\u008A':
					gr.drawLine(TUBE_MARGIN, 0, TUBE_MARGIN, TUBE_MARGIN);
					gr.drawLine(TUBE_MARGIN, TUBE_MARGIN, 0, TUBE_MARGIN);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, 0, TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN + TUBE_WIDTH);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN + TUBE_WIDTH, 0, TUBE_MARGIN + TUBE_WIDTH);
					gr.setColor(fillColor);
					gr.fillRect(TUBE_MARGIN + 1, 0, TUBE_WIDTH - 1, TUBE_MARGIN + TUBE_WIDTH);
					gr.fillRect(0, TUBE_MARGIN + 1, TUBE_MARGIN + TUBE_WIDTH, TUBE_WIDTH - 1);
					break;
				case '7':
				case 'W':
				case 'w':
					gr.drawLine(TUBE_MARGIN, TILE_SIZE - 1, TUBE_MARGIN, TUBE_MARGIN + TUBE_WIDTH);
					gr.drawLine(TUBE_MARGIN, TUBE_MARGIN + TUBE_WIDTH, 0, TUBE_MARGIN + TUBE_WIDTH);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, TILE_SIZE - 1, TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN, 0, TUBE_MARGIN);
					gr.setColor(fillColor);
					gr.fillRect(TUBE_MARGIN + 1, TUBE_MARGIN +1, TUBE_WIDTH - 1, TILE_SIZE);
					gr.fillRect(0, TUBE_MARGIN + 1, TUBE_MARGIN + TUBE_WIDTH, TUBE_WIDTH - 1);
					break;
				case 'L':
				case 'l':
				case '\u008C':
					gr.drawLine(TUBE_MARGIN, 0, TUBE_MARGIN, TUBE_MARGIN + TUBE_WIDTH);
					gr.drawLine(TUBE_MARGIN, TUBE_MARGIN + TUBE_WIDTH, TILE_SIZE-1, TUBE_MARGIN + TUBE_WIDTH);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, 0, TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN);
					gr.drawLine(TUBE_MARGIN + TUBE_WIDTH, TUBE_MARGIN, TILE_SIZE - 1, TUBE_MARGIN);
					gr.setColor(fillColor);
					gr.fillRect(TUBE_MARGIN + 1, 0, TUBE_WIDTH - 1, TUBE_MARGIN + TUBE_WIDTH);
					gr.fillRect(TUBE_MARGIN + 1, TUBE_MARGIN + 1, TILE_SIZE, TUBE_WIDTH - 1);
					break;
				case '.':
				case 'N':
				case 'n':
					// do nothing - empty space
			}
			tiles.put(symbol, tile);
		}
		return tiles.get(symbol);
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2023, 10));

		if (VISUAL) {
			SwingUtilities.invokeLater(() -> Exercise13.createAndShowGUI(ex));
		}

		// Process input
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 6773 (0.016s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 493 (13.341s with diagonal version; 0.733s with pre-processing/horizontal version)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
