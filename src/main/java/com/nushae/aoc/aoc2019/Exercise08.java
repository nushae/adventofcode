package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.OCRUtil.crt4x6AsString;

@Log4j2
@Aoc(year = 2019, day = 8, title="Space Image Format", keywords = {"layer-flattening", "bigtext"})
public class Exercise08 {

	ArrayList<String> lines;
	ArrayList<Integer> inputValues;

	public Exercise08() {
		inputValues = new ArrayList<>();
		lines = new ArrayList<>();
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	private void processInput() {
		lines.forEach(this::addInput);
	}

	private void addInput(String s) {
		for (String c : s.split("")) {
			inputValues.add(Integer.parseInt(c));
		}
	}

	public int doPart1(int cols, int rows) {
		// interpret data as a series of layers that are cols wide and rows high
		int idx = 0;
		int result = -1;
		int minZeroes = Integer.MAX_VALUE;
		int[] digiCount = new int[10];
		for (int pixel : inputValues) {
			idx++;
			digiCount[pixel]++;
			if (idx % (cols * rows) == 0) {
				if (minZeroes > digiCount[0]) {
					minZeroes = digiCount[0];
					result = digiCount[1]*digiCount[2];
				}
				digiCount = new int[10];
				idx = 0;
			}
		}
		return result;
	}

	public String doPart2(int cols, int rows) {
		boolean[][] image = new boolean[rows][cols];
		int layerSize = rows * cols;
		int layerCount = inputValues.size() / layerSize;
		for (int pixel = 0; pixel < layerSize; pixel++) {
			for (int layer = 0 ; layer < layerCount ; layer++) {
				int pixelVal = inputValues.get(layerSize * layer + pixel);
				if (pixelVal != 2 || layer == layerCount - 1) {
					image[pixel / cols][pixel % cols] = (pixelVal == 1);
					break;
				}
			}
		}
		return crt4x6AsString(image);
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2019, 8));

		// Process input (16ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1572 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(25, 6));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - KYHFE (<1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(25, 6));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}