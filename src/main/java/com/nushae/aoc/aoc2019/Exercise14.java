package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.Reaction;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 14, title="Space Stoichiometry", keywords = {"reactions", "binary-search"})
public class Exercise14 {

	public static final BigInteger TRILLION = BigInteger.valueOf(1_000_000_000_000L);

	ArrayList<String> lines;
	HashMap<String, Reaction> reactions;
	HashMap<String, BigInteger> leftovers;
	BigInteger totalOreUsed;

	public Exercise14() {
		lines = new ArrayList<>();
		reactions = new HashMap<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		for (String line : lines) {
			String[] parts = line.split(" => ");
			String[] outputVals = parts[1].split(" ");
			String[] inputs = parts[0].split(", ");
			HashMap<String, Integer> reactants = new HashMap<>();
			for (String input: inputs) {
				String[] inputVals = input.split(" ");
				reactants.put(inputVals[1], Integer.parseInt(inputVals[0]));
			}
			reactions.put(outputVals[1], new Reaction(Integer.parseInt(outputVals[0]), reactants));
		}
	}

	public BigInteger doPart1() {
		leftovers = new HashMap<>();
		totalOreUsed = BigInteger.ZERO;
		resolveReaction(BigInteger.ONE, "FUEL", 0);
		return totalOreUsed;
	}

	public void resolveReaction(BigInteger required, String substance, int depth) {
		BigInteger amount = BigInteger.ZERO;
		BigInteger factor = BigInteger.ONE;
		BigInteger leftover = leftovers.getOrDefault(substance, BigInteger.ZERO);
		if (leftover.compareTo(required) > 0) {
			amount = required;
			leftovers.put(substance, leftover.subtract(required));
		} else if (leftover.compareTo(BigInteger.ZERO) > 0) {
			amount = leftover;
			leftovers.remove(substance);
		}
		if (required.compareTo(amount) > 0) {
			if ("ORE".equals(substance)) {
				totalOreUsed = totalOreUsed.add(required.subtract(amount));
				return;
			}
			Reaction reaction = reactions.get(substance);
			if (required.subtract(amount).compareTo(BigInteger.valueOf(reaction.outputAmount)) > 0) {
				BigInteger[] dr = required.subtract(amount).divideAndRemainder(BigInteger.valueOf(reaction.outputAmount));
				factor = dr[0];
				if (dr[1].compareTo(BigInteger.ZERO) > 0) {
					factor = factor.add(BigInteger.ONE);
				}
			}
			for (Map.Entry<String, Integer> reactant : reaction.inputs.entrySet()) {
				resolveReaction(factor.multiply(BigInteger.valueOf(reactant.getValue())), reactant.getKey(), depth+1);
			}
			amount = amount.add(factor.multiply(BigInteger.valueOf(reaction.outputAmount)));
		}
		if (amount.compareTo(required) > 0) {
			leftovers.put(substance, leftovers.getOrDefault(substance, BigInteger.ZERO).add(amount).subtract(required));
		}
	}

	public void showLeftovers() {
		for (String substance: leftovers.keySet()) {
			System.out.println(substance + ": " + leftovers.get(substance));
		}
	}

	public BigInteger doPart2() {
		BigInteger lowerBound = BigInteger.ONE;
		BigInteger upperBound = BigInteger.ONE;
		totalOreUsed = BigInteger.ZERO;
		// find upper bound
		while (totalOreUsed.compareTo(TRILLION) < 0) {
			upperBound = upperBound.multiply(BigInteger.valueOf(2));
			totalOreUsed = BigInteger.ZERO;
			leftovers = new HashMap<>();
			resolveReaction(upperBound, "FUEL", 0);
		}
		// binary search
		while (lowerBound.add(BigInteger.ONE).compareTo(upperBound) < 0) {
			BigInteger fuelBatch = lowerBound.add(upperBound.subtract(lowerBound).divide(BigInteger.valueOf(2)));
			totalOreUsed = BigInteger.ZERO;
			leftovers = new HashMap<>();
			resolveReaction(fuelBatch, "FUEL", 0);
			if (totalOreUsed.compareTo(TRILLION) > 0) {
				upperBound = fuelBatch;
			} else {
				lowerBound = fuelBatch;
			}
		}
		return lowerBound;
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2019, 14));

		// Process input (1ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 248794 (6ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4906796 (72ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
