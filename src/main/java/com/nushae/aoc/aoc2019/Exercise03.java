package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.ListModel;
import com.nushae.aoc.aoc2019.domain.Segment;
import com.nushae.aoc.aoc2019.domain.Wire;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.view.ElementDisplayPanel;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 3, title="1202 Program Alarm", keywords = {"grid", "pathing", "visualized"})
public class Exercise03 {

	private ListModel<Wire> wires;
	private ArrayList<String> lines;
	private int minX;
	private int maxX;
	private int minY;
	private int maxY;
	private ElementDisplayPanel<Wire> displayPanel;

	public Exercise03() {
		wires = new ListModel<>();
		lines = new ArrayList<>();
		displayPanel = new ElementDisplayPanel<>();
		displayPanel.setModel(wires);
		minX = Integer.MAX_VALUE;
		maxX = Integer.MIN_VALUE;
		minY = Integer.MAX_VALUE;
		maxY = Integer.MIN_VALUE;
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		lines.forEach(this::addWire);
		displayPanel.setGridDimension(new Rectangle(minX, minY, maxX - minX, maxY - minY));
	}

	public void addWire(String line) {
		Wire w = new Wire();
		int curX = 0;
		int curY = 0;
		for (String word : line.split(",")) {
			int dx = 0, dy = 0;
			int val = Integer.parseInt(word.substring(1));
			switch (word.substring(0, 1)) {
				case "U" -> dy = val;
				case "D" -> dy = -val;
				case "R" -> dx = val;
				case "L" -> dx = -val;
				default -> log.warn("Unknown command: " + word);
			}
			if (Math.min(curX, curX + dx) < minX) {
				minX = Math.min(curX, curX + dx);
			}
			if (Math.min(curY, curY + dy) < minY) {
				minY = Math.min(curY, curY + dy);
			}
			if (Math.max(curX, curX + dx) > maxX) {
				maxX = Math.max(curX, curX + dx);
			}
			if (Math.max(curY, curY + dy) > maxY) {
				maxY = Math.max(curY, curY + dy);
			}
			w.addSegment(new Segment(new Point(curX, curY), new Point(curX + dx, curY + dy)));
			curX += dx;
			curY += dy;
		}
		wires.add(w);
	}

	public int doPart1() {
		Wire wire1 = wires.getElements().get(0);
		Wire wire2 = wires.getElements().get(1);
		// ignore any extra wires, we just need 2
		return wire1.findManhattanIntersectionsWith(wire2);
	}

	public int doPart2() {
		Wire wire1 = wires.getElements().get(0);
		Wire wire2 = wires.getElements().get(1);
		// ignore any extra wires, we just need 2
		return wire1.findStepsIntersectionsWith(wire2);
	}

	private static void createAndShowGUI(ElementDisplayPanel<Wire> displayPanel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(800, 600));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.add(displayPanel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2019, 3));

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex.displayPanel));

		// Process input (63ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 651 (481ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 7534 (12ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
