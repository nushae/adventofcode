package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.CyclicListInputProvider;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 9, title="Sensor Boost", keywords = {"intcode", "addressing"})
public class Exercise09 {

	IntCodeInterpreter interpreter;

	public Exercise09(Path fileName) {
		interpreter = new IntCodeInterpreter();
		interpreter.loadProgramFromFile(fileName);
	}

	public void execute(int input) {
		interpreter.setInputProvider(new CyclicListInputProvider(BigInteger.valueOf(input)));
		interpreter.reRunProgram();
	}

	public static void main(String[] args) {

		Exercise09 ex = new Exercise09(aocPath(2019, 9));

		// processInput not needed

		// part 1 - 2955820355 (15ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		ex.execute(1); // no OutputReceiver
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 46643 (1.427s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		ex.execute(2); // no OutputReceiver
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}