package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.IntCodeNetwork;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 23, title="Category Six", keywords = {"intcode", "network", "multiprogram", "synchronization"})
public class Exercise23 {

	public BigInteger runNetwork(Path path, boolean useNAT) {
		IntCodeNetwork network = new IntCodeNetwork(true);
		for (int i = 0; i < 50; i++) {
			network.attachComputer(BigInteger.valueOf(i), path);
		}
		return network.start(useNAT);
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23();

		// processInput not needed

		// part 1 - 23701 (27.76s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.runNetwork(aocPath(2019, 23), false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 17225 (40.709s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.runNetwork(aocPath(2019, 23), true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
