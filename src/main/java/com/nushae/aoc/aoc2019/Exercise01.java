package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 1, title="The Tyranny of the Rocket Equation", keywords = {"list-processing", "recursion"})
public class Exercise01 {

	private ArrayList<String> lines;
	private ArrayList<Long> numbers;

	private Exercise01() {
		lines = new ArrayList<>();
		numbers = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		lines.forEach(s -> numbers.add(Long.parseLong(s)));
	}

	public long doPart1() {
		long result = 0;
		for (Long num : numbers) {
			result += findFuel(num);
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		for (Long num : numbers) {
			result += findFuelFuel(num);
		}
		return result;
	}

	public static long findFuel(long weight) {
		return Math.max(0, weight / 3 - 2);
	}

	public static long findFuelFuel(long fuelWeight) {
		long fuelForFuel = findFuel(fuelWeight);
		if (fuelForFuel > 0) {
			fuelForFuel += findFuelFuel(fuelForFuel);
		}
		return fuelForFuel;
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2019, 1));

		// Process input (3ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 3349352 (<1ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 5021154 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
