package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.OutputReceiver;
import com.nushae.aoc.aoc2019.intcode.SingleInputOutputConnector;
import com.nushae.aoc.aoc2019.intcode.SingleOutputReceiver;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.function.Function;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 7, title="Amplification Circuit", keywords = {"intcode", "multiprogram"})
public class Exercise07 {

	private IntCodeInterpreter[] interpreters;
	private OutputReceiver outputReceiver;

	public Exercise07(Path path) {
		interpreters = new IntCodeInterpreter[5];
		for (int i=0; i<5; i++) {
			interpreters[i] = new IntCodeInterpreter();
			interpreters[i].loadProgramFromFile(path);
		}
	}

	public Exercise07(String data) {
		interpreters = new IntCodeInterpreter[5];
		for (int i=0; i<5; i++) {
			interpreters[i] = new IntCodeInterpreter();
			interpreters[i].loadProgramFromString(data);
		}
	}

	public BigInteger doPart1() {
		return permuteAndFindMax(this::findOutputForPhases, BigInteger.ZERO, BigInteger.ONE, BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(4));
	}

	public BigInteger doPart2() {
		return permuteAndFindMax(this::findOutputForPhasesFeedbackLoop, BigInteger.valueOf(5), BigInteger.valueOf(6), BigInteger.valueOf(7), BigInteger.valueOf(8), BigInteger.valueOf(9));
	}

	private BigInteger permuteAndFindMax(Function<BigInteger[], BigInteger> phaseFunction, BigInteger... elements) {
		int[] indexes = new int[elements.length];
		BigInteger accumulator = phaseFunction.apply(elements);
		int i = 0;
		while (i < indexes.length) {
			if (indexes[i] < i) {
				int j = i % 2 == 0 ?  0 : indexes[i];
				BigInteger tmp = elements[i];
				elements[i] = elements[j];
				elements[j] = tmp;
				accumulator = accumulator.max(phaseFunction.apply(elements));
				indexes[i]++;
				i = 0;
			}
			else {
				indexes[i] = 0;
				i++;
			}
		}
		return accumulator;
	}

	public BigInteger findOutputForPhases(BigInteger... phases) {
		if (phases.length < interpreters.length) {
			throw new IllegalArgumentException("Too few phases provided for program suite");
		}
		// init interpreters and connectors
		for (int i = 0; i < interpreters.length; i++) {
			interpreters[i].resetState();
			SingleInputOutputConnector conn = new SingleInputOutputConnector(phases[i]);
			interpreters[i].setInputProvider(conn);
			if (i > 0) {
				interpreters[i - 1].setOutputReceiver(conn);
			}
			if (i == interpreters.length - 1) {
				outputReceiver = new SingleOutputReceiver();
				interpreters[i].setOutputReceiver(outputReceiver);
			}
		}
		for (IntCodeInterpreter interpreter : interpreters) {
			interpreter.reRunProgram();
		}
		return outputReceiver.getLastOutput();
	}

	public BigInteger findOutputForPhasesFeedbackLoop(BigInteger... phases) {
		if (phases.length < interpreters.length) {
			throw new IllegalArgumentException("Too few phases provided for program suite");
		}
		// init programs and connectors
		for (int i = 0; i < interpreters.length; i++) {
			interpreters[i].resetState();
			SingleInputOutputConnector conn = new SingleInputOutputConnector(phases[i]);
			interpreters[i].setInputProvider(conn);
			interpreters[(i + interpreters.length - 1) % interpreters.length].setOutputReceiver(conn);
			if (i == 0) {
				outputReceiver = conn;
			}
		}

		do {
			for (IntCodeInterpreter interpreter : interpreters) {
				interpreter.runProgramAndPauseOnIO(false, true);
			}
			log.debug("Output at end of loop: " + outputReceiver.getLastOutput());
		} while (!interpreters[interpreters.length-1].isHalted());
		return outputReceiver.getLastOutput();
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2019, 7));

		// processInput not needed

		// part 1 - 21860 (209ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2645740 (800ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
