package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.CyclicListInputProvider;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.Observer;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 17, title="Set and Forget", keywords = {"grid", "intcode", "maze", "intersection-count", "macro-definition"})
public class Exercise17 {

	private IntCodeInterpreter interpreter;
	private Observer observer;

	public Exercise17() {
		observer = new Observer();
		interpreter = new IntCodeInterpreter();
		interpreter.setOutputReceiver(observer);
	}

	public Exercise17(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise17(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public long doPart1() {
		interpreter.reRunProgram();
		System.out.println(observer.getWidth() + ", " + observer.getHeight());
		return observer.getAlignmentParameters();
	}

	// After running part 1, the route turns out to be:
	// R4R10R8R4R10R6R4R4R10R8R4R10R6R4R4L12R6L12R10R6R4R4L12R6L12R4R10R8R4R10R6R4R4L12R6L12
	// Split up:
	// R4R10R8R4 -> A
	// R10R6R4 -> B
	// R4R10R8R4 -> A
	// R10R6R4 -> B
	// R4L12R6L12 -> C
	// R10R6R4 -> B
	// R4L12R6L12 -> C
	// R4R10R8R4 -> A
	// R10R6R4 -> B
	// R4L12R6L12 -> C


	public BigInteger doPart2() {
		observer = new Observer();
		CyclicListInputProvider router = new CyclicListInputProvider("A,B,A,B,C,B,C,A,B,C\n" +
				"R,4,R,10,R,8,R,4\n" +
				"R,10,R,6,R,4\n" +
				"R,4,L,12,R,6,L,12\n" +
				"n\n"
		);
		interpreter.setOutputReceiver(observer);
		interpreter.setInputProvider(router);
		interpreter.resetState();
		interpreter.poke(BigInteger.ZERO, BigInteger.valueOf(2));
		interpreter.runProgram();
		return observer.getLastOutput();
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17(aocPath(2019, 17));

		// processInput not needed

		// part 1 - 5788 (239ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 648545 (362ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
