package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.TreeNode;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 6, title="Universal Orbit Map", keywords = {"tree", "common-ancestor"})
public class Exercise06 {
	ArrayList<String> lines;
	HashMap<String, TreeNode<Integer>> orbitMap;
	TreeNode<Integer> root;

	public Exercise06() {
		lines = new ArrayList<>();
		orbitMap = new HashMap<>();
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	private void processInput() {
		lines.forEach(this::addOrbit);
		for (TreeNode<Integer> t: orbitMap.values()) {
			if (t.getParentNode() == null) {
				root = t;
				return;
			}
		}
	}

	private void addOrbit(String s) {
		int pos = s.indexOf(")");
		String parent = s.substring(0, pos);
		String child = s.substring(pos + 1);
		TreeNode<Integer> parentNode = orbitMap.getOrDefault(parent, new TreeNode<>(parent, 0));
		TreeNode<Integer> childNode = orbitMap.getOrDefault(child, new TreeNode<>(child, 0));
		parentNode.addChild(childNode);
		childNode.setParent(parentNode);
		orbitMap.put(parent, parentNode);
		orbitMap.put(child, childNode);
		log.debug(parent + ")" + child);
	}

	public int doPart1() {
		return addDepthsAndSummarizeLeaves(root, 0);
	}

	private int addDepthsAndSummarizeLeaves(TreeNode<Integer> tn, int depth) {
		int result;
		tn.setDepth(depth);
		result = depth;
		if (tn.isInternal()) {
			for (TreeNode<Integer> child : tn.getChildren()) {
				result += addDepthsAndSummarizeLeaves(child, depth + 1);
			}
		}
		return result;
	}

	public int doPart2() {
		TreeNode<Integer> san = orbitMap.get("SAN");
		TreeNode<Integer> you = orbitMap.get("YOU");
		TreeNode<Integer> anc = findNearestCommonAncestor(san, you);

		return you.getDepth() + san.getDepth() - 2 * anc.getDepth() - 2;
	}

	private TreeNode<Integer> findNearestCommonAncestor(TreeNode<Integer> left, TreeNode<Integer> right) {
		if (left.getName().equals(right.getName())) {
			return left;
		}
		if (left.getDepth() == right.getDepth()) {
			return findNearestCommonAncestor(left.getParentNode(), right.getParentNode());
		} else if (left.getDepth() < right.getDepth()) {
			return findNearestCommonAncestor(left, right.getParentNode());
		}
		return findNearestCommonAncestor(left.getParentNode(), right);
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2019, 6));

		// Process input (10ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 145250 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 274 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
