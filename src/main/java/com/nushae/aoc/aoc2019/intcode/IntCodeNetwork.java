package com.nushae.aoc.aoc2019.intcode;

import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.GraphicsUtil.drawAlignedString;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static javax.swing.SwingConstants.WEST;

@Log4j2
public class IntCodeNetwork extends JPanel implements InputProvider, OutputReceiver {

	private static final int IDLE_THRESHOLD = 10;
	private Map<BigInteger, IntCodeInterpreter> attachedComputers;
	private Map<BigInteger, List<BigInteger>> queues;
	private Map<BigInteger, List<BigInteger>> partialOutputs;

	// Processes that read a -1 probably do some sort of busy waiting, like, constantly looping over the input
	// instruction until the received value != -1. This means it is impossible to have directly consecutive
	// "failed read instructions". So what we need to track is how many read instructions failed in a row
	// (ignoring other instructions in between)
	private Map<BigInteger, Integer> failedReads;

	// NAT state, at the special 255 port
	private boolean natActive;
	private boolean natHasPacket;
	private BigInteger[] NATState;
	private BigInteger lastSent;
	private boolean visualize;
	int idleProcesses;
	long cycle;

	public IntCodeNetwork(boolean visualize) {
		this.visualize = visualize;
		attachedComputers = new HashMap<>();
		queues = new HashMap<>();
		partialOutputs = new HashMap<>();
		failedReads = new HashMap<>();
		if (this.visualize) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(this));
		}
	}

	public BigInteger start(boolean useNAT) {
		natActive = useNAT;
		int idleCycles = 0;
		NATState = new BigInteger[2];
		natHasPacket = false;
		boolean natSentSameTwice = false;
		cycle = 0;
		do {
			idleProcesses = 0;
			for (Map.Entry<BigInteger, IntCodeInterpreter> ici : attachedComputers.entrySet()) {
				if (natActive) {
					if (failedReads.getOrDefault(ici.getKey(), 0) < IDLE_THRESHOLD || queues.getOrDefault(ici.getKey(), new ArrayList<>()).size() > 0) {
						ici.getValue().runProgramInterleaved(1);
					} else {
						idleProcesses++;
					}
				} else {
					ici.getValue().runProgramInterleaved(1);
				}
			}
			cycle++;
			repaint();
			// NAT idle check - all computers idle (sufficient consecutive failed reads) == idle network
			if (idleProcesses == attachedComputers.size()) {
				idleCycles++;
				log.debug("Cycle " + cycle + ": idle cycle #" + idleCycles + " (" + idleProcesses + ")");
				if (natActive && idleCycles >= IDLE_THRESHOLD && natHasPacket) {
					log.debug("Sending package [X=" + NATState[0] + "][Y=" + NATState[1] + "] to computer 0 for the " + (lastSent == null ? "1st" : NATState[1].equals(lastSent) ? "2nd" : "1st") + " time.");
					List<BigInteger> queueZero = queues.getOrDefault(BigInteger.ZERO, new ArrayList<>());
					queueZero.addAll(Arrays.asList(NATState[0], NATState[1]));
					queues.put(BigInteger.ZERO, queueZero);
					natSentSameTwice = NATState[1].equals(lastSent);
					lastSent = NATState[1];
					idleCycles = 0; // to prevent continuous sending once the network becomes idle
				}
			} else {
				idleCycles = 0;
			}
		} while ((natActive && !natSentSameTwice) || (!natActive && !natHasPacket));
		log.debug("Ended after " + cycle + " cycles.");
		return NATState[1];
	}

	public void attachComputer(BigInteger networkAddress, Path programLocation) {
		IntCodeInterpreter ici = new IntCodeInterpreter(networkAddress, this, this);
		ici.loadProgramFromFile(programLocation);
		// put network address in queue as first input:
		List<BigInteger> queue = new ArrayList<>();
		queue.add(networkAddress);
		queues.put(networkAddress, queue);
		attachedComputers.put(networkAddress, ici);
	}

	public void reportStates() {
		for (Map.Entry<BigInteger, IntCodeInterpreter> ici : attachedComputers.entrySet()) {
			System.out.printf("%02d  %s\n", ici.getKey(), queues.getOrDefault(ici.getKey(), new ArrayList<>()));
		}
		if (natActive) {
			System.out.println("Last NAT Packet received: [" + (natHasPacket ? NATState[0] + "][" + NATState[1] : "none") + "] -- Network is " + (idleProcesses == attachedComputers.size() ? "" : "not") + " idle");
		}
	}

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		List<BigInteger> partialOutput = partialOutputs.getOrDefault(interpreterNetworkAddress, new ArrayList<>());
		partialOutput.add(output);
		if (partialOutput.size() > 2) {
			if (partialOutput.get(0).equals(BigInteger.valueOf(255))) {
				NATState[0] = partialOutput.get(1);
				NATState[1] = partialOutput.get(2);
				natHasPacket = true;
				log.debug("Cycle " + cycle + ": #### Packet sent to 255 from " + interpreterNetworkAddress + ": [X=" + NATState[0] + "][Y=" + NATState[1] + "]");
			} else {
				List<BigInteger> queue = queues.getOrDefault(partialOutput.get(0), new ArrayList<>());
				queue.add(partialOutput.get(1));
				queue.add(partialOutput.get(2));
				queues.put(partialOutput.get(0), queue);
			}
			partialOutput = new ArrayList<>();
		}
		partialOutputs.put(interpreterNetworkAddress, partialOutput);
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		List<BigInteger> queue = queues.getOrDefault(interpreterNetworkAddress, new ArrayList<>());
		if (queue.size() > 0) {
			failedReads.put(interpreterNetworkAddress, 0);
			return queue.remove(0); // successful read from queue
		}
		queues.put(interpreterNetworkAddress, queue);
		failedReads.put(interpreterNetworkAddress, failedReads.getOrDefault(interpreterNetworkAddress, 0) + 1);
		return BigInteger.valueOf(-1); // the default behavior as defined in part 1
	}

	@Override
	public BigInteger getLastOutput() {
		return NATState[1];
	}

	@Override
	public void paintComponent(Graphics g) {
		Font font = new Font("Arial", Font.PLAIN, 20);
		Font font2 = new Font("Arial", Font.PLAIN, 14);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.white);
		g2.fillRect(0,0,1200,1200);
		for (int i = 0 ; i <= 50; i++) {
			g2.setColor(Color.gray);
			g2.drawLine(88,22 + i * 22, 1200, 22 + i * 22);
		}
		g2.drawLine(88, 22, 88, 1122);
		g2.drawLine(110, 22, 110, 1122);
		g2.drawLine(216, 22, 216, 1122);
		g2.setColor(Color.black);
		drawAlignedString(g2, "Failed reads", new Rectangle(110, 0, 300, 22), font, WEST);
		drawAlignedString(g2, "Queue contents", new Rectangle(220, 0, 300, 22), font, WEST);
		drawAlignedString(g2, "Cycle: " + cycle, new Rectangle(88, 1124, 300, 22), font, WEST);
		for (Map.Entry<BigInteger, IntCodeInterpreter> entry : attachedComputers.entrySet()) {
			g2.setColor(Color.black);
			drawCenteredString(g2, entry.getKey().toString(), new Rectangle(4*22, (entry.getKey().intValueExact() + 1) * 22, 22, 22), font);
			int fr = failedReads == null ? 0 : failedReads.get(entry.getKey());
			drawAlignedString(g2, "" + fr, new Rectangle(5*22, (entry.getKey().intValueExact() + 1) * 22, 22, 22), font, WEST);
			String queue = queues.get(entry.getKey()).stream().map(BigInteger::toString).collect(Collectors.joining(", "));
			g2.setColor(Color.blue);
			drawAlignedString(g2, queue, new Rectangle(9*24, (entry.getKey().intValueExact() + 1) * 22, 22, 22), font2, WEST);
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
