package com.nushae.aoc.aoc2019.intcode;

import java.math.BigInteger;

public interface InputProvider {
	BigInteger nextValue(BigInteger interpreterNetworkAddress);
}
