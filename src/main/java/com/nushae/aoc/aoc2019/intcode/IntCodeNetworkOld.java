package com.nushae.aoc.aoc2019.intcode;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntCodeNetworkOld implements InputProvider, OutputReceiver {

	private Map<BigInteger, IntCodeInterpreter> attachedComputers;
	private Map<BigInteger, List<BigInteger>> queues;
	private Map<BigInteger, List<BigInteger>> partialOutputs;
	private BigInteger lastOutput;

	public IntCodeNetworkOld() {
		attachedComputers = new HashMap<>();
		queues = new HashMap<>();
		partialOutputs = new HashMap<>();
	}

	public BigInteger start() {
		long steps = 0;
		do {
			for (Map.Entry<BigInteger, IntCodeInterpreter> ici : attachedComputers.entrySet()) {
				ici.getValue().runProgramInterleaved(1);
			}
			steps++;
			if (steps % 50 == 0) {
				System.out.println(steps);
				reportStates();
			}
		} while (queues.getOrDefault(BigInteger.valueOf(255), new ArrayList<>()).size() < 2);
		System.out.println(steps);
		return queues.get(BigInteger.valueOf(255)).get(1);
	}

	public void attachComputer(BigInteger networkAddress, Path programLocation) {
		IntCodeInterpreter ici = new IntCodeInterpreter(networkAddress, this, this);
		ici.loadProgramFromFile(programLocation);
		// make sure the first input this interpreter receives is its network address.
		// we can't use singleton or arrays.asList because the queue needs to be modifiable
		List<BigInteger> queue = new ArrayList<>();
		queue.add(networkAddress);
		queues.put(networkAddress, queue);
		attachedComputers.put(networkAddress, ici);
	}

	public void reportStates() {
		for (Map.Entry<BigInteger, IntCodeInterpreter> ici : attachedComputers.entrySet()) {
			System.out.printf("%02d  %s\n", ici.getKey(), queues.getOrDefault(ici.getKey(), new ArrayList<>()));
		}
	}
	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		List<BigInteger> partialOutput = partialOutputs.getOrDefault(interpreterNetworkAddress, new ArrayList<>());
		partialOutput.add(output);
		if (partialOutput.get(0).equals(BigInteger.valueOf(255))) {
			System.out.println("############# 255: " + output);
		}
		lastOutput = output;
		if (partialOutput.size() > 2) {
			List<BigInteger> queue = queues.getOrDefault(partialOutput.get(0), new ArrayList<>());
			queue.add(partialOutput.get(1));
			queue.add(partialOutput.get(2));
			queues.put(partialOutput.get(0), queue);
			partialOutput = new ArrayList<>();
		}
		partialOutputs.put(interpreterNetworkAddress, partialOutput);
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		List<BigInteger> queue = queues.getOrDefault(interpreterNetworkAddress, new ArrayList<>());
		if (queue.size() > 0) {
			return queue.remove(0);
		}
		queues.put(interpreterNetworkAddress, queue);
		return BigInteger.valueOf(-1);
	}

	@Override
	public BigInteger getLastOutput() {
		return lastOutput;
	}
}
