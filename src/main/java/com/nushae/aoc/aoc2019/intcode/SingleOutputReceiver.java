package com.nushae.aoc.aoc2019.intcode;

import java.math.BigInteger;

public class SingleOutputReceiver implements OutputReceiver {
	protected BigInteger myLastOutput;

	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		myLastOutput = output;
	}

	public BigInteger getLastOutput() {
		if (myLastOutput == null) {
			throw new IllegalStateException("Output requested before OutputReceiver received any output");
		}
		return myLastOutput;
	}
}
