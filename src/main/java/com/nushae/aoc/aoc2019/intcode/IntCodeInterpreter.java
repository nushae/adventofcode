package com.nushae.aoc.aoc2019.intcode;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * <p>The IntCode class is a simple interpreter that is used for various exercises. Each exercise expands the
 * capabilities of the interpreter in a backwards compatible way.</p>
 *
 * <p><em>To assist with analysing programs I've added the possibility to add comments to input lines.
 * Before breaking a line of comma separated values into opcodes, everything after and including the '#'
 * character is removed. Whitespace is ignored as well, so opcodes may be aligned if desired.</em></p>
 *
 * <p><strong>Exercise02:</strong>
 * <ul>
 *     <li>Initial version with three instructions: halt, add, and subtract, and one parameter mode - 'position'</li>
 * </ul>
 *
 * <p><strong>Exercise05.1:</strong>
 * <ul>
 *     <li>New parameter mode: immediate (in addition to 'position')</li>
 *     <li>Two new instructions: input and output.</li>
 *     <li>For convenience I created the <code>InputProvider</code> and <code>OutputReceiver</code> interfaces for
 *       automatic input/output (input instructions will block until data is received, as though manually from a
 *       prompt)</li>
 *     <li>The interpreter falls back to interactive input/output if no IP / OR are provided</li>
 * </ul>
 *
 * <p><strong>Exercise05.2:</strong>
 * <ul>
 *     <li>Four additional instructions: jump if true, jump if false, less than, and equals.</li>
 * </ul>
 *
 * <p><strong>Exercise07.1:</strong>
 * <ul>
 *     <li>Introduces multiple interpreter instances (already supported, so happy I made those interfaces)</li>
 *     <li>Introduces one-on-one input/output program connections</li>
 *     <li>Added <code>CyclicListInputProvider</code> and <code>SingleOutputReceiver</code> for convenience</li>
 *     <li>An IP can be attached to any number of interpreters; it will provide input on a first come first served basis</li>
 *     <li>An OR can be attached to any number of interpreters; it will store received data in received order, regardless of source</li>
 * </ul>
 *
 * <p><strong>Exercise07.2:</strong>
 * <ul>
 *     <li>Introduces a cyclic chain of programs</li>
 *     <li>For this I added <code>SingleInputOutputConnector</code> (combines all input/output features so far) for convenience</li>
 *     <li>I also made it possible to 'pause on output': run a program only until just after it outputs (via instruction 4)
 *       (or until it halts) to be able to synchronize the chain of programs on each other</li>
 *     <li>If the program would halt right after outputting, this still counts as halting.</li>
 * </ul>
 *
 * <p><strong>Exercise09:</strong>
 * <ul>
 *     <li>Introduces relative parameter mode (like 'position', but counting from 'relative base' instead of 0)</li>
 *     <li>Introduces the 'relative base' register (starts at 0)</li>
 *     <li>Introduces a new instruction: relative base offset</li>
 *     <li>From now on memory size is dynamically increased if an address is peeked or poked that falls outside the memory</li>
 *     <li>Large number support: the interpreter now works with <code>BigInteger</code> instead of <code>int</code></li>
 * </ul>
 *
 * <p><strong>Exercise11:</strong>
 * <ul>
 *     <li>No new changes needed, my interfaces can be reused as is :)</li>
 *     <li>Created an additional special purpose IP/OR as a private <code>Robot</code> subclass</li>
 * </ul>
 *
 * <p><strong>Exercise 13:</strong>
 * <ul>
 *     <li>No new changes needed, my interfaces can be reused as is :)</li>
 *     <li>Created an additional special purpose IP/OR as a private <code>ArcadeGame</code> subclass</li>
 * </ul>
 *
 * <p><strong>Exercise 15:</strong>
 * <ul>
 *     <li>Added the ability to halt the interpreter through an external signal</li>
 *     <li>Created an additional special purpose IP/OR as a private <code>RepairDroid</code> subclass</li>
 * </ul>
 *
 * <p><strong>Exercise 17:</strong>
 * <ul>
 *     <li>Added the ability to halt the interpreter through an external signal</li>
 *     <li>Created the <code>Observer</code> OR, which can handle 'ASCII displays' which it outputs to the console</li>
 * </ul>
 *
 * <p><strong>Exercise 19:</strong><br />
 * No new changes needed, my interfaces can be reused as is :)</p>
 *
 * <p><strong>Exercise 21:</strong><br />
 * No new changes needed, my interfaces can be reused as is :)</p>
 *
 * <p><strong>Exercise 23:</strong>
 * <ul>
 *     <li>Introduces the concept of multiple interpreters communicating 'in parallel' via a network;</li>
 *     <li>Neither syncing on input, nor on output, nor on both worked, so added runInterleaved(N) method which will
 *       run the interpreter for N instructions and then pause it</li>
 *     <li>Parallelism is simulated by repeatedly calling runProgramInterleaved(N) and cycling over all interpreters</li>
 *     <li>For networking to work OR had to be able to differentiate between the interpreters producing the output, also
 *       IP had to be able to differentiate between multiple interpreters requesting input, which is done by including
 *       the interpreter's network address in any nextValue() or acceptOutput() instruction</li>
 *     <li>Every existing IP and OR has been adjusted to this change in the interfaces</li>
 *     <li>(An IP or OR can still be instantiated without network address and will then use address 0 for convenience in non
 *       network use)</li>
 * </ul>
 *
 * <p><strong>Exercise 25:</strong>
 * <ul>
 *     <li>No new changes needed, my interfaces can be reused as is :)</li>
 *     <li>Created the <code>AutoPlayer</code> IP/OR which doesn't yet do its name justice (it mixes interactive input/output with autoplay)</li>
 *     <li>LOVED this puzzle by the way.</li>
 * </ul>
 */
@Log4j2
public class IntCodeInterpreter {

	// instructions
	// From exercise 2:
	public static final int HALT = 99;
	public static final int ADD = 1;
	public static final int MULTIPLY = 2;
	// From exercise 5.1:
	public static final int INPUT = 3;
	public static final int OUTPUT = 4;
	// From exercise 5.2:
	public static final int JUMP_IF_TRUE = 5;
	public static final int JUMP_IF_FALSE = 6;
	public static final int LESS_THAN = 7;
	public static final int EQUALS = 8;
	public static final int RELATIVE_BASE_OFFSET = 9;

	// parameter modes (introduced in exercise 5)
	public static final int POSITION = 0;
	public static final int IMMEDIATE = 1;
	// From exercise 9
	public static final int RELATIVE = 2;

	private static final BigInteger B100 = BigInteger.valueOf(100);

	private HashMap<BigInteger, BigInteger> originalMemory; // used to preserve and reload the program
	private HashMap<BigInteger, BigInteger> workingMemory;
	private BigInteger highestAddress;

	private BigInteger instructionPointer;
	private String lastOp;
	private BigInteger relativeBase;
	private InputProvider inputProvider;
	private OutputReceiver outputReceiver;
	// these two were introduced with exercise23, to be able to pause on both input and output
	private boolean needInput;
	private boolean didOutput;
	// from exercise 23:
	private BigInteger networkAddress;

	//exercise 25 put some pressure on the manual input fallback, which doesn't handle strings. So:
	private Scanner in;
	private Deque<BigInteger> inputFallBack;

	public IntCodeInterpreter() {
		this(BigInteger.ZERO);
	}

	public IntCodeInterpreter(BigInteger netWorkAddress) {
		this(netWorkAddress, null);
	}

	public IntCodeInterpreter(BigInteger netWorkAddress, InputProvider inputProvider) {
		this(netWorkAddress, inputProvider, null);
	}

	public IntCodeInterpreter(BigInteger netWorkAddress, InputProvider inputProvider, OutputReceiver outputReceiver) {
		this.networkAddress = netWorkAddress;
		this.inputProvider = inputProvider;
		this.outputReceiver = outputReceiver;
	}

	public void loadProgramFromFile(String fileName) {
		loadProgramFromFile(Paths.get(fileName));
	}

	public void loadProgramFromFile(Path path) {
		originalMemory = new HashMap<>();
		highestAddress = BigInteger.valueOf(-1); // no addresses in use yet
		try (Stream<String> stream = Files.lines(path)) {
			stream.forEach(s -> appendOpcodes(toListOfBigInts((s.contains("#") ? s.substring(0, s.indexOf("#")) : s).trim())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.debug("Opcodes loaded: " + originalMemory.size() + ", highest address: " + highestAddress);
		resetState();
	}

	// for testing purposes
	public void loadProgramFromString(String program) {
		originalMemory = new HashMap<>();
		highestAddress = BigInteger.valueOf(-1); // no addresses in use yet
		appendOpcodes(toListOfBigInts((program)));
		log.debug("Opcodes loaded: " + originalMemory.size() + ", highest address: " + highestAddress);
		resetState();
	}

	private static List<BigInteger> toListOfBigInts(String nums) {
		return Arrays.stream(nums.split(",")).filter(s -> !s.isEmpty()).map(BigInteger::new).toList();
	}

	public void appendOpcodes(List<BigInteger> opcodes) {
		for (BigInteger opcode : opcodes) {
			highestAddress = highestAddress.add(BigInteger.ONE);
			originalMemory.put(highestAddress, opcode);
		}
	}

	public void resetState() {
		inputFallBack = new ArrayDeque<>();
		in = null;
		workingMemory = new HashMap<>();
		highestAddress = BigInteger.valueOf(-1);
		for (Map.Entry<BigInteger, BigInteger> memEntry : originalMemory.entrySet()) {
			BigInteger address = memEntry.getKey();
			BigInteger value = memEntry.getValue();
			highestAddress = highestAddress.max(address);
			workingMemory.put(address, value);
		}
		relativeBase = BigInteger.ZERO;
		instructionPointer = BigInteger.ZERO;
		lastOp = "";
	}

	public void setInputProvider(InputProvider inputProvider) {
		this.inputProvider = inputProvider;
	}

	public void setOutputReceiver(OutputReceiver outputReceiver) {
		this.outputReceiver = outputReceiver;
	}

	public void poke(BigInteger memAddress, BigInteger value) {
		if (memAddress.compareTo(BigInteger.ZERO) < 0) {
			throw new IllegalArgumentException("Negative memory addresses are not supported");
		}
		workingMemory.put(memAddress, value);
		highestAddress = highestAddress.max(memAddress);
	}

	public BigInteger peek(BigInteger memAddress) {
		if (memAddress.compareTo(BigInteger.ZERO) < 0) {
			throw new IllegalArgumentException("Negative memory addresses are not supported");
		}
		if (workingMemory.containsKey(memAddress)) {
			return workingMemory.get(memAddress);
		} else {
			poke(memAddress, BigInteger.ZERO);
			return BigInteger.ZERO;
		}
	}

	public boolean executeNextInstruction() {
		didOutput = false;
		needInput = false;
		BigInteger param1, param2, param3;
		BigInteger value = peek(instructionPointer);
		BigInteger opCode = value.mod(B100);
		value = value.divide(B100);
		String mnem = String.format("executing %03d:%02d", value, opCode);
		switch (opCode.intValueExact()) {
			case HALT:
				log.debug(String.format("%d:HALT", instructionPointer));
				lastOp = "HLT";
				break;
			case ADD:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param2 = getParam(instructionPointer.add(BigInteger.valueOf(2)), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param3 = getWriteParam(instructionPointer.add(BigInteger.valueOf(3)), value.mod(BigInteger.TEN).intValueExact());
				poke(param3, param1.add(param2));
				log.debug(String.format("%04d:  %s  ADD %d %d -> %d", instructionPointer, mnem, param1, param2, param3));
				instructionPointer = instructionPointer.add(BigInteger.valueOf(4));
				lastOp = "ADD";
				break;
			case MULTIPLY:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param2 = getParam(instructionPointer.add(BigInteger.valueOf(2)), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param3 = getWriteParam(instructionPointer.add(BigInteger.valueOf(3)), value.mod(BigInteger.TEN).intValueExact());
				poke(param3, param1.multiply(param2));
				log.debug(String.format("%04d:  %s  MUL %d %d -> %d", instructionPointer, mnem, param1, param2, param3));
				instructionPointer = instructionPointer.add(BigInteger.valueOf(4));
				lastOp = "MUL";
				break;
			case INPUT:
				param1 = getWriteParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				BigInteger receivedInput = doInput();
				if (receivedInput != null) {
					poke(param1, receivedInput);
					log.debug(String.format("%04d:  %s  INP %d -> %d", instructionPointer, mnem, peek(param1), param1));
					instructionPointer = instructionPointer.add(BigInteger.valueOf(2));
				} else {
					// unsuccessful input. Next round it will be attempted again
					needInput = true;
				}
				lastOp = "IN";
				break;
			case OUTPUT:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				doOutput(param1);
				log.debug(String.format("%04d:  %s  OUT %d", instructionPointer, mnem, param1));
				instructionPointer = instructionPointer.add(BigInteger.valueOf(2));
				didOutput = true;
				lastOp = "OUT";
				break;
			case JUMP_IF_TRUE:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param2 = getParam(instructionPointer.add(BigInteger.valueOf(2)), value.mod(BigInteger.TEN).intValueExact());
				if (!param1.equals(BigInteger.ZERO)) {
					log.debug(String.format("%04d:  %s  JIT (true) -> %d", instructionPointer, mnem, param2));
					instructionPointer = param2;
				} else {
					log.debug(String.format("%04d:  %s  JIT (false)", instructionPointer, mnem));
					instructionPointer = instructionPointer.add(BigInteger.valueOf(3));
				}
				lastOp = "JIT";
				break;
			case JUMP_IF_FALSE:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param2 = getParam(instructionPointer.add(BigInteger.valueOf(2)), value.mod(BigInteger.TEN).intValueExact());
				if (param1.equals(BigInteger.ZERO)) {
					log.debug(String.format("%04d:  %s  JIF (false) -> %d", instructionPointer, mnem, param2));
					instructionPointer = param2;
				} else {
					log.debug(String.format("%04d:  %s  JIF (true)", instructionPointer, mnem));
					instructionPointer = instructionPointer.add(BigInteger.valueOf(3));
				}
				lastOp = "JIF";
				break;
			case LESS_THAN:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param2 = getParam(instructionPointer.add(BigInteger.valueOf(2)), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param3 = getWriteParam(instructionPointer.add(BigInteger.valueOf(3)), value.mod(BigInteger.TEN).intValueExact());
				if (param1.compareTo(param2) < 0) {
					log.debug(String.format("%04d:  %s  LE %d < %d so write 1 -> %d", instructionPointer, mnem, param1, param2, param3));
					poke(param3, BigInteger.ONE);
				} else {
					log.debug(String.format("%04d:  %s  LE %d >= %d so write 0 -> %d", instructionPointer, mnem, param1, param2, param3));
					poke(param3, BigInteger.ZERO);
				}
				instructionPointer = instructionPointer.add(BigInteger.valueOf(4));
				lastOp = "LE";
				break;
			case EQUALS:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param2 = getParam(instructionPointer.add(BigInteger.valueOf(2)), value.mod(BigInteger.TEN).intValueExact());
				value = value.divide(BigInteger.TEN);
				param3 = getWriteParam(instructionPointer.add(BigInteger.valueOf(3)), value.mod(BigInteger.TEN).intValueExact());
				if (param1.equals(param2)) {
					log.debug(String.format("%04d:  %s  EQ %d == %d so write 1 -> %d", instructionPointer, mnem, param1, param2, param3));
					poke(param3, BigInteger.ONE);
				} else {
					log.debug(String.format("%04d:  %s  EQ %d != %d so write 0 -> %d", instructionPointer, mnem, param1, param2, param3));
					poke(param3, BigInteger.ZERO);
				}
				instructionPointer = instructionPointer.add(BigInteger.valueOf(4));
				lastOp = "EQ";
				break;
			case RELATIVE_BASE_OFFSET:
				param1 = getParam(instructionPointer.add(BigInteger.ONE), value.mod(BigInteger.TEN).intValueExact());
				log.debug(String.format("%04d:  %s  RBO %d -> %d", instructionPointer, mnem, relativeBase, relativeBase.add(param1)));
				relativeBase = relativeBase.add(param1);
				instructionPointer = instructionPointer.add(BigInteger.valueOf(2));
				lastOp = "RBO";
				break;
			default:
				throw new IllegalArgumentException("Unknown opCode at " + instructionPointer + ": " + opCode);
		}
		return didOutput || needInput;
	}

	private BigInteger getParam(BigInteger address, int mode) {
		if (mode == POSITION) {
			return peek(peek(address));
		} else if (mode == IMMEDIATE) {
			return peek(address);
		} else if (mode == RELATIVE) {
			return peek(relativeBase.add(peek(address)));
		}
		throw new IllegalArgumentException("Unknown parameter mode: " + mode);
	}

	private BigInteger getWriteParam(BigInteger address, int mode) {
		if (mode == POSITION) {
			return peek(address);
		} else if (mode == IMMEDIATE) {
			throw new IllegalStateException("IMMEDIATE mode used for write parameter");
		} else if (mode == RELATIVE) {
			return relativeBase.add(peek(address));
		}
		throw new IllegalArgumentException("Unknown parameter mode: " + mode);
	}

	/**
	 * Rerun the program from start, first resetting its state, until it HALTs
	 *
	 * @return state as String, for backwards compatibility
	 */
	public String reRunProgram() {
		resetState();
		return runProgram();
	}

	/**
	 * Continue running the program without resetting its state, until it HALTs.
	 *
	 * @return state as String, for backwards compatibility
	 */
	public String runProgram() { // rerun program without resetting its state
		while (!isHalted()) {
			executeNextInstruction();
		}
		return stateToString(); // for backwards compatibility
	}

	/**
	 * Continue running the program without resetting its state, until output or input occurs, or it HALTs.
	 * The parameters control when to pause, on input, on output, or both. The pause occurs just after the IO happens,
	 * and HALT state still takes precedence.
	 *
	 * @param onInput pause on unsuccessful input if true
	 * @param onOutput pause after any output if true
	 * @return state as String, for backwards compatibility
	 */
	public String runProgramAndPauseOnIO(boolean onInput, boolean onOutput) {
		boolean pause = false;
		while (!isHalted() && !pause) {
			pause = executeNextInstruction();
			if (pause) {
				pause = onOutput && didOutput || onInput && needInput;
			}
		}
		return stateToString(); // for backwards compatibility
	}

	/**
	 * Continue running the program without resetting its state, until instructionsToRun instructions have been
	 * executed, or the program HALTs.
	 *
	 * @param instructionsToRun number of instructions to run before pausing
	 * @return state as String, for backwards compatibility
	 */
	public String runProgramInterleaved(int instructionsToRun) {
		while (!isHalted() && instructionsToRun > 0) {
			executeNextInstruction();
			instructionsToRun--;
		}
		return stateToString(); // for backwards compatibility
	}

	public String getLastOp() {
		return lastOp;
	}

	public void list() {
		for (Map.Entry<BigInteger, BigInteger> memEntry : workingMemory.entrySet()) {
			BigInteger address = memEntry.getKey();
			BigInteger value = memEntry.getValue();
			System.out.printf("%016d: %d\n", address, value);
		}
		System.out.println("--");
		System.out.println();
	}

	public void abort() {
		poke(instructionPointer, BigInteger.valueOf(HALT));
	}

	public boolean isHalted() {
		return peek(instructionPointer).mod(B100).intValueExact() == HALT;
	}

	private BigInteger doInput() {
		if (inputProvider != null) {
			return inputProvider.nextValue(networkAddress);
		}
		// fall back to interactive input
		if (inputFallBack.isEmpty()) {
			if (in == null) {
				in = new Scanner(System.in);
				in.useDelimiter("\n");
			}
			String input = in.next();
			for (int i=0; i < input.length(); i++) {
				inputFallBack.add(BigInteger.valueOf(input.charAt(i)));
			}
			inputFallBack.add(BigInteger.valueOf(10)); // re-add the newline that the Scanner ate
		}
		return inputFallBack.poll();
	}

	private void doOutput(BigInteger value) {
		if (outputReceiver != null) {
			outputReceiver.acceptOutput(networkAddress, value);
		} else {
			// fallback to printing
			System.out.printf(": %d\n", value);
		}
	}

	public String stateToString() {
		StringBuilder sb = new StringBuilder(workingMemory.get(BigInteger.ZERO).toString());
		for (BigInteger mem = BigInteger.ONE; mem.compareTo(highestAddress)<=0; mem = mem.add(BigInteger.ONE)) {
			sb.append(",").append(peek(mem).toString());
		}
		return sb.toString();
	}
}
