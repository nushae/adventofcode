package com.nushae.aoc.aoc2019.intcode;

import com.nushae.aoc.domain.SearchNode;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.math.BigInteger;
import java.util.List;
import java.util.*;
import java.util.function.Predicate;

import static com.nushae.aoc.util.MathUtil.*;
import static com.nushae.aoc.util.SearchUtil.dfs;

/**
 * This 'autoplayer' was meant to play and solve the game automatically, but there are some things that make
 * this VERY hard if not impossible.
 * <p>
 * This needs to:
 * - explore the map, constructing that map as it explores, picking up every item it can (works)
 * - identify the airlock ('airlock' had to be hardcoded as its goal room, no way around this)
 * - once all items have been picked up, head for the airlock (works)
 * - try every item combination in inventory until the airlock can successfully be entered (works)
 * - since other players' inputs can have different items / rooms I don't know about, there must be a fallback to
 * "dump last output received and HALT" so I can expand the blacklist if needed (works)
 * <p>
 * However since some items are 'trap items' it would also need to:
 * - rerun the game whenever it dies
 * - identify when it is trapped and abort the game and rerun
 * - preserve knowledge gained between runs
 * <p>
 * The existence of the 'infinite loop' item, which makes the IntCode Interpreter enter an infinite loop, convinced
 * me that not all of these goals were attainable. So instead, I let the game be switched from manual to auto, so I
 * could explore the map and make a blacklist of items etc. This seemed like a good compromise, and indeed, after a
 * sufficient amount of manual exploration, I could immediately switch to auto as my first move.
 * <p>
 * Currently, the autoplayer starts in manual mode (type commands and read the output yourself), with a manual
 * switch to 'autosolve mode' that can be activated at *any* time.
 * <p>
 * MANUAL behaves like this:
 * - execute manual command by passing on whatever is typed to the interpreter, switch to automated mode when
 * 'auto' is entered. What type of auto mode we switch to (EXPLORE, TO_SEC, or SAFECRACKING) depends on the
 * "internal state", aka map knowledge, inventory, etc,
 * - process received output (whenever "Command?" is received) to update internal state
 * Actually, updating state is done regardless of whether state is MANUAL or not
 * - dump received output to console, with additionally an overview of the internal state:
 * - map-so-far
 * - inventory collected
 * - current blacklist
 * - items known to be in the world and not picked up (should be empty at the end, except for blacklisted items)
 * - list of 'yet to be explored' exits/rooms with paths to them if not there
 * <p>
 * AUTO behaves like this:
 * - when in EXPLORE: systematically visit every room and pass through every door, pick up all non-blacklisted items
 * Switch to TO_SEC once all items have been acquired and the map is known
 * - when in TO_SEC: move through the map until the security check room is reached, then switch to SAFECRACKING
 * - when in SAFECRACKING: systematically attempt to enter the pressure plate room once for every possible
 * combination of items in the inventory
 */
public class AutoPlayer implements InputProvider, OutputReceiver {

	private static final String SECURITY_CHECKPOINT = "Security Checkpoint";
	private static final String PRESSURE_FLOOR = "Pressure-Sensitive Floor";

	public static class Room implements SearchNode {
		public final String name;
		public final String description;
		@Getter
		public final List<String> items;
		private final List<Point> doors;
		private final Map<Point, Room> adjacentRooms;
		public final List<String> blacklist; // a reference to the item blacklist to filter items in the room against
		private SearchNode parent;

		public Room(String roomName, String description, List<Point> doors, List<String> items, List<String> blacklist) {
			name = roomName;
			this.description = description;
			adjacentRooms = new HashMap<>();
			this.doors = doors;
			this.items = items;
			this.blacklist = blacklist;
		}

		public void connect(Point dir, Room neighbour) {
			adjacentRooms.put(dir, neighbour);
		}

		public List<Point> getExits() {
			return doors;
		}

		// test whether this door has been explored yet
		public boolean doorIsExplored(Point door) {
			return adjacentRooms.containsKey(door);
		}

		// return first unexplored door in this room, or null if there are none left
		public Point getUnexploredDoor() {
			for (Point door : doors) {
				if (!adjacentRooms.containsKey(door)) {
					return door;
				}
			}
			return null;
		}

		@Override
		public Map<SearchNode, Long> getAdjacentNodes() {
			Map<SearchNode, Long> result = new HashMap<>();
			for (Room r : adjacentRooms.values()) {
				result.put(r, 1L);
			}
			return result;
		}

		@Override
		public String getState() {
			String status = "";
			if (items.stream().anyMatch(item -> !blacklist.contains(item))) {
				status = "items " + items;
			}
			if (doors.size() != adjacentRooms.size()) {
				status += (status.isEmpty() ? "" : ", ") + "exits";
			}
			return name + " is " + (status.isEmpty() ? "dull" : "interesting - " + status);
		}

		@Override
		public void setParent(SearchNode parent) {
			this.parent = parent;
		}

		@Override
		public SearchNode getParent() {
			return parent;
		}

		public void clearParent() {
			parent = null;
		}

		@Override
		public int hashCode() {
			return name.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (obj == this) {
				return true;
			}
			if (obj.getClass() != getClass()) {
				return false;
			}
			Room other = (Room) obj;
			return name.equals(other.name);
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public class State {
		public int mode; // what solving mode are we using?
		@Getter
		public String lastCommand; // the last command that we executed, in order to judge if output is as expected
		public List<String> pendingCommands; // a list of commands we plan to give if the last one was successful
		public List<String> itemBlacklist; // DON'T pick these up!!
		public List<String> inventory; // what we are carrying (NOTE: this changes when we start SAFECRACKING)
		public Map<String, Room> knownRooms; // known rooms by name
		public Map<String, Room> itemsInWorld; // items not picked up that are in the world
		public Room startRoom; // the room in which we started
		public Room currentRoom; // the room in which we are right now
		public Room previousRoom; // the last room we were in, handy for connecting rooms
		public Room finalRoom; // our final destination, the airlock
		public Map<String, Set<Point>> exploreTodoList; // names of rooms that have unexplored exits, along with those exits

		// these three are set when we switch to SAFECRACKING mode and don't need to be initialized
		public int attempt; // binary representation of the items to hold (1 = hold, #bits == inventory.size())
		public String attemptMove; // the direction to move in to make an attempt to enter the airlock
		public List<String> attemptSet; // the relevant set of items for the attempts

		// when exploreTodoList is empty and worldMap.size() > 1, we have explored the entire world. If additionally
		// itemsInWorld is empty, we can head for the end room

		// Start with no knowledge whatsoever, so figure out what room you're starting in, then start in MANUAL mode.
		public State() {
			this(MANUAL, null, null, null, null, new HashMap<>(), new ArrayList<>(), new ArrayList<>(), new HashMap<>(), new HashMap<>());
		}

		// start with any knowledge you want to hardcode
		public State(int mode, Room startRoom, Room currentRoom, Room previousRoom, Room finalRoom, Map<String, Room> knownRooms, List<String> startingInventory, List<String> startingBlacklist, Map<String, Room> startingItemKnowledge, Map<String, Set<Point>> startingTodoList) {
			this.mode = mode;
			this.lastCommand = "";
			this.pendingCommands = new ArrayList<>();
			this.itemBlacklist = startingBlacklist;
			this.inventory = startingInventory;
			this.itemsInWorld = startingItemKnowledge;
			this.startRoom = startRoom;
			this.currentRoom = currentRoom;
			this.previousRoom = previousRoom;
			this.finalRoom = finalRoom;
			this.knownRooms = knownRooms;
			this.exploreTodoList = startingTodoList;
		}

		public boolean isFullyExplored() {
			// the final door in the final room doesn't HAVE to be explored, but it could be if we came by it before we were ready.
			return exploreTodoList.isEmpty() || exploreTodoList.size() == 1 && exploreTodoList.containsKey(SECURITY_CHECKPOINT);
		}

		public boolean allItemsGathered() {
			for (String item : itemsInWorld.keySet()) {
				if (!itemBlacklist.contains(item)) {
					return false;
				}
			}
			return true;
		}

		public Point enteredRoomFrom() {
			if (currentRoom != null && previousRoom != null) {
				// we can assume they are connected, since we connect every room we enter to the room we came from
				for (Map.Entry<Point, Room> neighbour : currentRoom.adjacentRooms.entrySet()) {
					if (neighbour.getValue().equals(previousRoom)) {
						return neighbour.getKey();
					}
				}
			}
			return null;
		}

		public String getLastPickup() {
			if (!inventory.isEmpty()) {
				return inventory.get(inventory.size() - 1);
			}
			return null;
		}

		public void connectRooms(Room fromRoom, Point door, Room toRoom) {
			fromRoom.connect(door, toRoom);
			if (fromRoom.getUnexploredDoor() == null) {
				exploreTodoList.remove(fromRoom.name);
			} else {
				exploreTodoList.get(fromRoom.name).remove(door);
			}
			Point rev = scale(door, -1); // reverse direction
			toRoom.connect(rev, fromRoom);
			if (toRoom.getUnexploredDoor() == null) {
				exploreTodoList.remove(toRoom.name);
			} else {
				exploreTodoList.get(toRoom.name).remove(rev);
			}
		}

		// We cleverly use getState() with predicates here to be able to find paths to any room we are interested in
		public String[] pathToNearestRoomOfInterest(Predicate<SearchNode> interest) {
			if (currentRoom.getState().contains("interesting")) {
				return new String[]{"inv"};
			}
			Room result = (Room) dfs(currentRoom, interest);
			List<Room> path = new ArrayList<>();
			while (result != null) {
				path.add(0, result);
				result = (Room) result.getParent();
			}
			List<String> moves = new ArrayList<>();
			for (int i = 0; i < path.size() - 1; i++) {
				for (Map.Entry<Point, Room> neighbour : path.get(i).adjacentRooms.entrySet()) {
					if (neighbour.getValue().equals(path.get(i + 1))) {
						moves.add(DIR_TO_WORD.get(neighbour.getKey()));
						break;
					}
				}
			}
			// reset parents for next search
			for (Room room : knownRooms.values()) {
				room.clearParent();
			}
			return moves.toArray(new String[0]);
		}

		public void initAttemptSet() {
			// preserve items relevant for in the attempts
			attemptSet = new ArrayList<>(inventory);
			attempt = 0;
			// We should now be in GOAL_ROOM (or this method wouldn't have been called) with exactly
			// one unexplored door in our list, or none (if we hit the pressure room before we explored the whole map).
			// So eother the unexplored door is the direction towards the Airlock room, or the door
			// we found the Pressure plate behind
			if (currentRoom.getUnexploredDoor() != null) {
				attemptMove = DIR_TO_WORD.get(currentRoom.getUnexploredDoor());
			} else {
				for (Map.Entry<Point, Room> entry : currentRoom.adjacentRooms.entrySet()) {
					if (PRESSURE_FLOOR.equals(entry.getValue().name)) {
						attemptMove = DIR_TO_WORD.get(entry.getKey());
						break;
					}
				}
			}
		}

		public String nextAttempt() {
			int oldGray = toGrayCode(state.attempt);
			state.attempt++;
			int newGray = toGrayCode(state.attempt);
			int toggleItemIndex = (int) Math.round(Math.log(oldGray ^ newGray) / Math.log(2)); // indicates which item to drop or pick up
			String item = attemptSet.get(toggleItemIndex);
			if (inventory.contains(item)) { // drop
				return "drop " + item + ";" + attemptMove;
			} else { // pick up
				return "take " + item + ";" + attemptMove;
			}
		}
	}

	// Currently supports:
	// MANUAL -> (enter 'auto') -> SAFECRACKING -> interpreter HALTs with solution in lastOutput
	// You need to switch to auto when you have all items in inventory and are ready to move into the airlock
	//
	// The goal is to be able to switch to auto at any time:
	// MANUAL -> (enter 'auto') -> (depending on state, we go to EXPLORING, TO_SEC, or SAFECRACKING),
	//   then: EXPLORING -> TO_SEC -> SAFECRACKING -> interpreter HALTs with solution in lastOutput
	private static final int MANUAL = 0; // manual input, leave with command 'auto'
	private static final int EXPLORING = 1; // automatically exploring and building map / inventory
	private static final int TO_SEC = 2; // automatically returning to room before airlock
	private static final int SAFECRACKING = 3; // automatically trying all item combinations to enter airlock

	@Getter
	private final State state;
	Deque<BigInteger> command;
	Deque<String> inputBuffer;
	Scanner in;
	StringBuilder currentOutputLine;
	List<String> outputLines;
	BigInteger lastOutput;

	public AutoPlayer() {
		state = new State();
		state.itemBlacklist = Arrays.asList("escape pod", "giant electromagnet", "infinite loop", "molten lava", "photons");

		command = new ArrayDeque<>(); // a queue of BigIntegers representing the current command being input into the interpreter
		inputBuffer = new ArrayDeque<>(); // a queue of strings, each representing a single command, either manually entered or generated by the autoplayer
		currentOutputLine = new StringBuilder(); // used by the output receiver part: the incomplete current line of output being received
		outputLines = new ArrayList<>(); // the complete lines of output so far received
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		if (command.isEmpty()) {
			// produce more command(s); we either have already generated a series of commands (or input them manually)
			// that we can serve up one by one from inputBuffer, or we must produce more.
			if (inputBuffer.isEmpty()) {
				// produce at least one command to put into the input buffer; how depends on mode
				switch (state.mode) {
					case MANUAL:
						generateManualInput();
						break;
					case EXPLORING:
						generateAutoExploreInput();
						break;
					case TO_SEC:
						generateAirLockPath(); // not yet implemented!
						break;
					default: // SAFECRACKING, aka trying weight combinations to enter the airlock
						generateAirlockAttempt();
				}
			}
			if (!inputBuffer.isEmpty()) {
				state.lastCommand = inputBuffer.poll();
				if ("auto".equals(state.lastCommand)) {
					// figure out next state:
					// still remaining points of interest -> EXPLORE
					// not, but also not in Security -> TO_SEC
					// in security and all explored -> SAFECRACKING
					if (state.allItemsGathered() && state.isFullyExplored()) {
						if (SECURITY_CHECKPOINT.equals(state.currentRoom.name)) {
							state.mode = SAFECRACKING;
							inputBuffer.add("inv"); // final inventory check and trigger brute force
						} else {
							state.mode = TO_SEC;
							inputBuffer.addAll(Arrays.asList(state.pathToNearestRoomOfInterest(room -> room.getState().contains(SECURITY_CHECKPOINT))));
						}
					} else {
						state.mode = EXPLORING;
						inputBuffer.addAll(Arrays.asList(state.pathToNearestRoomOfInterest(room -> room.getState().contains("interesting"))));
					}
					state.lastCommand = inputBuffer.poll();
				}
				System.out.println("###CMD### <" + state.lastCommand + ">\n");
				for (int i = 0; i < state.lastCommand.length(); i++) {
					command.add(BigInteger.valueOf(state.lastCommand.charAt(i)));
				}
				command.add(BigInteger.valueOf(10));
			} else { // WTF
				System.out.println("No more input in buffer, aborting.");
				System.exit(0);
			}
		}
		return command.poll();
	}

	private void generateManualInput() {
		// manual mode: type commands, see output, do it yourself.
		// Use 'auto' to switch to auto-solve. Assumes you have picked everything up and are in security
		System.out.println("[MANUAL mode] Type command(s) separated with ';', use 'auto' to switch to AUTO mode>");
		if (in == null) {
			in = new Scanner(System.in);
			in.useDelimiter("\n");
		}
		// For my puzzle input, the full commands needed are
		// east;south;south;take hologram;north;north;west;south;take mouse;east;take shell;west
		// west;take whirled peas;east;north;west;north;north;west;take semiconductor;east;south
		// west;north;south;south;take hypercube;north;east;south;west;take antenna;south
		// take spool of cat6;north;west;south;south;auto
		//
		// east;south;south;take hologram;north;north;west;south;take mouse;east;take shell;west;west;take whirled peas;east;north;west;north;north;west;take semiconductor;east;south;west;north;south;south;take hypercube;north;east;south;west;take antenna;south;take spool of cat6;north;west;south;south;auto
		inputBuffer.addAll(Arrays.asList(in.next().split(";")));
	}

	private void generateAutoExploreInput() {
		// automatically walk the map and pick up everything not blacklisted. Priorities:
		// 1) if there are non-blacklisted items, pick all up                                                 -> take item
		// 2) if not but there are doors in this room we haven't tried yet, try one                           -> <dir>
		// 3) if there are still rooms of interest (unexplored exits or items in it), make route to the nearest
		// 4) if not, switch to TO_SEC and produce route to goal room
		System.out.print("[EXPLORE mode] generated command(s)> ");
		boolean pickedUp = false;
		StringBuilder pickups = new StringBuilder();
		for (String item : state.currentRoom.getItems()) {
			if (!state.itemBlacklist.contains(item)) {
				pickups.append(!pickups.isEmpty() ? "" : ";").append("take ").append(item);
				pickedUp = true;
				inputBuffer.add("take " + item);
			}
		}
		System.out.print(pickups);
		if (!pickedUp) {
			// We can't immediately add move(s) as well if we queued pickups, because those need
			// to be resolved before we can properly use itemsInWorld in our decisions here.
			if (state.currentRoom.getUnexploredDoor() != null) {
				System.out.print(DIR_TO_WORD.get(state.currentRoom.getUnexploredDoor()));
				inputBuffer.add(DIR_TO_WORD.get(state.currentRoom.getUnexploredDoor()));
			} else {
				String[] path = state.pathToNearestRoomOfInterest(room -> room.getState().contains("interesting"));
				if (path.length > 0) {
					System.out.print(StringUtils.join(path, ";"));
					inputBuffer.addAll(Arrays.asList(path));
				} else {
					if (SECURITY_CHECKPOINT.equals(state.currentRoom.name)) {
						state.mode = SAFECRACKING;
						inputBuffer.add("inv"); // final inventory check and trigger brute force
					} else {
						state.mode = TO_SEC;
						inputBuffer.addAll(Arrays.asList(state.pathToNearestRoomOfInterest(room -> room.getState().contains(SECURITY_CHECKPOINT))));
					}
				}
			}
		}
		System.out.println();
	}

	private void generateAirLockPath() {
		// TODO: implement
	}

	private void generateAirlockAttempt() {
		Collections.addAll(inputBuffer, state.nextAttempt().split(";"));
		System.out.println("| Trying combination " + Integer.toString(toGrayCode(state.attempt), 2));
	}

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		if (output.compareTo(BigInteger.valueOf(255)) <= 0) { // if not, this isn't ascii
			char p = (char) output.intValueExact();
			if (p == 10) { // newline; one more line added to the output
				String line = currentOutputLine.toString();
				outputLines.add(line);
				if (line.startsWith("\"Oh, hello!")) { // final room reached, IntCode interpreter will halt; save value for posterity
					lastOutput = new BigInteger(line.substring(51, 56));
				} else if (line.equals("Command?")) { // Full output received, inspect and update state
					// 1) print output
					dumpOutput();
					// 2) do output analysis and update state
					switch (state.lastCommand.split(" ")[0]) {
						case "": // just started the game, so we have successfully entered a room
						case "north": // the four dirs: we just moved. Either we have successfully entered a room, or got an error message
						case "east":
						case "south":
						case "west":
							handleMove();
							break;
						case "take": // either we successfully picked up an item, or got an error message
							handleTake();
							break;
						case "drop": // either we successfully dropped an item, or got an error message
							handleDrop();
							break;
						case "inv": // always succeeds, but we have a handler to verify our state against it
							handleInv();
							break;
						default:
							System.out.println("Unknown last command: " + state.lastCommand); // just in case something weird happens along the way
							System.exit(0);
					}
					// 3) show state, to help with finetuning the code
					dumpState();
					// 4) finally clear output, ready for the next command
					outputLines.clear();
				}
				currentOutputLine = new StringBuilder();
			} else {
				currentOutputLine.append(p);
			}
		}
	}

	private void handleMove() {
		// Either we successfully enter a room, or we get an error; also we can be automoved to a second
		// room after entering a room, so we 'eat' the output one room at a time. By sheer coincidence (yeah sure)
		// a room description either ends with "Command?" or some other line announcing automove, so one 'marker'
		// line is always there at the end of a room description.
		do {
			if (outputLines.size() > 3 && outputLines.get(3).startsWith("==")) { // success. New room? -> update state
				outputLines.remove(0); // empty
				outputLines.remove(0); // empty
				outputLines.remove(0); // empty
				String roomName = outputLines.get(0);
				roomName = roomName.substring(3, roomName.length() - 3);
				if (!state.knownRooms.containsKey(roomName)) { // new room, parse and create room, clearing output as we go
					outputLines.remove(0); // room name
					StringBuilder description = new StringBuilder();
					List<Point> doors = new ArrayList<>();
					List<String> items = new ArrayList<>();
					while (!outputLines.get(0).isEmpty()) {
						description.append(outputLines.get(0));
						outputLines.remove(0);
					}
					outputLines.remove(0); // empty line
					if ("Doors here lead:".equals(outputLines.get(0))) {
						outputLines.remove(0);
						while (outputLines.get(0).startsWith("-")) {
							doors.add(WORD_TO_DIR.get(outputLines.get(0).substring(2)));
							outputLines.remove(0);
						}
						outputLines.remove(0); // empty line
					}
					if ("Items here:".equals(outputLines.get(0))) {
						outputLines.remove(0);
						while (outputLines.get(0).startsWith("-")) {
							items.add(outputLines.get(0).substring(2));
							outputLines.remove(0);
						}
						outputLines.remove(0); // empty line
					}
					outputLines.remove(0); // "Command?", normally outputLines is now empty
					System.out.println("+-------NEW ROOM ENTERED");
					System.out.println("| Created room '" + roomName + "' with description '" + description + "'");
					System.out.println("| Exit(s): " + StringUtils.join(doors, ", "));
					System.out.println("| Item(s): " + StringUtils.join(items, ", "));
					Room newRoom = new Room(roomName, description.toString(), doors, items, state.itemBlacklist);
					state.knownRooms.put(roomName, newRoom);
					// update exploreTodoList
					state.exploreTodoList.put(newRoom.name, new HashSet<>(doors));
					// update itemsInWorld
					for (String item : items) {
						state.itemsInWorld.put(item, newRoom);
						System.out.println("| Updated items in world with '" + item + " in " + newRoom + "'");
					}
					// lastCommand is either "" or "<direction>" or we wouldn't be encountering a new room
					if ("".equals(state.lastCommand)) {
						// lastCommand is only "" after entering a room when we start the game, so we're in the start room
						state.startRoom = newRoom;
						state.currentRoom = newRoom;
						System.out.println("| Did not enter from any previous room into " + state.currentRoom);
					} else {
						state.previousRoom = state.currentRoom;
						state.currentRoom = newRoom;
						state.connectRooms(state.previousRoom, WORD_TO_DIR.get(state.lastCommand), state.currentRoom);
						System.out.println("| Connected " + state.previousRoom + " to " + state.currentRoom);
					}
				} else { // revisiting room, update previousRoom and currentRoom
					System.out.println("+-------KNOWN ROOM ENTERED");
					state.previousRoom = state.currentRoom;
					state.currentRoom = state.knownRooms.get(roomName);
					// make sure we remove exactly the output for this room, in case we got automoved to another room
					do {
						outputLines.remove(0); // description line
					} while (!outputLines.get(0).isEmpty());
					outputLines.remove(0); // empty line
					if ("Doors here lead:".equals(outputLines.get(0))) {
						do {
							outputLines.remove(0);
						} while (outputLines.get(0).startsWith("-"));
						outputLines.remove(0); // empty line
					}
					if ("Items here:".equals(outputLines.get(0))) {
						do {
							outputLines.remove(0);
						} while (outputLines.get(0).startsWith("-"));
						outputLines.remove(0); // empty line
					}
					outputLines.remove(0); // should normally be "Command?", and outputLines should be empty
				}
			} else if (outputLines.size() > 1 && "You can't go that way.".equals(outputLines.get(1))) {
				System.out.println("+-------MOVEMENT FAILED - INVALID DIRECTION");
				System.out.println("| Last command: " + state.lastCommand);
				System.out.println("| Room exits: " + state.currentRoom.getExits());
				if (state.mode != MANUAL) {
					// no updates to state, but this may indicate a flaw in our move decider, so abort
					System.out.println("| Aborting run.");
					dumpState();
					System.exit(0);
				} else {
					outputLines.clear();
				}
			} else if (outputLines.size() > 1 && outputLines.get(1).contains("You can't move!!")) {
				// ahhh, we need to remember the last item we picked up too, but inventory is a list so we KNOW THIS
				System.out.println("+-------MOVEMENT FAILED - UNABLE TO MOVE");
				System.out.println("| Last pickup (probably trap item): " + state.getLastPickup());
				System.out.println("| Last command: " + state.lastCommand);
				System.out.println("| Room exits: " + state.currentRoom.getExits());
				if (state.mode != MANUAL) {
					// no updates to state, but this may indicate a flaw in our move decider, so abort
					System.out.println("| Aborting run.");
					dumpState();
					System.exit(0);
				} else {
					outputLines.clear();
				}
			} else {
				System.out.println("+-------MOVEMENT FAILED (I THINK)");
				System.out.println("| Last command: " + state.lastCommand);
				System.out.println("| Room exits: " + state.currentRoom.getExits());
				if (state.mode != MANUAL) {
					// Unknown problem occurred. Better let the user handle it.
					System.out.println("| Trap?! Aborting.");
					dumpState();
					System.exit(0);
				} else {
					outputLines.clear();
				}
			}
		} while (!outputLines.isEmpty());
	}

	private void handleTake() {
		if (outputLines.size() > 1 && outputLines.get(1).startsWith("You take")) {
			System.out.println("+-------TAKE SUCCESSFUL");
			// update inventory / itemsInWorld using lastCommand
			String item = state.lastCommand.substring(5);
			boolean abort = false;
			if (state.currentRoom.items.contains(item)) {
				state.currentRoom.items.remove(item);
				System.out.println("| Removed " + item + " from room.");
			} else {
				System.out.println("| " + item + " is not in the room (should not happen).");
				if (state.mode != MANUAL) {
					abort = true;
				}
			}
			if (!state.inventory.contains(item)) {
				state.inventory.add(item);
				System.out.println("| Added " + item + " to inventory.");
			} else {
				System.out.println("| " + item + " is already in inventory (should not happen).");
				if (state.mode != MANUAL) {
					abort = true;
				}
			}
			if (state.itemsInWorld.containsKey(item)) {
				state.itemsInWorld.remove(item);
				System.out.println("| Removed " + item + " from 'items in world' list.");
			} else {
				System.out.println("| " + item + " is already not in the list (should not happen).");
				if (state.mode != MANUAL) {
					abort = true;
				}
			}
			if (abort) {
				System.out.println("| Inconsistent state detected - aborting.");
				dumpState();
				System.exit(0);
			}
		} else {
			System.out.println("+-------TAKE FAILED");
			if (state.mode != MANUAL) {
				System.out.println("| Don't know what happened, aborting.");
				dumpState();
				// unrecognised output, most likely the item is a new trap, let the user analyse it
				System.exit(0);
			}
		}
	}

	private void handleDrop() {
		if (outputLines.size() > 1 && outputLines.get(1).startsWith("You drop")) {
			System.out.println("+-------DROP SUCCESSFUL");
			String item = state.lastCommand.substring(5);
			boolean abort = false;
			if (!state.currentRoom.items.contains(item)) {
				state.currentRoom.items.add(item);
				System.out.println("| Added " + item + " to room.");
			} else {
				System.out.println("| " + item + " is already in the room (should not happen).");
				if (state.mode != MANUAL) {
					abort = true;
				}
			}
			if (state.inventory.contains(item)) {
				state.inventory.remove(item);
				System.out.println("| Removed " + item + " from inventory.");
			} else {
				System.out.println("| " + item + " is already not inventory (should not happen).");
				if (state.mode != MANUAL) {
					abort = true;
				}
			}
			if (!state.itemsInWorld.containsKey(item)) {
				state.itemsInWorld.put(item, state.currentRoom);
				System.out.println("| Added " + item + " to 'items in world'.");
			} else {
				System.out.println("| " + item + " is already in the world (should not happen).");
				if (state.mode != MANUAL) {
					abort = true;
				}
			}
			if (abort) {
				System.out.println("| Inconsistent state detected - aborting.");
				dumpState();
				System.exit(0);
			}
		} else {
			System.out.println("+-------DROP FAILED");
			if (state.mode != MANUAL) {
				System.out.println("| Don't know what happened, aborting.");
				dumpState();
				// unrecognised output, most likely the item is a new trap, let the user analyse it
				System.exit(0);
			}
		}
	}

	private void handleInv() {
		System.out.println("+-------INVENTORY OVERVIEW DETECTED");
		// "inv" with an unitialized attemptSet in SAFECRACKING mode triggers attempt initialization;
		// The first attempt with a full inventory is just a move in the direction of the airlock
		if (state.mode == SAFECRACKING && state.attemptSet == null && outputLines.size() > 1 && "Items in your inventory:".equals(outputLines.get(1))) {
			System.out.println("| INITIALIZING AUTOSOLVER");
			int itemsAdded = 0;
			for (int i = 2; i < outputLines.size() && !outputLines.get(i).isEmpty(); i++) {
				String item = outputLines.get(i).substring(2);
				if (!state.inventory.contains(item)) {
					System.out.println("| Added " + item + " to inventory (should already be in there)");
					itemsAdded++;
					state.inventory.add(item);
				}
			}
			if (itemsAdded == 0) {
				System.out.println("| Inventory matches inv output!");
			}
			state.initAttemptSet();
			System.out.println("| Starting with empty inventory so dropping everything.");
			System.out.println("| Corresponding gray code: " + StringUtils.repeat('0', state.inventory.size()));
			for (String item : state.inventory) {
				inputBuffer.add("drop " + item);
			}
			inputBuffer.add(state.attemptMove);
			outputLines.clear();
		} else {
			// not sure what to do here, plus I sometimes use inv as NOP
			System.out.println("| NOTHING TO DO");
		}
	}

	private void dumpOutput() {
		System.out.println("+-------OUTPUT RECEIVED:");
		for (String line : outputLines) {
			System.out.println("|" + line);
		}
	}

	private void dumpState() {
		System.out.println("+-------STATE OVERVIEW:");
		System.out.println("| In room '" + state.currentRoom + "' which has " + (state.currentRoom.getUnexploredDoor() == null ? "no " : "") + "unexplored exit(s)");
		System.out.println("| I came here from " + (state.previousRoom != null ? state.previousRoom : "outside") + " from " + (state.enteredRoomFrom() == null ? "a hole" : state.enteredRoomFrom()) + ".");
		System.out.print("| Exit(s) -");
		for (Point door : state.currentRoom.getExits()) {
			System.out.print(" " + DIR_TO_WORD.get(door) + (state.currentRoom.doorIsExplored(door) ? "" : " (unexplored)"));
		}
		System.out.println();
		System.out.println("| Item blacklist: " + state.itemBlacklist);
		System.out.println("| " + state.currentRoom.getState());
		System.out.println("| Remaining items to gather:");
		for (Map.Entry<String, Room> entry : state.itemsInWorld.entrySet()) {
			if (!state.itemBlacklist.contains(entry.getKey())) {
				System.out.println("|    - " + entry.getKey() + " in " + entry.getValue().name + (entry.getValue().equals(state.currentRoom) ? ", where we are" : ""));
			}
		}
		if (state.allItemsGathered()) {
			System.out.println("|    Done gathering!");
		} else {
			System.out.println("| Path to nearest room with items of interest: " + StringUtils.join(state.pathToNearestRoomOfInterest(room -> room.getState().contains("items")), ", "));
		}
		System.out.println("| Remaining doors to explore in the world:");
		for (Map.Entry<String, Set<Point>> entry : state.exploreTodoList.entrySet()) {
			System.out.println("|    - " + entry.getValue().size() + " " + entry.getValue() + " in " + entry.getKey() + (entry.getKey().equals(state.currentRoom.name) ? ", where we are" : ""));
		}
		if (state.isFullyExplored()) {
			System.out.println("|    Done exploring!");
		} else {
			System.out.println("| Path to nearest room with doors of interest: " + StringUtils.join(state.pathToNearestRoomOfInterest(room -> room.getState().contains("exits")), ", "));
		}
		if (state.allItemsGathered() && state.isFullyExplored()) {
			if (SECURITY_CHECKPOINT.equals(state.currentRoom.name)) {
				System.out.println("| We should be ready to try entering the airlock now");
			} else {
				System.out.println("| We should head to the " + SECURITY_CHECKPOINT + " now");
			}
		} else {
			System.out.println("| Path to nearest room of interest: " + StringUtils.join(state.pathToNearestRoomOfInterest(room -> room.getState().contains("interesting")), ", "));
		}
		System.out.println("+-------");
	}

	@Override
	public BigInteger getLastOutput() {
		return lastOutput; //
	}
}
