package com.nushae.aoc.aoc2019.intcode;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CyclicListInputProvider implements InputProvider {
	private int valuePointer;
	private ArrayList<BigInteger> values;

	public CyclicListInputProvider(List<BigInteger> values) {
		setValues(values);
		reset();
	}

	public CyclicListInputProvider(BigInteger... values) {
		this(Arrays.asList(values));
	}

	public CyclicListInputProvider(String valueString) { // for 'ascii enabled' intcode computers.
		values = new ArrayList<>();
		for (int i = 0; i < valueString.length(); i++) {
			values.add(BigInteger.valueOf(valueString.charAt(i)));
		}
		reset();
	}

	public void setValues(List<BigInteger> values) {
		this.values = new ArrayList<>();
		this.values.addAll(values);
		reset();
	}

	public void reset() {
		valuePointer = 0;
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		BigInteger result = values.get(valuePointer);
		valuePointer = (valuePointer + 1) % values.size();
		return result;
	}
}
