package com.nushae.aoc.aoc2019.intcode;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

/**
 * This OutputReceiver by default prints everything it receives to the console (so you can observe it)
 * Switch it off/on by calling printToConsole()
 */
public class Observer implements OutputReceiver {

	@Setter
	private boolean printToConsole = true;
	@Getter
	private int width = 0;
	@Getter
	private int height = 0;
	private int x = 0;
	private int y = 0;
	private char[][] view = new char[1000][1000 ]; // large enough
	char droidDir;
	int droidX;
	int droidY;
	BigInteger lastOutput;

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		lastOutput = output;
		if (output.compareTo(BigInteger.valueOf(255)) <= 0) {
			char p = (char) output.intValueExact();
			if (p == 10) { // newline
				if (printToConsole) System.out.println();
				width = Math.max(width, x + 1);
				y++;
				height = Math.max(height, y + 1);
				x = 0;
			} else {
				if (printToConsole) System.out.print(p);
				view[y][x] = p;
				if (p == '^' || p == 'v' || p == '>' || p == '<') {
					view[y][x] = '#';
					droidX = x;
					droidY = y;
					droidDir = p;
				}
				x++;
			}
		}
	}

	public long getAlignmentParameters() {
		long accum = 0;
		for (int y = 0; y < height - 2; y++) {
			for (int x = 0; x < width - 2; x++) {
				if (view[y][x] == '.' && view[y][x + 1] == '#' && view[y][x + 2] == '.' &&
						view[y + 1][x] == '#' && view[y + 1][x + 1] == '#' && view[y + 1][x + 2] == '#' &&
						view[y + 2][x] == '.' && view[y + 2][x + 1] == '#' && view[y + 2][x + 2] == '.'
				) {
					accum += (x + 1) * (y + 1);
				}
			}
		}
		return accum;
	}

	@Override
	public BigInteger getLastOutput() {
		return lastOutput;
	}
}
