package com.nushae.aoc.aoc2019.intcode;

import com.nushae.aoc.aoc2019.Exercise15;
import com.nushae.aoc.aoc2019.domain.Direction;

import javax.swing.*;
import java.awt.*;
import java.math.BigInteger;
import java.util.HashMap;

public class RepairDroid implements OutputReceiver, InputProvider {
	public Direction nextCommand;
	BigInteger myLastOutput;
	public Point position;
	public String route;
	public int exploredNeighbours;
	private IntCodeInterpreter interpreter;
	private HashMap<Point, BigInteger> maze;
	private boolean findOxy;
	private JPanel displayPanel;
	private int result;
	private Point oxyLoc;

	public RepairDroid(boolean findOxy, IntCodeInterpreter interpreterRef, HashMap<Point, BigInteger> mazeRef, JPanel displayPanel) {
		this.findOxy = findOxy;
		this.interpreter = interpreterRef;
		this.maze = mazeRef;
		this.displayPanel = displayPanel;
		route = "N"; // base value so we can backtrack onto the start square
		position = new Point(0, 0);
		nextCommand = Direction.NORTH;
		exploredNeighbours = 0;
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		return nextCommand.v;
	}

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		myLastOutput = output;
		if (position.x == 0 && position.y == 0 && exploredNeighbours >= 4) { // done
			interpreter.abort();
		}
		Point p = new Point(position.x + nextCommand.dx, position.y + nextCommand.dy);
		if (output.equals(Exercise15.WALL)) {
			maze.put(p, Exercise15.WALL);
			if (Math.abs(p.x) == 1 && p.y == 0 || p.x == 0 && Math.abs(p.y) == 1) {
				exploredNeighbours++;
			}
			nextCommand = nextCommand.rotR();
			while (nextCommand != Direction.NORTH && route.substring(route.length() - 1).equals(nextCommand.opp().s)) {
				nextCommand = nextCommand.rotR();
			}
			if (nextCommand == Direction.NORTH) { // tried every dir but backtracking, so backtrack
				nextCommand = Direction.ofLetter(route.substring(route.length() - 1)).opp();
			}
		} else {
			if (output.equals(Exercise15.OXY)) {
				maze.put(p, Exercise15.OXY);
				oxyLoc = p;
				result = route.length();
				if (findOxy) interpreter.abort();
			} else {
				maze.put(p, Exercise15.FLOOR);
			}
			if (Math.abs(p.x) == 1 && p.y == 0 || p.x == 0 && Math.abs(p.y) == 1) {
				exploredNeighbours++;
			}
			position = p;
			if (route.substring(route.length() - 1).equals(nextCommand.opp().s)) { // we're retracing our steps
				nextCommand = nextCommand.rotL();
				route = route.substring(0, route.length() - 1);
				while (nextCommand != Direction.NORTH && route.substring(route.length() - 1).equals(nextCommand.opp().s)) { // skip backtrack dir
					nextCommand = nextCommand.rotR();
				}
				if (nextCommand == Direction.NORTH) { // we tried all directions except backtracking
					nextCommand = Direction.ofLetter(route.substring(route.length() - 1)).opp();
				}
			} else {
				route += nextCommand.s;
				nextCommand = Direction.NORTH;
				while (maze.containsKey(new Point(position.x + nextCommand.dx, position.y + nextCommand.dy))) {
					nextCommand = nextCommand.rotR();
					if (nextCommand == Direction.NORTH) {
						nextCommand = Direction.ofLetter(route.substring(route.length() - 1)).opp();
						break;
					}
				}
			}
		}
		if (Exercise15.SPEED > 0) {
			displayPanel.repaint();
			try {
				Thread.sleep(Exercise15.SPEED);
			} catch (InterruptedException e) {
				// NOP
			}
		}
	}

	public void setFindOxy(boolean value) {
		findOxy = value;
	}

	public Point getOxyLocation() {
		return oxyLoc;
	}

	public int getResult() {
		return result;
	}

	@Override
	public BigInteger getLastOutput() {
		return myLastOutput;
	}
}
