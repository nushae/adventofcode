package com.nushae.aoc.aoc2019.intcode;

import java.math.BigInteger;

public class StringOutputReceiver extends SingleOutputReceiver {
	private StringBuffer myOutput = new StringBuffer();

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output)	{
		if (myOutput.length() > 0) {
			myOutput.append(",");
		}
		myLastOutput = output;
		myOutput.append(output);
	}

	public String getOutputs() {
		return myOutput.toString();
	}
}
