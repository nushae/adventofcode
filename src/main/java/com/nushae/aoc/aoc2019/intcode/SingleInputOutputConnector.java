package com.nushae.aoc.aoc2019.intcode;

import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class combines the InputProvider and OutputReceiver interfaces to form a connector between two interpreters
 * If any initialization values were provided, these will first be provided as input, one by one,
 * _regardless_ of whether any output has been received.
 * If the initialization values are exhausted, but no output has been received yet, the provided input will be 0.
 * Once output has been received and initialization values have been exhausted, the last received output
 * will be provided as input.
 */
@Log4j2
public class SingleInputOutputConnector implements InputProvider, OutputReceiver {

	private ArrayList<BigInteger> initialOutputs;
	private int outputCounter;
	private BigInteger myInputOutputValue;
	private boolean outputReceived;
	private String myID;

	private static int created = 0;

	public SingleInputOutputConnector(BigInteger... initialOutputs) {
		this();
		this.initialOutputs.addAll(Arrays.asList(initialOutputs));
	}

	public SingleInputOutputConnector() {
		myID = "SIOC" + created++;
		outputReceived = false;
		this.initialOutputs = new ArrayList<>();
		outputCounter = 0;
	}

	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		outputReceived = true;
		myInputOutputValue = output;
		log.debug("["+ myID + "] received " + output + " as output");
	}

	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		BigInteger result;
		if (outputCounter < initialOutputs.size()) {
			result = initialOutputs.get(outputCounter++);
		} else {
			if (!outputReceived) {
				result = BigInteger.ZERO;
			} else {
				result = myInputOutputValue;
			}
		}
		log.debug("["+ myID + "] provided " + result + " as input");
		return result;
	}

	public BigInteger getLastOutput() {
		if (!outputReceived) {
			throw new IllegalStateException("Output requested before OutputReceiver received any output");
		}
		return myInputOutputValue;
	}
}
