package com.nushae.aoc.aoc2019.intcode;

import java.awt.*;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class ArcadeGame implements OutputReceiver, InputProvider {

	public static final int EMPTY = 0;
	public static final int WALL = 1;
	public static final int BLOCK = 2;
	public static final int PADDLE = 3;
	public static final int BALL = 4;

	private HashMap<Point, Integer> screen;
	public int minX, minY, maxX, maxY;
	public int inputParity;
	public int[] values;
	int ballX;
	int paddleX;
	int blocks;
	private long score;

	public ArcadeGame() {
		screen = new HashMap<>();
		inputParity = 0;
		values = new int[3];
		minX = Integer.MAX_VALUE;
		minY = Integer.MAX_VALUE;
		maxX = Integer.MIN_VALUE;
		maxY = Integer.MIN_VALUE;
		ballX = 0;
		paddleX = 0;
		score = 0;
		blocks = 0;
	}

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		values[inputParity] = output.intValueExact();
		inputParity = (inputParity + 1) % 3;
		if (inputParity == 0) { // one full input received
			int x = values[0];
			int y = values[1];
			int val = values[2];
			if (x == -1 && y == 0) { // score update
				score = val;
			} else {
				screen.put(new Point(x, y), val);
				minX = Math.min(minX, x);
				maxX = Math.max(maxX, x);
				minY = Math.min(minY, y);
				maxY = Math.max(maxY, y);
				if (val == BALL) {
					ballX = x;
				} else if (val == PADDLE) {
					paddleX = x;
				} else if (val == BLOCK) {
					blocks++;
				}
			}
		}
	}

	@Override
	public BigInteger getLastOutput() {
		return null; // not used
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		if (paddleX < ballX) {
			return BigInteger.ONE;
		}
		if (ballX < paddleX) {
			return BigInteger.ONE.negate();
		}
		return BigInteger.ZERO; // neutral position
	}

	public long getBlocks() {
		return blocks;
	}

	public long getScore() {
		return score;
	}

	@Override
	public String toString() {
		String[][] paintJob = new String[maxX - minX + 1][maxY - minY + 1];
		for (Map.Entry<Point, Integer> panel : screen.entrySet()) {
			paintJob[panel.getKey().x - minX][panel.getKey().y - minY] = panel.getValue() != 0 ? "" + panel.getValue() : " ";
		}
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < paintJob[0].length; y++) {
			for (int x = 0; x < paintJob.length; x++) {
				if (paintJob[x][y] == null) {
					paintJob[x][y] = " ";
				}
				sb.append(paintJob[x][y]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
