package com.nushae.aoc.aoc2019.intcode;

import java.awt.*;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.MathUtil.*;

public class Robot implements OutputReceiver, InputProvider {

	private final BigInteger BLACK = BigInteger.ZERO;
	private final BigInteger WHITE = BigInteger.ONE;

	public HashMap<Point, BigInteger> paintedPanels;
	private Point location;
	private Point direction;
	private int inputParity;
	private BigInteger paint;
	private BigInteger turn;
	public int minX, minY, maxX, maxY;

	public Robot() {
		paintedPanels = new HashMap<>();
		location = new Point(0, 0); // center of a theoretically unbounded plane
		direction = new Point(0, -1); // facing up
		inputParity = 0;
	}

	@Override
	public void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output) {
		if (inputParity == 0) {
			paint = output;
		} else {
			turn = output;
			paintedPanels.put(location, paint);
			if (BigInteger.ZERO.equals(turn)) {
				direction = rot90L(1, direction);
			} else {
				direction = rot90R(1, direction);
			}
			location = sum(location, direction);
			minX = Math.min(minX, location.x);
			maxX = Math.max(maxX, location.x);
			minY = Math.min(minY, location.y);
			maxY = Math.max(maxY, location.y);
		}
		inputParity = 1 - inputParity;
	}

	@Override
	public BigInteger getLastOutput() {
		return null;
	}

	@Override
	public BigInteger nextValue(BigInteger interpreterNetworkAddress) {
		if (paintedPanels.size() == 0) {
			return WHITE;
		}
		if (paintedPanels.containsKey(location)) {
			return paintedPanels.get(location);
		}
		return BLACK;
	}

	@Override
	public String toString() {
		String[][] paintJob = new String[maxX - minX + 1][maxY - minY + 1];
		for (Map.Entry<Point, BigInteger> panel : paintedPanels.entrySet()) {
			paintJob[panel.getKey().x - minX][panel.getKey().y - minY] = panel.getValue().equals(WHITE) ? "#" : " ";
		}
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < paintJob[0].length; y++) {
			for (int x = 0; x < paintJob.length; x++) {
				if (paintJob[x][y] == null) {
					paintJob[x][y] = " ";
				}
				sb.append(paintJob[x][y]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
