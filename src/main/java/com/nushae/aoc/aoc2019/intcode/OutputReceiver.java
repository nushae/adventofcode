package com.nushae.aoc.aoc2019.intcode;

import java.math.BigInteger;

public interface OutputReceiver {
	void acceptOutput(BigInteger interpreterNetworkAddress, BigInteger output);
	BigInteger getLastOutput();
}
