package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.KeyMazeNode;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import com.nushae.aoc.domain.SearchNode;
import com.nushae.aoc.util.SearchUtil;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.GraphUtil.*;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 18, title="Many-Worlds Interpretation", keywords = {"grid", "maze", "graph", "subgraph", "pathing", "visualized"})
public class Exercise18 extends JPanel {

	private static final boolean VISUAL = true;

	ArrayList<String> lines;
	int height;
	int width;
	Set<Point> startingLocations;
	Character[][] grid;
	Point[][] locations;
	Point[] crossings;
	Map<String, List<? extends GraphNode<Character>>> paths;
	BasicGraphNode<Character> start;
	Map<Point, Map<Point, String>> distanceMatrix;
	Set<Character> letters;
	String keyLetters;
	String doorLetters;
	boolean show = false;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	// Map characters and their meaning:
	// .      empty space
	// #      wall
	// @      starting location
	// 0-3    'helper' waypoints (manually added) to help with simplifying pathing
	// a-z    key
	// A-Z    door
	private void processInput() {
		// input appears to always be rectangular, so take dimensions from list length and first line
		width = lines.get(0).length();
		height = lines.size();
		startingLocations = new HashSet<>();
		grid = new Character[width][height];
		locations = new Point[26][2];
		crossings = new Point[4];
		letters = new HashSet<>();
		Set<Character> keys = new HashSet<>();
		Set<Character> doors = new HashSet<>();
		for (int row = 0; row < lines.size(); row++) {
			String line = lines.get(row);
			for (int col = 0; col<line.length(); col++) {
				char c = line.charAt(col);
				grid[col][row] = c;
				if (c != '.' && c != '#' && (c < 'A' || c > 'Z')) { // ignore doors too
					letters.add(c);
				}
				if (c == '@') { // starting position
					startingLocations.add(new Point(col, row));
				} else if (c >= 'a' && c <= 'z') { // key
					locations[c-'a'][0] = new Point(col, row);
					keys.add(c);
					log.debug("Key " + c + " at " + col + ", " + row);
				} else if (c >= 'A' && c <= 'Z') { // door
					locations[c-'A'][1] = new Point(col, row);
					doors.add(c);
					log.debug("Door " + c + " at " + col + ", " + row);
				} else if (c >= '0' && c <= '3') { // crossings
					crossings[c - '0'] = new Point(col, row);
					log.debug("Crossing " + c + " at " + col + ", " + row);
				}
			}
		}
		keyLetters = keys.stream().sorted().map(Object::toString).collect(Collectors.joining());
		log.debug("Found these keys: " + keyLetters);
		doorLetters = doors.stream().sorted().map(Object::toString).collect(Collectors.joining());
		log.debug("Found these doors: " + doorLetters);

		paths = new HashMap<>();
		// first make a graph from the grid, so we can determine distances in the grid between 'adjacent' keys (lowercase letters)
		for (Point p : startingLocations) {
			start = gridToGraph(grid, Collections.singleton('#'), p);
			// now build the 'real' graph, connecting only those keys (lowercase letters) that have no other
			// keys between them (ie generate paths that have no other keys on them). We remember the doors (uppercase
			// letters) along the way, because they affect the 'distance' between keys.
			for (char from : letters) {
				BasicGraphNode<Character> startNode;
				if (from == '@') {
					startNode = start;
				} else if (from >= '0' && from <= '3') {
					startNode = findGraphNodeWithLocation(start, crossings[from - '0']);
				} else { // 'a' <= from && from <= 'z'
					startNode = findGraphNodeWithLocation(start, locations[from - 'a'][0]);
				}
				if (startNode != null) {
					for (char target : letters) {
						if (from != target) {
							// in theory there are multiple paths to a specific location, but the input is a tree-like maze,
							// so we can get away with finding THE path.
							List<? extends GraphNode<Character>> path = findShortestPath(startNode, c -> c.getPayload() == target);
							if (!path.isEmpty()) {
								// filter out paths that have intermediate lowercase letters (including '@', the start node)
								boolean adjacent = true;
								StringBuilder dsb = new StringBuilder();
								for (int i = 1; adjacent && i < path.size() - 1; i++) {
									char c = path.get(i).getPayload();
									adjacent = c != '@' && (c < 'a' || c > 'z');
									if (c >= 'A' && c <= 'Z') {
										// remember doors along the way
										dsb.append(c);
									}
								}
								if (adjacent) {
									paths.put("" + from + target + dsb, path);
								}
							}
						}
					}
				}
			}
		}

		// ([3, 7] -> ([10, 23] -> "ABF;123"))
		distanceMatrix = new HashMap<>();

		for (Map.Entry<String, List<? extends GraphNode<Character>>> path : paths.entrySet()) {
			// key: from / to / doors
			String doorsInBetween = path.getKey().substring(2);
			// value: complete path from 'from' to 'to' in the grid; we need only its length and the coordinates of the endpoints
			Point fromLocation = ((BasicGraphNode<Character>) path.getValue().get(0)).getLocation();
			Point toLocation = ((BasicGraphNode<Character>) path.getValue().get(path.getValue().size()-1)).getLocation();
			int pathLen = path.getValue().size()-1;
			distanceMatrix.computeIfAbsent(fromLocation, k -> new HashMap<>()).put(toLocation, doorsInBetween + ";" + pathLen);
			distanceMatrix.computeIfAbsent(toLocation, k -> new HashMap<>()).put(fromLocation, doorsInBetween + ";" + pathLen);
		}
		show = true;
		repaint();
	}

	public long doPart1() {
		processInput();

		KeyMazeNode startNode = new KeyMazeNode(startingLocations, '@', new HashSet<>(), grid, distanceMatrix);

		SearchNode goal = SearchUtil.dsp(startNode, n -> keyLetters.equals(n.getState().substring(n.getState().indexOf('/')+1)));
		long result = 0;
		while (goal != null) {
			Set<Point> locs1 = ((KeyMazeNode) goal).getLocations();
			goal = goal.getParent();
			if (goal != null) {
				Set<Point> locs0 = ((KeyMazeNode) goal).getLocations();
				Point loc0 = new Point(-1, -1);
				Point loc1 = new Point(-1, -1);
				for (Point p : locs0) {
					if (!locs1.contains(p)) {
						loc0 = p;
						break;
					}
				}
				for (Point p : locs1) {
					if (!locs0.contains(p)) {
						loc1 = p;
						break;
					}
				}
				result += Long.parseLong(distanceMatrix.get(loc0).get(loc1).split(";")[1]);
			}
		}
		return result;
	}

	public long doPart2() {
		processInput();

		KeyMazeNode startNode = new KeyMazeNode(startingLocations, '@', new HashSet<>(), grid, distanceMatrix);

		SearchNode goal = SearchUtil.dsp(startNode, n -> keyLetters.equals(n.getState().substring(n.getState().indexOf('/')+1)));
		long result = 0;
		while (goal != null) {
			Set<Point> locs1 = ((KeyMazeNode) goal).getLocations();
			goal = goal.getParent();
			if (goal != null) {
				Set<Point> locs0 = ((KeyMazeNode) goal).getLocations();
				Point loc0 = new Point(-1, -1);
				Point loc1 = new Point(-1, -1);
				for (Point p : locs0) {
					if (!locs1.contains(p)) {
						loc0 = p;
						break;
					}
				}
				for (Point p : locs1) {
					if (!locs0.contains(p)) {
						loc1 = p;
						break;
					}
				}
				result += Long.parseLong(distanceMatrix.get(loc0).get(loc1).split(";")[1]);
			}
		}
		return result;
	}

	@Override
	public void paintComponent(Graphics g) {
		if (show) {
			final int SIZE = Math.min(getWidth() / width, getHeight() / 2 / height);
			Font FONT = new Font("Arial", Font.PLAIN, SIZE / 2);
			Font FONT2 = new Font("Arial", Font.PLAIN, 3 * SIZE / 4);
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.white);
			g2.fillRect(0, 0, getWidth(), getHeight());
			int xOGrid = getWidth() / 2 - SIZE / 2 - width * SIZE / 2;
			int yOGrid = getHeight() / 2 - SIZE / 2 - height * SIZE;
			int xOLetter = getWidth() / 2 - SIZE / 2 - width * SIZE / 2;
			int yOLetter = getHeight() / 2 - SIZE / 2;

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					char c = grid[x][y];
					if (c == '.') {
						g2.setColor(Color.lightGray);
						// g2.setColor(Color.white);
					} else if (c == '#') {
						g2.setColor(Color.black);
						// g2.setColor(Color.white);
					} else if (c == '@') {
						g2.setColor(Color.blue);
					} else if (c >= 'a' && c <= 'z') {
						g2.setColor(new Color(100, 255, 200));
					} else if (c >= 'A' && c <= 'Z') {
						g2.setColor(new Color(255, 175, 175));
					}
					g2.fillRect(xOGrid + x * SIZE, yOGrid + y * SIZE, SIZE - 1, SIZE - 1);
					if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') {
						g2.setColor(Color.black);
						drawCenteredString(g2, "" + c, new Rectangle(xOGrid + x * SIZE, yOGrid + y * SIZE, SIZE - 1, SIZE - 1), FONT);
					}
				}
			}
			g2.setStroke(new BasicStroke(Math.max(1, SIZE / 10)));
			for (Map.Entry<String, List<? extends GraphNode<Character>>> pathEntry : paths.entrySet()) {
				List<? extends GraphNode<Character>> path = pathEntry.getValue();
				Point start = ((BasicGraphNode<Character>) path.get(0)).getLocation();
				Point end = ((BasicGraphNode<Character>) path.get(path.size() - 1)).getLocation();
				long pathLen = path.size() - 1;
				g2.setColor(Color.gray);
				g2.drawLine(
						xOLetter + start.x * SIZE + SIZE / 2,
						yOLetter + start.y * SIZE + SIZE / 2,
						xOLetter + end.x * SIZE + SIZE / 2,
						yOLetter + end.y * SIZE + SIZE / 2);
				g2.setColor(Color.white);
				g2.fillOval(xOLetter + start.x * SIZE + SIZE / 2 - SIZE / 3, yOLetter + start.y * SIZE + SIZE / 2 - SIZE / 3, 2 * SIZE / 3, 2 * SIZE / 3);
				g2.fillOval(xOLetter + end.x * SIZE + SIZE / 2 - SIZE / 3, yOLetter + end.y * SIZE + SIZE / 2 - SIZE / 3, 2 * SIZE / 3, 2 * SIZE / 3);
				g2.setColor(Color.gray);
				g2.drawOval(xOLetter + start.x * SIZE + SIZE / 2 - SIZE / 3, yOLetter + start.y * SIZE + SIZE / 2 - SIZE / 3, 2 * SIZE / 3, 2 * SIZE / 3);
				g2.drawOval(xOLetter + end.x * SIZE + SIZE / 2 - SIZE / 3, yOLetter + end.y * SIZE + SIZE / 2 - SIZE / 3, 2 * SIZE / 3, 2 * SIZE / 3);
				g2.setColor(Color.black);
				drawCenteredString(g2, "" + grid[start.x][start.y], new Rectangle(xOLetter + start.x * SIZE, yOLetter + start.y * SIZE, SIZE - 1, SIZE - 1), FONT);
				drawCenteredString(g2, "" + grid[end.x][end.y], new Rectangle(xOLetter + end.x * SIZE, yOLetter + end.y * SIZE, SIZE - 1, SIZE - 1), FONT);
				g2.setColor(Color.red);
				drawCenteredString(
						g2,
						pathLen + (null != distanceMatrix ? distanceMatrix.getOrDefault(start, new HashMap<>()).getOrDefault(end, ";").split(";")[0] : ""),
						new Rectangle(xOLetter + start.x * SIZE, yOLetter + start.y * SIZE, (end.x - start.x + 1) * SIZE, (end.y - start.y + 1) * SIZE),
						FONT2);
			}
		}
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		// For this exercise, the input has been manually adjusted for part 2
		Exercise18 ex1 = new Exercise18(aocPath(2019, 18, "input1.txt"));
		Exercise18 ex2 = new Exercise18(aocPath(2019, 18, "input2.txt"));

		if (VISUAL) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex1));
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex2));
		}

		// part 1 and 2 each call processInput to reset state, times are included in runtime for the part

		// part 1 - 7048 (4.5 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex1.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1836 (139 sec - look I'm just happy it gets the answer ok)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex2.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
