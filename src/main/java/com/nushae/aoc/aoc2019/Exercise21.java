package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.CyclicListInputProvider;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.Observer;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 21, title="Springdroid Adventure", keywords = {"intcode", "jumping", "boolean", "lookahead"})
public class Exercise21 {

	private final IntCodeInterpreter interpreter;
	private Observer output;
	private CyclicListInputProvider input;

	public Exercise21() {
		interpreter = new IntCodeInterpreter();
	}

	public Exercise21(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise21(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	// We need a jump program that does the following:
	// "provided there is floor where we would land, and there is a hole somewhere closer, jump"
	// in other words, (A == hole || B == hole || C == hole) && D == floor
	// or in jump code:
	// NOT A J
	// NOT B T
	// OR T J
	// NOT C T
	// OR T J
	// AND D J
	// ... SUCCESS!
	public BigInteger doPart1() {
		output = new Observer();
		output.setPrintToConsole(false);
		input = new CyclicListInputProvider("NOT A J\n" +
				"NOT B T\n" +
				"OR T J\n" +
				"NOT C T\n" +
				"OR T J\n" +
				"AND D J\n" +
				"WALK\n");
		interpreter.setOutputReceiver(output);
		interpreter.setInputProvider(input);
		interpreter.reRunProgram();
		return output.getLastOutput();
	}

	// As expected now we can look further than we can jump. Of course the program for part 1 is limited - we could
	// in theory jump to a place we can't continue from. Now we can run, and indeed it turns we now get stuck here with
	// the program from part 1:
	// .................
	// .................
	// ..@..............
	// #####.#.##.######
	//    ABCDEFGHI
	// The droid jumps to D but then can't jump again; so a floor spot D with a hole behind it (E)
	// AND a hole 3 down from it (H), is not a useful landing spot, and we must wait for a better one.
	// In other words: (NOT A || NOT B || NOT C ) && D && NOT (NOT E && NOT H)
	// In order to be able to calculate this sequentially, we turn this into:
	// (NOT A || NOT B || NOT C ) && D && (E || H)
	// Since we can't directly copy a register to another, we use NOT twice:
	// (NOT A || NOT B || NOT C ) && D && (NOT NOT E || H)
	// We arrive at:
	//  instr        J                                             T
	// -------     -----                                         -----
	// NOT A J     NOT A                                           -
	// NOT B T     NOT A                                         NOT B
	// OR T J      NOT A || NOT B                                NOT B
	// NOT C T     NOT A || NOT B                                NOT C
	// OR T J      NOT A || NOT B || NOT C                       NOT C
	// AND D J     (NOT A || NOT B || NOT C) && D                NOT C
	// NOT E T     (NOT A || NOT B || NOT C) && D                NOT E
	// NOT T T     (NOT A || NOT B || NOT C) && D                E
	// OR H T      (NOT A || NOT B || NOT C) && D                E || H
	// AND T J     (NOT A || NOT B || NOT C) && D && (E || H)    E || H
	// RUN
	public BigInteger doPart2() {
		output = new Observer();
		output.setPrintToConsole(false);
		input = new CyclicListInputProvider("NOT A J\n" +
				"NOT B T\n" +
				"OR T J\n" +
				"NOT C T\n" +
				"OR T J\n" +
				"AND D J\n" +
				"NOT E T\n" +
				"NOT T T\n" +
				"OR H T\n" +
				"AND T J\n" +
				"RUN\n");
		interpreter.setOutputReceiver(output);
		interpreter.setInputProvider(input);
		interpreter.reRunProgram();
		return output.getLastOutput();
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2019, 21));

		// processInput not needed

		// part 1 - 19358870 (267ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1143356492 (2.403s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
