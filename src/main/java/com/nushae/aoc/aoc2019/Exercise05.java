package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.CyclicListInputProvider;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 5, title="Sunny with a Chance of Asteroids", keywords = {"intcode", "input-output"})
public class Exercise05 {
	private IntCodeInterpreter interpreter;

	private Exercise05() {
		interpreter = new IntCodeInterpreter();
	}

	public Exercise05(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise05(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public void runProgram(int singleInputValue) {
		interpreter.setInputProvider(new CyclicListInputProvider(BigInteger.valueOf(singleInputValue)));
		interpreter.resetState();
		interpreter.reRunProgram();
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2019, 5));

		// processInput not needed

		// part 1 - 9775037 (12ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		ex.runProgram(1); // no OutputReceivers yet!
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 15586959 (6ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		ex.runProgram(5); // no OutputReceivers yet!
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
