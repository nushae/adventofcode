package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.DonutNode;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.GraphUtil.findShortestPath;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

@Log4j2
@Aoc(year = 2019, day = 20, title="Donut Maze", keywords = {"grid", "maze", "pathing", "nesting", "visualized"})
public class Exercise20 {

	private static final boolean REPORT = false;

	// 'sufficiently long' list of labels for the gates. @ is reserved for AA, & is reserved for ZZ.
	private static final char[] LABELS = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

	List<String> lines;
	Character[][] grid;
	int width;
	int height;
	Map<String, Character> labelMap;
	Map<Character, String> gateMap;
	Map<Character, List<Point>> gateLocations;

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		width = lines.get(0).length();
		height = lines.size();
		// we add a margin of empty cells around the torus to make processing easier
		grid = new Character[width+2][height+2];
		for (Character[] row : grid) {
			Arrays.fill(row, ' ');
		}
		for (int y = 0; y < height; y++) {
			String line = lines.get(y);
			for (int x = 0; x < width; x++) {
				grid[x+1][y+1] = line.charAt(x);
			}
		}
		labelMap = new HashMap<>();
		gateMap = new HashMap<>();
		gateLocations = new HashMap<>();
		int letter = 0;
		for (int x = 1; x < width+1; x++) {
			for (int y = 1; y < height+1; y++) {
				if (grid[x][y] >= 'A' && grid[x][y] <= 'Z') {
					// Modify the map so the labels are in situ on the grid.
					// We inventory the labels and assign them a 1 - char key and put the label in a map for lookup.
					// the 1 char key is put in the map on the two empty spots next to the two occurrences of the label
					// (AA and ZZ only occur once each)
					// We only want to process if we have the first letter of the pair (so the top or left one)
					// We can't just check for adjacent letters because we might have two labels side by side.
					// So, we check for the '.' that MUST be either in front of or behind the label.
					int modX = -1;
					int modY = -1;
					String label = null;
					if (grid[x+1][y] >= 'A' && grid[x+1][y] <= 'Z' && (grid[x-1][y] == '.' || grid[x+2][y] == '.'))	{ // horizontal label
						label = "" + grid[x][y] + grid[x+1][y];
						modX = (grid[x-1][y] == '.' ? x-1 : x+2);
						modY = y;
					} else if (grid[x][y+1] >= 'A' && grid[x][y+1] <= 'Z' && (grid[x][y-1] == '.' || grid[x][y+2] == '.')) { // vertical label
						label = "" + grid[x][y] + grid[x][y+1];
						modX = x;
						modY = (grid[x][y-1] == '.' ? y-1 : y+2);
					} // all other cases: second letter of the label
					if (label != null) {
						char key;
						if (!labelMap.containsKey(label)) { // first time we encounter this label, add it
							if ("AA".equals(label)) {
								key = '@';
							} else if ("ZZ".equals(label)) {
								key = '&';
							} else {
								key = LABELS[letter++];
							}
							labelMap.put(label, key);
							gateMap.put(key, label);
						} else {
							key = labelMap.get(label);
						}
						grid[modX][modY] = labelMap.get(label);
						List<Point> locs = gateLocations.getOrDefault(key, new ArrayList<>());
						locs.add(new Point(modX, modY));
						gateLocations.put(key, locs);
					}
				}
			}
		}
		if (REPORT) {
			System.out.println(width + "x" + height + " with " + (gateMap.size() - 2) + " gates besides AA (in) and ZZ (out).");
			gateMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(e -> System.out.println(e.getKey() + " = " + e.getValue()));
			for (int y = 1; y < height + 1; y++) {
				for (int x = 1; x < width + 1; x++) {
					System.out.print(grid[x][y]);
				}
				System.out.println();
			}
		}
	}

	public long doPart1() {
		processInput();
		BasicGraphNode<Character> start = gridToWarpGraph(grid, '@', new HashSet<>(Arrays.asList('#', ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')), gateLocations);
		List<? extends GraphNode<Character>> path = findShortestPath(start, c -> c.getPayload() == '&');
		return path.size()-1;
	}

	// RECURSIVE?!
	// I'll need a lazy evaluation solution... -_- I already wanted to make my dijkstra algorithm capable of
	// plugging in dynamic neighbour generation... I guess now is the time at last...
	public long doPart2() {
		processInput();
		DonutNode start = new DonutNode("", 0, gateLocations.get('@').get(0), "@0", grid, gateLocations);
		List<? extends GraphNode<String>> path = findShortestPath(start, c -> "&0".equals(c.getPayload()));
		return path.size()-1;
	}

	private BasicGraphNode<Character> gridToWarpGraph(Character[][] grid, Character rootPayload, Set<Character> verboten, Map<Character, List<Point>> gateLocations) {
		int width = grid.length;
		int height = grid[0].length;
		int rootX = 0;
		int rootY = 0;
		Map<Point, BasicGraphNode<Character>> nodes = new HashMap<>();
		for (int x = 0; x < width; x++) {
			for (int y=0; y < height; y++) {
				if (null != rootPayload && grid[x][y] == rootPayload) {
					rootX = x;
					rootY = y;
				}
				if (!verboten.contains(grid[x][y])) {
					Point coords = new Point(x, y);
					BasicGraphNode<Character> node = new BasicGraphNode<>("", coords, grid[x][y]);
					nodes.put(coords, node);
					for (Point dir : ORTHO_DIRS) {
						Point neighbour = new Point(coords.x + dir.x, coords.y + dir.y);
						if (nodes.containsKey(neighbour)) {
							node.addNeighbour(nodes.get(neighbour), 1);
							nodes.get(neighbour).addNeighbour(node, 1);
						}
					}
					if (gateLocations.containsKey(grid[x][y])) {
						List<Point> locations = gateLocations.get(grid[x][y]);
						for (Point otherGate : locations) {
							if (!otherGate.equals(coords) && nodes.containsKey(otherGate)) {
								node.addNeighbour(nodes.get(otherGate), 1);
								nodes.get(otherGate).addNeighbour(node, 1);
							}
						}
					}
				}
			}
		}
		return nodes.get(new Point(rootX, rootY));
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2019, 20));

		// part 1 and 2 each call processInput to reset state, times are included in runtime for the part

		// part 1 - 684 (300ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 7758 (14.2s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
