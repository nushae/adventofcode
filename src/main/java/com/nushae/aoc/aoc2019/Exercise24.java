package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

@Log4j2
@Aoc(year = 2019, day = 24, title="Planet of Discord", keywords = {"grid", "cellular-automaton", "nesting"})
public class Exercise24 {

	List<String> lines;
	boolean[][] grid; // for part 1
	List<boolean[][]> grids; // for part 2
	int level; // the level of the first item in grids -> level of grid(i) = level+i

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		grid = new boolean[5][5];
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				grid[j][i] = line.charAt(j) == '#';
			}
		}
		grids = new ArrayList<>();
		grids.add(grid);
		level = 0;
	}

	public long doPart1() {
		processInput();
		Set<Long> seen = new TreeSet<>();
		seen.add(codify(grid));
		while (true) {
			grid = doGeneration(grid);
			long newGrid = codify(grid);
			if (seen.contains(newGrid)) {
				return (newGrid);
			}
			seen.add(newGrid);
		}
	}

	/**
	 * Uh yeah. After many 'underwhelming part 2 twists' this one definitely is a doozy.
	 * Extension:
	 * 1) we now maintain a list of grids, innermost to outermost, and 'lazy extend' it with new grids when bugs 'spill over':
	 *    - whenever we 'look across a level border' we DON'T create a new level if it's absent but just assume 'no bug'
	 *    - in the lowest and highest level of the stack, we explicitly write across the 'level border' to create
	 *      a new level; but only if any new bug is created there
	 * 2) all cells still have 4 neighbours, except for the four around the center (they have more than 4 neighbours)
	 *    - for the 4 cells around the center we need to check 5 neighbours in the inner level (total 8)
	 *    - for the 16 cells around the outer perimeter of a level, we need to find 1 or 2 neighbours in the outer
	 *      level (total 4)
	 * 3) we divide the generation loop into three sections: outer rim, cells around the center, and rest of the level
	 *
	 * @return total bugs in system
	 */
	public long doPart2() {
		processInput();
		for (int gen = 0; gen < 200; gen++) {
			grids = donutGeneration(grids);
		}
		long result = 0;
		for (boolean[][] level : grids) {
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					result += level[i][j] ? 1 : 0;
				}
			}
		}
		return result;
	}

	// the simple version of the generation function, for part 1
	public static boolean[][] doGeneration(boolean[][] grid) {
		boolean[][] result = new boolean[5][5];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				int bugs = 0;
				for (Point dir : ORTHO_DIRS) {
					int newI = i + dir.x;
					int newJ = j + dir.y;
					if (0 <= newI && newI < 5 && 0 <= newJ && newJ < 5 && grid[newI][newJ]) {
						bugs++;
					}
				}
				if (grid[i][j] && bugs != 1) {
					result[i][j] = false;
				} else if (!grid[i][j] && (bugs == 1 || bugs == 2)) {
					result[i][j] = true;
				} else {
					result[i][j] = grid[i][j];
				}
			}
		}
		return result;
	}

	// the generation function for the recursive donuts is a TAD more complex...
	public static List<boolean[][]> donutGeneration(List<boolean[][]> grids) {
		List<boolean[][]> newGrids = new ArrayList<>();
		// check for spillover into a new lowest level from the lowest level here;
		// if, for example, cell 2,1 is filled in level 'lowest' then all cells in row 0 will
		// become filled in level 'lowest-1', same for 1,2 and the column 0, etc
		boolean[][] currentGrid = grids.get(0);
		if (currentGrid[2][1] || currentGrid[1][2] || currentGrid[2][3] || currentGrid[3][2]) { // need new level
			boolean[][] newGrid = new boolean[5][5];
			if (currentGrid[2][1]) {
				for (int i = 0; i < 5; i++) {
					newGrid[i][0] = true;
				}
			}
			if (currentGrid[1][2]) {
				for (int i = 0; i < 5; i++) {
					newGrid[0][i] = true;
				}
			}
			if (currentGrid[2][3]) {
				for (int i = 0; i < 5; i++) {
					newGrid[i][4] = true;
				}
			}
			if (currentGrid[3][2]) {
				for (int i = 0; i < 5; i++) {
					newGrid[4][i] = true;
				}
			}
			newGrids.add(newGrid);
		}

		// now do the existing levels (no spillover, but we do look in neighbouring levels)
		for (int g = 0; g < grids.size(); g++) {
			currentGrid = grids.get(g);
			boolean[][] newGrid = new boolean[5][5];
			boolean[][] outerGrid = (g < grids.size()-1 ? grids.get(g+1) : new boolean[5][5]);
			boolean[][] innerGrid = (g > 0 ? grids.get(g-1) : new boolean[5][5]);
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					if (i!=2 || j!=2) { // skip central, nonexistent cell so it stays empty
						int bugs = 0;
						for (Point dir : ORTHO_DIRS) {
							int newI = i + dir.x;
							int newJ = j + dir.y;
							if (newI < 0) { // look in outerGrid cell 1,2
								bugs += outerGrid[1][2] ? 1 : 0;
							} else if (newI > 4) { // look in outerGrid cell 3,2
								bugs += outerGrid[3][2] ? 1 : 0;
							} else if (newJ < 0) { // look in outerGrid cell 2,1
								bugs += outerGrid[2][1] ? 1 : 0;
							} else if (newJ > 4) { // look in outerGrid cell 2,3
								bugs += outerGrid[2][3] ? 1 : 0;
							} else if (newI == 2 && newJ == 2) { // look in inner level, depends on where i/j are
								if (i == 1) { // inner grid, column 0
									for (int q = 0; q < 5; q++) {
										bugs += innerGrid[0][q] ? 1 : 0;
									}
								} else if (i == 3) { // inner grid, column 4
									for (int q = 0; q < 5; q++) {
										bugs += innerGrid[4][q] ? 1 : 0;
									}
								} else if (j == 1) { // inner grid, row 0
									for (int q = 0; q < 5; q++) {
										bugs += innerGrid[q][0] ? 1 : 0;
									}
								} else if (j == 3) { // inner grid, row 4
									for (int q = 0; q < 5; q++) {
										bugs += innerGrid[q][4] ? 1 : 0;
									}
								}
							} else { // normal behavior
								bugs += currentGrid[newI][newJ] ? 1 : 0;
							}
						}
						if (currentGrid[i][j] && bugs != 1) {
							newGrid[i][j] = false;
						} else if (!currentGrid[i][j] && (bugs == 1 || bugs == 2)) {
							newGrid[i][j] = true;
						} else {
							newGrid[i][j] = currentGrid[i][j];
						}
					}
				}
			}
			newGrids.add(newGrid);
		}

		// finally, check for spillover into the next higher level from the top level of the stack
		int north = 0;
		int east = 0;
		int south = 0;
		int west = 0;
		for (int q=0; q<5; q++) {
			north += currentGrid[q][0] ? 1 : 0;
			east += currentGrid[4][q] ? 1 : 0;
			south += currentGrid[q][4] ? 1 : 0;
			west += currentGrid[0][q] ? 1 : 0;
		}
		if (north > 0 && north < 3 || east > 0 && east < 3 || south > 0 && south < 3 || west > 0 && west < 3) {
			boolean[][] newGrid = new boolean[5][5];
			newGrid[2][1] |= north > 0 && north < 3;
			newGrid[1][2] |= west > 0 && west < 3;
			newGrid[2][3] |= south > 0 && south < 3;
			newGrid[3][2] |= east > 0 && east < 3;
			newGrids.add(newGrid);
		}
		return newGrids;
	}

	public long codify(boolean[][] grid) {
		long bit = 1;
		long total = 0;
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 5; i++) {
				if (grid[i][j]) {
					total += bit;
				}
				bit *= 2;
			}
		}
		return total;
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2019, 24));

		// part 1 and 2 each call processInput to reset state, times are included in runtime for the part

		// part 1 - 3186366 (363ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2031 (63ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
