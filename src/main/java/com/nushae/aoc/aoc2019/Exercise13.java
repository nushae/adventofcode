package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.ArcadeGame;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 13, title="Care Package", keywords = {"intcode", "arcade", "breakout"})
public class Exercise13 {

	private final IntCodeInterpreter interpreter;
	private final ArcadeGame arcadeGame;

	private Exercise13() {
		interpreter = new IntCodeInterpreter();
		arcadeGame = new ArcadeGame();
		interpreter.setOutputReceiver(arcadeGame);
		interpreter.setInputProvider(arcadeGame);
	}

	public Exercise13(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise13(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public long doPart1() {
		interpreter.reRunProgram();
		return arcadeGame.getBlocks();
	}

	public long doPart2() {
		interpreter.resetState();
		interpreter.poke(BigInteger.ZERO, BigInteger.valueOf(2)); // free game!!
		interpreter.runProgram();
		return arcadeGame.getScore();
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2019, 13));

		// processInput not needed

		// part 1 - 296 (171ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 13824 (2.114s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
