package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 16, title="Flawed Frequency Transmission", keywords = {"feedback-loop", "list-zipping"})
public class Exercise16 {

	public static final int[] PATTERN = {0, 1, 0, -1};

	ArrayList<String> lines;

	public Exercise16() {
		lines = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	/**
	 * Analysis of the FFT algorithm to optimize it -
	 * we have a repeating pattern 0, 1, 0, -1; starting with signal ABCDEFGH... we do:
	 *    A  B  C  D  E  F  G  H  I  J  K  L  M  N ...
	 *    *  *  *  *  *  *  *  *  *  *  *  *  *  *     #
	 * 0  1  0 -1  0  1  0 -1  0  1  0 -1  0  1  0 ... A - C + E - G + I - K + M ... pos = 1; skip 1-1, add 1, skip 1, sub 1, etc
	 * 0  0  1  1  0  0 -1 -1  0  0  1  1  0  0 -1 ... B + C - F - G + J + K - N ... pos = 2; skip 2-1, add 2, skip 2, sub 2, etc
	 * 0  0  0  1  1  1  0  0  0 -1 -1 -1  0  0  0 ... C + D + E - I - J - K ...     pos = 3; skip 3-1, add 3, skip 3, sub 3, etc
	 * 0  0  0  0  1  1  1  1  0  0  0  0 -1 -1 -1 ... D + E + F + G - L - M - N ... pos = 4; skip 4-1, add 4, skip 4, sub 4, etc
	 * 0  0  0  0  0  1  1  1  1  1  0  0  0  0  0 ... #
	 * ...
	 * Check the column marked with #: the first digit we use for any digit is the digit itself.
	 *
	 * So in general for digit in position N (regardless of phase) we skip the first N-1 old-digits,
	 *   add next N old-digits, skip next N, sub next N, skip next N, add next N, etc, then do % 10
	 *
	 * Now let's think about skip. If we don't care about the first M digits in the output because our message starts at M+1,
	 * do we even need to calculate the output for earlier digits?
	 * The Nth output digit for phase 1 only depends on digits N and beyond in the input. So:
	 *
	 * digit 1 depends on input digits 1+
	 * digit 2 depends on input digits 2+
	 * ...
	 * digit N depends on input digits N+
	 *
	 * Then in phase 2, we get:
	 * digit 1 depends on phase 1 digits 1+, the first of which depends on input digits 1+
	 * digit 2 depends on phase 1 digits 2+, the first of which depends on input digits 2+
	 * ...
	 * digit N depends on phase 1 digits N+, the first of which depends on input digits N+
	 *
	 * So digit N never depends on any digit from any phase in a position before it.
	 * So if skip > 0, we can just pretend the first skip digits are 0, and calculate output accordingly,
	 * ignoring the first skip digits
	 *
	 * @param digits input digits
	 * @param skip the number of digits in the output to skip before extracting the message
	 * @param msgLength the message length
	 * @param phases the number of phases to run
	 * @return the message
	 */
	public static String doFFTOptimized(String digits, int skip, int msgLength, int phases) {
		if (skip > digits.length()/2) {
			return doFFTSpecialized(digits, skip, msgLength, phases);
		}
		char[] input = digits.substring(skip).toCharArray();
		for (int phase = 0; phase < phases; phase++) {
			char[] output = new char[input.length];
			for (int N = 1 + skip; N <= digits.length(); N++) {
				int startPos = N - 1 - skip;
				int patPos = 1;
				int total = 0;
				while (startPos < input.length) {
					if (PATTERN[patPos] != 0) {
						for (int count = 0; count < N && startPos + count < input.length; count++) {
							total = (total + PATTERN[patPos] * (input[startPos + count] - '0'));
						}
					}
					startPos += N;
					patPos = (patPos + 1) % PATTERN.length;
				}
				output[N - 1 - skip] = (char) (Math.abs(total % 10) + '0');
			}
			input = output;
		}
		return new String(input).substring(0, msgLength);
	}

	/**
	 * For part 2 we need to skip A LOT of digits. Note that if skip > digits.size()/2, the pattern will be
	 * all 1's for every digit we need to treat. This specialized version of the routine leverages this.
	 *
	 * @param digits input digits
	 * @param skip the number of digits in the output to skip before extracting the message
	 * @param msgLength the message length
	 * @param phases the number of phases to run
	 * @return the message
	 */
	public static String doFFTSpecialized(String digits, int skip, int msgLength, int phases) {
		char[] input = digits.substring(skip).toCharArray();
		for (int phase = 0; phase < phases; phase++) {
			char[] output = new char[input.length];
			int total = 0;
			for (int N = input.length-1; N >= 0; N--) {
				total = (total + input[N] - '0');
				output[N] = (char) (total  % 10 + '0');
			}
			input = output;
		}
		return new String(input).substring(0, msgLength);
	}

	/**
	 * Executes the FFT exactly as described in the puzzle. VERY slow, but useful to test other versions
	 * against its output.
	 *
	 * @param digits input digits
	 * @param skip the number of digits in the output to skip before extracting the message
	 * @param msgLength the message length
	 * @param phases the number of phases to run
	 * @return the message
	 */
	public static String doFFTAsIs(String digits, int skip, int msgLength, int phases) {
		int len = digits.length();
		int[] inputDigits = Arrays.stream(digits.split("")).mapToInt(Integer::parseInt).toArray();
		for (int phase = 1; phase <= phases; phase++) {
			int[] outputDigits = new int[len];
			for (int output = 0; output < digits.length(); output++) {
				outputDigits[output] = process(inputDigits, output);
			}
			inputDigits = outputDigits;
		}
		StringBuilder sb = new StringBuilder();
		for (int d = skip; d < skip + msgLength && d < inputDigits.length; d++) {
			sb.append(inputDigits[d]);
		}
		return sb.toString();
	}

	public static int process(int[] input, int patrepeat) {
		int accum = 0;
		int digit = 1;
		int patpointer = 0;
		for (int num : input) {
			if (digit > patrepeat) {
				patpointer = (patpointer + 1) % PATTERN.length;
				digit = 0;
			}
			digit++;
			accum += PATTERN[patpointer]*num;
		}
		String result = "" + accum;
		result = result.substring(result.length()-1);
		return Integer.parseInt(result);
	}

	public String doPart1() {
		return doFFTOptimized(lines.get(0), 0, 8, 100);
	}

	public String doPart2() {
		int skip = Integer.parseInt(lines.get(0).substring(0, 7));
		log.debug(lines.get(0).length()*10000 + " / skip first " + skip + "; so only using the last " + (lines.get(0).length()*10000 - skip) + " digits!");
		return doFFTOptimized(longify(lines.get(0)), skip, 8, 100);
	}

	public static String longify(String input) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i< 10000; i++) {
			result.append(input);
		}
		return result.toString();
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2019, 16));

		// no processInput needed

		// part 1 - 88323090 (29ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 50077964 (14ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
