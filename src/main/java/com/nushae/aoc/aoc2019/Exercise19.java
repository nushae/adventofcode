package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.CyclicListInputProvider;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.SingleOutputReceiver;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.Arrays;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 19, title="Tractor Beam", keywords = {"grid", "intcode", "area-detect", "extrapolation"})
public class Exercise19 {

	private final IntCodeInterpreter interpreter;
	boolean[][] tractorpull;

	public Exercise19() {
		interpreter = new IntCodeInterpreter();
	}

	public Exercise19(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise19(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public long doPart1() {
		long total = 0;
		tractorpull = new boolean[50][50];
		CyclicListInputProvider coords = new CyclicListInputProvider();
		interpreter.setInputProvider(coords);
		SingleOutputReceiver out = new SingleOutputReceiver();
		interpreter.setOutputReceiver(out);
		for (int j = 0; j < 50; j++) {
			for (int i=0; i< 50; i++) {
				coords.setValues(Arrays.asList(BigInteger.valueOf(i), BigInteger.valueOf(j)));
				interpreter.reRunProgram();
				long pulled = out.getLastOutput().longValue();
				// remember the values in tractorpull for part 2
				tractorpull[i][j] = pulled == 1;
				total += out.getLastOutput().longValue();
			}
		}
		return total;
	}

	// I guess the 'normal' approach would be to follow one of the cone edges and test the other corner whether it is
	// also pulled, until both are. But that's for uncreative muggles.
	public long doPart2() {
		// we can calculate the two borders for the cone of the tractor beam from our boolean[][] by finding the two
		// spots on the right and/or bottom edge. It won't be very accurate though, since we are dealing with discrete
		// lines. So we will have to do some manual adjustment, but it will be a lot faster than brute forcing it.
		// From the output of part1 I already know it's the bottom edge for my input, but this is a general solution to
		// find the two edge spots:
		Point pointX1 = new Point(0,0); // just some point definitely in the beam
		Point pointX2 = new Point(49, 0);
		for (int i = 0; i < 100 && (!tractorpull[pointX2.x][pointX2.y] || tractorpull[pointX1.x][pointX1.y]); i++) {
			Point testlocation = i < 50 ? new Point(49, i) : new Point(99-i, 49);
			if (!tractorpull[pointX2.x][pointX2.y]) { // haven't entered the beam before now, so still replace pointX2
				pointX2 = testlocation;
			}
			if (tractorpull[pointX2.x][pointX2.y] && !tractorpull[testlocation.x][testlocation.y]) { // out of the beam again, done
				break;
			} else {
				pointX1 = testlocation;
			}
		}
		log.debug("Edge spots for cone edges: X1 = (" + pointX1.x + ", " + pointX1.y + "), slope: " + (pointX1.x * 1.0 / pointX1.y) + " / X2 = (" + pointX2.x + ", " + pointX2.y + "), slope: " + (pointX2.x * 1.0 / pointX2.y));
		// Using the bounding lines of the cone to fit the square, an analytical solution.
		// Given that the bounding lines of the cone go through (0,0) / (x1,y1) and (0,0) / (x2,y2),
		// the line equations simplify to y = y1/x1 * x and y = y2/x2 * x respectively.
		// Note that any square with its edges aligned to the axes will have a diagonal with equation y = y3 - x.
		// We're looking to have its two corners on y = y3 - x fall exactly on the edges of the cone.
		// Let B and D be the intersection points of y = y1 / x1 * x and y = y2 / x2 * x with the line y = y3 - x.
		// Our goal is to find a value for y3, such that B and D are the opposite corners of a 100x100 square ABCD
		//
		// We have:
		// intersection B: yB = y3 - xB = y2/x2 * xB, so xB = y3 / (y2/x2 + 1)
		// intersection D: yD = y3 - xD = y1/x1 * xD, so xD = y3 / (y1/x1 + 1)
		// The horizontal distance between B and D must be a: a = xD - xB
		// a = y3/(y1/x1+1) - y3/(y2/x2+1)
		// Multiply by (y1/x1+1)(y2/x2+1) and simplify:
		// y3 = a(y1/x1+1)(y2/x2+1)/(y2/x2-y1/x1)
		//
		// So to find y3, we fill in a=100, x1, x2, y1, y2, and presto!
		double y3 = 100 * (1.0*pointX2.y/pointX2.x + 1)  * ((1.0*pointX1.y/pointX1.x + 1)) / (1.0*pointX1.y/pointX1.x - 1.0*pointX2.y/pointX2.x);
		log.debug("y3 = " + y3);
		log.debug("B = (" + (y3 / (1.0 * pointX1.y/pointX1.x + 1)) + ", " + (y3 - y3 / (1.0 * pointX1.y/pointX1.x + 1)) + ")");
		log.debug("D = (" + (y3 / (1.0 * pointX2.y / pointX2.x + 1)) + ", " + (y3 - y3 / (1.0 * pointX2.y / pointX2.x + 1)) + ")");
		long xA = Math.round(y3 / (1.0 * pointX1.y/pointX1.x + 1));
		long yA = Math.round(y3 - y3 / (1.0 * pointX2.y / pointX2.x + 1));
		// experimentally determined adjustments with the code that follows:
		xA = xA - 12;
		yA = yA - 37;
		// at this point the top right corner can move -1, 3 but the bottom left can only move to the left if it moves at least -1,4; or -2,6 vs -2,8; and so on. Whew
		log.debug("Using point A: (" + xA + ", " + yA + ")");
		// Since everything is discretised, we may not immediately hit the correct spot. So let's look at the 100x100 square with a 10 pixel border:
		CyclicListInputProvider coords = new CyclicListInputProvider();
		interpreter.setInputProvider(coords);
		SingleOutputReceiver out = new SingleOutputReceiver();
		interpreter.setOutputReceiver(out);
		for (int j = -10; j < 111; j++) {
			for (int i = -10; i < 111; i++) {
				coords.setValues(Arrays.asList(BigInteger.valueOf(xA + i), BigInteger.valueOf(yA + j)));
				interpreter.reRunProgram();
				long pulled = out.getLastOutput().longValue();
			}
		}
		return 10000 * xA + yA;
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2019, 19));

		// processInput not needed

		// part 1 - 156 (3.463s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2610980 (15.428s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
