package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.Moon;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;

@Log4j2
@Aoc(year = 2019, day = 12, title="N-Body Problem", keywords = {"simulation", "gravity", "loop-detection", "lcm"})
public class Exercise12 {

	private final Moon[] moons;
	private final Moon[] origMoons;
	private final Moon[] saveMoons;

	public Exercise12(Moon... moons) {
		saveMoons = moons;
		origMoons = new Moon[saveMoons.length];
		this.moons = new Moon[saveMoons.length];
	}

	public void reset() {
		int m = 0;
		for (Moon moon : saveMoons) {
			origMoons[m] = new Moon(moon);
			moons[m] = new Moon(moon);
			m++;
		}
	}

	public int doPart1(int steps) {
		reset();
		int result = 0;
		for (int i = 0; i < steps; i++) {
			for (Moon moon : moons) {
				moon.adjustVelocity(moons);
			}
			for (Moon moon : moons) {
				moon.adjustPosition();
			}
		}
		for (Moon moon : moons) {
			result += moon.getTotalEnergy();
		}
		return result;
	}

	// key to note is that the positions and velocities change independently per axis
	// so we can find a return-to-origin step count per axis independently. These loops are not expected to match,
	// so we seek the least common multiple of these loop lengths
	public BigInteger doPart2() {
		reset();
		int[] loopsteps = new int[3];
		for (int axis =0; axis < 3; axis++) {
			boolean eq;
			int steps = 0;
			do {
				steps++;
				for (Moon moon : moons) {
					moon.adjustVelocityBySingleAxis(axis, moons);
				}
				for (Moon moon : moons) {
					moon.adjustPositionBySingleAxis(axis);
				}
				eq = true;
				for (int m=0; m < moons.length; m++) {
					eq = eq && moons[m].equals(origMoons[m]);
				}
			} while (!eq);
			loopsteps[axis] = steps;
		}
		BigInteger lsX = BigInteger.valueOf(loopsteps[0]);
		BigInteger lsY = BigInteger.valueOf(loopsteps[1]);
		BigInteger lsZ = BigInteger.valueOf(loopsteps[2]);
		BigInteger prod1 = lsX.multiply(lsY);
		BigInteger lcm1 = prod1.divide(lsX.gcd(lsY));
		BigInteger prod2 = lsZ.multiply(lcm1);
		return prod2.divide(lsZ.gcd(lcm1));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Moon moon : moons) {
			sb.append(moon.toString()).append("\n");
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(
				new Moon(6, 10, 10),
				new Moon(-9, 3, 17),
				new Moon(9, -4, 14),
				new Moon(4, 14, 4)
		);

		// no processInput needed

		// part 1 - 13045 (3ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(1000));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 344724687853944 (48ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
