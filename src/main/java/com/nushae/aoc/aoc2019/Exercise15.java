package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.domain.Direction;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.RepairDroid;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.GraphicsUtil.DEFAULT_FONT;
import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 15, title="Oxygen System", keywords = {"grid", "intcode", "maze", "visualized", "animated"})
public class Exercise15 extends JPanel {

	public static final BigInteger WALL = BigInteger.ZERO;
	public static final BigInteger FLOOR = BigInteger.ONE;
	public static final BigInteger OXY = BigInteger.valueOf(2);

	public static final int SPEED = 5; // > 0 will result in animated visual representation
	public static final int SIZE = 9; // size of each grid cell in the visualization

	private final IntCodeInterpreter interpreter;
	private final RepairDroid repairDroid;
	private final HashMap<Point, BigInteger> maze;
	private Point maxLoc;
	private int maxValue;

	public Exercise15() {
		maze = new HashMap<>();
		interpreter = new IntCodeInterpreter();
		repairDroid = new RepairDroid(true, interpreter, maze, this);
		interpreter.setOutputReceiver(repairDroid);
		interpreter.setInputProvider(repairDroid);
		maze.put(repairDroid.position, FLOOR);
	}

	public Exercise15(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise15(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public int doPart1() {
		interpreter.reRunProgram();
		repaint();
		return repairDroid.getResult();
	}

	public int doPart2() {
		repairDroid.setFindOxy(false);
		interpreter.reRunProgram();
		repaint();
		int result = maxDist(repairDroid.getOxyLocation(), new HashMap<>(), 0);
		repaint();
		return result;
	}

	public int maxDist(Point currentLoc, HashMap<Point, Integer> visited, int distToCurrent) {
		int result = 0;
		if (maze.get(currentLoc).equals(WALL) || visited.containsKey(currentLoc)) {
			return result;
		}
		result = distToCurrent;
		visited.put(currentLoc, distToCurrent);
		if (result > maxValue) {
			maxValue = result;
			maxLoc = currentLoc;
		}
		for (Direction d: Direction.values()) {
			result = Math.max(result, maxDist(new Point(currentLoc.x + d.dx, currentLoc.y + d.dy), visited, distToCurrent+1));
		}
		return result;
	}

	@Override
	public void paintComponent(Graphics g) {
		Map<Point, BigInteger> temp = new HashMap<>(maze); // prevents concurrent modification exceptions
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		Graphics2D g2 = (Graphics2D) g;
		int xO = getWidth()/2-SIZE/2;
		int yO = getHeight()/2-SIZE/2;

		for (Map.Entry<Point, BigInteger> tile: temp.entrySet()) {
			Point p = tile.getKey();
			switch (tile.getValue().intValueExact()) {
				case 0 -> g2.setColor(Color.black);
				case 1 -> {
					g2.setColor(Color.lightGray);
					if (p.x == 0 && p.y == 0) {
						g2.setColor(Color.blue);
					}
				}
				case 2 -> g2.setColor(Color.yellow);
			}
			g2.fillRect(xO + p.x * SIZE, yO + p.y * SIZE, SIZE-1, SIZE-1);
		}

		g2.setColor(Color.red);
		g2.drawRect(xO + repairDroid.position.x * SIZE - 1, yO + repairDroid.position.y * SIZE - 1, SIZE, SIZE);
		g2.drawLine(xO+repairDroid.position.x*SIZE+SIZE/2, yO+repairDroid.position.y*SIZE+SIZE/2, xO+repairDroid.position.x*SIZE+SIZE/2+repairDroid.nextCommand.dx*SIZE, yO+repairDroid.position.y*SIZE+SIZE/2+repairDroid.nextCommand.dy*SIZE);

		if (maxLoc != null) {
			g2.setColor(Color.magenta);
			g2.drawLine(xO + maxLoc.x * SIZE, yO + maxLoc.y * SIZE, xO + maxLoc.x * SIZE + SIZE - 1, yO + maxLoc.y * SIZE + SIZE - 1);
			g2.drawLine(xO + maxLoc.x * SIZE, yO + maxLoc.y * SIZE + SIZE - 1, xO + maxLoc.x * SIZE + SIZE - 1, yO + maxLoc.y * SIZE);
		}
		drawCenteredString(g2, repairDroid.exploredNeighbours + "/"+repairDroid.route.length(), new Rectangle(0,getHeight()-20, getWidth(), 20), DEFAULT_FONT);
	}

	private static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 900));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise15 ex1 = new Exercise15(aocPath(2019, 15));
		// part 1 destroys the state so create second object to 'reset'
		Exercise15 ex2 = new Exercise15(aocPath(2019, 15));

		// processInput not needed

		// part 1 - 230 (209ms)
		long start = System.currentTimeMillis();
		if (SPEED > 0) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex1));
		}
		log.info("Part 1:");
		log.info(ex1.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 288 (450ms)
		start = System.currentTimeMillis();
		if (SPEED > 0) {
			SwingUtilities.invokeLater(() -> createAndShowGUI(ex2));
		}
		log.info("Part 2:");
		log.info(ex2.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
