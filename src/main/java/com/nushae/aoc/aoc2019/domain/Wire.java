package com.nushae.aoc.aoc2019.domain;

import com.nushae.aoc.view.Element;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.nushae.aoc.util.MathUtil.manhattanDistance;

public class Wire implements Element {
	private List<Segment> segments; // for fancy display only
	private HashMap<Point, Integer> gridPoints;
	private Point mySpecialPoint;
	private Point mySpecialPoint2;
	private int wireSteps;

	public Wire() {
		segments = new ArrayList<>();
		gridPoints = new HashMap<>();
		wireSteps = 0;
	}

	public void addSegment(Segment s) {
		segments.add(s);
		Point start = s.getStart();
		Point end = s.getEnd();
		if (start.x == end.x) { // vertical
			int change = (int) Math.signum(end.y-start.y);
			for (int i = start.y + change; i <= Math.max(start.y, end.y) && i >= Math.min(start.y, end.y); i = i + change) {
				Point p = new Point(start.x, i);
				wireSteps++;
				if (!gridPoints.containsKey(p)) {
					gridPoints.put(p, wireSteps);
				}
			}
		} else { // horizontal
			int change = (int) Math.signum(end.x-start.x);
			for (int i = start.x + change; i <= Math.max(start.x, end.x) && i >= Math.min(start.x, end.x); i = i + change) {
				Point p = new Point(i, start.y);
				wireSteps++;
				if (!gridPoints.containsKey(p)) {
					gridPoints.put(p, wireSteps);
				}
			}
		}
	}

	public int findManhattanIntersectionsWith(Wire other) {
		int dist = Integer.MAX_VALUE;
		for (Point p : gridPoints.keySet()) {
			if (other.gridPoints.containsKey(p)) {
				int md = manhattanDistance(p.x, p.y);
				if (md < dist) {
					dist = md;
					mySpecialPoint = p;
				}
			}
		}
		return dist;
	}

	public int findStepsIntersectionsWith(Wire other) {
		int dist = Integer.MAX_VALUE;
		for (Point p : gridPoints.keySet()) {
			if (other.gridPoints.containsKey(p)) {
				int md = gridPoints.get(p) + other.gridPoints.get(p);
				if (md < dist) {
					dist = md;
					mySpecialPoint2 = p;
				}
			}
		}
		return dist;
	}

	public void display(Graphics2D g2) {
		for (Segment s : segments) {
			s.display(g2);
		}
		if (mySpecialPoint != null) {
			g2.drawOval(mySpecialPoint.x-2, mySpecialPoint.y-2, 4, 4);
		}
		if (mySpecialPoint2 != null) {
			g2.fillOval(mySpecialPoint2.x-2, mySpecialPoint2.y-2, 4, 4);
		}
	}
}

