package com.nushae.aoc.aoc2019.domain;

import java.util.HashMap;

public class Reaction {
	public final int outputAmount;
	public final HashMap<String, Integer> inputs;

	public Reaction(int outputAmount, HashMap<String, Integer> reactants) {
		this.outputAmount = outputAmount;
		this.inputs = new HashMap<>(reactants);
	}
}
