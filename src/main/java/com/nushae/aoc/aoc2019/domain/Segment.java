package com.nushae.aoc.aoc2019.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.awt.*;

public class Segment {
	private Point start, end;

	public Segment(Point s, Point e) {
		start = s;
		end = e;
	}

	public Point getStart() {
		return start;
	}

	public Point getEnd() {
		return end;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Segment rhs = (Segment) obj;
		return new EqualsBuilder()
				.appendSuper(super.equals(obj))
				.append(start.x, rhs.start.x)
				.append(start.y, rhs.start.y)
				.append(end.x, rhs.end.x)
				.append(end.y, rhs.end.y)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(13, 37)
				.append(start)
				.append(end)
				.toHashCode();
	}

	@Override
	public String toString() {
		return "(" + start.x + "," + start.y + ")-(" +end.x + "," +end.y +")";
	}

	public void display(Graphics2D g2) {
		g2.drawLine(start.x, start.y, end.x, end.y);
	}
}

