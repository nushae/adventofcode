package com.nushae.aoc.aoc2019.domain;

import java.math.BigInteger;

public enum Direction {
	NORTH(0, -1, "N", BigInteger.ONE),
	EAST(1, 0, "E", BigInteger.valueOf(4)),
	SOUTH(0, 1, "S", BigInteger.valueOf(2)),
	WEST(-1, 0, "W", BigInteger.valueOf(3));

	public int dx;
	public int dy;
	public String s;
	public BigInteger v;

	public static Direction ofBigInteger(BigInteger b) {
		for (Direction d : Direction.values()) {
			if (d.v.equals(b)) {
				return d;
			}
		}
		throw new IllegalArgumentException("Unknown direction value " + b);
	}

	public static Direction ofLetter(String s) {
		for (Direction d : Direction.values()) {
			if (d.s.equals(s)) {
				return d;
			}
		}
		throw new IllegalArgumentException("Unknown direction letter " + s);
	}

	public Direction rotR() {
		return values()[(ordinal() + 1) % values().length];
	}

	public Direction rotL() {
		return values()[(ordinal() + values().length - 1) % values().length];
	}

	public Direction opp() {
		return values()[(ordinal() + values().length / 2) % values().length];
	}

	Direction(int dx, int dy, String s, BigInteger v) {
		this.dx = dx;
		this.dy = dy;
		this.s = s;
		this.v = v;
	}
}
