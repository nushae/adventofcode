package com.nushae.aoc.aoc2019.domain;

import com.nushae.aoc.domain.GraphNode;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

public class DonutNode implements GraphNode<String> {
	private static final boolean SHOW = false;

	private final String name; // contains gate label if this is a gate
	private final Point loc;
	private final String payload; // a concatenation of the actual (character) payload and the depth
	private final Character[][] grid;
	private final Map<Character, java.util.List<Point>> gateLocations;
	private final int depth;

	public DonutNode(String name, int depth, Point loc, String payload, Character[][] grid, Map<Character, List<Point>> gateLocations) {
		this.name = name;
		this.loc = loc;
		this.payload = payload;
		this.grid = grid;
		this.gateLocations = gateLocations;
		this.depth = depth;
	}

	@Override
	public Point getLocation() {
		return null;
	}

	@Override
	public Map<DonutNode, Long> getAdjacentNodes() {
		char myType = grid[loc.x][loc.y];
		if (SHOW && myType != '.') {
			System.out.print(payload + "-[" + loc.x + "/" + grid.length + "," + loc.y + "/" + grid[0].length + "]");
		}
		Map<DonutNode, Long> result = new HashMap<>();
		// use grid to return a set of neighbour nodes.
		// If depth == 0: all outer gates are walls, @ and & act normally;
		// If depth > 0: all outer gates act normally except @ and & are walls
		// Outer gates on depth connect to inner gates on depth-1, inner gates on depth connect to outer gates on depth+1
		for (Point p : ORTHO_DIRS) { // same-depth neighbour search
			Point neighbour = new Point(loc.x + p.x, loc.y + p.y);
			char neighbourType = grid[neighbour.x][neighbour.y];
			if (neighbourType == '.') { // normal space
				// add
				result.put(new DonutNode("", depth, neighbour, "" + neighbourType + depth, grid, gateLocations), 1L);
			} else if (neighbourType != ' ' && neighbourType != '#' && !(neighbourType >= 'A' && neighbourType <= 'Z') && neighbourType != '@' && (neighbourType != '&' || depth == 0)) {
				// normal gate, top level '&', or '.'; add to neighbours if not depth == 0 && outer gate && !'&'
				if (neighbour.x == 3 || neighbour.x == grid.length - 4 || neighbour.y == 3 || neighbour.y == grid[0].length - 4) { // outer gate, only ok if '&' or depth > 0
					if (depth > 0 || neighbourType == '&') {
						result.put(new DonutNode("", depth, neighbour, "" + neighbourType + depth, grid, gateLocations), 1L);
					}
				} else { // inner gate
					result.put(new DonutNode("", depth, neighbour, "" + neighbourType + depth, grid, gateLocations), 1L);
				}
			}
		}
		if (myType != '.') { // we MUST be on a gate, @ and & can effectively never occur -> add warp neighbours
			if (loc.x == 3 || loc.x == grid.length - 4 || loc.y == 3 || loc.y == grid[0].length - 4) { // outer gate, warp up
				if (SHOW) {
					System.out.print("(outer gate) ");
				}
				if (depth != 0) { // outer gates don't warp on depth 0
					for (Point warp : gateLocations.get(myType)) {
						if (!warp.equals(loc)) {
							result.put(new DonutNode("", depth - 1, warp, "" + myType + (depth - 1), grid, gateLocations), 1L);
						}
					}
				}
			} else { // inner gate, warp down
				if (SHOW) {
					System.out.print("(inner gate) ");
				}
				for (Point warp : gateLocations.get(myType)) {
					if (!warp.equals(loc) && depth < 100) { // without this cutoff, solution-less mazes lead to infinite recursion
						result.put(new DonutNode("", depth + 1, warp, "" + myType + (depth + 1), grid, gateLocations), 1L);
					}
				}
			}
		}
		if (SHOW && myType != '.') {
			for (DonutNode dn : result.keySet()) {
				System.out.print(" " + dn.payload + "-[" + dn.loc.x + "," + dn.loc.y + "]");
			}
			System.out.println();
		}
		return result;
	}

	@Override
	public void addNeighbour(GraphNode<String> neighbour, long distanceToNeighbour) {
		// NOP - neighbours are dynamically generated
	}

	@Override
	public String getPayload() {
		return payload;
	}

	@Override
	public void setPayload(String payload) {
		// NOP - payloads don't change.
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		// NOP - names don't change.
	}

	@Override
	public int hashCode() {
		return (payload + loc).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		DonutNode other = (DonutNode) obj;
		return (payload + loc).equals(other.payload + other.loc);
	}
}
