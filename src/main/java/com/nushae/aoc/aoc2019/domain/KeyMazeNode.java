package com.nushae.aoc.aoc2019.domain;

import com.nushae.aoc.domain.SearchNode;

import java.awt.*;
import java.util.*;
import java.util.stream.Collectors;

public class KeyMazeNode implements SearchNode {

	// keys are of the form x,y/<String containing sorted list of collected keys>
	private static final Map<String, KeyMazeNode> nodes = new HashMap<>();

	private SearchNode parent;
	private final Set<Point> locations;
	private final String myNodeKey;
	private final char mySymbolReached;
	// all keys collected on the way here, including the key in this location (if any; remember we also have @ and 1-4):
	private final Set<Character> collectedKeys;
	private final Map<Point, Map<Point, String>> distanceMatrixRef;
	private final Character[][] gridRef;

	public KeyMazeNode(Set<Point> locs, char symbolReachedWithMove, Set<Character> collectedKeys, Character[][] gridRef, Map<Point, Map<Point, String>> distanceMatrixRef) {
		mySymbolReached = symbolReachedWithMove;
		String locationString = locs.stream().sorted(Comparator.comparingDouble(Point::getX).thenComparingDouble(Point::getY)).map(p -> p.x + "," + p.y).collect(Collectors.joining("|"));
		myNodeKey = locationString + "/" + collectedKeys.stream().sorted().map(Object::toString).collect(Collectors.joining());
		this.collectedKeys = collectedKeys;
		this.locations = locs;
		if (symbolReachedWithMove >= 'a' && symbolReachedWithMove <= 'z') {
			collectedKeys.add(symbolReachedWithMove);
		}
		this.gridRef = gridRef;
		this.distanceMatrixRef = distanceMatrixRef;
	}

	@Override
	public Map<SearchNode, Long> getAdjacentNodes() {
		Map<SearchNode, Long> result = new HashMap<>();
		for (Point location : locations) {
			for (Map.Entry<Point, String> neighbourEntry : distanceMatrixRef.getOrDefault(location, new HashMap<>()).entrySet()) {
				String[] vals = neighbourEntry.getValue().split(";");
				long distance = Long.parseLong(vals[1]);
				Point nLoc = neighbourEntry.getKey();
				Set<Point> newLocations = new HashSet<>(locations);
				newLocations.remove(location);
				newLocations.add(nLoc);
				String doors = vals[0];
				// all letters in 'doors' must be in this node's collectedKeys, or it won't be reachable from here
				boolean reachable = true;
				for (int c = 0; c < doors.length(); c++) {
					reachable = reachable && collectedKeys.contains((char) (doors.charAt(c) - 'A' + 'a'));
				}
				if (reachable) {
					Set<Character> neighBourKeys = new HashSet<>(collectedKeys);
					char newlyReachedSymbol = gridRef[nLoc.x][nLoc.y];
					if (newlyReachedSymbol >= 'a' && newlyReachedSymbol <= 'z') {
						neighBourKeys.add(newlyReachedSymbol);
					}
					String nodeKey = newLocations.stream().sorted(Comparator.comparingDouble(Point::getX).thenComparingDouble(Point::getY)).map(p -> p.x + "," + p.y).collect(Collectors.joining("|")) + "/" + neighBourKeys.stream().sorted().map(Object::toString).collect(Collectors.joining());
					nodes.computeIfAbsent(nodeKey, k -> new KeyMazeNode(newLocations, newlyReachedSymbol, neighBourKeys, gridRef, distanceMatrixRef));
					result.put(nodes.get(nodeKey), distance);
				}
			}
		}
		return result;
	}

	public Set<Point> getLocations() {
		return locations;
	}

	@Override
	public String getState() {
		return myNodeKey;
	}

	@Override
	public void setParent(SearchNode parent) {
		this.parent = parent;
	}

	@Override
	public SearchNode getParent() {
		return parent;
	}

	@Override
	public int hashCode() {
		return myNodeKey.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		KeyMazeNode other = (KeyMazeNode) obj;
		return myNodeKey.equals(other.myNodeKey);
	}

	public static void reInit() {
		nodes.clear();
	}
}
