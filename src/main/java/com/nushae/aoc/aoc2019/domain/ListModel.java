package com.nushae.aoc.aoc2019.domain;

import com.nushae.aoc.domain.Model;

import java.util.ArrayList;
import java.util.List;

public class ListModel<T> implements Model<T> {

	private ArrayList<T> myElements;

	public ListModel() {
		myElements = new ArrayList<>();
	}

	public void add(T t) {
		myElements.add(t);
	}

	@Override
	public List<T> getElements() {
		return myElements;
	}
}
