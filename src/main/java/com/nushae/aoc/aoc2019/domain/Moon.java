package com.nushae.aoc.aoc2019.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Moon {
	private int x, y, z;
	private int vx, vy, vz;

	public Moon(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.vx = 0;
		this.vy = 0;
		this.vz = 0;
	}

	public Moon(Moon moon) {
		this(moon.x, moon.y, moon.z);
	}

	// note that m.adjustVelocity(m) will have no effect, which simplifies things!
	public void adjustVelocity(Moon... moons) {
		for (Moon other : moons) {
			vx += Integer.compare(other.x, x);
			vy += Integer.compare(other.y, y);
			vz += Integer.compare(other.z, z);
		}
	}

	public void adjustPosition() {
		x += vx;
		y += vy;
		z += vz;
	}

	public void adjustVelocityBySingleAxis(int axis, Moon... moons) {
		for (Moon other : moons) {
			switch (axis) {
				case 0: // x
					vx += Integer.compare(other.x, x);
					break;
				case 1: // y
					vy += Integer.compare(other.y, y);
					break;
				case 2: // z
					vz += Integer.compare(other.z, z);
					break;
			}
		}
	}

	public void adjustPositionBySingleAxis(int axis) {
		switch (axis) {
			case 0: // x
				x += vx;
				break;
			case 1: // y
				y += vy;
				break;
			case 2: // z
				z += vz;
				break;
		}
	}

	public int getTotalEnergy() {
		int potential = Math.abs(x) + Math.abs(y) + Math.abs(z);
		int kinetic = Math.abs(vx) + Math.abs(vy) + Math.abs(vz);
		return kinetic * potential;
	}

	@Override
	public String toString() {
		return String.format("pos=<x=%3d, y=%3d, z=%3d>, vel=<x=%3d, y=%3d, z=%3d>", x, y, z, vx, vy, vz);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Moon)) {
			return false;
		}
		Moon other  = (Moon) o;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(x, other.x).append(y, other.y).append(z, other.z)
				.append(vx, other.vx).append(vy, other.vy).append(vz, other.vz);
		return builder.isEquals();
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder(13, 37);
		builder.append(x).append(y).append(z)
				.append(vx).append(vy).append(vz);
		return builder.toHashCode();
	}

}
