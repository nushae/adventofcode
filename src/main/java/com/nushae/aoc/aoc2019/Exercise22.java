package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.bezout_coeff;
import static com.nushae.aoc.util.MathUtil.modPow;

@Log4j2
@Aoc(year = 2019, day = 22, title="Slam Shuffle", keywords = {"modulo", "linear-function-composition", "inverse"})
public class Exercise22 {

	List<String> lines;
	List<Function<Long, Long>> operations;
	List<Function<Long, Long>> inverseOps;
	List<long[]> standardizedOperations;
	List<long[]> standardizedInverseOps;
	long[] forwardShuffle; // flattened version, just the resulting A and B coefficient of the final linear function card -> (A * card + B) % decksize
	long[] reverseShuffle; // same for the inverse
	final long decksize;

	public Exercise22(long decksize) {
		this.decksize = decksize;
		lines = new ArrayList<>();
	}

	public Exercise22(long decksize, Path path) {
		this(decksize);
		loadInputFromFile(lines, path);
	}

	public Exercise22(long decksize, String data) {
		this(decksize);
		loadInputFromData(lines, data);
	}

	private void processInput() {
		operations = new ArrayList<>(); // for part 1
		inverseOps = new ArrayList<>(); // for part 2
		// as above, but with just the A, B for all functions rewritten in the form "card -> (A * card + B) % decksize", for flattening purposes
		standardizedOperations = new ArrayList<>();
		standardizedInverseOps = new ArrayList<>();
		for (String line : lines) {
			// For part 1:
			// 1) deal into new stack: card -> decksize - card - 1
			// 2) cut C: card -> (card - C) % decksize
			// 3) deal with inc I: card -> (card * I) % decksize
			//
			// For part 2:
			// Determine the inverse operation so we can calculate the original position of any card in
			// the deck by applying the inverses of the operations, in reverse order.
			// 1) deal into new stack is its own inverse: card -> decksize - card - 1
			// 2) cut C, inverse is cut -C: card -> (card + C) % decksize
			// 3) deal with inc I: card -> (card * inv(I)) % decksize, where inv(I) is the modular multiplicative inverse of I under decksize
			//   incr does newpos = oldpos * incrAmount % decksize so given newpos and incrAmount and decksize, to find oldpos:
			//   oldpos = newpos / incrAmount % decksize = newpos * inv(incrAmount) % decksize
			//   so we calculate the multiplicative inverse Q of incrAmount under decksize and then just do incr(newpos, Q, decksize)
			//
			// Flattening: all of these can be expressed as card -> (card * N + M) % decksize:
			// 1) N == -1, M == decksize - 1 (regular and inverse)
			// 2) N == 1, M == decksize - C (regular)
			//    N == 1, M = C (inverse)
			// 3) N == I, M == 0
			//    N == inv(I), M == 0 (inverse)
			if (line.startsWith("deal with increment ")) {
				long incrAmount = Long.parseLong(line.substring(20));
				long[] inverses = bezout_coeff(incrAmount, decksize);
				operations.add(card -> incr(card, incrAmount, decksize));
				inverseOps.add(card -> incr(card, inverses[0] % decksize, decksize));
				standardizedOperations.add(new long[] {incrAmount, 0});
				standardizedInverseOps.add(0, new long[] {inverses[0] % decksize, 0});
			} else if (line.startsWith("cut ")) {
				final long cutAmount = Long.parseLong(line.substring(4));
				operations.add(card -> cut(card, cutAmount, decksize));
				inverseOps.add(card -> cut(card, -cutAmount, decksize));
				standardizedOperations.add(new long[] {1, decksize - cutAmount});
				standardizedInverseOps.add(0, new long[] {1, cutAmount});
			} else if ("deal into new stack".equals(line)) {
				operations.add(a -> rev(a, decksize));
				inverseOps.add(a -> rev(a, decksize));
				standardizedOperations.add(new long[]{-1, decksize - 1});
				standardizedInverseOps.add(0, new long[]{-1, decksize - 1});
			} else {
				log.warn("Don't know how to parse '" + line + "'");
			}
		}
		forwardShuffle = flatten(standardizedOperations);
		reverseShuffle = flatten(standardizedInverseOps);
	}

	public long doPart1(long findPosOf, long shuffles) {
		processInput();
		BigInteger result = BigInteger.valueOf(findPosOf);
		BigInteger ds = BigInteger.valueOf(decksize);
		for (long s = 0; s < shuffles; s++) {
			result = result.multiply(BigInteger.valueOf(forwardShuffle[0])).add(BigInteger.valueOf(forwardShuffle[1])).mod(ds);
		}
		return result.longValueExact();
	}

	// Starting with the a and b of the 'flattened' inverse shuffle function f(card) = (a * card + b) % decksize,
	// we can shortcut the N-fold composition of f with itself (aka do N inverse shuffles) as follows:
	//
	// f(f(x)) = f(a * x + b) = a * (a * x + b) + b = a^2 * x + a * b  + b = a^2 * x + b * (a + 1),
	// f(f(f(x))) = a * (a^2x + ab + b) + b = a^3 * x + b * (a^2 + a + 1),
	// f(f(f(f(x)))) = a^4 * x + b * (a^3 + a^2 + a + 1),
	// ...
	// f^N (x) = a^N * x + b * (a^(N-1) + a^(N-2) + ... + a + 1)
	//
	// The latter can be further simplified to:
	//
	// f^N (x) = a^N * x + b * (1 - a^N) / (1-a)
	//
	// All of this modulo decksize of course. The problem is now reduced to finding A = a^N and
	// Q = the modular multiplicative inverse of 1-a under decksize, so we can immediately
	// calculate A * x + b * (1 - A) * Q (mod decksize) to find the original position of x in the deck.
	public long doPart2(long findvalueInPos, long shuffles) {
		processInput();
		BigInteger find = BigInteger.valueOf(findvalueInPos);
		BigInteger ds = BigInteger.valueOf(decksize);
		BigInteger a = BigInteger.valueOf(reverseShuffle[0]);
		BigInteger b = BigInteger.valueOf(reverseShuffle[1]);
		BigInteger Q = BigInteger.valueOf(bezout_coeff(1-reverseShuffle[0], decksize)[0]);
		BigInteger A = modPow(a, BigInteger.valueOf(shuffles), BigInteger.valueOf(decksize));
		return (A.multiply(find).add(BigInteger.ONE.subtract(A).multiply(Q).multiply(b))).mod(ds).longValueExact();
	}

	public long doPart1Slow(long findPosOf, long shuffles) {
		processInput();
		long result = findPosOf;
		for (long s = 0; s < shuffles; s++) {
			for (Function<Long, Long> shuffle : operations) {
				result = shuffle.apply(result);
			}
		}
		return result;
	}

	public long doPart2Slow(long findvalueInPos, long shuffles) {
		processInput();
		long result = findvalueInPos;
		for (long s = 0; s < shuffles; s++) {
			for (int f = 0; f < inverseOps.size(); f++) {
				Function<Long, Long> shuffle = inverseOps.get(inverseOps.size() - 1 - f);
				result = shuffle.apply(result);
			}
		}
		return result;
	}

	public static long cut(long a, long cutAmount, long decksize) {
		return (a + decksize - cutAmount) % decksize;
	}
	public static long rev(long a, long decksize) {
		return decksize-a-1;
	}
	public static long incr(long oldPos, long incrAmount, long decksize) {
		return BigInteger.valueOf(oldPos).multiply(BigInteger.valueOf(incrAmount)).mod(BigInteger.valueOf(decksize)).longValueExact();
	}

	// for functions f(x) = ax + b and g(x) = cx + d, the composition h(x) = f(g(x)) = ex + f, where
	// e = ac, and f = ad + b. This also holds true when calculating modulo M.
	private long[] flatten(List<long[]> linearFunctions) {
		BigInteger resultA = BigInteger.ONE;
		BigInteger resultB = BigInteger.ZERO;
		BigInteger bd = BigInteger.valueOf(decksize);
		for (long[] ab : linearFunctions) {
			BigInteger ab0 = BigInteger.valueOf(ab[0]);
			resultA = resultA.multiply(ab0).mod(bd);
			resultB = resultB.multiply(ab0).add(BigInteger.valueOf(ab[1])).mod(bd);
		}
		return new long[] {resultA.longValueExact(), resultB.longValueExact()};
	}

	public static void main(String[] args) {

		// part 1 and 2 include a processInput call, times are included in runtime for the part.
		// (We use separate instances for each part anyway because the parameters change a lot)

		// part 1 - 8775 (356ms)
		long start = System.currentTimeMillis();
		Exercise22 ex1 = new Exercise22(10007, aocPath(2019, 22));
		log.info("Part 1:");
		log.info(ex1.doPart1(2019, 1));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 47141544607176 (2ms)
		start = System.currentTimeMillis();
		Exercise22 ex2 = new Exercise22(119315717514047L , aocPath(2019, 22));
		log.info("Part 2:");
		log.info(ex2.doPart2(2020, 101741582076661L));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}
}
