package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.aoc2019.intcode.Robot;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 11, title="Space Police", keywords = {"intcode", "robot", "bigtext"})
public class Exercise11 {

	private final IntCodeInterpreter interpreter;
	private final Robot robot;

	private Exercise11() {
		interpreter = new IntCodeInterpreter();
		robot = new Robot();
		interpreter.setOutputReceiver(robot);
		interpreter.setInputProvider(robot);
	}

	public Exercise11(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise11(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public int runRobot() {
		interpreter.reRunProgram();
		return robot.paintedPanels.size();
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2019, 11));

		// processInput not needed

		// part 1 - 249 (449ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.runRobot());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - GLBEPJZP (<1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info("\n" + ex.robot.toString());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
