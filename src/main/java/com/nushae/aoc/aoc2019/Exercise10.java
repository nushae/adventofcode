package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.util.MathUtil;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.gcd;

@Log4j2
@Aoc(year = 2019, day = 10, title="Monitoring Station", keywords = {"line-of-sight"})
public class Exercise10 {

	ArrayList<String> lines;
	int[][] canSee;
	boolean[][] asteroids;
	TreeMap<Double, Point> linesOfSight;
	int width;
	int height;
	int max;
	Point bestLocation;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		height = lines.size();
		width = lines.stream().mapToInt(String::length).max().orElse(-1);
		if (width < 0 ) {
			throw new IllegalStateException("Empty map?!");
		}
		asteroids = new boolean[height][width];
		for (int y = 0; y < height; y++) {
			String line = lines.get(y);
			for (int x = 0; x < width; x++) {
				asteroids[y][x] = (x < line.length() && line.charAt(x) == '#');
			}
		}

		// now that we know width and height we can calculate all possible line slopes that could exist between any two grid points.
		// For part 2 they must be sorted by angle so we can go through them clockwise starting from straight up.
		// This can be achieved by using atan2(x, -y) instead of atan2(y, x), increase negative angles by 360, and sorting.
		// On the fly we calculate if an asteroid can be seen in each direction, for each asteroid in the grid
		linesOfSight = new TreeMap<>();
		canSee = new int[height][width];
		for (int dy = 1 - height; dy < height; dy++) {
			for (int dx = 1 - width; dx < width; dx++) {
				if (gcd(dx, dy) == 1 || Math.abs(dx + dy) == 1) { // automatically excludes (0,0)
					double angle = MathUtil.toPositiveDegrees(Math.atan2(dx, -dy));
					log.debug(String.format("%d, %d -> %3.2f", dx, dy, angle));
					linesOfSight.put(angle, new Point(dx, dy));
					findAsteroidInDirection(dx, dy);
				}
			}
		}

		// now calc max val in canSee:
		max = Integer.MIN_VALUE;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int val = canSee[y][x];
				if (max < val) {
					bestLocation = new Point(x, y);
					max = val;
				}
			}
		}
	}

	public void findAsteroidInDirection(int dx, int dy) {
		log.debug("processing dx = " + dx + " / dy = " + dy);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (asteroids[y][x]) {
					for (int i = 1; 0 <= x + i * dx && x + i * dx < width && 0 <= y + i * dy && y + i * dy < height; i++) {
						log.debug("Testing " + x + ", " + y + " against " + (x + i * dy) + ", " + (y + i * dy));
						if (asteroids[y + i * dy][x + i * dx]) {
							canSee[y][x]++;
							break;
						}
					}
				}
			}
		}
	}

	public int doPart1() {
		return max;
	}

	public int doPart2(int order) {
		log.debug("Zapping from " + bestLocation + " in " + width + "x" + height);
		boolean[][] roids = new boolean[height][width];
		for (int r= 0 ; r < height; r++) {
			System.arraycopy(asteroids[r], 0, roids[r], 0, width);
		}
		int zapped = 0;
		boolean zappedSomething = true;
		while (zapped < order && zappedSomething) {
			zappedSomething = false;
			for (Map.Entry<Double, Point> entry: linesOfSight.entrySet()) {
				int dx = entry.getValue().x;
				int dy = entry.getValue().y;
				log.debug("Zapping in direction " + entry.getKey() + " (" + dx + ", " + dy + ")");
				int testX = bestLocation.x;
				int testY = bestLocation.y;
				for (int i=1; 0 <= testX + i * dx && testX + i * dx < width && 0 <= testY + i * dy && testY + i * dy < height; i++) {
					log.debug("Testing " + testX + ", " + testY + " against " + (testX + i * dx) + ", " + (testY + i * dy));
					if (roids[testY + i * dy][testX + i * dx]) {
						zappedSomething = true;
						zapped++;
						log.debug("Zapped asteroid #" + zapped + ": " + (testX + i * dx) + ", " + (testY + i * dy));
						roids[testY + i * dy][testX + i * dx] = false;
						if (zapped == order) {
							return (testX + i * dx) * 100 + testY + i * dy;
						} else {
							break;
						}
					}
				}
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2019, 10));

		// Process input (325ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 227 (<1ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 523 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(200));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
