package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.AutoPlayer;
import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
@Aoc(year = 2019, day = 25, title="Cryostasis", keywords = {"intcode", "text-adventure", "autoplay"})
public class Exercise25 {

	private final IntCodeInterpreter turing;
	private final AutoPlayer nintendo;

	public Exercise25() {
		turing = new IntCodeInterpreter();
		nintendo = new AutoPlayer();
		turing.setOutputReceiver(nintendo);
		turing.setInputProvider(nintendo);
	}

	public Exercise25(Path path) {
		this();
		turing.loadProgramFromFile(path);
	}

	public Exercise25(String data) {
		this();
		turing.loadProgramFromString(data);
	}

	// Did some manual exploring with an Observer to see all output, and NO input provider, so the IntCode
	// computer falls back on manual input. Using that analysis I then added more and more automation.
	// 1) The plan was to simply exhaustively walk around, pick everything up (that isn't blacklisted),
	//    then try every combination until one works
	// 2) There are some trap items! For example trying to move after picking up the giant electromagnet:
	//    "The giant electromagnet is stuck to you.  You can't move!!" -> introduced a blacklist for such items
	// 3) I decided to halt the autoplayer if it ever gets an unexpected response, to see if I can
	//    make it handle the situation or not (eg. blacklist the giant electromagnet ;)
	public BigInteger doPart1() {
		turing.reRunProgram();
		BigInteger result = nintendo.getLastOutput();
		if (result == null) {
			// Program halted for some unknown reason, probably because we died.
			// Ultimately we should add the item to blacklist and replay, while preserving state.
			// For now just report it like when we get unexpected output.
			System.out.println("We died, I think. Last command: " + nintendo.getState().getLastCommand());
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2019, 25));

		// processInput not needed

		// part 1 -> type 'auto' - 20483 (17.238s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// as is tradition, no part 2 for day 25
	}
}
