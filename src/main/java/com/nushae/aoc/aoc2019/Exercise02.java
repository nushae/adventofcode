package com.nushae.aoc.aoc2019;

import com.nushae.aoc.aoc2019.intcode.IntCodeInterpreter;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

/**
 * This is the first part of the series of IntCode exercises (the next one is Exercise05)
 * All exercises make use of the IntCode package (see there)
 */
@Log4j2
@Aoc(year = 2019, day = 2, title="1202 Program Alarm", keywords = {"intcode", "arithmetic", "brute-force"})
public class Exercise02 {

	IntCodeInterpreter interpreter;

	private Exercise02() {
		interpreter = new IntCodeInterpreter();
	}

	public Exercise02(Path path) {
		this();
		interpreter.loadProgramFromFile(path);
	}

	public Exercise02(String data) {
		this();
		interpreter.loadProgramFromString(data);
	}

	public BigInteger doPart1(int noun, int verb) {
		interpreter.resetState();
		// modify program:
		interpreter.poke(BigInteger.ONE, BigInteger.valueOf(noun));
		interpreter.poke(BigInteger.valueOf(2), BigInteger.valueOf(verb));
		// now run WITHOUT resetting:
		return new BigInteger(interpreter.runProgram().split(",")[0]);
	}

	public String runProgramNoInputs() {
		return interpreter.reRunProgram();
	}

	// Brute force all possible inputs, ignore programs that fail on unrecognized opcodes or similar errors, since not
	// all inputs will produce valid programs, and we're only interested in the one that produces the desired output.
	public int doPart2(BigInteger output) {
		for (int noun = 0; noun < 100; noun++) {
			for (int verb = 0; verb < 100; verb++) {
				try {
					interpreter.resetState();
					BigInteger result = doPart1(noun, verb);
					if (result.equals(output)) {
						return 100 * noun + verb;
					}
				} catch (IllegalArgumentException e) {
					// program failed, just ignore
				}
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2019, 2));

		// Process input not needed

		// Part 1 - 2842648 (6ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(12, 2));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// Part 2 - 9074 (1.158s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(BigInteger.valueOf(19690720)));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

	}

}
