package com.nushae.aoc.aoc2019;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.stream.IntStream;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2019, day = 4, title="Secure Container", keywords = {"password", "brute-force"})
public class Exercise04 {
	private int lower;
	private int upper;
	private ArrayList<String> lines; // gross overkill, we only get one line

	public Exercise04(Path path) {
		lines = new ArrayList<>();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		lines = new ArrayList<>();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		// only one line expected; so only the last line read is remembered
		lines.forEach(s -> {
			lower = Integer.parseInt(s.substring(0,6));
			upper = Integer.parseInt(s.substring(7,13));
		});
	}

	public long doPart1() {
		return IntStream.range(lower, upper)
				.filter(this::hasAdjacentRepeat)
				.filter(this::isAscending)
				.count();
	}

	public long doPart2() {
		return IntStream.range(lower, upper)
				.filter(this::hasAdjacentRepeatOfTwo)
				.filter(this::isAscending)
				.count();
	}

	public boolean isAscending(int num) {
		int previousDigit = 10; // anything larger than any single digit will do
		while (num > 0) {
			int lastDigit = num % 10;
			num = num / 10;
			if (lastDigit > previousDigit) {
				return false;
			}
			previousDigit = lastDigit;
		}
		return true;
	}

	public boolean hasAdjacentRepeat(int num) {
		int previousDigit = 10; // anything definitely not equal to any single digit will do
		while (num > 0) {
			int lastDigit = num % 10;
			num = num / 10;
			if (lastDigit == previousDigit) {
				return true;
			}
			previousDigit = lastDigit;
		}
		return false;
	}

	public boolean hasAdjacentRepeatOfTwo(int num) {
		int sequence = 0;
		int previousDigit = 10; // anything definitely not equal to any single digit will do
		while (num > 0) {
			int lastDigit = num % 10;
			num = num / 10;
			if (lastDigit == previousDigit) {
				sequence++;
			} else {
				if (sequence == 2) {
					return true;
				}
				sequence = 1;
			}
			previousDigit = lastDigit;
		}
		return sequence == 2;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2019, 4));

		// Process input (3ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Input processed.");
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1610 (0.007s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1104 (0.009s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
