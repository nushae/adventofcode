package com.nushae.aoc;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.nushae.aoc.LeaderBoardCalculator.SortOrder.*;
import static com.nushae.aoc.util.IOUtil.lbPath;

/**
 * parses an AOC leaderboard json file and shows scores broken down by day/star (because I like that)
 */
@Log4j2
public class LeaderBoardCalculator {

	private static final boolean TESTALL = false;
	private static final int TIMESTAMP_COLUMNS = 3;

	public enum SortOrder {
		DELTAS,
		NAMES,
		SCORES,
		TIMESTAMPS
	}

	private static final ZoneId AOC_TIME = ZoneId.of("America/New_York");

	private static final DateTimeFormatter DAY_ONLY = DateTimeFormatter.ofPattern("dd");
	private static final DateTimeFormatter COMPACT_TIME = DateTimeFormatter.ofPattern("HH:mm:ss");

	// for json parsing
	private class LeaderBoard {
		private String event;
		private String owner_id;
		private Map<String, Member> members;
	}
	private class Member {
		private String name;
		private int stars;
		private int global_score;
		private int local_score;
		private String id;
		private long last_star_ts;
		private Map<Integer, Map<Integer, Timestamp>> completion_day_level;
	}
	private class Timestamp {
		String get_star_ts;
	}

	// convenience object for storing computed stats
	private static class PlayerStats {
		private final String name;
		private final int[][] scores = new int[25][2];
		private final ZonedDateTime[][] timestamps = new ZonedDateTime[25][2];
		private final Duration[] deltas = new Duration[25];
		private int totalScore = 0;
		private int rank = 0;
		private int pointsDiffWithNextUp = 0;
		private Duration minDelta;
		private Duration maxDelta;
		private Duration avgDelta;
		private Duration medianDelta;

		public PlayerStats(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setScore(int day, int part, int score) {
			scores[day][part] = score;
		}

		public int getScore(int day, int part) {
			return scores[day][part];
		}

		public void setTimestamp(int day, int part, long epoch) {
			if (epoch > 0) { // no use setting that value!
				timestamps[day][part] = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), AOC_TIME);
			}
			if (timestamps[day][0] != null && timestamps[day][1] != null) {
				deltas[day] = Duration.between(timestamps[day][0], timestamps[day][1]);
				recalcMeanAndAvg();
			}
		}

		private void recalcMeanAndAvg() {
			List<Duration> sortMe = new ArrayList<>();
			long total = 0;
			for (Duration d : deltas) {
				if (d != null) {
					total += d.toMillis();
					sortMe.add(d);
				}
			}

			Collections.sort(sortMe);
			int count = sortMe.size();
			minDelta = sortMe.get(0);
			maxDelta = sortMe.get(count-1);
			avgDelta = Duration.ofMillis(Math.round(total/(count*1.0)));
			medianDelta = count % 2 == 1 ? sortMe.get(count/2) : Duration.ofMillis(Math.round((sortMe.get(count/2-1).toMillis() + sortMe.get(count/2).toMillis())/2.0));
		}

		public ZonedDateTime getTimestamp(int day, int part) {
			return timestamps[day][part];
		}

		public Duration getDelta(int day) {
			return deltas[day];
		}

		public Duration getMinDelta() {
			return minDelta;
		}

		public Duration getMaxDelta() {
			return maxDelta;
		}

		public Duration getAvgDelta()  {
			return avgDelta;
		}

		public Duration getMedianDelta() {
			return medianDelta;
		}

		public void setRank(int r) {
			rank = r;
		}

		public int getRank() {
			return rank;
		}

		public void setTotal(int total) {
			totalScore = total;
		}

		public int getTotal() {
			return totalScore;
		}

		public void setPointsDiffWithNextUp(int pointsDiffWithNextUp) {
			this.pointsDiffWithNextUp = pointsDiffWithNextUp;
		}

		public int getPointsDiffWithNextUp() {
			return pointsDiffWithNextUp;
		}
	}

	private final Map<String, long[][]> timestamps;
	private Map<String, PlayerStats> stats;
	private final boolean skipFirstDay; // for that thing in 2020
	private int eventYear;
	private int maxDay;

	public LeaderBoardCalculator(boolean skipFirstDay, Path path, boolean weightedScores) {
		this.skipFirstDay = skipFirstDay;
		timestamps = new HashMap<>();
		readJsonFile(path);
		computeStats(weightedScores);
	}

	private void readJsonFile(Path path) {
		try (Reader reader = Files.newBufferedReader(path)) {
			LeaderBoard leaderBoard = new Gson().fromJson(reader, LeaderBoard.class);
			eventYear = Integer.parseInt(leaderBoard.event);
			maxDay = getHighestDay();
			for (Map.Entry<String, Member> m : leaderBoard.members.entrySet()) {
				String name = m.getValue().name;
				if (name == null) {
					name = "(id:" + m.getValue().id + ")";
				}
				long[][] scr = new long[25][2];
				Map<Integer, Map<Integer, Timestamp>> completionDayLevel = m.getValue().completion_day_level;
				for (Map.Entry<Integer, Map<Integer, Timestamp>> d : completionDayLevel.entrySet()) {
					int day = d.getKey()-1;
					if (d.getValue().get(1) != null) {
						scr[day][0] = Long.parseLong(d.getValue().get(1).get_star_ts);
					}
					if (d.getValue().get(2) != null) {
						scr[day][1] = Long.parseLong(d.getValue().get(2).get_star_ts);
					}
				}
				timestamps.put(name, scr);
			}
		} catch (IOException e) {
			log.error("File error reading JSON", e);
			System.exit(0);
		}
	}

	public void computeStats(boolean weighted) {
		stats = new HashMap<>();
		// compute scores, submission times and deltas
		for (Map.Entry<String, long[][]> playerTimestamps: timestamps.entrySet()) {
			PlayerStats p = new PlayerStats(playerTimestamps.getKey());
			int total = 0;
			for (int day=0; day<25; day++) {
				for (int part=0; part<2; part++) {
					long ts = playerTimestamps.getValue()[day][part];
					p.setTimestamp(day, part, ts);
					if (ts == 0) {
						p.setScore(day, part, 0);
					} else {
						if (weighted) {
							p.setScore(day, part, (day+1) * (timestamps.size() - countEarlier(p.getName(), day, part, ts)));
						} else {
							p.setScore(day, part, timestamps.size() - countEarlier(p.getName(), day, part, ts));
						}
					}
					if (!skipFirstDay || day > 0) { // due to outing, day 1 does not count towards total for 2020 edition
						total += p.getScore(day, part);
					}
				}
			}
			p.setTotal(total);
			// rank and diff stay open for now, we need all the totals first
			stats.put(p.name, p);
		}

		// compute ranks and diffs between consecutive players
		int total = 0;
		int previousTotal;
		int rank = 1;
		for (Map.Entry<String, PlayerStats> entry : stats.entrySet().stream().sorted(Comparator.comparingLong((Map.Entry<String, PlayerStats> a) -> a.getValue().getTotal()).reversed()).collect(Collectors.toList())) {
			previousTotal = total;
			total = entry.getValue().getTotal();
			entry.getValue().setPointsDiffWithNextUp(previousTotal - total);
			entry.getValue().setRank(rank++);
		}
	}

	private int countEarlier(String name, int day, int part, long timestamp) {
		int accumulator = 0;
		for (Map.Entry<String, long[][]> playerTimestamps: timestamps.entrySet()) {
			if (!name.equals(playerTimestamps.getKey())) {
				long otherTimestamp = playerTimestamps.getValue()[day][part];
				if (otherTimestamp != 0 && otherTimestamp < timestamp) {
					accumulator++;
				}
			}
		}
		return accumulator;
	}

	private int getHighestDay() {
		LocalDate today = LocalDate.now();
		if (eventYear > today.getYear()) {
			return 25; // event finished
		} else if (eventYear == today.getYear()) {
			if (today.getMonthValue() == 12) {
				return Math.min(today.getDayOfMonth(), 25);
			}
		}
		return 0; // hasn't started yet!
	}

	// list printers

	private void prettyPrintScores(SortOrder sortBy) {
		int minDay = Math.max(maxDay-TIMESTAMP_COLUMNS+1, 1);
		System.out.print("Scores per submission, ");
		switch (sortBy) {
			case DELTAS -> {
				System.out.println("sorted by delta (if any) for most recent day.");
				prettyPrintScores(minDay, maxDay, this::byDelta);
			}
			case NAMES -> {
				System.out.println("sorted by player name.");
				prettyPrintScores(minDay, maxDay, this::byName);
			}
			case SCORES -> {
				System.out.println("sorted by total score.");
				prettyPrintScores(minDay, maxDay, this::byTotalScore);
			}
			case TIMESTAMPS -> {
				System.out.println("sorted by timestamp (if any) of part 2 for most recent day");
				prettyPrintScores(minDay, maxDay, this::byTimestamps);
			}
		}
	}
	private void prettyPrintScores(int minDay, int maxDay, Comparator<PlayerStats> comparator) {
		System.out.printf("%30s  ##  total  (lag)", "Name");
		for (int i = minDay-1; i < maxDay; i++) {
			System.out.printf("  %02d___", i+1);
		}
		System.out.println();
		for (PlayerStats pStats : stats.values().stream().sorted(comparator).collect(Collectors.toList())) {
			System.out.printf("%30s  %2d", pStats.getName(), pStats.getRank());
			int total = pStats.getTotal();
			int diff = pStats.getPointsDiffWithNextUp();
			StringBuilder infoLine = new StringBuilder();
			infoLine.append(String.format("  %5d %6s", total, diff > 0 ? "(" + diff + ")" : ""));
			for (int day = minDay-1; day < maxDay; day++) {
				int part1 = pStats.getScore(day, 0);
				int part2 = pStats.getScore(day, 1);
				infoLine.append("  ").append(part1 > 0 ? String.format("%02d", part1) : "..").append(part2 > 0 ? String.format(",%02d", part2) : " ..");
			}
			System.out.println(infoLine);
		}
	}

	private void toCSV(SortOrder sortBy) {
		int minDay = Math.max(maxDay-TIMESTAMP_COLUMNS+1, 1);
		System.out.println("CSV of player stats ");
		switch (sortBy) {
			case DELTAS -> {
				System.out.println("sorted by delta (if any) for most recent day.");
				toCSV(minDay, maxDay, this::byDelta);
			}
			case NAMES -> {
				System.out.println("sorted by player name.");
				toCSV(minDay, maxDay, this::byName);
			}
			case SCORES -> {
				System.out.println("sorted by total score.");
				toCSV(minDay, maxDay, this::byTotalScore);
			}
			case TIMESTAMPS -> {
				System.out.println("sorted by timestamp (if any) of part 2 for most recent day");
				toCSV(minDay, maxDay, this::byTimestamps);
			}
		}
	}
	private void toCSV(int minDay, int maxDay, Comparator<PlayerStats> comparator) {
		System.out.print("Name\tTotal");
		for (int i = minDay-1; i < maxDay; i++) {
			System.out.printf("\t%d\ttime1\ts1\t\ttime2\ts2", i+1);
		}
		System.out.println();
		for (PlayerStats pStats : stats.values().stream().sorted(comparator).collect(Collectors.toList())) {
			System.out.printf("%s\t%d", pStats.getName(), pStats.getTotal());
			StringBuilder infoLine = new StringBuilder();
			for (int day = minDay-1; day < maxDay; day++) {
				for (int part = 0; part < 2; part++) {
					ZonedDateTime ts = pStats.getTimestamp(day, part);
					if (ts != null) {
						infoLine.append(String.format("\t%s", DAY_ONLY.format(ts)))
								.append(String.format("\t%s", COMPACT_TIME.format(ts)));
					} else {
						infoLine.append("\t\t");
					}
					infoLine.append(pStats.getScore(day, part) > 0 ? String.format("\t%d", pStats.getScore(day, part)) : "\t");
				}
			}
			System.out.println(infoLine );
		}
	}

	private void prettyPrintTimeStamps(SortOrder sortBy, boolean printDeltas) {
		int minDay = Math.max(maxDay-TIMESTAMP_COLUMNS+1, 1);
		System.out.print("Submission times per part" + (printDeltas ? ", with deltas, " : " "));
		switch (sortBy) {
			case DELTAS -> {
				System.out.println("sorted by delta (if any) for most recent day.");
				prettyPrintTimeStamps(minDay, maxDay, this::byDelta, printDeltas);
			}
			case NAMES -> {
				System.out.println("sorted by player name.");
				prettyPrintTimeStamps(minDay, maxDay, this::byName, printDeltas);
			}
			case SCORES -> {
				System.out.println("sorted by total score.");
				prettyPrintTimeStamps(minDay, maxDay, this::byTotalScore, printDeltas);
			}
			case TIMESTAMPS -> {
				System.out.println("sorted by submission time (if any) of part 2 for most recent day");
				prettyPrintTimeStamps(minDay, maxDay, this::byTimestamps, printDeltas);
			}
		}
	}
	private void prettyPrintTimeStamps(int minDay, int maxDay, Comparator<PlayerStats> comparator, boolean printDeltas) {
		System.out.printf("%32s  ##  total  (lag)", "Name");
		for (int i = minDay-1; i < maxDay; i++) {
			System.out.printf(" | _______%02d.1 %s_______%02d.2", i+1, printDeltas ? String.format("___%02d.delta ", i+1) : "", i+1);
		}
		System.out.println(" | __delta_min __delta_max __delta_avg __delta_mdn");
		for (PlayerStats pStats : stats.values().stream().sorted(comparator).collect(Collectors.toList())) {
			System.out.printf("%32s  %2d", pStats.getName(), pStats.getRank());
			StringBuilder infoLine = new StringBuilder();
			infoLine.append(String.format("  %5d %6s", pStats.getTotal(), pStats.getPointsDiffWithNextUp() > 0 ? "(" + pStats.getPointsDiffWithNextUp() + ")" : ""));
			for (int day = minDay-1; day < maxDay; day++) {
				infoLine.append(" |");
				ZonedDateTime ts1 = pStats.getTimestamp(day, 0);
				ZonedDateTime ts2 = pStats.getTimestamp(day, 1);
				infoLine.append(ts1 != null ? makeMinimalTimeString(day, ts1.getDayOfMonth(), ts1.getHour(), ts1.getMinute(), ts1.getSecond()) : " ...........");
				if (printDeltas && pStats.getDelta(day) != null) {
					infoLine.append(makeMinimalTimeString(pStats.getDelta(day)));
				} else if (printDeltas) {
					infoLine.append(" ...........");
				}
				infoLine.append(ts2 != null ? makeMinimalTimeString(day, ts2.getDayOfMonth(), ts2.getHour(), ts2.getMinute(), ts2.getSecond()) : " ...........");
			}
			infoLine.append(" |")
				.append(makeMinimalTimeString(pStats.getMinDelta()))
				.append(makeMinimalTimeString(pStats.getMaxDelta()))
				.append(makeMinimalTimeString(pStats.getAvgDelta()))
				.append(makeMinimalTimeString(pStats.getMedianDelta()));
			System.out.println(infoLine);
		}
	}

	private String makeMinimalTimeString(Duration delta) {
		if (delta != null) {
			long deltaSeconds = delta.toMillis() / 1000;
			int hours = (int) (deltaSeconds / 3600);
			int days = hours / 24;
			hours = hours % 24;
			deltaSeconds = deltaSeconds % 3600;
			int minutes = (int) (deltaSeconds / 60);
			int seconds = (int) (deltaSeconds % 60);
			return makeMinimalTimeString(-1, days, hours, minutes, seconds);
		}
		return(" ...........");
	}

	private String makeMinimalTimeString(int day, int days, int hours, int mins, int secs) {
		String text;
		if (days == day + 1 || days == 0) {
			text = "...";
			if (hours > 0) {
				text += (hours > 9 ? "" + hours/10 : ".") + hours%10 + ":";
			} else {
				text += "...";
			}
			text += (mins > 9 ? "" + mins/10 : hours > 0 ? "0" : ".") + mins%10 + ":";
			text += String.format("%02d", secs);
		} else {
			text = "" + (days > 9 ? days/10 : ".") + days%10 + "d" + String.format("%02d:%02d:%02d", hours, mins, secs);
		}
		return " " + text;
	}

	// comparators for the list printers

	private int byDelta(PlayerStats left, PlayerStats right) {
		Duration leftDelta = left.getDelta(maxDay-1);
		Duration rightDelta = right.getDelta(maxDay-1);
		if (leftDelta == null) {
			return rightDelta == null ? 0 : 1;
		} else {
			return rightDelta == null ? -1 : leftDelta.compareTo(rightDelta);
		}
	}

	public int byLatestScore(PlayerStats left, PlayerStats right) {
		return Integer.compare(right.getScore(maxDay-1,0)+right.getScore(maxDay-1, 1), left.getScore(maxDay-1,0)+left.getScore(maxDay-1, 1)); // greater than, not lower than
	}

	private int byName(PlayerStats left, PlayerStats right) {
		return left.getName().compareTo(right.getName());
	}

	private int byTimestamps(PlayerStats left, PlayerStats right) {
		ZonedDateTime leftTimestamp1 = left.getTimestamp(Math.max(0, maxDay-1), 0);
		ZonedDateTime rightTimestamp1 = right.getTimestamp(Math.max(0, maxDay-1), 0);
		ZonedDateTime leftTimestamp2 = left.getTimestamp(Math.max(0, maxDay-1), 1);
		ZonedDateTime rightTimestamp2 = right.getTimestamp(Math.max(0, maxDay-1), 1);
		return 100*Long.compare(leftTimestamp2 != null ? leftTimestamp2.toEpochSecond() : Long.MAX_VALUE, rightTimestamp2 != null ? rightTimestamp2.toEpochSecond() : Long.MAX_VALUE)
				+10*Long.compare(leftTimestamp1 != null ? leftTimestamp1.toEpochSecond() : Long.MAX_VALUE, rightTimestamp1 != null ? rightTimestamp1.toEpochSecond() : Long.MAX_VALUE)
				+Long.compare(right.getTotal(), left.getTotal());
	}

	private int byTotalScore(PlayerStats left, PlayerStats right) {
		return Integer.compare(right.getTotal(), left.getTotal()); // greater than, not lower than
	}

	public static void main(String[] args) {
		LeaderBoardCalculator lbc = new LeaderBoardCalculator(false, lbPath(2024, "sogeti"), false);
		if (TESTALL) {
			lbc.prettyPrintTimeStamps(DELTAS, true);
			System.out.println();
			lbc.prettyPrintTimeStamps(NAMES, true);
			System.out.println();
			lbc.prettyPrintTimeStamps(TIMESTAMPS, true);
			System.out.println();
			lbc.prettyPrintTimeStamps(SCORES, true);
			System.out.println();
			lbc.prettyPrintScores(DELTAS);
			System.out.println();
			lbc.prettyPrintScores(NAMES);
			System.out.println();
			lbc.prettyPrintScores(TIMESTAMPS);
			System.out.println();
			lbc.prettyPrintScores(SCORES);
			System.out.println();
			lbc.toCSV(SCORES);
		} else {
			lbc.prettyPrintTimeStamps(TIMESTAMPS, true);
		}
	}
}
