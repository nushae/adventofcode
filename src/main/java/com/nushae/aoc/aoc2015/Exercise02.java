package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 2, title = "I Was Told There Would Be No Math", keywords = {"area", "volume"})
public class Exercise02 {

	ArrayList<String> lines;
	ArrayList<BoxWrap> boxes;

	public record BoxWrap(int x, int y, int z) {

		public BoxWrap(int x, int y, int z) {
			this.x = Math.min(x, Math.min(y, z));
			this.z = Math.max(x, Math.max(y, z));
			this.y = x + y + z - this.x - this.z;
		}

		// smallest perimeter of a side + volume of box
		public int getRibbonNeeded() {
			return 2 * x + 2 * y + x * y * z;
		}

		// surface area of box plus area of smallest side
		public int getPaperNeeded() {
			return 3 * x * y + 2 * y * z + 2 * x * z;
		}
	}

	public Exercise02() {
		lines = new ArrayList<>();
		boxes = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		for (String line : lines) {
			String[] dims = line.split("x");
			boxes.add(new BoxWrap(Integer.parseInt(dims[0]), Integer.parseInt(dims[1]), Integer.parseInt(dims[2])));
		}
	}

	public int doPart1() {
		int accum = 0;
		for (BoxWrap box: boxes) {
			accum += box.getPaperNeeded();
		}
		return accum;
	}

	public int doPart2() {
		int accum = 0;
		for (BoxWrap box: boxes) {
			accum += box.getRibbonNeeded();
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2015, 2));

		// process input (17ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1598415 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 3812909 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
