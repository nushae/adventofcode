package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.util.Arrays;

@Log4j2
@Aoc(year = 2015, day = 17, title = "No Such Thing as Too Much", keywords = {"partitioning"})
public class Exercise17 {

	int[] numbers;

	public Exercise17(int... n) {
		numbers = n;
	}

	public long doPart1(int amount) {
		return countCombos(numbers, amount);
	}

	public long doPart2(int amount) {
		int minimum = findMinimumContainers(numbers, amount, 0);
		log.debug("Minimum: " + minimum + ". Ways to do this:");
		return countCombos2(minimum, numbers, amount, 0);
	}

	public long countCombos(int[] containers, int amount) {
		long combos = 0;
		if (containers[0] == amount) {
			combos += 1;
		}
		if (containers.length > 1) {
			if (containers[0] < amount) {
				combos += countCombos(Arrays.copyOfRange(containers, 1, containers.length), amount - containers[0]);
			}
			combos += countCombos(Arrays.copyOfRange(containers, 1, containers.length), amount);
		}
		return combos;
	}

	public long countCombos2(int budget, int[] containers, int amount, int containersUsedSoFar) {
		long combos = 0;
		if (budget > containersUsedSoFar) {
			if (containers[0] == amount) {
				combos += 1;
			}
			if (containers.length > 1) {
				if (containers[0] < amount) {
					combos += countCombos2(budget, Arrays.copyOfRange(containers, 1, containers.length), amount - containers[0], containersUsedSoFar + 1);
				}
				combos += countCombos2(budget, Arrays.copyOfRange(containers, 1, containers.length), amount, containersUsedSoFar);
			}
		}
		return combos;
	}

	public int findMinimumContainers(int[] containers, int amount, int containersUsedSoFar) {
		if (containers[0] == amount) {
			return containersUsedSoFar + 1;
		}
		if (containers.length > 1) {
			int used = findMinimumContainers(Arrays.copyOfRange(containers, 1, containers.length), amount, containersUsedSoFar);
			if (containers[0] < amount) {
				used = Math.min(used, findMinimumContainers(Arrays.copyOfRange(containers, 1, containers.length), amount - containers[0], containersUsedSoFar+1));
			}
			return used;
		}
		return 500; // high enough to be neutral for the min function
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17(11, 30, 47, 31, 32, 36, 3, 1, 5, 3, 32, 36, 15, 11, 46, 26, 28, 1, 19, 3);

		// process input not needed

		// part 1 - 4372 (18ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(150));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4 (5ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(150));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
