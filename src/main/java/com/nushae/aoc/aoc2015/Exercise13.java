package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 13, title = "Knights of the Dinner Table", keywords = {"optimization"})
public class Exercise13 {

	public static final Pattern HAPPINESS = Pattern.compile("(\\S+) would (gain|lose) (\\d+) happiness units by sitting next to (\\S+)\\.");
	public static final String MYNAME = "@@ME@@";

	ArrayList<String> lines;
	ArrayList<String> names;
	Map<String, Integer> happinessValues;

	public Exercise13() {
		lines = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput(boolean addMe) {
		for (String line : lines) {
			Matcher m = HAPPINESS.matcher(line);
			if (m.find()) {
				String person = m.group(1);
				int change = Integer.parseInt(m.group(3)) * ("gain".equals(m.group(2)) ? 1 : -1);
				String neighbour = m.group(4);
				happinessValues.put(person + "->" + neighbour, change);
				if (!names.contains(person)) {
					names.add(person);
				}
			} else {
				log.warn("This line confused me: " + line);
			}
		}
		if (addMe) {
			for (String neighbour : names) {
				happinessValues.put(MYNAME + "->" + neighbour, 0);
				happinessValues.put(neighbour + "->" + MYNAME, 0);
			}
			names.add(MYNAME);
		}
		Collections.sort(names);
	}

	public long permuteAndCalcMax(String... elements) {
		int[] indexes = new int[elements.length];
		long accumulator = calcHappiness(elements);
		int i = 0;
		while (i < indexes.length) {
			if (indexes[i] < i) {
				int j = i % 2 == 0 ?  0 : indexes[i];
				String tmp = elements[i];
				elements[i] = elements[j];
				elements[j] = tmp;
				accumulator = Math.max(accumulator, calcHappiness(elements));
				indexes[i]++;
				i = 0;
			}
			else {
				indexes[i] = 0;
				i++;
			}
		}
		return accumulator;
	}

	public long calcHappiness(String... elements) {
		long accum = 0;
		for (int i=0; i< elements.length; i++) {
			String person = elements[i];
			String leftNeighbour = elements[(i + elements.length - 1) % elements.length];
			String rightNeighbour = elements[(i + 1) % elements.length];
			accum += happinessValues.get(person + "->" + leftNeighbour) + happinessValues.get(person + "->" + rightNeighbour);
		}
		return accum;
	}

	public long doPart1() {
		names = new ArrayList<>();
		happinessValues = new HashMap<>();
		processInput(false);
		return permuteAndCalcMax(names.toArray(new String[0]));
	}

	public long doPart2() {
		names = new ArrayList<>();
		happinessValues = new HashMap<>();
		processInput(true);
		return permuteAndCalcMax(names.toArray(new String[0]));
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2015, 13));

		// input processing included in parts 1 and 2

		// part 1 - 709 (76ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 668 (293ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
