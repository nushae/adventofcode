package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 8, title = "Matchsticks ", keywords = {"substitution", "escaping", "unescaping"})
public class Exercise08 {

	public static final String BACKSLASH_DQUOTE = "\\\\\\\"";
	public static final String BACKSLASH_BACKSLASH = "\\\\\\\\";
	public static final String BACKSLASH_HEXCHAR = "\\\\x[0-9a-fA-F][0-9a-fA-F]";
	public static final String BACKSLASH = "\\\\";
	public static final String DQUOTE = "\\\"";

	ArrayList<String> lines;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long result = 0;
		for (String line : lines) {
			String replacement = line
					.replaceAll(BACKSLASH_BACKSLASH, "@")
					.replaceAll(BACKSLASH_DQUOTE, "@")
					.replaceAll(BACKSLASH_HEXCHAR, "@");
			log.debug(line + " -> " + replacement);
			result += line.length() - replacement.length() + 2;
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		for (String line : lines) {
			String replacement = line
					.replaceAll(BACKSLASH, "@@")
					.replaceAll(DQUOTE, "@@");
			log.debug(line + " -> " + replacement);
			result += replacement.length() + 2 - line.length();
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2015, 8));

		// process input not needed

		// part 1 - 1371 (18ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2117 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
