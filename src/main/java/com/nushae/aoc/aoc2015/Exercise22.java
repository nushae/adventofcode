package com.nushae.aoc.aoc2015;

import com.nushae.aoc.aoc2015.domain.GameState;
import com.nushae.aoc.aoc2015.domain.Spell;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.SearchNode;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.nushae.aoc.util.SearchUtil.dfs;

@Log4j2
@Aoc(year = 2015, day = 22, title = "Wizard Simulator 20XX", keywords = {"min-maxing", "game"})
public class Exercise22 {

	final int BOSS_HP;
	final int BOSS_DMG;
	final int PLAYER_HP;
	final int PLAYER_MANA;

	public Exercise22(int myHp, int myMana, int theirHp, int theirDamage) {
		PLAYER_HP = myHp;
		PLAYER_MANA = myMana;
		BOSS_HP = theirHp;
		BOSS_DMG = theirDamage;
	}

	public long doGame(boolean hardMode) {
		Set<Spell> available = new HashSet<>();
		Map<Spell, Integer> active = new HashMap<>();
		available.add(new Spell("Magic Missile", 53, 0, 4, 0, 0, 0));
		available.add(new Spell("Drain", 73, 0, 2, 2, 0, 0));
		available.add(new Spell("Shield", 113, 6, 0, 0, 7, 0));
		available.add(new Spell("Poison", 173, 6, 3, 0, 0, 0));
		available.add(new Spell("Restore", 229, 5, 0, 0, 0, 101));
		SearchNode result = dfs(new GameState(true, 0, 50, 0, 500, BOSS_HP, BOSS_DMG, available, active, hardMode),
				n -> ((GameState) n).getTheirHp() < 1);
		if (result != null) {
			return ((GameState) result).getSpentSoFar();
		}
		return -1L;
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(50, 500, 51, 9);

		// part 1 - 900 (62ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doGame(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1216 (34ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doGame(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
