package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 19, title = "Medicine for Rudolph", keywords = {"substitution", "combinatorics"})
public class Exercise19 {

	public static final Pattern WORD = Pattern.compile("([A-Z][a-z]*)");
	public final static Pattern PATTERN_SUBSTITIUTION_RULE = Pattern.compile("(.*) => (.*)");

	ArrayList<String> lines;
	String molecule;
	Map<String, List<String>> replaceDict = new HashMap<>();

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		molecule = lines.get(lines.size()-1);
		for (int i = 0; i < lines.size()-1; i++) {
			String line = lines.get(i);
			if (!"".equals(line)) {
				Matcher m = PATTERN_SUBSTITIUTION_RULE.matcher(line);
				if (m.find()) {
					String source = m.group(1);
					List<String> dest = replaceDict.getOrDefault(source, new ArrayList<>());
					dest.add(m.group(2));
					replaceDict.put(source, dest);
				} else {
					log.warn("I don't know what to do with '" + line + "'");
				}
			}
		}
	}

	public long doPart1() {
		Set<String> result = new HashSet<>();
		for (int i = 0; i < molecule.length(); i++) {
			for (String key : replaceDict.keySet()) {
				if (molecule.substring(i).startsWith(key)) {
					List<String> replacements = replaceDict.getOrDefault(key, new ArrayList<>());
					for (String rep : replacements) {
						String newMol = molecule.substring(0, i) + rep + molecule.substring(i + key.length());
						result.add(newMol);
					}
				}
			}
		}
		return result.size();
	}

	// Some analysis for part 2 (it took me almost a week to figure this out):
	//
	// We're asked for fewest number of steps only, not an exact construction. We're dealing with production rules in
	// a grammar here, so it might be useful to see if there is a structure hidden in here.
	//
	// Looking at the left sides of the rules it becomes obvious that all left sides are of the form [A-Z][a-z]* so
	// we start by trying to tokenize the molecule that way (I'll put spaces between the tokens to make this obvious).
	//
	// Analyzing the right sides with the analyseInput() method we find four more tokens that don't occur on the left side:
	// Ar, C, Y, and Rn.
	//
	// The rules break apart as follows (N is any nonterminal token):
	//    e -> N N
	//    N -> N N
	//    N -> N Rn N Ar | N Rn N Y N Ar | N Rn N Y N Y N Ar
	//    N -> C Rn N Ar | C Rn N Y N Ar | C Rn N Y N Y N Ar
	//
	// Basically the Rn is a left bracket and the Ar a right bracket, and Y is a comma. C is just a special case so
	// let's include it in the N. We get:
	//    e -> N N
	//    N -> N N
	//    N -> N ( N ) | N ( N , N ) | N ( N , N , N )
	//
	// Any output produced by these rules will consist of properly nested brackets, commas, and strings of 1 or more N.
	// The strings NN+ can only have come from repeatedly selecting one of the N -> NN rules and applying it. (We don't
	// care which rule, just that it increases the list of tokens by 1 for each application).
	// Without the N -> NN rules only outputs with single N's between the brackets and commas can be made. Such a
	// string must, somewhere, have either the sequence N ( N ) or N ( N , N )  or N ( N , N , N ) which was produced
	// by the corresponding N -> N (...) rule. These increase the token length by 3, 5, and 7 respectively.
	//
	// These different increases result from the added () and the extra comma's and N's, so let's look for an invariant
	// over the steps taken and number and type of tokens in the final output.
	//
	// 1) Say we only applied N -> NN, then the length (in tokens) of the final output is always 1 more than the number
	// of steps taken. In other words, we'd have the invariant:
	//
	//    steps = output.count(any token) - 1
	//
	// (Any token just always happens to be an N token). If at any point we applied N -> (N), then the number of tokens
	// would be increased by an additional 2 (one for each bracket) for each such step taken. We need to account for this.
	// This leads to a new invariant that works for both rules:
	//
	//    steps = output.count(any token) - input.count(bracket token) - 1
	//
	// (Any token is now either N or any bracket). If we applied N -> ( N , N ) then the number of tokens would be
	// increased with yet ANOTHER 2 (the comma and the extra N) and for N -> ( N , N , N ) we get 4 extra (2 commas and
	// 2 N). In other words, these two rules increase the tokens by an additional 2 per comma they generate, on top of
	// the 2 from the brackets already accounted for by our current invariant. The new invariant becomes:
	//
	//    steps = output.count(any token) - output.count(bracket token) - 2 * output.count(comma token) - 1
	//
	// We can simply calculate these 3 values on the fly as we tokenize... O(N) solution!
	public long doPart2() {
		Matcher m = WORD.matcher(molecule);
		long totalTokens = 0;
		long totalBrackets = 0;
		long totalCommas = 0;
		while (m.find()) {
			String token = m.group(1);
			totalTokens++;
			if ("Ar".equals(token) || "Rn".equals(token)) {
				totalBrackets++;
			}
			if ("Y".equals(token)) {
				totalCommas++;
			}
		}
		return totalTokens - totalBrackets - 2 * totalCommas - 1;
	}

	public void analyseInput() {
		Set<String> terminals = new HashSet<>();
		for (Map.Entry<String, List<String>> replacements : replaceDict.entrySet()) {
			for (String phrase : replacements.getValue()) {
				// toggle between phrase and !phrase on the following line to see the other half of the rules
				if (!phrase.contains(replacements.getKey())) {
					System.out.print(replacements.getKey() + " ->");
					Matcher m = WORD.matcher(phrase);
					while (m.find()) {
						String word = m.group(1);
						if (replaceDict.containsKey(word)) {
							System.out.print(" [");
						} else {
							terminals.add(word);
							System.out.print(" ");
						}
						System.out.print(word);
						if (word.equals(replacements.getKey())) {
							System.out.print("!");
						}
						if (replaceDict.containsKey(word)) {
							System.out.print("]");
						}
					}
					System.out.println();
				}
			}
		}
		System.out.println("Terminals: " + terminals);
		System.out.println(molecule
				.replaceAll("C(?!a)", "#")
				.replaceAll("Rn", "(")
				.replaceAll("Ar", ")")
				.replaceAll("Y", ","));
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2015, 19));

		// process input (8ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 509 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// Part 2 required some analysis. Uncomment the following lines to tinker with that yourself
//		ex.analyseInput();
//		System.exit(0);

		// part 2 - 195 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
