package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.function.Predicate;

import static com.nushae.aoc.util.IOUtil.*;
import static org.apache.commons.lang3.StringUtils.join;

@Log4j2
@Aoc(year = 2015, day = 5, title = "Doesn't He Have Intern-Elves For This?", keywords = {"filtering", "pattern-matching"})
public class Exercise05 {

	ArrayList<String> lines;

	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long applyPredicate(Predicate<String> criterion) {
		return lines.stream().filter(criterion).count();
	}

	public long doPart1() {
		return applyPredicate(hasVowels(3).and(hasDoubleLetter()).and(hasNoTabooWord("ab", "cd", "pq", "xy")));
	}

	public long doPart2() {
		return applyPredicate(hasDoubleSkipOne().and(hasDoublePair()));
	}

	public static Predicate<String> hasDoubleLetter() {
		return p -> p.matches(".*(.)\\1.*");
	}

	public static Predicate<String> hasVowels(int amount) {
		return p -> p.replaceAll("[bcdfghjklmnpqrstvwxyz]", "").length() >= amount;
	}

	public static Predicate<String> hasNoTabooWord(String... words) {
		return p -> !p.matches(".*("+ join(words,"|")+").*");
	}

	public static Predicate<String> hasDoubleSkipOne() {
		return p -> p.matches(".*(.).\\1.*");
	}

	public static Predicate<String> hasDoublePair() {
		return p -> p.matches(".*(..).*\\1.*");
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2015, 5));

		// process input not needed

		// part 1 - 258 (37ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 53 (8ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
