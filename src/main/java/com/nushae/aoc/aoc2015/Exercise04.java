package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import static com.nushae.aoc.util.AlgoUtil.getMD5;

@Log4j2
@Aoc(year = 2015, day = 4, title = "The Ideal Stocking Stuffer", keywords = {"md5"})
public class Exercise04 {

	private String input;
	private long startFrom = 0;

	public Exercise04(String data) {
		input = data;
	}

	public long doPart1() {
		long postfix = 0;
		while (true) {
			if (getMD5(input + postfix).startsWith("00000")) {
				startFrom = postfix;
				return postfix;
			} else {
				postfix++;
			}
		}
	}

	public long doPart2() {
		// since we need one more 0, the new postfix is probably also one magnitude bigger... that should still be bruteforceable
		// we can skip the result of part 1 if present, for about 10% time saved
		long postfix = startFrom;
		while (true) {
			if (getMD5(input + postfix).startsWith("000000")) {
				return postfix;
			} else {
				postfix++;
			}
		}
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04("ckczppom");

		// process input not needed

		// part 1 - 117946 (160ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 3938038 (1.531s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
