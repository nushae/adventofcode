package com.nushae.aoc.aoc2015;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 12, title = "JSAbacusFramework.io", keywords = {"json", "evaluation"})
public class Exercise12 {

	public static final Pattern PATTERN_NUMBER = Pattern.compile("(-?\\d+)");

	ArrayList<String> lines;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long accum = 0;
		String json = lines.get(0);
		Matcher m = PATTERN_NUMBER.matcher(json);
		while (m.find()) {
			accum += Integer.parseInt(m.group(1));
		}
		return accum;
	}

	public long doPart2() {
		return preOrderTraverse(new Gson().fromJson(lines.get(0), Object.class));
	}

	public long preOrderTraverse(Object obj) {
		long accum = 0;
		if (obj instanceof LinkedTreeMap) {
			LinkedTreeMap<String, Object> node = (LinkedTreeMap<String, Object>) obj;
			// object: count all members unless "red" is present
			boolean hasRed = false;
			for (Map.Entry<String, Object> member : node.entrySet()) {
				if (member.getValue() instanceof String && "red".equals(member.getValue())) {
					hasRed = true;
					break;
				}
			}
			if (!hasRed) {
				for (Map.Entry<String, Object> member : node.entrySet()) {
					accum += preOrderTraverse(member.getValue());
				}
			}
		} else if (obj instanceof ArrayList) {
			ArrayList<Object> node = (ArrayList<Object>) obj;
			// array: count all members
			for (Object member : node) {
				accum += preOrderTraverse(member);
			}
		} else if (obj instanceof String) {
			log.debug("Skipping string as garbage");
			// String: simply skip
		} else if (obj instanceof Double) {
			// number: add
			accum = Math.round((Double)obj);
		} else {
			log.debug("Unrecognized object: " + obj);
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2015, 12));

		// process input not needed

		// part 1 - 119433 (12ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 68466 (33ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
