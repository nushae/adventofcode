package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Aoc(year = 2015, day = 11, title = "Corporate Policy", keywords = {"passwords", "successor"})
public class Exercise11 {

	public static String findNextPW(String pw) {
		do {
			pw = lexicographicIncrement(pw);
		} while (!hasStraight3(pw) || !hasNoOil(pw) || !hasTwoDifferentPairs(pw));
		return pw;
	}

	public static String lexicographicIncrement(String pw) {
		// interpret string as base36, inc, convert back, fix carries:
		return Long.toString(Long.parseLong(pw, 36) +1, 36).replace("10","aa").replace('0', 'a');
	}

	public static boolean hasStraight3(String testme) {
		return testme.matches(".*(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz).*");
	}

	public static boolean hasNoOil(String testme) {
		return !testme.matches(".*[oil].*");
	}

	public static boolean hasTwoDifferentPairs(String testme) {
		// I first took "must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz" to mean
		// that the two pairs must be different letters, but all solutions I see on the web allow same letter pairs...
		// So I guess that's ok then
		return testme.matches(".*(.)\\1.*(.)\\2.*");
	}

	public static void main(String[] args) {

		// process input not needed

		// part 1 - cqjxxyzz (712ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		String replacement = findNextPW("cqjxjnds");
		log.info(replacement);
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - cqkaabcc (2.124s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(findNextPW(replacement));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
