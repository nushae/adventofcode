package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.util.AlgoUtil;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.nushae.aoc.util.AlgoUtil.allCombinations;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 24, title = "It Hangs in the Balance", keywords = {"balanced-sums"})
public class Exercise24 {

	List<String> lines;
	List<Long> numbers;
	List<Long> bestSubset;
	BigInteger bestQE;
	long total;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		total = 0;
		numbers = new ArrayList<>();
		for (String line : lines) {
			long val = Long.parseLong(line);
			numbers.add(val);
			total += val;
		}
		if (total % 3 != 0) {
			log.warn("Total is " + total + " which is not divisible by 3");
			System.exit(0);
		}
		if (total % 4 != 0) {
			log.warn("Total is " + total + " which is not divisible by 4");
			System.exit(0);
		}
	}

	public BigInteger doPart1() {
		// divide into 3 equal groups with lowest QE for group 1
		bestQE = BigInteger.valueOf(numbers.get(numbers.size()-1)).pow(numbers.size());
		long groupSum = total / 3;
		// determine minimum number of numbers to add for group 1 (we can improve this by observing that the input is all odd numbers and total / 3 is even):
		int minCount = 0;
		int partialSum = 0;
		while (partialSum < groupSum) {
			minCount++;
			partialSum += numbers.get(numbers.size() - minCount);
		}
		log.debug("Aiming for " + groupSum + " weight per group");
		log.debug("Minimum # of packages to put in front: " + minCount);
		bestSubset = null;
		for (int groupSize = minCount; bestSubset == null && groupSize < numbers.size(); groupSize++) {
			Long[] arr = new Long[numbers.size()];
			AlgoUtil.allCombinations(groupSize, numbers.toArray(arr)).forEach(s -> testSubset(s, 3, groupSum));
		}
		return bestQE;
	}

	public BigInteger doPart2() {
		// divide into 4 equal groups with lowest QE for group 1
		bestQE = BigInteger.valueOf(numbers.get(numbers.size()-1)).pow(numbers.size());
		long groupSum = total / 4;
		// determine minimum number of numbers to add for group 1 (we can improve this by observing that the input is all odd numbers and total / 4 is even):
		int minCount = 0;
		int partialSum = 0;
		while (partialSum < groupSum) {
			minCount++;
			partialSum += numbers.get(numbers.size() - minCount);
		}
		log.debug("Aiming for " + groupSum + " weight per group");
		log.debug("Minimum # of packages to put in front: " + minCount);
		bestSubset = null;
		for (int groupSize = minCount; bestSubset == null && groupSize < numbers.size(); groupSize++) {
			Long[] arr = new Long[numbers.size()];
			AlgoUtil.allCombinations(groupSize, numbers.toArray(arr)).forEach(s -> testSubset(s, 4, groupSum));
		}
		return bestQE;
	}

	public void testSubset(List<Long> subset, int groups, long groupSum) {
		List<Long> numberCopy = new ArrayList<>(numbers);
		long tot = 0;
		BigInteger qe = BigInteger.ONE;
		for (long n : subset) {
			numberCopy.remove(n);
			tot += n;
			qe = qe.multiply(BigInteger.valueOf(n));
			if (qe.compareTo(bestQE) > 0) {
				break;
			}
		}
		if (tot == groupSum) { // found one
			if (qe.compareTo(bestQE) < 0) {
				if (splitsInto(groupSum, groups - 1, numberCopy)) {
					bestSubset = subset;
					bestQE = qe;
				}
			}
		}
	}

	// tests whether subset can be split into groups subsets each totaling total
	// for my input I first tried without testing this and my answers were accepted, solution provided for completeness
	public boolean splitsInto(long total, int groups, List<Long> subset) {
		Long[] nums = new Long[subset.size()];
		nums = subset.toArray(nums);
		Optional<List<Long>> subsubset = allCombinations(nums).filter(s -> s.stream().reduce(0L, Long::sum) == total).findFirst();
		if (subsubset.isPresent()) {
			if (groups > 2) {
				subset.removeAll(subsubset.get());
				return splitsInto(total, groups - 1, subset);
			}
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2015, 24));

		// process input (6ms)
		long start = System.currentTimeMillis();
		log.info("Process input:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 11846773891 (1.778s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 80393059 (255ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
