package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.AlgoUtil.allPermutations;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 9, title = "All in a Single Night", keywords = {"euler-path", "shortest-distance", "longest-distance"})
public class Exercise09 {

	public static final Pattern PATTERN_DISTANCE = Pattern.compile("([A-Za-z]+) to ([A-Za-z]+) = (\\d+)");

	ArrayList<String> lines;
	ArrayList<String> nodes;
	Map<String, Integer> distances;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		nodes = new ArrayList<>();
		distances = new HashMap<>();
		for (String line : lines) {
			Matcher m = PATTERN_DISTANCE.matcher(line);
			if (m.find()) {
				String source = m.group(1);
				String dest = m.group(2);
				int dist = Integer.parseInt(m.group(3));
				if (!nodes.contains(source)) {
					nodes.add(source);
				}
				if (!nodes.contains(dest)) {
					nodes.add(dest);
				}
				distances.put(source + "-" + dest, dist);
				distances.put(dest + "-" + source, dist);
			} else {
				log.warn(line + " could not be parsed...");
			}
		}
		Collections.sort(nodes);
	}

	public long doPart1() {
		return allPermutations(nodes).mapToLong(this::calcDist).reduce(Math::min).getAsLong();
	}

	public long doPart2() {
		return allPermutations(nodes).mapToLong(this::calcDist).reduce(Math::max).getAsLong();
	}

	// calculator
	public long calcDist(List<String> elements) {
		long accum = 0;
		for (int i = 1; i< elements.size(); i++) {
			String key = elements.get(i-1) + "-" + elements.get(i);
			accum += distances.get(key);
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2015, 9));

		// process input (12ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 207 (30ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 804 (21ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
