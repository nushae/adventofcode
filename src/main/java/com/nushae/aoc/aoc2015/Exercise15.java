package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 15, title = "Science for Hungry People", keywords = {"filtering", "intersection"})
public class Exercise15 {

	public static final Pattern INGREDIENTS = Pattern.compile("(\\S+): capacity (-?\\d+), durability (-?\\d+), flavor (-?\\d+), texture (-?\\d+), calories (-?\\d+)");

	public record Ingredient(int capacity, int durability, int flavor, int texture, int calories) {}

	ArrayList<String> lines;
	ArrayList<String> names;
	Map<String, Ingredient> ingredients;

	public Exercise15() {
		lines = new ArrayList<>();
		names = new ArrayList<>();
		ingredients = new HashMap<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		for (String line : lines) {
			Matcher m = INGREDIENTS.matcher(line);
			if (m.find()) {
				ingredients.put(m.group(1), new Ingredient(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)), Integer.parseInt(m.group(6))));
				names.add(m.group(1));
			}
		}
	}

	// ugly!! (Too lazy to write generic code for generating all k-part partitions of n)
	public long doPart1() {
		int depth = names.size();
		long accum = 0;
		if (depth > 1) {
			for (long first = 0; first < 101; first++) {
				if (depth > 2) {
					for (long second = 0; second < 101 - first; second++) {
						if (depth > 3) {
							for (long third = 0; third < 101 - first - second; third++) {
								long fourth = 100 - first - second - third;
								long totalCapacity = Math.max(0L, ingredients.get(names.get(0)).capacity * first + ingredients.get(names.get(1)).capacity * second + ingredients.get(names.get(2)).capacity * third + ingredients.get(names.get(3)).capacity * fourth);
								long totalDurability = Math.max(0L, ingredients.get(names.get(0)).durability * first + ingredients.get(names.get(1)).durability * second + ingredients.get(names.get(2)).durability * third + ingredients.get(names.get(3)).durability * fourth);
								long totalFlavor = Math.max(0L, ingredients.get(names.get(0)).flavor * first + ingredients.get(names.get(1)).flavor * second + ingredients.get(names.get(2)).flavor * third + ingredients.get(names.get(3)).flavor * fourth);
								long totalTexture = Math.max(0L, ingredients.get(names.get(0)).texture * first + ingredients.get(names.get(1)).texture * second + ingredients.get(names.get(2)).texture * third + ingredients.get(names.get(3)).texture * fourth);
								long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
								accum = Math.max(accum, score);
							}
						} else {
							long third = 100 - first - second;
							long totalCapacity = Math.max(0L, ingredients.get(names.get(0)).capacity * first + ingredients.get(names.get(1)).capacity * second + ingredients.get(names.get(2)).capacity * third);
							long totalDurability = Math.max(0L, ingredients.get(names.get(0)).durability * first + ingredients.get(names.get(1)).durability * second + ingredients.get(names.get(2)).durability * third);
							long totalFlavor = Math.max(0L, ingredients.get(names.get(0)).flavor * first + ingredients.get(names.get(1)).flavor * second + ingredients.get(names.get(2)).flavor * third);
							long totalTexture = Math.max(0L, ingredients.get(names.get(0)).texture * first + ingredients.get(names.get(1)).texture * second + ingredients.get(names.get(2)).texture * third);
							long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
							accum = Math.max(accum, score);
						}
					}
				} else {
					long second = 100 - first;
					long totalCapacity = Math.max(0L, ingredients.get(names.get(0)).capacity * first + ingredients.get(names.get(1)).capacity * second);
					long totalDurability = Math.max(0L, ingredients.get(names.get(0)).durability * first + ingredients.get(names.get(1)).durability * second);
					long totalFlavor = Math.max(0L, ingredients.get(names.get(0)).flavor * first + ingredients.get(names.get(1)).flavor * second);
					long totalTexture = Math.max(0L, ingredients.get(names.get(0)).texture * first + ingredients.get(names.get(1)).texture * second);
					long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
					accum = Math.max(accum, score);
				}
			}
		} else {
			long totalCapacity = ingredients.get(names.get(0)).capacity * 100L;
			long totalDurability = ingredients.get(names.get(0)).durability * 100L;
			long totalFlavor = ingredients.get(names.get(0)).flavor * 100L;
			long totalTexture = ingredients.get(names.get(0)).texture * 100L;
			long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
			accum = Math.max(accum, score);
		}
		return accum;
	}

	public long doPart2() {
		int depth = names.size();
		long accum = 0;
		if (depth > 1) {
			for (long first = 0; first < 101; first++) {
				if (depth > 2) {
					for (long second = 0; second < 101 - first; second++) {
						if (depth > 3) {
							for (long third = 0; third < 101 - first - second; third++) {
								long fourth = 100 - first - second - third;
								if (ingredients.get(names.get(0)).calories * first + ingredients.get(names.get(1)).calories * second + ingredients.get(names.get(2)).calories * third + ingredients.get(names.get(3)).calories * fourth == 500) {
									long totalCapacity = Math.max(0L, ingredients.get(names.get(0)).capacity * first + ingredients.get(names.get(1)).capacity * second + ingredients.get(names.get(2)).capacity * third + ingredients.get(names.get(3)).capacity * fourth);
									long totalDurability = Math.max(0L, ingredients.get(names.get(0)).durability * first + ingredients.get(names.get(1)).durability * second + ingredients.get(names.get(2)).durability * third + ingredients.get(names.get(3)).durability * fourth);
									long totalFlavor = Math.max(0L, ingredients.get(names.get(0)).flavor * first + ingredients.get(names.get(1)).flavor * second + ingredients.get(names.get(2)).flavor * third + ingredients.get(names.get(3)).flavor * fourth);
									long totalTexture = Math.max(0L, ingredients.get(names.get(0)).texture * first + ingredients.get(names.get(1)).texture * second + ingredients.get(names.get(2)).texture * third + ingredients.get(names.get(3)).texture * fourth);
									long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
									accum = Math.max(accum, score);
								}
							}
						} else {
							long third = 100 - first - second;
							if (ingredients.get(names.get(0)).calories * first + ingredients.get(names.get(1)).calories * second + ingredients.get(names.get(2)).calories * third == 500) {
								long totalCapacity = Math.max(0L, ingredients.get(names.get(0)).capacity * first + ingredients.get(names.get(1)).capacity * second + ingredients.get(names.get(2)).capacity * third);
								long totalDurability = Math.max(0L, ingredients.get(names.get(0)).durability * first + ingredients.get(names.get(1)).durability * second + ingredients.get(names.get(2)).durability * third);
								long totalFlavor = Math.max(0L, ingredients.get(names.get(0)).flavor * first + ingredients.get(names.get(1)).flavor * second + ingredients.get(names.get(2)).flavor * third);
								long totalTexture = Math.max(0L, ingredients.get(names.get(0)).texture * first + ingredients.get(names.get(1)).texture * second + ingredients.get(names.get(2)).texture * third);
								long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
								accum = Math.max(accum, score);
							}
						}
					}
				} else {
					long second = 100 - first;
					if (ingredients.get(names.get(0)).calories * first + ingredients.get(names.get(1)).calories * second == 500) {
						long totalCapacity = Math.max(0L, ingredients.get(names.get(0)).capacity * first + ingredients.get(names.get(1)).capacity * second);
						long totalDurability = Math.max(0L, ingredients.get(names.get(0)).durability * first + ingredients.get(names.get(1)).durability * second);
						long totalFlavor = Math.max(0L, ingredients.get(names.get(0)).flavor * first + ingredients.get(names.get(1)).flavor * second);
						long totalTexture = Math.max(0L, ingredients.get(names.get(0)).texture * first + ingredients.get(names.get(1)).texture * second);
						long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
						accum = Math.max(accum, score);
					}
				}
			}
		} else {
			if (ingredients.get(names.get(0)).calories == 5) {
				long totalCapacity = ingredients.get(names.get(0)).capacity * 100L;
				long totalDurability = ingredients.get(names.get(0)).durability * 100L;
				long totalFlavor = ingredients.get(names.get(0)).flavor * 100L;
				long totalTexture = ingredients.get(names.get(0)).texture * 100L;
				long score = totalCapacity * totalDurability * totalFlavor * totalTexture;
				accum = Math.max(accum, score);
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2015, 15));

		// process input (7ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 13882464 (70ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 11171160 (24ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
