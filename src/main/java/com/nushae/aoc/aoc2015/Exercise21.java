package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 21, title = "RPG Simulator 20XX", keywords = {"min-maxing", "game"})
public class Exercise21 {

	public static final Map<Integer, Point> WEAPONS = Map.of(
			8, new Point(4, 0), // dagger
			10, new Point(5, 0), // shortsword
			25, new Point(6, 0), // warhammer
			40, new Point(7, 0), // longsword
			74, new Point(8, 0) // greataxe
	);

	public static final Map<Integer, Point> ARMOR = Map.of(
			0, new Point(0, 0), // no armor
			13, new Point(0, 1), // leather
			31, new Point(0, 2), // chainmail
			53, new Point(0, 3), // splintmail
			75, new Point(0, 4), // bandedmail
			102, new Point(0, 5) // platemail
	);

	public static final Map<Integer, Point> RINGS = Map.of(
			-1, new Point(0, 0), // no ring a
			0, new Point(0, 0), // no ring b
			25, new Point(1, 0), // damage +1
			50, new Point(2, 0), // damage +2
			100, new Point(3, 0), // damage +3
			20, new Point(0, 1), // defense +1
			40, new Point(0, 2), // defense +2
			80, new Point(0, 3) // defense +3
	);

	ArrayList<String> lines;
	int bossHitPoints;
	int bossDamage;
	int bossArmor;

	public Exercise21() {
		lines = new ArrayList<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		bossHitPoints = Integer.parseInt(lines.get(0).substring("Hit Points:".length()).trim());
		bossDamage = Integer.parseInt(lines.get(1).substring("Damage:".length()).trim());
		bossArmor = Integer.parseInt(lines.get(2).substring("Armor:".length()).trim());
	}

	public Point doBothParts() {
		// quad loop go BRRRRR
		int result1 = Integer.MAX_VALUE;
		int result2 = Integer.MIN_VALUE;
		for (Map.Entry<Integer, Point> wpntry : WEAPONS.entrySet()) {
			for (Map.Entry<Integer, Point> armtry : ARMOR.entrySet()) {
				for (Map.Entry<Integer, Point> ringtry1 : RINGS.entrySet()) {
					for (Map.Entry<Integer, Point> ringtry2 : RINGS.entrySet()) {
						if (ringtry2.getKey() < ringtry1.getKey()) {
							int totalCost = wpntry.getKey() + armtry.getKey() + Math.max(0, ringtry1.getKey()) + Math.max(0, ringtry2.getKey());
							int totalDamage = wpntry.getValue().x + armtry.getValue().x + ringtry1.getValue().x + ringtry2.getValue().x;
							int totalArmor = wpntry.getValue().y + armtry.getValue().y + ringtry1.getValue().y + ringtry2.getValue().y;
							if (doIWin(100, totalDamage, totalArmor, bossHitPoints, bossDamage, bossArmor)) {
								result1 = Math.min(result1, totalCost);
							} else {
								result2 = Math.max(result2, totalCost);
							}
						}
					}
				}
			}
		}
		return new Point(result1, result2);
	}

	public boolean doIWin(int myHP, int myDamage, int myArmor, int theirHP, int theirDamage, int theirArmor) {
		while (myHP > 0 && theirHP > 0) {
			theirHP -= Math.max(1, myDamage - theirArmor);
			if (theirHP < 1) { // I won!
				return true;
			}
			myHP -= Math.max(1, theirDamage - myArmor);
		}
		return false;
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2015, 21));

		// process input (<1ms)
		long start = System.currentTimeMillis();
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 and 2 combined - 111 / 188 (2ms)
		start = System.currentTimeMillis();
		Point result = ex.doBothParts();
		log.info("Part 1:");
		log.info(result.x);
		log.info("Part 2:");
		log.info(result.y);
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
