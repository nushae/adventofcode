package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import static com.nushae.aoc.util.MathUtil.modPow;
import static com.nushae.aoc.util.MathUtil.triangle;

@Log4j2
@Aoc(year = 2015, day = 25, title = "Let It Snow", keywords = {"triangle-numbers", "modular-exponentiation"})
public class Exercise25 {

	public long doPart1(long y, long x) {
		// If we add the coordinates of any point on diagonal N, we get N+2 (since our coordinates start at 1)
		// The first index number on diagonal N (in column 1) is equal to The Nth triangle number: triangle(n) = n*(n+1)/2
		// This then needs to be increased by x-1 to arrive at the xth number on diagonal N, our sought index
		long index = triangle(x + y - 2) + x - 1;
		// then calculate 20151125 * Math.pow(252533, index) % 33554393
		// (see https://en.wikipedia.org/wiki/Modular_exponentiation for this part)
		return 20151125 * modPow(252533, index, 33554393) % 33554393;
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25();

		// part 1 - 2650453 (315ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(2978, 3083));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// No part 2
	}
}
