package com.nushae.aoc.aoc2015;

import com.nushae.aoc.aoc2015.domain.AuntieSue;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 16, title = "Aunt Sue", keywords = {"filtering", "intersection"})
public class Exercise16 {

	public static final Pattern[] ATTRIBUTES = new Pattern[] {
			Pattern.compile(".*children: (\\d+).*"),
			Pattern.compile(".*cats: (\\d+).*"),
			Pattern.compile(".*samoyeds: (\\d+).*"),
			Pattern.compile(".*pomeranians: (\\d+).*"),
			Pattern.compile(".*akitas: (\\d+).*"),
			Pattern.compile(".*vizslas: (\\d+).*"),
			Pattern.compile(".*goldfish: (\\d+).*"),
			Pattern.compile(".*trees: (\\d+).*"),
			Pattern.compile(".*cars: (\\d+).*"),
			Pattern.compile(".*perfumes: (\\d+).*")
	};

	ArrayList<String> lines;
	ArrayList<AuntieSue> aunties;

	public Exercise16() {
		lines = new ArrayList<>();
		aunties = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		int auntie = 1;
		for (String line : lines) {
			AuntieSue a = new AuntieSue();
			a.number = auntie++;
			for (int i = 0; i < 10; i++) {
				Matcher m = ATTRIBUTES[i].matcher(line);
				if (m.find()) {
					a.attributes[i] = Integer.parseInt(m.group(1));
				}
			}
			aunties.add(a);
		}
	}

	public long doPart1() {
		return aunties.stream()
				.filter(a -> a.attributes[0] == -1 || a.attributes[0] == 3)
				.filter(a -> a.attributes[1] == -1 || a.attributes[1] == 7)
				.filter(a -> a.attributes[2] == -1 || a.attributes[2] == 2)
				.filter(a -> a.attributes[3] == -1 || a.attributes[3] == 3)
				.filter(a -> a.attributes[4] == -1 || a.attributes[4] == 0)
				.filter(a -> a.attributes[5] == -1 || a.attributes[5] == 0)
				.filter(a -> a.attributes[6] == -1 || a.attributes[6] == 5)
				.filter(a -> a.attributes[7] == -1 || a.attributes[7] == 3)
				.filter(a -> a.attributes[8] == -1 || a.attributes[8] == 2)
				.filter(a -> a.attributes[9] == -1 || a.attributes[9] == 1)
				.findFirst().orElse(new AuntieSue()).number;
	}

	public long doPart2() {
		return aunties.stream()
				.filter(a -> a.attributes[0] == -1 || a.attributes[0] == 3)
				.filter(a -> a.attributes[1] == -1 || a.attributes[1] > 7)
				.filter(a -> a.attributes[2] == -1 || a.attributes[2] == 2)
				.filter(a -> a.attributes[3] == -1 || a.attributes[3] < 3)
				.filter(a -> a.attributes[4] == -1 || a.attributes[4] == 0)
				.filter(a -> a.attributes[5] == -1 || a.attributes[5] == 0)
				.filter(a -> a.attributes[6] == -1 || a.attributes[6] < 5)
				.filter(a -> a.attributes[7] == -1 || a.attributes[7] > 3)
				.filter(a -> a.attributes[8] == -1 || a.attributes[8] == 2)
				.filter(a -> a.attributes[9] == -1 || a.attributes[9] == 1)
				.findFirst().orElse(new AuntieSue()).number;
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2015, 16));

		// process input (32ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 103 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 405 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
