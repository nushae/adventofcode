package com.nushae.aoc.aoc2015;

import com.nushae.aoc.aoc2015.domain.wires.*;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 7, title = "Some Assembly Required", keywords = {"logic-gates", "emulation"})
public class Exercise07 {

	public static final Pattern PATTERN_WIRE_NOT = Pattern.compile("NOT ([a-z]+|\\d+) -> ([a-z]+)");
	public static final Pattern PATTERN_WIRE_AND = Pattern.compile("([a-z]+|\\d+) AND ([a-z]+|\\d+) -> ([a-z]+)");
	public static final Pattern PATTERN_WIRE_OR = Pattern.compile("([a-z]+|\\d+) OR ([a-z]+|\\d+) -> ([a-z]+)");
	public static final Pattern PATTERN_WIRE_LSHIFT = Pattern.compile("([a-z]+|\\d+) LSHIFT (\\d+) -> ([a-z]+)");
	public static final Pattern PATTERN_WIRE_RSHIFT = Pattern.compile("([a-z]+|\\d+) RSHIFT (\\d+) -> ([a-z]+)");
	public static final Pattern PATTERN_WIRE_SOURCE = Pattern.compile("(\\d+) -> ([a-z]+)");
	public static final Pattern PATTERN_WIRE_CONNECT = Pattern.compile("([a-z]+) -> ([a-z]+)");

	ArrayList<String> lines;
	Map<String, WireGate> wireDict;

	public Exercise07() {
		lines = new ArrayList<>();
		wireDict = new HashMap<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		for (String line : lines) {
			WireGate gate = null;
			String output = "--";
			if (line.contains("NOT")) {
				Matcher m = PATTERN_WIRE_NOT.matcher(line);
				if (m.find()) {
					gate = new NotGate(line, m.group(1));
					output = m.group(2);
				} else {
					log.warn("Does not match NOT: '" + line + "'");
				}
			} else if (line.contains("AND")) {
				Matcher m = PATTERN_WIRE_AND.matcher(line);
				if (m.find()) {
					gate = new AndGate(line, m.group(1), m.group(2));
					output = m.group(3);
				} else {
					log.warn("Does not match AND: '" + line + "'");
				}
			} else if (line.contains("OR")) {
				Matcher m = PATTERN_WIRE_OR.matcher(line);
				if (m.find()) {
					gate = new OrGate(line, m.group(1), m.group(2));
					output = m.group(3);
				} else {
					log.warn("Does not match OR: '" + line + "'");
				}
			} else if (line.contains("LSHIFT")) {
				Matcher m = PATTERN_WIRE_LSHIFT.matcher(line);
				if (m.find()) {
					gate = new LshiftGate(line, m.group(1), Integer.parseInt(m.group(2)));
					output = m.group(3);
				} else {
					log.warn("Does not match LSHIFT: '" + line + "'");
				}
			} else if (line.contains("RSHIFT")) {
				Matcher m = PATTERN_WIRE_RSHIFT.matcher(line);
				if (m.find()) {
					gate = new RshiftGate(line, m.group(1), Integer.parseInt(m.group(2)));
					output = m.group(3);
				} else {
					log.warn("Does not match RSHIFT: '" + line + "'");
				}
			} else {
				Matcher m = PATTERN_WIRE_SOURCE.matcher(line);
				if (m.find()) {
					gate = new SourceGate(line, Integer.parseInt(m.group(1)));
					output = m.group(2);
				} else {
					m = PATTERN_WIRE_CONNECT.matcher(line);
					if (m.find()) {
						gate = new ConnectGate(line, m.group(1));
						output = m.group(2);
					} else {
						log.warn("Does not match SOURCE: '" + line + "'");
					}
				}
			}
			wireDict.put(output, gate);
		}
		log.debug("Read " + wireDict.size() + " lines.");
		for (WireGate w : wireDict.values()) {
			w.resolveConnections(wireDict);
		}
		log.debug("Everything connected.");
	}

	public int doPart1() {
		wireDict.get("a").eval();
		return wireDict.get("a").getOutput();
	}

	public int doPart2(int overrideVal) {
		for (String key : wireDict.keySet()) {
			if (!"b".equals(key)) {
				wireDict.get(key).reset();
			}
		}
		wireDict.get("b").overrideOutput(overrideVal);
		wireDict.get("a").eval();
		return wireDict.get("a").getOutput();
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2015, 7));

		// process input (19ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 16076 (13ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		int valA = ex.doPart1();
		log.info(valA);
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2797 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(valA));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
