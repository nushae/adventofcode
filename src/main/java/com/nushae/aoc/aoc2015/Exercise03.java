package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static com.nushae.aoc.util.IOUtil.aocPath;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;
import static com.nushae.aoc.util.MathUtil.CHAR_TO_DIR;
import static com.nushae.aoc.util.MathUtil.sum;

@Log4j2
@Aoc(year = 2015, day = 3, title = "Perfectly Spherical Houses in a Vacuum", keywords = {"list-processing", "pathing"})
public class Exercise03 {

	ArrayList<String> lines;
	String input;
	HashMap<Point, Integer> housesVisited;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
		input = lines.get(0);
	}

	public Exercise03(String data) {
		this();
		input = data;
	}

	public int doPart1() {
		housesVisited = new HashMap<>();
		var current = new Point(0, 0);
		housesVisited.put(current, 1);
		for (var dir : input.split("")) {
			current = sum(current, CHAR_TO_DIR.get(dir.charAt(0)));
			housesVisited.put(current, housesVisited.getOrDefault(current, 0) + 1);
		}
		return housesVisited.size();
	}

	public int doPart2() {
		housesVisited = new HashMap<>();
		housesVisited.put(new Point(0, 0), 1);
		var loc = new Point[2];
		loc[0] = new Point(0, 0);
		loc[1] = new Point(0, 0);
		for (int chr = 0; chr < input.length(); chr++) {
			char dir = input.charAt(chr);
			loc[chr % 2] = sum(loc[chr % 2], CHAR_TO_DIR.get(dir));
			housesVisited.put(loc[chr % 2], housesVisited.getOrDefault(loc[chr % 2], 0) + 1);
		}
		return housesVisited.size();
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2015, 3));

		// process input not needed

		// part 1 - 2565 (413ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2639 (2ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
