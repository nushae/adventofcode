package com.nushae.aoc.aoc2015.domain.wires;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Map;

@Log4j2
public class AndGate implements WireGate {

	private String firstSourceName;
	private String secondSourceName;
	private String line;
	private int amountConnected;
	private boolean evaluated = false;

	@Getter
	private int output;

	private WireGate firstSource;

	private WireGate secondSource;

	public AndGate(String line, WireGate firstSource, WireGate secondSource) {
		this.line = line;
		this.firstSource = firstSource;
		this.secondSource = secondSource;
		amountConnected = 2;
	}

	public AndGate(String line, String firstSource, WireGate secondSource) {
		this.line = line;
		this.secondSource = secondSource;
		amountConnected = 1;
		if (firstSource.matches("\\d+")) {
			this.firstSource = new SourceGate(line, Integer.parseInt(firstSource));
			amountConnected++;
		} else {
			this.firstSourceName = firstSource;
			this.firstSource = null;
		}
	}

	public AndGate(String line, WireGate firstSource, String secondSource) {
		this.line = line;
		this.firstSource = firstSource;
		amountConnected = 1;
		if (secondSource.matches("\\d+")) {
			this.secondSource = new SourceGate(line, Integer.parseInt(secondSource));
			amountConnected++;
		} else {
			this.secondSourceName = secondSource;
			this.secondSource = null;
		}
	}

	public AndGate(String line, String firstSource, String secondSource) {
		this.line = line;
		amountConnected = 0;
		if (firstSource.matches("\\d+")) {
			this.firstSource = new SourceGate(line, Integer.parseInt(firstSource));
			amountConnected++;
		} else {
			this.firstSourceName = firstSource;
			this.firstSource = null;
		}
		if (secondSource.matches("\\d+")) {
			this.secondSource = new SourceGate(line, Integer.parseInt(secondSource));
			amountConnected++;
		} else {
			this.secondSourceName = secondSource;
			this.secondSource = null;
		}
	}

	@Override
	public boolean isFullyConnected() {
		return amountConnected == 2;
	}

	@Override
	public boolean resolveConnections(Map<String, WireGate> components) {
		if (amountConnected < 2) {
			amountConnected = 0;
			if (components.containsKey(firstSourceName)) {
				firstSource = components.get(firstSourceName);
			}
			if (firstSource != null) {
				amountConnected++;
			}
			if (components.containsKey(secondSourceName)) {
				secondSource = components.get(secondSourceName);
			}
			if (secondSource != null) {
				amountConnected++;
			}
		}
		return amountConnected == 2;
	}

	public void eval() {
		if (amountConnected == 2 && !evaluated) {
			firstSource.eval();
			secondSource.eval();
			output = firstSource.getOutput() & secondSource.getOutput();
			log.debug("[" + line + "] " + firstSource.getOutput() + " & " + secondSource.getOutput() + " -> " + output);
			evaluated = true;
		}
	}

	@Override
	public void reset() {
		evaluated = false;
	}

	@Override
	public void overrideOutput(int overrideVal) {
		output = overrideVal;
		amountConnected = 2;
		evaluated = true;
	}
}
