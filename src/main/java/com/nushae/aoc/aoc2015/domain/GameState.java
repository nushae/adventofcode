package com.nushae.aoc.aoc2015.domain;

import com.nushae.aoc.domain.SearchNode;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Log4j2
public class GameState implements SearchNode {
	private SearchNode parent;
	private final boolean playerTurn;
	@Getter
	private final int spentSoFar;
	private final int myHp;
	private final int myArmor;
	private final int myMana;
	@Getter
	private final int theirHp;
	private final int theirDamage;
	private final Set<Spell> availableSpells;
	private final Map<Spell, Integer> activeSpells;
	private final boolean hardGame; // for part 2

	public GameState(boolean playerTurn, int spentSoFar, int myHp, int myArmor, int myMana, int theirHp, int theirDamage, Set<Spell> available, Map<Spell, Integer> active, boolean hardGame) {
		this.playerTurn = playerTurn;
		this.spentSoFar = spentSoFar;
		this.myHp = myHp;
		this.myArmor = myArmor;
		this.myMana = myMana;
		this.theirHp = theirHp;
		this.theirDamage = theirDamage;
		this.availableSpells = available;
		this.activeSpells = active;
		this.hardGame = hardGame;
	}

	@Override
	public Map<SearchNode, Long> getAdjacentNodes() {
		log.debug("Get adjacent nodes for:\n  " + this.getState());
		Map<SearchNode, Long> result = new HashMap<>();
		int newMyHp = myHp;
		if (hardGame && playerTurn) {
			// lose 1HP, need to survive this
			newMyHp--;
			if (newMyHp < 1) { // we lose, no next moves
				log.debug("Lost at start of my turn (hard mode sucks)");
				return result;
			}
		}
		Set<Spell> newAvailable = new HashSet<>();
		for (Spell option : availableSpells) {
			newAvailable.add(new Spell(option));
		}
		Map<Spell, Integer> newActive = new HashMap<>();
		int newMyArmor = myArmor;
		int newTheirHp = theirHp;
		int newMyMana = myMana;
		for (Spell active : activeSpells.keySet()) {
			newMyHp += active.healing;
			newTheirHp -= active.damage;
			newMyMana += active.manaRestore;
			int remainingDuration = activeSpells.get(active) - 1;
			if (remainingDuration > 0) {
				newActive.put(new Spell(active), remainingDuration);
			} else {
				newMyArmor -= active.armorIncrease;
				newAvailable.add(new Spell(active));
			}
		}
		if (newTheirHp < 1) {
			// We won at start of turn, only this one next move to return
			log.debug("  Generated a winning move (start of turn)");
			result.put(new GameState(!playerTurn, spentSoFar, newMyHp, newMyArmor, newMyMana, newTheirHp, theirDamage, newAvailable, newActive, hardGame), 1L);
			return result;
		}
		// Boss not dead, so take actual turn
		if (playerTurn) {
			// our turn - loop over all available spells that we can afford and generate a next GameState by 'casting' it
			for (Spell choice : newAvailable) {
				if (choice.cost <= newMyMana) {
					if (choice.isInstantaneous()) { // just apply its effect, spell stays available
						result.put(new GameState(false, spentSoFar + choice.cost, newMyHp + choice.healing, newMyArmor, newMyMana - choice.cost + choice.manaRestore, newTheirHp - choice.damage, theirDamage, newAvailable, newActive, hardGame), 1L);
					} else { // spell with duration, set to active
						Set<Spell> recurseOptions = new HashSet<>();
						for (Spell recOpt : newAvailable) {
							if (!recOpt.name.equals(choice.name)) {
								recurseOptions.add(recOpt);
							}
						}
						Map<Spell, Integer> recurseActive = new HashMap<>(newActive);
						recurseActive.put(choice, choice.duration);
						result.put(new GameState(false, spentSoFar + choice.cost, newMyHp, newMyArmor + choice.armorIncrease, newMyMana - choice.cost, newTheirHp, theirDamage, recurseOptions, recurseActive, hardGame), 1L);
					}
				}
			}
			if (!result.isEmpty()) {
				log.debug("  Generated " + result.size() + " move(s).");
			}
		} else {
			// boss' turn - just apply their damage and everything else stays the same
			log.debug("performing Boss turn; their dmg: " + theirDamage + ", my HP: " + newMyHp + ", my armor: " + newMyArmor + ".");
			int effectiveDmg = Math.max(1, theirDamage - newMyArmor);
			newMyHp -= effectiveDmg;
			log.debug(" Receiving " + effectiveDmg + " dmg. My new HP: " + newMyHp);
			// 'prune': don't return a losing state
			if (newMyHp > 0) {
				result.put(new GameState(true, spentSoFar, newMyHp, newMyArmor, newMyMana, newTheirHp, theirDamage, newAvailable, newActive, hardGame), 1L);
				log.debug("  Generated 1 Boss move, which I survived.");
			}
		}
		return result;
	}

	@Override
	public String getState() {
		StringBuilder sb = new StringBuilder();
		sb.append(playerTurn ? "My" : "Their");
		sb.append(" turn - spent ").append(spentSoFar);
		sb.append(" mana. My mana: ").append(myMana);
		sb.append(", my HP: ").append(myHp);
		sb.append(", my armor: ").append(myArmor);
		sb.append(". Boss HP: ").append(theirHp);
		sb.append(". Boss deals ").append(theirDamage);
		sb.append(" every turn. Available spells:");
		for (Spell a : availableSpells) {
			sb.append(" ").append(a.name);
		}
		sb.append(" / active spells:");
		for (Spell a : activeSpells.keySet()) {
			sb.append(" ").append(a.name).append("(").append(activeSpells.get(a)).append(")");
		}
		return sb.toString();
	}

	@Override
	public void setParent(SearchNode parent) {
		this.parent = parent;
	}

	@Override
	public SearchNode getParent() {
		return parent;
	}

	@Override
	public int hashCode() {
		return getState().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		GameState other = (GameState) obj;
		return getState().equals(other.getState());

	}
}
