package com.nushae.aoc.aoc2015.domain.wires;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Map;

@Log4j2
public class NotGate implements WireGate {

	@Getter
	private int output;

	private WireGate source;
	private String sourceName;
	private String line;
	private boolean fullyConnected;
	private boolean evaluated = false;

	public NotGate(String line, WireGate source) {
		this.line = line;
		this.source = source;
		fullyConnected = true;
	}

	public NotGate(String line, String source) {
		this.line = line;
		if (source.matches("\\d+")) {
			this.source = new SourceGate(line, Integer.parseInt(source));
			this.fullyConnected = true;
		} else {
			this.sourceName = source;
			this.fullyConnected = false;
		}
	}

	@Override
	public boolean isFullyConnected() {
		return fullyConnected;
	}

	@Override
	public boolean resolveConnections(Map<String, WireGate> components) {
		if (!fullyConnected) {
			if (components.containsKey(sourceName)) {
				source = components.get(sourceName);
				fullyConnected = true;
			}
		}
		return fullyConnected;
	}

	public void eval() {
		if (fullyConnected && !evaluated) {
			source.eval();
			output = ~source.getOutput();
			if (output < 0) {
				output += WORDSIZE;
			}
			log.debug("[" + line + "] ~" + source.getOutput() + " -> " + output);
			evaluated = true;
		}
	}

	@Override
	public void reset() {
		evaluated = false;
	}

	@Override
	public void overrideOutput(int overrideVal) {
		output = overrideVal;
		fullyConnected = true;
		evaluated = true;
	}
}
