package com.nushae.aoc.aoc2015.domain;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Spell {
	public final String name;
	public final int cost;
	public final int duration;
	public int active = 0;
	public final int damage;
	public final int healing;
	public final int armorIncrease;
	public final int manaRestore;

	public Spell(String name, int cost, int duration, int damage, int healing, int armorIncrease, int manaRestore) {
		this.name = name;
		this.cost = cost;
		this.duration = duration;
		this.damage = damage;
		this.healing = healing;
		this.armorIncrease = armorIncrease;
		this.manaRestore = manaRestore;
	}

	public Spell(Spell copyFrom) {
		this.name = copyFrom.name;
		this.cost = copyFrom.cost;
		this.duration = copyFrom.duration;
		this.damage = copyFrom.damage;
		this.healing = copyFrom.healing;
		this.armorIncrease = copyFrom.armorIncrease;
		this.manaRestore = copyFrom.manaRestore;
		this.active = copyFrom.active;
	}

	public boolean isInstantaneous() { // if not, it creates an effect
		return duration == 0;
	}

	public boolean isActive() {
		return active > 0;
	}

	public void apply(String indent) {
		if (isActive()) {
			active--;
			StringBuilder actions = new StringBuilder();
			if (damage > 0) {
				actions.append("dealt ").append(damage).append(" damage to the boss");
			}
			if (healing > 0) {
				if (!actions.isEmpty()) {
					actions.append(" and ");
				}
				actions.append("healed the player for ").append(healing).append("HP");
			}
			if (manaRestore > 0) {
				if (!actions.isEmpty()) {
					actions.append(" and ");
				}
				actions.append("restored ").append(manaRestore).append(" mana for the player");
			}
			if (armorIncrease > 0) {
				if (!actions.isEmpty()) {
					actions.append(" and ");
				}
				actions.append("will provide ").append(armorIncrease).append(" additional armor for the player this turn");
			}
			log.debug(String.format("%s%s %s, its timer is now %d%n", indent, name, actions, active));
		}
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Spell other = (Spell) obj;
		return (name.equals(other.name));
	}
}
