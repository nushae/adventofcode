package com.nushae.aoc.aoc2015.domain.wires;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Map;

@Log4j2
public class SourceGate implements WireGate {

	@Getter
	private int output;
	private String line;
	private boolean evaluated = false;

	public SourceGate(String line, int outputVal) {
		this.line = line;
		this.output = outputVal;
	}

	@Override
	public boolean isFullyConnected() {
		return true;
	}

	@Override
	public boolean resolveConnections(Map<String, WireGate> components) {
		return true;
	}

	public void eval() {
		if (!evaluated) {
			log.debug("[" + line + "] () -> " + output);
			evaluated = true;
		}
	}

	@Override
	public void reset() {
		evaluated = false;
	}

	@Override
	public void overrideOutput(int overrideVal) {
		output = overrideVal;
		evaluated = true;
	}
}
