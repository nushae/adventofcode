package com.nushae.aoc.aoc2015.domain.wires;

import java.util.Map;

public interface WireGate {
	int WORDSIZE = 65536;

	int getOutput();
	boolean isFullyConnected();
	boolean resolveConnections(Map<String, WireGate> components);
	void eval();
	void reset();
	void overrideOutput(int overrideVal);
}
