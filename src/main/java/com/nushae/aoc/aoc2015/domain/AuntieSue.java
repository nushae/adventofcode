package com.nushae.aoc.aoc2015.domain;

public class AuntieSue {
	public int number;
	public int[] attributes = new int[10];

	public AuntieSue() {
		number = -1;
		for (int i = 0; i < 10; i++) {
			attributes[i] = -1;
		}
	}
}
