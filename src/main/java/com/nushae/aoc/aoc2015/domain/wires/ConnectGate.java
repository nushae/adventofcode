package com.nushae.aoc.aoc2015.domain.wires;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Map;

@Log4j2
public class ConnectGate implements WireGate {

	@Getter
	private int output;

	private String line;
	private WireGate source;
	private String sourceName;
	private boolean fullyConnected;
	private boolean evaluated = false;

	public ConnectGate(String line, WireGate source) {
		this.line = line;
		this.source = source;
		fullyConnected = true;
	}

	public ConnectGate(String line, String source) {
		this.line = line;
		this.sourceName = source;
		this.fullyConnected = false;
	}

	@Override
	public boolean isFullyConnected() {
		return fullyConnected;
	}

	@Override
	public boolean resolveConnections(Map<String, WireGate> components) {
		if (!fullyConnected) {
			if (components.containsKey(sourceName)) {
				source = components.get(sourceName);
				fullyConnected = true;
			}
		}
		return fullyConnected;
	}

	public void eval() {
		if (fullyConnected && !evaluated) {
			source.eval();
			output = source.getOutput();
			log.debug("["+sourceName+"] " + source.getOutput() + " -> " + output);
			evaluated = true;
		}
	}

	@Override
	public void reset() {
		evaluated = false;
	}

	@Override
	public void overrideOutput(int overrideVal) {
		output = overrideVal;
		fullyConnected = true;
		evaluated = true;
	}
}
