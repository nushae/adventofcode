package com.nushae.aoc.aoc2015.domain.wires;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Map;

@Log4j2
public class LshiftGate implements WireGate {

	private int shiftBy;
	private String sourceName;
	private String line;
	private boolean fullyConnected;
	private boolean evaluated = false;

	@Getter
	private int output;

	private WireGate source;

	public LshiftGate(WireGate source, int shiftBy) {
		this.source = source;
		this.shiftBy = shiftBy;
	}

	public LshiftGate(String line, String source, int shiftBy) {
		this.line = line;
		if (source.matches("\\d+")) {
			this.source = new SourceGate(line, Integer.parseInt(source));
			this.fullyConnected = true;
		} else {
			this.sourceName = source;
			this.fullyConnected = false;
		}
		this.shiftBy = shiftBy;
	}

	@Override
	public boolean isFullyConnected() {
		return fullyConnected;
	}

	@Override
	public boolean resolveConnections(Map<String, WireGate> components) {
		if (!fullyConnected) {
			if (components.containsKey(sourceName)) {
				source = components.get(sourceName);
				fullyConnected = true;
			}
		}
		return fullyConnected;
	}

	public void eval() {
		if (fullyConnected && !evaluated) {
			source.eval();
			output = (source.getOutput() << shiftBy) % WORDSIZE;
			log.debug("[" + line + "] " + source.getOutput() + " << " + shiftBy + " -> " + output);
			evaluated = true;
		}
	}

	@Override
	public void reset() {
		evaluated = false;
	}

	@Override
	public void overrideOutput(int overrideVal) {
		output = overrideVal;
		fullyConnected = true;
		evaluated = true;
	}
}
