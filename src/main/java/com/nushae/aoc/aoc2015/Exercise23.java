package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 23, title = "Opening the Turing Lock", keywords = {"opcodes", "interpreter"})
public class Exercise23 {

	List<String> lines;

	public Exercise23() {
		lines = new ArrayList<>();
	}

	public Exercise23(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise23(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doProgram(long aStart, long bStart) {
		Map<String, Long> registers = new HashMap<>();
		registers.put("a", aStart);
		registers.put("b", bStart);
		int pointer = 0;
		while (pointer < lines.size() && pointer >= 0) {
			String line = lines.get(pointer);
			log.debug(String.format("%d -> %s [a: %d] [b: %d]  => ", pointer, line, registers.get("a"), registers.get("b")));
			String[] parts = line.split(",?\\s+");
			switch (parts[0]) {
				case "hlf" -> {
					registers.put(parts[1], registers.get(parts[1]) / 2);
					pointer++;
				}
				case "tpl" -> {
					registers.put(parts[1], registers.get(parts[1]) * 3);
					pointer++;
				}
				case "inc" -> {
					registers.put(parts[1], registers.get(parts[1]) + 1);
					pointer++;
				}
				case "jie" -> {
					if (registers.get(parts[1]) % 2 == 0) {
						pointer += Integer.parseInt(parts[2]);
					} else {
						pointer++;
					}
				}
				case "jio" -> {
					if (registers.get(parts[1]) == 1) { // ONE NOT ODD
						pointer += Integer.parseInt(parts[2]);
					} else {
						pointer++;
					}
				}
				case "jmp" -> {
					pointer += Integer.parseInt(parts[1]);
				}
				default -> {
					log.error("Unknown instruction: " + parts[0]);
					System.exit(0);
				}
			}
			log.debug(String.format("%d [a: %d] [b: %d]%n", pointer, registers.get("a"), registers.get("b")));
		}
		return registers.get("b");
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(aocPath(2015, 23));

		// process input not needed

		// part 1 - 184 (18ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doProgram(0, 0));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 231 (3ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doProgram(1, 0));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
