package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 14, title = "Reindeer Olympics", keywords = {"linear-extrapolation", "modulo"})
public class Exercise14 {

	public static final Pattern REINDEER = Pattern.compile("(\\S+) can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds.");

	public record SpeedParameters(int speed, int duration, int rest) {}	ArrayList<String> lines;

	Map<String, SpeedParameters> reinDict;

	public Exercise14() {
		lines = new ArrayList<>();
		reinDict = new HashMap<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		for (String line : lines) {
			Matcher m = REINDEER.matcher(line);
			if (m.find()) {
				reinDict.put(m.group(1), new SpeedParameters(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4))));
			} else {
				log.warn("This reindeer confuses me: " + line);
			}
		}
	}

	public long doPart1(long seconds) {
		long accum = 0;
		for (Map.Entry<String, SpeedParameters> reindeer : reinDict.entrySet()) {
			SpeedParameters par = reindeer.getValue();
			long cycle = par.duration + par.rest;
			long numCycles = seconds / cycle; // every cycle travels speed * duration km
			long remainder = seconds % cycle; // plus min(remainder, duration) * speed km
			long totalDistance = numCycles * par.speed * par.duration + Math.min(remainder, par.duration) * par.speed;
			log.debug(reindeer.getKey() + " travels " + totalDistance + " km.");
			accum = Math.max(accum, totalDistance);
		}
		return accum;
	}

	public long doPart2(long seconds) {
		// Whoever is ahead after each second, gets a point (when tied, each gets 1) -> total score per reindeer
		Map<String, Long> scores = new HashMap<>();
		for (int sec = 0; sec < seconds; sec++) {
			Map<String, Long> distances = new HashMap<>();
			long accum = 0;
			for (Map.Entry<String, SpeedParameters> reindeer : reinDict.entrySet()) {
				SpeedParameters par = reindeer.getValue();
				long cycle = par.duration + par.rest;
				long numCycles = (sec+1) / cycle; // every cycle travels speed * duration km
				long remainder = (sec+1) % cycle; // plus min(remainder, duration) * speed km
				long totalDistance = numCycles * par.speed * par.duration + Math.min(remainder, par.duration) * par.speed;
				distances.put(reindeer.getKey(), totalDistance);
				accum = Math.max(accum, totalDistance);
			}
			for (Map.Entry<String, Long> distance : distances.entrySet()) {
				if (distance.getValue() == accum) {
					scores.put(distance.getKey(), scores.getOrDefault(distance.getKey(), 0L) + 1);
				}
			}
		}
		long accum = 0;
		for (Map.Entry<String, Long> score : scores.entrySet()) {
			log.debug(score.getKey() + ": " + score.getValue());
			accum = Math.max(accum, score.getValue());
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2015, 14));

		// Process input (12ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 2655 (13ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(2503));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1059 (7ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(2503));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
