package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Aoc(year = 2015, day = 20, title = "Infinite Elves and Infinite Houses", keywords = {"factorization", "brute-force"})
public class Exercise20 {
	int input;

	public Exercise20(int input) {
		this.input = input;
	}

	// Surprisingly the dumb "emulate the process" brute force approach is FINE, for both parts...
	// No need for chinese remainder theorems or divisors or perfect numbers or... Kind of a let-down
	// BTW, I think there is a smarter solution that leverages the fact that each house gets deliveries only from its divisors, but I don't know how
	public long doPart1() {
		int[] houses = new int[input/10];
		for (int elf = 0; elf < input/10; elf++) {
			for (int house = elf; house < input/10; house += (elf+1)) {
				houses[house] += 10 * (elf+1);
			}
		}
		for (int house = 0; house < input/10; house++) {
			if (houses[house] > input) {
				return house+1;
			}
		}
		return -1;
	}

	public long doPart2() {
		int[] houses = new int[input/10];
		for (int elf = 0; elf < input/10; elf++) {
			for (int house = elf; house < input/10 && house < 50 * (elf + 1); house += elf + 1) {
				houses[house] += 11 * (elf+1);
			}
		}
		for (int house = 0; house < input/10; house++) {
			if (houses[house] > input) {
				return house+1;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(29000000);

		// process input not needed

		// part 1 - 665280 (242ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 705600 (31ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
