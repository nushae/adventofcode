package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.loadInputFromData;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
@Aoc(year = 2015, day = 10, title = "Elves Look, Elves Say", keywords = {"substitution", "iteration"})
public class Exercise10 {

	public static final Pattern PATTERN_REPEATED_DIGIT = Pattern.compile("((\\d)(\\2*))");

	ArrayList<String> lines;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		String sequence = lines.get(0);
		for (int i = 0; i< 40; i++) {
			sequence = lookAndSay(sequence);
		}
		return sequence.length();
	}

	public long doPart2() {
		String sequence = lines.get(0);
		for (int i = 0; i< 50; i++) {
			sequence = lookAndSay(sequence);
		}
		return sequence.length();
	}

	// Note: the sequence will never contain a digit other than 1, 2, or 3
	public static String lookAndSay(String sequence) {
		StringBuilder result = new StringBuilder();
		Matcher m = PATTERN_REPEATED_DIGIT.matcher(sequence);
		while (m.find()) {
			result.append(m.group(1).length()).append(m.group(1).charAt(0));
		}
		return result.toString();
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10("1113122113");

		// process input not needed

		// part 1 - 360154 (100ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		long result = ex.doPart1();
		log.info(result);
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 5103798 (817ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info("Part 2 estimate: " + result * Math.pow(1.303577269,10));
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
