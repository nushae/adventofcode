package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 6, title = "Probably a Fire Hazard", keywords = {"grid", "constructive-geometry"})
public class Exercise06 {

	public static final Pattern PATTERN_LIGHTSERIES = Pattern.compile(".* (\\d+),(\\d+) through (\\d+),(\\d+)");

	ArrayList<String> lines;
	long[][] lightGrid = new long[1000][1000];

	public Exercise06() {
		lines = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void resetGrid() {
		for (int i = 0 ; i < 1000 ; i++) {
			for (int j = 0; j < 1000; j++) {
				lightGrid[i][j] = 0;
			}
		}
	}

	public long doPart1() {
		return doLights(false);
	}

	public long doPart2() {
		return doLights(true);
	}

	public long doLights(boolean part2) {
		resetGrid();
		long result = 0;
		for (String line : lines) {
			Matcher m = PATTERN_LIGHTSERIES.matcher(line);
			if (m.find()) {
				int x1 = Integer.parseInt(m.group(1));
				int y1 = Integer.parseInt(m.group(2));
				int x2 = Integer.parseInt(m.group(3));
				int y2 = Integer.parseInt(m.group(4));
				for (int i = x1 ; i <= x2 ; i++) {
					for (int j = y1; j <= y2; j++) {
						if (line.startsWith("turn on")) {
							if (!part2) {
								lightGrid[i][j] = 1;
							} else {
								lightGrid[i][j]++;
							}
						} else if (line.startsWith("turn off")) {
							if (!part2) {
								lightGrid[i][j] = 0;
							} else {
								lightGrid[i][j]--;
								if (lightGrid[i][j] < 0) {
									lightGrid[i][j] = 0;
								}
							}
						} else if (line.startsWith("toggle")) {
							if (!part2) {
								lightGrid[i][j] = 1 - lightGrid[i][j];
							} else {
								lightGrid[i][j] += 2;
							}
						}
					}
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		for (int i = 0 ; i < 1000 ; i++) {
			for (int j = 0; j < 1000; j++) {
				result += lightGrid[i][j];
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2015, 6));

		// no processInput - easier to reparse everything for each part

		// part 1 - 569999 (120ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 17836115 (271ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
