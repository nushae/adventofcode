package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2015, day = 18, title = "Like a GIF For Your Yard", keywords = {"cellular-automaton", "life"})
public class Exercise18 {

	ArrayList<String> lines;
	int size;
	boolean[][] gameOfLife;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		size = lines.size();
		log.debug("Grid dimension = " + size + "x" + size + ".");
		gameOfLife = new boolean[size+2][size+2]; // extra edge of zeroes
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			for (int j = 0; j < line.length(); j++) {
				gameOfLife[i+1][j+1] = (line.charAt(j) == '#');
			}
		}
	}

	public long doPart1(int steps, boolean print) {
		long result = 0;
		output(gameOfLife, print);
		boolean[][] state = process(gameOfLife, false);
		output(state, print);
		for (int i=1; i < steps; i++) {
			state = process(state, false);
			result = output(state, print);
		}
		return result;
	}

	public long doPart2(int steps, boolean print) {
		long result = 0;
		gameOfLife[1][1] = true;
		gameOfLife[1][size] = true;
		gameOfLife[size][1] = true;
		gameOfLife[size][size] = true;
		output(gameOfLife, print);
		boolean[][] state = process(gameOfLife, true);
		output(state, print);
		for (int i=1; i < steps; i++) {
			state = process(state, true);
			result = output(state, print);
		}
		return result;
	}

	public boolean[][] process(boolean[][] input, boolean stucklights) {
		 boolean[][] result = new boolean[size+2][size+2];
		 for (int i = 0; i < size; i++) {
		 	for (int j = 0; j < size; j++) {
		 		int n = 0;
		 		for (int x = -1; x < 2; x++) {
		 			for (int y = -1; y < 2; y++) {
		 				if (x != 0 || y != 0) {
		 					n += input[x+i+1][y+j+1] ? 1 : 0;
						}
					}
				}
		 		result[i+1][j+1] = input[i+1][j+1] && n == 2 || n == 3;
			}
		 }
		 if (stucklights) {
			result[1][1] = true;
			result[1][size] = true;
			result[size][1] = true;
			result[size][size] = true;
		 }
		 return result;
	}

	public long output(boolean[][] input, boolean print) {
		long result = 0;
		if (print) {
			System.out.println();
		}
		StringBuffer printOut = new StringBuffer();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				boolean state = input[i+1][j+1];
				printOut.append(state ? "#" : ".");
				result += state ? 1 : 0;
			}
			printOut.append("\n");
		}
		if (print) {
			System.out.print(printOut);
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2015, 18));

		// process input (7ms)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1061 (43ms)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(100, false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1006 (21ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(100, false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
