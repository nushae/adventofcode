package com.nushae.aoc.aoc2015;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.aocPath;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
@Aoc(year = 2015, day = 1, title = "Not Quite Lisp", keywords = {"linear-scanning", "parentheses"})
public class Exercise01 {

	private ArrayList<String> lines;
	private String input;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
		input = lines.get(0);
	}

	public Exercise01(String data) {
		this();
		input = data;
	}

	public int doPart1() {
		int result = 0;
		for (int pos = 0; pos < input.length(); pos++) {
			result += input.charAt(pos) == '(' ? 1 : -1;
		}
		return result;
	}

	public int doPart2() {
		int result = 0;
		for (int pos = 0; pos < input.length(); pos++) {
			result += input.charAt(pos) == '(' ? 1 : -1;
			if (result == -1) {
				return pos + 1;
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2015, 1));

		// no input processing needed

		// part 1 - 74 (10ms)
		var start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1795 (1ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
