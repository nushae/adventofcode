package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.manhattanDistance;

@Log4j2
public class Exercise01 {

	private static final Pattern LR_ROUTE = Pattern.compile("([LR])(\\d+)");

	ArrayList<String> lines;
	ArrayList<Integer> numbers;
	Set<Point> visited;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		numbers = new ArrayList<>();
		for (String line : lines) {
			Matcher m = LR_ROUTE.matcher(line);
			while (m.find()) {
				if ("L".equals(m.group(1))) {
					numbers.add(-Integer.parseInt(m.group(2)));
				} else {
					numbers.add(Integer.parseInt(m.group(2)));
				}
			}
		}
	}

	public long doPart1() {
		processWithPattern();
		int x = 0;
		int y = 0;
		int dx = 0;
		int dy = 1;
		for (int num : numbers) {
			if (num < 0) { // rotate left, move
				int tmp = dx;
				dx = -dy;
				dy = tmp;
				x = x - num * dx;
				y = y - num * dy;
			} else { // rotate right, move
				int tmp = dx;
				dx = dy;
				dy = -tmp;
				x = x + num * dx;
				y = y + num * dy;
			}
		}
		return manhattanDistance(x, y);
	}

	public long doPart2() {
		visited = new HashSet<>();
		processWithPattern();
		int x = 0;
		int y = 0;
		int dx = 0;
		int dy = 1;
		for (int num : numbers) {
			if (num < 0) { // rotate left, move
				int tmp = dx;
				dx = -dy;
				dy = tmp;
				num = -num;
			} else { // rotate right, move
				int tmp = dx;
				dx = dy;
				dy = -tmp;
			}
			for (int i=0; i< num; i++) {
				x = x + dx;
				y = y + dy;
				Point p = new Point(x, y);
				if (visited.contains(p)) {
					return manhattanDistance(p.x, p.y);
				}
				visited.add(p);
			}
		}
		return manhattanDistance(x, y);
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2016, 1));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
