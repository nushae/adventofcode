package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise07 {

	ArrayList<String> lines;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long result = 0;
		for (String line : lines) {
			result += supportsTLS(line) ? 1 : 0;
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		for (String line : lines) {
			result += supportsSSL(line) ? 1 : 0;
		}
		return result;
	}

	public boolean supportsTLS(String ipv7) {
		boolean insideBrack = false;
		boolean hasABBA = false;
		for (int i = 0; i < ipv7.length()-3; i++) {
			char c = ipv7.charAt(i);
			if (c == '[') {
				insideBrack = true;
			} else if (c == ']') {
				insideBrack = false;
			} else if (insideBrack) {
				if (ipv7.charAt(i) != ipv7.charAt(i+1) && ipv7.charAt(i) == ipv7.charAt(i+3) && ipv7.charAt(i+1) == ipv7.charAt(i+2)) {
					return false;
				}
			} else {
				if (ipv7.charAt(i) != ipv7.charAt(i+1) && ipv7.charAt(i) == ipv7.charAt(i+3) && ipv7.charAt(i+1) == ipv7.charAt(i+2)) {
					hasABBA = true;
				}
			}
		}
		return hasABBA;
	}

	public boolean supportsSSL(String ipv7) {
		boolean insideBrack = false;
		Set<String> abas = new HashSet<>(); // outside brackets: yxy
		Set<String> babs = new HashSet<>(); // inside brackets: xyx
		for (int i = 0; i < ipv7.length()-2; i++) {
			char c = ipv7.charAt(i);
			if (c == '[') {
				insideBrack = true;
			} else if (c == ']') {
				insideBrack = false;
			} else if (insideBrack) {
				if (ipv7.charAt(i) != ipv7.charAt(i+1) && ipv7.charAt(i) == ipv7.charAt(i+2) && ipv7.charAt(i+1) != ']') {
					System.out.println("BAB: " + ipv7.charAt(i + 1) + ipv7.charAt(i) + ipv7.charAt(i + 1));
					babs.add(String.valueOf(ipv7.charAt(i + 1)) + ipv7.charAt(i) + ipv7.charAt(i + 1));
				}
			} else {
				if (ipv7.charAt(i) != ipv7.charAt(i+1) && ipv7.charAt(i) == ipv7.charAt(i+2) && ipv7.charAt(i+1) != '[') {
					System.out.println("ABA: " + ipv7.charAt(i) + ipv7.charAt(i + 1) + ipv7.charAt(i));
					abas.add(String.valueOf(ipv7.charAt(i)) + ipv7.charAt(i + 1) + ipv7.charAt(i));
				}
			}
		}
		abas.retainAll(babs);
		return abas.size()>0;
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2016, 7));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
