package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;

@Log4j2
public class Exercise16 {

	// brute force version
	public String doPart1(String input, int discSize) {
		String a = input;
		System.out.println(a.length() /*+ " " + a*/);
		while (a.length() < discSize) {
			String b = new BigInteger(StringUtils.repeat("1", a.length()), 2).subtract(new BigInteger(StringUtils.reverse(a), 2)).toString(2);
			if (b.length() < a.length()) {
				b = StringUtils.repeat("0", a.length() - b.length()) + b;
			}
			a = a + "0" + b;
			System.out.println(a.length() /*+ " " + a*/);
		}
		System.out.println(" -> ");
		a = a.substring(0, discSize);
		return checksum(a);
	}

	// the dragon curve with starting input a, length(a) = L, and b = inv(rev(a)), looks like:
	// curve                                                           length
	// a 0 b                                                           2L + 1
	// a 0 b 0 a 1 b                                                   4L + 3
	// a 0 b 0 a 1 b 0 a 1 b 1 a 0 b                                   8L + 7
	// a 0 b 0 a 1 b 0 a 1 b 1 a 0 b 0 a 1 b 1 a 0 b 1 a 0 b 0 a 1 b   16L + 15
	// etc
	// In other words, alternating a's and b's with the bits of the dragon curve from input '0' in between.
	// The length of the Nth generation is 2^N*L + 2^N - 1 = 2^N * (L+1) -1.
	// The first generation that will equal or exceed the discSize is therefore ceil(2log(discSize/L)).
	// So we can just generate that generation of the '0' curve and insert a's and b's accordingly.
	// (There is probably also a more efficient way to calculate the dragon curve from a '0' input by using
	// parity or something, but this is good enough I guess.)
	public String doPart2(String input, int discSize) {
		String a = input;
		String b = new BigInteger(StringUtils.repeat("1", a.length()), 2).subtract(new BigInteger(StringUtils.reverse(a), 2)).toString(2);
		if (b.length() < a.length()) {
			b = StringUtils.repeat("0", a.length() - b.length()) + b;
		}
		System.out.println(a + " / " + b);
		int generations = (int) Math.ceil(Math.log(1.0*discSize/a.length())/Math.log(2));
		StringBuilder dragon = new StringBuilder("0");
		while (generations > 0) {
			String second = new BigInteger(StringUtils.repeat("1", dragon.length()), 2).subtract(new BigInteger(StringUtils.reverse(dragon.toString()), 2)).toString(2);
			if (second.length() < dragon.length()) {
				second = StringUtils.repeat("0", dragon.length() - second.length()) + second;
			}
			dragon.append("0").append(second);
			generations--;
		}
		StringBuilder result = new StringBuilder();
		for (int i=0; i< dragon.length(); i++) {
			result.append(i % 2 == 0 ? a : b).append(dragon.charAt(i));
		}
		result.append(b);
		return checksumEfficient(result.substring(0, discSize));
	}

	public String checksum(String input) {
		while (input.length() % 2 == 0) {
			StringBuilder checksum = new StringBuilder();
			for (int i = 0; i < input.length() / 2; i++) {
				checksum.append(input.charAt(i*2) == input.charAt(i*2+1) ? "1" : "0");
			}
			input = checksum.toString();
		}
		return input;
	}

	public String checksumEfficient(String input) {
		long len = input.length();
		int groupsize = 1;
		while (len % 2 == 0) {
			len = len / 2;
			groupsize = groupsize * 2;
		}
		StringBuilder checksum = new StringBuilder();
		for (int i = 0; i < input.length() / groupsize; i++) {
			System.out.println(i);
			if (new BigInteger(input.substring(i*groupsize, i*groupsize + groupsize), 2).bitCount() % 2 == 0) {
				checksum.append("1");
			} else {
				checksum.append("0");
			}
		}
		return checksum.toString();
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1("00101000101111010", 272));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2("00101000101111010", 35651584));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
