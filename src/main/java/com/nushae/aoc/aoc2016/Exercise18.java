package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Exercise18 {

	String input;
	List<String> lines;

	public Exercise18(String data) {
		input = data;
	}

	public long doPart1() {
		lines = new ArrayList<>();
		lines.add(input);
		long safe = input.replace("^", "").length();
		while (lines.size() < 40) {
			String current = "." + lines.get(lines.size()-1) + ".";
			StringBuilder next = new StringBuilder();
			for (int c = 0; c < input.length(); c ++) {
				String patt = current.substring(c, c+3);
				if ("^^.".equals(patt) || ".^^".equals(patt) || "^..".equals(patt) || "..^".equals(patt)) {
					next.append("^");
				} else {
					next.append(".");
					safe++;
				}
			}
			lines.add(next.toString());
		}
		return safe;
	}

	public long doPart2() {
		long safe = input.replace("^", "").length();
		String previous = "." + input + ".";
		int count = 1;
		while (count < 400000) {
			StringBuilder next = new StringBuilder();
			for (int c = 0; c < input.length(); c ++) {
				String patt = previous.substring(c, c+3);
				if ("^^.".equals(patt) || ".^^".equals(patt) || "^..".equals(patt) || "..^".equals(patt)) {
					next.append("^");
				} else {
					next.append(".");
					safe++;
				}
			}
			previous = "." + next + ".";
			count++;
		}
		return safe;
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18("^^.^..^.....^..^..^^...^^.^....^^^.^.^^....^.^^^...^^^^.^^^^.^..^^^^.^^.^.^.^.^.^^...^^..^^^..^.^^^^");

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
