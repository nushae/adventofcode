package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.StorageNode;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise22 {

	private static final Pattern DF = Pattern.compile("/dev/grid/node-x(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)%");

	List<String> lines;
	List<StorageNode> nodes;
	StorageNode[][] storage;
	char[][] slider;
	int maxX;
	int maxY;
	int emptyX;
	int emptyY;

	public Exercise22() {
		lines = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		nodes = new ArrayList<>();
		for (int i = 2; i < lines.size(); i++) {
			String line = lines.get(i);
			Matcher m = DF.matcher(line);
			if (m.find()) {
				StorageNode s = new StorageNode(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
				maxX = Math.max(maxX, s.x);
				maxY = Math.max(maxY, s.y);
				if (s.used == 0) {
					emptyX = s.x;
					emptyY = s.y;
				}
				nodes.add(s);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		maxX++;
		maxY++;
		storage = new StorageNode[maxX][maxY];
		slider = new char[maxX][maxY];
		for (StorageNode s : nodes) {
			storage[s.x][s.y] = s;
			if (s.used == 0) { // the empty node
				slider[s.x][s.y] = '_';
			} else if (s.used > 100) { // blocked node
				slider[s.x][s.y] = '#';
			} else {
				slider[s.x][s.y] = '.';
			}
			slider[0][0] = 'O';
			slider[maxX-1][0] = 'G';
		}
	}

	public long doPart1() {
		processWithPattern();
		long result = 0;
		for (int idx = 0; idx < maxX * maxY - 1; idx++) {
			int x = idx % maxX;
			int y = idx / maxX;
			StorageNode n1 = storage[x][y];
			int idx1 = y * maxX + x + 1;
			while (idx1 < maxX * maxY) {
				int x1 = idx1 % maxX;
				int y1 = idx1 / maxX;
				StorageNode n2 = storage[x1][y1];
				if (n1.used > 0 && n2.avail >= n1.used) {
					result++;
				}
				if (n2.used > 0 && n1.avail >= n2.used) {
					result++;
				}
				idx1++;
			}
		}
		return result;
	}

	// It's a slide puzzle! (Yay)
	public long doPart2() {
		for (int y = 0; y < maxY; y++) {
			for (int x = 0; x < maxX; x++) {
				System.out.print(slider[x][y]);
			}
			System.out.println();
		}
		// Using this generated overview, I manually solved the puzzle.
		// 80 moves to move the _ to the left of the G. This is the manhattan distance (around the #) from the _ position
		// to the . to the left of the G.
		// Then, every time the G is moved to the left (with the _ returning to its left) we spend 5 moves - this is
		// needed 36 times.
		// Finally, the G is moved onto the O - 1 additional move.
		return 80+5*36+1;
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(aocPath(2016, 22));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
