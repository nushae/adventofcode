package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise04 {

	ArrayList<String> lines;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long result = 0;
		for (String line : lines) {
			Map<Character, String> frequency = new HashMap<>();
			String checksum = line.substring(line.length()-6, line.length()-1);
			line = line.substring(0, line.length()-7);
			String[] parts = line.split("-");
			long id = Long.parseLong(parts[parts.length-1]);
			StringBuilder roomName = new StringBuilder();
			for (int p = 0; p < parts.length - 1; p++) {
				roomName.append(parts[p]).append(" ");
				for (int c = 0; c < parts[p].length(); c++) {
					Character ch = parts[p].charAt(c);
					String letter = frequency.getOrDefault(ch, "");
					letter += ch;
					frequency.put(ch, letter);
				}
			}
			String newChecksum = frequency.values().stream()
					.sorted((a, b) -> a.length() > b.length() ? -1 : a.length() < b.length() ? 1 : a.compareTo(b))
					.map(a -> a.substring(0, 1))
					.reduce((a, b) -> a+b)
					.orElse("-----")
					.substring(0, 5);
			if (checksum.equals(newChecksum)) {
				String decrypted = decrypt(roomName.toString(), id);
				if (decrypted.contains("orth")) {
					System.out.println(id + " = " + decrypted);
				}
				result += id;
			}
		}
		return result;
	}

	public long doPart2() {
		return -1;
	}

	public String decrypt(String encrypted, long id) {
		StringBuilder decrypted = new StringBuilder();
		for (int c=0; c<encrypted.length(); c++) {
			char chr = encrypted.charAt(c);
			if (chr >= 'a' && chr <= 'z') {
				decrypted.append((char) (((chr - 'a' + id) % 26) + 'a'));
			} else {
				decrypted.append(chr);
			}
		}
		return decrypted.toString();
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2016, 4));

		// part 1 & 2
		// A bit of an exception: the solution to part 2 is printed out as the solution to part 1 is calculated
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
