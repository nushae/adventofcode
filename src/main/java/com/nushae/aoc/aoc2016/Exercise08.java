package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise08 {

	private static final Pattern SCREEN_INSTRUCTIONS = Pattern.compile("rect (\\d+)+x(\\d+)|rotate row y=(\\d+) by (\\d+)|rotate column x=(\\d+) by (\\d+)");

	ArrayList<String> lines;
	boolean[][] screen;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long processWithPattern() {
		long result = 0;
		screen = new boolean[50][6];
		for (String line : lines) {
			Matcher m = SCREEN_INSTRUCTIONS.matcher(line);
			if (m.find()) {
				if (line.startsWith("rect")) {
					int a = Integer.parseInt(m.group(1));
					int b = Integer.parseInt(m.group(2));
					for (int x = 0; x < a; x++) {
						for (int y = 0; y < b; y++) {
							screen[x][y] = true;
						}
					}
				} else if (line.startsWith("rotate row")) {
					boolean[] dummy = new boolean[50];
					int a = Integer.parseInt(m.group(3));
					int b = Integer.parseInt(m.group(4));
					for (int i = 0; i < 50; i++) {
						dummy[(i + b) % 50] = screen[i][a];
					}
					for (int i = 0; i < 50; i++) {
						screen[i][a] = dummy[i];
					}
				} else { // rotate column
					boolean[] dummy = new boolean[6];
					int a = Integer.parseInt(m.group(5));
					int b = Integer.parseInt(m.group(6));
					for (int i = 0; i < 6; i++) {
						dummy[(i + b) % 6] = screen[a][i];
					}
					System.arraycopy(dummy, 0, screen[a], 0, 6);
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		for (int y=0; y< 6; y++) {
			for (int x = 0; x < 50; x++) {
				result += screen[x][y] ? 1 : 0;
				System.out.print(screen[x][y] ? "#" : ".");
			}
			System.out.println();
		}
		return result;
	}

	public long doPart1() {
		return processWithPattern();
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2016, 8));

		// part 1 / 2
		// Again, the solution to part 2 is displayed as part 1 is calculated
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
