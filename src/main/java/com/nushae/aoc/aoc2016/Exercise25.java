package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.AssemBunnyInterpreter;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.aocPath;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
public class Exercise25 {

	private static final Pattern PARAMETER = Pattern.compile("cpy (\\d+) [abcd]");

	AssemBunnyInterpreter hopkins;
	List<String> lines;

	public Exercise25() {
		lines = new ArrayList<>();
		hopkins = new AssemBunnyInterpreter();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
		hopkins.loadProgramFromFile(path);
	}

	// extract A and B from the second and third instruction to calculate A*B, then find the smallest binary number C 101010..
	// so that C - A*B >= 0
	// then return C - A*B
	public long doPart1() {
		// if you want to analyse the output:
		// System.out.println("GO!\n");
		// hopkins.runProgram(0, 0, 0, 0);
		Matcher m = PARAMETER.matcher(lines.get(1));
		if (m.find()) {
			long A = Long.parseLong(m.group(1));
			m = PARAMETER.matcher(lines.get(2));
			if (m.find()) {
				long B = Long.parseLong(m.group(1));
				long C = 0;
				for (long bit = 2; C < A*B; bit = bit << 2) {
					C += bit;
				}
				return C - A*B;
			}
		}
		return -1;
	}

	// what is the lowest integer that can be used to initialize register a, so that the code outputs 0,1,0,1,... forever?
	// 00 cpy a d     | d = a                           | d = a + 11*231
	// 01 cpy 11 c    | for (c = 11; c != 0; c--) {     | NOP
	// 02 cpy 231 b   |   for (b=231; b !=0; b--) {     | NOP
	// 03 inc d       |     d++                         | NOP
	// 04 dec b       |     NOP                         | NOP
	// 05 jnz b -2    |   }                             | NOP
	// 06 dec c       |   NOP                           | NOP
	// 07 jnz c -5    | }                               | NOP
	// 08 cpy d a     | a = d                           | while (true) {
	// 09 jnz 0 0     | NOP                             |   for (a = init + 11*231; a != 0; a = a / 2) {
	// 10 cpy a b     | b = a                           |     NOP
	// 11 cpy 0 a     | a = 0                           |     NOP
	// 12 cpy 2 c     | c = 2                           |     NOP
	// 13 jnz b 2     | if (b!=0) goto 15               |     NOP
	// 14 jnz 1 6     | goto 20                         |     NOP
	// 15 dec b       | b--                             |     NOP
	// 16 dec c       | c--                             |     NOP
	// 17 jnz c -4    | if (c!=0) goto 13               |     NOP
	// 18 inc a       | a++                             |     NOP
	// 19 jnz 1 -7    | goto 12                         |     NOP
	// 20 cpy 2 b     | b = 2;                          |     NOP
	// 21 jnz c 2     | if (c!=0) goto 23               |     NOP
	// 22 jnz 1 4     | goto 26                         |     NOP
	// 23 dec b       | b--                             |     NOP
	// 24 dec c       | c--                             |     NOP
	// 25 jnz 1 -4    | goto 21                         |     NOP
	// 26 jnz 0 0     | NOP                             |     NOP
	// 27 out b       | out b                           |     out a % 2
	// 28 jnz a -19   | if (a != 0 ) goto 09            |   }
	// 29 jnz 1 -21   | goto 08                         | }
	// so this routine calculates a = init + 11*231 = init + 2541;
	// then eternally outputs the bits of a in reverse order.
	// 2541 in binary = 100111101101
	//                  101010101010 - 100111101101 = 2730 - 2541 = 189
	// so init = 189
	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2016, 25));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// traditionally, there is no part 2 for day 25
	}
}
