package com.nushae.aoc.aoc2016;

import com.nushae.aoc.domain.BasicGraphNode;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.AlgoUtil.permutation;
import static com.nushae.aoc.util.GraphUtil.*;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.factorial;

@Log4j2
public class Exercise24 {

	List<String> lines;
	Character[][] grid;
	Map<Character, Point> points;
	int height;
	int width;
	int pointsOfInterest;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		height = lines.size();
		width = lines.get(0).length();
		grid = new Character[width][height];
		pointsOfInterest = 0;
		points = new HashMap<>();
		for (int y = 0; y < height; y++) {
			String line = lines.get(y);
			for (int x = 0; x < width; x++) {
				grid[x][y] = line.charAt(x);
				if (grid[x][y] >= '0' && grid[x][y] <= '9') {
					pointsOfInterest++;
					points.put(grid[x][y], new Point(x, y));
				}
			}
		}
		for (Map.Entry<Character, Point> entry : points.entrySet()) {
			System.out.println(entry.getKey() + " at " + entry.getValue());
		}
	}

	public long doPart1() {
		processInput();
		return findPoiPath(false);
	}

	public long doPart2() {
		processInput();
		return findPoiPath(true);
	}

	public long findPoiPath(boolean fullCircle) {
		List<BasicGraphNode<Character>> pois = new ArrayList<>();
		BasicGraphNode<Character> start = gridToGraph(grid, '0', new HashSet<>(Collections.singletonList('#')));
		for (int i=1; i<pointsOfInterest; i++) {
			BasicGraphNode<Character> node = findGraphNodeWithLocation(start, points.get((char) ('0'+i)));
			pois.add(node);
		}
		Map<Point, Long> distances = new HashMap<>();
		for (BasicGraphNode<Character> poi : pois) {
			long dist = findShortestDistanceInGraph(start, poi.getPayload());
			start.addNeighbour(poi, dist);
			distances.put(new Point(0, poi.getPayload() - '0'), dist);
		}
		for (int c = 0; c < pois.size()-1; c++) {
			BasicGraphNode<Character> poi1 = pois.get(c);
			for (int d = c + 1; d < pois.size(); d++) {
				BasicGraphNode<Character> poi2 = pois.get(d);
				long dist = findShortestDistanceInGraph(poi1, poi2.getPayload());
				poi1.addNeighbour(poi2, dist);
				poi2.addNeighbour(poi1, dist);
				distances.put(new Point(poi1.getPayload() - '0', poi2.getPayload() - '0'), dist);
			}
		}
		// we now have a matrix of distances from 0 - i and from i-j for all POIs in the maze. I think the
		// number of POIs (7) is low enough to brute force it.
		// So we simply calculate the shortest sum of distances 0 - a - b- c - d - e - f - g - h for all permutations a...g of [1, ..., 7]
		// and track the minimum. For part 2 we append 0 as an extra POI.
		int minDist = Integer.MAX_VALUE;
		for (int i = 0; i < factorial(pointsOfInterest-1); i++) {
			int from = 0;
			int totalDist = 0;
			for (int d : permutation(i, Arrays.asList(1, 2, 3, 4, 5, 6, 7))) {
				totalDist += distances.get(new Point(Math.min(from, d), Math.max(from, d)));
				from = d;
			}
			if (fullCircle) {
				totalDist += distances.get(new Point(0, from));
			}
			if (minDist > totalDist) {
				minDist = totalDist;
			}
		}
		return minDist;
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2016, 24));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 (a bit of a letdown)
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
