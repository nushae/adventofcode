package com.nushae.aoc.aoc2016.domain;

import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

public class AssemBunnyInterpreter {

	int instructionPointer;
	private List<String> origProgram;
	private List<String> currProgram; // since we're self modifying, we need to have a working copy
	private Map<Character, Long> registers;

	public void loadProgramFromFile(Path path) {
		origProgram = new ArrayList<>();
		registers = new HashMap<>();
		instructionPointer = 0;
		registers.put('a', 0L);
		registers.put('b', 0L);
		registers.put('c', 0L);
		registers.put('d', 0L);
		loadInputFromFile(origProgram, path);
	}

	public void execute(String opcode, String par1, String par2) {
		//System.out.printf("%04d[a:%d|b:%d|c:%d|d:%d] %s %s %s -> ", instructionPointer, registers.get('a'), registers.get('b'), registers.get('c'), registers.get('d'), opcode, par1, par2 != null ? par2 : "");
		switch (opcode) {
			case "cpy":
				if (par1 != null && par1.length() > 0) {
					try {
						long num;
						char reg = par1.charAt(0);
						if (registers.containsKey(reg)) {
							num = registers.get(reg);
						} else {
							num = Long.parseLong(par1);
						}
						if (par2 != null && par2.length() > 0) {
							char reg2 = par2.charAt(0);
							if (registers.containsKey(reg2)) {
								registers.put(reg2, num);
							}
						}
					} catch (NumberFormatException e) {
						System.out.println("OOPS: " + e.getMessage());
						// NOP; just skip instruction
					}
				}
				break;
			case "inc":
				if (par1 != null && par1.length() > 0) {
					char reg = par1.charAt(0);
					if (registers.containsKey(reg)) {
						registers.put(reg, registers.get(reg) + 1);
					}
				}
				break;
			case "dec":
				if (par1 != null && par1.length() > 0) {
					char reg = par1.charAt(0);
					if (registers.containsKey(reg)) {
						registers.put(reg, registers.get(reg) - 1);
					}
				}
				break;
			case "jnz":
				if (par1 != null && par1.length() > 0) {
					try {
						long num;
						char reg = par1.charAt(0);
						if (registers.containsKey(reg)) {
							num = registers.get(reg);
						} else {
							num = Long.parseLong(par1);
						}
						if (num != 0) {
							if (par2 != null && par2.length() > 0) {
								char reg2 = par2.charAt(0);
								long jmp;
								if (registers.containsKey(reg2)) {
									jmp = registers.get(reg2);
								} else {
									jmp = Long.parseLong(par2);
								}
								if (jmp > -currProgram.size() && jmp < currProgram.size()) { // no need to try if the jmp val is too large, also avoids overflow
									instructionPointer += jmp - 1;
								}
							}
						}
					} catch (NumberFormatException e) {
						System.out.println("OOPS: " + e.getMessage());
						// NOP; just skip this instruction
					}
				}
				break;
			case "tgl":
				if (par1 != null && par1.length() > 0) {
					long jmp;
					char reg = par1.charAt(0);
					if (registers.containsKey(reg)) {
						jmp = registers.get(reg);
					} else {
						jmp = Long.parseLong(par1);
					}
					if (jmp > -currProgram.size() && jmp < currProgram.size()) { // no need to try if the jmp val is too large, also avoids overflow
						int modLoc = (int) (instructionPointer + jmp);
						if (modLoc >= 0 && modLoc < currProgram.size()) {
							String[] modParts = currProgram.get(modLoc).split(" ");
							if (modParts.length > 2) { // 2-arg
								if ("jnz".equals(modParts[0])) {
									modParts[0] = "cpy";
								} else {
									modParts[0] = "jnz";
								}
							} else { // 1-arg
								if ("inc".equals(modParts[0])) {
									modParts[0] = "dec";
								} else {
									modParts[0] = "inc";
								}
							}
							currProgram.set(modLoc, StringUtils.join(modParts, " "));
						}
					}
				}
				break;
			case "out":
				if (par1 != null && par1.length() > 0) {
					char reg = par1.charAt(0);
					long num;
					if (registers.containsKey(reg)) {
						num = registers.get(reg);
					} else {
						num = Long.parseLong(par1);
					}
					System.out.println(num);
				}
				break;
			default:
				throw new IllegalArgumentException("Unknown opcode : '" + opcode + "'");
		}
		instructionPointer++;
		// System.out.printf("%04d[a:%d|b:%d|c:%d|d:%d]%n", instructionPointer, registers.get('a'), registers.get('b'), registers.get('c'), registers.get('d'));
	}

	public long runProgram(long a, long b, long c, long d) {
		registers = new HashMap<>();
		registers.put('a', a);
		registers.put('b', b);
		registers.put('c', c);
		registers.put('d', d);
		instructionPointer = 0;
		currProgram = new ArrayList<>();
		currProgram.addAll(origProgram);
		while (instructionPointer >=0 && instructionPointer < currProgram.size()) {
			String[] parts = currProgram.get(instructionPointer).split(" ");
			execute(parts[0], parts[1], parts.length > 2 ? parts[2] : null);
		}
		return registers.get('a');
	}
}
