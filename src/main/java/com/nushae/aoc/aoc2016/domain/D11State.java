package com.nushae.aoc.aoc2016.domain;

import com.nushae.aoc.domain.SearchNode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

@Log4j2
public class D11State implements SearchNode {

	public final static Map<String, String> LONG_ELEMENTS = Map.of(
		"co", "cobalt",
		"cu", "curium",
		"di", "dilithium",
		"el", "elerium",
		"hy", "hydrogen",
		"li", "lithium",
		"pl", "plutonium",
		"pr", "promethium",
		"ru", "ruthenium"
	);

	public final static Map<String, String> SHORT_ELEMENTS = Map.of(
		"cobalt", "co",
		"curium", "cu",
		"dilithium", "di",
		"elerium", "el",
		"hydrogen", "hy",
		"lithium", "li",
		"plutonium", "pl",
		"promethium", "pr",
		"ruthenium", "ru"
	);

	private final Map<Integer, Set<String>> generators;
	private final Map<Integer, Set<String>> microchips;
	private final Map<Integer, Set<String>> pairs;
	private String rep;
	private String fingerprint;
	// for path tracking
	@Getter
	@Setter
	private SearchNode parent;
	@Setter
	private int elevator;

	public D11State() {
		generators = new HashMap<>();
		microchips = new HashMap<>();
		pairs = new HashMap<>();
		rep = "";
		fingerprint = "";
	}

	public D11State(String rep) { // for testing purposes
		this();
		String[] parts = rep.split("-");
		elevator = Integer.parseInt(parts[0]);
		for (int floor = 1; floor < 5; floor++) {
			String part = parts[floor].substring(2, parts[floor].length() - 1);
			while (!part.isEmpty()) {
				String thing = part.substring(0, 3);
				if (thing.charAt(0) != 'M') {
					add(floor, "G", LONG_ELEMENTS.get(thing.substring(1)));
				}
				if (thing.charAt(0) != 'G') {
					add(floor, "M", LONG_ELEMENTS.get(thing.substring(1)));
				}
				part = part.substring(3);
			}
		}
	}

	public int getTotalGeneratorCount(int floor) {
		return generators.getOrDefault(floor, new TreeSet<>()).size() + pairs.getOrDefault(floor, new TreeSet<>()).size();
	}

	public int getTotalChipCount(int floor) {
		return microchips.getOrDefault(floor, new TreeSet<>()).size() + pairs.getOrDefault(floor, new TreeSet<>()).size();
	}

	public List<String> getUnpairedGenerators(int floor) {
		return new ArrayList<>(generators.getOrDefault(floor, new TreeSet<>()));
	}

	public List<String> getUnpairedChips(int floor) {
		return new ArrayList<>(microchips.getOrDefault(floor, new TreeSet<>()));
	}

	public List<String> getPairs(int floor) {
		return new ArrayList<>(pairs.getOrDefault(floor, new TreeSet<>()));
	}

	public void add(int floor, String type, String element) {
		if ("G".equals(type)) {
			if (microchips.getOrDefault(floor, new TreeSet<>()).contains(element)) {
				microchips.get(floor).remove(element);
				Set<String> stuff = pairs.getOrDefault(floor, new TreeSet<>());
				stuff.add(element);
				pairs.put(floor, stuff);
			} else {
				Set<String> stuff = generators.getOrDefault(floor, new TreeSet<>());
				stuff.add(element);
				generators.put(floor, stuff);
			}
		} else {
			if (generators.getOrDefault(floor, new TreeSet<>()).contains(element)) {
				generators.get(floor).remove(element);
				Set<String> stuff = pairs.getOrDefault(floor, new TreeSet<>());
				stuff.add(element);
				pairs.put(floor, stuff);
			} else {
				Set<String> stuff = microchips.getOrDefault(floor, new TreeSet<>());
				stuff.add(element);
				microchips.put(floor, stuff);
			}
		}
		rep = "";
	}

	public void remove(int floor, String type, String element) {
		if ("G".equals(type)) {
			if (pairs.getOrDefault(floor, new TreeSet<>()).contains(element)) {
				pairs.get(floor).remove(element);
				Set<String> stuff = microchips.getOrDefault(floor, new TreeSet<>());
				stuff.add(element);
				microchips.put(floor, stuff);
			} else {
				Set<String> stuff = generators.getOrDefault(floor, new TreeSet<>());
				stuff.remove(element);
				generators.put(floor, stuff);
			}
		} else {
			if (pairs.getOrDefault(floor, new TreeSet<>()).contains(element)) {
				pairs.get(floor).remove(element);
				Set<String> stuff = generators.getOrDefault(floor, new TreeSet<>());
				stuff.add(element);
				generators.put(floor, stuff);
			} else {
				Set<String> stuff = microchips.getOrDefault(floor, new TreeSet<>());
				stuff.remove(element);
				microchips.put(floor, stuff);
			}
		}
		rep = "";
	}

	/**
	 * <p><b>Has bugs, preserved for historical reasons</b></p>
	 * <p>Generate a list of 'reachable adjacent nodes in the state graph', aka legal next moves.</p>
	 *
	 * <p>Since any unpaired M on a floor with another G will 'fry', the only possible legal floor contents are:</p>
	 * <ol>
	 * <li>any number of GM pairs, and any number of unpaired G</li>
	 * <li>any number of unpaired M, with no G</li>
	 * </ol>
	 *
	 * <p>General observations on what moves to consider:</p>
	 * <ul>
	 * <li>the elevator moves one floor at a time: only generate moves from the current floor to the one(s) directly above and below</li>
	 * <li>NOT TRUE?! (this appears to break for my particular example) - if you can move 2 items up, don't move just 1: only generate up moves with 1 item if you can't generate up moves with 2 items</li>
	 * <li>NOT TRUE?! (this appears to break for my particular example) - if you can move just 1 item down, don't move 2: only generate down moves with 2 items if you can't generate down moves with 1 item</li>
	 * <li>any GM pair is equivalent to any other: if there are multiple GM pairs on a floor, only move the pair that is lexicographically first</li>
	 * </ul>
	 *
	 * <p>1) Consider legal departures from the current floor:</p>
	 * <p><b>if</b> (G > 0 && G < 3 && M < G) <b>&rarr;</b> 'the' GM, any unpaired G or GG, "all G on the floor", any MM or M.<br/>
	 * <b>[]</b> (G > 2 && M < G) <b>&rarr;</b> 'the' GM, any unpaired G or GG, any MM or M.<br/>
	 * <b>[]</b> (G > 0 && G < 3 && M == G) <b>&rarr;</b> 'the' GM, "all G on the floor", any MM or M.<br/>
	 * <b>[]</b> (G > 2 && M == G) <b>&rarr;</b> 'the' GM, any MM or M.<br/>
	 * <b>[]</b> (G == 0) <b>&rarr;</b> any MM or M<br/>
	 * <b>fi</b></p>
	 *
	 * <p>2) Consider legal arrivals on the destination floor:</p>
	 * <p><b>if</b> (G > 0 && M < G) <b>&rarr;</b> 'the' GM, any GG or G, any M or MM that can be paired<br/>
	 * <b>[]</b> (G > 0 && M == G) <b>&rarr;</b> 'the' GM, any GG or G <br/>
	 * <b>[]</b> (G == 0 && M > 0 && M < 3) <b>&rarr;</b> any MM or M, "the GG or G that pairs everything"<br/>
	 * <b>[]</b> (G == 0 && M > 2) <b>&rarr;</b> any MM or M<br/>
	 * <b>[]</b> (G == 0 && M == 0) <b>&rarr;</b> anything: 'the' GM, any GG or G, any MM or M<br/>
	 * <b>fi</b></p>
	 *
	 * <p>3) Finally, if we're generating up moves we only generate moves of 1 item if we have no legal moves of 2 items;
	 * if we're generating down moves we only generate moves of 2 items if we have no legal moves of 1 item.</p>
	 *
	 * @return a Map of SearchNodes representing legal next moves
	 */
	public Map<SearchNode, Long> getAdjacentNodesSmart() {
		log.debug("Generating moves for: " + rep());
		Map<SearchNode, Long> result = new HashMap<>();

		// 1) departures
		String[] gmThatCanDepart = null;
		List<String[]> pairsThatCanDepart = new ArrayList<>();
		List<String> singlesThatCanDepart = new ArrayList<>();
		List<String> pairElementsHere = getPairs(elevator);
		List<String> genElementsHere = getUnpairedGenerators(elevator);
		List<String> chpElementsHere = getUnpairedChips(elevator);
		int G = getTotalGeneratorCount(elevator);
		int M = getTotalChipCount(elevator);
		// M can always depart, it appears, so always generate: any MM, any M.
		for (String element : pairElementsHere) {
			log.debug("---> single chip move (removed from pair) departure from " + elevator);
			singlesThatCanDepart.add("M " + element);
		}
		for (String element : chpElementsHere) {
			log.debug("---> single chip move departure from " + elevator);
			singlesThatCanDepart.add("M " + element);
		}
		for (int firstThing = 0; firstThing < singlesThatCanDepart.size() - 1; firstThing++) {
			for (int secondThing = firstThing + 1; secondThing < singlesThatCanDepart.size(); secondThing++) {
				log.debug("---> double chip move departure from " + elevator);
				pairsThatCanDepart.add(new String[]{singlesThatCanDepart.get(firstThing), singlesThatCanDepart.get(secondThing)});
			}
		}
		if (G > 0) {
			// generate 'the' GM pair if possible. (Any pair will do, we take the one that comes lexicographically first.)
			if (M > 0) {
				gmThatCanDepart = new String[]{"G " + pairElementsHere.get(0), "M " + pairElementsHere.get(0)};
			}
			// 'the move that removes all G'; the G could be split among GM and unpaired G's though
			if (G < 3) {
				int item = 0;
				String[] gennies = new String[pairElementsHere.size() + genElementsHere.size()];
				for (String element : pairElementsHere) {
					gennies[item++] = "G " + element;
				}
				for (String element : genElementsHere) {
					gennies[item++] = "G " + element;
				}
				if (gennies.length > 1) {
					log.debug("-x-> double generator move departure from " + elevator + ": " + gennies[0] + " / " + gennies[1]);
					pairsThatCanDepart.add(gennies);
				} else {
					log.debug("-x-> single generator move departure from " + elevator + ": " + gennies[0]);
					singlesThatCanDepart.add(gennies[0]);
				}
			}
			if (M < G) {
				// generate: (for unpaired G) all GG and G
				for (String element : genElementsHere) {
					log.debug("-u-> single generator move departure from " + elevator + ": " + element);
					singlesThatCanDepart.add("G " + element);
				}
				for (int e = 0; e < genElementsHere.size() - 1; e++) {
					for (int f = e + 1; f < genElementsHere.size(); f++) {
						log.debug("-u-> double generator move departure from " + elevator + ": " + genElementsHere.get(e) + " / " + genElementsHere.get(f));
						pairsThatCanDepart.add(new String[]{"G " + genElementsHere.get(e), "G " + genElementsHere.get(f)});
					}
				}
			} else { // M == G
				if (M < 3) { // also G but, you know, it's the chips that make this happen, so credit where credit is due
					// generate: the pair or single that removes every G. These all come from GM pairs
					if (M == 1) {
						log.debug("-p-> single generator move (removing from pair) departure from " + elevator + ": " + gmThatCanDepart[0]);
						singlesThatCanDepart.add(gmThatCanDepart[0]);
					} else {
						log.debug("-p-> double generator move (removing from pairs) departure from " + elevator + ": " + gmThatCanDepart[0] + " / " + pairElementsHere.get(1));
						pairsThatCanDepart.add(new String[]{gmThatCanDepart[0], "G " + pairElementsHere.get(1)});
					}
				}
			}
		}

		// 2) arrivals
		for (int destinationFloor = elevator - 1; destinationFloor <= elevator + 1; destinationFloor += 2) {
			if (destinationFloor >= 1 && destinationFloor <= 4) { // so... floor - 1, if possible, then floor + 1, if possible
				log.debug("  Going " + (destinationFloor < elevator ? "down" : " up") + ":");
				List<D11State> singleArrivals = new ArrayList<>();
				List<D11State> pairArrivals = new ArrayList<>();
				G = getTotalGeneratorCount(destinationFloor);
				M = getTotalChipCount(destinationFloor);
				if (G > 0) {
					// the GM, any GG, any G
					if (gmThatCanDepart != null) {
						pairArrivals.add(generateMove(gmThatCanDepart, elevator, destinationFloor));
					}
					for (String[] pair : pairsThatCanDepart) {
						if (pair[0].startsWith("G")) {
							pairArrivals.add(generateMove(pair, elevator, destinationFloor));
						}
					}
					for (String single : singlesThatCanDepart) {
						if (single.startsWith("G")) {
							singleArrivals.add(generateMove(new String[]{single}, elevator, destinationFloor));
						}
					}
					if (M < G) {
						// we can move any M that can be paired with one of the unpaired G here
						List<String> unpairedGens = getUnpairedGenerators(destinationFloor);
						for (int first = 0; first < unpairedGens.size(); first++) {
							String firstElement = unpairedGens.get(first);
							if (singlesThatCanDepart.contains("M " + firstElement)) {
								log.debug("---> single chip move arrival on " + destinationFloor);
								singleArrivals.add(generateMove(new String[]{"M " + firstElement}, elevator, destinationFloor));
								for (int second = first + 1; second < unpairedGens.size(); second++) {
									String secondElement = unpairedGens.get(second);
									if (singlesThatCanDepart.contains("M " + secondElement)) {
										log.debug("---> double chip move arrival on " + destinationFloor);
										pairArrivals.add(generateMove(new String[]{"M " + firstElement, "M " + secondElement}, elevator, destinationFloor));
									}
								}
							}
						}
					}
				} else { // G == 0
					// any MM, any M
					for (String[] pair : pairsThatCanDepart) {
						if (pair[0].startsWith("M")) {
							pairArrivals.add(generateMove(pair, elevator, destinationFloor));
						}
					}
					for (String single : singlesThatCanDepart) {
						if (single.startsWith("M")) {
							singleArrivals.add(generateMove(new String[]{single}, elevator, destinationFloor));
						}
					}
					if (M > 0) {
						if (M == 2) {
							// if possible, move the matching GG there (and pair everything)
							String[] destPair = new String[2];
							int i = 0;
							for (String element : getUnpairedChips(destinationFloor)) {
								destPair[i++] = "G " + element; // prepend "G " to this chip for easier matching
							}
							for (String[] gPair : pairsThatCanDepart) {
								if (destPair[0].equals(gPair[0]) && destPair[1].substring(2).equals(gPair[1])) {
									pairArrivals.add(generateMove(gPair, elevator, destinationFloor));
								}
							}
						} else if (M == 1) {
							// if possible, move the matching G there (and pair everything)
							String destSingle = "G " + getUnpairedChips(destinationFloor).get(0);
							for (String single : singlesThatCanDepart) {
								if (destSingle.equals(single)) {
									singleArrivals.add(generateMove(new String[]{single}, elevator, destinationFloor));
								}
							}
						}
					} else {
						// the GM, any GG, any G
						if (gmThatCanDepart != null) {
							pairArrivals.add(generateMove(gmThatCanDepart, elevator, destinationFloor));
						}
						for (String[] pair : pairsThatCanDepart) {
							if (pair[0].startsWith("G")) {
								log.debug("---> single generator move arrival on " + destinationFloor);
								pairArrivals.add(generateMove(pair, elevator, destinationFloor));
							}
						}
						for (String single : singlesThatCanDepart) {
							if (single.startsWith("G")) {
								log.debug("---> double generator move arrival on " + destinationFloor);
								singleArrivals.add(generateMove(new String[]{single}, elevator, destinationFloor));
							}
						}
					}
				}
				// 3) choose actual moves based on up or down
				// I've used this heuristic but then no path is found, apparently for my input you DO need to move
				// a pair down when there are singles as well
				if (destinationFloor > elevator) { // going up: only generate moves of 1 item if we have no legal moves of 2 items
					log.debug("  Up moves from " + elevator + " to " + destinationFloor + " - pairs before singles");
					for (D11State move : pairArrivals) {
						result.put(move, 1L);
						log.debug("    Added " + move.rep() + " to move set.");
					}
					//if (pairArrivals.isEmpty()) {
					for (D11State move : singleArrivals) {
						result.put(move, 1L);
						log.debug("    Added " + move.rep() + " to move set.");
					}
					//}
				} else { // going down: only generate moves of 2 items if we have no legal moves of 1 item
					log.debug("  Down moves from " + elevator + " to " + destinationFloor + " - singles before pairs");
					for (D11State move : singleArrivals) {
						result.put(move, 1L);
						log.debug("    Added " + move.rep() + " to move set.");
					}
					//if (singleArrivals.isEmpty()) {
					for (D11State move : pairArrivals) {
						result.put(move, 1L);
						log.debug("    Added " + move.rep() + " to move set.");
					}
					//}
				}
			}
		}
		return result;
	}

	/**
	 * Generate a list of valid next states (aka moves) by stupidly generating all possibilities and validating them.
	 * (This generates the same set as the 'smart' version but is a lot simpler to understand -_-)
	 *
	 * @return set of all valid next states
	 */
	@Override
	public Map<SearchNode, Long> getAdjacentNodes() {
		List<String> stuff = new ArrayList<>();
		List<String[]> options = new ArrayList<>();
		Map<SearchNode, Long> result = new HashMap<>();
		for (String element : getUnpairedGenerators(elevator)) {
			stuff.add("G " + element);
		}
		for (String element : getPairs(elevator)) {
			stuff.add("G " + element);
			stuff.add("M " + element);
		}
		for (String element : getUnpairedChips(elevator)) {
			stuff.add("M " + element);
		}
		for (int a = 0; a < stuff.size(); a++) {
			options.add(new String[]{stuff.get(a)});
			for (int b = a + 1; b < stuff.size(); b++) {
				options.add(new String[]{stuff.get(a), stuff.get(b)});
			}
		}
		for (String[] option : options) {
			if (elevator > 1) {
				D11State possibleMove = generateMove(option, elevator, elevator - 1);
				if (possibleMove.isValid()) {
					result.put(possibleMove, 1L);
				}
			}
			if (elevator < 4) {
				D11State possibleMove = generateMove(option, elevator, elevator + 1);
				if (possibleMove.isValid()) {
					result.put(possibleMove, 1L);
				}
			}
		}
		return result;
	}

	private D11State generateMove(String[] items, int fromFloor, int toFloor) {
		D11State newState = new D11State();
		newState.setParent(this);
		for (int floor = 1; floor < 5; floor++) {
			for (String gen : generators.getOrDefault(floor, new TreeSet<>())) {
				newState.add(floor, "G", gen);
			}
			for (String chp : microchips.getOrDefault(floor, new TreeSet<>())) {
				newState.add(floor, "M", chp);
			}
			for (String both : pairs.getOrDefault(floor, new TreeSet<>())) {
				newState.add(floor, "G", both);
				newState.add(floor, "M", both);
			}
		}
		for (String item : items) {
			String[] parts = item.split(" ");
			newState.remove(fromFloor, parts[0], parts[1]);
			newState.add(toFloor, parts[0], parts[1]);
		}
		newState.setElevator(toFloor);
		log.debug("    Generated " + (items.length > 1 ? "pair" : "single") + " move: " + newState.rep());
		return newState;
	}

	public boolean isValid() {
		for (int floor = 1; floor < 5; floor++) {
			for (String element : microchips.getOrDefault(floor, new HashSet<>())) {
				Set<String> gens = generators.getOrDefault(floor, new HashSet<>());
				Set<String> prs = pairs.getOrDefault(floor, new HashSet<>());
				if (!gens.isEmpty() && !gens.contains(element) || !prs.isEmpty() && !prs.contains(element)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * <p>Creates a compact String representation of this node's state that can be used to recreate the state, of the form:
	 * <b>[</b>&lt;currentfloor&gt;<b>]-1(</b>&lt;stuff on floor&gt;<b>)2(</b>&lt;stuff on floor&gt;<b>)3(</b>&lt;stuff on floor&gt;<b>)4(</b>&lt;stuff on floor&gt;<b>)</b></p>
	 * <p>Where currentfloor == 1..4 and stuff on floor == {G[a-z][a-z]}{M[a-z][a-z]}{P[a-z][a-z]}</p>
	 *
	 * @return String representation of this node's state
	 */
	public String rep() {
		if ("".equals(rep)) {
			StringBuilder result = new StringBuilder();
			result.append(elevator);
			for (int floor = 1; floor < 5; floor++) {
				result.append("-").append(floor).append("(");
				for (String gen : generators.getOrDefault(floor, new TreeSet<>())) {
					result.append("G").append(SHORT_ELEMENTS.get(gen));
				}
				for (String chp : microchips.getOrDefault(floor, new TreeSet<>())) {
					result.append("M").append(SHORT_ELEMENTS.get(chp));
				}
				for (String pr : pairs.getOrDefault(floor, new TreeSet<>())) {
					result.append("P").append(SHORT_ELEMENTS.get(pr));
				}
				result.append(")");
			}
			rep = result.toString();
		}
		return rep;
	}

	/**
	 * Creates a String representation that reduces a state to its 'fingerprint' - the elevator floor, followed
	 * by the pattern of G,P,M ignoring the actual element types. Two states with the same fingerprint are
	 * interchangeable for the purpose of comparing moves - either will lead to the same solution path. This is
	 * useful for pruning duplicate moves.
	 * Note that this will sometimes yield the same fingerprint for two states that aren't truly interchangeable,
	 * but for the algorithm that turns out to make no difference. These are the three types of cases:
	 * (Only the path that results      (the G's must be brought       (Only the path that results
	 * in 2P will be solvable)       together eventually anyway)       in 2P will be solvable)
	 * 4  G1         4  G2          4  G2            4  G1             4  G2         4  G1
	 * 3  G2         3  G1         [3] G1            3  G2             3  M1         3  M1
	 * 2  M2   vs    2  M2          2  P3 P4   vs    2  P3 P4          2  G1   vs    2  G2
	 * [1] M1        [1] M1          1  M1 M2        [1] M1 M2         [1] M2        [1] M2
	 *
	 * @return the fingerprint of this state.
	 */
	@Override
	public String getState() {
		if ("".equals(fingerprint)) {
			StringBuilder result = new StringBuilder();
			result.append(elevator);
			for (int floor = 1; floor < 5; floor++) {
				result.append(":");
				result.append(StringUtils.repeat('G', generators.getOrDefault(floor, new TreeSet<>()).size()));
				result.append(StringUtils.repeat('M', microchips.getOrDefault(floor, new TreeSet<>()).size()));
				result.append(StringUtils.repeat('P', pairs.getOrDefault(floor, new TreeSet<>()).size()));
			}
			fingerprint = result.toString();
		}
		return fingerprint;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("+----+-------------------------------------------------------------------\n");
		for (int floor = 4; floor >= 1; floor--) {
			result.append(elevator == floor ? "|[" : "| ").append(floor).append(elevator == floor ? "] :" : "  :");
			for (String gen : generators.getOrDefault(floor, new TreeSet<>())) {
				result.append(" G:").append(gen);
			}
			for (String chp : microchips.getOrDefault(floor, new TreeSet<>())) {
				result.append(" M:").append(chp);
			}
			for (String pr : pairs.getOrDefault(floor, new TreeSet<>())) {
				result.append(" P:").append(pr);
			}
			result.append("\n");
			if (floor > 1) {
				result.append("|    +-------------------------------------------------------------------\n");
			} else {
				result.append("+----+-------------------------------------------------------------------\n");
			}
		}
		return result.toString();
	}

	@Override
	public int hashCode() {
		return getState().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (!(obj instanceof D11State)) {
			return false;
		}

		D11State other = (D11State) obj;
		return getState().equals(other.getState());
	}
}
