package com.nushae.aoc.aoc2016.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Bot implements InputBin, OutputBin {
	private TreeSet<Integer> values;
	private int myNum;
	public OutputBin givesLowTo;
	public OutputBin givesHighTo;
	List<InputBin> inputs;

	public Bot(int myNum) {
		inputs = new ArrayList<>();
		this.myNum = myNum;
		values = new TreeSet<>();
	}

	public int getNum() {
		return myNum;
	}

	public void addValue(int val) {
		values.add(val);
	}

	@Override
	public int getOutput() {
		if (!canPropagate()) {
			for (InputBin ib : inputs) {
				ib.propagate();
			}
		}
		return values.first();
	}

	@Override
	public void addInput(InputBin sender) {
		inputs.add(sender);
	}

	@Override
	public void setLowReceiver(OutputBin receiver) {
		givesLowTo = receiver;
	}

	@Override
	public void setHighReceiver(OutputBin receiver) {
		givesHighTo = receiver;
	}

	@Override
	public OutputBin getLowReceiver() {
		return givesLowTo;
	}

	@Override
	public OutputBin getHighReceiver() {
		return givesHighTo;
	}

	@Override
	public boolean canPropagate() {
		return values.size()>=2;
	}

	@Override
	public void propagate() {
		if (!canPropagate()) {
			for (InputBin ib : inputs) {
				ib.propagate();
			}
		}
		if (givesLowTo != null) {
			givesLowTo.addValue(values.first());
		}
		if (givesHighTo != null) {
			givesHighTo.addValue(values.last());
		}
	}

	@Override
	public int getLowVal() {
		return values.first();
	}

	@Override
	public int getHighVal() {
		return values.last();
	}
}
