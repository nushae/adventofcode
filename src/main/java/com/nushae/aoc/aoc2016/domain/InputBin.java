package com.nushae.aoc.aoc2016.domain;

public interface InputBin {
	int getLowVal();
	int getHighVal();
	void setLowReceiver(OutputBin receiver);
	void setHighReceiver(OutputBin receiver);
	OutputBin getLowReceiver();
	OutputBin getHighReceiver();
	boolean canPropagate();
	public void propagate();
}
