package com.nushae.aoc.aoc2016.domain;

public class SourceBin implements InputBin {
	private int myNum;
	private int myValue;
	public OutputBin givesLowTo;
	public OutputBin givesHighTo;

	public SourceBin(int num) {
		myNum = num;
		myValue = num;
	}

	public void setHighReceiver(OutputBin receiver) {
		this.givesHighTo = receiver;
	}

	@Override
	public OutputBin getLowReceiver() {
		return givesLowTo;
	}

	@Override
	public OutputBin getHighReceiver() {
		return givesHighTo;
	}

	@Override
	public boolean canPropagate() {
		return true;
	}

	public void propagate() {
		if (givesLowTo != null) {
			givesLowTo.addValue(myValue);
		}
		if (givesHighTo != null) {
			givesHighTo.addValue(myValue);
		}
	}

	public void setLowReceiver(OutputBin receiver) {
		this.givesLowTo = receiver;
	}

	public int getNum() {
		return myNum;
	}

	@Override
	public int getLowVal() {
		return myValue;
	}

	@Override
	public int getHighVal() {
		return myValue;
	}
}
