package com.nushae.aoc.aoc2016.domain;

import java.util.ArrayList;
import java.util.List;

public class DestBin implements OutputBin {
	private int myValue;
	private int myNum;
	public List<InputBin> inputs;
	boolean hasReceived;

	public DestBin(int num) {
		inputs = new ArrayList<>();
		myNum = num;
		hasReceived = false;
	}

	@Override
	public int getNum() {
		return myNum;
	}

	@Override
	public int getOutput() {
		if (hasReceived) {
			return myValue;
		} else {
			for (InputBin ib : inputs) {
				ib.propagate();
			}
			return myValue;
		}
	}

	@Override
	public void addValue(int val) {
		hasReceived = true;
		myValue = val;
	}

	@Override
	public void addInput(InputBin sender) {
		inputs.add(sender);
	}
}
