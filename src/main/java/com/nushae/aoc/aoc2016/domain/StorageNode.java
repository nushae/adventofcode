package com.nushae.aoc.aoc2016.domain;

public class StorageNode {

	public int x;
	public int y;
	public int size;
	public int used;
	public int avail;

	public StorageNode(int x, int y, int size, int used, int avail) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.used = used;
		this.avail = avail;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		StorageNode other = (StorageNode) obj;
		return x == other.x && y == other.y;
	}

	@Override
	public int hashCode() {
		return 17 * x + 13 * y;
	}
}
