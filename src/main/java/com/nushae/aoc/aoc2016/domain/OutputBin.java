package com.nushae.aoc.aoc2016.domain;

public interface OutputBin {
	void addValue(int val);
	void addInput(InputBin sender);
	int getNum();
	int getOutput();
}
