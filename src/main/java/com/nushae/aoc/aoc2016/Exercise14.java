package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.AlgoUtil.getMD5;

@Log4j2
public class Exercise14 {

	private static final Pattern QUINT = Pattern.compile("(.)\\1{4}");
	private static final Pattern TRIPLE = Pattern.compile("(.)\\1{2}");

	public long findHash64(String salt, boolean stretch) {
		long index = 0;
		String[] hashLookahead = new String[1001];
		TreeSet<Long> indexes = new TreeSet<>();
		while (indexes.size() < 64 || index - indexes.last() < 1001) {
			String md5;
			Matcher qm;
			do {
				md5 = getMD5(salt + index);
				if (stretch) {
					for (int s = 0; s < 2016; s++) {
						md5 = getMD5(md5);
					}
				}
				hashLookahead[(int)(index % 1001)] = md5;
				index++;
				qm = QUINT.matcher(md5);
			} while(!qm.find());
			for (int i = 0 ; i < 1000; i++) {
				int idx = ((int)(index % 1001) + i) % 1001;
				if (hashLookahead[idx] != null && hasFirstTrip(hashLookahead[idx], qm.group(1))) {
					long hashIndex = index - 1001 + i;
					indexes.add(hashIndex);
					if (indexes.size() >= 64 && index - indexes.stream().skip(63).findFirst().orElse(0L) >= 1001) {
						return indexes.stream().skip(63).findFirst().orElse(0L);
					}
				}
			}
		}
		return index - 1;
	}

	public boolean hasFirstTrip(String hash, String c) {
		Matcher m = TRIPLE.matcher(hash);
		if (m.find(0)) {
			return m.group(1).equals(c);
		}
		return false;
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.findHash64("zpqevtbw", false));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.findHash64("zpqevtbw", true));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
