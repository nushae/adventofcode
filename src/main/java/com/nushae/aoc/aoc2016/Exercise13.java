package com.nushae.aoc.aoc2016;

import com.nushae.aoc.domain.BasicGraphNode;
import lombok.extern.log4j.Log4j2;

import java.util.Collections;

import static com.nushae.aoc.util.GraphUtil.findShortestDistanceInGraph;
import static com.nushae.aoc.util.GraphUtil.gridToGraph;
import static com.nushae.aoc.util.MathUtil.manhattanDistance;

@Log4j2
public class Exercise13 {

	long input;

	public long doPart1(long input) {
		this.input = input;
		String[][] grid = new String[100][100];
		for (int y = 0; y < 100; y++) {
			for (int x = 0; x < 100; x++) {
				if (isOpen(x, y)) {
					grid[x][y] = ".";
				} else {
					grid[x][y] = "#";
				}
			}
		}
		grid[1][1] = "S";
		grid[31][39] = "T";
		BasicGraphNode<String> rootNode = gridToGraph(grid, "S", Collections.singleton("#"));
		return findShortestDistanceInGraph(rootNode, "T");
	}

	// not the most efficient but idgaf
	public long doPart2(long input) {
		this.input = input;
		String[][] grid = new String[100][100];
		for (int y = 0; y < 100; y++) {
			for (int x = 0; x < 100; x++) {
				if (isOpen(x, y)) {
					grid[x][y] = ".";
				} else {
					grid[x][y] = "#";
				}
			}
		}
		grid[1][1] = "S";
		long total = 1;
		for (int x = 0; x < 50; x++) {
			for (int y = 0; manhattanDistance(1, 1, x, y) <= 50; y++) {
				// System.out.println(x + ", " + y);
				if (!"#".equals(grid[x][y]) && !"S".equals(grid[x][y])) {
					grid[x][y] = "T";
					BasicGraphNode<String> rootNode = gridToGraph(grid, "S", Collections.singleton("#"));
					long dist = findShortestDistanceInGraph(rootNode, "T");
					if (dist > 0 && dist <= 50) {
						total++;
					}
					grid[x][y] = ".";
				}
			}
		}
		return total;
	}

	public boolean isOpen(long x, long y) {
		return Long.bitCount(x*x + 3*x + 2*x*y + y + y*y + input) % 2 == 0;
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(1358));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(1358));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
