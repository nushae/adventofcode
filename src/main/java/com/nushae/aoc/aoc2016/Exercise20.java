package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise20 {

	private static final Pattern RANGE = Pattern.compile("(\\d+)-(\\d+)");

	List<String> lines;
	List<long[]> ranges;

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		ranges = new ArrayList<>();
		for (String line : lines) {
			Matcher m = RANGE.matcher(line);
			if (m.find()) {
				long[] range = new long[2];
				range[0] = Long.parseLong(m.group(1));
				range[1] = Long.parseLong(m.group(2));
				ranges.add(range);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		processWithPattern();
		// sort ranges lexicographically for easier processing:
		ranges.sort(Comparator.comparingLong((long[] r) -> r[0]).thenComparing((long[] r) -> r[1]));
		long[] range1 = ranges.get(0);
		if (range1[0] > 0) { // yeah not likely
			return 0;
		}
		for (int i = 1; i < ranges.size(); i++) {
			long[] range2 = ranges.get(i);
			System.out.print(range1[0] + "-" + range1[1] + " vs " + range2[0] + "-" + range2[1]);
			if (range1[1] < range2[0]-1) { // there is a gap
				System.out.println(" has a gap, lowest value is " + (range1[1]+1));
				return range1[1] + 1;
			} else { // merge ranges, since they are sequential or overlapping
				System.out.print(" overlap so merging into ");
				range1[1] = Math.max(range1[1], range2[1]);
				System.out.println(range1[0] + "-" + range1[1]);
			}
		}
		return range1[1]+1;
	}

	// same basic algorithm, except we continue to the end and add up the gaps
	public long doPart2() {
		processWithPattern();
		ranges.sort(Comparator.comparingLong((long[] r) -> r[0]).thenComparing((long[] r) -> r[1]));
		long[] range1 = ranges.get(0);
		// just in case the first range doesn't start at 0 (of course it does)
		long result = range1[0];
		for (int i = 1; i < ranges.size(); i++) {
			long[] range2 = ranges.get(i);
			if (range1[1] < range2[0]-1) { // there is a gap
				System.out.print(range1[0] + "-" + range1[1] + " vs " + range2[0] + "-" + range2[1]);
				System.out.println(" has a gap, size is " + (range2[0] - range1[1] - 1) + " (continuing with " + range2[0] + "-" + range2[1] +")");
				result = result + range2[0] - range1[1] - 1;
				range1 = range2;
			} else { // merge ranges, since they are sequential or overlapping
				range1[1] = Math.max(range1[1], range2[1]);
			}
		}
		// don't forget the tail end
		result = result + (4294967295L - range1[1]);
		return result;
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2016, 20));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
