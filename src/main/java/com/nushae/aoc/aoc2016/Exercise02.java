package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise02 {

	// codePad for part 1:
	// 1 2 3
	// 4 5 6
	// 7 8 9
	private static final Map<Point, String> CODE_PAD_1 = Map.of(
		new Point(-1, 1), "1",
		new Point(0, 1), "2",
		new Point(1, 1), "3",
		new Point(-1, 0), "4",
		new Point(0, 0), "5",
		new Point(1, 0), "6",
		new Point(-1, -1), "7",
		new Point(0, -1), "8",
		new Point(1, -1), "9"
	);

	// codePad for part 2:
	// . . 1 . .
	// . 2 3 4 .
	// 5 6 7 8 9
	// . A B C .
	// . . D . .
	private static final Map<Point, String> CODE_PAD_2 = Map.ofEntries(
		Map.entry(new Point(-2, 2), "."),
		Map.entry(new Point(-2, 1), "."),
		Map.entry(new Point(-2, 0), "5"),
		Map.entry(new Point(-2, -1), "."),
		Map.entry(new Point(-2, -2), "."),
		Map.entry(new Point(-1, 2), "."),
		Map.entry(new Point(-1, 1), "2"),
		Map.entry(new Point(-1, 0), "6"),
		Map.entry(new Point(-1, -1), "A"),
		Map.entry(new Point(-1, -2), "."),
		Map.entry(new Point(0, 2), "1"),
		Map.entry(new Point(0, 1), "3"),
		Map.entry(new Point(0, 0), "7"),
		Map.entry(new Point(0, -1), "B"),
		Map.entry(new Point(0, -2), "D"),
		Map.entry(new Point(1, 2), "."),
		Map.entry(new Point(1, 1), "4"),
		Map.entry(new Point(1, 0), "8"),
		Map.entry(new Point(1, -1), "C"),
		Map.entry(new Point(1, -2), "."),
		Map.entry(new Point(2, 2), "."),
		Map.entry(new Point(2, 1), "."),
		Map.entry(new Point(2, 0), "9"),
		Map.entry(new Point(2, -1), "."),
		Map.entry(new Point(2, -2), ".")
	);
	ArrayList<String> lines;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public String findCode(Map<Point, String> codePad, Point loc) {
		StringBuilder code = new StringBuilder();
		for (String line : lines) {
			for (int c = 0; c < line.length(); c++) {
				char dir = line.charAt(c);
				Point newLoc;
				switch (dir) {
					case 'U':
						newLoc = new Point(loc.x, loc.y + 1);
						break;
					case 'D':
						newLoc = new Point(loc.x, loc.y - 1);
						break;
					case 'L':
						newLoc = new Point(loc.x - 1, loc.y);
						break;
					case 'R':
						newLoc = new Point(loc.x + 1, loc.y);
						break;
					default:
						log.debug("Unknown symbol '" + dir + "', ignoring");
						newLoc = null;
				}
				if (codePad.containsKey(newLoc) && !".".equals(codePad.get(newLoc))) {
					loc = newLoc;
				}
			}
			code.append(codePad.get(loc));
		}
		return code.toString();
	}

	public String doPart1() {
		return findCode(CODE_PAD_1, new Point(0, 0));
	}

	public String doPart2() {
		return findCode(CODE_PAD_2, new Point(-2, 0));
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2016, 2));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
