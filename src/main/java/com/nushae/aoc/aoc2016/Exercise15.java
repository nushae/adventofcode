package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.crt;

@Log4j2
public class Exercise15 {

	private static final Pattern DISC_DEF = Pattern.compile("Disc #\\d+ has (\\d+) positions; at time=0, it is at position (\\d+)\\.");

	List<String> lines;
	List<Point> discs;

	public Exercise15() {
		lines = new ArrayList<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		discs = new ArrayList<>();
		lines.stream().sorted().forEach(line -> {
			Matcher m = DISC_DEF.matcher(line);
			if (m.find()) {
				discs.add(new Point(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		});
	}

	public long doPart1() {
		processWithPattern();
		return calcCRT();
	}

	public long doPart2() {
		processWithPattern();
		discs.add(new Point(11, 0));
		int prod = 1;
		for (Point p : discs) {
			prod = prod * p.x;
		}
		System.out.println(prod);
		return calcCRT();
	}

	// All disc periods in the input are prime, so each pair is also co-prime, so we can use the chinese remainder theorem:
	//
	// "Given N = product(n_i) for a series of integers n_i that are pairwise co-prime. Then if 0 <= a_i < n_i are integers,
	// then there is precisely one integer X so that 0 <= X < N and X % n_i == a_i for every i."
	//
	// How is this useful here? Well, we are looking for a value t so that, for all i:
	// t + start_pos_i + (i+1)  == 0 (mod period_i), or: t == -start_pos - (i+1) (mod period_i)
	// So if we take the n_i to be period_i and the a_i to be -start_pos_i - i - 1, we can calculate t by way of the CRT
	//
	public long calcCRT() {
		List<Long> n = new ArrayList<>();
		List<Long> a = new ArrayList<>();
		for (int i = 0; i < discs.size(); i++) {
			n.add((long) discs.get(i).x);
			a.add(-discs.get(i).y-i-1L);
		}
		return crt(n, a);
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2016, 15));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
