package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Exercise19 {

	// If you try the 'game' for the first 10 or so numbers you start to see a pattern:
	// number:  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
	//          -  -     -           -                      --
	// winner:  1  1  3  1  3  5  7  1  3  5  7  9 11 13 15  1
	// In other words: at every power of 2 we restart with 1, and every number after is 2 higher
	// until the next power of 2... Easy.
	public long doPart1(long circleSize) {
		return 2 * (circleSize - (long) Math.pow(2, Math.floor(Math.log(circleSize)/Math.log(2)))) + 1;
	}

	// This time the pattern is a little harder to catch in a single calculation.
	// number:  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 ...
	//          -     -        -        -                         --                         --
	// winner:  1  1  3  1  2  3  5  7  9  1  2  3  4  5  6  7  8  9 11 13 15 17 19 21 23 25 27  1 ...
	// So at every power of 3 +1, we restart at 1, incrementing by 1 until we reach twice that power of 3.
	// then we continue with just the odd numbers until we reach the next power of 3, and so on.
	// Or:
	// For a circle size 3^N, the winner is 3^N.
	// For a circle size 3^N + i (i == 1..3^N) , the winner is i.
	// for a circle size 2*3^N, the winner is again 3^N.
	// for a circle size 2*3^N + i (i == 1..3^N), the winner is 3^N + 2*i
	// So:
	// let S = circle size
	// let P = 3 ^ floor(3log(S))
	// let R = S - P
	// if (R == 0) -> winner = S
	// [] (R <= S/2) -> winner = R
	// [] else -> winner = 2 * R - P
	// fi
	public long doPart2(long circleSize) {
		long P = (long) Math.pow(3, Math.floor(Math.log(circleSize)/Math.log(3)));
		long R = circleSize - P;
		if (R == 0) {
			return circleSize;
		} else if (R <= circleSize/2) {
			return R;
		}
		return 2 * R - P;
	}

	// After solving this by analysing the pattern by hand, I realised the problem title ("An elephant named Joseph")
	// refers to the Josephus problem -_-
	// So the above solutions can be even simpler. Proud of my own result though ;)
	public static void main(String[] args) {
		Exercise19 ex = new Exercise19();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(3017957));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(3017957));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
