package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise09 {

	private static final Pattern MARKER = Pattern.compile("(\\d+)x(\\d+)");

	ArrayList<String> lines;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		return decompressedLength(lines.get(0), false);
	}

	public long doPart2() {
		return decompressedLength(lines.get(0), true);
	}

	public long decompressedLength(String input, boolean recursive) {
		int position = 0;
		long result = 0;
		while (position < input.length()) {
			if (input.charAt(position) != '(') {
				result++;
				position++;
			} else { // opening (
				int closing = input.indexOf(')', position+1);
				String marker = input.substring(position+1, closing);
				position = closing+1;
				Matcher m = MARKER.matcher(marker);
				if (m.find()) {
					String repeat = input.substring(position, position + Integer.parseInt(m.group(1)));
					position += repeat.length();
					if (recursive) {
						result += decompressedLength(repeat, true) * Integer.parseInt(m.group(2));
					} else {
						result += (long) repeat.length() * Integer.parseInt(m.group(2));
					}
				} else {
					log.warn("I don't udnerstand this marker: " + marker);
					System.exit(0);
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2016, 9));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
