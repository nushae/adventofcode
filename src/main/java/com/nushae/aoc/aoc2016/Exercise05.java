package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.AlgoUtil.getMD5;
import static com.nushae.aoc.util.IOUtil.loadInputFromData;
import static com.nushae.aoc.util.IOUtil.loadInputFromFile;

@Log4j2
public class Exercise05 {

	ArrayList<String> lines;

	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public String doPart1() {
		StringBuilder result = new StringBuilder();
		for (String hash : findHashes(lines.get(0), false)) {
			result.append(hash.charAt(5));
		}
		return result.toString();
	}

	public String doPart2() {
		char[] result = new char[8];
		for (String hash : findHashes(lines.get(0), true)) {
			result[hash.charAt(5) - '0'] = hash.charAt(6);
		}
		StringBuilder sb = new StringBuilder();
		for (char c : result) {
			sb.append(c);
		}
		return sb.toString();
	}

	public String[] findHashes(String password, boolean skipInvalid) {
		String[] result = new String[8];
		boolean[] alreadyFound = new boolean[8];
		int found = 0;
		long index = 0;
		while (found < 8) {
			String myHash = getMD5(password + index);
			if (myHash.startsWith("00000")) {
				char chr = myHash.charAt(5);
				if (!skipInvalid || (chr >= '0' && chr <= '7') && !alreadyFound[chr - '0']) {
					if (skipInvalid) {
						alreadyFound[chr - '0'] = true;
					}
					result[found++] = myHash;
				}
			}
			index++;
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05("wtnhxymk");

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
