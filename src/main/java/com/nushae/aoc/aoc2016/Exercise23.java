package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.AssemBunnyInterpreter;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;
import static com.nushae.aoc.util.MathUtil.factorial;

@Log4j2
public class Exercise23 {

	AssemBunnyInterpreter hopkins;

	public Exercise23() {
		hopkins = new AssemBunnyInterpreter();
	}

	public Exercise23(Path path) {
		this();
		hopkins.loadProgramFromFile(path);
	}

	public long doPart1() {
		return hopkins.runProgram(7, 0, 0, 0);
	}

	public long doPart2() {
		// If you run the program normally, it will run for a looooong time (well, 307.5 seconds, just over 5 mins), but!
		// The program actually calculates factorial(a) + 80 * 94; so let's just do that here
		return factorial(12) + 80*94;
		// return hopkins.runProgram(12, 0, 0, 0);
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(aocPath(2016, 23));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
