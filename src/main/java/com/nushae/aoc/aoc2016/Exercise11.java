package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.D11State;
import com.nushae.aoc.domain.SearchNode;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.SearchUtil.dfs;

@Log4j2
public class Exercise11 {

	private static final Pattern INTERESTING_STUFF = Pattern.compile(" a ([a-z]+)(-compatible microchip| generator)");
	private static final Pattern FLOOR = Pattern.compile("(first|second|third|fourth)");

	ArrayList<String> lines;
	D11State stuff;
	D11State goal;

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		stuff = new D11State();
		goal = new D11State();
		int floorNum = 0;
		for (String line : lines) {
			Matcher m1 = FLOOR.matcher(line);
			if (m1.find()) {
				floorNum = "first".equals(m1.group(1)) ? 1 : "second".equals(m1.group(1)) ? 2 : "third".equals(m1.group(1)) ? 3 : 4;
			} else {
				log.warn("Don't know how to parse floor from " + line); // maybe part 2 adds extra floors...
			}
			Matcher m = INTERESTING_STUFF.matcher(line);
			while (m.find()) {
				stuff.add(floorNum, (m.group(2).startsWith("-") ? "M" : "G"), m.group(1));
				goal.add(4, (m.group(2).startsWith("-") ? "M" : "G"), m.group(1));
			}
		}
		stuff.setElevator(1);
		goal.setElevator(4);
	}

	public long doPart1(boolean verbose) {
		processWithPattern();
		return findSolution(verbose);
	}

	public long doPart2(boolean verbose) {
		processWithPattern();
		stuff.add(1, "M", "elerium");
		stuff.add(1, "G", "elerium");
		stuff.add(1, "M", "dilithium");
		stuff.add(1, "G", "dilithium");
		goal.add(4, "M", "elerium");
		goal.add(4, "G", "elerium");
		goal.add(4, "M", "dilithium");
		goal.add(4, "G", "dilithium");
		return findSolution(verbose);
	}

	public long findSolution(boolean verbose) {
		if (verbose) {
			System.out.println("Starting state: " + stuff.rep() + " with fingerprint " + stuff.getState());
			System.out.println("Goal state: " + goal.rep() + " with fingerprint " + goal.getState() + "\n\n" + stuff.toString());
		}
		long count = 0;
		SearchNode result = dfs(stuff, n -> goal.getState().equals(n.getState()));
		if (result != null) {
			List<String> moves = new ArrayList<>();
			if (verbose) {
				moves.add(result.toString());
			}
			while (result != null) {
				result = result.getParent();
				if (result != null) {
					if (verbose) {
						moves.add(result.toString());
					}
					count++;
				}
			}
			if (verbose) {
				for (int i = moves.size() - 2; i >= 0; i--) {
					System.out.println(moves.get(i));
				}
			}
		}
		return count;
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2016, 11));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(false));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(false));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
