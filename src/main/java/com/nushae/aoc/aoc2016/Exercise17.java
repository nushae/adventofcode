package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import static com.nushae.aoc.util.AlgoUtil.getMD5;

@Log4j2
public class Exercise17 {

	String[] dirs = new String[] { "U", "D", "L", "R"};
	int[] dx = new int[] {0, 0, -1, 1};
	int[] dy = new int[] {-1, 1, 0, 0};
	int pathLength;
	String prefix;
	String path;

	public String doPart1(String prefix) {
		this.prefix = prefix;
		pathLength = Integer.MAX_VALUE;
		findPath(true, "", 0, 0, 3, 3);
		return path;
	}

	public long doPart2(String prefix) {
		this.prefix = prefix;
		pathLength = -1;
		findPath(false, "", 0, 0, 3, 3);
		return pathLength;
	}

	public void findPath(boolean shortest, String pathSoFar, int currentX, int currentY, int maxX, int maxY) {
		if (currentX == maxX && currentY == maxY) { // reached goal
			if (shortest && pathSoFar.length() < pathLength || !shortest && pathSoFar.length() > pathLength) {
				if (shortest) {
					path = pathSoFar;
				}
				pathLength = pathSoFar.length();
			}
			return;
		}
		// if looking for shortest path prune; we can't prune for the longest path of course
		if (shortest && pathSoFar.length() >= pathLength) {
			return;
		}
		String hash = getMD5(prefix+pathSoFar).substring(0, 4);
		for (int i = 0; i < 4; i++) {
			int newX = currentX + dx[i];
			int newY = currentY + dy[i];
			if (hash.charAt(i) >= 'b' && hash.charAt(i) <= 'f' && newX >= 0 && newX <= maxX && newY >=0 && newY <= maxY) { // open, and within area
				findPath(shortest, pathSoFar + dirs[i], newX, newY, maxX, maxY);
			}
		}
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17();

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1("mmsxrhfx"));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2("mmsxrhfx"));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
