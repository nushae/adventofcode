package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.swapPos;

@Log4j2
public class Exercise21 {

	private static final Pattern SWAP_POS = Pattern.compile("swap position (\\d+) with position (\\d+)");
	private static final Pattern SWAP_LETTER = Pattern.compile("swap letter ([a-z]) with letter ([a-z])");
	private static final Pattern ROTATE = Pattern.compile("rotate (left|right) (\\d+) steps?");
	private static final Pattern ROTATE_BASED = Pattern.compile("rotate based on position of letter ([a-z])");
	private static final Pattern REVERSE = Pattern.compile("reverse positions (\\d+) through (\\d+)");
	private static final Pattern MOVE = Pattern.compile("move position (\\d+) to position (\\d+)");

	List<String> lines;

	public Exercise21() {
		lines = new ArrayList<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public String doPart1(String scramble, boolean unscramble) {
		for (int i = 0; i < lines.size(); i++) {
			String line = unscramble ? lines.get(lines.size()-1-i) : lines.get(i);
			if (line.startsWith("swap position")) { // this is its own inverse
				Matcher m = SWAP_POS.matcher(line);
				if (m.find()) {
					scramble = swapPos(scramble, Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
					log.debug(line + " -> " + scramble);
				} else {
					log.warn("Don't know how to parse swap position '" + line + "'");
				}
			} else if (line.startsWith("swap letter")) { // this is its own inverse
				Matcher m = SWAP_LETTER.matcher(line);
				if (m.find()) {
					scramble = swapPos(scramble, scramble.indexOf(m.group(1)), scramble.indexOf(m.group(2)));
					log.debug(line + " -> " + scramble);
				} else {
					log.warn("Don't know how to parse swap letter '" + line + "'");
				}
			} else if (line.startsWith("rotate based")) { // for input length 8 this is a bijection so reversable (whew)
				Matcher m = ROTATE_BASED.matcher(line);
				if (m.find()) {
					int pos = scramble.indexOf(m.group(1));
					// old pos:    0  1  2  3  4  5  6  7
					// amount:     1  2  3  4  6  7  8  9
					// new pos:    1  3  5  7  2  4  6  0
					// shiftback: -1 -2 -3 -4  2  1  0  7
					int amount;
					if (unscramble) {
						int newpos = pos % 2 == 0 ? (pos == 0 ? 7 : pos/2 + 3) : pos/2;
						amount = newpos - pos;
					} else {
						amount = 1 + pos + (pos >= 4 ? 1 : 0);
					}
					scramble = StringUtils.rotate(scramble, amount);
					log.debug(line + " -> " + scramble);
				} else {
					log.warn("Don't know how to parse rotate based '" + line + "'");
				}
			} else if (line.startsWith("rotate")) { // to unscramble, reverse the meanings of left and right
				Matcher m = ROTATE.matcher(line);
				if (m.find()) {
					int dir = (unscramble ? -1 : 1) * ("left".equals(m.group(1)) ? -1 : 1);
					int amount = Integer.parseInt(m.group(2)) * dir;
					scramble = StringUtils.rotate(scramble, amount);
					log.debug(line + " -> " + scramble);
				} else {
					log.warn("Don't know how to parse rotate '" + line + "'");
				}
			} else if (line.startsWith("reverse")) { // this is its own inverse
				Matcher m = REVERSE.matcher(line);
				if (m.find()) {
					int pos1 = Integer.parseInt(m.group(1));
					int pos2 = Integer.parseInt(m.group(2));
					if (pos2 < pos1) { // swap pos1 and pos2
						pos1 = pos1 + pos2;
						pos2 = pos1 - pos2;
						pos1 = pos1 - pos2;
					}
					String front = pos1 > 0 ? scramble.substring(0, pos1) : "";
					String middle = scramble.substring(pos1, pos2+1);
					String back = pos2 < scramble.length()-1 ? scramble.substring(pos2+1) : "";
					scramble = front + StringUtils.reverse(middle) + back;
					log.debug(line + " -> " + scramble);
				} else {
					log.warn("Don't know how to parse reverse '" + line + "'");
				}
			} else if (line.startsWith("move")) { // this can be inverted by reversing the role of from and to
				Matcher m = MOVE.matcher(line);
				if (m.find()) {
					int pos1 = Integer.parseInt(m.group(1));
					int pos2 = Integer.parseInt(m.group(2));
					if (unscramble) { // swap pos1 and pos2
						pos1 = pos1 + pos2;
						pos2 = pos1 - pos2;
						pos1 = pos1 - pos2;
					}
					String moved = scramble.substring(pos1, pos1 + 1);
					scramble = scramble.substring(0, pos1) + scramble.substring(pos1 + 1);
					scramble = scramble.substring(0, pos2) + moved + scramble.substring(pos2);
					log.debug(line + " -> " + scramble);
				} else {
					log.warn("Don't know how to parse move '" + line + "'");
				}
			} else {
				log.warn("Don't know how to parse '" + line + "'");
			}
		}
		return scramble;
	}

	public long doPart2() {
		return -1;
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2016, 21));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1("abcdefgh", false));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart1("fbgdceah", true));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
