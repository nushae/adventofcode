package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.AssemBunnyInterpreter;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;

import static com.nushae.aoc.util.IOUtil.aocPath;

@Log4j2
public class Exercise12 {

	AssemBunnyInterpreter hopkins;

	public Exercise12() {
		hopkins = new AssemBunnyInterpreter();
	}

	public Exercise12(Path path) {
		this();
		hopkins.loadProgramFromFile(path);
	}

	public long doPart1() {
		return hopkins.runProgram(0, 0, 0, 0);
	}

	public long doPart2() {
		return hopkins.runProgram(0, 0, 1, 0);
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2016, 12));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
