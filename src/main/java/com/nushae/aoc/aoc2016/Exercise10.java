package com.nushae.aoc.aoc2016;

import com.nushae.aoc.aoc2016.domain.*;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise10 {

	private static final Pattern VALUE_TO_BOT = Pattern.compile("value (\\d+) goes to bot (\\d+)");
	private static final Pattern BOT_COMPARE = Pattern.compile("bot (\\d+) gives low to (output|bot) (\\d+) and high to (output|bot) (\\d+)");

	private static final DestBin UNITY = new DestBin(-1);
	static {
		UNITY.addValue(1);
	}

	ArrayList<String> lines;
	Map<Integer, InputBin> allInputs;
	Map<Integer, Bot> allBots;
	Map<Integer, OutputBin> allOutputs;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		allBots = new HashMap<>();
		allInputs = new HashMap<>();
		allOutputs = new HashMap<>();
		for (String line : lines) {
			if (line.startsWith("val")) { // hook up input to bot
				Matcher m = VALUE_TO_BOT.matcher(line);
				if (m.find()) {
					int value = Integer.parseInt(m.group(1));
					int botNum = Integer.parseInt(m.group(2));
					Bot receiver = allBots.getOrDefault(botNum, new Bot(botNum));
					InputBin from = new SourceBin(value); // we assume no source occurs twice
					from.setLowReceiver(receiver);
					from.setHighReceiver(receiver);
					receiver.addInput(from);
					allBots.put(botNum, receiver);
					allInputs.put(value, from);
				}
			} else if ( line.startsWith("bot")) { // hook up bot to its two receivers
				Matcher m = BOT_COMPARE.matcher(line);
				if (m.find()) {
					int botNum = Integer.parseInt(m.group(1));
					boolean lowToOutput = "output".equals(m.group(2));
					int lowTarget = Integer.parseInt(m.group(3));
					boolean highToOutput = "output".equals(m.group(4));
					int highTarget = Integer.parseInt(m.group(5));
					Bot decider = allBots.getOrDefault(botNum, new Bot(botNum));
					if (lowToOutput) {
						OutputBin destination = new DestBin(lowTarget);
						destination.addInput(decider);
						decider.setLowReceiver(destination);
						allOutputs.put(lowTarget, destination);
					} else {
						Bot lowReceiver = allBots.getOrDefault(lowTarget, new Bot(lowTarget));
						lowReceiver.addInput(decider);
						allBots.put(lowTarget, lowReceiver);
						decider.setLowReceiver(lowReceiver);
					}
					if (highToOutput) {
						OutputBin destination = new DestBin(highTarget);
						destination.addInput(decider);
						decider.setHighReceiver(destination);
						allOutputs.put(highTarget, destination);
					} else {
						Bot highReceiver = allBots.getOrDefault(highTarget, new Bot(highTarget));
						highReceiver.addInput(decider);
						allBots.put(highTarget, highReceiver);
						decider.setHighReceiver(highReceiver);
					}
					allBots.put(botNum, decider);
				}
			}
		}
	}

	public long doPart1(int lowchip, int highchip) {
		processInput();
		System.out.println("Inputs: " + allInputs.size() + " Bots: " + allBots.size() + " Outputs: " + allOutputs.size());
		for (Map.Entry<Integer, InputBin> entry : allInputs.entrySet()) {
			InputBin sb = entry.getValue();
			int chip = entry.getKey();
			if ((chip == lowchip || chip == highchip)) { // starting point found
				sb.propagate();
				Bot next = (Bot) sb.getLowReceiver();
				while (true) {
					next.propagate();
					if (next.getLowVal() == lowchip && next.getHighVal() == highchip) {
						return next.getNum();
					} else {
						if (next.getLowVal() == lowchip || next.getLowVal() == highchip) {
							next = (Bot) next.getLowReceiver();
						} else {
							next = (Bot) next.getHighReceiver();
						}
					}
				}
			}
		}
		return -1;
	}

	public long doPart2(int... chips) {
		processInput();
		long result = 1;
		for (int chip : chips) {
			result *= allOutputs.getOrDefault(chip, UNITY).getOutput();
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2016, 10));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1(17, 61));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2(0, 1, 2));
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
