package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise03 {

	private static final Pattern TRIANGLE_NUMS = Pattern.compile("(\\d+)\\s+(\\d+)\\s+(\\d+)");

	ArrayList<String> lines;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public boolean isTriangle (int a, int b, int c) {
		return a + b > c && b + c > a && a + c > b;
	}

	public long doPart1() {
		long result = 0;
		for (String line : lines) {
			Matcher m = TRIANGLE_NUMS.matcher(line);
			if (m.find()) {
				result += isTriangle(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3))) ? 1 : 0;
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		return result;
	}

	public long doPart2() {
		long result = 0;
		int[][] triangles = new int[3][3];
		int rowsRead = 0;
		for (String line : lines) {
			Matcher m = TRIANGLE_NUMS.matcher(line);
			if (m.find()) {
				for (int i = 0; i< 3; i++) {
					triangles[i][rowsRead] = Integer.parseInt(m.group(i + 1));
				}
				rowsRead++;
				if (rowsRead == 3) {
					rowsRead = 0;
					for (int i = 0; i< 3; i++) {
						result += isTriangle(triangles[i][0], triangles[i][1], triangles[i][2]) ? 1 : 0;
					}
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2016, 3));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
