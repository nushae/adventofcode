package com.nushae.aoc.aoc2016;

import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
public class Exercise06 {

	ArrayList<String> lines;

	public Exercise06() {
		lines = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public String doPart1() {
		StringBuilder result = new StringBuilder();
		int length = lines.get(0).length();
		int[][] frequencies = new int[26][length];

		for (String line : lines) {
			for (int i = 0; i < length; i++) {
				frequencies[line.charAt(i) - 'a'][i]++;
			}
		}

		for (int i = 0; i < length; i++) {
			char mostCommon = '@';
			int maxCount = -1;
			for (int c = 0; c<26; c++) {
				int count = frequencies[c][i];
				if (count > maxCount) {
					maxCount = count;
					mostCommon = (char) ('a' + c);
				}
			}
			result.append(mostCommon);
		}
		return result.toString();
	}

	public String doPart2() {
		StringBuilder result = new StringBuilder();
		int length = lines.get(0).length();
		int[][] frequencies = new int[26][length];

		for (String line : lines) {
			for (int i = 0; i < length; i++) {
				frequencies[line.charAt(i) - 'a'][i]++;
			}
		}

		for (int i = 0; i < length; i++) {
			char leastCommon = '@';
			int minCount = lines.size() + 10;
			for (int c = 0; c<26; c++) {
				int count = frequencies[c][i];
				if (count > 0  && count < minCount) {
					minCount = count;
					leastCommon = (char) ('a' + c);
				}
			}
			result.append(leastCommon);
		}
		return result.toString();
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2016, 6));

		// part 1
		long start = System.currentTimeMillis();
		System.out.println(ex.doPart1());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2
		start = System.currentTimeMillis();
		System.out.println(ex.doPart2());
		System.out.println("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
