package com.nushae.aoc.aoc2021.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * A straightforward Ordered Tree implementation for 2021.18
 */
public class SnailFishNumber {
	public SnailFishNumber left;
	public SnailFishNumber right;
	public int literalValue;

	public SnailFishNumber() {}

	public SnailFishNumber(String representation) {
		StringBuilder remainder = new StringBuilder(representation);
		SnailFishNumber tmp = parseSFN(remainder);
		if (remainder.length() > 0) {
			throw new IllegalArgumentException("Expected just a ']' but there's junk at the end: '" + remainder + "'");
		}
		if (tmp.left != null) {
			left = tmp.left;
			right = tmp.right;
		} else {
			literalValue = tmp.literalValue;
		}
	}

	private static SnailFishNumber parseSFN(StringBuilder remainder) {
		SnailFishNumber result = new SnailFishNumber();
		if (remainder.charAt(0) == '[') { // compound number
			remainder.delete(0, 1);
			result.left = parseSFN(remainder);
			if (remainder.charAt(0) == ',') {
				remainder.delete(0, 1); // the comma
			} else {
				throw new IllegalArgumentException("Expected ',' in '" + remainder + "'");
			}
			result.right = parseSFN(remainder);
			if (remainder.charAt(0) == ']') {
				remainder.delete(0, 1); // the comma
			} else {
				throw new IllegalArgumentException("Expected ']' in '" + remainder + "'");
			}
		} else if (remainder.charAt(0) >= '0' && remainder.charAt(0) <= '9') { // literal
			int val = 0;
			while(remainder.charAt(0) >= '0' && remainder.charAt(0) <= '9') {
				val = 10 * val + remainder.charAt(0) - '0';
				remainder.delete(0, 1);
			}
			result.literalValue = val;
		} else {
			throw new IllegalArgumentException("Expected ']' or digit in '" + remainder + "'");
		}
		return result;
	}

	public boolean isLiteral() {
		return left == null;
	}

	public List<SnailFishNumber> inOrder() {
		ArrayList<SnailFishNumber> result = new ArrayList<>();
		if (isLiteral()) {
			result.add(this);
		} else {
			result.addAll(left.inOrder());
			result.add(this);
			result.addAll(right.inOrder());
		}
		return result;
	}

	public SnailFishNumber add(SnailFishNumber other) {
		SnailFishNumber result = new SnailFishNumber();
		result.left = this;
		result.right = other;
		result.reduce();
		return result;
	}

	public void reduce() {
		while (true) {
			SnailFishNumber explodeNode = findExplodeNode(0);
			if (explodeNode != null) { // do explosion, this is always a compound node
				// flatten tree into in-order, then find our exploder in that list
				List<SnailFishNumber> flat = inOrder();
				int nodeIndex = 0;
				for (; nodeIndex < flat.size(); nodeIndex++) {
					if (flat.get(nodeIndex) == explodeNode) {
						break;
					}
				}
				// our explode node has two literal children which will be its left and right direct neighbour in the list, skip those.
				for (int i = nodeIndex - 2; i>=0; i--) {
					if (flat.get(i).isLiteral()) {
						flat.get(i).literalValue += explodeNode.left.literalValue; // could also use flat.get(nodeIndex-1).literalValue ;)
						break;
					}
				}
				for (int i = nodeIndex+2; i < flat.size(); i++) {
					if (flat.get(i).isLiteral()) {
						flat.get(i).literalValue += explodeNode.right.literalValue;
						break;
					}
				}
				// now replace the exploder
				explodeNode.left = null;
				explodeNode.right = null;
				explodeNode.literalValue = 0;
			} else {
				SnailFishNumber splitNode = findSplitNode();
				if (splitNode == null) { // no more replacements; done
					break;
				}
				// do split, this is always a literal
				int leftNum = splitNode.literalValue / 2;
				int rightNum = splitNode.literalValue - leftNum;
				splitNode.left = new SnailFishNumber();
				splitNode.left.literalValue = leftNum;
				splitNode.right = new SnailFishNumber();
				splitNode.right.literalValue = rightNum;
				splitNode.literalValue = -1; // not needed but eases my conscience
			}
		}
	}

	private SnailFishNumber findSplitNode() {
		if (isLiteral()) {
			if (literalValue > 9) {
				return this;
			} else {
				return null;
			}
		}
		SnailFishNumber lt = left.findSplitNode();
		if (lt != null) {
			return lt;
		}
		return right.findSplitNode();

	}

	private SnailFishNumber findExplodeNode(int depth) {
		if (isLiteral()) {
			return null;
		}
		SnailFishNumber lt = left.findExplodeNode(depth+1);
		if (lt != null) {
			return lt;
		}
		if (depth >= 4) {
			if (left.isLiteral() && right.isLiteral()) {
				return this;
			}
		}
		return right.findExplodeNode(depth+1);
	}

	public long magnitude() {
		if (isLiteral()) {
			return literalValue;
		} else {
			return 3L * left.magnitude() + 2L * right.magnitude();
		}
	}

	@Override
	public String toString() {
		if (isLiteral()) {
			return "" + literalValue;
		}
		return String.format("[%s,%s]", left.toString(), right.toString());
	}
}
