package com.nushae.aoc.aoc2021.domain;

import java.util.ArrayList;
import java.util.List;

public class Cuboid {
	public final int lowX;
	public final int highX;
	public final int lowY;
	public final int highY;
	public final int lowZ;
	public final int highZ;
	public final boolean newState;
	private List<Cuboid> holes;

	public Cuboid(int lowX, int highX, int lowY, int highY, int lowZ, int highZ, boolean newState) {
		this.lowX = lowX;
		this.highX = highX;
		this.highY = highY;
		this.highZ = highZ;
		this.lowY = lowY;
		this.lowZ = lowZ;
		this.newState = newState;
		holes = new ArrayList<>();
	}

	public long volume() {
		return (highX - lowX  + 1L) * (highY - lowY + 1L) * (highZ - lowZ + 1L) - totalVolumeHoles();
	}

	private long totalVolumeHoles() {
		long total = 0;
		for (Cuboid hole : holes) {
			total += hole.volume();
		}
		return total;
	}

	public void remove(Cuboid cuboid) {
		Cuboid effective = intersect(cuboid);
		if (effective != null) {
			for (Cuboid hole : holes) {
				hole.remove(effective);
			}
			holes.add(effective);
		}
	}

	public Cuboid intersect(Cuboid other) {
		if (highX < other.lowX || lowX > other.highX || highY < other.lowY || lowY > other.highY || highZ < other.lowZ || lowZ > other.highZ) {
			// no overlap
			return null;
		}

		return new Cuboid(
				Math.max(lowX, other.lowX), Math.min(highX, other.highX),
				Math.max(lowY, other.lowY), Math.min(highY, other.highY),
				Math.max(lowZ, other.lowZ), Math.min(highZ, other.highZ),
				other.newState // we don't care about this value for intersections tbh
		);
	}

	@Override
	public String toString() {
		return (newState ? "on [" : "off [") + lowX + "-" + highX + "," + lowY + "-" + highY + "," + lowZ + "-" + highZ + "]";
	}
}
