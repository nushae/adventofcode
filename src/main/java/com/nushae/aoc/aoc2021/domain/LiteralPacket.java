package com.nushae.aoc.aoc2021.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LiteralPacket implements Packet {
	private int type;
	private int version;
	private long value;

	public LiteralPacket(int version, int type, long value) {
		this.version = version;
		this.type = type;
		this.value = value;
	}

	@Override
	public int getVersionSum() {
		return version;
	}
	@Override
	public long evaluate() {
		return value;
	}
}