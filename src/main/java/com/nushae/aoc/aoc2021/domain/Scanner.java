package com.nushae.aoc.aoc2021.domain;

import com.nushae.aoc.domain.Coord3D;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
public class Scanner {
	private final int number;
	private Set<Coord3D> beacons;

	public Scanner(int myNumber) {
		number = myNumber;
		beacons = new HashSet<>();
	}

	public void addBeacon(long x, long y, long z) {
		beacons.add(new Coord3D(x, y, z));
	}

	public List<Set<Coord3D>> allOrientations() {
		// I'm sure there is a more intelligent way to do this using permutations or something but time is precious, life is short
		List<Set<Coord3D>> result = new ArrayList<>();
		for (int i = 0; i < 24; i++) {
			Set<Coord3D> orient = new HashSet<>();
			for (Coord3D beacon : beacons) {
				Coord3D newBeacon;
				// transform to face +/- x, +/- y, or +/- z
				switch (i / 4) {
					case 0:
						newBeacon = beacon.rotY90(1); // -z
						break;
					case 1:
						newBeacon = beacon.rotY90(3); // +z
						break;
					case 2:
						newBeacon = beacon.rotX90(3); // -y
						break;
					case 3:
						newBeacon = beacon.rotX90(1); // +y
						break;
					case 5:
						newBeacon = beacon.rotY90(2); // -x
						break;
					default:
						newBeacon = beacon; // default facing = +x
				}
				// now transform to one of 4 'up' options:
				newBeacon = newBeacon.rotZ90(i % 4);
				orient.add(newBeacon);
			}
			result.add(orient);
		}
		return result;
	}
}
