package com.nushae.aoc.aoc2021.domain;

import java.awt.*;
import java.util.Set;

// Slightly overengineered, thought I'd need bounding box to recognize basins found twice,
// but simply permanently marking the grid already takes care of this problem. Useful for visualization though.
public class Basin {
	public int size;
	public Set<Point> edge;
	public Set<Point> inside;
	public int topleftX;
	public int topleftY;
	public int botrightX;
	public int botrightY;

	public Basin(int topleftX, int topleftY, int botrightX, int botrightY, int size, Set<Point> inside, Set<Point> edge) {
		this.topleftX = topleftX;
		this.topleftY = topleftY;
		this.botrightX = botrightX;
		this.botrightY = botrightY;
		this.size = size;
		this.edge = edge;
		this.inside = inside;
	}
}
