package com.nushae.aoc.aoc2021.domain;

public interface Packet {
	int getVersionSum();
	long evaluate();
}
