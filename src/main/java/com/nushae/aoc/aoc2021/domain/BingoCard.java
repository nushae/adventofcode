package com.nushae.aoc.aoc2021.domain;

import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class BingoCard {
	int[][] origcard = new int[5][5];
	int[][] card = new int[5][5];
	long total = 0;
	List<Integer> myNums = new ArrayList<>();

	public BingoCard(List<Integer> nums) {
		myNums.addAll(nums);
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				origcard[i][j] = nums.get(5*i+j);
				card[i][j] = nums.get(5*i+j);
				total += nums.get(5*i+j);
			}
		}
	}

	public long callNum(int num) {
		if (myNums.contains(num)) {
			boolean found = false;
			int i = 0;
			int j = 0;
			for (; i < 5; i++) {
				j = 0;
				for (; j < 5; j++) {
					if (card[i][j] == num) {
						card[i][j] = -1; // mark
						total -= num;
						found = true;
						break;
					}
				}
				if (found) break;
			}
			if (found) { // now use i and j
				boolean col = true;
				boolean row = true;
				for (int k = 0; k < 5;k++) {
					col &= card[i][k] < 0;
					row &= card[k][j] < 0;
				}
				if (col || row) {
					return total * num;
				}
			}
		}
		return -1;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				sb.append(card[i][j] < 0 ? String.format("[%02d]", origcard[i][j]) : String.format(" %02d ", card[i][j]));
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
