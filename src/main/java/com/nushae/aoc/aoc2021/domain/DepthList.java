package com.nushae.aoc.aoc2021.domain;

import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.ArrayList;

// Represents an ordered tree as the inorder, with added node depths. This is reversible into a tree
// Created for 2021.18 in a simpler way. You judge if that worked...
@Log4j2
public class DepthList {
	private final ArrayList<Point> valuesAndDepths;

	public DepthList() {
		valuesAndDepths = new ArrayList<>();
	}

	public DepthList(ArrayList<Point> input) {
		valuesAndDepths = new ArrayList<>();
		valuesAndDepths.addAll(input);
	}

	public DepthList(String snailFishNumber) {
		valuesAndDepths = new ArrayList<>();
		int depth = 0;
		for (int i = 0; i < snailFishNumber.length();) {
			switch (snailFishNumber.charAt(i)) {
				case '[':
					depth++;
					i++;
					break;
				case ']':
					depth--;
					i++;
					break;
				case ',':
					i++;
					break;
				default: // '0' - '9'
					int val = snailFishNumber.charAt(i++) - '0';
					while (i < snailFishNumber.length() && snailFishNumber.charAt(i) >= '0' && snailFishNumber.charAt(i) <= '9') {
						val = 10 * val + snailFishNumber.charAt(i++) - '0';
					}
					valuesAndDepths.add(new Point(val, depth));
			}
		}
	}

	public void addValueDepth(Point p) {
		valuesAndDepths.add(p);
	}

	public Point get(int index) {
		if (index >=0 && index < valuesAndDepths.size()) {
			return valuesAndDepths.get(index);
		}
		return null;
	}

	public java.util.List<Point> contents() {
		return valuesAndDepths;
	}

	public DepthList add(DepthList right) {
		DepthList result = new DepthList();
		for (Point p : valuesAndDepths) {
			result.addValueDepth(new Point(p.x, p.y+1));
		}
		for (Point p : right.contents()) {
			result.addValueDepth(new Point(p.x, p.y+1));
		}
		return result.reduce();
	}

	public DepthList reduce() {
		log.debug("Reducing " + this);
		ArrayList<Point> result = new ArrayList<>(valuesAndDepths);
		boolean action = true;
		while (action) {
			String prefix = "";
			ArrayList<Point> result2 = new ArrayList<>();
			action = false;
			for (int i = 0; i < result.size(); i++) {
				Point p = result.get(i);
				if (!action && p.y > 4) { // explode
					prefix = "Explode [" + p.x + "," + result.get(i+1).x + "] -> ";
					int left = p.x;
					if (i > 0) {
						Point tmp = result.get(i-1);
						result2.set(result2.size()-1, new Point(tmp.x + left, tmp.y));
					}
					result2.add(new Point(0, p.y-1));
					int right = result.get(i+1).x; // always there by construction
					i++;
					if (i < result.size()-1) {
						Point tmp = result.get(i+1);
						i++;
						result2.add(new Point(tmp.x + right, tmp.y));
					}
					action = true;
				} else if (!action && p.x > 9) { // split
					prefix = "Split " + p.x + " -> ";
					int left = p.x / 2;
					int right = p.x - left;
					result2.add(new Point(left, p.y + 1));
					result2.add(new Point(right, p.y + 1));
					action = true;
				} else { // just copy
					result2.add(result.get(i));
				}
			}
			result = result2;
			if (action) {
				log.debug(prefix + new DepthList(result));
			}
		}
		return new DepthList(result);
	}

	@Override
	public String toString() {
		ArrayList<Point> nodes = new ArrayList<>(valuesAndDepths);
		return makePair(1, nodes);
	}

	private String makePair(int atDepth, ArrayList<Point> nodes) {
		if (nodes.size() > 1) {
			StringBuilder result = new StringBuilder("[");
			Point left = nodes.get(0);
			if (left == null) {
				throw new IllegalArgumentException("Out of nodes before completing parse");
			}
			if (left.y == atDepth) {
				result.append(left.x);
				nodes.remove(0);
			} else {
				result.append(makePair(atDepth + 1, nodes));
			}
			result.append(",");
			Point right = nodes.get(0);
			if (right == null) {
				throw new IllegalArgumentException("Out of nodes before completing parse");
			}
			if (right.y == atDepth) {
				result.append(right.x);
				nodes.remove(0);
			} else {
				result.append(makePair(atDepth + 1, nodes));
			}
			result.append("]");
			return result.toString();
		}
		return "";
	}

	public int magnitude() {
		ArrayList<Point> nodes = new ArrayList<>(valuesAndDepths);
		return partialMagnitude(1, nodes);
	}

	private int partialMagnitude(int atDepth, ArrayList<Point> nodes) {
		if (nodes.size() > 1) {
			int result = 0;
			Point left = nodes.get(0);
			if (left == null) {
				throw new IllegalArgumentException("Out of nodes before completing parse");
			}
			if (left.y == atDepth) {
				result += 3 * left.x;
				nodes.remove(0);
			} else {
				result += 3 * partialMagnitude(atDepth + 1, nodes);
			}
			Point right = nodes.get(0);
			if (right == null) {
				throw new IllegalArgumentException("Out of nodes before completing parse");
			}
			if (right.y == atDepth) {
				result += 2 * right.x;
				nodes.remove(0);
			} else {
				result += 2 * partialMagnitude(atDepth + 1, nodes);
			}
			return result;
		}
		return 0;
	}
}
