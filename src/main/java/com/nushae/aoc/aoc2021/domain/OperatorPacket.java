package com.nushae.aoc.aoc2021.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OperatorPacket implements Packet {
	private int type;
	private int version;
	private List<Packet> children;

	public OperatorPacket(int version, int type, List<Packet> children) {
		this.version = version;
		this.type = type;
		this.children = children;
	}

	@Override
	public int getVersionSum() {
		int result = version;
		for (Packet p : children) {
			result += p.getVersionSum();
		}
		return result;
	}

	@Override
	public long evaluate() {
		long result;
		switch (type) {
			case 0: // sum
				result = 0;
				for (Packet p : children) {
					result += p.evaluate();
				}
				return result;
			case 1: // product
				result = 1;
				for (Packet p : children) {
					result *= p.evaluate();
				}
				return result;
			case 2: // minimum
				result = Long.MAX_VALUE;
				for (Packet p : children) {
					result = Math.min(result, p.evaluate());
				}
				return result;
			case 3: // maximum
				result = Long.MIN_VALUE;
				for (Packet p : children) {
					result = Math.max(result, p.evaluate());
				}
				return result;
			case 5: // 1 > 2 -> 1, else 0
				if (children.get(0).evaluate() > children.get(1).evaluate()) {
					return 1;
				}
				return 0;
			case 6: // 1 < 2 -> 1, else 0
				if (children.get(0).evaluate() < children.get(1).evaluate()) {
					return 1;
				}
				return 0;
			case 7: // 1 == 2 -> 1, else 0
				if (children.get(0).evaluate() == children.get(1).evaluate()) {
					return 1;
				}
				return 0;
		}
		return 0;
	}
}
