package com.nushae.aoc.aoc2021.domain;

public class Universe {
	public long s1;
	public long s2;
	public int p1;
	public int p2;

	public Universe(int position1, long score1, int position2, long score2) {
		p1 = position1;
		s1 = score1;
		p2 = position2;
		s2 = score2;
	}

	@Override
	public int hashCode() {
		int hash = p1;
		hash = 11 * hash + p2;
		hash = 23 * hash + (int) s1;
		hash = 13 * hash + (int) s2;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		Universe other = (Universe) obj;
		return (p1 == other.p1 && p2 == other.p2 && s1 == other.s1 && s2 == other.s2);
	}
}
