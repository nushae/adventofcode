package com.nushae.aoc.aoc2021.domain;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Log4j2
public class AmphipodBurrow {

	private final static long[] STEPVAL = {1, 10, 100, 1000};

	private String[] rooms;
	private String hallway;

	// Amphipod burrow states look like this:
	//
	// #############       The horizontal line of dots is the hallway. The vertical stacks of dots are the rooms
	// #...........#       (left to right = A .. D). Any dot could also be an A-D meaning an amphipod of that type
	// ###.#.#.#.###       occupies that space. Exception: dots right in front of rooms (ie with 3 neighbouring dots)
	//   #.#.#.#.#         can't be occupied by amphipods and must always be empty. Rooms can be 2+ long (all rooms
	//   #########         are the same size).
	//
	// We encode burrows as a linear String by inserting every room into the hallway part as a (..) string at the spot
	// where the amphipods can't stop:
	//
	// #############       Becomes  "..(..).(.C).(.D)C(.A).B"
	// #.......C..B#
	// ###.#.#.#.###
	//   #.#C#D#A#
	//   #########
	//
	// #############       Becomes  "..(DDDD).(CCBC).(ABAB).(BABA).."
	// #...........#
	// ###D#C#A#B###
	//   #D#C#B#A#
	//   #D#B#A#C#
	//   #D#C#B#A#
	//   #########
	//
	public AmphipodBurrow(String encoded) {
		rooms = encoded.substring(3, encoded.length()-3).split("\\).\\(");
		String[] hallways = encoded.split("\\([^(]*\\)");
		StringBuilder strb = new StringBuilder();
		for (int i=0; i<rooms.length; i++) {
			strb.append(hallways[i]).append(i);
		}
		strb.append(hallways[rooms.length]);
		hallway = strb.toString();
	}

	/**
	 * Find the state(s) that can be reached from this one with a single amphipod move.
	 *
	 * Movement rules:
	 * 1) letters never move into rooms that are not their destination OR THAT STILL HAVE WRONG LETTERS.
	 * 2) letters that stop in the hallway DON'T MOVE AGAIN UNLESS IT IS INTO THEIR DESTINATION ROOM.
	 * 3) letters can't stop directly outside the room - these spaces count for moves but are not valid destinations.
	 * 4) letters can't move through other letters.
	 *
	 * Analysis of these rules:
	 * 1) only the topmost letter in a room can ever move out of it, so other letters in that room need not be considered.
	 * 2) the top letter of a room that still has wrong letters in it MUST eventually move out - either it is the wrong
	 *    letter, or it has the wrong letter below it. Conversely, if a room has no wrong letters in it, no letters need
	 *    ever be moved out of it (of course)
	 * 3) since we must eventually pack all available letters into their rooms, arriving letters might as well move as far
	 *    down as possible in a room.
	 * 4) a move directly from one room to another is only possible if that letter heads home. There is no downside to
	 *    executing it, no other path for the letter can ever have a lower cost. So generating only that move for the
	 *    letter eliminates a lot of, *at best*, equally good moves. Corollary: only move letters to hallways that can't
	 *    be moved directly to their destination room.
	 *
	 * This leads to these move generation rules which minimizes the number of moves from a state:
	 * A) for any top letter in a room that has wrong letters in it:
	 *    1) if it can move to its destination room (ie the room is reachable and has no wrong letters in it), move it
	 *       there and generate no other moves for it
	 *    2) skip that room in step B
	 * B) for any spot in the hallway:
	 *    1) if there is a letter there, try to move it into its destination room (if reachable & no wrong letters)
	 *    2) if it is an empty spot, for each room that was not already used in step 1 and has wrong letters in it, try
	 *       to move the top letter here (if reachable)
	 *
	 * @return a Map<String, Integer> of state strings, each one resulting from a different legal move, with their associated costs
	 */
	public Map<AmphipodBurrow, Long> getAdjacentNodes() {
		log.debug("Generating moves for " + this);
		Map<AmphipodBurrow, Long> result = new HashMap<>();

		// First try rooms from which we can move a letter directly home
		log.debug("  Phase 1, considering rooms for directly-home moves...");
		Set<Integer> skipRooms = new HashSet<>();
		for (char room = 'A'; room < 'A' + rooms.length; room++) {
			int roomNumber = room -'A';
			log.debug("    Room " + roomNumber);
			if (!hasOnly(rooms[roomNumber], room)) { // not all destination letters; see if we can move the top one home
				log.debug("      room has wrong letters: " + rooms[roomNumber]);
				int topSpot = StringUtils.countMatches(rooms[roomNumber], '.');
				char letterToMove = rooms[roomNumber].charAt(topSpot);
				int targetRoom = letterToMove -'A';
				if (hasOnly(rooms[targetRoom], letterToMove)) { // find out if it is reachable; all spots in hallway[targetRoom*2+1] (down)to [roomNumber*2+1] must be clear
					log.debug("      target room (" + targetRoom +") has no wrong letters in it ");
					int stepsTaken = 0;
					boolean reachable = true;
					int roomLoc1 = Math.min(targetRoom*2+2, roomNumber*2+2);
					int roomLoc2 = Math.max(targetRoom*2+2, roomNumber*2+2);
					log.debug("      (testing for reachability from position " + roomLoc1 + " to " + roomLoc2 + " in " + hallway + ")");
					for (int i = roomLoc1; reachable && i <= roomLoc2; i++) {
						stepsTaken++;
						reachable = hallway.charAt(i) == '.' || (hallway.charAt(i) >= '0' && hallway.charAt(i) <= '3');
					}
					if (reachable) { // generate move into room
						log.debug("      room is reachable, so generating move (both source and target room will be skipped in phase 2)");
						int freeSpacesHere = StringUtils.countMatches(rooms[roomNumber], '.');
						stepsTaken += freeSpacesHere;
						int freeSpacesThere = StringUtils.countMatches(rooms[targetRoom], '.');
						stepsTaken += freeSpacesThere;
						String newTargetRoomContents = StringUtils.repeat('.', freeSpacesThere - 1) + StringUtils.repeat(letterToMove, rooms[0].length() - freeSpacesThere + 1);
						String newSourceRoomContents = StringUtils.repeat('.', freeSpacesHere + 1) + rooms[roomNumber].substring(Math.min(rooms[roomNumber].length(), freeSpacesHere+1));
						String newState = hallway;
						for (int i = 0; i < 4; i++) {
							newState = newState.replaceAll("" + i, "(" + (i == roomNumber ? newSourceRoomContents : i == targetRoom ? newTargetRoomContents : rooms[i]) + ")");
						}
						log.debug("      Generated " + this + "  ->  " + newState + "  at " + STEPVAL[targetRoom] * stepsTaken);
						result.put(new AmphipodBurrow(newState), STEPVAL[targetRoom] * stepsTaken);
						skipRooms.add(roomNumber);
						skipRooms.add(targetRoom);
					} else {
						log.debug("      room is not reachable.");
					}
				} else {
					log.debug("      target room (" + targetRoom +") still has wrong letters, so can't move to it.");
				}
			} else {
				log.debug("      room has only correct letters and spaces: " + rooms[roomNumber] + " (it will be skipped in phase 2)");
				skipRooms.add(roomNumber);
			}
		}

		log.debug("  These rooms will not be considered in phase 2: " + skipRooms);
		// now try the hallway spaces, considering only rooms
		log.debug("  Phase 2, considering hallway spaces for moves from and to it...");
		for (int hallwaySpot = 0; hallwaySpot < hallway.length(); hallwaySpot++) {
			log.debug("    Spot " + hallwaySpot);
			char letterToMove = hallway.charAt(hallwaySpot);
			if (letterToMove >= 'A' && letterToMove < 'A' + rooms.length) {
				int letterNumber = letterToMove - 'A';
				log.debug("      has a letter (" + letterToMove + ")");
				if (hasOnly(rooms[letterNumber], letterToMove)) {
					log.debug("      its destination room (" + letterNumber + ") has no wrong letters");
					int targetRoom = 2 * letterNumber + 2;
					int direction = (int) Math.signum(targetRoom - hallwaySpot);
					boolean reachable = true;
					int stepsTaken = 0;
					// find out if room is reachable; all spots in hallway[hallwaySpot] through [targetRoom] must be clear
					log.debug("      (testing for reachability from position " + (hallwaySpot + direction) + (direction < 0 ? " down" : "") + " to " + targetRoom + " in " + hallway + ")");
					for (int i = hallwaySpot + direction; reachable && i != targetRoom + direction; i += direction) {
						stepsTaken++;
						reachable = hallway.charAt(i) == '.' || (hallway.charAt(i) >= '0' && hallway.charAt(i) <= '3');
					}
					if (reachable) { // generate move into room
						log.debug("      room is reachable, so generating move");
						int alreadyThere = StringUtils.countMatches(rooms[letterNumber], letterToMove);
						stepsTaken += rooms[0].length() - alreadyThere;
						String newRoomContents = StringUtils.repeat('.', rooms[0].length() - alreadyThere - 1) + StringUtils.repeat(letterToMove, alreadyThere + 1);
						String newState = hallway.substring(0, hallwaySpot) + '.' + hallway.substring(hallwaySpot + 1);
						for (int i = 0; i < 4; i++) {
							newState = newState.replaceAll("" + i, "(" + (i == letterNumber ? newRoomContents : rooms[i]) + ")");
						}
						log.debug("    Generated " + this + "  ->  " + newState + "  at " + STEPVAL[letterNumber] * stepsTaken);
						result.put(new AmphipodBurrow(newState), STEPVAL[letterNumber] * stepsTaken);
					} else {
						log.debug("      room is not reachable");
					}
				} else {
					log.debug("      its destination room (" + letterNumber + ") has wrong letters in it, so can't move there.");
				}
			} else if (letterToMove == '.') { // open spot in hallway, try to move from any room into it
				log.debug("      is empty, checking rooms for possible movement here");
				for (char room = 'A'; room < 'A' + rooms.length; room++) {
					int letterNumber = room - 'A';
					if (!skipRooms.contains(letterNumber) && !hasOnly(rooms[letterNumber], room)) { // not all destination letters; move the top one out
						log.debug("        Room " + letterNumber + " should not be skipped and has wrong letters in it");
						int sourceRoom = 2 * letterNumber + 2;
						boolean reachable = true;
						int stepsTaken = 0;
						log.debug("        (testing for reachability from position " + Math.min(sourceRoom, hallwaySpot) + " to " + (Math.max(sourceRoom, hallwaySpot)-1) + " in " + hallway + ") ");
						for (int i = Math.min(sourceRoom, hallwaySpot); reachable && i < Math.max(sourceRoom, hallwaySpot); i ++) {
							stepsTaken++;
							reachable = hallway.charAt(i) == '.' || (hallway.charAt(i) >= '0' && hallway.charAt(i) <= '3');
						}
						if (reachable) {
							log.debug("        room is reachable, generating move");
							int topSpot = StringUtils.countMatches(rooms[letterNumber], '.');
							char letter = rooms[letterNumber].charAt(topSpot);
							stepsTaken += topSpot+1;
							String newRoomContents = StringUtils.repeat('.', topSpot + 1) + rooms[letterNumber].substring(topSpot+1);
							String newState = hallway.substring(0, hallwaySpot) + letter + hallway.substring(hallwaySpot + 1);
							for (int i = 0; i < rooms.length; i++) {
								newState = newState.replaceAll("" + i, "(" + (i == letterNumber ? newRoomContents : rooms[i]) + ")");
							}
							log.debug("      Generated " + this + "  ->  " + newState + "  at " + STEPVAL[letter - 'A'] * stepsTaken);
							result.put(new AmphipodBurrow(newState), STEPVAL[letter - 'A'] * stepsTaken);
						} else {
							log.debug("        room is not reachable");
						}
					}
				}
			} else {
				log.debug("      is in front of a room (skipped)");
			}
		}
		return result;
	}

	/**
	 * Tests whether a room has only letters of the given type besides empty spaces.
	 *
	 * @param room String representation fo the spaces in the room
	 * @param letter char the letter to test for
	 * @return true iff letter is the only kind of letter that occurs in room
	 */
	public static boolean hasOnly(String room, char letter) {
		return "".equals(room.replaceAll("\\.","").replaceAll(""+letter, ""));
	}

	@Override
	public int hashCode() {
		int hash = hallway.hashCode();
		for (int i=0; i < rooms.length; i++) {
			hash = ((3 + 10 * i) % 30) * hash + rooms[i].hashCode();
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof AmphipodBurrow)) {
			return false;
		}
		AmphipodBurrow other = (AmphipodBurrow) obj;
		boolean equal = hallway.equals(other.hallway) && rooms.length == other.rooms.length;
		for (int i=0; equal && i < rooms.length; i++) {
			equal = rooms[i].equals(other.rooms[i]);
		}
		return equal;
	}

	@Override
	public String toString() {
		String state = hallway;
		for (int i = 0; i < rooms.length; i++) {
			state = state.replaceAll("" + i, "(" + rooms[i] + ")");
		}
		return state;
	}
}
