package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.GraphUtil.findShortestPath;
import static com.nushae.aoc.util.GraphUtil.weightGridToGraph;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 15, title = "Chiton", keywords = {"grid", "shortest-path"})
public class Exercise15 extends JPanel {

	public static Color[] COLORS = new Color[]{
			new Color(128, 128, 0),
			new Color(255, 255, 0),
			new Color(255, 225, 0),
			new Color(255, 200, 0),
			new Color(255, 175, 0),
			new Color(255, 150, 0),
			new Color(255, 125, 0),
			new Color(255, 100, 0),
			new Color(255, 75, 0),
			new Color(255, 0, 0),
	};

	ArrayList<String> lines;
	int[][] grid;
	int width;
	int height;
	List<? extends GraphNode<Point>> shortest;

	public Exercise15() {
		lines = new ArrayList<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		height = lines.size();
		width = lines.get(0).length();
		grid = new int[width][height];
		int y = 0;
		for (String line : lines) {
			for (int x=0; x<line.length(); x++) {
				grid[x][y] = line.charAt(x) - '0';
			}
			y++;
		}
	}

	private void processInput2() {
		height = lines.size()*5;
		width = lines.get(0).length()*5;
		grid = new int[width][height];
		int y = 0;
		for (String line : lines) {
			for (int x = 0; x < line.length(); x++) {
				grid[x][y] = line.charAt(x) - '0';
				for (int i = 0; i< 5; i++) {
					for (int j = 0; j < 5; j++) {
						if (i != 0 || j != 0) {
							grid[x+i*line.length()][y+j*lines.size()] = (grid[x][y] + i + j) % 9;
							if (grid[x+i*line.length()][y+j*lines.size()] == 0) {
								grid[x+i*line.length()][y+j*lines.size()] = 9;
							}
						}
					}
				}
			}
			y++;
		}
	}

	public long doPart1() {
		processInput();
		shortest = new ArrayList<>();
		long result = 0;
		shortest = findShortestPath(weightGridToGraph(grid), n -> n.getPayload().x == width - 1 && n.getPayload().y == height - 1);
		for (int i = 0; i < shortest.size() - 1; i++) {
			GraphNode<Point> current = shortest.get(i);
			GraphNode<Point> next = shortest.get(i+1);
			result += (current.getAdjacentNodes().getOrDefault(next, 0L));
		}
		repaint();
		return result;
	}

	public long doPart2() {
		processInput2();
		shortest = new ArrayList<>();
		long result = 0;
		shortest = findShortestPath(weightGridToGraph(grid), n -> n.getPayload().x == width - 1 && n.getPayload().y == height - 1);
		for (int i = 0; i < shortest.size() - 1; i++) {
			GraphNode<Point> current = shortest.get(i);
			GraphNode<Point> next = shortest.get(i+1);
			result += (current.getAdjacentNodes().getOrDefault(next, 0L));
		}
		repaint();
		return result;
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g) {
		int SIZE = 1000 / width;
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		Graphics2D g2 = (Graphics2D) g;
		int xO = (getWidth() - width*SIZE) / 2;
		int yO = (getHeight() - height*SIZE) / 2;

		for (int x = 0; x < width; x ++) {
			for (int y = 0; y < height; y++) {
				g2.setColor(COLORS[grid[x][y]]);
				g2.fillRect(xO + x * SIZE, yO + y * SIZE, SIZE-1, SIZE-1);
			}
		}
		g2.setColor(Color.cyan);
		for (int i = 0; i < shortest.size() - 1; i++) {
			GraphNode<Point> current = shortest.get(i);
			GraphNode<Point> next = shortest.get(i+1);
			g2.drawLine(xO + SIZE * ((BasicGraphNode<Point>) current).getLocation().x + SIZE / 2, yO + SIZE * ((BasicGraphNode<Point>) current).getLocation().y + SIZE / 2, xO + SIZE * ((BasicGraphNode<Point>) next).getLocation().x + SIZE / 2, yO + SIZE * ((BasicGraphNode<Point>) next).getLocation().y + SIZE / 2);
		}
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2021, 15));

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1 - 698 (0.563s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 3022 (3.022s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
