package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 25, title = "Sonar Sweep", keywords = {"grid", "torus", "pathing"})
public class Exercise25 {

	ArrayList<String> lines;
	char[][] grid;
	int gridWidth;
	int gridHeight;

	public Exercise25() {
		lines = new ArrayList<>();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise25(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		gridWidth = lines.get(0).length();
		gridHeight = lines.size();
		grid = new char[gridWidth][gridHeight];
		for (int row = 0; row < gridHeight; row++) {
			String line = lines.get(row);
			for (int col = 0; col < gridWidth; col++) {
				grid[col][row] = line.charAt(col);
			}
		}
	}

	public long doPart1() {
		long result = 0;
		if (log.isDebugEnabled()) {
			printGrid();
		}
		boolean someMoved = true;
		while (someMoved) {
			long moved = moveCucumbers(true);
			moved += moveCucumbers(false);
			result++;
			log.debug("After move " + (result) + ", " + moved + " have moved.");
			someMoved = moved > 0;
		}
		return result;
	}

	public long moveCucumbers(boolean eastward) {
		char cucumberType = eastward ? '>' : 'v';
		int xDir = eastward ? 1 : 0;
		int yDir = eastward ? 0 : 1;
		char[][] newGrid = new char[gridWidth+1][gridHeight+1];
		long somethingMoved = 0;
		for (int row = gridHeight - 1; row >= 0; row--) {
			for (int col = gridWidth - 1; col >= 0; col--) {
				if (grid[col][row] == cucumberType && grid[(col+xDir) % gridWidth][(row+yDir) % gridHeight] == '.') { // move
					newGrid[(col+xDir)][(row+yDir)] = grid[col][row];
					newGrid[col][row] = '.';
					somethingMoved++;
				} else {
					newGrid[col][row] = grid[col][row];
				}
			}
			if (newGrid[gridWidth][row+yDir] == cucumberType) {
				newGrid[0][row] = newGrid[gridWidth][row+yDir];
			}
		}
		for (int col = gridWidth - 1; col >= 0; col--) {
			if (newGrid[col][gridHeight] == cucumberType) {
				newGrid[col][0] = newGrid[col][gridHeight];
			}
		}
		for (int row = gridHeight - 1; row >= 0; row--) {
			for (int col = gridWidth - 1; col >= 0; col--) {
				grid[col][row] = newGrid[col][row];
			}
		}
		return somethingMoved;
	}

	public void printGrid() {
		log.debug("Grid:");
		for (int row = 0; row < gridHeight; row++) {
			StringBuilder sb = new StringBuilder();
			for (int col = 0; col < gridWidth; col++) {
				sb.append(grid[col][row]);
			}
			log.debug(sb);
		}
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2021, 25));

		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 305 (0.1 sec)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// no part 2, Merry Christmas everyone!
	}
}
