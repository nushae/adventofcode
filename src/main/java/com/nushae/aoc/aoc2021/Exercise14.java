package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 14, title = "Extended Polymerization", keywords = {"grammar", "production-rules"})
public class Exercise14 {

	private static final Pattern SUBSTITUTION_RULE = Pattern.compile("([A-Z][A-Z]) -> ([A-Z])");

	ArrayList<String> lines;
	String template;
	Map<String, String> rules;
	Map<String, BigInteger> pairs;

	public Exercise14() {
		lines = new ArrayList<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		template = lines.get(0);
		pairs = new HashMap<>();
		for (int i =0; i < template.length()-1; i++) {
			String key = template.substring(i, i+2);
			BigInteger amount = pairs.getOrDefault(key, BigInteger.ZERO);
			pairs.put(key, amount.add(BigInteger.ONE));
		}
		rules = new HashMap<>();
		for (int i = 2; i< lines.size(); i++) {
			String line = lines.get(i);
			Matcher m = SUBSTITUTION_RULE.matcher(line);
			if (m.find()) {
				rules.put(m.group(1), m.group(2));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public BigInteger applyRules(int steps) {
		processInput(); // to reset

		for (int step = 0; step < steps; step++) {
			pairs = substitutePairs(pairs);
		}

		// Now find letter counts -_-
		BigInteger[] freq = new BigInteger[26];
		for (int i = 0; i < 26; i++) {
			freq[i] = BigInteger.ZERO;
		}
		freq[template.charAt(template.length()-1)-'A'] = BigInteger.ONE;
		for (String key : pairs.keySet()) {
			freq[key.charAt(0) - 'A'] = freq[key.charAt(0) - 'A'].add(pairs.get(key));
		}

		BigInteger maxOccurs = BigInteger.ZERO;
		BigInteger minOccurs = BigInteger.valueOf(-1);
		for (int i = 0; i < 26; i++) {
			maxOccurs = maxOccurs.max(freq[i]);
			if (freq[i].compareTo(BigInteger.ZERO) > 0) {
				if (minOccurs.compareTo(BigInteger.ZERO) < 0) {
					minOccurs = freq[i];
				} else {
					minOccurs = minOccurs.min(freq[i]);
				}
			}
		}
		return maxOccurs.subtract(minOccurs);
	}

	public Map<String, BigInteger> substitutePairs(Map<String, BigInteger> input) {
		Map<String, BigInteger> result = new HashMap<>();
		for (String key : input.keySet()) {
			String appender = rules.getOrDefault(key, "");
			if ("".equals(appender)) { // no rule applies
				result.put(key, result.getOrDefault(key, BigInteger.ZERO).add(input.get(key)));
			} else { // 'substitute'
				String key1 = key.charAt(0) + appender;
				String key2 = appender + key.charAt(1);
				result.put(key1, result.getOrDefault(key1, BigInteger.ZERO).add(input.get(key)));
				result.put(key2, result.getOrDefault(key2, BigInteger.ZERO).add(input.get(key)));
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2021, 14));

		// part 1 - 2313 (0.002 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.applyRules(10));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 3711743744429 (0.002 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.applyRules(40));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
