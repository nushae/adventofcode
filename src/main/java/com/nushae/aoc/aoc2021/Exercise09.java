package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.Basin;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 9, title = "Smoke Basin", keywords = {"grid", "local-minimum", "flood-fill"})
public class Exercise09 extends JPanel {

	private static final Font FONT = new Font("Dubai", Font.BOLD, 18);
	private static final Font FONT2 = new Font("Dubai", Font.BOLD, 36);

	public static final int SIZE = 10;
	public static final Color LOWPOINT_COLOR = Color.red;
	public static final Color EDGE_COLOR = Color.green;
	public static final Color LABEL_COLOR = Color.orange;
	public static final Color TEXT_COLOR = Color.black;
	public static Color[] COLORS = new Color[]{
			LOWPOINT_COLOR,
			new Color(0, 0, 50),
			new Color(0, 0, 100),
			new Color(0, 0, 150),
			new Color(0, 0, 200),
			new Color(0, 0, 255),
			new Color(50, 50, 255),
			new Color(100, 100, 255),
			new Color(150, 150, 255),
			new Color(200, 200, 255),
			LOWPOINT_COLOR,
			new Color(0, 50, 0),
			new Color(0, 100, 0),
			new Color(0, 150, 0),
			new Color(0, 200, 0),
			new Color(0, 255, 0),
			new Color(50, 255, 50),
			new Color(100, 255, 100),
			new Color(150, 255, 150),
			new Color(200, 255, 200),
	};

	ArrayList<String> lines;
	int[][] grid;
	java.util.List<Basin> basins;
	java.util.List<Basin> topThree;
	int width;
	int height;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		width = lines.get(0).length();
		height = lines.size();
		grid = new int[lines.get(0).length()][lines.size()];
		for (int row = 0; row < lines.size(); row++) {
			String[] rowNums = lines.get(row).split("");
			for (int col = 0 ; col < rowNums.length; col++) {
				grid[col][row] = Integer.parseInt(rowNums[col]);
			}
		}
	}

	public long doPart1() {
		long result = 0;
		processInput();
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				boolean smaller = true;
				for (int dx = -1; dx < 2; dx++) {
					for (int dy = -1; dy < 2; dy++) {
						if ((dx !=0 || dy != 0) && (dx ==0 || dy == 0) && col+dx>=0 && col+dx < lines.get(0).length() && row+dy >=0 && row+dy < lines.size()) {
							smaller &= grid[col][row] < grid[col+dx][row+dy];
						}
					}
				}
				if (smaller) {
					result += 1 + grid[col][row];
				}
			}
		}
		return result;
	}

	public long doPart2() {
		basins = new ArrayList<>();
		topThree = new ArrayList<>();
		processInput();
		for (int col = 0; col < lines.get(0).length(); col++) {
			for (int row = 0; row < lines.size(); row++) {
				boolean smaller = true;
				for (int dx = -1; dx < 2; dx++) {
					for (int dy = -1; dy < 2; dy++) {
						if ((dx !=0 || dy != 0) && (dx ==0 || dy == 0) && col+dx>=0 && col+dx < lines.get(0).length() && row+dy >=0 && row+dy < lines.size()) {
							smaller &= grid[col][row] < grid[col+dx][row+dy];
						}
					}
				}
				if (smaller) { // lowpoint; floodfill
					grid[col][row] += 10; // mark as filled, yes, this will destroy state, WE DON'T CARE! HELTER SKELTER!!
					Basin basin = findBasin(col, row);
					basins.add(basin);
				}
			}
		}
		// for visual:
		topThree = basins.stream().sorted(Comparator.comparingInt(b -> b.size)).skip(basins.size()-3).collect(Collectors.toList());
		return topThree.stream().map(b -> b.size).reduce(1, Math::multiplyExact);
	}

	public Basin findBasin(int lowPointX, int lowPointY) {
		Basin start = new Basin(lowPointX, lowPointY, lowPointX, lowPointY, 1, new HashSet<>(Collections.singletonList(new Point(lowPointX, lowPointY))), new HashSet<>());
		for (int dx = -1; dx < 2; dx++) {
			for (int dy = -1; dy < 2; dy++) {
				if ((dx !=0 || dy != 0) && (dx ==0 || dy == 0) && lowPointX+dx>=0 && lowPointX+dx < lines.get(0).length() && lowPointY+dy >=0 && lowPointY+dy < lines.size()) {
					if (grid[lowPointX+dx][lowPointY+dy] < 9) {
						grid[lowPointX+dx][lowPointY+dy] += 10; // mark as filled
						Basin rest = findBasin(lowPointX+dx, lowPointY+dy);
						start.topleftX = Math.min(start.topleftX, rest.topleftX);
						start.topleftY = Math.min(start.topleftY, rest.topleftY);
						start.botrightX = Math.max(start.botrightX, rest.botrightX);
						start.botrightY = Math.max(start.botrightY, rest.botrightY);
						start.size += rest.size;
						start.inside.addAll(rest.inside);
						start.edge.addAll(rest.edge);
					} else if(grid[lowPointX+dx][lowPointY+dy] == 9) {
						start.edge.add(new Point(lowPointX+dx, lowPointY+dy));
					}
				}
			}
		}
		return start;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		Graphics2D g2 = (Graphics2D) g;
		int xO = (getWidth() - SIZE - width*SIZE)/2;
		int yO = (getHeight() - SIZE - height*SIZE)/2;

		for (int x = 0; x < width; x ++) {
			for (int y = 0; y < height; y++) {
				g2.setColor(COLORS[grid[x][y] % 10]); // remove % 10 to check whether any basins remained unprocessed
				g2.fillRect(xO + x * SIZE, yO + y * SIZE, SIZE-1, SIZE-1);
			}
		}

		StringBuilder result = new StringBuilder();
		long product = 1;
		for (Basin b : topThree) {
			if (product > 1) {
				result.append(" * ");
			}
			result.append(b.size);
			for (Point p : b.edge) {
				g2.setColor(EDGE_COLOR);
				g2.drawRect(xO + p.x * SIZE - 1, yO + p.y * SIZE - 1, SIZE, SIZE);
			}
			g2.setColor(LABEL_COLOR);
			Rectangle boundingBox = new Rectangle(xO + SIZE * b.topleftX, yO + SIZE * b.topleftY, SIZE * (b.botrightX - b.topleftX), SIZE * (b.botrightY - b.topleftY));
			drawCenteredString(g2, ""+b.size, boundingBox, FONT);

			product *= b.size;
		}
		result.append(" = ");
		result.append(product);
		g2.setColor(TEXT_COLOR);
		drawCenteredString(g2, result.toString(), new Rectangle(xO, yO + (height+1)*SIZE + SIZE/2, (width + 1) * SIZE, SIZE), FONT2);
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2021, 9));

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1 - 452 (0.012s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1263735 (0.036s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
