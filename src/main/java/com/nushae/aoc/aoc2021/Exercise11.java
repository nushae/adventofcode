package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.GraphicsUtil.drawCenteredString;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.KING_DIRS;

@Log4j2
@Aoc(year = 2021, day = 11, title = "Dumbo Octopus", keywords = {"grid", "cellular-automaton"})
public class Exercise11 extends JPanel {

	public static final int SPEED = 25;
	public static final int SIZE = 100;
	public static final Color FLASHED_COLOR = Color.black;
	public static final Color TEXT_COLOR = Color.black;
	public static Color[] COLORS = new Color[]{
			FLASHED_COLOR,
			new Color(255, 255, 0),
			new Color(255, 225, 0),
			new Color(255, 200, 0),
			new Color(255, 175, 0),
			new Color(255, 150, 0),
			new Color(255, 125, 0),
			new Color(255, 100, 0),
			new Color(255, 75, 0),
			new Color(255, 0, 0),
	};
	private static final Font FONT = new Font("Dubai", Font.BOLD, 36);

	ArrayList<String> lines;
	int[][] grid;
	long width;
	long height;
	long generation; // for display in the visual rep

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		width = lines.get(0).length();
		height = lines.size();
		grid = new int[lines.get(0).length()][lines.size()];
		for (int row = 0; row < lines.size(); row++) {
			String[] rowNums = lines.get(row).split("");
			for (int col = 0 ; col < rowNums.length; col++) {
				grid[col][row] = Integer.parseInt(rowNums[col]);
			}
		}
	}

	public long doPart1(int generations) {
		long result = 0;
		processInput();
		generation = 0;
		for (int gen = 0; gen < generations; gen++) {
			result += doGeneration();
			generation++;
		}
		return result;
	}

	public long doPart2() {
		generation = 0;
		processInput();
		long resultThisRound;
		repaint();
		if (SPEED > 0) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// NOP
			}
		}
		do {
			resultThisRound = doGeneration();
			generation++;
		} while (resultThisRound < width * height);
		return generation;
	}

	public long doGeneration() {
		long result = 0;
		// initial energy increase
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				grid[x][y]++;
			}
		}
		if (SPEED > 0) {
			repaint();
			try {
				Thread.sleep(SPEED);
			} catch (InterruptedException e) {
				// NOP
			}
		}
		// now do flashes until no flashes occur
		long flashes;
		do {
			flashes = 0;
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					if (grid[x][y] > 9) { // REFLASH
						flashes++;
						grid[x][y] = 0;
						for (Point dir : KING_DIRS) {
							int newX = x + dir.x;
							int newY = y + dir.y;
							if (0 <= newX && newX < width && 0 <= newY && newY < height) {
								if (grid[newX][newY] > 0) { // hasn't flashed yet
									grid[newX][newY]++;
								}
							}
						}
					}
				}
			}
			result += flashes;
		} while (flashes > 0);
		if (SPEED > 0) {
			repaint();
			try {
				Thread.sleep(SPEED);
			} catch (InterruptedException e) {
				// NOP
			}
		}
		return result;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		Graphics2D g2 = (Graphics2D) g;
		int xO = (int) ((getWidth() - width*SIZE) / 2);
		int yO = (int) ((getHeight() - height*SIZE) / 2);

		for (int x = 0; x < width; x ++) {
			for (int y = 0; y < height; y++) {
				g2.setColor(COLORS[grid[x][y] % 10]);
				g2.fillRect(xO + x * SIZE, yO + y * SIZE, SIZE-1, SIZE-1);
			}
		}

		g2.setColor(TEXT_COLOR);
		drawCenteredString(g2, "" + generation, new Rectangle(xO, yO + (int) ((height+1)*SIZE + SIZE/2), (int) ((width + 1) * SIZE), SIZE), FONT);
	}

	public static void createAndShowGUI(JPanel panel){
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension(1200, 1200));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2021, 11));

		SwingUtilities.invokeLater(() -> createAndShowGUI(ex));

		// part 1 - 1599 (0.3 sec @ SPEED == 0; 5.4 sec @ SPEED == 25)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(100));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 418 (0.002 sec @ SPEED == 0; 23.2 sec @ SPEED == 25)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
