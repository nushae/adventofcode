package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.Cuboid;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Coord3D;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 22, title = "Reactor Reboot", keywords = {"xor", "boundingbox-intersection", "constructive-solid-geometry"})
public class Exercise22 {

	private static final Pattern CUBOID = Pattern.compile("(on|off) x=(-?\\d+)\\.\\.(-?\\d+),y=(-?\\d+)\\.\\.(-?\\d+),z=(-?\\d+)\\.\\.(-?\\d+)");

	ArrayList<String> lines;
	Map<Coord3D, Boolean> engine;
	List<Cuboid> cuboids;

	public Exercise22() {
		lines = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		cuboids = new ArrayList<>();
		for (String line : lines) {
			Matcher m = CUBOID.matcher(line);
			if (m.find()) {
				boolean newState = "on".equals(m.group(1));
				int lowX = Math.min(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)));
				int highX = Math.max(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)));
				int lowY = Math.min(Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
				int highY = Math.max(Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)));
				int lowZ = Math.min(Integer.parseInt(m.group(6)), Integer.parseInt(m.group(7)));
				int highZ = Math.max(Integer.parseInt(m.group(6)), Integer.parseInt(m.group(7)));
				cuboids.add(new Cuboid(lowX, highX, lowY, highY, lowZ, highZ, newState));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long reboot() {
		engine = new HashMap<>();
		for (Cuboid c : cuboids) {
			for (int x = c.lowX; x <= c.highX; x++) {
				if (x >= -50 && x <= 50) {
					for (int y = c.lowY; y <= c.highY; y++) {
						if (y >= -50 && y <= 50) {
							for (int z = c.lowZ; z <= c.highZ; z++) {
								if (z >= -50 && z <= 50) {
									Coord3D coord = new Coord3D(x, y, z);
									if (c.newState) {
										engine.put(coord, true);
									} else {
										engine.remove(coord);
									}
								}
							}
						}
					}
				}
			}
		}
		return engine.size();
	}

	public long doPart1() {
		processInput();
		return reboot();
	}

	// variant a, 'effective volume'
	public long doPart2a() {
		processInput();
		long engineOn = 0;
		for (int c = 0; c < cuboids.size(); c++) {
			if (cuboids.get(c).newState) {
				engineOn += effectiveVolume(cuboids.get(c), c+1);
			}
		}
		return engineOn;
	}

	// variant b, constructive solid geometry
	public long doPart2b() {
		processInput();
		List<Cuboid> processed = new ArrayList<>();
		for (Cuboid c : cuboids) {
			// make copy to avoid side effects
			Cuboid cuboid = new Cuboid(c.lowX, c.highX, c.lowY, c.highY, c.lowZ, c.highZ, c.newState);
			for (Cuboid proc : processed) {
				proc.remove(cuboid);
			}
			if (cuboid.newState) {
				processed.add(cuboid);
			}
		}
		long engineOn = 0;
		for (Cuboid c : processed) {
			engineOn += c.volume();
		}
		return engineOn;
	}

	public long effectiveVolume(Cuboid c, int remainder) {
		long result = c.volume();

		for (int r = remainder; r < cuboids.size(); r++) {
			Cuboid intersect = c.intersect(cuboids.get(r));
			if (intersect != null) {
				result -= effectiveVolume(intersect, r+1);
			}
		}

		return result;
	}

	public static void main(String[] args) {
		Exercise22 ex = new Exercise22(aocPath(2021, 22));

		// part 1 - 609563 (0.644s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2, variant a (used for submission) - 1234650223944734 (0.056s)
		start = System.currentTimeMillis();
		log.info("Part 2 (effective volume):");
		log.info(ex.doPart2a());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2, variant b (classic CSG) - 1234650223944734 (0.009s)
		start = System.currentTimeMillis();
		log.info("Part 2 (CSG):");
		log.info(ex.doPart2b());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
