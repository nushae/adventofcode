package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 3, title = "Binary Diagnostic", keywords = {"binary", "bit-counting"})
public class Exercise03 {

	ArrayList<String> lines;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		StringBuilder gammaRate = new StringBuilder();
		StringBuilder epsilonRate = new StringBuilder();
		for (int bit = 0; bit < lines.get(0).length(); bit++) {
			long ones = 0;
			for (String line : lines) {
				ones += line.charAt(bit) - '0';
			}
			if (ones > lines.size() - ones) {
				gammaRate.append("1");
				epsilonRate.append("0");
			} else {
				gammaRate.append("0");
				epsilonRate.append("1");
			}
		}
		return Long.parseLong(gammaRate.toString(),2) * Long.parseLong(epsilonRate.toString(), 2);
	}

	public long doPart2() {
		ArrayList<String> oxyList = new ArrayList<>(lines);
		ArrayList<String> co2List = new ArrayList<>(lines);
		int bit = 0;
		while (oxyList.size()>1) {
			oxyList = findOxy(oxyList, bit++);
		}
		bit = 0;
		while (co2List.size()>1) {
			co2List = findCO2(co2List, bit++);
		}
		return Long.parseLong(oxyList.get(0),2) * Long.parseLong(co2List.get(0), 2);
	}

	public ArrayList<String> findOxy(ArrayList<String> sourcelist, int bitFromLeft) {
		long ones = 0;
		ArrayList<String> newlist = new ArrayList<>();
		for (String line : sourcelist) {
			ones += line.charAt(bitFromLeft) - '0';
		}
		char toCount;
		if (ones >= sourcelist.size()-ones) { // 1 most common
			toCount = '1';
		} else {
			toCount = '0';
		}
		for (String line : sourcelist) {
			if (line.charAt(bitFromLeft) == toCount) {
				newlist.add(line);
			}
		}
		return newlist;
	}

	public ArrayList<String> findCO2(ArrayList<String> sourcelist, int bitFromLeft) {
		long ones = 0;
		ArrayList<String> newlist = new ArrayList<>();
		for (String line : sourcelist) {
			ones += line.charAt(bitFromLeft) - '0';
		}
		char toCount;
		if (ones < sourcelist.size()-ones) { // 1 least common
			toCount = '1';
		} else {
			toCount = '0';
		}
		for (String line : sourcelist) {
			if (line.charAt(bitFromLeft) == toCount) {
				newlist.add(line);
			}
		}
		return newlist;
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2021, 3));

		// part 1 - 4006064 (<0.001s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 5941884 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
