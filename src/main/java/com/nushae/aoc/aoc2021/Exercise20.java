package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 20, title = "Trench Map", keywords = {"grid", "shader", "cellular-automaton"})
public class Exercise20 {

	ArrayList<String> lines;
	String algo;
	public boolean[][] grid;
	boolean emulateFlip; // emulate bit-flipping of the infinite plane, as happens in the main input, but not the example (!)
	boolean outsideval; // value of the infinite plane's bits, when employing said flipping

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		algo = lines.get(0);
		outsideval = false;
		emulateFlip = (algo.charAt(0) == '#' && algo.charAt(511) == '.');
		// note that algo's with only one of these two effects would result in a VERY hard to emulate process, so are highly unlikely to occur.
		// if you happen to have such an input that actually came from the aoc site, I'd be very interested in seeing it.
		log.debug("'.........' -> '" + algo.charAt(0) + "', and '#########' -> '" + algo.charAt(511) + "', so " + (emulateFlip ? "" : "not ") + "emulating infinite plane inversion.");
		int width = lines.get(2).length();
		int height = lines.size()-2;
		grid = new boolean[width+2][height+2];
		for (int y=1; y < lines.size()-1; y++) {
			String line = lines.get(y+1);
			for (int x=1; x < line.length()+1; x++) {
				grid[x][y] = line.charAt(x-1) == '#';
			}
		}
	}

	public long doPart1() {
		processInput(); // to reset
		grid = enhanceImage(grid);
		grid = enhanceImage(grid);
		if (log.isDebugEnabled()) {
			show(grid);
		}
		long lit = 0;
		for (int i = 0; i < grid[0].length; i++) {
			for (int j = 0; j < grid.length; j++) {
				lit += grid[i][j] ? 1 : 0;
			}
		}
		return lit;
	}

	public long doPart2() {
		processInput();
		for (int i=0; i< 50; i++) {
			grid = enhanceImage(grid);
		}
		long lit = 0;
		for (int i = 0; i < grid[0].length; i++) {
			for (int j = 0; j < grid.length; j++) {
				lit += grid[i][j] ? 1 : 0;
			}
		}
		if (log.isDebugEnabled()) {
			show(grid);
		}
		return lit;
	}

	public boolean[][] enhanceImage(boolean[][] input) {
		int W = input.length+2;
		int H = input[0].length+2;
		boolean[][] result = new boolean[W][H];
		for (int x = 0; x < W; x++) {
			for (int y = 0; y < H; y++) {
				int index = getIndex(input, x-1, y-1);
				result[x][y] = algo.charAt(index) == '#';
			}
		}
		if (emulateFlip) {
			outsideval = !outsideval;
		}
		return result;
	}

	public int getIndex(boolean[][] input, int x, int y) {
		int W = input.length;
		int H = input[0].length;
		int index = 0;
		for (int j = 0; j <= 2; j++) {
			for (int i = 0; i <= 2; i++) {
				int testX = x + i-1;
				int testY = y + j-1;
				if (testX < 0 || testX >= W || testY < 0 || testY >= H) { // assume outsideval
					index = 2 * index + (outsideval ? 1 : 0);
				} else {
					index = 2 * index + (grid[testX][testY] ? 1 : 0);
				}
			}
		}
		return index;
	}

	public void show(boolean[][] input) {
		for (int j = 0; j < input.length; j++) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < input[0].length; i++) {
				sb.append(input[i][j] ? '#' : '.');
			}
			log.debug(sb);
		}
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2021, 20));

		// part 1 - 5786 (0.003s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 16757 (0.058s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
