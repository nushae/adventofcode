package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.MathUtil.triangle;

@Log4j2
@Aoc(year = 2021, day = 17, title = "Trick Shot", keywords = {"ballistics", "discrete-values", "triangle-numbers", "enumeration"})
public class Exercise17 {

	private static final Pattern TARGET_AREA = Pattern.compile("target area: x=(-?\\d+)\\.\\.(-?\\d+), y=(-?\\d+)\\.\\.(-?\\d+)");

	ArrayList<String> lines;
	Set<Point> velocities;
	Point topLeft;
	Point botRight;
	Point velocity;
	Point location;

	public Exercise17() {
		lines = new ArrayList<>();
	}

	public Exercise17(String data) {
		this();
		Matcher m = TARGET_AREA.matcher(data);
		if (m.find()) {
			topLeft = new Point(Math.min(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))), Math.max(Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4))));
			botRight = new Point(Math.max(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))), Math.min(Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4))));
		} else {
			System.out.println("Don't know how to parse " + data);
			System.exit(0);
		}
	}

	public long doPart1() {
		// The highest y-pos for starting vy Y is Y + Y-1 + Y-2 + ... + 1 = triangle(Y)
		// If Y is the starting vy, then eventually we will return to y=0 at the end of a tick where vy was -Y.
		// After the next tick, the y-position is -Y-1. So we have the highest possible value for Y when
		// -Y-1 == botRight.y, so Y = -botRight.y-1 (since botRight.y < 0)
		return triangle(-botRight.y-1);
	}

	public long doPart2() {
		// Part two must be brute forced to some extent, but we can limit the min and max values for our speeds:
		// From part 1 we know the starting vy must range from botRight.y to -botRight.y-1 (both inclusive)
		// the starting x-velocity must similarly range from N (where triangle(N) = N*(N+1)/2 = topLeft.x)
		// to botRight.x (for a 1 tick shot that lands on the right edge).
		// We derive:
		// 0.5*N^2 + 0.5*N - topLeft.x = 0, so N = (-0.5 +- sqrt(0.5^2 + 2*topLeft.x))/1, with N>=0,
		// so N -0.5 + sqrt(0.25 + 2*topLeft.x). We round down to be sure.
		// For all vx, vy within these bounds it takes at most 2*(maxVY)+1 ticks to reach the target, so run that many steps:
		velocities = new HashSet<>();
		Point initialVelocity;
		for (int velX = (int) Math.floor(-0.5 + Math.sqrt(0.25 + 2 * topLeft.x)); velX <= botRight.x; velX++) {
			for (int velY = botRight.y; velY <= -botRight.y-1; velY++) {
				initialVelocity = new Point(velX, velY);
				location = new Point(0, 0);
				velocity = new Point(initialVelocity.x, initialVelocity.y);
				int maxY = 0;
				boolean targetHit = false;
				for (int i = 0; i <= 2*(-botRight.y-1)+1; i++) {
					location.x += velocity.x;
					location.y += velocity.y;
					maxY = Math.max(maxY, location.y);
					if (location.x >= topLeft.x && location.x <= botRight.x && location.y <= topLeft.y && location.y >= botRight.y) {
						targetHit = true;
						break;
						// in target area!
					} else if (location.x > botRight.x || location.y < botRight.y) {
						break;
						// overshot
					}
					// adjust velocity:
					velocity.x = Math.max(velocity.x - 1, 0);
					velocity.y--;
				}
				if (targetHit) {
					velocities.add(initialVelocity);
				}
			}
		}
		return velocities.size();
	}

	public static void main(String[] args) {
		Exercise17 ex = new Exercise17("target area: x=287..309, y=-76..-48");

		// part 1 - 2850 (0.25 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1117 (0.008 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
