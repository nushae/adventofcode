package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.intersection;

@Log4j2
@Aoc(year = 2021, day = 8, title = "Seven Segment Search", keywords = {"seven-segment", "boolean-logic", "set-intersection"})
public class Exercise08 {

	private static final Pattern SEGMENTS = Pattern.compile("([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+\\|\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)\\s+([a-g]+)");

	private static final Map<String, Integer> DIGITS = Map.of(
		"abcefg", 0,
		"cf", 1,
		"acdeg", 2,
		"acdfg", 3,
		"bcdf", 4,
		"abdfg", 5,
		"abdefg", 6,
		"acf", 7,
		"abcdefg", 8,
		"abcdfg", 9
	);

	/* Analysis
	 *    0:      1:      2:      3:      4:
	 *    aaaa    ....    aaaa    aaaa    ....
	 *   b    c  .    c  .    c  .    c  b    c
	 *   b    c  .    c  .    c  .    c  b    c
	 *    ....    ....    dddd    dddd    dddd
	 *   e    f  .    f  e    .  .    f  .    f
	 *   e    f  .    f  e    .  .    f  .    f
	 *    gggg    ....    gggg    gggg    ....
	 *
	 *    5:      6:      7:      8:      9:
	 *    aaaa    aaaa    aaaa    aaaa    aaaa
	 *   b    .  b    .  .    c  b    c  b    c
	 *   b    .  b    .  .    c  b    c  b    c
	 *    dddd    dddd    ....    dddd    dddd
	 *   .    f  e    f  .    f  e    f  .    f
	 *   .    f  e    f  .    f  e    f  .    f
	 *    gggg    gggg    ....    gggg    gggg
	 *
	 * Segment to digit(s):
	 * a occurs in: 0,2,3,5,6,7,8,9   = 8 digits
	 * b occurs in: 0,4,5,6,8,9       = 6 digits
	 * c occurs in: 0,1,2,3,4,7,8,9   = 8 digits
	 * d occurs in: 2,3,4,5,6,8,9     = 7 digits
	 * e occurs in: 0,2,6,8           = 4 digits
	 * f occurs in: 0,1,3,4,5,6,7,8,9 = 9 digits
	 * g occurs in: 0,2,3,5,6,8,9     = 7 digits
	 *
	 * This means b, e, and f are uniquely identified by number of sequences they occur in, and a/c and d/g must be differentiated
	 *
	 * e is uniquely identified as the 4 occurrence
	 * b is uniquely identified as the 6 occurrence
	 * f is uniquely identified as the 9 occurrence
	 * d/g both occur 7 times, but d occurs in the sequence of length 4 (digit 4) and g does not
	 * a/c both occur 8 times, but c occurs in the sequence of length 2 ( digit 1) and a does not
	 */
	ArrayList<String> lines;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long accum = 0;
		for (String line : lines) {
			for (String word : line.split("\\s+\\|\\s+")[1].trim().split("\\s+")) {
				if (word.length() >= 2 && word.length() <= 4 || word.length() == 7) {
					accum++;
				}
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		for (String line : lines) {
			Matcher m = SEGMENTS.matcher(line);
			if (m.find()) {
				char[] map = new char[7];
				int[] freq = new int[7];
				String one = "";
				String four = ""; // no need to remember the others
				// 1) determine occurrences of each letter a-g
				for (int i = 1; i < 11; i++) {
					String word = m.group(i);
					if (word.length() == 2) {
						one = word;
					} else if (word.length() == 4) {
						four = word;
					}
					for (int c = 0; c < word.length(); c++) {
						freq[word.charAt(c)-'a']++;
					}
				}
				// 2) use occurrences and one/four to map to real letter
				for (char c = 'a'; c < 'h'; c++) {
					int i = c - 'a';
					if (freq[i] == 4) {
						map[i] = 'e';
					} else if (freq[i] == 6) {
						map[i] = 'b';
					} else if (freq[i] == 7) {
						if (four.contains(Character.toString(c))) {
							map[i]= 'd';
						} else {
							map[i]= 'g';
						}
					} else if (freq[i] == 8) {
						if (one.contains(Character.toString(c))) {
							map[i]= 'c';
						} else {
							map[i]= 'a';
						}
					} else if (freq[i] == 9) {
						map[i]= 'f';
					}
				}
				// 3) now decode numbers
				long number = 0;
				for (int i = 11; i < 15; i++) {
					number = number * 10 + DIGITS.get(decode(map, m.group(i)));
				}
				accum += number;
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		return accum;
	}

	public String decode(char[] map, String toDecode) {
		StringBuilder result = new StringBuilder();
		for (int c = 0; c < toDecode.length(); c++) {
			char newChar = map[toDecode.charAt(c)-'a'];
			result.append(newChar);
		}
		return intersection("abcdefg", result.toString());
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2021, 8));

		// part 1 - 470 (0.006 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 989396 (0.023 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
