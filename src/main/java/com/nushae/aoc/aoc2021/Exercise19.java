package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.Scanner;
import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Coord3D;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 19, title = "Beacon Scanner", keywords = {"3d-jigsaw", "manhattan-distance"})
public class Exercise19 {

	private static final Pattern SCAN_NUM = Pattern.compile("---[^0-9]*(\\d+) ---");
	private static final Pattern COORDS = Pattern.compile("(-?\\d+),(-?\\d+),(-?\\d+)");

	ArrayList<String> lines;
	ArrayList<Scanner> scanners;
	ArrayList<Coord3D> syncedScanners; // for part 2

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		Scanner scanner = null;
		scanners = new ArrayList<>();
		syncedScanners = new ArrayList<>();
		for (String line : lines) {
			if (line.startsWith("---")) {
				Matcher m = SCAN_NUM.matcher(line);
				if (m.find()) {
					scanner = new Scanner(Integer.parseInt(m.group(1)));
				} else {
					log.warn("Don't know how to parse " + line);
				}
			} else if (!"".equals(line)) {
				Matcher m = COORDS.matcher(line);
				if (m.find()) {
					if (null != scanner) {
						scanner.addBeacon(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)));
					} else {
						log.warn("No scanner to add " + line + " to.");
					}
				} else {
					log.warn("Don't know how to parse " + line);
				}
			} else {
				scanners.add(scanner);
				scanner = null;
			}
		}
		if (null != scanner) {
			scanners.add(scanner);
		}
	}

	public long doPart1() {
		Scanner map = new Scanner(42); // create map in here
		for (Coord3D beacon : scanners.get(0).getBeacons()) {
			map.addBeacon(beacon.x, beacon.y, beacon.z);
		}
		syncedScanners.add(new Coord3D(0 , 0, 0));
		scanners.remove(0);
		boolean haveAdded = true;
		// Brute force loops go brrrr :/
		while (haveAdded && scanners.size()>0) {
			haveAdded = false;
			Iterator<Scanner> itr = scanners.iterator();
			while (itr.hasNext()) {
				Scanner s = itr.next();
				for (Set<Coord3D> orientation : s.allOrientations()) {
					Coord3D relative = hasOverlapWithDisplacement(map.getBeacons(), orientation);
					if (null != relative) {
						for (Coord3D beacon : orientation) {
							map.addBeacon(beacon.x - relative.x, beacon.y - relative.y, beacon.z - relative.z);
						}
						haveAdded = true;
						syncedScanners.add(relative);
						itr.remove();
						break;
					}
				}
			}
		}
		return map.getBeacons().size();
	}

	public long doPart2() {
		// NOTE: depends on part 1 having been run!
		long manhattan = 0;
		for (int i = 0; i < syncedScanners.size()-1; i++) {
			for (int j = i+1; j < syncedScanners.size(); j++) {
				manhattan = Math.max(manhattan, syncedScanners.get(i).manhattan(syncedScanners.get(j)));
			}
		}
		return manhattan;
	}

	public static Coord3D hasOverlapWithDisplacement(Set<Coord3D> left, Set<Coord3D> right) {
		for (Coord3D leftBeacon : left) {
			for (Coord3D rightBeacon: right) {
				Coord3D displacement = new Coord3D(rightBeacon.x - leftBeacon.x, rightBeacon.y - leftBeacon.y, rightBeacon.z - leftBeacon.z);
				int overlap = 0;
				for (Coord3D tester : right) {
					if (left.contains(new Coord3D(tester.x-displacement.x, tester.y -displacement.y, tester.z - displacement.z))) {
						overlap++;
						if (overlap >= 12) {
							return displacement;
						}
					}
				}
			}
		}
		return null;
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2021, 19));

		// Input processing (0.006s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 365 (1.391s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 11060 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
