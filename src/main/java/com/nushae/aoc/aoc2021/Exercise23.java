package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.AmphipodBurrow;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.util.*;

@Log4j2
@Aoc(year = 2021, day = 23, title = "Amphipod", keywords = {"arrangement-puzzle", "hanoi"})
public class Exercise23 {

	// States are encoded as a String like eg. ".D(.D).(.C)C(.B).(BA).A"
	// The four '(XY)' parts represent both an (ordered) room of N spaces and the hallway spot in font of it where
	// you can't stop; the dots and letters are the (un)occupied spaces in the hallways and rooms

	static long bestVal = -1; // negative indicates no solution found

	public Exercise23() {
	}

	public long doPart1(String startStateEncoded, String targetStateEncoded) {
		bestVal = -1;
		// printPath(findShortestPath(startStateEncoded, targetStateEncoded));
		findShortestPath(new AmphipodBurrow(startStateEncoded), new AmphipodBurrow(targetStateEncoded));
		return bestVal;
	}

	public long doPart2(String startStateEncoded, String targetStateEncoded) {
		bestVal = -1;
		// printPath(findShortestPath(new AmphipodBurrow(startStateEncoded), new AmphipodBurrow(targetStateEncoded)));
		findShortestPath(new AmphipodBurrow(startStateEncoded), new AmphipodBurrow(targetStateEncoded));
		return bestVal;
	}

	/**
	 * Dijkstra's algorithm, rewritten for a possibly infinite graph. Rather than a physical GraphNode with a map of
	 * adjacent nodes, we now use a method that generates neighbours (= possible valid moves)
	 *
	 * @param startState String encoded room to use as starting point
	 * @param targetState String encoded room that we need to reach
	 * @return List<String> the move sequence with the lowest total cost. As a side effect,
	 *     that cost is also stored in the global variable bestVal.
	 */
	public static List<AmphipodBurrow> findShortestPath(AmphipodBurrow startState, AmphipodBurrow targetState) {
		List<AmphipodBurrow> result = new ArrayList<>();
		HashSet<AmphipodBurrow> visited = new HashSet<>();
		Map<AmphipodBurrow, Long> bestDistances = new HashMap<>();
		Map<AmphipodBurrow, AmphipodBurrow> prev = new HashMap<>();
		Set<AmphipodBurrow> queue = new HashSet<>();
		bestDistances.put(startState, 0L);
		queue.add(startState);
		while (queue.size() > 0) {
			AmphipodBurrow minState = null;
			long minDist = Long.MAX_VALUE;
			for (AmphipodBurrow n : queue) {
				if (minDist > bestDistances.getOrDefault(n, Long.MAX_VALUE)) {
					minDist = bestDistances.getOrDefault(n, Long.MAX_VALUE);
					minState = n;
				}
			}
			visited.add(minState);
			queue.remove(minState);
			if (minState != null && !targetState.equals(minState)) {
				for (Map.Entry<AmphipodBurrow, Long> move : minState.getAdjacentNodes().entrySet()) {
					AmphipodBurrow neighbour = move.getKey();
					if (!visited.contains(neighbour)) {
						long val = move.getValue();
						queue.add(neighbour);
						long tentative = minDist + val;
						if (tentative < bestDistances.getOrDefault(neighbour, Long.MAX_VALUE)) {
							bestDistances.put(neighbour, tentative);
							prev.put(neighbour, minState);
						}
					}
				}
			} else {
				bestVal = minDist;
				result.add(minState);
				while (prev.containsKey(minState)) {
					minState = prev.get(minState);
					result.add(0, minState);
				}
				return result;
			}
		}
		return result;
	}

	public void printPath(List<AmphipodBurrow> moves) {
		for (int m = 0; m < moves.size()-1; m++) {
			System.out.println(moves.get(m) + " -> " + moves.get(m + 1));
		}
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23();

		// part 1 - 19160 (18.378s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1("..(DD).(CC).(AB).(BA)..", "..(AA).(BB).(CC).(DD).."));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 47232 (1m15.5s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2("..(DDDD).(CCBC).(ABAB).(BACA)..", "..(AAAA).(BBBB).(CCCC).(DDDD).."));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
