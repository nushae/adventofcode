package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 10, title = "Syntax Scoring", keywords = {"parsing", "brackets", "pushdown-automaton"})
public class Exercise10 {

	private static final Map<Character, Long> BRACKET_SCORES = Map.of(
		')', 3L,
		']', 57L,
		'}', 1197L,
		'>', 25137L
	);

	private static final Map<Character, Long> AUTO_SCORES = Map.of(
		')', 1L,
		']', 2L,
		'}', 3L,
		'>', 4L
	);

	ArrayList<String> lines;
	ArrayList<Character> brackstack;
	ArrayList<Long> autoscores;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		return processInput();
	}

	public long doPart2() {
		processInput();
		Collections.sort(autoscores);
		return autoscores.get(autoscores.size()/2);
	}

	public long processInput() {
		long score = 0;
		autoscores = new ArrayList<>();
		for (String line : lines) {
			brackstack = new ArrayList<>();
			for (int c = 0; c < line.length(); c++) {
				char bracket = line.charAt(c);
				if (bracket == '(') {
					brackstack.add(')');
				} else if (bracket == '[') {
					brackstack.add(']');
				} else if (bracket == '{') {
					brackstack.add('}');
				} else if (bracket == '<') {
					brackstack.add('>');
				} else { // closing bracket
					if (brackstack.remove(brackstack.size()-1) != bracket) {
						score += BRACKET_SCORES.getOrDefault(bracket, 0L);
						brackstack = new ArrayList<>();
						break;
					}
				}
			}
			if (brackstack.size() > 0) { // unfinished line, 'autocomplete'
				autoscores.add(autocomplete(brackstack));
			}
		}
		return score;
	}

	public long autocomplete(ArrayList<Character> brackstack) {
		long auto = 0;
		for (int i = 0; i < brackstack.size() ; i++) {
			auto = auto * 5 + AUTO_SCORES.getOrDefault(brackstack.get(brackstack.size()-1-i), 0L);
		}
		return auto;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2021, 10));

		// part 1 - 341823 (0.001 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2801302861 (0.001 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
