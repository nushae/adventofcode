package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.DepthList;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 18, title = "Snailfish", keywords = {"alternative-approach", "incorrect-solution", "tree-manipulation", "nested-numbers", "esoteric-math"})
public class Exercise18alt {

	ArrayList<String> lines;
	ArrayList<DepthList> snailfishNumbers;

	public Exercise18alt() {
		lines = new ArrayList<>();
	}

	public Exercise18alt(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18alt(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		snailfishNumbers = new ArrayList<>();
		for (String line : lines) {
			snailfishNumbers.add(new DepthList(line));
		}
	}

	public long doPart1() {
		processInput();
		DepthList result = snailfishNumbers.get(0);
		for (int i=1; i< snailfishNumbers.size(); i++) {
			result = result.add(snailfishNumbers.get(i));
		}
		log.info(result.toString());
		return result.magnitude();
	}

	public long doPart2() {
		long result = Long.MIN_VALUE;
		DepthList largest = new DepthList(lines.get(0));
		for (int i = 0; i < lines.size(); i++) {
			for (int j = 0; j < lines.size(); j++) {
				if (i != j) {
					DepthList first = new DepthList(lines.get(i));
					DepthList second = new DepthList(lines.get(j));
					DepthList sum = first.add(second);
					long mag = sum.magnitude();
					if (mag > result) {
						result = mag;
						largest = sum;
					}
				}
			}
		}
		log.info(largest);
		return result;
	}

	public static void main(String[] args) {
		Exercise18alt ex = new Exercise18alt(aocPath(2021, 18));

		// part 1 - 3813 (incorrect)
		long start = System.currentTimeMillis();
		log.info("Part 1 (incorrect!):");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4686 (incorrect)
		start = System.currentTimeMillis();
		log.info("Part 2 (incorrect!):");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
