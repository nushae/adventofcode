package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 5, title = "Hydrothermal Venture", keywords = {"line-segment-intersection"})
public class Exercise05 {

	public static final Pattern PATTERN_LINE = Pattern.compile("(\\d+),(\\d+) -> (\\d+),(\\d+)");

	ArrayList<String> lines;
	int[][] grid;

	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		return calcLines(false);
	}

	public long doPart2() {
		return calcLines(true);
	}

	public long calcLines(boolean includeDiagonals) {
		grid = new int[1000][1000];
		int maxX = 0;
		int maxY = 0;
		for (String line : lines) {
			Matcher m = PATTERN_LINE.matcher(line);
			if (m.find()) {
				int x1 = Integer.parseInt(m.group(1));
				int y1 = Integer.parseInt(m.group(2));
				int x2 = Integer.parseInt(m.group(3));
				int y2 = Integer.parseInt(m.group(4));
				if (includeDiagonals || x1 == x2 || y1 == y2) {
					int dx = (int) Math.signum(x2 - x1);
					int dy = (int) Math.signum(y2 - y1);
					maxX = Math.max(maxX, Math.max(x1, x2));
					maxY = Math.max(maxY, Math.max(y1, y2));
					StringBuilder debugLine = new StringBuilder(line);
					for (int diag = 0; diag <= Math.max(Math.abs(x2 - x1), Math.abs(y2 - y1)); diag++) {
						debugLine.append(" (").append(x1 + dx * diag).append(",").append(y1 + dy * diag).append(")");
						grid[x1 + dx * diag][y1 + dy * diag]++;
					}
					log.debug(debugLine);
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		log.debug("Max: " + maxX + "," + maxY);
		long result = 0;
		for (int x = 0; x <= maxX; x++) {
			for (int y = 0; y <= maxY; y++) {
				result += grid[x][y] > 1 ? 1 : 0;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2021, 5));

		// part 1 - 7269 (0.024s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 21140 (0.027s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
