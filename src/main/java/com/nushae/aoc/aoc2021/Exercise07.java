package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.util.MathUtil;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.function.Function;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 7, title = "The Treachery of Whales", keywords = {"line-fitting", "optimal-path"})
public class Exercise07 {

	ArrayList<String> lines;
	ArrayList<Long> numbers;
	long maxNum;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		numbers = new ArrayList<>();
		maxNum = -1;
		for (String line : lines.get(0).split(",")) {
			long num = Long.parseLong(line);
			numbers.add(num);
			maxNum = Math.max(num, maxNum);
		}
	}

	public long findBestScore(Function<Long, Long> score) {
		processInput();
		long accum = maxNum * score.apply(maxNum+1); // this is definitely higher than the highest possible score
		for (long pivot = 0; pivot < maxNum; pivot++) {
			long total = 0;
			for (long num : numbers) {
				total += score.apply(Math.abs(pivot-num));
			}
			accum = Math.min(total, accum);
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2021, 7));

		// part 1 - 3699852 (0.4 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.findBestScore(MathUtil::identity));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 3561107550 (0.043 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.findBestScore(MathUtil::triangle));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
