package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 1, title = "Sonar Sweep", keywords = {"flank-detection", "sliding-window"})
public class Exercise01 {

	private final List<String>lines;
	private final List<Long> numbers;

	private Exercise01() {
		lines = new ArrayList<>();
		numbers = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		lines.stream().mapToLong(Long::parseLong).forEach(numbers::add);
	}

	public long doPart1() {
		return countUpflanks(numbers, 1);
	}

	public long doPart2() {
		return countUpflanks(numbers, 3);
	}

	// A + B + C > B + C + D  <=> A > D
	private static long countUpflanks(List<Long> numbers, int windowSize) {
		return IntStream.range(windowSize, numbers.size()).filter(i -> numbers.get(i) > numbers.get(i - windowSize)).count();
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2021, 1));

		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1527 (0.003 sec)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1575 (0.001 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
