package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.LiteralPacket;
import com.nushae.aoc.aoc2021.domain.OperatorPacket;
import com.nushae.aoc.aoc2021.domain.Packet;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 16, title = "Packet Decoder", keywords = {"deserialization", "expression-tree", "recursive-descent-parsing"})
public class Exercise16 {

	ArrayList<String> lines;
	List<Packet> packetList;
	StringBuilder input;

	public Exercise16() {
		lines = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		input = new StringBuilder(new BigInteger(input.toString(), 16).toString(2));
		while (input.length() % 4 != 0) {
			input.insert(0, "0");
		}
		packetList = new ArrayList<>();
		while (!input.toString().replaceAll("0", "").isEmpty()) {
			decodePacket(packetList);
		}
		log.info("Read " + packetList.size() + " top level packet(s)");
	}

	/**
	 * Decode next Packet from the input, consuming the bits that describe it from the input.
	 * Return the number of bits read.
	 * Store the Packet in the passed list storeInto as a side effect.
	 *
	 * @param storeInto List<Packet> to store the Packet in
	 * @return amount of bits read
	 */
	public int decodePacket(List<Packet> storeInto) {
		int pversion = eat(3);
		int ptype = eat(3);
		int bitsRead = 6;
		if (ptype == 4) {
			boolean stop = false;
			long num = 0;
			while (!stop) {
				if (eat(1) == 0) {
					stop = true;
				}
				num = 16 * num + eat(4);
				bitsRead += 5;
			}
			storeInto.add(new LiteralPacket(pversion, ptype, num));
		} else { // operator packet
			List<Packet> children = new ArrayList<>();
			if (eat(1) == 0) { // by total length
				long totalLength = eat(15);
				bitsRead += 16 + totalLength;
				while (totalLength > 0) {
					int br = decodePacket(children);
					totalLength -= br;
				}
			} else { // by number of subpackets
				int subPackets = eat(11);
				bitsRead += 12;
				for (int i =0 ; i < subPackets; i++) {
					bitsRead += decodePacket(children);
				}
			}
			storeInto.add(new OperatorPacket(pversion, ptype, children));
		}
		return bitsRead;
	}

	/**
	 * Consume numBits bits from the input, and return the number they represent
	 *
	 * @param numBits int the number of bits to consume
	 * @return long the number represented by the consumed bits
	 */
	public int eat(int numBits) {
		int result = Integer.parseInt(input.substring(0, numBits), 2);
		input.delete(0, numBits);
		return result;
	}

	public long doPart1() {
		long result = 0;
		for (Packet p : packetList) {
			result += p.getVersionSum();
		}
		return result;
	}

	public long doPart2() {
		return packetList.get(0).evaluate();
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2021, 16));

		// Input processing (0.01s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 993 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 144595909277 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
