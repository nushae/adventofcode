package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 24, title = "Arithmetic Logic Unit", keywords = {"code-analysis", "erlang"})
public class Exercise24 {

	// the parameters for the simplified version of the input
	public final static int[] PARAM_A = new int[] {1,  1,  1,  1,  26,  1,  1, 26,  1, 26, 26, 26,  26, 26};
	public final static int[] PARAM_B = new int[] {12, 11, 10, 10, -16, 14, 12, -4, 15, -7, -8, -4, -15, -8};
	public final static int[] PARAM_C = new int[] {6, 12, 5, 10, 7, 0, 4, 12, 14, 13, 10, 11, 9, 9};

	ArrayList<String> lines;
	Map<String, Long> registers;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1Naive() {
		// Even though I found a much quicker solution for part 2, I'm leaving this in for educational, historical, and
		// (most of all) entertainment purposes... 14 nested loops baby! (Takes only 3 min 26 sec to run...)
		//
		// Full disclosure, I did some educated guessing here. I suspected the number would be quite low, so I tried
		// starting with these numbers (I waited up to 5 mins before trying the next, planning to submit each answer
		// found until one was accepted, so at worst 13 failed tries):
		// 91111111111111, or rather 89999999999999, so d1 ranges from 8 instead of 9
		// 99111111111111, or rather 98999999999999, so d2 ...
		// 99911111111111, or rather 99899999999999
		// etc.
		// This yielded no results for the original interpreter variant :(
		// Then I reimplemented the input directly in java and tried again resulting in the 8 for d3 and the answer was accepted too :)
		for (int d1 = 9; d1 > 0; d1--) {
			for (int d2 = 9; d2 > 0; d2--) {
				for (int d3 = 8; d3 > 0; d3--) {
					for (int d4 = 9; d4 > 0; d4--) {
						for (int d5 = 9; d5 > 0; d5--) {
							for (int d6 = 9; d6 > 0; d6--) {
								for (int d7 = 9; d7 > 0; d7--) {
									for (int d8 = 9; d8 > 0; d8--) {
										for (int d9 = 9; d9 > 0; d9--) {
											for (int d10 = 9; d10 > 0; d10--) {
												for (int d11 = 9; d11 > 0; d11--) {
													for (int d12 = 9; d12 > 0; d12--) {
														for (int d13 = 9; d13 > 0; d13--) {
															for (int d14 = 9; d14 > 0; d14--) {
																int[] numbers = new int[]{d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14};
																// run(numbers); // heh, 'run the numbers'
																// if (registers.getOrDefault("z", 0L) == 0) {
																if (runSimplified(numbers)) {
																	long result = 0;
																	for (int i = 0; i < 14; i++) {
																		result = result * 10 + numbers[i];
																	}
																	return result;
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return -1;
	}

	public long doPart1or2(boolean part1) {
		// The approach from part1 doesn't work here and after trying it fruitlessly for some time I did a bit of more analysis
		// WHY NOT SOONER -_-
		//
		// So what does this code DO?
		// w = nextDigit();
		// x = (z % 26 + B) != w ? 1 : 0;
		// z = z / A;                      // either z stays the same (A == 1) or is divided by 26
		// z = z * (25 * x + 1);           // either z stays the same (x == 0) or gets multiplied by 26
		// z = z + (w+C) * x;              // either z stays the same (x == 0) or is increased by some value < 26 (C is always < 16)
		//
		// Since A is always either 1 or 26 it acts as a sort of boolean; so does x which is always either 1 or 0:
		//
		// w = nextDigit();
		// x = (z % 26 + B) != w ? 1 : 0;
		// if (A == 26) {
		//   z = z / 26;
		// }
		// if (x == 1) {
		//   z = z *26 + w+C;
		// }
		//
		// This makes it clear that z acts like a stack of 26-ary digits that either:
		// 1) shrinks (if A == 26 and x == 0)
		// 2) stays the same 'size' (if A == 26 and x == 1 or A == 1 and x == 0)
		// 3) grows (if A == 1 and x == 1)
		//
		// z can only ever be 0 at the end if it shrinks *at least* as often as it grows.
		//
		// It turns out that whenever A == 1, x is always 1 as well. This is because all B values are > 9 whenever
		// A == 1, and of course w is always 9 or less.
		//
		// So if A == 1, z definitely grows. This happens 7 times, so the other 7 times z must shrink or it won't be 0 again.
		// If A == 26, z only shrinks if x == 0
		//
		// So we need z % 26 + b != w to be false in ALL cases that A == 26.
		//
		// But that simply means that 'last added digit' + its C + current B == current digit
		//
		// So this is what happens:
		//
		// digit    A     B     C    add or remove?                      z when all the ifs are true:
		// -----   --    --    --    --------------------------------    ---------------------------
		// 0        1    12     6    add                                 [0]
		// 1        1    11    12    add                                 [0][1]
		// 2        1    10     5    add                                 [0][1][2]
		// 3        1    10    10    add                                 [0][1][2][3]
		// 4       26   -16     7    remove (if [3] + 10 - 16 == [4])    [0][1][2]
		// 5        1    14     0    add                                 [0][1][2][5]
		// 6        1    12     4    add                                 [0][1][2][5][6]
		// 7       26    -4    12    remove (if [6] + 4 - 4 == [7])      [0][1][2][5]
		// 8        1    15    14    add                                 [0][1][2][5][8]
		// 9       26    -7    13    remove (if [8] + 14 - 7 ==[9])      [0][1][2][5]
		// 10      26    -8    10    remove (if [5] + 0 - 8 == [10])     [0][1][2]
		// 11      26    -4    11    remove (if [2] + 5 - 4 == [11])     [0][1]
		// 12      26   -15     9    remove (if [1] + 12 - 15 == [12])   [0]
		// 13      26    -8     9    remove (if [0] + 6 - 8 == [13])     --
		//
		// Or, in other words:
		// valid == (input[3] - 6 == input[4] &&
		//           input[6] == input[7] &&
		//           input[8] + 7 == input[9] &&
		//           input[5] - 8 == input[10] &&
		//           input[2] + 1 == input[11] &&
		//           input[1] - 3 == input[12] &&
		//           input[0] - 2 == input[13]
		//
		// From these rules we can deduce the minimum & maximum numbers since each digit has a range it must fall within
		// to be valid:
		// digit  range
		//   0    3 - 9
		//   1    4 - 9
		//   2    1 - 8
		//   3    7 - 9
		//   4    1 - 3
		//   5    9 - 9
		//   6    1 - 9
		//   7    1 - 9
		//   8    1 - 2
		//   9    8 - 9
		//   10   1 - 1
		//   11   2 - 9
		//   12   1 - 6
		//   13   1 - 7
		// Reading vertically, the minimum becomes 34171911181211 and the maximum is indeed 99893999291967 as calculated -_-'
		return part1 ? 99893999291967L : 34171911181211L;
	}

	public void run(int[] input) {
		registers = new HashMap<>();
		int inPointer = 0;
		for (String line : lines) {
			String[] parts = line.split("\\s");
			long value;
			switch (parts[0]) {
				case "inp":
					if (inPointer >= input.length) { // already processed every input, do not accept
						System.out.println("Input ran out...");
						registers.put("z", 1L);
						return;
					}
					value = input[inPointer++];
					if (isRegister(parts[1])) {
						registers.put(parts[1], value);
					} else {
						System.out.println("Not a register: " + line);
						registers.put("z", 1L);
						return;
					}
					break;
				case "add":
					if (isRegister(parts[1])) {
						if (isRegister(parts[2])) {
							value = registers.getOrDefault(parts[2], 0L);
						} else {
							value = Long.parseLong(parts[2]);
						}
						registers.put(parts[1], registers.getOrDefault(parts[1], 0L) + value);
					}
					break;
				case "mul":
					if (isRegister(parts[1])) {
						if (isRegister(parts[2])) {
							value = registers.getOrDefault(parts[2], 0L);
						} else {
							value = Long.parseLong(parts[2]);
						}
						registers.put(parts[1], registers.getOrDefault(parts[1], 0L) * value);
					}
					break;
				case "div":
					if (isRegister(parts[1])) {
						if (isRegister(parts[2])) {
							value = registers.getOrDefault(parts[2], 0L);
						} else {
							value = Long.parseLong(parts[2]);
						}
						if (value == 0) {
							System.out.println("Division by zero");
							registers.put("z", 1L);
							return;
						}
						registers.put(parts[1], registers.getOrDefault(parts[1], 0L) / value);
					}
					break;
				case "mod":
					if (isRegister(parts[1])) {
						long stored = registers.getOrDefault(parts[1], 0L);
						if (isRegister(parts[2])) {
							value = registers.getOrDefault(parts[2], 0L);
						} else {
							value = Long.parseLong(parts[2]);
						}
						if (stored < 0 || value <= 0) {
							System.out.println("Mod error: " + stored + " % " + value);
							registers.put("z", 1L);
							return;
						}
						registers.put(parts[1], registers.getOrDefault(parts[1], 0L) % value);
					}
					break;
				case "eql":
					if (isRegister(parts[1])) {
						if (isRegister(parts[2])) {
							value = registers.getOrDefault(parts[2], 0L);
						} else {
							value = Long.parseLong(parts[2]);
						}
						registers.put(parts[1], registers.getOrDefault(parts[1], 0L) == value ? 1L : 0L);
					}
					break;
				default:
					System.out.println("Unknown instruction " + line);
			}
		}
	}
	public boolean isRegister(String test) {
		return "@w@x@y@z@".contains("@" + test + "@");
	}

	// Input analysis:
	// The input is 14 blocks of 'the same' code (with A, B and C changing):
	// inp w
	// mul x 0
	// add x z
	// mod x 26
	// div z A = { 1,  1,  1,  1,  26,  1,  1, 26,  1, 26, 26, 26,  26, 26}
	// add x B = {12, 11, 10, 10, -16, 14, 12, -4, 15, -7, -8, -4, -15, -8}
	// eql x w
	// eql x 0
	// mul y 0
	// add y 25
	// mul y x
	// add y 1
	// mul z y
	// mul y 0
	// add y w
	// add y C = {6, 12, 5, 10, 7, 0, 4, 12, 14, 13, 10, 11, 9, 9}
	// mul y x
	// add z y
	//
	// This becomes:
	// w = nextDigit();
	// x = (z % 26 + B) != w ? 1 : 0;
	// z = z / A;
	// z = z * (25 * x + 1) + (w+C) * x;
	//
	// So we can make a version of the input in java and run against this code:
	boolean runSimplified(int[] input) {
		int w;
		int x;
		int z = 0;
		for (int i = 0; i< 14; i++) {
			w = input[i];
			x = ((z % 26) + PARAM_B[i]) != w ? 1 : 0;
			z = z / PARAM_A[i];
			z = z * (25 * x +1) + (w + PARAM_C[i]) * x;
		}
		return z == 0;
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2021, 24));

		// Naive:
		// part 1 - 99893999291967 (3 min 26.027 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1 via running the code:");
		log.info(ex.doPart1Naive());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// After analysis:
		// part 1 - 99893999291967 (instantaneous)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1or2(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 34171911181211 (instantaneous)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart1or2(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
