package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.Universe;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Aoc(year = 2021, day = 21, title = "Dirac Dice", keywords = {"game", "optimal-strategy", "pruning"})
public class Exercise21 {

	int start1;
	int start2;

	public Exercise21(int player1Start, int player2Start) {
		start1 = player1Start;
		start2 = player2Start;
	}

	public long doPart1() {
		return deterministic(start1, start2);
	}

	public long doPart2() {
		return dirac(start1, start2);
	}

	// Deterministic, aka brute force that mf
	public long deterministic(int pos1, int pos2) {
		long score1 = 0;
		long score2 = 0;
		int dieRoll = 0;
		for (int turn = 1; turn <= 1000; turn++) {
			int totalRoll = (dieRoll = dieRoll % 100 + 1);
			totalRoll += (dieRoll = dieRoll % 100 + 1);
			totalRoll += (dieRoll = dieRoll % 100 + 1);
			pos1 = (pos1 + totalRoll - 1) % 10 + 1;
			score1 += pos1;
			if (score1 >= 1000) {
				return (2 * turn - 1) * 3 * score2;
			}
			totalRoll = (dieRoll = dieRoll % 100 + 1);
			totalRoll += (dieRoll = dieRoll % 100 + 1);
			totalRoll += (dieRoll = dieRoll % 100 + 1);
			pos2 = (pos2 + totalRoll - 1) % 10 + 1;
			score2 += pos2;
			if (score2 >= 1000) {
				return 2 * turn * 3 * score1;
			}
		}
		return -1;
	}

	// Combinatorial explosion, aka DON'T brute force that mf (we're talking 9^21 here)
	// It's the lantern fish all over again!
	public long dirac(int pos1, int pos2) {
		// this map saves one loop per player ;)
		Map<Integer, Integer> totalRolls = new HashMap<>();
		for (int i = 1; i < 4; i++) {
			for (int j = 1; j < 4; j++) {
				for (int k = 1; k < 4; k++) {
					totalRolls.put(i+j+k, totalRolls.getOrDefault(i+j+k, 0)+1);
				}
			}
		}

		// We'll keep track of 'number of not-yet-won universes with the same state' to keep things managable.
		// Our state will be the positions and scores of both players.
		Map<Universe, Long> universes = new HashMap<>();
		universes.put(new Universe (pos1, 0L, pos2, 0L), 1L); // start with just 'our' universe
		long wins1 = 0;
		long wins2 = 0;
		while (universes.size() > 0) {
			Map<Universe, Long> newUniverses = new HashMap<>();
			for (Map.Entry<Universe, Long> entry : universes.entrySet()) {
				int position1 = entry.getKey().p1;
				long score1 = entry.getKey().s1;
				int position2 = entry.getKey().p2;
				long score2 = entry.getKey().s2;
				long uniCount = entry.getValue();
				for (Map.Entry<Integer, Integer> entry1 : totalRolls.entrySet()) {
					int roll1 = entry1.getKey();
					int rollCount1 = entry1.getValue();
					int newPos1 = (position1 + roll1 - 1) % 10 + 1;
					long newScore1 = score1 + newPos1;
					if (newScore1 >= 21) {
						wins1 += uniCount * rollCount1;
						// do not copy over any universe entry, so they are deleted from the todolist
					} else {
						// one didn't win, we've now virtually split these uniCount universes into uniCount*rollCount1 universes
						for (Map.Entry<Integer, Integer> entry2 : totalRolls.entrySet()) {
							int roll2 = entry2.getKey();
							int rollCount2 = entry2.getValue();
							int newPos2 = (position2 + roll2 - 1) % 10 + 1;
							long newScore2 = score2 + newPos2;
							if (newScore2 >= 21) {
								wins2 += uniCount * rollCount1 * rollCount2;
								// do not copy over any universe entry, so they are deleted from the todolist
							} else {
								// neither won; concretely split these uniCount*rollCount1 universes into uniCount*rollCount1*uniCount2 new ones
								Universe univ = new Universe(newPos1, newScore1, newPos2, newScore2);
								newUniverses.put(univ, newUniverses.getOrDefault(univ, 0L) + uniCount * rollCount1 * rollCount2);
							}
						}
					}
				}
			}
			universes = newUniverses;
		}
		return Math.max(wins1, wins2);
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(6, 4);

		// part 1 - 920580 (<0.001 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 647920021341197 (0.063 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
