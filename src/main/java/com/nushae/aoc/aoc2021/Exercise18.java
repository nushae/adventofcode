package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.SnailFishNumber;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 18, title = "Snailfish", keywords = {"tree-manipulation", "nested-numbers", "esoteric-math"})
public class Exercise18 {

	ArrayList<String> lines;
	ArrayList<SnailFishNumber> snailfishNumbers;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		snailfishNumbers = new ArrayList<>();
		for (String line : lines) {
			snailfishNumbers.add(new SnailFishNumber(line));
		}
	}

	public long doPart1() {
		processInput();
		SnailFishNumber result = snailfishNumbers.get(0);
		for (int i=1; i< snailfishNumbers.size(); i++) {
			result = result.add(snailfishNumbers.get(i));
		}
		log.debug(result.toString());
		return result.magnitude();
	}

	public long doPart2() {
		long result = Long.MIN_VALUE;
		SnailFishNumber largest = null;
		for (int i = 0; i < lines.size(); i++) {
			for (int j = 0; j < lines.size(); j++) {
				if (i != j) {
					SnailFishNumber first = new SnailFishNumber(lines.get(i));
					SnailFishNumber second = new SnailFishNumber(lines.get(j));
					SnailFishNumber sum = first.add(second);
					long sumMag = sum.magnitude();
					if (sumMag > result) {
						largest = sum;
						result = sumMag;
					}
				}
			}
		}
		log.debug(largest);
		return result;
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2021, 18));

		// part 1 - 4235 (0.05 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4659 (0.2 sec)
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
