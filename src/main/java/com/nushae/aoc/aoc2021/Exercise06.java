package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 6, title = "Lanternfish", keywords = {"exponential-growth", "aggregation"})
public class Exercise06 {

	ArrayList<String> lines;
	ArrayList<Integer> numbers;
	long[] lanternFishByDay;

	public Exercise06() {
		lines = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private long processInput() {
		lanternFishByDay = new long[9];
		numbers = new ArrayList<>();
		long totalFish = 0;
		for (String num : lines.get(0).split(",")) {
			int n = Integer.parseInt(num);
			numbers.add(n);
			lanternFishByDay[n]++;
			totalFish++;
		}
		return totalFish;
	}

	public long doItNaively(int numdays) {
		processInput();
		ArrayList<Integer> squids = new ArrayList<>();
		for (int i = 0 ; i < numdays ; i++) {
			squids = new ArrayList<>();
			int eights = 0;
			for (int num : numbers) {
				if (num == 0) {
					eights++;
					squids.add(6);
				} else {
					squids.add(num - 1);
				}
			}
			for (int j = 0 ; j < eights; j++) {
				squids.add(8);
			}
			numbers = squids;

		}
		return squids.size();
	}

	public long doItSmart(int days) {
		processInput();
		int zero = 0;
		long accum = processInput();
		for (int i = 0; i < days; i++) {
			if (lanternFishByDay[zero] > 0 ) {
				lanternFishByDay[(zero + 7) % 9] += lanternFishByDay[zero];
				accum += lanternFishByDay[zero];
			}
			zero = (zero + 1 ) % 9;
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2021, 6));

		// part 1 - 374927 (<0.001s; naively: 0.056s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
//		System.out.println(ex.doItNaively(80));
		log.info(ex.doItSmart(80));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1687617803407 (0.001s; we don't talk about naively)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doItSmart(256));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
