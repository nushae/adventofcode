package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 2, title = "Dive!", keywords = {"pirate-walk", "pathing"})
public class Exercise02 {

	ArrayList<String> lines;
	private long horizontalPos;
	private long depth;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		horizontalPos = 0;
		depth = 0;
		for (String line : lines) {
			if (line.startsWith("forward ")) {
				int val = Integer.parseInt(line.substring(8));
				horizontalPos += val;
			} else if (line.startsWith("down ")) {
				int val = Integer.parseInt(line.substring(5));
				depth += val;
			} else if (line.startsWith("up ")) {
				int val = Integer.parseInt(line.substring(3));
				depth -= val;
			}
		}
		return horizontalPos * depth;
	}

	public long doPart2() {
		horizontalPos = 0;
		depth = 0;
		long aim = 0;
		for (String line : lines) {
			if (line.startsWith("forward ")) {
				int val = Integer.parseInt(line.substring(8));
				horizontalPos += val;
				depth += aim*val;
			} else if (line.startsWith("down ")) {
				int val = Integer.parseInt(line.substring(5));
				aim += val;
			} else if (line.startsWith("up ")) {
				int val = Integer.parseInt(line.substring(3));
				aim -= val;
			}
		}
		return horizontalPos * depth;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2021, 2));

		// part 1 - 1561344 (0.001s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 1848454425 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
