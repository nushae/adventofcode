package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.util.OCRUtil;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 13, title = "Transparent Origami", keywords = {"grid", "folding", "big-letters"})
public class Exercise13 {

	private static final Pattern PIXEL = Pattern.compile("(\\d+),(\\d+)");
	private static final Pattern FOLD = Pattern.compile("fold along ([xy])=(\\d+)");

	ArrayList<String> lines;
	ArrayList<Integer> instructions;
	boolean[][] paper;

	public Exercise13() {
		lines = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		instructions = new ArrayList<>();
		Set<Point> pixels = new HashSet<>();
		int maxX = 0;
		int maxY = 0;
		for (String line : lines) {
			if (line.trim().isEmpty()) {
				continue;
			}
			if (line.startsWith("fold")) {
				Matcher m = FOLD.matcher(line);
				if (m.find()) {
					int num = Integer.parseInt(m.group(2));
					if ("x".equals(m.group(1))) {
						instructions.add(-num);
					} else {
						instructions.add(num);
					}
				} else {
					log.warn("Don't know how to parse " + line);
				}
			} else { // line starts with digit
				Matcher m = PIXEL.matcher(line);
				if (m.find()) {
					int x = Integer.parseInt(m.group(1));
					int y = Integer.parseInt(m.group(2));
					pixels.add(new Point(x, y));
					maxX = Math.max(maxX, x);
					maxY = Math.max(maxY, y);
				} else {
					log.warn("Don't know how to parse " + line);
				}
			}
		}
		paper = new boolean[maxY+1][maxX+1];
		for (Point p : pixels) {
			paper[p.y][p.x] = true;
		}
	}

	public long doPart1() {
		long result = 0;
		processInput();
		paper = fold(instructions.get(0), paper);
		for (boolean[] bools : paper) {
			for (boolean b : bools){
				result += b ? 1 : 0;
			}
		}
		return result;
	}

	public String doPart2() {
		processInput();
		for (int num : instructions) {
			paper = fold(num, paper);
		}
		return(OCRUtil.crt4x6AsString(paper));
	}

	// Observe that the fold line disappears!
	// suppose we have a grid of 8x3:
	//  01234567
	// 0.......#
	// 1..#.....
	// 2....#...
	//    Fold along x = 6:                    But fold along x = 3:
	//  01234567          012345  (6x3)     01234567          0123  (4x3)
	// 0......|#         0.....#            0...|...#         0#...
	// 1..#...|.    ->   1..#...            1..#|....    ->   1...#
	// 2....#.|.         2....#.            2...|#...         2...#
	// The left side of the fold is along wide, while the right side is width - along - 1 wide.
	// The resulting grid will be max(width - along - 1, along) wide.
	// Analogously, new height will be max(height - along - 1, along) for vertical folds.
	public boolean[][] fold(int along, boolean[][] paper) {
		boolean foldHorizontal = true;
		int inputHeight = paper.length;
		int inputWidth = paper[0].length;
		boolean[][] newPaper;
		if (along < 0) { // along x = N
			along = -along;
			newPaper = new boolean[inputHeight][Math.max(inputWidth - along - 1, along)];
		} else { // along y = N
			newPaper = new boolean[Math.max(inputHeight - along - 1, along)][inputWidth];
			foldHorizontal = false;
		}

		int height = newPaper.length;
		int width = newPaper[0].length;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (foldHorizontal) {
					// new(i, j) = combine(old(ALONG - WIDTH + i, j), old(ALONG + WIDTH - i, j))
					newPaper[j][i] = (along - width + i >= 0) && paper[j][along - width + i] || (along + width - i < inputWidth) && paper[j][along + width - i];
				} else {
					newPaper[j][i] = (along - height + j >= 0) && paper[along - height + j][i] || (along + height - j < inputHeight) && paper[along + height - j][i];
				}
			}
		}
		return newPaper;
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2021, 13));

		// part 1 - 755 (13ms)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - BLKJRBAG (17ms)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
