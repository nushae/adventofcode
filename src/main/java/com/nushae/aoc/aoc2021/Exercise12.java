package com.nushae.aoc.aoc2021;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

/**
 * Part two allows a single node to be visited twice, with all others still only allowed to be visited once.
 * It is unclear how to extend that rule to max visit counts larger than 2: (A) is a single node allowed to be visited
 * more than 1x to a maximum of MAX (with the others still only allowed to be visited once), or (B) are all nodes
 * allowed to be visited more than once up to MAX - 1 times, with one node allowed to be visited MAX times? Both
 * interpretations have the same meaning for MAX = 2...
 * Since it doesn't matter for the contest, I chose interpretation A, which is simpler to implement, so now you
 * can see what happens if nodes may be visited 4 times, for example ;)
 */
@Log4j2
@Aoc(year = 2021, day = 12, title = "Passage Pathing", keywords = {"graph", "pathing", "enumeration"})
public class Exercise12 {

	private static final Pattern CAVE_ENDPOINTS = Pattern.compile("(.*)-(.*)");

	ArrayList<String> lines;
	Map<String, Set<String>> adjacentNodes;
	Set<String> pathsFoundSoFar;
	int maxVisits;
	Map<String, Integer> visitedNodes;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		adjacentNodes = new HashMap<>();
		for (String line : lines) {
			Matcher m = CAVE_ENDPOINTS.matcher(line);
			if (m.find()) {
				Set<String> endPoints = adjacentNodes.getOrDefault(m.group(1), new HashSet<>());
				endPoints.add(m.group(2));
				adjacentNodes.put(m.group(1), endPoints);
				endPoints = adjacentNodes.getOrDefault(m.group(2), new HashSet<>());
				endPoints.add(m.group(1));
				adjacentNodes.put(m.group(2), endPoints);
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		processWithPattern();
		unMarkAll();
		pathsFoundSoFar = new HashSet<>();
		mark("start");
		return findPaths("start", "start", 1);
	}

	public long doPart2() {
		processWithPattern();
		unMarkAll();
		pathsFoundSoFar = new HashSet<>();
		mark("start");
		return findPaths("start", "start", 2);
	}

	public void unMarkAll() {
		maxVisits = 0;
		visitedNodes = new HashMap<>();
	}

	public long findPaths(String node, String pathSoFar, int maxVisitsToLowerCase) {
		pathSoFar += "-" + node;
		if ("end".equals(node)) {
			if (!pathsFoundSoFar.contains(pathSoFar)) {
				pathsFoundSoFar.add(pathSoFar);
				log.debug(pathSoFar);
				return 1;
			}
			return 0;
		}
		long result = 0;
		for (String dest : adjacentNodes.get(node)) {
			if (!isMarked(dest, maxVisitsToLowerCase)) {
				mark(dest);
				result += findPaths(dest, pathSoFar, maxVisitsToLowerCase);
				unmark(dest);
			}
		}
		return result;
	}

	/**
	 * <p>Returns true when a node is considered marked, false otherwise.</p>
	 *
	 * <p>If a node is all uppercase it is never considered marked; the "start" node is always considered marked.</p>
	 *
	 * <ul>
	 * <li>If maxVisitsToLowerCase is less than 2:<br/>
	 *    All other nodes are considered marked if they have been visited</li>
	 *
	 * <li>If maxVisitsToLowerCase is larger than 1:<br/>
	 *    All other nodes are considered marked if they have been visited maxVisitsToLowerCase - 1 times except for
	 *    one that may be visited maxVisitsToLowerCase times.</li>
	 * </ul>
	 *
	 * @param node node to test
	 * @param maxVisitsToLowerCase highest number of visits allowed to a lower case node
	 * @return true iff the node is considered marked
	 */
	public boolean isMarked(String node, int maxVisitsToLowerCase) {
		if ("start".equals(node)) {
			return true;
		}
		if (node.equals(node.toUpperCase())) {
			return false;
		}
		if (maxVisitsToLowerCase < 2) { // normal visit-once behavior
			return visitedNodes.getOrDefault(node, 0) > 0;
		}
		// higher-visit-count-allowed-for-one-node behavior
		return maxVisits == maxVisitsToLowerCase && visitedNodes.getOrDefault(node, 0) >= maxVisitsToLowerCase-1;
	}

	public void mark(String node) {
		if (node.equals(node.toUpperCase())) {
			return;
		}
		int visits = visitedNodes.getOrDefault(node, 0) + 1;
		visitedNodes.put(node, visits);
		maxVisits = Math.max(maxVisits, visits);
	}

	public void unmark(String node) {
		if (node.equals(node.toUpperCase())) {
			return;
		}
		visitedNodes.put(node, visitedNodes.getOrDefault(node, 0) - 1);
		maxVisits = 0;
		for (int v : visitedNodes.values()) {
			maxVisits = Math.max(maxVisits, v);
		}
	}

	public boolean isAllCaps(String testme) {
		return testme.equals(testme.toUpperCase());
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2021, 12));

		// part 1 - 4338 (0.02 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 114189 (0.25 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
