package com.nushae.aoc.aoc2021;

import com.nushae.aoc.aoc2021.domain.BingoCard;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2021, day = 4, title = "Giant Squid", keywords = {"bingo"})
public class Exercise04 {

	ArrayList<String> lines;
	Map<Integer, List<BingoCard>> cardIndex;
	ArrayList<BingoCard> bingoBoards;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		bingoBoards = new ArrayList<>();
		cardIndex = new HashMap<>();
		for (int i = 0; i < (lines.size() - 1) / 6; i++) {
			List<Integer> nums = new ArrayList<>();
			for (int j=0; j < 5; j++) {
				String line = lines.get(i*6+2+j);
				for (String num : line.trim().split("\\s+")) {
					nums.add(Integer.parseInt(num));
				}
			}
			BingoCard b =  new BingoCard(nums);
			bingoBoards.add(b);
			for (int num: nums) {
				List<BingoCard> cards = cardIndex.getOrDefault(num, new ArrayList<>());
				cards.add(b);
				cardIndex.put(num, cards);
			}
		}
	}

	public long doPart1() {
		processInput();
		for (String num : lines.get(0).split(",")) {
			int n = Integer.parseInt(num);
			log.info("Called: " + n);
			for (BingoCard bc : cardIndex.get(n)) {
				long result = bc.callNum(n);
				if (result > 0) {
					log.debug("BINGO on this card:\n" + bc);
					return result;
				}
			}
		}
		return -1;
	}

	public long doPart2() {
		processInput();
		for (String num : lines.get(0).split(",")) {
			int n = Integer.parseInt(num);
			for (BingoCard bc : cardIndex.get(n)) {
				if (bingoBoards.contains(bc)) {
					long result = bc.callNum(n);
					if (result > 0) {
						if (bingoBoards.size() > 1) {
							bingoBoards.remove(bc);
						} else {
							log.debug("Worst card found:\n" + bc);
							return result;
						}
					}
				}
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2021, 4));

		// part 1 - 74320 (0.015s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 17884 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
