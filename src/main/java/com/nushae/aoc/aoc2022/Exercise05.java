package com.nushae.aoc.aoc2022;

import com.nushae.aoc.aoc2022.domain.Move;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.transpose;

@Log4j2
@Aoc(year = 2022, day = 5, title="Supply Stacks", keywords = {"shuffling", "list-manipulation"})
public class Exercise05 {

	private static final Pattern MOVE = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");

	List<String> lines;
	List<Move> moveList;
	String[] stacks;

	// Some analysis
	//
	// This is the first assignment where the meat of the matter is the input processing. While the moves themselves
	// can simply be parsed with a pattern matcher (I will not go into that), the first part of the input must
	// be reorganized, essentially, from a set of columns into rows. The analysis will focus on these 'stack' lines.
	//
	// Every line naturally breaks into chunks of 4 characters, either "    " (4 spaces) for empty columns,
	// or "[X] ", where X is any letter, for a column with data. The trailing space is missing for the last column in
	// each line, but that is ok, since we only need the 2nd character from each chunk anyway.
	//
	// Each column is formed by appending its corresponding character from each line; this yields the columns in
	// reverse order (you must skip appending the spaces, or trim the column data after the fact to get rid of the
	// empty spots). Since we'll be operating on the tops of the stacks while executing the moves, this reverse order
	// is actually useful to keep the operations simple.
	//
	// Example:
	// line[0] = "[A]"
	// line[1] = "[B] [C]"
	// line[2] = "[D] [E] [F]"
	// line[3] = " 1   2   3"
	//
	// Will become
	// stacks[0] = "ABD1"
	// stacks[1] = "CE2"
	// stacks[2] = "F3"
	//
	// Originally I trimmed off the column numbers but this is not actually necessary since the moves will never
	// operate on those spots.
	//
	// Executing a move like "move N from i to j" will then remove the first N characters from string i and insert them
	// in reverse order at the front of string j (part 1) or insert them without reversing (part 2). That's the simple part.
	// The answer in both cases is just a concatenation of the first characters of each string.

	public Exercise05() {
		lines = new ArrayList<>();
	}

	public Exercise05(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise05(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		String[] stackrows = new String[20];
		int row = 0;
		stacks = new String[20];
		moveList = new ArrayList<>();
		boolean readingRows = true;
		for (String line : lines) {
			if ("".equals(line)) { // switch mode
				readingRows = false;
				stackrows = transpose(stackrows);
				// remove leading spaces that came from empty spots in the tops of shorter columns
				for (int i=0; i < (stackrows.length+1)/4; i++) {
					if (stackrows[i*4+1] != null) {
						stacks[i] = stackrows[i*4+1].trim();
					}
				}
			} else {
				if (readingRows) {
					stackrows[row++] = line;
				} else { // moves
					Matcher m = MOVE.matcher(line);
					if (m.find()) {
						moveList.add(new Move(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3))));
					} else {
						log.warn("Don't know how to parse " + line);
					}
				}
			}
		}
	}

	public String calculate(boolean part1) {
		processInput(); // needed to reset state
		StringBuilder result = new StringBuilder();
		for (Move m : moveList) {
			String source = stacks[m.src-1];
			String dest = stacks[m.dest-1];
			String letters = source.substring(0,m.amount);
			if (part1) {
				letters = StringUtils.reverse(letters);
			}
			source = source.substring(m.amount);
			dest = letters + dest;
			stacks[m.src-1] = source;
			stacks[m.dest-1] = dest;
		}
		for (int i = 0; i < 20; i++) {
			if (stacks[i] != null) {
				result.append(stacks[i].charAt(0));
			}
		}
		return result.toString();
	}

	public static void main(String[] args) {
		Exercise05 ex = new Exercise05(aocPath(2022, 5));

		// part 1 - QMBMJDFTD (0.021s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.calculate(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - NBTVTJNFJ (0.001s)
		log.info("Part 2:");
		start = System.currentTimeMillis();
		log.info(ex.calculate(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
