package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.intersection;

@Log4j2
@Aoc(year = 2022, day = 3, title="Rucksack Reorganization", keywords = {"set-intersection", "sliding-window", "map-reduce"})
public class Exercise03 {

	List<String> lines;

	public Exercise03() {
		lines = new ArrayList<>();
	}

	public Exercise03(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise03(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doPart1() {
		long accum = 0;
		for (String line : lines) {
			String line1 = line.substring(0, line.length() / 2);
			String line2 = line.substring(line.length() / 2);
			String match = intersection(line1, line2);
			accum += "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(match) + 1;
		}
		return accum;
	}

	public long doPart2() {
		long accum = 0;
		String[] groups = new String[3];
		int index = 0;
		for (String line : lines) {
			groups[index] = line;
			index++;
			if (index > 2) {
				index = 0;
				String match = intersection(intersection(groups[0], groups[1]), groups[2]);
				accum += "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(match)+1;
				groups = new String[3];
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise03 ex = new Exercise03(aocPath(2022, 3));

		// part 1 - 8233 (0.015s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2821 (0.005s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
