package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static java.lang.Math.pow;

@Log4j2
@Aoc(year = 2022, day = 25, title="Full of Hot Air", keywords = {"5-ary", "esoteric-numbers"})
public class Exercise25 {

	List<String> lines;
	List<Long> numbers;
	long total;

	public Exercise25() {
		lines = new ArrayList<>();
	}

	public Exercise25(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise25(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private String processInput() {
		numbers = new ArrayList<>();
		total = 0;
		for (String line : lines) {
			long num = fromSnafu(line);
			numbers.add(num);
			total += num;
		}
		System.out.println(total);
		return toSnafu(total);
	}

	public long fromSnafu(String line) {
		long num = 0;
		for (int place = 0; place < line.length(); place++) {
			int pos = line.length()-1-place;
			char c = line.charAt(pos);
			num += pow(5, place) * (c == '=' ? -2 : (c == '-' ? -1 : c-'0'));
		}
		return num;
	}

	public String toSnafu(long num) {
		StringBuilder result = new StringBuilder();
		while (num > 0) {
			int digit = (int) (num % 5);
			if (digit > 2) {
				digit -= 5;
				num = num / 5 + 1;
			} else {
				num /= 5;
			}
			char symbol = new char[] {'=','-','0','1','2'}[2+digit];
			result.insert(0, symbol);
		}
		return result.toString();
	}

	public String doPart1() {
		return processInput();
	}

	public static void main(String[] args) {
		Exercise25 ex = new Exercise25(aocPath(2022, 25));

		// part 1 - "2=0--0---11--01=-100" (<0.001s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// no part 2, Merry Christmas!
	}
}
