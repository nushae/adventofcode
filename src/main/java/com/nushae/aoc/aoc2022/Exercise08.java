package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 8, title = "Treetop Tree House", keywords = {"grid", "line-of-sight"})
public class Exercise08 {

	List<String> lines;
	int[][] treeHeights;
	int width;
	int height;

	public Exercise08() {
		lines = new ArrayList<>();
	}

	public Exercise08(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise08(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		width = lines.get(0).length();
		height = lines.size();
		treeHeights = new int[height][width];
		for (int y = 0; y < lines.size(); y++) {
			String line = lines.get(y);
			for (int x = 0; x < line.length(); x++) {
				treeHeights[y][x] = line.charAt(x)-'0';
			}
		}
	}

	private boolean isVisible(int y, int x) {
		if (x == 0 || y == 0 || x == width-1 || y == height-1) {
			return true;
		}
		boolean visible = true;
		for (int i = y; visible && i < height - 1; i++) {
			visible = treeHeights[i+1][x] < treeHeights[y][x];
		}
		if (!visible) {
			visible = true;
			for (int i = y; visible && i > 0; i--) {
				visible = treeHeights[i - 1][x] < treeHeights[y][x];
			}
			if (!visible) {
				visible = true;
				for (int j = x; visible && j < width - 1; j++) {
					visible = treeHeights[y][j + 1] < treeHeights[y][x];
				}
				if (!visible) {
					visible = true;
					for (int j = x; visible && j > 0; j--) {
						visible = treeHeights[y][j - 1] < treeHeights[y][x];
					}
				}
			}
		}
		return visible;
	}

	private long scenicScore(int y, int x) {
		if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
			return 0;
		}
		long accum = 1;
		long dist = 0;
		for (int i = y; i < height - 1; i++) {
			dist++;
			if (treeHeights[i+1][x] >= treeHeights[y][x]) {
				break;
			}
		}
		accum *= dist;
		dist = 0;
		for (int i = y; i > 0; i--) {
			dist++;
			if (treeHeights[i-1][x] >= treeHeights[y][x]) {
				break;
			}
		}
		accum *= dist;
		dist = 0;
		for (int j = x; j < width - 1; j++) {
			dist++;
			if (treeHeights[y][j+1] >= treeHeights[y][x]) {
				break;
			}
		}
		accum *= dist;
		dist = 0;
		for (int j = x; j > 0; j--) {
			dist++;
			if (treeHeights[y][j-1] >= treeHeights[y][x]) {
				break;
			}
		}
		return accum * dist;
	}

	public long doPart1() {
		long accum = 0;
		for (int y=0; y < height; y++) {
			for (int x =0; x< width; x++) {
				accum += isVisible(y, x) ? 1 : 0;
			}
		}
		return accum;
	}

	public long doPart2() {
		long accum = -1;
		for (int y=0; y < height; y++) {
			for (int x =0; x< width; x++) {
				accum = Math.max(accum, scenicScore(y, x));
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise08 ex = new Exercise08(aocPath(2022, 8));

		// Input processing (0.002s)
		long start = System.currentTimeMillis();
		log.info("Preprocessing:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1792 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 334880 (0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
