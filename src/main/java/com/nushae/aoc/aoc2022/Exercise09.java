package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 9, title="Rope Bridge", keywords = {"grid", "snake-like"})
public class Exercise09 {

	List<String> lines;
	Set<Point> grid;
	Set<Point> grid2;

	public Exercise09() {
		lines = new ArrayList<>();
	}

	public Exercise09(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise09(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void walkOfShame() {
		grid = new HashSet<>();
		grid2 = new HashSet<>();
		Point head = new Point(0,0);
		Point[] tails = new Point[9];
		for (int i=0; i< 9; i++) {
			tails[i] = new Point(0, 0);
		}
		grid.add(tails[0]);
		grid2.add(tails[8]);
		for (String line : lines) {
			String[] parts = line.split(" ");
			for (int i = 0; i < Integer.parseInt(parts[1]); i++) {
				switch (parts[0]) {
					case "R":
						head.x += 1;
						break;
					case "U":
						head.y += 1;
						break;
					case "L":
						head.x -= 1;
						break;
					case "D":
						head.y -= 1;
						break;
					default:
						System.out.println("Don't know this dir!!");
						System.exit(0);
				}
				// adjust tails to head movement:
				tails[0] = update(head, tails[0]);
				for (int t = 1; t < 9; t++) {
					tails[t] = update(tails[t-1], tails[t]);
				}
				grid.add(tails[0]);
				grid2.add(tails[8]);
			}
		}
	}

	public Point update(Point head, Point tail) {
		int diffX = head.x - tail.x;
		int diffY = head.y - tail.y;
		int deltaX = 0;
		int deltaY = 0;
		if (Math.abs(diffX) > 1 || Math.abs(diffY) > 1) {
			deltaX = Math.round(Math.signum(diffX));
			deltaY = Math.round(Math.signum(diffY));
		}
		// THE REAL CULPRIT: RETURN A NEW POINT, DUMBASS
		return new Point(tail.x + deltaX, tail.y + deltaY);
	}

	public long doPart1() {
		walkOfShame();
		return grid.size();
	}

	public long doPart2() {
		walkOfShame();
		return grid2.size();
	}

	public static void main(String[] args) {
		Exercise09 ex = new Exercise09(aocPath(2022, 9));

		// part 1 - 5735 (0.014 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2478 (0.004 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
