package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.StringUtil.union;

@Log4j2
@Aoc(year = 2022, day = 6, title="Tuning Trouble", keywords = {"sliding-window"})
public class Exercise06 {

	List<String> lines;

	public Exercise06() {
		lines = new ArrayList<>();
	}

	public Exercise06(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise06(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long findFirst(int windowsize) {
		String input = lines.get(0);
		String window = input.substring(0, windowsize - 1);
		int i = windowsize - 1;
		boolean allDifferent = false;
		while (!allDifferent) {
			String letter = input.substring(i, i + 1);
			i++;
			window = window+letter;
			if (window.length()>windowsize) {
				window = window.substring(1);
			}
			allDifferent = !(union(window,window).length() < windowsize);
		}
		return i;
	}

	public long doPart1() {
		return findFirst(4);
	}

	public long doPart2() {
		return findFirst(14);
	}

	public static void main(String[] args) {
		Exercise06 ex = new Exercise06(aocPath(2022, 6));

		// part 1 - 1538 (0.026s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2315 (0.013s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
