package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 19, title="Not Enough Minerals", keywords = {"heuristic-search", "pruning"})
public class Exercise19 {

	public record Blueprint (int id, int ore_ore_cost, int clay_ore_cost, int obs_ore_cost, int obs_clay_cost, int geo_ore_cost, int geo_obs_cost) {}

	public record State(int clay, int ore, int obsidian, int geodes, int clayBots, int oreBots, int obsidianBots, int geodeBots, int remainingTime) {
		@Override
		public String toString() {
			return "C:"+clayBots+"O:"+oreBots+"B:"+obsidianBots+"G:"+geodeBots+"@"+"C:"+clay+"O:"+ore+"B:"+obsidian+"G:"+geodes+"@"+remainingTime;
		}
	}

	private static final boolean REPORT = false;
	private static final Pattern BLUEPRINTS = Pattern.compile("Blueprint (\\d+): Each ore robot costs (\\d+) ore. Each clay robot costs (\\d+) ore. Each obsidian robot costs (\\d+) ore and (\\d+) clay. Each geode robot costs (\\d+) ore and (\\d+) obsidian.");

	List<String> lines;
	List<Blueprint> blueprints;

	public Exercise19() {
		lines = new ArrayList<>();
	}

	public Exercise19(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise19(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		blueprints = new ArrayList<>();
		for (String line : lines) {
			Matcher m = BLUEPRINTS.matcher(line);
			if (m.find()) {
				blueprints.add(new Blueprint(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)), Integer.parseInt(m.group(6)), Integer.parseInt(m.group(7))));
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		log.debug(blueprints.size() + " blueprints processed.");
	}

	public long doPart1() {
		long accum = 0;
		State start = new State(0, 0, 0, 0, 0, 1, 0, 0, 24);
		for (Blueprint bp : blueprints) {
			long geodes = search(start, bp.clay_ore_cost, bp.ore_ore_cost, bp.obs_ore_cost, bp.obs_clay_cost, bp.geo_ore_cost, bp.geo_obs_cost);
			log.info((bp.id+1) + ": " + geodes);
			accum += bp.id * geodes;
		}
		return accum;
	}

	public long doPart2() {
		long accum = 1;
		State start = new State(0, 0, 0, 0, 0, 1, 0, 0, 32);
		for (int i = 0 ; i < Math.min(3, blueprints.size()); i++) {
			Blueprint bp = blueprints.get(i);
			long geodes = search(start, bp.clay_ore_cost, bp.ore_ore_cost, bp.obs_ore_cost, bp.obs_clay_cost, bp.geo_ore_cost, bp.geo_obs_cost);
			log.info((i+1) + ": " + geodes);
			accum *= geodes;
		}
		return accum;
	}

	public long search(State start, int clay_ore_cost, int ore_ore_cost, int obs_ore_cost, int obs_clay_cost, int geo_ore_cost, int geo_obs_cost) {
		long result = Long.MIN_VALUE;
		// for pruning: never produce more bots of a type than absolutely necessary
		int maxOreBots = Math.max(Math.max(clay_ore_cost, ore_ore_cost), Math.max(obs_ore_cost, geo_ore_cost));
		int maxClayBots = obs_clay_cost;
		int maxObsBots = geo_obs_cost;

		Deque<State> Q = new ArrayDeque<>();
		Set<State> visited = new HashSet<>();
		Q.add(start);
		while (!Q.isEmpty()) {
			State current = Q.poll();
			result = Math.max(current.geodes, result);

			if (current.remainingTime > 0) { // otherwise, no next moves!
				// more pruning: reduce higher resource amounts than we will ultimately need to that amount to reduce generated states
				// Eg. if we always built the most expensive bot that costs ore that would cost max(ore costs) * remainingTime.
				// We also produce oreBots * (remainingTime - 1) more usable ore (the ore produced in the last minute is wasted)
				int maxOre = current.remainingTime * maxOreBots - current.oreBots * (current.remainingTime - 1);
				int maxClay = current.remainingTime * maxClayBots - current.clayBots * (current.remainingTime - 1);
				int maxObsidian = current.remainingTime * maxObsBots - current.obsidianBots * (current.remainingTime - 1);
				if (!visited.contains(current)) {
					visited.add(current);
					// yet more pruning: once we reach max bots for the first three types, the answer is known
					if (current.clayBots == maxClayBots && current.oreBots == maxOreBots && current.obsidianBots == maxObsBots) { // just geodebots from now on!!
						int t = current.remainingTime;
						// with T remaining time, we add 0 + 1 + 2 + ... + T-1 = T*(T-1)/2 geodes from new bots, and we keep producing with the current ones (T * geodeBots more) plus we may have geodes already
						int extraGeodes = t * (t-1) / 2 + current.geodeBots * t + current.geodes;
						State newMove = new State(current.clay, current.ore, current.obsidian, current.geodes + extraGeodes, current.clayBots, current.oreBots, current.obsidianBots, current.geodeBots + t, 0);
						Q.add(newMove);
					} else {
						if (current.clayBots < maxClayBots && current.ore >= clay_ore_cost) {
							State newMove = new State(
									Math.min(maxClay, current.clay + current.clayBots),
									Math.min(maxOre, current.ore + current.oreBots - clay_ore_cost),
									Math.min(maxObsidian, current.obsidian + current.obsidianBots),
									current.geodes + current.geodeBots,
									current.clayBots + 1,
									current.oreBots,
									current.obsidianBots,
									current.geodeBots,
									current.remainingTime - 1);
							if (!visited.contains(newMove)) {
								Q.add(newMove);
							}
						}
						if (current.oreBots < maxOreBots && current.ore >= ore_ore_cost) {
							State newMove = new State(
									Math.min(maxClay, current.clay + current.clayBots),
									Math.min(maxOre, current.ore + current.oreBots - ore_ore_cost),
									Math.min(maxObsidian, current.obsidian + current.obsidianBots),
									current.geodes + current.geodeBots,
									current.clayBots,
									current.oreBots + 1,
									current.obsidianBots,
									current.geodeBots,
									current.remainingTime - 1);
							if (!visited.contains(newMove)) {
								Q.add(newMove);
							}
						}
						if (current.obsidianBots < maxObsBots && current.clay >= obs_clay_cost && current.ore >= obs_ore_cost) {
							State newMove = new State(
									Math.min(maxClay, current.clay + current.clayBots - obs_clay_cost),
									Math.min(maxOre, current.ore + current.oreBots - obs_ore_cost),
									Math.min(maxObsidian, current.obsidian + current.obsidianBots),
									current.geodes + current.geodeBots,
									current.clayBots,
									current.oreBots,
									current.obsidianBots + 1,
									current.geodeBots,
									current.remainingTime - 1);
							if (!visited.contains(newMove)) {
								Q.add(newMove);
							}
						}
						if (current.ore >= geo_ore_cost && current.obsidian >= geo_obs_cost) {
							State newMove = new State(
									Math.min(maxClay, current.clay + current.clayBots),
									Math.min(maxOre, current.ore + current.oreBots - geo_ore_cost),
									Math.min(maxObsidian, current.obsidian + current.obsidianBots - geo_obs_cost),
									current.geodes + current.geodeBots,
									current.clayBots,
									current.oreBots,
									current.obsidianBots,
									current.geodeBots + 1,
									current.remainingTime - 1);
							if (!visited.contains(newMove)) {
								Q.add(newMove);
							}
						}
						State newMove = new State(Math.min(maxClay, current.clay + current.clayBots), Math.min(maxOre, current.ore + current.oreBots), Math.min(maxObsidian, current.obsidian + current.obsidianBots), current.geodes + current.geodeBots, current.clayBots, current.oreBots, current.obsidianBots, current.geodeBots, current.remainingTime - 1);
						if (!visited.contains(newMove)) {
							Q.add(newMove);
						}
					}
				}
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Exercise19 ex = new Exercise19(aocPath(2022, 19));

		// Process input (0.003s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1144 (4.098s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 19980 (51.913s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
