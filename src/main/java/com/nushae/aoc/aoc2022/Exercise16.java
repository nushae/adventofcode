package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.GraphUtil.findShortestPath;
import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 16, title="Proboscidea Volcanium", keywords = {"graph", "optimal-path"})
public class Exercise16 {

	private static final Pattern VALVES = Pattern.compile("Valve ([a-zA-Z]+) has flow rate=(\\d+); tunnels? leads? to valves? ([a-zA-Z, ]+)");

	List<String> lines;
	Map<String, BasicGraphNode<Integer>> allNodes;
	Map<String, BasicGraphNode<Integer>> justTheCoolNodes;
	Map<String, Integer> distances;
	Map<String, Long> flowsByValvesOpened;
	int LIMIT;

	public Exercise16() {
		lines = new ArrayList<>();
	}

	public Exercise16(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise16(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		allNodes = new HashMap<>();
		allNodes.put("AA", new BasicGraphNode<>("AA", 0));
		for (String line : lines) {
			Matcher m = VALVES.matcher(line);
			if (m.find()) {
				String name = m.group(1);
				int rate = Integer.parseInt(m.group(2));
				BasicGraphNode<Integer> current = allNodes.computeIfAbsent(name, s -> new BasicGraphNode<>(s, rate));
				current.setPayload(rate); // for nodes added as neighbours first
				for (String connectsTo : m.group(3).split(", ")) {
					BasicGraphNode<Integer> child = allNodes.computeIfAbsent(connectsTo, s -> new BasicGraphNode<>(s, null));
					current.addNeighbour(child, 1);
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}

		// now make the REAL graph (done dynamically)
		// we can ignore the valves with flow=0, they are just used to walk to other nodes.
		// So: we make a net of just the valves with flow>0, with distances = shortest route in steps between them.
		// Then a move is either to stay put until the end of the runtime or to move to a reachable unopened valve and open it
		// Presto, Dijkstra is your uncle.

		justTheCoolNodes = new HashMap<>();
		distances = new HashMap<>();
		for (Map.Entry<String, BasicGraphNode<Integer>> entry : allNodes.entrySet()) {
			if (entry.getValue().getPayload() != 0 || "AA".equals(entry.getKey())) {
				BasicGraphNode<Integer> current = justTheCoolNodes.computeIfAbsent(entry.getKey(), k -> new BasicGraphNode<>(k, entry.getValue().getPayload()));
				for (Map.Entry<String, BasicGraphNode<Integer>> entry2 : allNodes.entrySet()) {
					if (!entry.getKey().equals(entry2.getKey()) && (entry2.getValue().getPayload() != 0 || "AA".equals(entry2.getKey()))) {
						int len = findShortestPath(entry.getValue(), n -> n.getName().equals(entry2.getValue().getName())).size() - 1;
						distances.put(entry.getKey() + "-" + entry2.getKey(), len);
						current.addNeighbour(entry2.getValue(), len);
					}
				}
			}
		}

		for (Map.Entry<String, BasicGraphNode<Integer>> entry : justTheCoolNodes.entrySet()) {
			log.debug(entry.getKey() + " connects to: ");
			for (Map.Entry<GraphNode<Integer>, Long> entry2 : entry.getValue().getAdjacentNodes().entrySet()) {
				log.debug("  " + entry2.getKey().getName() + " @ " + distances.get(entry.getKey() + "-" + entry2.getKey().getName()) + " minutes.");
			}
		}
	}

	public List<String> generateOrders(int remainingTime, String currentNode, List<String> remainingNodes, String indent) {
		List<String> result = new ArrayList<>();
		for (String nextNode : remainingNodes) {
			int distance = distances.get(currentNode+"-"+nextNode) + 1;
			if (remainingTime >= distance) {
				result.add(nextNode);
				if (remainingNodes.size() > 1) {
					List<String> newRemainingNodes = new ArrayList<>(remainingNodes);
					newRemainingNodes.remove(nextNode);
					for (String order : generateOrders(remainingTime - distance, nextNode, newRemainingNodes, indent + "  ")) {
						result.add(nextNode + ":" + order);
					}
				}
			}
		}
		return result;
	}

	public long calcPathLength(String[] elements) {
		long totalDistance = distances.get("AA"+"-"+elements[0]) + 1;
		long totalFlow = 0;
		long currentFlow = justTheCoolNodes.get(elements[0]).getPayload();
		for (int i = 1; i< elements.length; i++) {
			long distance = distances.get(elements[i-1]+"-"+elements[i]) + 1;
			totalDistance += distance;
			totalFlow += distance * currentFlow;
			currentFlow += justTheCoolNodes.get(elements[i]).getPayload();
		}
		if (totalDistance <= LIMIT) {
			totalFlow += (LIMIT - totalDistance) * currentFlow;
			return totalFlow;
		} else {
			log.warn("I should never appear on your screen");
			return Long.MIN_VALUE;
		}
	}

	// You know, fuck it, I'll just generate all possible orders.
	public long doPart1() {
		LIMIT = 30;
		log.debug("The only valves with flow > 0 are: " + justTheCoolNodes.keySet().stream().sorted().collect(Collectors.joining(":")));
		List<String> orders = generateOrders(LIMIT, "AA", justTheCoolNodes.keySet().stream().filter(s -> !"AA".equals(s)).sorted().collect(Collectors.toList()), "");
		log.debug(orders.size() + " different viable orders found");
		long accum = Long.MIN_VALUE;
		for (String order : orders) {
			accum = Math.max(accum, calcPathLength(order.split(":")));
		}
		return accum;
	}

	public long doPart2() {
		LIMIT = 26;
		log.debug("The only valves with flow > 0 are: " + justTheCoolNodes.keySet().stream().filter(s -> !"AA".equals(s)).sorted().collect(Collectors.joining(":")));
		List<String> orders = generateOrders(LIMIT, "AA", justTheCoolNodes.keySet().stream().filter(s -> !"AA".equals(s)).sorted().collect(Collectors.toList()), "");
		log.debug(orders.size() + " different viable orders found");
		flowsByValvesOpened = new HashMap<>(); // will contain best flow regardless of order for each combo of opened valves
		for (String order : orders) {
			// make sorted version of order for use as key
			String orderKey = Arrays.stream(order.split(":")).sorted().collect(Collectors.joining(":"));
			flowsByValvesOpened.put(orderKey, Math.max(flowsByValvesOpened.getOrDefault(orderKey, Long.MIN_VALUE), calcPathLength(order.split(":"))));
		}
		log.debug("Orders reduced to " + flowsByValvesOpened.size() + " keys");
		orders = flowsByValvesOpened.keySet().stream().sorted().collect(Collectors.toList()); // make vastly reduced list for calculation

		long accum = Long.MIN_VALUE;
		for (int i = 0; i < orders.size(); i++) {
			String myOrder = orders.get(i);
			long myFlow = flowsByValvesOpened.get(myOrder);
			Set<String> myOrderSet = Arrays.stream(myOrder.split(":")).collect(Collectors.toSet());
			for (int j = i + 1; j < orders.size(); j++) {
				String elephantOrder = orders.get(j);
				Set<String> elOrderSet = Arrays.stream(elephantOrder.split(":")).collect(Collectors.toSet());
				int before = elOrderSet.size();
				elOrderSet.removeAll(myOrderSet);
				if (before == elOrderSet.size()) { //  && before + myOrderSet.size() == justTheCoolNodes.size() - 1) { // precise partition
					long elephantFlow = flowsByValvesOpened.get(elephantOrder);
					accum = Math.max(accum, myFlow + elephantFlow);
				}
			}
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise16 ex = new Exercise16(aocPath(2022, 16));

		// Input processing (0.032s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1947 (0.418s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 2556 (4.91s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
