package com.nushae.aoc.aoc2022.domain;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Map;

import static java.lang.Math.*;

public class RotatingCube extends JPanel {
	int UNITS; // UNITS * UNITS squares on one face

	Color[][][] FACE_MODEL; // will contain the 'pixels' to draw on the faces

	// quite enormous color table for the 150x150 image projections. Add extra colors as desired, just leave
	// the ' ', '.', '#', and digits in for backwards compatibility
	static final Map<Character, Color> COLORS = Map.ofEntries(
			Map.entry(' ', new Color(255, 255, 255)),
			Map.entry('.', new Color(255, 255, 255)),
			Map.entry('#', new Color(0, 0, 0)),
			Map.entry('0', new Color(224, 224, 224)),
			Map.entry('1', new Color(255, 0, 0)),
			Map.entry('2', new Color(255, 160, 0)),
			Map.entry('3', new Color(255, 255, 0)),
			Map.entry('4', new Color(0, 255, 0)),
			Map.entry('5', new Color(0, 0, 255)),
			Map.entry('6', new Color(0, 255, 255)),
			Map.entry('7', new Color(255, 0, 255)),
			Map.entry('8', new Color(255, 192, 192)),
			Map.entry('9', new Color(10, 14, 31)),
			Map.entry('!', new Color(255, 224, 224)),
			Map.entry('"', new Color(224, 224, 255)),
			Map.entry('$', new Color(192, 224, 224)),
			Map.entry('%', new Color(192, 192, 224)),
			Map.entry('&', new Color(192, 224, 255)),
			Map.entry('(', new Color(224, 255, 224)),
			Map.entry(')', new Color(160, 192, 224)),
			Map.entry('*', new Color(128, 192, 224)),
			Map.entry('+', new Color(128, 160, 224)),
			Map.entry(',', new Color(128, 192, 192)),
			Map.entry('-', new Color(96, 192, 224)),
			Map.entry('/', new Color(96, 160, 224)),
			Map.entry(':', new Color(64, 160, 192)),
			Map.entry(';', new Color(32, 160, 192)),
			Map.entry('<', new Color(32, 128, 192)),
			Map.entry('=', new Color(64, 160, 224)),
			Map.entry('>', new Color(224, 255, 255)),
			Map.entry('?', new Color(0, 128, 192)),
			Map.entry('@', new Color(0, 160, 224)),
			Map.entry('A', new Color(0, 160, 192)),
			Map.entry('B', new Color(32, 160, 224)),
			Map.entry('C', new Color(128, 160, 192)),
			Map.entry('D', new Color(96, 160, 192)),
			Map.entry('E', new Color(0, 128, 224)),
			Map.entry('F', new Color(160, 160, 192)),
			Map.entry('G', new Color(160, 192, 192)),
			Map.entry('H', new Color(0, 96, 160)),
			Map.entry('I', new Color(0, 96, 192)),
			Map.entry('J', new Color(160, 224, 224)),
			Map.entry('K', new Color(0, 128, 160)),
			Map.entry('L', new Color(128, 192, 255)),
			Map.entry('M', new Color(160, 192, 255)),
			Map.entry('N', new Color(64, 128, 192)),
			Map.entry('O', new Color(32, 128, 160)),
			Map.entry('P', new Color(64, 192, 224)),
			Map.entry('Q', new Color(192, 224, 192)),
			Map.entry('R', new Color(160, 224, 255)),
			Map.entry('S', new Color(32, 192, 224)),
			Map.entry('T', new Color(128, 224, 255)),
			Map.entry('U', new Color(128, 224, 224)),
			Map.entry('V', new Color(96, 192, 255)),
			Map.entry('W', new Color(96, 128, 160)),
			Map.entry('X', new Color(96, 128, 192)),
			Map.entry('Y', new Color(255, 224, 255)),
			Map.entry('Z', new Color(64, 128, 160)),
			Map.entry('[', new Color(96, 192, 192)),
			Map.entry(']', new Color(32, 96, 160)),
			Map.entry('^', new Color(128, 160, 160)),
			Map.entry('_', new Color(0, 96, 128)),
			Map.entry('`', new Color(128, 128, 160)),
			Map.entry('a', new Color(0, 64, 128)),
			Map.entry('b', new Color(64, 96, 160)),
			Map.entry('c', new Color(64, 96, 128)),
			Map.entry('d', new Color(192, 192, 192)),
			Map.entry('e', new Color(32, 96, 128)),
			Map.entry('f', new Color(0, 64, 160)),
			Map.entry('g', new Color(0, 64, 96)),
			Map.entry('h', new Color(32, 128, 224)),
			Map.entry('i', new Color(96, 96, 160)),
			Map.entry('j', new Color(32, 64, 128)),
			Map.entry('k', new Color(32, 96, 192)),
			Map.entry('l', new Color(96, 96, 96)),
			Map.entry('m', new Color(96, 96, 64)),
			Map.entry('n', new Color(96, 96, 128)),
			Map.entry('o', new Color(64, 96, 96)),
			Map.entry('p', new Color(128, 96, 64)),
			Map.entry('q', new Color(128, 96, 32)),
			Map.entry('r', new Color(128, 64, 32)),
			Map.entry('s', new Color(128, 96, 0)),
			Map.entry('t', new Color(128, 64, 0)),
			Map.entry('u', new Color(32, 96, 96)),
			Map.entry('v', new Color(160, 96, 0)),
			Map.entry('w', new Color(160, 128, 0)),
			Map.entry('x', new Color(64, 64, 64)),
			Map.entry('y', new Color(160, 96, 32)),
			Map.entry('z', new Color(192, 128, 0)),
			Map.entry('{', new Color(160, 128, 32)),
			Map.entry('|', new Color(128, 128, 64)),
			Map.entry('}', new Color(32, 64, 96)),
			Map.entry('~', new Color(64, 64, 96)),
			Map.entry('¢', new Color(96, 64, 64)),
			Map.entry('£', new Color(192, 160, 0)),
			Map.entry('¥', new Color(0, 96, 96)),
			Map.entry('¦', new Color(224, 160, 0)),
			Map.entry('§', new Color(224, 192, 0)),
			Map.entry('©', new Color(96, 128, 32)),
			Map.entry('«', new Color(32, 96, 64)),
			Map.entry('¬', new Color(160, 160, 64)),
			Map.entry('®', new Color(224, 160, 32)),
			Map.entry('±', new Color(192, 192, 32)),
			Map.entry('µ', new Color(128, 160, 64)),
			Map.entry('¶', new Color(32, 64, 64)),
			Map.entry('·', new Color(192, 160, 32)),
			Map.entry('¸', new Color(255, 192, 0)),
			Map.entry('»', new Color(224, 192, 32)),
			Map.entry('À', new Color(128, 128, 96)),
			Map.entry('Á', new Color(64, 96, 64)),
			Map.entry('Â', new Color(96, 128, 96)),
			Map.entry('Ã', new Color(192, 160, 64)),
			Map.entry('Ä', new Color(96, 96, 32)),
			Map.entry('Å', new Color(96, 128, 64)),
			Map.entry('Æ', new Color(128, 128, 32)),
			Map.entry('Ç', new Color(160, 160, 32)),
			Map.entry('È', new Color(255, 224, 32)),
			Map.entry('É', new Color(224, 224, 0)),
			Map.entry('Ê', new Color(255, 255, 224)),
			Map.entry('Ë', new Color(192, 128, 32)),
			Map.entry('Ì', new Color(255, 224, 0)),
			Map.entry('Í', new Color(255, 192, 32)),
			Map.entry('Î', new Color(0, 64, 64)),
			Map.entry('Ï', new Color(96, 64, 0)),
			Map.entry('Ð', new Color(96, 96, 0)),
			Map.entry('Ñ', new Color(192, 192, 0)),
			Map.entry('Ò', new Color(128, 160, 32)),
			Map.entry('Ó', new Color(32, 0, 0)),
			Map.entry('Ô', new Color(32, 32, 0)),
			Map.entry('Õ', new Color(255, 224, 64)),
			Map.entry('Ö', new Color(64, 64, 0)),
			Map.entry('×', new Color(224, 192, 64)),
			Map.entry('Ø', new Color(32, 32, 32)),
			Map.entry('Ù', new Color(128, 128, 0)),
			Map.entry('Ú', new Color(64, 32, 0)),
			Map.entry('Û', new Color(32, 0, 32)),
			Map.entry('Ü', new Color(0, 32, 64)),
			Map.entry('Ý', new Color(32, 0, 64)),
			Map.entry('Þ', new Color(0, 0, 64)),
			Map.entry('ß', new Color(0, 0, 32)),
			Map.entry('à', new Color(224, 224, 32)),
			Map.entry('á', new Color(0, 32, 32)),
			Map.entry('â', new Color(0, 32, 0)),
			Map.entry('ã', new Color(64, 32, 32)),
			Map.entry('ä', new Color(32, 64, 32)),
			Map.entry('å', new Color(64, 0, 32)),
			Map.entry('æ', new Color(64, 128, 64)),
			Map.entry('ç', new Color(64, 0, 0)),
			Map.entry('è', new Color(192, 192, 64)),
			Map.entry('é', new Color(64, 128, 96)),
			Map.entry('ê', new Color(160, 160, 96)),
			Map.entry('ë', new Color(64, 128, 128)),
			Map.entry('ì', new Color(64, 192, 255)),
			Map.entry('í', new Color(128, 128, 128)),
			Map.entry('î', new Color(64, 64, 32)),
			Map.entry('ï', new Color(96, 64, 32)),
			Map.entry('ð', new Color(192, 192, 96)),
			Map.entry('ñ', new Color(96, 128, 128)),
			Map.entry('ò', new Color(160, 160, 0)),
			Map.entry('ó', new Color(128, 160, 128)),
			Map.entry('ô', new Color(128, 192, 96)),
			Map.entry('õ', new Color(96, 160, 160)),
			Map.entry('ö', new Color(0, 160, 160)),
			Map.entry('÷', new Color(32, 160, 160)),
			Map.entry('ø', new Color(64, 192, 192)),
			Map.entry('ù', new Color(64, 160, 255)),
			Map.entry('ú', new Color(128, 96, 96)),
			Map.entry('û', new Color(160, 128, 96)),
			Map.entry('ü', new Color(224, 160, 64)),
			Map.entry('ý', new Color(192, 160, 128)),
			Map.entry('þ', new Color(32, 128, 128)),
			Map.entry('ÿ', new Color(64, 160, 160)),
			Map.entry('Ā', new Color(160, 160, 160)),
			Map.entry('ā', new Color(160, 160, 128)),
			Map.entry('Ă', new Color(192, 160, 96)),
			Map.entry('ă', new Color(32, 192, 255)),
			Map.entry('Ą', new Color(128, 128, 192)),
			Map.entry('ą', new Color(0, 32, 96)),
			Map.entry('Ć', new Color(0, 32, 128)),
			Map.entry('ć', new Color(224, 192, 224)),
			Map.entry('Ĉ', new Color(32, 32, 96)),
			Map.entry('ĉ', new Color(0, 0, 96)),
			Map.entry('Ċ', new Color(160, 128, 128)),
			Map.entry('ċ', new Color(192, 160, 160)),
			Map.entry('Č', new Color(224, 224, 192)),
			Map.entry('č', new Color(32, 32, 64)),
			Map.entry('Ď', new Color(192, 128, 96)),
			Map.entry('ď', new Color(192, 128, 64)),
			Map.entry('Đ', new Color(224, 160, 96)),
			Map.entry('đ', new Color(224, 192, 160)),
			Map.entry('Ē', new Color(0, 64, 192)),
			Map.entry('ē', new Color(160, 192, 160)),
			Map.entry('Ĕ', new Color(64, 32, 96)),
			Map.entry('ĕ', new Color(64, 32, 64)),
			Map.entry('Ė', new Color(32, 64, 0)),
			Map.entry('ė', new Color(64, 64, 128)),
			Map.entry('Ę', new Color(96, 224, 255)),
			Map.entry('ę', new Color(224, 192, 192)),
			Map.entry('Ě', new Color(32, 0, 96)),
			Map.entry('ě', new Color(0, 96, 64)),
			Map.entry('Ĝ', new Color(0, 96, 0)),
			Map.entry('ĝ', new Color(0, 64, 0)),
			Map.entry('Ğ', new Color(96, 0, 32)),
			Map.entry('ğ', new Color(64, 0, 96)),
			Map.entry('Ġ', new Color(64, 0, 128)),
			Map.entry('ġ', new Color(0, 160, 96)),
			Map.entry('Ģ', new Color(0, 128, 0)),
			Map.entry('ģ', new Color(64, 96, 0)),
			Map.entry('Ĥ', new Color(32, 96, 0)),
			Map.entry('ĥ', new Color(64, 0, 64)),
			Map.entry('Ħ', new Color(0, 64, 32)),
			Map.entry('ħ', new Color(0, 96, 32)),
			Map.entry('Ĩ', new Color(64, 128, 0)),
			Map.entry('ĩ', new Color(64, 160, 0)),
			Map.entry('Ī', new Color(96, 0, 64)),
			Map.entry('ī', new Color(96, 128, 0)),
			Map.entry('Ĭ', new Color(96, 32, 0)),
			Map.entry('ĭ', new Color(96, 32, 96)),
			Map.entry('Į', new Color(32, 0, 128)),
			Map.entry('į', new Color(0, 128, 128)),
			Map.entry('İ', new Color(0, 128, 32)),
			Map.entry('ı', new Color(96, 64, 96)),
			Map.entry('Ĳ', new Color(192, 224, 64)),
			Map.entry('ĳ', new Color(64, 128, 32)),
			Map.entry('Ĵ', new Color(64, 96, 32)),
			Map.entry('ĵ', new Color(128, 96, 128)),
			Map.entry('Ķ', new Color(32, 96, 32)),
			Map.entry('ķ', new Color(160, 192, 64)),
			Map.entry('ĸ', new Color(96, 0, 0)),
			Map.entry('Ĺ', new Color(224, 0, 0)),
			Map.entry('ĺ', new Color(192, 0, 0)),
			Map.entry('Ļ', new Color(128, 0, 0)),
			Map.entry('ļ', new Color(160, 0, 0)),
			Map.entry('Ľ', new Color(224, 224, 96)),
			Map.entry('ľ', new Color(255, 255, 96)),
			Map.entry('Ŀ', new Color(192, 160, 192)),
			Map.entry('ŀ', new Color(160, 128, 160)),
			Map.entry('Ł', new Color(224, 128, 96)),
			Map.entry('ł', new Color(224, 64, 32)),
			Map.entry('Ń', new Color(224, 96, 64)),
			Map.entry('ń', new Color(224, 160, 160)),
			Map.entry('Ņ', new Color(224, 96, 96)),
			Map.entry('ņ', new Color(224, 64, 0)),
			Map.entry('Ň', new Color(224, 96, 0)),
			Map.entry('ň', new Color(224, 128, 32)),
			Map.entry('ŉ', new Color(192, 32, 0)),
			Map.entry('Ŋ', new Color(224, 160, 128)),
			Map.entry('ŋ', new Color(224, 32, 0)),
			Map.entry('Ō', new Color(224, 192, 128)),
			Map.entry('ō', new Color(224, 128, 0)),
			Map.entry('Ŏ', new Color(224, 128, 64)),
			Map.entry('ŏ', new Color(192, 64, 0)),
			Map.entry('Ő', new Color(128, 64, 64)),
			Map.entry('ő', new Color(64, 128, 224)),
			Map.entry('Œ', new Color(96, 32, 32)),
			Map.entry('œ', new Color(255, 160, 32)),
			Map.entry('Ŕ', new Color(32, 128, 255)),
			Map.entry('ŕ', new Color(0, 96, 224)),
			Map.entry('Ŗ', new Color(0, 128, 255)),
			Map.entry('ŗ', new Color(0, 160, 255)),
			Map.entry('Ř', new Color(160, 160, 224)),
			Map.entry('ř', new Color(32, 64, 160)),
			Map.entry('Ś', new Color(255, 224, 160)),
			Map.entry('ś', new Color(255, 224, 96)),
			Map.entry('Ŝ', new Color(0, 0, 128)),
			Map.entry('ŝ', new Color(224, 96, 32)),
			Map.entry('Ş', new Color(255, 224, 192))
	);

	// The cube net this is based on is as follows, if you pass a face model it must match this (ie the data
	// you want to appear on face '0' must have index 0 etc)
	//
	// Nodes are annotated with the indexes from the nodes array
	// Faces are annotated with the indexes from the faces array, as well as identified with the rubik's cube face
	//   colors - (R)ed, (G)reen, (Y)ellow, (B)lue, (W)hite, (O)range
	// (The actual colors on the faces come from the symbols in the passed cube model, of course)
	//
	//  ..0RRR1.....
	//  ...R0R......
	//  0.3RRR2..1.0
	//  GGGYYYBBBWWW
	//  G1GY2YB3BW4W
	//  GGGYYYBBBWWW
	//  7.4OOO5..6.7
	//  ...O5O......
	//  ..7OOO6.....

	// -x is towards the left, -y is up, -z is towards the viewer
	double[][] nodes = {{-1, 1, -1}, {-1, 1, 1}, {-1, -1, 1}, {-1, -1, -1}, {1, -1, -1}, {1, -1, 1}, {1, 1, 1}, {1, 1, -1}};

	int[][] faces = {
			// in order: the 'red', 'green', 'yellow', 'blue', 'white', and 'orange' face
			{0, 1, 2, 3}, {0, 3, 4, 7}, {3, 2, 5, 4}, {2, 1, 6, 5}, {1, 0, 7, 6}, {4, 5, 6, 7}
	};

	public RotatingCube() {
		this(5, new char[][][] { // default cube for demo purposes
				{ // 'red' face
					{'1','1','0','1','1'},
					{'1','1','1','1','1'},
					{'4','1','1','1','5'}, // oriented as is compared to the net
					{'1','1','1','1','1'},
					{'1','1','3','1','1'}
				},
				{ // 'green' face
					{'4','4','1','4','4'},
					{'4','4','4','4','4'},
					{'0','4','4','4','3'}, // oriented as is compared to the net
					{'4','4','4','4','4'},
					{'4','4','2','4','4'}
				},
				{ // 'yellow' face
					{'3','3','1','3','3'},
					{'3','3','3','3','3'},
					{'4','3','3','3','5'}, // oriented as is compared to the net
					{'3','3','3','3','3'},
					{'3','3','2','3','3'}
				},
				{ // 'blue' face
					{'5','5','1','5','5'},
					{'5','5','5','5','5'},
					{'3','5','5','5','0'}, // oriented as is compared to the net
					{'5','5','5','5','5'},
					{'5','5','2','5','5'}
				},
				{ // 'white' face
					{'0','0','1','0','0'},
					{'0','0','0','0','0'},
					{'5','0','0','0','4'}, // oriented as is compared to the net
					{'0','0','0','0','0'},
					{'0','0','2','0','0'}
				},
				{ // 'orange' face
					{'2','2','3','2','2'},
					{'2','2','2','2','2'},
					{'4','2','2','2','5'}, // oriented as is compared ot the net
					{'2','2','2','2','2'},
					{'2','2','0','2','2'}
				}
		});
	}

	public RotatingCube(int faceSize, char[][][] faceModel) {
		Color[][][] colorModel = new Color[6][faceModel[0].length][faceModel[0][0].length];
		for (int f = 0; f < 6; f++) {
			for (int y = 0; y < faceModel[0].length; y++) {
				for (int x = 0; x < faceModel[0][0].length; x++) {
					// Uses white as the fallback for unknown characters
					colorModel[f][y][x] = COLORS.getOrDefault(faceModel[f][y][x], Color.white);
				}
			}
		}
		init(faceSize, colorModel);
	}

	public RotatingCube(int faceSize, Color[][][] faceModel) {
		init(faceSize, faceModel);
	}

	private void init(int faceSize, Color[][][] faceModel) {
		UNITS = faceSize;
		FACE_MODEL = faceModel;
		setPreferredSize(new Dimension(900, 900));
		setBackground(Color.white);

		scale(200);
		rotateCube(PI / 4, atan(sqrt(2)));

		new Timer(15, (ActionEvent e) -> {
			rotateCube(PI / 180, 0);
			repaint();
		}).start();
	}

	final void scale(double s) {
		for (double[] node : nodes) {
			node[0] *= s;
			node[1] *= s;
			node[2] *= s;
		}
	}

	final void rotateCube(double angleX, double angleY) {
		double sinX = sin(angleX);
		double cosX = cos(angleX);

		double sinY = sin(angleY);
		double cosY = cos(angleY);

		for (double[] node : nodes) {
			double x = node[0];
			double y = node[1];
			double z = node[2];

			node[0] = x * cosX - z * sinX;
			node[2] = z * cosX + x * sinX;

			z = node[2];

			node[1] = y * cosY - z * sinY;
			node[2] = z * cosY + y * sinY;
		}
	}

	void drawCube(Graphics2D g) {
		g.translate(getWidth() / 2, getHeight() / 2);

		// identify the 'hidden vertex' by its Z coordinate:
		double maxZ = -1000;
		for (double[] node : nodes) {
			maxZ = Math.max(maxZ, node[2]);
		}

		for (int idx = 0; idx < 6; idx++) {
			int[] face = faces[idx];
			double[] xy0 = nodes[face[0]];
			double[] xy1 = nodes[face[1]];
			double[] xy2 = nodes[face[2]];
			double[] xy3 = nodes[face[3]];
			if (!(Math.abs(xy0[2] - maxZ) < 0.05 || Math.abs(xy1[2] - maxZ) < 0.05 || Math.abs(xy2[2] - maxZ) < 0.05 || Math.abs(xy3[2] - maxZ) < 0.05)) {
				// Every point (x, y) needed on the cube face, where xy0 = (0, 0), can be constructed out of
				// xy0 + x*V + y*W where
				// V is the vector pointing from xy0 to xy1 with length 1/UNITS: ((xy1[0]-xy0[0])/UNITS, (xy1[1]-xy0[1])/UNITS)
				// W is the vector pointing from xy0 to xy3 with length 1/UNITS: ((xy3[0]-xy0[0])/UNITS, (xy3[1]-xy0[1])/UNITS)
				double v_x = (xy1[0] - xy0[0])/UNITS;
				double v_y = (xy1[1] - xy0[1])/UNITS;
				double w_x = (xy3[0] - xy0[0])/UNITS;
				double w_y = (xy3[1] - xy0[1])/UNITS;
				for (int y = 0; y < UNITS; y++) {
					for (int x = 0; x < UNITS; x++) {
						g.setColor(FACE_MODEL[idx][y][x]);
						g.fillPolygon(
							new int[] {(int) round(xy0[0] + x*v_x + y*w_x), (int) round(xy0[0] + (x+1)*v_x + y*w_x), (int) round(xy0[0] + (x+1)*v_x + (y+1)*w_x), (int) round(xy0[0] + x*v_x + (y+1)*w_x)},
							new int[] {(int) round(xy0[1] + x*v_y + y*w_y), (int) round(xy0[1] + (x+1)*v_y + y*w_y), (int) round(xy0[1] + (x+1)*v_y + (y+1)*w_y), (int) round(xy0[1] + x*v_y + (y+1)*w_y)},
							4);
					}
				}

				g.setColor(Color.black);
				if (UNITS < 10) { // gridlines, but only if there aren't too many
					for (int u = 1; u < UNITS; u++) {
						// draw lines from [xy0 - xy1 in UNITS steps] to [xy3 - xy2 in UNITS steps]
						g.drawLine((int) round(xy0[0] + u * (xy1[0] - xy0[0]) / UNITS), (int) round(xy0[1] + u * (xy1[1] - xy0[1]) / UNITS), (int) round(xy3[0] + u * (xy2[0] - xy3[0]) / UNITS), (int) round(xy3[1] + u * (xy2[1] - xy3[1]) / UNITS));
						// draw lines from [xy3 - xy0 in UNITS steps] to [xy2 - xy1 in UNITS steps]
						g.drawLine((int) round(xy3[0] + u * (xy0[0] - xy3[0]) / UNITS), (int) round(xy3[1] + u * (xy0[1] - xy3[1]) / UNITS), (int) round(xy2[0] + u * (xy1[0] - xy2[0]) / UNITS), (int) round(xy2[1] + u * (xy1[1] - xy2[1]) / UNITS));
					}
				}
				g.drawLine((int) round(xy0[0]), (int) round(xy0[1]), (int) round(xy1[0]), (int) round(xy1[1]));
				g.drawLine((int) round(xy1[0]), (int) round(xy1[1]), (int) round(xy2[0]), (int) round(xy2[1]));
				g.drawLine((int) round(xy2[0]), (int) round(xy2[1]), (int) round(xy3[0]), (int) round(xy3[1]));
				g.drawLine((int) round(xy3[0]), (int) round(xy3[1]), (int) round(xy0[0]), (int) round(xy0[1]));
			}
		}
	}

	@Override
	public void paintComponent(Graphics gg) {
		super.paintComponent(gg);
		Graphics2D g = (Graphics2D) gg;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		drawCube(g);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame f = new JFrame();
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setTitle("Advent Of Code Day 22 - Rotating Cube");
			f.setResizable(false);
			f.add(new RotatingCube(), BorderLayout.CENTER);
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);
		});
	}
}