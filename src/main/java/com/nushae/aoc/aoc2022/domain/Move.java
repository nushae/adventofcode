package com.nushae.aoc.aoc2022.domain;

public class Move {
	public int amount;
	public int src;
	public int dest;

	public Move(int amount, int src, int dest) {
		this.amount = amount;
		this.src = src;
		this.dest = dest;
	}


	public String toString() {
		return amount + "x " + src + " -> " + dest;
	}
}
