package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Coord3D;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 18, title="Boiling Boulders")
public class Exercise18 {

	List<String> lines;
	List<Coord3D> cubes;
	Set<Coord3D> seen;

	public Exercise18() {
		lines = new ArrayList<>();
	}

	public Exercise18(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise18(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		cubes = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split(",");
			cubes.add(new Coord3D(Long.parseLong(parts[0]), Long.parseLong(parts[1]), Long.parseLong(parts[2])));
		}
	}

	public long doPart1() {
		processInput();
		return calcSurfaceArea(cubes);
	}

	public long calcSurfaceArea(List<Coord3D> cubes) {
		long touchingSides = 0;
		long allSides = cubes.size()*6L;
		for (int c1 = 0; c1 < cubes.size()-1; c1++) {
			Coord3D cube1 = cubes.get(c1);
			for (int c2 = c1+1; c2 < cubes.size(); c2++) {
				Coord3D cube2 = cubes.get(c2);
				touchingSides += cube1.manhattan(cube2) == 1 ? 2 : 0;
			}
		}
		return allSides - touchingSides;
	}

	public long doPart2(long part1Result) {
		processInput();
		seen = new HashSet<>(cubes);
		// droplet bounding box with 1 cube margin all around:
		Coord3D minCorner = new Coord3D(Long.MAX_VALUE, Long.MAX_VALUE,Long.MAX_VALUE);
		Coord3D maxCorner = new Coord3D(Long.MIN_VALUE, Long.MIN_VALUE,Long.MIN_VALUE);
		for (Coord3D c : cubes) {
			minCorner.x = Math.min(minCorner.x, c.x-1);
			maxCorner.x = Math.max(maxCorner.x, c.x+1);
			minCorner.y = Math.min(minCorner.y, c.y-1);
			maxCorner.y = Math.max(maxCorner.y, c.y+1);
			minCorner.z = Math.min(minCorner.z, c.z-1);
			maxCorner.z = Math.max(maxCorner.z, c.z+1);
		}
		log.debug("Bounding box: " + minCorner + " - " + maxCorner + " so " + ((maxCorner.x-minCorner.x + 1) * (maxCorner.y-minCorner.y + 1) * (maxCorner.z-minCorner.z + 1) - cubes.size())+ " cubes to fill at most");
		// fill all outside space within bounding box (any point on the outer shell, like minCorner, is guaranteed to
		// be outside the shape, and since the whole shell is connected we will fill everything):
		boundedFloodFill(minCorner, minCorner, maxCorner);

		// Anything within the bounding box not already in seen will now be the inner cavity
		Set<Coord3D> countMe = new HashSet<>();
		for (long x = minCorner.x+1; x < maxCorner.x; x++) {
			for (long y = minCorner.y+1; y < maxCorner.y; y++) {
				for (long z = minCorner.z+1; z < maxCorner.z; z++) {
					Coord3D c = new Coord3D(x, y, z);
					if (!seen.contains(c)) {
						countMe.add(c);
					}
				}
			}
		}

		log.debug("Inner cavity is " + countMe.size() + " cubes in size");
		// the inner cavity's 'surface area' will precisely be the faces we overcounted:
		return part1Result - calcSurfaceArea(new ArrayList<>(countMe));
	}

	// bounded floodfill: fill all reachable points within the cuboid bounded by minCorner and maxCorner
	void boundedFloodFill(Coord3D origin, Coord3D minCorner, Coord3D maxCorner) {
		List<Coord3D> queue = new ArrayList<>();
		queue.add(origin);
		while (!queue.isEmpty()) {
			Coord3D current = queue.remove(0);
			seen.add(current);
			for (Coord3D delta : Coord3D.ORTHO_NEIGHBOURS) {
				Coord3D adjacent = new Coord3D(current.x + delta.x, current.y + delta.y, current.z + delta.z);
				if (!seen.contains(adjacent) && !queue.contains(adjacent)
					&& minCorner.x <= adjacent.x && minCorner.y <= adjacent.y && minCorner.z <= adjacent.z
					&& maxCorner.x >= adjacent.x && maxCorner.y >= adjacent.y && maxCorner.z >= adjacent.z) {
					queue.add(adjacent);
				}
			}
		}
	}

	public static void main(String[] args) {
		Exercise18 ex = new Exercise18(aocPath(2022, 18));

		// part 1 - 4302 (0.027s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		long result = ex.doPart1();
		log.info(result);
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 (depends on part 1!) - 2492 (0.009s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(result));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
