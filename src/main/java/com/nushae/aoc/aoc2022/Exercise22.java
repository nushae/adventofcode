package com.nushae.aoc.aoc2022;

import com.nushae.aoc.aoc2022.domain.RotatingCube;
import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.ConsoleColors.*;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2022, day = 22, title="Monkey Map", keywords = {"pathing", "cube-net"})
public class Exercise22 {

	public static class MapCell {
		public int x;
		public int y;
		public boolean isWall;
		public MapCell up;
		public Point upFacing; // dir we end up facing after moving up
		public MapCell right;
		public Point rightFacing; // dir we end up facing after moving right
		public MapCell down;
		public Point downFacing; // dir we end up facing after moving down
		public MapCell left;
		public Point leftFacing; // dir we end up facing after moving left
		private int connectMask = 0;
		public char symbol;
		public int faceID; // this is really only used for the base cube (for visualization purposes)

		public MapCell(int x, int y, char c, boolean isWall) {
			this(x, y, c, -1, isWall);
		}

		public MapCell(int x, int y, char c, int faceID, boolean wall) {
			this.x = x;
			this.y = y;
			this.faceID = faceID;
			this.symbol = c;
			isWall = wall;
		}

		/**
		 * Move one step forward facing dir, whether that is a wall or not, and return the new x,y location and facing
		 * if connected; return null, null if not.
		 *
		 * @param dir current facing and direction to move one step in.
		 * @return Point[] two points giving location and facing after the move if connected, or null, null if not
		 */
		public Point[] getNeighbour(Point dir) {
			if (dir.x == -1) {
				if (left != null) {
					return new Point[] {new Point(left.x, left.y), leftFacing};
				}
			} else if (dir.x == 1) {
				if (right != null) {
					return new Point[] {new Point(right.x, right.y), rightFacing};
				}
			} else {
				if (dir.y == -1) {
					if (up != null) {
						return new Point[] {new Point(up.x, up.y), upFacing};
					}
				} else { // so dir.y == 1
					if (down != null) {
						return new Point[] {new Point(down.x, down.y), downFacing};
					}
				}
			}
			return new Point[] {null, null};
		}

		/**
		 * Attempt to move one step in the given dir (this will fail if that location is a wall or not connected),
		 * and if successful return the new x,y location and facing, otherwise return the old location and facing.
		 *
		 * @param dir direction to move one step in.
		 * @return Point[] two points giving location and facing after the (attempted) move
		 */
		public Point[] walkTo(Point dir) {
			if (dir.x == -1) {
				if (left != null && !left.isWall) {
					return new Point[] {new Point(left.x, left.y), leftFacing};
				}
			} else if (dir.x == 1) {
				if (right != null && !right.isWall) {
					return new Point[] {new Point(right.x, right.y), rightFacing};
				}
			} else {
				if (dir.y == -1) {
					if (up != null && !up.isWall) {
						return new Point[] {new Point(up.x, up.y), upFacing};
					}
				} else { // so dir.y == 1
					if (down != null && !down.isWall) {
						return new Point[] {new Point(down.x, down.y), downFacing};
					}
				}
			}
			return new Point[] {new Point(x, y), dir};
		}

		/**
		 * Make an ordinary connection - connect this MapCell's dir port to connectTo's rot90L(2, dir) port;
		 * resulting facing will remain dir. Equivalent to connectWithRotation(dir, connectTo, rot90L(2, dir).
		 * 
		 * @param dir port to connect from
		 * @param connectTo MapCell to connect to
		 */
		public void connect(Point dir, MapCell connectTo) {
			connectFolding(dir, connectTo, rot90L(2, dir));
		}

		/**
		 * Make a folding connection - connect this MapCell's dir port to connectTo's connectDir port;
		 * resulting facing will be the opposite of connectDir.
		 * 
		 * @param dir port to connect from
		 * @param connectTo MapCell to connect to
		 * @param connectDir port of that MapCell to connect to
		 */
		public void connectFolding(Point dir, MapCell connectTo, Point connectDir) {
			if (dir.x == -1) { // connect our LEFT port...
				left = connectTo;
				connectMask |= 8;
				leftFacing = rot90L(2, connectDir); // ... to the opposite of connectDir
			} else if (dir.x == 1) { // connect our RIGHT port...
				right = connectTo;
				connectMask |= 2;
				rightFacing = rot90L(2, connectDir); // ... to the opposite of connectDir
			} else {
				if (dir.y == -1) { // connect our UP port...
					up = connectTo;
					connectMask |= 1;
					upFacing = rot90L(2, connectDir); // ... to the opposite of connectDir
				} else { // connect our DOWN port...
					down = connectTo;
					connectMask |= 4;
					downFacing = rot90L(2, connectDir); // ... to the opposite of connectDir
				}
			}
		}

		/**
		 * Return a char representing in which direction(s), if any, this MapCell has connections to other MapCells.
		 * Eg. if this MapCell is connecte din all four directions, it will be represented as ┼.
		 * If this MapCell is a wall, the char will use heavy lines, otherwise light lines.
		 * 
		 * @return char representing this MapCell's connections
		 */
		public char getRep() {
			return isWall ? CONNECT_CHARS_HEAVY[connectMask] : CONNECT_CHARS_LIGHT[connectMask];
		}
	}

	private static final boolean VERBOSE = false;
	private static final boolean VISUAL = false;
	public static final Pattern INSTRUCTIONS = Pattern.compile("(\\d+|[LR])");

	//   .0.
	//   3?1 -> if ? is connected to the NORTH, idx += 2^0, if ? is connected to the EAST, idx += 2^1, etc
	//   .2.
	final static char[] CONNECT_CHARS_LIGHT = {'▫', '╵', '╶', '└', '╷', '│', '┌', '├', '╴', '┘', '─', '┴', '┐', '┤', '┬', '┼'};
	final static char[] CONNECT_CHARS_HEAVY = {'▪', '╹', '╺', '┗', '╻', '┃', '┏', '┣', '╸', '┛', '━', '┻', '┓', '┫', '┳', '╋'};

	final static String[] FACE_CONSOLE_COLORS = { // sadly, not all rubik's colours are available, so we make do
			WHITE + RED_BACKGROUND_BRIGHT, WHITE + GREEN_BACKGROUND_BRIGHT, WHITE + YELLOW_BACKGROUND_BRIGHT,
			WHITE + BLUE_BACKGROUND_BRIGHT, WHITE + PURPLE_BACKGROUND_BRIGHT, WHITE + CYAN_BACKGROUND_BRIGHT,
			WHITE_BACKGROUND_BRIGHT + RED, WHITE_BACKGROUND_BRIGHT  + GREEN, WHITE_BACKGROUND_BRIGHT + YELLOW,
			WHITE_BACKGROUND_BRIGHT + BLUE, WHITE_BACKGROUND_BRIGHT + PURPLE, WHITE_BACKGROUND_BRIGHT + CYAN};

	//  ...000......   Since we're going to project stuff on a cube, regardless of the net we're handed, we're going
	//  ...000......   to standardize and work off this standard cube layout for visualization purposes, and map the
	//  ...000......   faces of other cube nets to this one.
	//  111222333444   This will be a 2-step process - first during net reading, each face is assigned an
	//  111222333444   essentially random number from 6-11. Then during the edge walk (each time we move from face to
	//  111222333444   face we take the corresponding step on the base cube and retrieve the face number for that face;
	//  ...555......   This way each of the random numbers is replaced with one of the predefined digits 0-5 in this
	//  ...555......   layout. (The face that has the starting location will always be face 0)
	//  ...555......   These digits are then used to color everything, and (later) project stuff onto the faces.

	static final MapCell[] BASE_CUBE = {
			new MapCell(1, 0, ' ', 0, false), // red
			new MapCell(0, 0, ' ', 1, false), // green
			new MapCell(1, 1, ' ', 2, false), // yellow
			new MapCell(2, 1, ' ', 3, false), // blue
			new MapCell(3, 1, ' ', 4, false), // white
			new MapCell(1, 2, ' ', 5, false)  // orange
	};

	static {
		BASE_CUBE[0].connectFolding(NORTH, BASE_CUBE[4], NORTH);
		BASE_CUBE[0].connectFolding(EAST, BASE_CUBE[3], NORTH);
		BASE_CUBE[0].connectFolding(SOUTH, BASE_CUBE[2], NORTH);
		BASE_CUBE[0].connectFolding(WEST, BASE_CUBE[1], NORTH);
		BASE_CUBE[1].connectFolding(NORTH, BASE_CUBE[0], WEST);
		BASE_CUBE[1].connectFolding(EAST, BASE_CUBE[2], WEST);
		BASE_CUBE[1].connectFolding(SOUTH, BASE_CUBE[5], WEST);
		BASE_CUBE[1].connectFolding(WEST, BASE_CUBE[4], EAST);
		BASE_CUBE[2].connectFolding(NORTH, BASE_CUBE[0], SOUTH);
		BASE_CUBE[2].connectFolding(EAST, BASE_CUBE[3], WEST);
		BASE_CUBE[2].connectFolding(SOUTH, BASE_CUBE[5], NORTH);
		BASE_CUBE[2].connectFolding(WEST, BASE_CUBE[1], EAST);
		BASE_CUBE[3].connectFolding(NORTH, BASE_CUBE[0], EAST);
		BASE_CUBE[3].connectFolding(EAST, BASE_CUBE[4], WEST);
		BASE_CUBE[3].connectFolding(SOUTH, BASE_CUBE[5], EAST);
		BASE_CUBE[3].connectFolding(WEST, BASE_CUBE[2], EAST);
		BASE_CUBE[4].connectFolding(NORTH, BASE_CUBE[0], NORTH);
		BASE_CUBE[4].connectFolding(EAST, BASE_CUBE[1], WEST);
		BASE_CUBE[4].connectFolding(SOUTH, BASE_CUBE[5], SOUTH);
		BASE_CUBE[4].connectFolding(WEST, BASE_CUBE[3], EAST);
		BASE_CUBE[5].connectFolding(NORTH, BASE_CUBE[2], SOUTH);
		BASE_CUBE[5].connectFolding(EAST, BASE_CUBE[3], SOUTH);
		BASE_CUBE[5].connectFolding(SOUTH, BASE_CUBE[4], SOUTH);
		BASE_CUBE[5].connectFolding(WEST, BASE_CUBE[1], SOUTH);
	}
	static final Map<Point, Integer> BASE_FACE_INDEXES;
	static {
		Map<Point, Integer> aMap = new HashMap<>();
		for (int i=0; i< 6; i++) {
			aMap.put(new Point(BASE_CUBE[i].x, BASE_CUBE[i].y), i);
		}
		BASE_FACE_INDEXES = Collections.unmodifiableMap(aMap);
	}

	static final Map<Point, MapCell> BASE_MAP;
	static {
		Map<Point, MapCell> aMap = new HashMap<>();
		for (int i=0; i< 6; i++) {
			aMap.put(new Point(BASE_CUBE[i].x, BASE_CUBE[i].y), BASE_CUBE[i]);
		}
		BASE_MAP = Collections.unmodifiableMap(aMap);
	}

	List<String> lines;
	public List<Point> instructions;
	public Map<Point, MapCell> map;
	public Map<Point, Integer> faceIndexes;
	MapCell current;
	MapCell currentBase;
	public Point startLoc;
	public Point facing;
	public Point facingBase;
	char[][][] faceModel;
	int minx, maxx, miny, maxy;
	int faceSize;

	public Exercise22() {
		lines = new ArrayList<>();
	}

	public Exercise22(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise22(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput(boolean part1) {
		instructions = new ArrayList<>();
		map = new HashMap<>();
		faceIndexes = new HashMap<>();
		AtomicInteger maxIndex = new AtomicInteger(6); // faces not yet mapped to the base cube get 6...11
		startLoc = null;
		boolean mapRead = false;
		minx = Integer.MAX_VALUE;
		maxx = Integer.MIN_VALUE;
		miny = Integer.MAX_VALUE;
		maxy = Integer.MIN_VALUE;
		for (int y = 0; y < lines.size(); y++) {
			String line = lines.get(y);
			if ("".equals(line)) {
				mapRead = true;
				continue;
			}
			if (mapRead) { // instruction line
				Matcher m = INSTRUCTIONS.matcher(line);
				while (m.find()) {
					Point p;
					if (m.group(1).equals("L") || m.group(1).equals("R")) {
						p = new Point(0, m.group(1).charAt(0) == 'L' ? -1 : 1);
					} else {
						p = new Point(Integer.parseInt(m.group(1)), 0);
					}
					instructions.add(p);
				}
			} else { // map line
				for (int x = 0; x < line.length(); x++) {
					char c = line.charAt(x);
					if (c != ' ') {
						if (startLoc == null) { // leftmost nonempty cell in top row
							startLoc = new Point(x, y);
						}
						minx = Math.min(minx, x);
						maxx = Math.max(maxx, x);
						miny = Math.min(miny, y);
						maxy = Math.max(maxy, y);
						MapCell mc = new MapCell(x, y, c, c == '#');
						map.put(new Point(x,y), mc);
					}
				}
			}
		}

		assert startLoc != null; // if not, you gave me bad input

		faceSize = (int) Math.sqrt(map.size()/6.0);
		faceIndexes.put(new Point(startLoc.x/faceSize, startLoc.y/faceSize), 6); // > 5 to trigger faceModel copy

		// Make all 'internal' connections, as well as (for part1) 'wraparound' connections for the edges of the net
		for (int x = minx; x <= maxx; x++) {
			for (int y = miny; y <= maxy; y++) {
				Point p = new Point(x,y);
				if (map.containsKey(p)) {
					faceIndexes.computeIfAbsent(new Point(p.x/faceSize, p.y/faceSize), pt -> maxIndex.getAndIncrement());
					for (Point dir : ORTHO_DIRS) {
						Point p1 = sum(p, dir);
						if (map.containsKey(p1) || part1) {
							connectNearest(p, dir);
						}
					}
				}
			}
		}

		// for part 2, connect the edges of the net by folding the cube
		if (!part1) {
			facing = NORTH;
			foldCubeNet();
		}

		if (VERBOSE) {
			log.info(map.size() + " map cells added.");
			log.info(instructions.size() + " instructions added.");
			log.info("startLoc determined at " + startLoc);
			log.info("Input processed.");
			if (!part1) {
				reportCube();
			}
		}

		if (!part1 && VISUAL) {
			SwingUtilities.invokeLater(() -> {
				JFrame f = new JFrame();
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setTitle("Advent Of Code Day 22 - Visualized as a Rotating Cube");
				f.setResizable(false);
				f.add(new RotatingCube(faceSize, faceModel), BorderLayout.CENTER);
				f.pack();
				f.setLocationRelativeTo(null);
				f.setVisible(true);
			});
		}
	}

	// For part 1: connect the MapCell at location p in direction dir. When there is no MapCell there (ie we are on
	// the edge of the net) make a wrap around connection instead. Formally: turn 180 degrees and find the furthest
	// reachable MapCell on the net and connect to that
	public void connectNearest(Point p, Point dir) {
		MapCell mc = map.get(p);
		MapCell nextCell;
		Point next = new Point(p.x+dir.x, p.y+dir.y);
		if (map.containsKey(next)) {
			nextCell = map.get(next);
		} else {
			dir.x = -dir.x;
			dir.y = -dir.y;
			while (map.containsKey(new Point(p.x+dir.x, p.y+dir.y))) {
				p = new Point(p.x+dir.x, p.y+dir.y);
			}
			nextCell = map.get(p);
			dir.x = -dir.x;
			dir.y = -dir.y;
		}
		mc.connect(dir, nextCell);
	}

	/**
	 * <p>For part 2: walk the edge of the net, zipping two yet-unattached faces together whenever possible.
	 * Only the corners of the faces are 'interesting' so we move in steps of N or N-1, depending, where N is the size
	 * of a face.</p>
	 *
	 * <p>There are only 6 possible corner layouts we can encounter (on the net):</p>
	 * <ul>
	 *   <li>either an "outside corner" - the next location in the direction we are facing is not connected (ie an
	 *       empty space in the net), nor is our left side;</li>
	 *   <li>two types of "straight" -
	 *     <ol type="a">
	 *       <li>the next location Y is connected, but Y is not connected towards our left</li>
	 *       <li>the next location ahead is not connected but to our left we are connected to Y;</li>
	 *     </ol></li>
	 *   <li>three types of "inside corner" -
	 *     <ol type="a">
	 *       <li>the next location Y is connected AND Y is connected toward the left to a 3rd location Z</li>
	 *       <li>we're connected to the left to Y and ahead to Z</li>
	 *       <li>we're connected to the left to Y, and it is connected forward to Z</li>
	 *     </ol></li>
	 * </ul>
	 *
	 * <p>(The above directions are relative to our facing, NOT the respective MapCells)</p>
	 *
	 * <p>Visually - ("." is an empty or unconnected space, and we are in X, facing right):</p>
	 * <table border="0">
	 *   <tr>
	 *     <td><pre>.. <br/>X. </pre></td>
	 *     <td>"outside corner"</td>
	 *     <td>Turn 90 degrees right and continue in the new direction until the end<br/>
	 *         of the face is reached (ie take only N-1 steps)</td>
	 *   </tr>
	 *   <tr>
	 *     <td><pre>.. <br/>XY </pre></td>
	 *     <td>"straight (a)"</td>
	 *     <td>Continue moving in the same direction, onto the new face, until its<br/>
	 *         other end is reached (ie take N steps)</td>
	 *   </tr>
	 *   <tr>
	 *     <td><pre>Y. <br/>X. </pre></td>
	 *     <td>"straight (b)"</td>
	 *     <td>Treat as an outside corner - turn right and take N-1 steps ahead</td>
	 *   </tr>
	 *   <tr>
	 *     <td><pre>.Z <br/>XY </pre></td>
	 *     <td>"inside corner (a)"</td>
	 *     <td>Move 1 step ahead onto Y and turn left. (This way it will become<br/>
	 *         an inside corner (b), see there</td>
	 *   </tr>
	 *   <tr>
	 *     <td><pre>Y. <br/>XZ </pre></td>
	 *     <td>"inside corner (b)"</td>
	 *     <td>First, if Y and Z are also connected we are done, and can exit<br/>
	 *         the loop.<br/>
	 *         Otherwise, we zip - connect the side of Y corresponding with our<br/>
	 *         "ahead" (so right in the picture) to the side of Z corresponding<br/>
	 *         with our "left" (up in the picture). Same for the N-1 locations to<br/>
	 *         the 'left' of Y (from our perspective, so above Y) and 'behind' Z<br/>
	 *         (from our perspective, so to the right of Z).<br/>
	 *         Lastly, move N steps straight ahead (ie take N steps towards Z).</td>
	 *   </tr>
	 *   <tr>
	 *       <td><pre>YZ <br/>X. </pre></td>
	 *       <td>"inside corner (c)"</td>
	 *       <td>Turn left, move 1 forward and take a U-turn. This way it will<br/>
	 *           become an inside corner (b), see there</td>
	 *   </tr>
	 * </table>
	 *
	 * Using these walking and connecting rules, the entire net will be zipped.
	 */
	public void foldCubeNet() {
		log.debug("Face size: " + faceSize);
		faceModel = new char[6][faceSize][faceSize];
		current = map.get(new Point(startLoc.x, startLoc.y));
		facing = NORTH;
		// for parallel movement on the base cube:
		currentBase = BASE_CUBE[0];
		facingBase = NORTH;
		log.debug("Walking the edge to fold the cube...");
		while (true) {
			int faceIndex = faceIndexes.get(new Point(current.x/faceSize, current.y/faceSize));
			if (faceIndex > 5) { // new face reached; set proper index value and calculate corners that correspond to 'upper left' and 'lower right' corners of this face on the base cube
				log.debug(FACE_CONSOLE_COLORS[faceIndex] + "(" + current.x + ", " + current.y + ") is on an unmapped face..." + RESET);
				faceIndex = BASE_FACE_INDEXES.get(new Point(currentBase.x, currentBase.y));
				log.debug(FACE_CONSOLE_COLORS[faceIndex] + "Mapping this face to " + faceIndex + " on the base cube." + RESET);
				faceIndexes.put(new Point(current.x/faceSize, current.y/faceSize), faceIndex);
				log.debug("Facing on the face: " + DIR_TO_CHAR.get(facing) + ". Facing on the base cube face: " + DIR_TO_CHAR.get(facingBase));
				int rotNeeded = 0;
				while (DIR_TO_CHAR.get(facing) != DIR_TO_CHAR.get(rot90L(rotNeeded, facingBase))) {
					rotNeeded++;
				}
				log.debug((rotNeeded == 0 ? "No" : rotNeeded) + " 90-left-rotation" + (rotNeeded != 1 ? "s" : "") + " of the base face needed to have it matching the face.");
				Point UPPER_LEFT = new Point(current.x/faceSize * faceSize, current.y/faceSize * faceSize);
				copyFaceModel(faceIndex, UPPER_LEFT, rotNeeded);
			}
			// Draw net on screen for reference:
			if (VERBOSE) reportCube();
			log.debug(FACE_CONSOLE_COLORS[faceIndex] + "Currently on (" + current.x + ", " + current.y + ") which is on face " + faceIndex + " facing '" + DIR_TO_CHAR.get(facing) + "'" + RESET);
			Point[] ahead = current.getNeighbour(facing);
			Point[] left = current.getNeighbour(rot90L(1, facing));
			Point[] aheadBase = currentBase.getNeighbour(facingBase);
			Point[] leftBase = currentBase.getNeighbour(rot90L(1, facingBase));
			if (ahead[0] == null && left[0] == null) {
				log.debug("This is an outside corner. Turn right and take " + (faceSize-1) + " steps forward.");
				facing = rot90L(3, facing);
				forward(faceSize - 1);
				facingBase = rot90L(3, facingBase);
			} else if (ahead[0] == null && map.get(left[0]).getNeighbour(rot90L(3, left[1]))[0] != null) { // inside corner type-C
				log.debug("This is an inside corner (type-C). Rotate left, step 1 forward and make a U-turn.");
				current = map.get(left[0]);
				facing = rot90L(2, left[1]);
				currentBase = BASE_MAP.get(leftBase[0]);
				facingBase = rot90L(2, leftBase[1]);
			} else if (ahead[0] == null) { // straight type-B
				log.debug("This is a straight (type-B). Turn right and take " + (faceSize-1) + " steps forward.");
				facing = rot90L(3, facing);
				forward(faceSize - 1);
				facingBase = rot90L(3, facingBase);
			} else if (left[0] != null) { // inside type-B, check if ahead and left are also connected to each other!
				if (map.get(left[0]).getNeighbour(rot90L(3, left[1]))[0] != null) { // DONE!
					log.debug("This is an inside corner (type-B) but ahead and left are already connected, so done.");
					break;
				}
				log.debug("This is an inside corner (type-B). Zip the two adjacent faces, then take " + faceSize + " steps forward.");
				log.debug("Ahead is " + ahead[0] + " and there I would be facing '" + DIR_TO_CHAR.get(ahead[1]) + "' so connecting its '" + DIR_TO_CHAR.get(rot90L(1, ahead[1])) + "' port.");
				log.debug("On my left is " + left[0] + " and there I would be facing " + DIR_TO_CHAR.get(left[1]) + "' so connecting its '" + DIR_TO_CHAR.get(rot90L(3, left[1])) + "' port.");
				// ZIPPITY
				for (int i=0; i < faceSize; i++) {
					MapCell zipLeft = map.get(left[0]);
					MapCell zipAhead = map.get(ahead[0]);
					log.debug("Zipping (" + zipLeft.x + ", " + zipLeft.y + ")" + DIR_TO_CHAR.get(rot90L(3, left[1])) + " to (" + zipAhead.x + ", " + zipAhead.y + ")" + DIR_TO_CHAR.get(rot90L(1, ahead[1])) + ".");
					zipLeft.connectFolding(rot90L(3, left[1]), zipAhead, rot90L(1, ahead[1]));
					zipAhead.connectFolding(rot90L(1, ahead[1]), zipLeft, rot90L(3, left[1]));
					left = zipLeft.getNeighbour(left[1]);
					ahead = zipAhead.getNeighbour(ahead[1]);
				}
				forward(faceSize);
				currentBase = BASE_MAP.get(aheadBase[0]);
				facingBase = aheadBase[1];
			} else if (map.get(ahead[0]).getNeighbour(rot90L(1, ahead[1]))[0] != null) { // inside corner type-A
				log.debug("This is an inside corner (type-A). Take 1 step forward and turn left.");
				forward(1);
				facing = rot90L(1, facing);
				currentBase = BASE_MAP.get(aheadBase[0]);
				facingBase = rot90L(1, aheadBase[1]);
			} else { // ahead != null and to our left == null and front-left == null: straight type-A
				log.debug("This is a straight (type-A). Take " + faceSize + " steps forward.");
				forward(faceSize);
				currentBase = BASE_MAP.get(aheadBase[0]);
				facingBase = aheadBase[1];
			}
		}
	}

	private void forward(int N) {
		for (int i = 0; i < N; i++) {
			Point[] result = current.getNeighbour(facing);
			if (result[0] != null) {
				current = map.get(result[0]);
				facing = result[1];
			}
		}
	}

	private void copyFaceModel(int face, Point UPPER_LEFT, int rotations) {
		float faceCenterX = UPPER_LEFT.x + (faceSize-1)/2.0f;
		float faceCenterY = UPPER_LEFT.y + (faceSize-1)/2.0f;
		for (int faceX = UPPER_LEFT.x; faceX < UPPER_LEFT.x + faceSize; faceX++) {
			for (int faceY = UPPER_LEFT.y; faceY < UPPER_LEFT.y + faceSize; faceY++) {
				if (rotations > 0) {
					// rotate left around the center of the face, rotations times
					float displacementX = faceX - faceCenterX;
					float displacementY = faceY - faceCenterY;
					for (int i = 0; i < rotations; i++) {
						float tmp = displacementX;
						displacementX = displacementY;
						displacementY = -tmp;
					}
					int actualX = Math.round(faceCenterX + displacementX);
					int actualY = Math.round(faceCenterY + displacementY);
					faceModel[face][faceY-UPPER_LEFT.y][faceX-UPPER_LEFT.x] = map.get(new Point(actualX, actualY)).symbol;
				} else {
					faceModel[face][faceY-UPPER_LEFT.y][faceX-UPPER_LEFT.x] = map.get(new Point(faceX, faceY)).symbol;
				}
			}
		}
	}

	private void reportCube() {
		System.out.println();
		for (int y = miny; y <= maxy; y++) {
			for (int x = minx; x <= maxx; x++) {
				Point p = new Point(x, y);
				if (map.containsKey(p)) {
					System.out.print(FACE_CONSOLE_COLORS[faceIndexes.get(new Point(p.x/faceSize, p.y/faceSize))]);
					if (p.x == current.x && p.y == current.y) {
						System.out.print(DIR_TO_CHAR.get(facing));
					} else if (map.get(p).getRep() == '┼' || map.get(p).getRep() == '╋') {
						int idx = faceIndexes.get(new Point(p.x/faceSize, p.y/faceSize));
						System.out.print((char) (idx < 6 ? ('0' + idx) : ('A' + idx % 6)));
					} else {
						System.out.print(map.get(p).getRep());
					}
				} else {
					System.out.print(BLACK + WHITE_BACKGROUND_BRIGHT);
					System.out.print(CONNECT_CHARS_LIGHT[0]);
				}
			}
			System.out.println(BLACK + WHITE_BACKGROUND_BRIGHT);
		}
	}

	public long runMap(boolean part1) {
		processInput(part1);
		current = map.get(startLoc);
		facing = EAST;
		for (Point inst : instructions) {
			execute(inst);
		}
		return 1000L*(current.y+1) + 4L*(current.x+1) + (facing.y == 1 ? 1 : (facing.y == -1 ? 3 : (facing.x == -1 ? 2 : 0)));
	}

	// split off to make it testable
	public void execute(Point inst) {
		// move
		for (int i = 0; i < inst.x; i++) {
			Point[] result = current.walkTo(facing);
			current = map.get(result[0]);
			facing = result[1];
		}
		// rotate
		facing = (inst.y == -1 ? rot90L(1, facing) : (inst.y == 1 ? rot90R(1, facing) : facing));
	}

	public static void main(String[] args) {
		// The original assignment for day 22:
		Exercise22 ex = new Exercise22(aocPath(2022, 22));

		// Or, have some fun, set VISUAL to true, and try one of these (part 1/2 output can be ignored):
		// Exercise22 ex = new Exercise22(aocPath(2022, 22, "input_demo_sogeti.txt"));
		// Exercise22 ex = new Exercise22(aocPath(2022, 22, "input_demo_sogeti_small.txt"));
		// Exercise22 ex = new Exercise22(aocPath(2022, 22, "input_3d_cube_A.txt"));
		// Exercise22 ex = new Exercise22(aocPath(2022, 22, "input_3d_cube_H.txt"));
		// Exercise22 ex = new Exercise22(aocPath(2022, 22, "input_orientation_check.txt"));

		// part 1 73346 (0.04s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.runMap(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 106392 (0.16s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.runMap(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
