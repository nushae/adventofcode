package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 21, title="Monkey Math", keywords = {"expression-tree", "substitution"})
public class Exercise21 {

	public static class Calculation {
		public final String name;
		@Setter
		public Long value;
		@Setter
		public Calculation leftOp;
		@Setter
		public Calculation rightOp;
		@Setter
		public String operator;

		public Calculation(String name, Long value, Calculation leftOp, Calculation rightOp, String operator) {
			this.name = name;
			this.value = value;
			this.leftOp = leftOp;
			this.rightOp = rightOp;
			this.operator = operator;
		}

		public Calculation(String name, Long value) {
			this(name, value, null, null, null);
		}

		public Calculation(String name) {
			this(name, null);
		}

		/**
		 * Return the value of this node, if recalc == true recalculate it, otherwise calculate it only for the first request.
		 *
		 * @param recalc recalculate this node's value iff true
		 * @return the value of this node
		 */
		public Long getValue(boolean recalc) {
			if (value == null || leftOp != null && rightOp != null && recalc) {
				log.debug(name + " value is null, calculating...");
				Long leftVal = leftOp != null ? leftOp.getValue(recalc) : null;
				log.debug(name + " left operand " + (leftOp == null ? " is null (?!)" : leftOp.name + " has value " + leftVal));
				Long rightVal = rightOp != null ? rightOp.getValue(recalc) : null;
				log.debug(name + " right operand " + (rightOp == null ? " is null (?!)" : rightOp.name + " has value " + rightVal));
				if (leftVal != null && rightVal != null) {
					log.debug(name + " both operands now not null.");
					switch (operator) {
						case "+":
							value = leftVal + rightVal;
							break;
						case "-":
							value = leftVal - rightVal;
							break;
						case "/":
							value = leftVal / rightVal;
							break;
						case "*":
							value = leftVal * rightVal;
							break;
						default:
							System.out.println("HUHUHUHUHUHHHHHH?!");
					}
				}
			}
			log.debug(name + " has value " + value);
			return value;
		}

		/**
		 * <p>Given a desiredValue for this Calculation, and the name of a (sub-) Calculation:</p>
		 * <ul>
		 *     <li>if this Calculation is not the named Calculation and it is an expression: figure out what value
		 *         the sub-Calculation containing the named Calculation must have in order to yield the requested
		 *         desiredValue, and request that value from the sub-Calculation.</li>
		 *     <li>if this Calculation is the named Calculation (and a value): set its value to desiredValue.</li>
		 *     <li>if this Calculation is either not the named Calculation and a value, or the named Calculation
		 *         and an expression: this should never occur, the result is unspecified</li>
		 * </ul>
		 *
		 * @param name the name of the node whose value must change
		 * @param desiredValue the expected new value for this Calculation
		 */
		public void fixValueForSubtree(String name, long desiredValue) {
			if (this.name.equals(name)) {
				log.debug("I am the value node with name = '" + name + "' so I'm setting my value to " + desiredValue);
				this.value = desiredValue;
			} else if (leftOp != null && rightOp != null) {
				long leftVal = leftOp.getValue(false);
				long rightVal = rightOp.getValue(false);
				boolean leftHasHuman = leftOp.containsNamedNode(name);
				log.debug("I am an expression node with value = " + leftVal + operator + rightVal + " = " + value + ". " + (leftHasHuman ? "Left" : "Right") + " has the node with name '"+name+"', so ");
				switch (operator) {
					case "+":
						if (leftHasHuman) {
							log.debug("  it must become " + desiredValue + " - " + rightVal + " = " + (desiredValue - rightVal));
							leftOp.fixValueForSubtree(name, desiredValue - rightVal);
						} else {
							log.debug("  it must become " + desiredValue + " - " + leftVal + " = " + (desiredValue - leftVal));
							rightOp.fixValueForSubtree(name, desiredValue - leftVal);
						}
						break;
					case "-":
						if (leftHasHuman) {
							log.debug("  it must become " + rightVal + " + " + desiredValue + " = " + (rightVal + desiredValue));
							leftOp.fixValueForSubtree(name, rightVal + desiredValue);
						} else {
							log.debug("  it must become " + leftVal + " - " + desiredValue + " = " + (leftVal - desiredValue));
							rightOp.fixValueForSubtree(name, leftVal - desiredValue);
						}
						break;
					case "*":
						if (leftHasHuman) {
							log.debug("  it must become " + desiredValue + " / " + rightVal + " = " + (desiredValue / rightVal));
							leftOp.fixValueForSubtree(name, desiredValue / rightVal);
						} else {
							log.debug("  it must become " + desiredValue + " / " + leftVal + " = " + (desiredValue / leftVal));
							rightOp.fixValueForSubtree(name, desiredValue / leftVal);
						}
						break;
					case "/":
						if (leftHasHuman) {
							log.debug("  it must become " + desiredValue + " * " + rightVal + " = " + (desiredValue * rightVal));
							leftOp.fixValueForSubtree(name, desiredValue * rightOp.getValue(false));
						} else {
							log.debug("  it must become " + leftVal + " / " + desiredValue + " = " + (leftVal / desiredValue));
							rightOp.fixValueForSubtree(name, leftVal / desiredValue);
						}
						break;
					default:
						log.warn("HUHUHUHUHUHHHHHH?!");
				}
			}
		}

		public boolean containsNamedNode(String name) {
			if (leftOp == null || rightOp == null) { // assume value node
				return this.name.equals(name);
			}
			return leftOp.containsNamedNode(name) || rightOp.containsNamedNode(name);
		}

		@Override
		public String toString() {
			return "[" + name + "=" + (value != null ? value : leftOp + operator + rightOp) + "]";
		}

		public void showStructure(String indent) {
			String prefix = indent + name + " = ";
			if (value != null) {
				log.debug(prefix + value);
			} else {
				log.debug(prefix);
				if (leftOp != null) {
					leftOp.showStructure(indent + "  ");
				} else {
					log.debug(indent + "  " + null);
				}
				log.debug(indent + "  " + operator);
				if (rightOp != null) {
					rightOp.showStructure(indent + "  ");
				} else {
					log.debug(indent + "  " + null);
				}
			}
		}
	}

	List<String> lines;
	Map<String, Calculation> numbers;

	public Exercise21() {
		lines = new ArrayList<>();
	}

	public Exercise21(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise21(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		numbers = new HashMap<>();
		for (String line : lines) {
			String[] parts = line.split(" ");
			String name = parts[0].substring(0, parts[0].length()-1); // cut off ":"
			Calculation current = numbers.computeIfAbsent(name, Calculation::new);
			if (parts.length > 2) { // composite value
				current.setOperator(parts[2]);
				Calculation left = numbers.computeIfAbsent(parts[1], Calculation::new);
				Calculation right = numbers.computeIfAbsent(parts[3], Calculation::new);
				current.setLeftOp(left);
				current.setRightOp(right);
			} else {
				current.setValue(Long.parseLong(parts[1]));
			}
			numbers.put(name, current);
		}
		log.info("Input processed - " + numbers.size() + " monkeys read.");
	}

	public long doPart1() {
		return numbers.get("root").getValue(true);
	}

	public long doPart2() {
		Calculation root = numbers.get("root");
		root.getValue(true);
		if (root.leftOp.containsNamedNode("humn")) {
			log.debug("Balancing left side to " + root.rightOp.getValue(false));
			root.leftOp.fixValueForSubtree("humn", root.rightOp.getValue(false));
		} else {
			log.debug("Balancing right side to " + root.leftOp.getValue(false));
			root.rightOp.fixValueForSubtree("humn", root.leftOp.getValue(false));
		}
		return numbers.get("humn").getValue(false);
	}

	// Older less sophisticated solution
	public long doPart2Old(long limit) {
		Calculation left = numbers.get("root").leftOp;
		Calculation right = numbers.get("root").rightOp;
		Calculation itsaMe = numbers.get("humn");

		boolean flip;
		long leftVal = left.getValue(true);
		long rightVal = right.getValue(true);
		long diff = rightVal - leftVal;
		// automatically find half that has humn in it
		// from research I know that multiple consecutive values of humn will yield the same result (due to / being integer div I guess)
		int times = 0;
		while (true) {
			itsaMe.setValue(itsaMe.getValue(false)+1);
			times++;
			long newLeftVal = left.getValue(true);
			long newRightVal = right.getValue(true);
			long diff2 = newRightVal - newLeftVal;
			if (diff != diff2) { // finally a value change
				if (leftVal == newLeftVal) { // humn in right side, swap left and right
					log.debug("Swapping left and right subtree so 'humn' appears in left subtree");
					left = numbers.get("root").rightOp;
					right = numbers.get("root").leftOp;
				}
				// This is needed to make the example work... thank god I didn't test part 2 with the example -_-
				flip = Math.signum(diff2-diff) < 0;
				if (flip) log.debug("Negative trend detected, flipping logic for binsearch.");
				break;
			}
		}
		itsaMe.setValue(itsaMe.getValue(false) - times); // reset value
		rightVal = right.getValue(true);

		long low = 0;
		long high = limit;
		long mid = (low + high) / 2;
		while (low < high) {
			mid = (low + high) / 2;
			itsaMe.setValue(mid);
			leftVal = left.getValue(true);
			diff = rightVal - leftVal;
			log.debug(low + " <" + mid + "> " + high + " -> " + diff);
			if (!flip && diff < 0 || flip && diff > 0) {
				low = mid;
			} else if (!flip && diff > 0 || flip && diff < 0) {
				high = mid;
			} else {
				break;
			}
		}
		log.debug(" ---------> " + mid);
		// multiple values can yield the same equality (!), assume lowest is expected
		while (true) {
			itsaMe.setValue(itsaMe.getValue(false) - 1);
			long next = left.getValue(true);
			if (rightVal != next) {
				break;
			} else {
				mid--;
			}
		}
		log.debug(" ---------> " + mid);
		return mid;
	}

	public static void main(String[] args) {
		Exercise21 ex = new Exercise21(aocPath(2022, 21));

		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 142707821472432 (0.006 sec)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - improved solution with recursive expression adjustment - 3587647562851 (0.001 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - original solution with binsearch and correction loop - 3587647562851 (0.003 sec)
		start = System.currentTimeMillis();
		log.info("Part 2, old way:");
		log.info(ex.doPart2Old(200000000000000L));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
