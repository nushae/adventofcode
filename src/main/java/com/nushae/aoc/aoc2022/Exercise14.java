package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 14, title="Regolith Reservoir", keywords = {"grid", "fluid-simulation"})
public class Exercise14 {

	List<String> lines;
	Set<Point> rocks;
	int minY;
	int maxY;
	int minX;
	int maxX;

	public Exercise14() {
		lines = new ArrayList<>();
	}

	public Exercise14(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise14(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		rocks = new HashSet<>();
		minX = Integer.MAX_VALUE;
		maxX = Integer.MIN_VALUE;
		minY = Integer.MAX_VALUE;
		maxY = Integer.MIN_VALUE;
		for (String line : lines) {
			String[] coords = line.split(" -> ");
			for (int i = 1; i < coords.length; i++) {
				String[] parts = coords[i].split(",");
				Point current = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
				parts = coords[i-1].split(",");
				Point prev = new Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
				minX = Math.min(minX, Math.min(current.x, prev.x));
				maxX = Math.max(maxX, Math.max(current.x, prev.x));
				minY = Math.min(minY, Math.min(current.y, prev.y));
				maxY = Math.max(maxY, Math.max(current.y, prev.y));
				if (current.x == prev.x) { // horizontal line
					for (int dy = Math.min(current.y, prev.y); dy <= Math.max(current.y, prev.y); dy++) {
						rocks.add(new Point(current.x, dy));
					}
				} else { // vertical line
					for (int dx = Math.min(current.x, prev.x); dx <= Math.max(current.x, prev.x); dx++) {
						rocks.add(new Point(dx, current.y));
					}
				}
			}
		}
		log.debug("X-extent: " + minX + "-" + maxX);
		log.debug("Y-extent: " + minY + "-" + maxY);
		for (int y = minY-2; y < maxY+2; y++) {
			StringBuilder sb = new StringBuilder();
			for (int x = minX-2; x < maxX+2; x++) {
				sb.append(rocks.contains(new Point(x,y)) ? "#" : ".");
			}
			log.debug(sb.toString());
		}
	}

	public long simulateSand(boolean part1) {
		processInput(); // to reset
		long startSize = rocks.size();
		do {
			Point current = new Point(500,0);
			if (!part1 && rocks.contains(current)) {
				return rocks.size() - startSize;
			}
			do {
				if (current.y > maxY) {
					if (part1) {
						return rocks.size() - startSize;
					} else {
						rocks.add(current);
						break;
					}
				}
				if (!rocks.contains(new Point(current.x, current.y+1))) { // straight down
					current = new Point(current.x, current.y + 1);
				} else if (!rocks.contains(new Point(current.x-1, current.y+1))) { // diagonal left
					current = new Point(current.x - 1, current.y + 1);
				} else if (!rocks.contains(new Point(current.x+1, current.y+1))) { // diagonal right
					current = new Point(current.x + 1, current.y + 1);
				} else { // come to rest
					rocks.add(current);
					break;
				}
			} while (true);
		} while (true);
	}

	public static void main(String[] args) {
		Exercise14 ex = new Exercise14(aocPath(2022, 14));

		// part 1 - 614 (0.02s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.simulateSand(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 26170 (0.14s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.simulateSand(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
