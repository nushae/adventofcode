package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 2, title="Rock Paper Scissors", keywords = {"mapping", "modulo"})
public class Exercise02 {

	List<String> lines;

	public Exercise02() {
		lines = new ArrayList<>();
	}

	public Exercise02(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise02(String data) {
		this();
		loadInputFromData(lines, data);
	}

	// Some analysis.
	// Rock / Paper / Scissors is a cyclic game, which becomes apparent when you draw a table of outcomes:
	//
	//       them
	//       +---+---+---+                  The outcomes form diagonal stripes (an extra copy of the table
	//         R   P   S   r   p   s        was appended to the right to make this extra clear)
	// m +   +---+---+---+---+---+---+
	// e | R | 1 | 0 | 2 | 1 | 0 | 2 |	    This is a good indicator that the outcome can directly be calculated
	//   +   +---+---+---+---+---+---+      from the outputs through a simple linear function with mod 3 applied
	//   | P | 2 | 1 | 0 | 2 | 1 | 0 |
	//   +   +---+---+---+---+---+---+      With a bit of trial and error you can quickly find the function.
	//   | S | 0 | 2 | 1 | 0 | 2 | 1 |
	//   +   +---+---+---+---+---+---+
	//
	// For example, outcome(R,R) = outcome(P,P) = outcome(S,S) = 1. If we use R=0, P=1, and S=2, note that
	// 0-0 = 1-1 = 2-2 = 0 % 3; one off!
	// This suggests either the function outcome = (theirs - mine + 1) mod 3, or outcome = (1 - (theirs - mine)) % 3
	//
	// Indeed, outcome(R,P) = outcome(P,S) = outcome(S,R) = 0, while 1-0 = 2-1 = 0-2 = 1 % 3, and
	// outcome(R,S) = outcome(P,R) = outcome(S,P) = 2, while 2-0 = 0-1 = 1-2 = -1 % 3;
	// only the function (1 - (theirs - mine)) % 3 fits. To avoid problems with the mod function we use the
	// (mathematically) equivalent outcome = (4 - (theirs - mine) % 3.
	//
	// For part 2 a similar approach with a new table will yield: mine = (theirs + outcome) mod 3

	public int score1(int opval, int myval) {
		int outcome = (4 - (opval - myval)) % 3;
		return myval + 1 + outcome * 3;
	}

	public int score2(int opval, int myoutcome) {
		int myval = (opval + myoutcome + 2) % 3;
		return myval + 1 + myoutcome * 3;
	}

	public long calculate(boolean part1) {
		long accum = 0;
		for (String line : lines) {
			String[] guidevals = line.split(" ");
			int a = "ABC".indexOf(guidevals[0]);
			int b = "XYZ".indexOf(guidevals[1]);
			accum += part1 ? score1(a, b) : score2(a, b);
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise02 ex = new Exercise02(aocPath(2022, 2));

		// part 1 - 13809 (0.004s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.calculate(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 12316 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.calculate(false));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
