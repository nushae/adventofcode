package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.lcm;

@Log4j2
@Aoc(year = 2022, day = 11, title="Monkey in the Middle", keywords = {"expressions", "pseudo-game"})
public class Exercise11 {

	public static class Monkey {
		public int num;
		public List<BigInteger> items;
		public String op; // always + or *
		public BigInteger secondOperand; // if -1 -> old
		public int throwToIfTrue;
		public int throwToIfFalse;
		public BigInteger divisibleBy;

		public Monkey() {
			items = new ArrayList<>();
		}

		public String toString() {
			return "Monkey " + num +
					":\n  Starting items: " + StringUtils.join(items,", ") +
					"\n  Operation: new = old " + op + " " + (secondOperand.compareTo(BigInteger.valueOf(-1)) == 0 ? "old" : ""+secondOperand) +
					"\n  Test: divisible by " + divisibleBy +
					"\n    If true: throw to monkey " + throwToIfTrue +
					"\n    If false: throw to monkey "+throwToIfFalse;
		}
	}

	List<String> lines;
	Map<Integer, Monkey> monkeys;
	BigInteger allDivisors;

	public Exercise11() {
		lines = new ArrayList<>();
	}

	public Exercise11(Path path) {
		this();
		loadInputFromFile(lines, path);
		lines.add("");
	}

	public Exercise11(String data) {
		this();
		loadInputFromData(lines, data);
		lines.add("");
	}

	private void processInput() {
		monkeys = new HashMap<>();
		Monkey current = new Monkey();
		allDivisors = BigInteger.ONE;
		for (String line : lines) {
			if ("".equals(line)) { // new monkey
				monkeys.put(current.num, current);
				allDivisors = lcm(allDivisors, current.divisibleBy);
				current = new Monkey();
			} else if (line.startsWith("Monkey ")) {
				current.num = Integer.parseInt(line.substring(7, line.length()-1));
			} else if (line.startsWith("  Starting items: ")) {
				String items = line.substring("  Starting items: ".length());
				for (String item : items.split(", ")) {
					current.items.add(BigInteger.valueOf(Integer.parseInt(item)));
				}
			} else if (line.startsWith("  Operation: ")) {
				current.op = line.substring("  Operation: new = old ".length(), "  Operation: new = old X".length());
				String oper = line.substring("  Operation: new = old X ".length());
				current.secondOperand = ("old".equals(oper) ? BigInteger.valueOf(-1) : BigInteger.valueOf(Integer.parseInt(oper)));
			} else if (line.startsWith("  Test: divisible by ")) {
				current.divisibleBy = BigInteger.valueOf(Integer.parseInt(line.substring("  Test: divisible by ".length())));
			} else if (line.startsWith("    If true: throw to monkey ")) {
				current.throwToIfTrue = Integer.parseInt(line.substring("    If true: throw to monkey ".length()));
			} else if (line.startsWith("    If false: throw to monkey ")) {
				current.throwToIfFalse = Integer.parseInt(line.substring("    If false: throw to monkey ".length()));
			} else {
				System.out.println("Don't recognise this line: '" + line + "'");
			}
		}
		for (Monkey m : monkeys.values()) {
			log.debug(m.toString());
		}
	}

	public long doTheMonkeyShuffle(int rounds) {
		processInput(); // needed to reset
		long[] inspected = new long[monkeys.size()];
		for (int round = 0; round < rounds; round++) {
			// one round:
			for (int m = 0; m < monkeys.size(); m++) {
				Monkey current = monkeys.get(m);
				Iterator<BigInteger> itemIt = current.items.iterator();
				while (itemIt.hasNext()) {
					inspected[current.num]++;
					int nxt;
					BigInteger currentItem = itemIt.next();
					if ("+".equals(current.op)) {
						currentItem = currentItem.add(current.secondOperand.compareTo(BigInteger.valueOf(-1)) == 0 ? currentItem : current.secondOperand);
					} else {
						currentItem = currentItem.multiply(current.secondOperand.compareTo(BigInteger.valueOf(-1)) == 0 ? currentItem : current.secondOperand);
					}
					if (rounds <= 20) {
						currentItem = currentItem.divide(BigInteger.valueOf(3));
					}
					currentItem = currentItem.mod(allDivisors);
					if (currentItem.mod(current.divisibleBy).compareTo(BigInteger.ZERO) == 0) {
						nxt = current.throwToIfTrue;
					} else {
						nxt = current.throwToIfFalse;
					}
					itemIt.remove();
					monkeys.get(nxt).items.add(currentItem);
				}
			}
		}
		// now with automated product of the two maxes!
		return Arrays.stream(inspected).map(i -> -i).sorted().map(i -> -i).limit(2).reduce(1L, (a, b) -> a * b);
	}

	public static void main(String[] args) {
		Exercise11 ex = new Exercise11(aocPath(2022, 11));

		// part 1 - 120056 (0.312s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doTheMonkeyShuffle(20));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 21816744824 (0.158s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doTheMonkeyShuffle(10000));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
