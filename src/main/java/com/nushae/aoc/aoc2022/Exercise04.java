package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 4, title="Camp Cleanup", keywords = {"ranges", "intersection"})
public class Exercise04 {

	private static final Pattern RANGES = Pattern.compile("(\\d+)-(\\d+),(\\d+)-(\\d+)");

	List<String> lines;
	int count1, count2;

	public Exercise04() {
		lines = new ArrayList<>();
	}

	public Exercise04(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise04(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processWithPattern() {
		count1 = 0;
		count2 = 0;
		for (String line : lines) {
			Matcher m = RANGES.matcher(line);
			if (m.find()) {
				int left1 = Integer.parseInt(m.group(1));
				int right1 = Integer.parseInt(m.group(2));
				int left2 = Integer.parseInt(m.group(3));
				int right2 = Integer.parseInt(m.group(4));
				if (right1 <= right2 && left1 >= left2 || right2 <= right1 && left2 >= left1) {
					count1++;
				}
				if (!(right1 < left2 || right2 < left1)) {
					count2++;
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
	}

	public long doPart1() {
		return count1;
	}

	public long doPart2() {
		return count2;
	}

	public static void main(String[] args) {
		Exercise04 ex = new Exercise04(aocPath(2022, 4));

		// Input processing (0.005s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 576 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 905 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
