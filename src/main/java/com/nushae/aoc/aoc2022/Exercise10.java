package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.OCRUtil.crt4x6AsString;
import static com.nushae.aoc.util.OCRUtil.drawCRT;

@Log4j2
@Aoc(year = 2022, day = 10, title="Cathode-Ray Tube", keywords = {"big-letters", "computer-simulation"})
public class Exercise10 {

	List<String> lines;
	boolean[][] grid;
	long cycle;
	long registerX;

	public Exercise10() {
		lines = new ArrayList<>();
	}

	public Exercise10(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise10(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public long doBothParts(boolean drawCrt) {
		cycle = 1;
		registerX = 1;
		grid = new boolean[6][40];
		long accum = 0;
		for (String line : lines) {
			String[] parts = line.split(" ");
			accum += (cycle % 40 == 20) ? cycle * registerX : 0;
			calcPixel();
			switch (parts[0]) {
				case "noop":
					break;
				case "addx":
					cycle++;
					accum += (cycle % 40 == 20) ? cycle * registerX : 0;
					calcPixel();
					registerX += Long.parseLong(parts[1]);
					break;
			}
			cycle++;
		}
		if (drawCrt) {
			System.out.println(drawCRT(grid));
		}
		log.info(crt4x6AsString(grid));
		return accum;
	}

	public void calcPixel() {
		int crtPosition = (int) ((cycle-1) % 240);
		int y = crtPosition / 40;
		int x = crtPosition % 40;
		grid[y][x] = registerX - 1 <= x && registerX + 1 >= x;
	}

	public static void main(String[] args) {
		Exercise10 ex = new Exercise10(aocPath(2022, 10));

		// parts 1 & 2 - 14760 / EFGERURE (0.008 sec)
		long start = System.currentTimeMillis();
		log.info("Part 1 & 2:");
		log.info(ex.doBothParts(true));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
