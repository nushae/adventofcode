package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.TreeNode;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 7, title="No Space Left On Device", keywords = {"tree-processing"})
public class Exercise07 {

	List<String> lines;
	List<Long> numbers;
	TreeNode<Integer> fileSystem;

	public Exercise07() {
		lines = new ArrayList<>();
	}

	public Exercise07(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise07(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		numbers = new ArrayList<>();
		fileSystem = new TreeNode<>("/", 0);
		TreeNode<Integer> currentLoc = fileSystem;
		for (String line : lines) {
			if (line.startsWith("$ cd /")) {
				currentLoc = fileSystem;
			} else if (line.startsWith("$ cd ..")) {
				currentLoc = currentLoc.getParentNode();
			} else if (line.startsWith("$ cd ")) {
				String name = line.substring(5);
				for (TreeNode<Integer> tn : currentLoc.getChildren()) {
					if (tn.getName().equals(name)) {
						currentLoc = tn;
						break;
					}
				}
			} else if (line.startsWith("dir ")) { // add dir
				String name = line.substring(4);
				TreeNode<Integer> file = new TreeNode<>(name, 0);
				currentLoc.addChild(file);
				file.setParent(currentLoc);
			} else if (!line.startsWith("$ ls")) { // file
				int size = Integer.parseInt(line.split(" ")[0]);
				String name = line.split(" ")[1];
				TreeNode<Integer> file = new TreeNode<>(name, size);
				file.setParent(currentLoc);
				currentLoc.addChild(file);
			}
		}
		calcDirs(fileSystem);
	}

	// Helper function to calculate the values of 'dir' nodes after tree construction
	private void calcDirs(TreeNode<Integer> start) {
		if (start.getValue() == 0) { // unprocessed dir
			for (TreeNode<Integer> tn : start.getChildren()) {
				calcDirs(tn);
				start.setValue(start.getValue() + tn.getValue());
			}
		}
	}

	public long doPart1() {
		return fileSystem.getPreOrder().stream()
				.filter(TreeNode::isInternal)
				.filter(n -> n.getValue() < 100000)
				.mapToLong(TreeNode::getValue)
				.sum();
	}

	public long doPart2() {
		long needed = 30000000 - (70000000 - fileSystem.getValue());
		return fileSystem.getPreOrder().stream()
				.filter(TreeNode::isInternal)
				.filter(n -> n.getValue() >= needed)
				.mapToLong(TreeNode::getValue)
				.min().orElse(0L);
	}

	public static void main(String[] args) {
		Exercise07 ex = new Exercise07(aocPath(2022, 7));

		// Input processing (0.005s)
		long start = System.currentTimeMillis();
		log.info("Preprocessing:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 1491614 (0.005s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 6400111 (0.003s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
