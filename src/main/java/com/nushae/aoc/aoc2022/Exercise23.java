package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2022, day = 23, title="Unstable Diffusion", keywords = {"grid", "pathing"})
public class Exercise23 {

	private static final Point[][] MOVES = {
			{NORTH, NORTHEAST, NORTHWEST},
			{SOUTH, SOUTHEAST, SOUTHWEST},
			{WEST, NORTHWEST, SOUTHWEST},
			{EAST, NORTHEAST, SOUTHEAST}
	};

	List<String> lines;
	Set<Point> elves;
	Map<Point, List<Point[]>> moveMap;
	int round;
	int minx;
	int maxx;
	int miny;
	int maxy;

	public Exercise23() {
		lines = new ArrayList<>();
	}

	public Exercise23(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise23(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		elves = new HashSet<>();
		for (int row = 0; row < lines.size(); row++) {
			String line = lines.get(row);
			for (int col = 0; col < line.length(); col++) {
				if (line.charAt(col) == '#') {
					elves.add(new Point(col, row));
				}
			}
		}
		log.info("Input processed - " + elves.size() + " elves read.");
	}

	public long doPart1() {
		processInput();
		round = 0;
		for (int i = 0; i < 10; i++) {
			oneRound();
		}
		minx = Integer.MAX_VALUE;
		maxx = Integer.MIN_VALUE;
		miny = Integer.MAX_VALUE;
		maxy = Integer.MIN_VALUE;
		for (Point p : elves) {
			minx = Math.min(minx, p.x);
			maxx = Math.max(maxx, p.x);
			miny = Math.min(miny, p.y);
			maxy = Math.max(maxy, p.y);
		}
		return (maxx - minx + 1L) * (maxy - miny + 1L) - elves.size();
	}

	public int oneRound() {
		// one round
		int moved = 0;
		moveMap = new HashMap<>();
		for (Point elf : elves) {
			Point goalPoint = new Point(elf.x, elf.y);
			// FIRST CHECK IF THERE ARE NEIGHBOURS DUMMY
			boolean free = true;
			for (Point p : KING_DIRS) {
				free &= !elves.contains(new Point(elf.x+p.x,elf.y+p.y));
			}
			// if free is still true, there were no elves in the 8 surrounding cells -> skip moving
			if (free) {
				continue;
			}
			for (int i = 0; !free && i < 4; i++) {
				int index = (round +i) % 4;
				goalPoint = new Point(elf.x + MOVES[index][0].x, elf.y + MOVES[index][0].y);
				Point nb1 = new Point(elf.x + MOVES[index][1].x, elf.y + MOVES[index][1].y);
				Point nb2 = new Point(elf.x + MOVES[index][2].x, elf.y + MOVES[index][2].y);
				free = !elves.contains(goalPoint) && !elves.contains(nb1) && !elves.contains(nb2);
			}
			if (free) {
				moveMap.computeIfAbsent(goalPoint, p -> new ArrayList<>()).add(new Point[] {elf, goalPoint});
			}
		}
		round++;
		// now the map has either a single value for a destination, or multiple, only move in the former case
		for (Map.Entry<Point, List<Point[]>> moves : moveMap.entrySet()) {
			Point goal = new Point(moves.getKey().x, moves.getKey().y);
			List<Point[]> elfMoves = moves.getValue();
			if (elfMoves.size() == 1) { // execute move
				moved++;
				elves.remove(elfMoves.get(0)[0]);
				elves.add(goal);
			}
		}

		log.debug(getStateString());
		return moved;
	}

	public String getStateString() {
		StringBuilder stb = new StringBuilder();
		minx = Integer.MAX_VALUE;
		maxx = Integer.MIN_VALUE;
		miny = Integer.MAX_VALUE;
		maxy = Integer.MIN_VALUE;
		for (Point p : elves) {
			minx = Math.min(minx, p.x);
			maxx = Math.max(maxx, p.x);
			miny = Math.min(miny, p.y);
			maxy = Math.max(maxy, p.y);
		}

		stb.append(maxx-minx+1).append(" x ").append(maxy-miny+1);

		for (int y = miny; y <= maxy; y++) {
			for (int x = minx; x <= maxx; x++) {
				stb.append(elves.contains(new Point(x, y)) ? "#" : ".");
			}
			stb.append("\n");
		}
		return stb.toString();
	}

	public long doPart2() {
		processInput();
		round = 0;
		while (oneRound() > 0);
		return round;
	}

	public static void main(String[] args) {
		Exercise23 ex = new Exercise23(aocPath(2022, 23));

		// part 1 - 3689 (0.033s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 965 (1.168s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
