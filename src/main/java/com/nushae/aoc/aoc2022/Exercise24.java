package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.*;

@Log4j2
@Aoc(year = 2022, day = 24, title="Blizzard Basin", keywords = {"grid", "pathing", "lcm"})
public class Exercise24 {

	List<String> lines;
	Set<Point> passable;
	Map<Character, Set<Point>> blizzards;
	Point startLoc;
	Point goalLoc;
	int hperiod;
	int vperiod;
	int indexPeriod;
	int minute;

	public Exercise24() {
		lines = new ArrayList<>();
	}

	public Exercise24(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise24(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		passable = new HashSet<>();
		blizzards = new HashMap<>();
		hperiod = (lines.get(0).length()-2);
		vperiod = (lines.size()-2);
		indexPeriod = lcm(hperiod, vperiod);
		for (int row =0; row < lines.size(); row++) {
			String line = lines.get(row);
			for (int col = 0; col < line.length(); col++) {
				char c = line.charAt(col);
				Point pos = new Point(col-1, row-1);
				switch (c) {
					case '#': // wall
						break;
					case '<':
					case '>':
					case '^':
					case 'v':
						log.debug("Added blizzard " + c + " at " + pos);
						Set<Point> blizzLocs = blizzards.computeIfAbsent(c, x -> new HashSet<>());
						blizzLocs.add(pos);
					case '.': // open space
						passable.add(pos);
						if (row == 0) {
							startLoc = pos;
						} else if (row == lines.size()-1) {
							goalLoc = pos;
						}
						break;
				}
			}
		}
		for (char dir : new char[]{'<', '>', 'v', '^'}) {
			log.debug(dir + ": " + blizzards.getOrDefault(dir, new HashSet<>()).size());
		}
		log.debug("H-period: " + hperiod);
		log.debug("V-period: " + vperiod);
		log.debug("Combined period: " + indexPeriod);
	}

	public long blizzBFS(int numTrips) {
		processInput(); // to reset
		Set<Point> currentReachable = new HashSet<>();
		currentReachable.add(startLoc);
		int trips = 0;
		minute = 1;
		while (true) {
			// calculate the location of each blizzard - these positions are blocked
			Set<Point> blocked = new HashSet<>();
			for (char dir : new char[]{'<', '>', 'v', '^'}) {
				for (Point p : blizzards.getOrDefault(dir, new HashSet<>())) {
					blocked.add(new Point(((p.x + CHAR_TO_DIR.get(dir).x * minute) % hperiod + hperiod) % hperiod, ((p.y + CHAR_TO_DIR.get(dir).y * minute) % vperiod + vperiod) % vperiod));
				}
			}
			// calculate next search frontier from the current one
			Set<Point> nextReachable = new HashSet<>();
			for (Point p : currentReachable) {
				for (Point nextStep : new Point[] {p, sum(p, NORTH), sum(p, EAST), sum(p, SOUTH), sum(p, WEST)}) {
					if (passable.contains(nextStep) && !blocked.contains(nextStep)) {
						nextReachable.add(nextStep);
					}
				}
			}
			// if the next frontier has our goal, we're done
			if (nextReachable.contains(goalLoc)) {
				trips++;
				if (trips == numTrips) {
					return minute;
				} else {
					// reset search frontier but in opposite direction
					Point tmp = new Point(goalLoc.x, goalLoc.y);
					goalLoc = new Point(startLoc.x, startLoc.y);
					startLoc = new Point(tmp.x, tmp.y);
					currentReachable = new HashSet<>();
					currentReachable.add(startLoc);
				}
			} else {
				currentReachable = nextReachable;
			}
			minute++;
		}
	}

	public long doPart1() {
		return blizzBFS(1);
	}

	public long doPart2() {
		return blizzBFS(3);
	}

	public static void main(String[] args) {
		Exercise24 ex = new Exercise24(aocPath(2022, 24));

		// part 1 - 228 (0.142s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 723 (0.293s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
