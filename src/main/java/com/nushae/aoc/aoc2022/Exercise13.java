package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 13, title="Distress Signal", keywords = {"sorting", "nested-numbers"})
public class Exercise13 {

	public static class Packet {
		private static String remainingInput;
		private Integer value;
		private boolean hasList = false;
		public List<Packet> sublist = new ArrayList<>();

		public Packet(String representation) {
			remainingInput = representation;
			Packet p = parsePacket();
			this.value = p.value;
			this.hasList = p.hasList;
			this.sublist.addAll(p.sublist);
		}

		public Packet(int value) {
			this.value = value;
		}

		public Packet(List<Packet> values) {
			hasList = true;
			sublist.addAll(values);
		}

		private Packet parsePacket() {
			if (remainingInput.startsWith("[")) {
				remainingInput = remainingInput.substring(1); // eat [
				List<Packet> sub = new ArrayList<>();
				while (!remainingInput.startsWith("]")) {
					sub.add(parsePacket());
					if (remainingInput.startsWith(",")) {
						remainingInput = remainingInput.substring(1); // eat ,
					}
				}
				remainingInput = remainingInput.substring(1); // eat ]
				return new Packet(sub);
			} else {
				StringBuilder num = new StringBuilder();
				while (remainingInput.charAt(0) >= '0' && remainingInput.charAt(0) <= '9') {
					num.append(remainingInput.charAt(0));
					remainingInput = remainingInput.substring(1); // eat digit
				}
				return new Packet(Integer.parseInt(num.toString().trim()));
			}
		}

		public void addValue(int val) {
			if (!hasList) {
				sublist.add(new Packet(value));
				value = null;
				hasList = true;
			}
			sublist.add(new Packet(val));
		}

		public int compareTo(Packet other) {
			if (!hasList && !other.hasList) {
				return (int) Math.signum(value - other.value);
			} else { // at most one value is an integer
				List<Packet> left;
				List<Packet> right;
				if (hasList) {
					left = new ArrayList<>(sublist);
				} else {
					left = Collections.singletonList(this);
				}
				if (other.hasList) {
					right = new ArrayList<>(other.sublist);
				} else {
					right = Collections.singletonList(other);
				}
				if (left.isEmpty() || right.isEmpty()) {
					return (int) Math.signum(left.size() - right.size());
				}
				for (int i=0; i < Math.min(left.size(), right.size()); i++) {
					if (left.get(i).compareTo(right.get(i)) != 0) { // do NOT use equals here because of wrapping-in-singleton :(
						return left.get(i).compareTo(right.get(i));
					}
				}
				return (int) Math.signum(left.size() - right.size());
			}
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (obj == this) {
				return true;
			}
			if (obj.getClass() != getClass()) {
				return false;
			}
			Packet other = (Packet) obj;
			return compareTo(other) == 0;
		}

		@Override
		public int hashCode() {
			return toString().replaceAll("[\\[\\],]", "").hashCode();
		}

		@Override
		public String toString() {
			StringBuilder stb = new StringBuilder();
			if (hasList || value == null) {
				stb.append("[");
			}
			if (value != null) {
				stb.append(value);
			}
			if (hasList) {
				List<String> sublists = new ArrayList<>();
				for (Packet p : sublist) {
					sublists.add(p.toString());
				}
				stb.append(StringUtils.join(sublists, ","));
			}
			if (hasList || value == null) {
				stb.append("]");
			}
			return stb.toString();
		}
	}

	List<String> lines;
	List<Packet[]> packetPairs;
	List<Packet> allPackets;

	public Exercise13() {
		lines = new ArrayList<>();
	}

	public Exercise13(Path path) {
		this();
		loadInputFromFile(lines, path);
		lines.add("");
	}

	public Exercise13(String data) {
		this();
		loadInputFromData(lines, data);
		lines.add("");
	}

	private void processInput() {
		packetPairs = new ArrayList<>();
		allPackets = new ArrayList<>();
		Packet[] pair;
		Iterator<String> lineIt = lines.iterator();
		while (lineIt.hasNext()) {
			pair = new Packet[2];
			pair[0] = new Packet(lineIt.next());
			allPackets.add(pair[0]);
			pair[1] = new Packet(lineIt.next());
			allPackets.add(pair[1]);
			packetPairs.add(pair);
			lineIt.next();
		}
	}

	public long doPart1() {
		processInput();
		long correct = 0;
		int index = 1;
		for (Packet[] pair : packetPairs) {
			if (pair[0].compareTo(pair[1]) <= 0) {
				correct += index;
			}
			index++;
		}
		return correct;
	}

	public long doPart2() {
		processInput();
		final Packet p2 = new Packet("[[2]]");
		final Packet p6 = new Packet("[[6]]");
		allPackets.add(p2);
		allPackets.add(p6);
		allPackets = allPackets.stream().sorted(Packet::compareTo).collect(Collectors.toList());
		long accum = 1;
		int index = 1;
		for (Packet p : allPackets) {
			if (p.equals(p2) || p.equals(p6)) {
				accum *= index;
			}
			index++;
		}
		return accum;
	}

	public static void main(String[] args) {
		Exercise13 ex = new Exercise13(aocPath(2022, 13));

		// part 1 - 5625 (0.011s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 23111 (0.011s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
