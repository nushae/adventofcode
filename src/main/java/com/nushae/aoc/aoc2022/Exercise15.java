package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.List;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.manhattanDistance;

@Log4j2
@Aoc(year = 2022, day = 15, title="Beacon Exclusion Zone", keywords = {"3d", "boundingbox-intersection"})
public class Exercise15 {

	private static final boolean REPORT = false; // do NOT put on true for actual AOC input data!! Only for an example
	private static final Pattern SENSORS = Pattern.compile("Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)");

	List<String> lines;
	Map<Point, Point> sensorBeaconMap;

	public Exercise15() {
		lines = new ArrayList<>();
	}

	public Exercise15(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise15(String data) {
		this();
		loadInputFromData(lines, data);
	}

	void processWithPattern() {
		sensorBeaconMap = new HashMap<>();
		Set<Point> allpoints = new HashSet<>(); // only doable for very small datasets!!
		int minX = Integer.MAX_VALUE;
		int maxX = Integer.MIN_VALUE;
		int minY = Integer.MAX_VALUE;
		int maxY = Integer.MIN_VALUE;

		for (String line : lines) {
			Matcher m = SENSORS.matcher(line);
			if (m.find()) {
				Point sensor = new Point(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
				Point beacon = new Point(Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)));
				sensorBeaconMap.put(sensor, beacon);
				if (REPORT) {
					int radius = manhattanDistance(sensor, beacon);
					for (int x = sensor.x-radius; x <= sensor.x+radius; x++) {
						for (int y = sensor.y-radius; y <= sensor.y+radius; y++) {
							Point p = new Point(x, y);
							if (manhattanDistance(p, sensor) <= radius) {
								minX = Math.min(minX, p.x);
								maxX = Math.max(maxX, p.x);
								minY = Math.min(minY, p.y);
								maxY = Math.max(maxY, p.y);
								allpoints.add(p);
							}
						}
					}
				}
			} else {
				log.warn("Don't know how to parse " + line);
			}
		}
		if (REPORT) {
			log.debug("y = " + minY);
			for (int row = minY; row <= maxY; row++) {
				StringBuilder sb = new StringBuilder();
				for (int col = minX; col <= maxX; col++) {
					Point p = new Point(col, row);
					sb.append(sensorBeaconMap.containsKey(p) ? "S" : sensorBeaconMap.containsValue(p) ? "B" : allpoints.contains(p) ? "#" : ".");
				}
				log.debug(sb);
			}
			log.debug("y = " + maxY);
		}
	}

	public long doPart1(int row) {
		Set<Point> beacons = new HashSet<>();
		Set<Point> sensors = new HashSet<>();
		Set<Point> areaChecked = new HashSet<>();
		for (Point sensor : sensorBeaconMap.keySet()) {
			Point beacon = sensorBeaconMap.get(sensor);
			sensors.add(sensor);
			beacons.add(beacon);
			int d = manhattanDistance(sensor, beacon);
			if (sensor.y-d <= row && sensor.y+d >= row) { // row intersects sensor area
				for (int x = sensor.x - d; x <= sensor.x + d; x++) {
					Point p = new Point(x, row);
					if (manhattanDistance(sensor, p) <= d) { // p is within sensor area
						if (!sensors.contains(p) && !beacons.contains(p)) { // sensors and beacons aren't counted (!)
							areaChecked.add(p);
						}
					}
				}
			}
		}
		return areaChecked.size();
	}

	public long doPart2(int limit) {
		for (Point sensor : sensorBeaconMap.keySet()) {
			int radius = manhattanDistance(sensor, sensorBeaconMap.get(sensor)) + 1;
			Set<Point> ring = new HashSet<>();
			for (int i = 0; i < radius; i++) {
				Point p = new Point(sensor.x-radius+i, sensor.y-i); // NW quarter, clockwise
				if (p.x >= 0 && p.y >= 0 && p.x <= limit && p.y <= limit) {
					ring.add(p);
				}
				p = new Point(sensor.x+i, sensor.y-radius+i); // NE quarter, clockwise
				if (p.x >= 0 && p.y >= 0 && p.x <= limit && p.y <= limit) {
					ring.add(p);
				}
				p = new Point(sensor.x+radius-i, sensor.y+i); // SE quarter, clockwise
				if (p.x >= 0 && p.y >= 0 && p.x <= limit && p.y <= limit) {
					ring.add(p);
				}
				p = new Point(sensor.x-i, sensor.y+radius-i); // SW quarter, clockwise
				if (p.x >= 0 && p.y >= 0 && p.x <= limit && p.y <= limit) {
					ring.add(p);
				}
			}
			// now remove the points one by one if in range of any sensor
			Iterator<Point> pit = ring.iterator();
			while (pit.hasNext()) {
				Point ringPoint = pit.next();
				for (Point otherSensor : sensorBeaconMap.keySet()) {
					if (manhattanDistance(otherSensor, ringPoint) <= manhattanDistance(otherSensor, sensorBeaconMap.get(otherSensor))) { // falls within this sensor's range
						pit.remove();
						break;
					}
				}
			}
			if (ring.size() == 1) { // it's on this sensor's ring!
				Point p = ring.stream().findFirst().get();
				return 4000000L * (long) p.x + (long) p.y;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Exercise15 ex = new Exercise15(aocPath(2022, 15));

		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processWithPattern();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 5461729 (1.889s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(2000000));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 10621647166538 (0.565s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2(4000000));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
