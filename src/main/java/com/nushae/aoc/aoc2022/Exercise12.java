package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.BasicGraphNode;
import com.nushae.aoc.domain.GraphNode;
import lombok.extern.log4j.Log4j2;

import java.awt.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import static com.nushae.aoc.util.GraphUtil.findShortestPath;
import static com.nushae.aoc.util.IOUtil.*;
import static com.nushae.aoc.util.MathUtil.ORTHO_DIRS;

@Log4j2
@Aoc(year = 2022, day = 12, title="Hill Climbing Algorithm", keywords = {"grid", "shortest-path"})
public class Exercise12 {

	List<String> lines;
	char[][] grid;

	public Exercise12() {
		lines = new ArrayList<>();
	}

	public Exercise12(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise12(String data) {
		this();
		loadInputFromData(lines, data);
	}

	public void processInput() {
		grid = new char[lines.size()][lines.get(0).length()];
		for (int y = 0; y < lines.size(); y++) {
			String line = lines.get(y);
			for (int x = 0 ; x < line.length(); x++) {
				grid[y][x] = line.charAt(x);

			}
		}
	}

	public long doPart1() {
		BasicGraphNode<Point> start = makeGraph(grid, p -> grid[p.x][p.y] == 'S', (p, q) -> dist(grid[p.x][p.y],grid[q.x][q.y]) <= 1).get(0);
		List<? extends GraphNode<Point>> result = findShortestPath(start, n -> grid[n.getPayload().x][n.getPayload().y] == 'E');
		for (int x = 0; x < grid.length; x++) {
			StringBuilder sb = new StringBuilder();
			for (int y = 0; y < grid[0].length; y++) {
				final int xX = x;
				final int yY = y;
				if (result.stream().anyMatch(a -> a.getPayload().x == xX && a.getPayload().y == yY)) {
					sb.append(grid[x][y]);
				} else {
					sb.append(".");
				}
			}
			log.debug(sb.toString());
		}
		return result.size()-1;
	}

	public long doPart2() {
		List<BasicGraphNode<Point>> goals = makeGraph(grid, p -> grid[p.x][p.y] == 'S' || grid[p.x][p.y] == 'a', (p, q) -> dist(grid[p.x][p.y],grid[q.x][q.y]) <= 1);
		long minLen = Long.MAX_VALUE;
		for (BasicGraphNode<Point> start : goals) {
			long len = findShortestPath(start, n -> grid[n.getPayload().x][n.getPayload().y] == 'E').size()-1; // -1 if no path available -> skip
			if (len > 0) {
				minLen = Math.min(minLen, len);
			}
		}
		return minLen;
	}

	// LESSON: do not assume start = (0,0)!!
	public List<BasicGraphNode<Point>> makeGraph(char[][] grid, Predicate<Point> startPredicate, BiPredicate<Point, Point> connectPredicate) {
		int width = grid.length;
		int height = grid[0].length;
		Map<Point, BasicGraphNode<Point>> nodes = new HashMap<>();
		List<BasicGraphNode<Point>> startNodes = new ArrayList<>();
		for (int x = 0; x < width; x++) {
			for (int y=0; y < height; y++) {
				Point coords = new Point(x, y);
				BasicGraphNode<Point> node = new BasicGraphNode<>("", coords, coords);
				if (startPredicate.test(coords)) {
					startNodes.add(node);
				}
				nodes.put(coords, node);
				for (Point dir : ORTHO_DIRS) {
					Point neighbour = new Point(coords.x + dir.x, coords.y + dir.y);
					if (nodes.containsKey(neighbour)) {
						if (connectPredicate.test(coords, neighbour)) {
							node.addNeighbour(nodes.get(neighbour), 1);
						}
						if (connectPredicate.test(neighbour, coords)) {
							nodes.get(neighbour).addNeighbour(node, 1);
						}
					}
				}
			}
		}
		return startNodes;
	}

	public int dist(char a, char b) {
		if (a == 'S') {
			a = 'a';
		} else if (a == 'E') {
			a = 'z';
		}
		if (b == 'E') {
			b = 'z';
		} else if (b == 'S') {
			b = 'a';
		}
		return b - a;
	}

	public static void main(String[] args) {
		Exercise12 ex = new Exercise12(aocPath(2022, 12));

		// Input processing (0.002 sec)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 468 (0.396 sec)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 459 (0.459 sec)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
