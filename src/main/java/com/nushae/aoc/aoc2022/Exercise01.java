package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 1, title="Calorie Counting", keywords = {"grouping", "max"})
public class Exercise01 {

	List<String> lines;
	List<Long> numbers;

	public Exercise01() {
		lines = new ArrayList<>();
	}

	public Exercise01(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise01(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput() {
		lines.add("");
		numbers = new ArrayList<>();
		long accum = 0;
		for (String line : lines) {
			if ("".equals(line)) {
				numbers.add(accum);
				accum = 0;
			} else {
				accum += Long.parseLong(line);
			}
		}
		numbers.sort(Comparator.reverseOrder());

		log.info("Input processed: " + numbers.size() + " totals calculated");
	}

	public long doPart1() {
		return numbers.get(0);
	}

	public long doPart2() {
		return numbers.get(0)+numbers.get(1)+numbers.get(2);
	}

	public static void main(String[] args) {
		Exercise01 ex = new Exercise01(aocPath(2022, 1));

		// Process input (0.007s)
		long start = System.currentTimeMillis();
		log.info("Preprocess:");
		ex.processInput();
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 1 - 69626 (<0.001s)
		start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 206780 (0.002s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart2());
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
