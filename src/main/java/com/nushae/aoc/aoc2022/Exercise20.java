package com.nushae.aoc.aoc2022;

import com.nushae.aoc.domain.Aoc;
import com.nushae.aoc.domain.Pair;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.nushae.aoc.util.IOUtil.*;

@Log4j2
@Aoc(year = 2022, day = 20, title="Grove Positioning System", keywords = {"permutation", "circular-list", "modulo"})
public class Exercise20 {

	List<String> lines;
	List<Pair<Long, Long>> numbers;

	public Exercise20() {
		lines = new ArrayList<>();
	}

	public Exercise20(Path path) {
		this();
		loadInputFromFile(lines, path);
	}

	public Exercise20(String data) {
		this();
		loadInputFromData(lines, data);
	}

	private void processInput(int key) {
		numbers = new ArrayList<>();
		long index = 0;
		for (String line : lines) {
			numbers.add(new Pair<>(index++, key*Long.parseLong(line))); // uniquefy duplicates!!
		}
		// report on processed data
		log.debug("Read " + numbers.size() + " numbers.");
	}

	public long doPart1(int rounds, int key) {
		processInput(key);
		List<Pair<Long, Long>> toBeMixed = new ArrayList<>(numbers);
		for (int r = 0; r < rounds; r++) {
			for (Pair<Long, Long> num : numbers) {
				int positionInTBM = 0;
				for (int i = 0; i < toBeMixed.size(); i++) {
					if (toBeMixed.get(i).equals(num)) {
						positionInTBM = i;
						break;
					}
				}
				log.debug(toBeMixed + " -[" + num + "]-> ");
				toBeMixed.remove(positionInTBM);
				toBeMixed.add((int) ((positionInTBM + num.getRight()) % toBeMixed.size() + toBeMixed.size()) % toBeMixed.size(), num);
				log.debug(" = " + toBeMixed);
			}
		}
		int zeroPos = 0;
		for (int i=0; i< toBeMixed.size(); i++) {
			if (toBeMixed.get(i).getRight() == 0) {
				zeroPos = i;
				break;
			}
		}
		long first = toBeMixed.get((zeroPos+1000) % toBeMixed.size()).getRight();
		long second = toBeMixed.get((zeroPos+2000) % toBeMixed.size()).getRight();
		long third = toBeMixed.get((zeroPos+3000) % toBeMixed.size()).getRight();
		log.debug(zeroPos + ": " + first + " + " + second + " " + third + " = " + (first+second+third));
		return first+second+third;
	}

	public static void main(String[] args) {
		Exercise20 ex = new Exercise20(aocPath(2022, 20));

		// part 1 - 18257 (5.847s)
		long start = System.currentTimeMillis();
		log.info("Part 1:");
		log.info(ex.doPart1(1, 1));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");

		// part 2 - 4148032160983 (1m21.555s)
		start = System.currentTimeMillis();
		log.info("Part 2:");
		log.info(ex.doPart1(10, 811589153));
		log.info("Took " + (System.currentTimeMillis() - start) + "ms");
	}
}
