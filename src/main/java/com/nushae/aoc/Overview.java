package com.nushae.aoc;

import com.nushae.aoc.domain.Aoc;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Overview {

	private static final String MY_PACKAGE = Overview.class.getPackage().getName();
	private static final boolean RUN_EVERYTHING = false;

	static int compareClasses(Class<?> a, Class<?> b) {
		Aoc annotationA = a.getAnnotation(Aoc.class);
		Aoc annotationB = b.getAnnotation(Aoc.class);
		return 100 * annotationA.year() + annotationA.day() - 100 * annotationB.year() - annotationB.day();
	}

	public static void main(String[] args) {
		int currentYear = -1;
		// The following line produces a 'SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder"' and I think it's inside the Reflections
		List<Class<?>> annotatedClasses = new ArrayList<>(new Reflections(MY_PACKAGE).getTypesAnnotatedWith(Aoc.class));
		annotatedClasses.sort(Overview::compareClasses);
		for (Class<?> clazz : annotatedClasses) {
			Aoc annotation = clazz.getAnnotation(Aoc.class);
			if (annotation.year() != currentYear && annotation.day() != 0) {
				currentYear = annotation.year();
				System.out.println("\n====");
				System.out.println(currentYear);
				System.out.println("====\n");
			}
			if (annotation.day() == 0 || List.of(annotation.keywords()).contains("incorrect-solution")) {
				continue;
			}
			System.out.printf("%02d - \"%s\"%n", annotation.day(), annotation.title());
			if (annotation.keywords().length > 0) {
				System.out.println("    " + StringUtils.join(annotation.keywords(), ", "));
			}
			if (RUN_EVERYTHING) {
				try {
					Method mainMethod = clazz.getMethod("main", String[].class);
					String[] params = null;
					try {
						mainMethod.invoke(null, (Object) params);
					} catch (IllegalAccessException | InvocationTargetException e) {
						log.warn("Invoking main method failed with an " + e.getClass().getSimpleName() + " exception...");
					}
				} catch (NoSuchMethodException e) {
					log.warn("Class does not have a main method...");
				}
			}
		}
	}
}
